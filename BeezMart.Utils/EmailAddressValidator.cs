﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BeezMart.Utils;

public static class EmailAddressValidator
{
    public static readonly string EmailAddressRegularExpression = @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17}))$";

    public static bool IsValidEmailAddress(string? EmailAddressToTest)
    {

        EmailAddressValidator._emailAddressIsinvalid = false;
        if (string.IsNullOrEmpty(EmailAddressToTest)) return false;
        // Use IdnMapping class to convert Unicode domain names. 
        try
        {
            EmailAddressToTest = Regex.Replace(EmailAddressToTest, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
        if (_emailAddressIsinvalid) return false;
        // Return true if strIn is in valid e-mail format. 
        try
        {
            return Regex.IsMatch(EmailAddressToTest, EmailAddressValidator.EmailAddressRegularExpression, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }

    private static IdnMapping _intlDomainNameMapper = new IdnMapping();

    private static string DomainMapper(Match match)
    {
        IdnMapping idn = new IdnMapping();
        string domainName = match.Groups[2].Value;
        try
        {
            domainName = EmailAddressValidator._intlDomainNameMapper.GetAscii(domainName);
        }
        catch (ArgumentException)
        {
            EmailAddressValidator._emailAddressIsinvalid = true;
        }
        return match.Groups[1].Value + domainName;
    }

    private static bool _emailAddressIsinvalid = false;
}