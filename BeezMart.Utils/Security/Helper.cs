﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeezMart.Utils.Security;

public class Helper
{
    public static readonly char[] lowerLetters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
    public static readonly char[] upperLetters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    public static readonly char[] figureChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    public static readonly char[] signChars = { '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '=', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~' };
    public static readonly char[][] allowedAscii = { lowerLetters, upperLetters, figureChars, signChars };
    /// <summary>
    ///     <para>Generates a strong password of minimum 8 characters.</para>
    ///     <para>If 'PasswordLength' parameter is lower than 8, it will be enforced to 8 characters.</para>
    ///     <para>Parameters validation:</para>
    ///     <para>if (MinimumLowercaseOccurences + MinimumUppercaseOccurences + MinimumFigureOccurences + MinimumSignOccurences > PasswordLength)</para>
    ///     <para>then all the above parameters will be reset to their default value of 2.</para>
    /// </summary>
    /// <param name="PasswordLength"></param>
    /// <param name="MinimumLowercaseOccurences"></param>
    /// <param name="MinimumUppercaseOccurences"></param>
    /// <param name="MinimumFigureOccurences"></param>
    /// <param name="MinimumSignOccurences"></param>
    /// <returns>A strong password of minimum 8 characters.</returns>
    public static string GeneratePassword(byte PasswordLength = 12, byte MinimumLowercaseOccurences = 2, byte MinimumUppercaseOccurences = 2, byte MinimumFigureOccurences = 2, byte MinimumSignOccurences = 2)
    {
        if (PasswordLength < 8) PasswordLength = 8;
        if (MinimumLowercaseOccurences + MinimumUppercaseOccurences + MinimumFigureOccurences + MinimumSignOccurences > PasswordLength)
        {
            MinimumLowercaseOccurences = 2;
            MinimumUppercaseOccurences = 2;
            MinimumFigureOccurences = 2;
            MinimumSignOccurences = 2;
        }
        // the password is initially an array of empty slots to be filled with chars
        char[] PasswordCharSlots = new char[PasswordLength];
        // keep a list with indexes of empty char slots in password; this list decreases while populating password
        List<byte> IndexesOfEmptySlotsInPassword = new List<byte>();
        // initially all password slots are empty; make a list of their indexes to know which one is empty
        for (byte i = 0; i < PasswordLength; i++)
        {
            IndexesOfEmptySlotsInPassword.Add(i);
        }
        Random random = new Random();
        byte emptySlotPicker;
        char passwordChar;
        #region fill the minimum number of char types: lowercase and uppercase letters, figures and signs
        for (byte i = 0; i < MinimumLowercaseOccurences; i++)
        {
            emptySlotPicker = (byte)random.Next(IndexesOfEmptySlotsInPassword.Count);
            passwordChar = lowerLetters[random.Next(lowerLetters.Length)];
            PasswordCharSlots[IndexesOfEmptySlotsInPassword[emptySlotPicker]] = passwordChar;
            IndexesOfEmptySlotsInPassword.RemoveAt(emptySlotPicker);
        }
        for (byte i = 0; i < MinimumUppercaseOccurences; i++)
        {
            emptySlotPicker = (byte)random.Next(IndexesOfEmptySlotsInPassword.Count);
            passwordChar = upperLetters[random.Next(upperLetters.Length)];
            PasswordCharSlots[IndexesOfEmptySlotsInPassword[emptySlotPicker]] = passwordChar;
            IndexesOfEmptySlotsInPassword.RemoveAt(emptySlotPicker);
        }
        for (byte i = 0; i < MinimumFigureOccurences; i++)
        {
            emptySlotPicker = (byte)random.Next(IndexesOfEmptySlotsInPassword.Count);
            passwordChar = figureChars[random.Next(figureChars.Length)];
            PasswordCharSlots[IndexesOfEmptySlotsInPassword[emptySlotPicker]] = passwordChar;
            IndexesOfEmptySlotsInPassword.RemoveAt(emptySlotPicker);
        }
        for (byte i = 0; i < MinimumSignOccurences; i++)
        {
            emptySlotPicker = (byte)random.Next(IndexesOfEmptySlotsInPassword.Count);
            passwordChar = signChars[random.Next(signChars.Length)];
            PasswordCharSlots[IndexesOfEmptySlotsInPassword[emptySlotPicker]] = passwordChar;
            IndexesOfEmptySlotsInPassword.RemoveAt(emptySlotPicker);
        }
        #endregion
        #region fill the rest of the empty slots in password
        byte charType;
        while (IndexesOfEmptySlotsInPassword.Count > 0)
        {
            charType = (byte)random.Next(4);
            emptySlotPicker = (byte)random.Next(IndexesOfEmptySlotsInPassword.Count);
            passwordChar = allowedAscii[charType][random.Next(allowedAscii[charType].Length)];
            PasswordCharSlots[IndexesOfEmptySlotsInPassword[emptySlotPicker]] = passwordChar;
            IndexesOfEmptySlotsInPassword.RemoveAt(emptySlotPicker);
        }
        #endregion
        return new string(PasswordCharSlots);
    }

    /// <summary>
    ///		<para>When searching for a string in SQL using LIKE comparison operator, this function replaces * with %, and ? with _</para>
    ///		<para>It also eliminates some of the dangerous chars used in SQL injection.</para>
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string? CleanupSqlSearchTextFilter(string? input)
    {
        if (string.IsNullOrEmpty(input)) return null;
        StringBuilder retValue = new StringBuilder(input);
        retValue.Replace("--", "%");
        retValue.Replace("*", "%");
        retValue.Replace("?", "_");
        retValue.Replace("'", "_");
        retValue.Replace("--", "");
        retValue.Replace(">", "_");
        retValue.Replace("<", "_");
        return retValue.ToString().Trim();
    }
    /// <summary>
    ///		<para>Inserts separators into a large string (usually with no breaks) to delimit chunks.</para>
    ///		<para>The large string becomes more easily readable or suitable for HTML.</para>
    /// </summary>
    /// <param name="largeString">The large string (usually with no breaks)</param>
    /// <param name="chunkLength">The lenght of the chunks to be delimited</param>
    /// <param name="largeString">The string used to separate chunks</param>
    /// <returns></returns>
    public static string? ChunkifyString(string? largeString, int chunkLength = 10, string chunkSeparator = " ")
    {
        if (string.IsNullOrEmpty(largeString)) return null;
        if (largeString.Length <= chunkLength) return largeString;
        StringBuilder stb = new StringBuilder();
        int currentChunkLength;
        for (int i = 0; i < largeString.Length; i += chunkLength)
        {
            currentChunkLength = Math.Min(chunkLength, largeString.Length - i);
            stb.Append(largeString.Substring(i, currentChunkLength));
            stb.Append(chunkSeparator);
        }
        return stb.ToString();
    }
}