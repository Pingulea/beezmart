﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.Utils;

public enum SearchOperator
{
    And = 0,
    Or = 1,
    Xor = 2
}
