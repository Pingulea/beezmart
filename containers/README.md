#   BeezMart as container app

BeezMart can run inside a container, like most of the Asp.Net apps. Find below instructions on how to prepare a container image with BeezMart.

##  1: Get source code

Clone the BeezMart repository to a location, on a local disk:

```PowerShell
cd "E:\BitBucket\";
git clone "https://Pingulea@bitbucket.org/Pingulea/beezmart.git";
```

##  2: Go to folder

Navigate with a PowerShell console to the location on the disk where the BeezMart repository was cloned:

```PowerShell
cd "E:\BitBucket\beezmart";
```

##  3: Create container image

Within the repository's location path, create the container image from the current folder `.`:

```PowerShell
docker build --tag "beezmart" --label "nanoserver:ltsc2022" --file ".\containers\Dockerfile.nanoserver" . ;
```

##  4: Test the image

To test the generated `beezmart` image:

### A. Prepare app settings

Create a file with the environment configuration settings that BeezMart expect. For a reference of these settings, see the [appSettings.json](../BeezMart/appSettings.json) file.
The file should resemble the [docker-env.txt](./docker-env.txt) offered as an example.
If the image is being built and tested on a development machine, then it is a good idea to place the `docker-env.txt` file right next to the [user secrets](https://learn.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-8.0&tabs=windows#how-the-secret-manager-tool-works) one:
+   On Windows: `$Env:AppData\Microsoft\UserSecrets\BeezMart\secrets.json`
+   On Linux: `~/.microsoft/usersecrets/BeezMart/secrets.json`

### B. Run the container

```PowerShell
#   Environment variables for the container that would be run:
#   C:\Users\<UserName>\AppData\Roaming\Microsoft\UserSecrets\BeezMart\docker-env.txt
$SetttingsFile = "$Env:AppData\Microsoft\UserSecrets\BeezMart\docker-env.txt";
docker run --interactive --tty --rm --publish 7000:8080 --name "beezmart_instance" --env-file $SetttingsFile beezmart;
```

Now try BeezMart running in a container at [http://localhost:7000](http://localhost:7000).

##  5. Optionally: upload image

As an illustration, the steps for uploading the BeezMart container image to an Azure Container Registry service:

1.  Create the Azure Container Registry resource; here, it will be named `nurbiosis`.
2.  Within the Container Registry resource, select the _Tokens_ blade.
3.  Add a token named `Pingulea`, and assign scope map `_repositories_push`.
4.  Generate a password for this token, and note it down.
5.  From the development machine, where the `beezmart` container image was built, login to the Container Registry and upload the image:

```PowerShell
docker login `
    --username "Pingulea" `
    --password "uTYU+bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla+KU2" `
    "nurbiosis.azurecr.io" `
    ;

docker tag "beezmart" nurbiosis.azurecr.io/beezmart;

docker push nurbiosis.azurecr.io/beezmart;
```

That's it. You may now navigate to the Container Registry, pick the _Repositories_ blade and find your `beezmart` repository with the `latest` container image there.