﻿using BeezMart.DAL;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL;

public static partial class AppSettings
{
	public static async Task<Dictionary<string, string>> ListAsync()
	{
		Dictionary<string, string> settings = new Dictionary<string, string>();
		string key, value;
		SqlDataReader rdrReader, row;
		using (Database db = new Database())
		{
			row = rdrReader = await db.GetSqlDataReaderAsync("App.Settings_Grid_1");
			while (await rdrReader.ReadAsync())
			{
				key = (string)row["Setting_Key"];
				value = (string)row["Setting_Value"];
				settings.Add(key, value);
			}
			rdrReader.Dispose();
		}
		return settings;
	}
	
	public static async Task SaveAsync(Dictionary<string, string> settings)
	{
		using (Database db = new Database())
		{
			db.AddParam("@SettingKey", SqlDbType.VarChar, 256);
			db.AddParam("@SettingValue", SqlDbType.NVarChar, 1024);
			db.Command.CommandText = "App.Settings_Upsert";
			await db.Connection.OpenAsync();
			foreach (KeyValuePair<string, string> setting in settings)
			{
				db.Params["@SettingKey"].Value = setting.Key;
				db.Params["@SettingValue"].Value = setting.Value;
				await db.Command.ExecuteNonQueryAsync();
			}
			db.Connection.Close();
		}
	}
}