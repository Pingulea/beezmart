﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class PersHugetexts
	{
		public static async Task<bool> SaveAsync(PersHugetextModel persHugetext)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (persHugetext.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = persHugetext.ID.Value;			// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persHugetext.FieldID;			// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NText).Value = persHugetext.Value;				// NVarChar(MAX)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persHugetext.UpdatedOn;	// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persHugetext.UpdatedBy;	// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_HugeText_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;	// BigInt OUTPUT
					db.AddParam("@PersonID", SqlDbType.Int).Value = persHugetext.PersonID;				// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persHugetext.FieldID;			// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NText).Value = persHugetext.Value;				// NVarChar(MAX)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persHugetext.UpdatedOn;	// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persHugetext.UpdatedBy;	// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_HugeText_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) persHugetext.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}

		public static async Task<PersHugetextModel?> RetrieveAsync(long persHugetextID)
		{
			PersHugetextModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = persHugetextID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_HugeText_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PersHugetextModel();
					entity.ID = (long)row["Record_ID"];																//			-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					entity.PersonID = (int)row["Person_ID"];														//			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.FieldID = (short)row["Field_ID"];														//			-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		//			-- NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														//			-- NVarChar(64) NOT NULL
					entity.Value = (string)row["Field_Value"];														//			-- NVarChar(MAX) NOT NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													//			-- SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			//			-- NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<PersHugetextModel>> ListAsync(int personID)
		{
			PersHugetextModel item;
			List<PersHugetextModel> list = new List<PersHugetextModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_HugeText_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PersHugetextModel();
					item.ID = (long)row["Record_ID"];															// BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					item.PersonID = (int)row["Person_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];	// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					item.Value = (string)row["Field_Value"];													// NVarChar(MAX) NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];												// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long persHugetextID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = persHugetextID;
				if (await db.DeleteRecordAsync("CRM.Pers_HugeText_Fields_Del") > 0) success = true;
			}
			return success;
		}

		/// <summary>
		///		<para>Gets the available categories of hugetext fields.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await PersFields.ListCategoriesAsync(FieldType.HugeText);
		}

		/// <summary>
		///		<para>Gets the available hugetext types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<PersFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await PersFields.ListAsync(FieldType.HugeText);
		}
	}
}
