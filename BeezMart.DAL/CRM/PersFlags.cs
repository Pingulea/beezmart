﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class PersFlags
	{
		public static async Task<bool> SaveAsync(PersFlagModel persFlag)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (persFlag.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = persFlag.ID.Value;					// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persFlag.FieldID;					// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = persFlag.Comments;			// NVarChar(256) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persFlag.UpdatedOn;			// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persFlag.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_Flag_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;		// BigInt OUTPUT
					db.AddParam("@PersonID", SqlDbType.Int).Value = persFlag.PersonID;						// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persFlag.FieldID;					// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = persFlag.Comments;			// NVarChar(256) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persFlag.UpdatedOn;			// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persFlag.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_Flag_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) persFlag.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}

		public static async Task<PersFlagModel?> RetrieveAsync(long persFlagID)
		{
			PersFlagModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = persFlagID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Flag_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PersFlagModel();
					entity.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					entity.PersonID = (int)row["Person_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					if (row["Field_Value"] != DBNull.Value) entity.Comments = (string)row["Field_Value"];			// NVarChar(256) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<PersFlagModel>> ListAsync(int personID)
		{
			PersFlagModel item;
			List<PersFlagModel> list = new List<PersFlagModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Flag_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PersFlagModel();
					item.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					item.PersonID = (int)row["Person_ID"];															// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.FieldID = (short)row["Field_ID"];															// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					if (row["Field_Value"] != DBNull.Value) item.Comments = (string)row["Field_Value"];				// NVarChar(256) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long persFlagID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = persFlagID;
				if ((await db.DeleteRecordAsync("CRM.Pers_Flag_Fields_Del")) > 0) success = true;
			}
			return success;
		}

		/// <summary>
		///		<para>Gets the available categories of flags.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await PersFields.ListCategoriesAsync(FieldType.Flag);
		}

		/// <summary>
		///		<para>Gets the available flag types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<PersFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await PersFields.ListAsync(FieldType.Flag);
		}
	}
}
