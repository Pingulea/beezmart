﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgActivities
	{
		public static class ActivityTypes
		{
			public static async Task<bool> SaveAsync(OrgActivityModel.ActivityTypeModel activityType)
			{
				bool success = false;
				using (Database db = new Database())
				{
					if (activityType.ID.HasValue)
					{
						db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activityType.ID.Value;				// SmallInt
						db.AddParam("@ActivityTypeName", SqlDbType.NVarChar, 64).Value = activityType.Name;				// NVarChar(64)
						db.AddParam("@ActivityTypeCategory", SqlDbType.NVarChar, 64).Value = activityType.Category;		// NVarChar(64) = NULL
						db.AddParam("@ActivityTypeComments", SqlDbType.NText).Value = activityType.Comments;			// NVarChar(MAX) = NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = activityType.IsDiscontinued;				// Bit NOT NULL = 0
						db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;						// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
						db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = activityType.UpdatedBy;				// NVarChar(128) = NULL
						if ((await db.SaveRecordAsync("CRM.Orgs_Activity_Types_Upd")) > 0) success = true;
					}
					else
					{
						db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;		// SmallInt OUTPUT
						db.AddParam("@ActivityTypeName", SqlDbType.NVarChar, 64).Value = activityType.Name;				// NVarChar(64)
						db.AddParam("@ActivityTypeCategory", SqlDbType.NVarChar, 64).Value = activityType.Category;		// NVarChar(64) = NULL
						db.AddParam("@ActivityTypeComments", SqlDbType.NText).Value = activityType.Comments;			// NVarChar(MAX) = NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = activityType.IsDiscontinued;				// Bit NOT NULL = 0
						db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;						// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
						db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = activityType.UpdatedBy;				// NVarChar(128) = NULL
						if ((await db.SaveRecordAsync("CRM.Orgs_Activity_Types_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
						if (success) activityType.ID = (short)db.Params["@ActivityTypeID"].Value;
					}
				}
				return success;
			}
			public static async Task<OrgActivityModel.ActivityTypeModel?> RetrieveAsync(short activityTypeID)
			{
				OrgActivityModel.ActivityTypeModel? entity = null;
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activityTypeID;
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Activity_Types_Sel");
					if (await rdrReader.ReadAsync())
					{
						entity = new OrgActivityModel.ActivityTypeModel();
						entity.ID = (short)row["Activity_Type_ID"];																		// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
						entity.Name = (string)row["Activity_Type_Name"];																// NVarChar(64) NOT NULL
						if (row["Activity_Type_Category"] != DBNull.Value) entity.Category = (string)row["Activity_Type_Category"];		// NVarChar(64) NULL
						if (row["Activity_Type_Comments"] != DBNull.Value) entity.Comments = (string)row["Activity_Type_Comments"];		// NVarChar(MAX) NULL
						entity.IsDiscontinued = (bool)row["Discontinued"];																// Bit NOT NULL DEFAULT 0
						entity.UpdatedOn = (DateTime)row["Updated_On"];																	// SmallDateTime NOT NULL DEFAULT GETDATE()
						if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];							// NVarChar(128) NULL
					}
					rdrReader.Dispose();
				}
				return entity;
			}
			/// <summary>
			///		<para>Gets the list of available activity categories.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<string>> ListCategoriesAsync()
			{
				List<string> list = new List<string>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Activity_Types_Grid_3");
					while (await rdrReader.ReadAsync())
					{
						if (row[0] != DBNull.Value) list.Add((string)row[0]);			// NVarChar(64) NULL
					}
					rdrReader.Dispose();
				}
				return list;
			}
			/// <summary>
			///		<para>Gets the list of available activity types.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<OrgActivityModel.ActivityTypeModel>> ListAsync()
			{
				OrgActivityModel.ActivityTypeModel item;
				List<OrgActivityModel.ActivityTypeModel> list = new List<OrgActivityModel.ActivityTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Activity_Types_Grid_2");
					while (await rdrReader.ReadAsync())
					{
						item = new OrgActivityModel.ActivityTypeModel();
						item.ID = (short)row["Activity_Type_ID"];																		// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
						item.Name = (string)row["Activity_Type_Name"];																	// NVarChar(64) NOT NULL
						if (row["Activity_Type_Category"] != DBNull.Value) item.Category = (string)row["Activity_Type_Category"];		// NVarChar(64) NULL
						if (row["Activity_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Activity_Type_Comments"];		// NVarChar(MAX) NULL
						item.IsDiscontinued = (bool)row["Discontinued"];																// Bit NOT NULL DEFAULT 0
						item.UpdatedOn = (DateTime)row["Updated_On"];																	// SmallDateTime NOT NULL DEFAULT GETDATE()
						if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];								// NVarChar(128) NULL
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}
			/// <summary>
			///		<para>Gets the list of all activity types, including the retired ones.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<OrgActivityModel.ActivityTypeModel>> ListAllAsync()
			{
				OrgActivityModel.ActivityTypeModel item;
				List<OrgActivityModel.ActivityTypeModel> list = new List<OrgActivityModel.ActivityTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Activity_Types_Grid_1");
					while (await rdrReader.ReadAsync())
					{
						item = new OrgActivityModel.ActivityTypeModel();
						item.ID = (short)row["Activity_Type_ID"];																		// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
						item.Name = (string)row["Activity_Type_Name"];																	// NVarChar(64) NOT NULL
						if (row["Activity_Type_Category"] != DBNull.Value) item.Category = (string)row["Activity_Type_Category"];		// NVarChar(64) NULL
						if (row["Activity_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Activity_Type_Comments"];		// NVarChar(MAX) NULL
						item.IsDiscontinued = (bool)row["Discontinued"];																// Bit NOT NULL DEFAULT 0
						item.UpdatedOn = (DateTime)row["Updated_On"];																	// SmallDateTime NOT NULL DEFAULT GETDATE()
						if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];								// NVarChar(128) NULL
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}
			public static async Task<bool> DeleteAsync(short activityTypeID, bool isDiscontinued = true, string? updatedBy = null, DateTime? updatedOn = null)
			{
				bool success = false;
				using (Database db = new Database())
				{
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activityTypeID;		// SmallInt
					db.AddParam("@Discontinued", SqlDbType.Bit).Value = isDiscontinued;				// Bit NOT NULL = 1
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;			// SmallDateTime DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;			// NVarChar(128) = NULL
					if ((await db.DeleteRecordAsync("CRM.Orgs_Activity_Types_Del")) > 0) success = true;
				}
				return success;
			}
		}

		public static async Task<bool> SaveAsync(OrgActivityModel orgActivity)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (orgActivity.ID.HasValue)
				{
					db.AddParam("@ActivityID", SqlDbType.BigInt).Value = orgActivity.ID.Value;								// BigInt
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgActivity.OrganizationID;						// Int
					db.AddParam("@ActivityStatusID", SqlDbType.TinyInt).Value = (byte)orgActivity.Status;					// TinyInt = 10
					db.AddParam("@ActivityStatusComment", SqlDbType.NVarChar, 256).Value = orgActivity.StatusComment;		// NVarChar(256) = NULL
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = orgActivity.TypeID;							// SmallInt
					db.AddParam("@Title", SqlDbType.NVarChar, 1024).Value = orgActivity.Title;								// NVarChar(1024) NOT NULL
					db.AddParam("@Details", SqlDbType.NText).Value = orgActivity.Details;									// NVarChar(MAX) = NULL
					db.AddParam("@ScheduledFor", SqlDbType.SmallDateTime).Value = orgActivity.ScheduledFor;					// SmallDateTime = NULL
					db.AddParam("@ReminderOn", SqlDbType.SmallDateTime).Value = orgActivity.ReminderOn;						// SmallDateTime = NULL
					db.AddParam("@StartedOn", SqlDbType.SmallDateTime).Value = orgActivity.StartedOn;						// SmallDateTime = NULL
					db.AddParam("@CompletedOn", SqlDbType.SmallDateTime).Value = orgActivity.CompletedOn;					// SmallDateTime = NULL
					db.AddParam("@RequestedBy", SqlDbType.NVarChar, 128).Value = orgActivity.RequestedBy;					// NVarChar(128) = NULL
					db.AddParam("@CompletedBy", SqlDbType.NVarChar, 128).Value = orgActivity.CompletedBy;					// NVarChar(128) = NULL
					db.AddParam("@Billable", SqlDbType.Bit).Value = orgActivity.IsBillable;													// Bit = 0
					db.AddParam("@InvoiceIDs", SqlDbType.VarChar, 256).Value = orgActivity.InvoiceIDs;						// VarChar(256) = NULL
					db.AddParam("@DurationInMinutes", SqlDbType.Int).Value = orgActivity.DurationInMinutes;					// Int = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;								// SmallDateTime DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgActivity.UpdatedBy;						// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Activities_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@ActivityID", SqlDbType.BigInt).Direction = ParameterDirection.Output;						// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgActivity.OrganizationID;						// Int
					db.AddParam("@ActivityStatusID", SqlDbType.TinyInt).Value = (byte)orgActivity.Status;					// TinyInt = 10
					db.AddParam("@ActivityStatusComment", SqlDbType.NVarChar, 256).Value = orgActivity.StatusComment;		// NVarChar(256) = NULL
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = orgActivity.TypeID;							// SmallInt
					db.AddParam("@Title", SqlDbType.NVarChar, 1024).Value = orgActivity.Title;								// NVarChar(1024) NOT NULL
					db.AddParam("@Details", SqlDbType.NText).Value = orgActivity.Details;									// NVarChar(MAX) = NULL
					db.AddParam("@ScheduledFor", SqlDbType.SmallDateTime).Value = orgActivity.ScheduledFor;					// SmallDateTime = NULL
					db.AddParam("@ReminderOn", SqlDbType.SmallDateTime).Value = orgActivity.ReminderOn;						// SmallDateTime = NULL
					db.AddParam("@StartedOn", SqlDbType.SmallDateTime).Value = orgActivity.StartedOn;						// SmallDateTime = NULL
					db.AddParam("@CompletedOn", SqlDbType.SmallDateTime).Value = orgActivity.CompletedOn;					// SmallDateTime = NULL
					db.AddParam("@RequestedBy", SqlDbType.NVarChar, 128).Value = orgActivity.RequestedBy;					// NVarChar(128) = NULL
					db.AddParam("@CompletedBy", SqlDbType.NVarChar, 128).Value = orgActivity.CompletedBy;					// NVarChar(128) = NULL
					db.AddParam("@Billable", SqlDbType.Bit).Value = orgActivity.IsBillable;													// Bit = 0
					db.AddParam("@InvoiceIDs", SqlDbType.VarChar, 256).Value = orgActivity.InvoiceIDs;						// VarChar(256) = NULL
					db.AddParam("@DurationInMinutes", SqlDbType.Int).Value = orgActivity.DurationInMinutes;					// Int = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;								// SmallDateTime DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgActivity.UpdatedBy;						// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Activities_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) orgActivity.ID = (long)db.Params["@ActivityID"].Value;
				}
			}
			return success;
		}
		public static async Task<OrgActivityModel?> RetrieveAsync(long activityID)
		{
			OrgActivityModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@ActivityID", SqlDbType.BigInt).Value = activityID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Activities_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgActivityModel();
					entity.ID = (long)row["Activity_ID"];																					// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					entity.OrganizationID = (int)row["Organization_ID"];																	// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					entity.Status = (ActivityStatus)(byte)row["Activity_Status_ID"];														// TinyInt NOT NULL FOREIGN KEY REFERENCES CRM.App_Activity_Status(Activity_Status_ID)
					if (row["Activity_Status_Comment"] != DBNull.Value) entity.StatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					entity.TypeID = (short)row["Activity_Type_ID"];																			// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Activity_Types(Activity_Type_ID)
					entity.Title = (string)row["Title"];																					// NVarChar(1024) NOT NULL
					if (row["Details"] != DBNull.Value) entity.Details = (string)row["Details"];											// NVarChar(MAX) NULL
					if (row["Scheduled_For"] != DBNull.Value) entity.ScheduledFor = (DateTime)row["Scheduled_For"];							// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) entity.ReminderOn = (DateTime)row["Reminder_On"];								// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) entity.StartedOn = (DateTime)row["Started_On"];									// SmallDateTime NULL
					if (row["Completed_On"] != DBNull.Value) entity.CompletedOn = (DateTime)row["Completed_On"];							// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) entity.RequestedBy = (string)row["Requested_By"];								// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) entity.CompletedBy = (string)row["Completed_By"];								// NVarChar(128) NULL
					entity.IsBillable = (bool)row["Billable"];																				// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) entity.InvoiceIDs = (string)row["Invoice_IDs"];									// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) entity.DurationInMinutes = (int)row["Duration_In_Minutes"];				// Int NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																			// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];									// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<List<OrgActivityModel>> ListAsync(int organizationID)
		{
			OrgActivityModel item;
			List<OrgActivityModel> list = new List<OrgActivityModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Activities_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgActivityModel();
					item.ID = (long)row["Activity_ID"];																						// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					item.OrganizationID = (int)row["Organization_ID"];																		// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.Status = (ActivityStatus)(byte)row["Activity_Status_ID"];															// TinyInt NOT NULL FOREIGN KEY REFERENCES CRM.App_Activity_Status(Activity_Status_ID)
					if (row["Activity_Status_Comment"] != DBNull.Value) item.StatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					item.TypeID = (short)row["Activity_Type_ID"];																			// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Activity_Types(Activity_Type_ID)
					item.Title = (string)row["Title"];																						// NVarChar(1024) NOT NULL
					if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];												// NVarChar(MAX) NULL
					if (row["Scheduled_For"] != DBNull.Value) item.ScheduledFor = (DateTime)row["Scheduled_For"];							// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) item.ReminderOn = (DateTime)row["Reminder_On"];									// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];									// SmallDateTime NULL
					if (row["Completed_On"] != DBNull.Value) item.CompletedOn = (DateTime)row["Completed_On"];								// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) item.RequestedBy = (string)row["Requested_By"];								// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) item.CompletedBy = (string)row["Completed_By"];								// NVarChar(128) NULL
					item.IsBillable = (bool)row["Billable"];																				// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) item.InvoiceIDs = (string)row["Invoice_IDs"];									// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) item.DurationInMinutes = (int)row["Duration_In_Minutes"];				// Int NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];																			// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];										// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(long activityID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@ActivityID", SqlDbType.BigInt).Value = activityID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_Activities_Del")) > 0) success = true;
			}
			return success;
		}
	}
}
