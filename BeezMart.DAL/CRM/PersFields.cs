﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class PersFields
	{
		public static async Task<bool> SaveAsync(PersFieldModel persField)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (persField.ID.HasValue)
				{
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persField.ID.Value;					// SmallInt
					db.AddParam("@FieldCategory", SqlDbType.NVarChar, 64).Value = persField.Category;		// NVarChar(64) = NULL
					db.AddParam("@FieldName", SqlDbType.NVarChar, 64).Value = persField.Name;				// NVarChar(64)
					db.AddParam("@FieldComments", SqlDbType.NText).Value = persField.Comments;				// NVarChar(MAX) = NULL
					db.AddParam("@IsUnique", SqlDbType.Bit).Value = persField.IsUnique;						// Bit = 0
					db.AddParam("@IsRetired", SqlDbType.Bit).Value = persField.IsRetired;					// Bit = 0
					db.AddParam("@FieldTypeID", SqlDbType.TinyInt).Value = (byte)persField.FieldType;		// TinyInt
					if ((await db.SaveRecordAsync("CRM.Pers_Field_Names_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@FieldID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;		// SmallInt OUTPUT
					db.AddParam("@FieldCategory", SqlDbType.NVarChar, 64).Value = persField.Category;		// NVarChar(64) = NULL
					db.AddParam("@FieldName", SqlDbType.NVarChar, 64).Value = persField.Name;				// NVarChar(64)
					db.AddParam("@FieldComments", SqlDbType.NText).Value = persField.Comments;				// NVarChar(MAX) = NULL
					db.AddParam("@IsUnique", SqlDbType.Bit).Value = persField.IsUnique;						// Bit = 0
					db.AddParam("@IsRetired", SqlDbType.Bit).Value = persField.IsRetired;					// Bit = 0
					db.AddParam("@FieldTypeID", SqlDbType.TinyInt).Value = (byte)persField.FieldType;		// TinyInt
					if ((await db.SaveRecordAsync("CRM.Pers_Field_Names_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) persField.ID = (short)db.Params["@FieldID"].Value;
				}
			}
			return success;
		}

		public static async Task<PersFieldModel?> RetrieveAsync(short persFieldID)
		{
			PersFieldModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persFieldID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Field_Names_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PersFieldModel();
					entity.ID = (short)row["Field_ID"];																// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					if (row["Field_Comments"] != DBNull.Value) entity.Comments = (string)row["Field_Comments"];		// NVarChar(MAX) NULL
					entity.IsUnique = (bool)row["Is_Unique"];														// Bit NOT NULL DEFAULT 0
					entity.IsRetired = (bool)row["Is_Retired"];														// Bit NOT NULL DEFAULT 0
					entity.FieldType = (FieldType)(byte)row["Field_Type_ID"];										// TinyInt NOT NULL FOREIGN KEY REFERENCES App_Field_Types(Field_Type_ID)
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		/// <summary>
		///		<para>Gets only the available field names (so excluding the retired ones), optionally filtered by field type.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <param name="typeOfField">Specifies if fields should be filtered by some type</param>
		/// <returns></returns>
		public static async Task<List<PersFieldModel>> ListAsync(FieldType typeOfField)
		{
			PersFieldModel item;
			List<PersFieldModel> list = new List<PersFieldModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@FieldTypeID", SqlDbType.TinyInt).Value = typeOfField;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Field_Names_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new PersFieldModel();
					item.ID = (short)row["Field_ID"];																// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					if (row["Field_Comments"] != DBNull.Value) item.Comments = (string)row["Field_Comments"];		// NVarChar(MAX) NULL
					item.IsUnique = (bool)row["Is_Unique"];															// Bit NOT NULL DEFAULT 0
					item.IsRetired = (bool)row["Is_Retired"];														// Bit NOT NULL DEFAULT 0
					item.FieldType = (FieldType)(byte)row["Field_Type_ID"];											// TinyInt NOT NULL FOREIGN KEY REFERENCES App_Field_Types(Field_Type_ID)
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		/// <summary>
		///		<para>Gets all the field names, no mater if retired or not, optionally filtered by field type.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <param name="typeOfField">Specifies if fields should be filtered by some type</param>
		/// <returns></returns>
		public static async Task<List<PersFieldModel>> ListAllAsync(FieldType? typeOfField = null)
		{
			PersFieldModel item;
			List<PersFieldModel> list = new List<PersFieldModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@FieldTypeID", SqlDbType.TinyInt).Value = typeOfField;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Field_Names_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PersFieldModel();
					item.ID = (short)row["Field_ID"];																// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					if (row["Field_Comments"] != DBNull.Value) item.Comments = (string)row["Field_Comments"];		// NVarChar(MAX) NULL
					item.IsUnique = (bool)row["Is_Unique"];															// Bit NOT NULL DEFAULT 0
					item.IsRetired = (bool)row["Is_Retired"];														// Bit NOT NULL DEFAULT 0
					item.FieldType = (FieldType)(byte)row["Field_Type_ID"];											// TinyInt NOT NULL FOREIGN KEY REFERENCES App_Field_Types(Field_Type_ID)
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		/// <summary>
		///		<para>Lists only available categories for a specific field type, excluding retired ones.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <param name="typeOfField"></param>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync(FieldType? typeOfField)
		{
			List<string> list = new List<string>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@FieldTypeID", SqlDbType.TinyInt).Value = typeOfField;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Field_Names_Grid_3");
				while (await rdrReader.ReadAsync())
				{
					if (row[0] != DBNull.Value) list.Add((string)row[0]);
				}
				rdrReader.Dispose();
			}
			return list;
		}
	}
}
