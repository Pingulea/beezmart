﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
    public static class RelationsOrgsPers
    {
		public static class RelationTypes
		{
			public static async Task<bool> SaveAsync(RelationOrgsPersModel.RelationTypeModel relationType)
			{
				bool success = false;
				using (Database db = new Database())
				{
					if (relationType.ID.HasValue)
					{
						db.AddParam("@JobTypeID", SqlDbType.SmallInt).Value = relationType.ID.Value;			// SmallInt
						db.AddParam("@JobTypeCategory", SqlDbType.NVarChar, 64).Value = relationType.Category;	// NVarChar(64) = NULL
						db.AddParam("@JobTypeName", SqlDbType.NVarChar, 64).Value = relationType.Name;			// NVarChar(64)
						db.AddParam("@JobTypeComments", SqlDbType.NText).Value = relationType.Comments;			// NVarChar(MAX) = NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = relationType.Discontinued;				// Bit = 0
						if ((await db.SaveRecordAsync("CRM.Relation_Orgs_Pers_Job_Types_Upd_1")) > 0) success = true;
					}
					else
					{
						db.AddParam("@JobTypeID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;	// SmallInt OUTPUT
						db.AddParam("@JobTypeCategory", SqlDbType.NVarChar, 64).Value = relationType.Category;	// NVarChar(64) = NULL
						db.AddParam("@JobTypeName", SqlDbType.NVarChar, 64).Value = relationType.Name;			// NVarChar(64)
						db.AddParam("@JobTypeComments", SqlDbType.NText).Value = relationType.Comments;			// NVarChar(MAX) = NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = relationType.Discontinued;				// Bit = 0
						if ((await db.SaveRecordAsync("CRM.Relation_Orgs_Pers_Job_Types_Ins_1", clearParamsAfterCompletion: false)) > 0) success = true;
						if (success) relationType.ID = (short)db.Params["@JobTypeID"].Value;
					}
				}
				return success;
			}

			public static async Task<RelationOrgsPersModel.RelationTypeModel?> RetrieveAsync(short relationTypeID)
			{
				RelationOrgsPersModel.RelationTypeModel? entity = null;
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					db.AddParam("@JobTypeID", SqlDbType.SmallInt).Value = relationTypeID;
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Job_Types_Sel_1");
					if (await rdrReader.ReadAsync())
					{
						entity = new RelationOrgsPersModel.RelationTypeModel();
						entity.ID = (short)row["Job_Type_ID"];																// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
						if (row["Job_Type_Category"] != DBNull.Value) entity.Category = (string)row["Job_Type_Category"];	// NVarChar(64) NULL
						entity.Name = (string)row["Job_Type_Name"];															// NVarChar(64) NOT NULL
						if (row["Job_Type_Comments"] != DBNull.Value) entity.Comments = (string)row["Job_Type_Comments"];	// NVarChar(MAX) NULL
						entity.Discontinued = (bool)row["Discontinued"];														// Bit NOT NULL DEFAULT 0
					}
					rdrReader.Dispose();
				}
				return entity;
			}

			/// <summary>
			///		<para>Gets the list of available relation (or job) categories.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<string>> ListCategoriesAsync()
			{
				List<string> list = new List<string>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Job_Types_Grid_1");
					while (await rdrReader.ReadAsync())
					{
						if(row[0] != DBNull.Value) list.Add((string)row[0]);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			/// <summary>
			///		<para>Gets the list of available relation types (or job types), excluding retired/discontinued ones.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<RelationOrgsPersModel.RelationTypeModel>> ListAsync()
			{
				RelationOrgsPersModel.RelationTypeModel item;
				List<RelationOrgsPersModel.RelationTypeModel> list = new List<RelationOrgsPersModel.RelationTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Job_Types_Grid_3");
					while (await rdrReader.ReadAsync())
					{
						item = new RelationOrgsPersModel.RelationTypeModel();
						item.ID = (short)row["Job_Type_ID"];																	// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
						if (row["Job_Type_Category"] != DBNull.Value) item.Category = (string)row["Job_Type_Category"];			// NVarChar(64) NULL
						item.Name = (string)row["Job_Type_Name"];																// NVarChar(64) NOT NULL
						if (row["Job_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Job_Type_Comments"];			// NVarChar(MAX) NULL
						item.Discontinued = (bool)row["Discontinued"];															// Bit NOT NULL DEFAULT 0
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			/// <summary>
			///		<para>Gets the list of available relation types (or job types), filtered by category, excluding retired/discontinued ones.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <param name="category">If null or empty, job types with no category will be retrieved</param>
			/// <returns></returns>
			public static async Task<List<RelationOrgsPersModel.RelationTypeModel>> ListAsync(string category)
			{
				RelationOrgsPersModel.RelationTypeModel item;
				List<RelationOrgsPersModel.RelationTypeModel> list = new List<RelationOrgsPersModel.RelationTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					db.AddParam("@JobTypeCategory", SqlDbType.NVarChar, 64).Value = category;
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Job_Types_Grid_2");
					while (await rdrReader.ReadAsync())
					{
						item = new RelationOrgsPersModel.RelationTypeModel();
						item.ID = (short)row["Job_Type_ID"];																// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
						if (row["Job_Type_Category"] != DBNull.Value) item.Category = (string)row["Job_Type_Category"];		// NVarChar(64) NULL
						item.Name = (string)row["Job_Type_Name"];															// NVarChar(64) NOT NULL
						if (row["Job_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Job_Type_Comments"];		// NVarChar(MAX) NULL
						item.Discontinued = (bool)row["Discontinued"];														// Bit NOT NULL DEFAULT 0
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			/// <summary>
			///		<para>Gets all relation types (or job types), including the retired/discontinued ones.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<RelationOrgsPersModel.RelationTypeModel>> ListAllAsync()
			{
				RelationOrgsPersModel.RelationTypeModel item;
				List<RelationOrgsPersModel.RelationTypeModel> list = new List<RelationOrgsPersModel.RelationTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Job_Types_Grid_4");
					while (await rdrReader.ReadAsync())
					{
						item = new RelationOrgsPersModel.RelationTypeModel();
						item.ID = (short)row["Job_Type_ID"];																// SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
						if (row["Job_Type_Category"] != DBNull.Value) item.Category = (string)row["Job_Type_Category"];		// NVarChar(64) NULL
						item.Name = (string)row["Job_Type_Name"];															// NVarChar(64) NOT NULL
						if (row["Job_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Job_Type_Comments"];		// NVarChar(MAX) NULL
						item.Discontinued = (bool)row["Discontinued"];														// Bit NOT NULL DEFAULT 0
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			public static async Task<bool> DeleteAsync(short relationTypeID, bool isDiscontinued = true, string? updatedBy = null, DateTime? updatedOn = null)
			{
				bool success = false;
				using (Database db = new Database())
				{
					db.AddParam("@JobTypeID", SqlDbType.BigInt).Value = relationTypeID;
					db.AddParam("@Discontinued", SqlDbType.Bit).Value = isDiscontinued;
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
					if ((await db.DeleteRecordAsync("CRM.Relation_Orgs_Pers_Job_Types_Del_1")) > 0) success = true;
				}
				return success;
			}
		}
		public static async Task<bool> SaveAsync(RelationOrgsPersModel relation)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (relation.ID.HasValue)
				{
					db.AddParam("@RelationID", SqlDbType.BigInt).Value = relation.ID.Value;					// BigInt
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = relation.OrganizationID;			// Int
					db.AddParam("@PersonID", SqlDbType.Int).Value = relation.PersonID;						// Int
					db.AddParam("@PositionTitle", SqlDbType.NVarChar, 256).Value = relation.PositionTitle;	// NVarChar(256) = NULL
					db.AddParam("@JobTypeID", SqlDbType.SmallInt).Value = relation.JobTypeID;				// SmallInt = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = relation.UpdatedOn;			// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = relation.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Relation_Orgs_Pers_Upd_1")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RelationID", SqlDbType.BigInt).Direction = ParameterDirection.Output;		// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = relation.OrganizationID;			// Int
					db.AddParam("@PersonID", SqlDbType.Int).Value = relation.PersonID;						// Int
					db.AddParam("@PositionTitle", SqlDbType.NVarChar, 256).Value = relation.PositionTitle;	// NVarChar(256) = NULL
					db.AddParam("@JobTypeID", SqlDbType.SmallInt).Value = relation.JobTypeID;				// SmallInt = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = relation.UpdatedOn;			// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = relation.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Relation_Orgs_Pers_Ins_1", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) relation.ID = (long)db.Params["@RelationID"].Value;
				}
			}
			return success;
		}

		public static async Task<RelationOrgsPersModel?> RetrieveAsync(long relationOrgsPersID)
		{
			RelationOrgsPersModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RelationID", SqlDbType.BigInt).Value = relationOrgsPersID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Sel_1");
				if (await rdrReader.ReadAsync())
				{
					entity = new RelationOrgsPersModel();
					entity.ID = (long)row["Relation_ID"];																		// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					entity.OrganizationID = (int)row["Organization_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.PersonID = (int)row["Person_ID"];																	// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Position_Title"] != DBNull.Value) entity.PositionTitle = (string)row["Position_Title"];			// NVarChar(256) NULL
					if (row["Job_Type_ID"] != DBNull.Value) entity.JobTypeID = (short)row["Job_Type_ID"];						// SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
					if (row["Job_Type_Name"] != DBNull.Value) entity.JobTypeName = (string)row["Job_Type_Name"];				// NVarChar(64) NULL
					if (row["Job_Type_Category"] != DBNull.Value) entity.JobTypeCategory = (string)row["Job_Type_Category"];	// NVarChar(64) NULL
					entity.OrgName = (string)row["Org_Name"];																	// NVarChar(256) NOT NULL
					if (row["Name_Prefix"] != DBNull.Value) entity.OrgPrefix = (string)row["Org_Name_Prefix"];					// NVarChar(16) NULL
					if (row["Name_Suffix"] != DBNull.Value) entity.OrgSuffix = (string)row["Org_Name_Suffix"];					// NVarChar(16) NULL
					if (row["Alternate_Names"] != DBNull.Value) entity.OrgAltnames = (string)row["Alternate_Names"];			// NVarChar(1024) NULL
					entity.OrgSegmentID = (byte)row["Org_Segment_ID"];															// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					entity.OrgSegment = (string)row["Org_Segment"];																// NVarChar(256) NOT NULL
					entity.PersFirstName = (string)row["First_Name"];															// NVarChar(64) NOT NULL
					entity.PersLastName = (string)row["Last_Name"];																// NVarChar(64) NOT NULL
					if (row["Name_Prefix"] != DBNull.Value) entity.PersPrefix = (string)row["Pers_Name_Prefix"];				// NVarChar(16) NULL
					if (row["Name_Suffix"] != DBNull.Value) entity.PersSuffix = (string)row["Pers_Name_Suffix"];				// NVarChar(16) NULL
					if (row["Nicknames"] != DBNull.Value) entity.PersNicknames = (string)row["Nicknames"];						// NVarChar(256) NULL
					entity.PersSegmentID = (byte)row["Pers_Segment_ID"];														// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					entity.PersSegment = (string)row["Pers_Segment"];															// NVarChar(256) NOT NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<RelationOrgsPersModel>> ListPersonsAsync(int organizationID)
		{
			RelationOrgsPersModel item;
			List<RelationOrgsPersModel> list = new List<RelationOrgsPersModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new RelationOrgsPersModel();
					item.ID = (long)row["Relation_ID"];																		// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					item.OrganizationID = organizationID;
					item.PersonID = (int)row["Person_ID"];																	// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Position_Title"] != DBNull.Value) item.PositionTitle = (string)row["Position_Title"];			// NVarChar(256) NULL
					if (row["Job_Type_ID"] != DBNull.Value) item.JobTypeID = (short)row["Job_Type_ID"];						// SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
					if (row["Job_Type_Name"] != DBNull.Value) item.JobTypeName = (string)row["Job_Type_Name"];				// NVarChar(64) NULL
					if (row["Job_Type_Category"] != DBNull.Value) item.JobTypeCategory = (string)row["Job_Type_Category"];	// NVarChar(64) NULL
					item.PersFirstName = (string)row["First_Name"];															// NVarChar(64) NOT NULL
					item.PersLastName = (string)row["Last_Name"];															// NVarChar(64) NOT NULL
					if (row["Name_Prefix"] != DBNull.Value) item.PersPrefix = (string)row["Name_Prefix"];					// NVarChar(16) NULL
					if (row["Name_Suffix"] != DBNull.Value) item.PersSuffix = (string)row["Name_Suffix"];					// NVarChar(16) NULL
					if (row["Nicknames"] != DBNull.Value) item.PersNicknames = (string)row["Nicknames"];					// NVarChar(256) NULL
					item.PersSegmentID = (byte)row["Segment_ID"];															// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					item.PersSegment = (string)row["Segment_Name"];															// NVarChar(256) NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<List<RelationOrgsPersModel>> ListOrganizationsAsync(int personID)
		{
			RelationOrgsPersModel item;
			List<RelationOrgsPersModel> list = new List<RelationOrgsPersModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Relation_Orgs_Pers_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new RelationOrgsPersModel();
					item.ID = (long)row["Relation_ID"];																		// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					item.OrganizationID = (int)row["Organization_ID"];
					item.PersonID = personID;																				// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Position_Title"] != DBNull.Value) item.PositionTitle = (string)row["Position_Title"];			// NVarChar(256) NULL
					if (row["Job_Type_ID"] != DBNull.Value) item.JobTypeID = (short)row["Job_Type_ID"];						// SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
					if (row["Job_Type_Name"] != DBNull.Value) item.JobTypeName = (string)row["Job_Type_Name"];				// NVarChar(64) NULL
					if (row["Job_Type_Category"] != DBNull.Value) item.JobTypeCategory = (string)row["Job_Type_Category"];	// NVarChar(64) NULL
					item.OrgName = (string)row["Org_Name"];																	// NVarChar(256) NOT NULL
					if (row["Name_Prefix"] != DBNull.Value) item.OrgPrefix = (string)row["Name_Prefix"];					// NVarChar(16) NULL
					if (row["Name_Suffix"] != DBNull.Value) item.OrgSuffix = (string)row["Name_Suffix"];					// NVarChar(16) NULL
					if (row["Alternate_Names"] != DBNull.Value) item.OrgAltnames = (string)row["Alternate_Names"];			// NVarChar(1024) NULL
					item.OrgSegmentID = (byte)row["Segment_ID"];															// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					item.OrgSegment = (string)row["Segment_Name"];															// NVarChar(256) NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long relationOrgsPersID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RelationID", SqlDbType.BigInt).Value = relationOrgsPersID;
				if ((await db.DeleteRecordAsync("CRM.Relation_Orgs_Pers_Del_1")) > 0) success = true;
			}
			return success;
		}
	}
}
