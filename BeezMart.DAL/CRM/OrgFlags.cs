﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgFlags
    {
		public static async Task<bool> SaveAsync(OrgFlagModel orgFlag)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (orgFlag.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgFlag.ID.Value;					// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgFlag.FieldID;					// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = orgFlag.Comments;			// NVarChar(256) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgFlag.UpdatedOn;			// SmallDateTime NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgFlag.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Flag_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;		// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgFlag.OrganizationID;			// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgFlag.FieldID;					// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = orgFlag.Comments;			// NVarChar(256) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgFlag.UpdatedOn;			// SmallDateTime NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgFlag.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Flag_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) orgFlag.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}
		public static async Task<OrgFlagModel?> RetrieveAsync(long orgFlagID)
		{
			OrgFlagModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgFlagID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Flag_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgFlagModel();
					entity.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					entity.OrganizationID = (int)row["Organization_ID"];											// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					entity.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					if (row["Field_Value"] != DBNull.Value) entity.Comments = (string)row["Field_Value"];			// NVarChar(256) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<List<OrgFlagModel>> ListAsync(int organizationID)
		{
			OrgFlagModel item;
			List<OrgFlagModel> list = new List<OrgFlagModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Flag_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgFlagModel();
					item.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					item.OrganizationID = (int)row["Organization_ID"];												// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.FieldID = (short)row["Field_ID"];															// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					if (row["Field_Value"] != DBNull.Value) item.Comments = (string)row["Field_Value"];				// NVarChar(256) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(long orgFlagID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgFlagID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_Flag_Fields_Del")) > 0) success = true;
			}
			return success;
		}
		/// <summary>
		///		<para>Gets the available categories of flags.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await OrgFields.ListCategoriesAsync(FieldType.Flag);
		}
		/// <summary>
		///		<para>Gets the available flag types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<OrgFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await OrgFields.ListAsync(FieldType.Flag);
		}
	}
}
