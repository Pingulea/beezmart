﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgSites
    {
		public static async Task<bool> SaveAsync(OrgSiteModel orgSite)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (orgSite.ID.HasValue)
				{
					db.AddParam("@SiteID", SqlDbType.Int).Value = orgSite.ID.Value;								// Int
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgSite.OrganizationID;				// Int
					db.AddParam("@ContactStatusID", SqlDbType.TinyInt).Value = (byte)orgSite.ContactStatus;		// TinyInt = 0
					db.AddParam("@SiteName", SqlDbType.NVarChar, 128).Value = orgSite.SiteName;					// NVarChar(128) = NULL
					db.AddParam("@Country", SqlDbType.Char, 3).Value = orgSite.Country;							// Char(3) = 'ROM'
					db.AddParam("@Region", SqlDbType.NVarChar, 128).Value = orgSite.Region;						// NVarChar(128) = NULL
					db.AddParam("@StateProvince", SqlDbType.NVarChar, 128).Value = orgSite.StateProvince;		// NVarChar(128) = NULL
					db.AddParam("@City", SqlDbType.NVarChar, 64).Value = orgSite.City;							// NVarChar(64)
					db.AddParam("@StreetAddress1", SqlDbType.NVarChar, 128).Value = orgSite.StreetAddress1;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress2", SqlDbType.NVarChar, 128).Value = orgSite.StreetAddress2;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress3", SqlDbType.NVarChar, 128).Value = orgSite.StreetAddress3;		// NVarChar(128) = NULL
					db.AddParam("@Department", SqlDbType.NVarChar, 128).Value = orgSite.Department;				// NVarChar(128) = NULL
					db.AddParam("@Directions", SqlDbType.NVarChar, 128).Value = orgSite.Directions;				// NVarChar(128) = NULL
					db.AddParam("@ZIPPostCode", SqlDbType.VarChar, 32).Value = orgSite.ZipPostCode;				// VarChar(32) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgSite.UpdatedOn;				// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgSite.UpdatedBy;				// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Sites_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@SiteID", SqlDbType.Int).Direction = ParameterDirection.Output;				// Int OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgSite.OrganizationID;				// Int
					db.AddParam("@ContactStatusID", SqlDbType.TinyInt).Value = (byte)orgSite.ContactStatus;		// TinyInt = 0
					db.AddParam("@SiteName", SqlDbType.NVarChar, 128).Value = orgSite.SiteName;					// NVarChar(128) = NULL
					db.AddParam("@Country", SqlDbType.Char, 3).Value = orgSite.Country;							// Char(3) = 'ROM'
					db.AddParam("@Region", SqlDbType.NVarChar, 128).Value = orgSite.Region;						// NVarChar(128) = NULL
					db.AddParam("@StateProvince", SqlDbType.NVarChar, 128).Value = orgSite.StateProvince;		// NVarChar(128) = NULL
					db.AddParam("@City", SqlDbType.NVarChar, 64).Value = orgSite.City;							// NVarChar(64)
					db.AddParam("@StreetAddress1", SqlDbType.NVarChar, 128).Value = orgSite.StreetAddress1;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress2", SqlDbType.NVarChar, 128).Value = orgSite.StreetAddress2;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress3", SqlDbType.NVarChar, 128).Value = orgSite.StreetAddress3;		// NVarChar(128) = NULL
					db.AddParam("@Department", SqlDbType.NVarChar, 128).Value = orgSite.Department;				// NVarChar(128) = NULL
					db.AddParam("@Directions", SqlDbType.NVarChar, 128).Value = orgSite.Directions;				// NVarChar(128) = NULL
					db.AddParam("@ZIPPostCode", SqlDbType.VarChar, 32).Value = orgSite.ZipPostCode;				// VarChar(32) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgSite.UpdatedOn;				// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgSite.UpdatedBy;				// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Sites_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) orgSite.ID = (int)db.Params["@SiteID"].Value;
				}
			}
			return success;
		}

		public static async Task<OrgSiteModel?> RetrieveAsync(int orgSiteID)
		{
			OrgSiteModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@SiteID", SqlDbType.Int).Value = orgSiteID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Sites_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgSiteModel();
					entity.ID = (int)row["Site_ID"];																			// Int NOT NULL PRIMARY KEY
					entity.OrganizationID = (int)row["Organization_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					entity.ContactStatus = (ContactStatus)(byte)row["Contact_Status_ID"];										// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					if (row["Site_Name"] != DBNull.Value) entity.SiteName = (string)row["Site_Name"];							// NVarChar(128) NULL
					entity.Country = (string)row["Country"];																	// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) entity.Region = (string)row["Region"];									// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) entity.StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					entity.City = (string)row["City"];																			// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) entity.StreetAddress1 = (string)row["Street_Address_1"];		// NVarChar(128) NULL
					if (row["Street_Address_2"] != DBNull.Value) entity.StreetAddress2 = (string)row["Street_Address_2"];		// NVarChar(128) NULL
					if (row["Street_Address_3"] != DBNull.Value) entity.StreetAddress3 = (string)row["Street_Address_3"];		// NVarChar(128) NULL
					if (row["Department"] != DBNull.Value) entity.Department = (string)row["Department"];						// NVarChar(128) NULL
					if (row["Directions"] != DBNull.Value) entity.Directions = (string)row["Directions"];						// NVarChar(128) NULL
					if (row["ZIP_Post_Code"] != DBNull.Value) entity.ZipPostCode = (string)row["ZIP_Post_Code"];				// VarChar(32) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		/// <summary>
		///		<para>Gets all sites or offices of an organization</para>
		/// </summary>
		/// <param name="organizationID"></param>
		/// <returns></returns>
		public static async Task<List<OrgSiteModel>> ListAllAsync(int organizationID)
		{
			OrgSiteModel item;
			List<OrgSiteModel> list = new List<OrgSiteModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Sites_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgSiteModel();
					item.ID = (int)row["Site_ID"];																			// Int NOT NULL PRIMARY KEY
					item.OrganizationID = (int)row["Organization_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.ContactStatus = (ContactStatus)(byte)row["Contact_Status_ID"];										// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					if (row["Site_Name"] != DBNull.Value) item.SiteName = (string)row["Site_Name"];							// NVarChar(128) NULL
					item.Country = (string)row["Country"];																	// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) item.Region = (string)row["Region"];									// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					item.City = (string)row["City"];																		// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];		// NVarChar(128) NULL
					if (row["Street_Address_2"] != DBNull.Value) item.StreetAddress2 = (string)row["Street_Address_2"];		// NVarChar(128) NULL
					if (row["Street_Address_3"] != DBNull.Value) item.StreetAddress3 = (string)row["Street_Address_3"];		// NVarChar(128) NULL
					if (row["Department"] != DBNull.Value) item.Department = (string)row["Department"];						// NVarChar(128) NULL
					if (row["Directions"] != DBNull.Value) item.Directions = (string)row["Directions"];						// NVarChar(128) NULL
					if (row["ZIP_Post_Code"] != DBNull.Value) item.ZipPostCode = (string)row["ZIP_Post_Code"];				// VarChar(32) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		/// <summary>
		///		<para>Gets sites or offices of an organization that are not marked as headquarter, billing, or shipping address</para>
		/// </summary>
		/// <param name="organizationID"></param>
		/// <returns></returns>
		public static async Task<List<OrgSiteModel>> ListOtherAsync(int organizationID)
		{
			OrgSiteModel item;
			List<OrgSiteModel> list = new List<OrgSiteModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Sites_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgSiteModel();
					item.ID = (int)row["Site_ID"];																			// Int NOT NULL PRIMARY KEY
					item.OrganizationID = (int)row["Organization_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.ContactStatus = (ContactStatus)(byte)row["Contact_Status_ID"];										// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					if (row["Site_Name"] != DBNull.Value) item.SiteName = (string)row["Site_Name"];							// NVarChar(128) NULL
					item.Country = (string)row["Country"];																	// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) item.Region = (string)row["Region"];									// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					item.City = (string)row["City"];																		// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];		// NVarChar(128) NULL
					if (row["Street_Address_2"] != DBNull.Value) item.StreetAddress2 = (string)row["Street_Address_2"];		// NVarChar(128) NULL
					if (row["Street_Address_3"] != DBNull.Value) item.StreetAddress3 = (string)row["Street_Address_3"];		// NVarChar(128) NULL
					if (row["Department"] != DBNull.Value) item.Department = (string)row["Department"];						// NVarChar(128) NULL
					if (row["Directions"] != DBNull.Value) item.Directions = (string)row["Directions"];						// NVarChar(128) NULL
					if (row["ZIP_Post_Code"] != DBNull.Value) item.ZipPostCode = (string)row["ZIP_Post_Code"];				// VarChar(32) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> MarkForHomeAsync(int orgSiteID, int organizationID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			return await Organizations.SetAddressForHomeAsync(organizationID, orgSiteID, updatedBy, updatedOn);
		}
		
		public static async Task<bool> MarkForBillAsync(int orgSiteID, int organizationID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			return await Organizations.SetAddressForBillAsync(organizationID, orgSiteID, updatedBy, updatedOn);
		}
		
		public static async Task<bool> MarkForShipAsync(int orgSiteID, int organizationID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			return await Organizations.SetAddressForShipAsync(organizationID, orgSiteID, updatedBy, updatedOn);
		}
	}
}
