﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgSegments
    {
		public static async Task<bool> SaveAsync(OrgSegmentModel segment)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (segment.ID.HasValue)
				{
					db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = segment.ID;					// TinyInt
					db.AddParam("@SegmentName", SqlDbType.NVarChar, 64).Value = segment.Name;			// NVarChar(64)
					db.AddParam("@SegmentComments", SqlDbType.NText).Value = segment.Comments;			// NVarChar(MAX) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Segment_Names_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@SegmentID", SqlDbType.TinyInt).Direction = ParameterDirection.Output;	// TinyInt OUTPUT
					db.AddParam("@SegmentName", SqlDbType.NVarChar, 64).Value = segment.Name;			// NVarChar(64)
					db.AddParam("@SegmentComments", SqlDbType.NText).Value = segment.Comments;			// NVarChar(MAX) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Segment_Names_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) segment.ID = (byte)db.Params["@SegmentID"].Value;
				}
			}
			return success;
		}

		public static async Task<OrgSegmentModel?> RetrieveAsync(byte segmentID)
		{
			OrgSegmentModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = segmentID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Segment_Names_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgSegmentModel();
					entity.ID = (byte)row["Segment_ID"];																// Int NOT NULL PRIMARY KEY
					entity.Name = (string)row["Segment_Name"];															// NVarChar(64) NOT NULL
					if (row["Segment_Comments"] != DBNull.Value) entity.Comments = (string)row["Segment_Comments"];		// NVarChar(MAX) NULL
					entity.OrgsCount = (int)row["Organizations_Count"];													// ISNULL((SELECT COUNT(*) FROM CRM.Organizations O WHERE O.Segment_ID = S.Segment_ID), 0)
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		/// <summary>
		///		<para>Gets the list of all segments defined by admin. Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<OrgSegmentModel>> ListAsync()
		{
			OrgSegmentModel item;
			List<OrgSegmentModel> list = new List<OrgSegmentModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Segment_Names_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgSegmentModel();
					item.ID = (byte)row["Segment_ID"];																	// TinyInt NOT NULL PRIMARY KEY
					item.Name = (string)row["Segment_Name"];															// NVarChar(64) NOT NULL
					if (row["Segment_Comments"] != DBNull.Value) item.Comments = (string)row["Segment_Comments"];		// NVarChar(MAX) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		/// <summary>
		///		<para>Gets the current list of segments defined by admin. Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<OrgSegmentModel>> ListWithCountsAsync()
		{
			OrgSegmentModel item;
			List<OrgSegmentModel> list = new List<OrgSegmentModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Segment_Names_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgSegmentModel();
					item.ID = (byte)row["Segment_ID"];																	// TinyInt NOT NULL PRIMARY KEY
					item.Name = (string)row["Segment_Name"];															// NVarChar(64) NOT NULL
					if (row["Segment_Comments"] != DBNull.Value) item.Comments = (string)row["Segment_Comments"];		// NVarChar(MAX) NULL
					item.OrgsCount = (int)row["Organizations_Count"];													// ISNULL((SELECT COUNT(*) FROM CRM.Organizations O WHERE O.Segment_ID = S.Segment_ID), 0)
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(byte segmentID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = segmentID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_Segment_Names_Del")) > 0) success = true;
			}
			return success;
		}
	}
}
