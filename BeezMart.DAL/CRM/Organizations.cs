﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class Organizations
    {
		public static async Task<bool> SaveAsync(OrganizationModel organization)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (organization.ID.HasValue)
				{
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = organization.ID.Value;						// Int
					db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = organization.SegmentID;						// TinyInt = 0
					db.AddParam("@ParentOrgID", SqlDbType.Int).Value = organization.ParentOrgID;						// Int = NULL
					db.AddParam("@OrgName", SqlDbType.NVarChar, 256).Value = organization.Name;							// NVarChar(256)
					db.AddParam("@NamePrefix", SqlDbType.NVarChar, 16).Value = organization.NamePrefix;					// NVarChar(16) = NULL
					db.AddParam("@NameSuffix", SqlDbType.NVarChar, 16).Value = organization.NameSuffix;					// NVarChar(16) = NULL
					db.AddParam("@AlternateNames", SqlDbType.NVarChar, 1024).Value = organization.AlternateNames;		// NVarChar(1024) = NULL
					db.AddParam("@CountryOfOrigin", SqlDbType.Char, 3).Value = organization.CountryOfOrigin;			// Char(3) = 'ROM'
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = organization.UpdatedOn;					// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = organization.UpdatedBy;					// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Organizations_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@OrganizationID", SqlDbType.Int).Direction = ParameterDirection.Output;				// Int OUTPUT
					db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = organization.SegmentID;						// TinyInt = 0
					db.AddParam("@ParentOrgID", SqlDbType.Int).Value = organization.ParentOrgID;						// Int = NULL
					db.AddParam("@OrgName", SqlDbType.NVarChar, 256).Value = organization.Name;							// NVarChar(256)
					db.AddParam("@NamePrefix", SqlDbType.NVarChar, 16).Value = organization.NamePrefix;					// NVarChar(16) = NULL
					db.AddParam("@NameSuffix", SqlDbType.NVarChar, 16).Value = organization.NameSuffix;					// NVarChar(16) = NULL
					db.AddParam("@AlternateNames", SqlDbType.NVarChar, 1024).Value = organization.AlternateNames;		// NVarChar(1024) = NULL
					db.AddParam("@CountryOfOrigin", SqlDbType.Char, 3).Value = organization.CountryOfOrigin;			// Char(3) = 'ROM'
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = organization.UpdatedOn;					// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = organization.UpdatedBy;					// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Organizations_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) organization.ID = (int)db.Params["@OrganizationID"].Value;
				}
			}
			return success;
		}
		public static async Task<OrganizationModel?> RetrieveAsync(int organizationID)
		{
			OrganizationModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Organizations_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrganizationModel();
                    entity.ID = (int)row["Organization_ID"];                                                                            // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    entity.SegmentID = (byte)row["Segment_ID"];                                                                         // TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID)
                    entity.Segment = (string)row["Segment_Name"];                                                                       // NVarChar(64) NOT NULL
                    if (row["Parent_Org_ID"] != DBNull.Value) entity.ParentOrgID = (int)row["Parent_Org_ID"];                           // Int NULL
                    entity.Name = (string)row["Org_Name"];                                                                              // NVarChar(256) NOT NULL
                    if (row["Name_Prefix"] != DBNull.Value) entity.NamePrefix = (string)row["Name_Prefix"];                             // NVarChar(16) NULL
                    if (row["Name_Suffix"] != DBNull.Value) entity.NameSuffix = (string)row["Name_Suffix"];                             // NVarChar(16) NULL
                    if (row["Alternate_Names"] != DBNull.Value) entity.AlternateNames = (string)row["Alternate_Names"];                 // NVarChar(1024) NULL
                    if (row["Country_Of_Origin"] != DBNull.Value) entity.CountryOfOrigin = (string)row["Country_Of_Origin"];            // Char(3) NOT NULL DEFAULT 'ROM'
                    if (row["Site_ID_Headquarter"] != DBNull.Value) entity.SiteIdHome = (int)row["Site_ID_Headquarter"];                // Int NULL
                    if (row["Site_ID_Billing"] != DBNull.Value) entity.SiteIdBill = (int)row["Site_ID_Billing"];                        // Int NULL
                    if (row["Site_ID_Shipping"] != DBNull.Value) entity.SiteIdShip = (int)row["Site_ID_Shipping"];                      // Int NULL
                    if (row["Country"] != DBNull.Value) entity.HQ_Country = (string)row["Country"];                                     // Char(3) NULL DEFAULT 'ROM'
                    if (row["Region"] != DBNull.Value) entity.HQ_Region = (string)row["Region"];                                        // NVarChar(128) NULL
                    if (row["State_Province"] != DBNull.Value) entity.HQ_StateProvince = (string)row["State_Province"];                 // NVarChar(128) NULL
                    if (row["City"] != DBNull.Value) entity.HQ_City = (string)row["City"];                                              // NVarChar(64) NOT NULL
                    if (row["Street_Address_1"] != DBNull.Value) entity.HQ_Address = (string)row["Street_Address_1"];                   // NVarChar(128) NULL
                    entity.UpdatedOn = (DateTime)row["Updated_On"];                                                                     // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			                    // NVarChar(128) NULL
                }
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<bool> SetAddressForHomeAsync(int organizationID, int? siteID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				db.AddParam("@SiteID", SqlDbType.Int).Value = siteID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("CRM.Organizations_Upd_1")) > 0) success = true;
			}
			return success;
		}
		public static async Task<bool> SetAddressForBillAsync(int organizationID, int? siteID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				db.AddParam("@SiteID", SqlDbType.Int).Value = siteID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("CRM.Organizations_Upd_2")) > 0) success = true;
			}
			return success;
		}
		public static async Task<bool> SetAddressForShipAsync(int organizationID, int? siteID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				db.AddParam("@SiteID", SqlDbType.Int).Value = siteID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("CRM.Organizations_Upd_3")) > 0) success = true;
			}
			return success;
		}
		public static async Task<bool> MergeAsync(int fromOrganizationID, int toOrganizationID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@FromOrganizationID", SqlDbType.Int).Value = fromOrganizationID;
				db.AddParam("@ToOrganizationID", SqlDbType.Int).Value = toOrganizationID;
				if ((await db.ExecuteProcedureAsync("CRM.Organizations_Merge")) > 0) success = true;
			}
			return success;
		}
		public static async Task<List<OrganizationModel>> LookupAsync(OrganizationModel.LookupFilter filterCriteria, short maxRecordsCount = 1000)
		{
			OrganizationModel item;
			List<OrganizationModel> list = new List<OrganizationModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				StringBuilder stbTransactSqlCommand = new StringBuilder();
				#region Build Database Query
				StringBuilder stbSelectClause = new StringBuilder();
				StringBuilder stbFromClause = new StringBuilder();
				StringBuilder stbWhereClause = new StringBuilder();
				StringBuilder stbOrderByClause = new StringBuilder();
				#region Build SELECT clause
				stbSelectClause.AppendLine(filterCriteria.HasAnyCriteria ? $"SELECT TOP {maxRecordsCount}" : $"SELECT TOP {Defaults.Search_Result_Unfiltered_MaxCount}");
				stbSelectClause.AppendLine(" O.Organization_ID");
				stbSelectClause.AppendLine(",O.Org_Name");
				stbSelectClause.AppendLine(",O.Segment_ID");
				stbSelectClause.AppendLine(",N.Segment_Name");
				stbSelectClause.AppendLine(",O.Parent_Org_ID");
				stbSelectClause.AppendLine(",O.Name_Prefix");
				stbSelectClause.AppendLine(",O.Name_Suffix");
				stbSelectClause.AppendLine(",O.Alternate_Names");
				stbSelectClause.AppendLine(",O.Country_Of_Origin");
				stbSelectClause.AppendLine(",O.Site_ID_Headquarter");
				stbSelectClause.AppendLine(",O.Site_ID_Billing");
				stbSelectClause.AppendLine(",O.Site_ID_Shipping");
				stbSelectClause.AppendLine(",S.Country");
				stbSelectClause.AppendLine(",S.Region");
				stbSelectClause.AppendLine(",S.State_Province");
				stbSelectClause.AppendLine(",S.City");
				stbSelectClause.AppendLine(",S.Street_Address_1");
				#endregion
				#region Build FROM clause
				stbFromClause.AppendLine("FROM CRM.Organizations O");
				stbFromClause.AppendLine("    INNER JOIN CRM.Orgs_Segment_Names N ON O.Segment_ID = N.Segment_ID");
				stbFromClause.AppendLine("    LEFT JOIN CRM.Orgs_Sites S ON O.Site_ID_Headquarter = S.Site_ID");
                #endregion
                #region Build WHERE clause
                string searchOperator = filterCriteria.SearchOperator.ToString().ToUpper();
                if (!string.IsNullOrWhiteSpace(filterCriteria.NamePattern))
				{
					stbWhereClause.AppendLine($"{searchOperator} (O.Org_Name LIKE @OrgName OR O.Alternate_Names LIKE @OrgName)");
					db.AddParam("@OrgName", SqlDbType.NVarChar, 128).Value = filterCriteria.NameExactMatch ? filterCriteria.NamePattern : "%" + filterCriteria.NamePattern + "%";
				}
				if (!string.IsNullOrWhiteSpace(filterCriteria.Contact))
				{
					stbWhereClause.AppendLine($"{searchOperator} O.Organization_ID IN (SELECT DISTINCT Organization_ID FROM CRM.Orgs_Contacts WHERE Contact LIKE @Contact)");
					db.AddParam("@Contact", SqlDbType.NVarChar, 128).Value = "%" + filterCriteria.Contact + "%";
				}
				if (!string.IsNullOrWhiteSpace(filterCriteria.City))
				{
					stbWhereClause.AppendLine($"{searchOperator} O.Organization_ID IN (SELECT DISTINCT Organization_ID FROM CRM.Orgs_Sites WHERE City LIKE @City)");
					db.AddParam("@City", SqlDbType.NVarChar, 64).Value  = "%" + filterCriteria.City + "%";
				}
				if (filterCriteria.InSegments != null && filterCriteria.InSegments.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (byte SegmentID in filterCriteria.InSegments)
					{
						stbTemp.Append(SegmentID);
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} O.Segment_ID IN ({stbTemp})");
				}
				if (filterCriteria.HasFlags != null && filterCriteria.HasFlags.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (short FlagID in filterCriteria.HasFlags)
					{
						stbTemp.Append(FlagID);
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} O.Organization_ID IN (SELECT DISTINCT Organization_ID FROM CRM.Orgs_Flag_Fields WHERE Field_ID IN ({stbTemp}))");
				}

				if (stbWhereClause.Length > 0)
				{
					stbWhereClause.Remove(0, searchOperator.Length + 1);
					stbWhereClause.Insert(0, "WHERE ");
				}
				#endregion
				#region Build ORDER BY clause
				stbOrderByClause.AppendLine("ORDER BY O.Org_Name, S.City, S.State_Province, S.Street_Address_1, O.Organization_ID");
				#endregion
				stbTransactSqlCommand.AppendLine(stbSelectClause.ToString());
				stbTransactSqlCommand.AppendLine(stbFromClause.ToString());
				stbTransactSqlCommand.AppendLine(stbWhereClause.ToString());
				stbTransactSqlCommand.AppendLine(stbOrderByClause.ToString());
				#endregion
				row = rdrReader = await db.GetSqlDataReaderFromTextCommandAsync(stbTransactSqlCommand.ToString());
				while (await rdrReader.ReadAsync())
				{
					item = new OrganizationModel();
					item.ID = (int)row["Organization_ID"];																			// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.Name = (string)row["Org_Name"];																			// NVarChar(256) NOT NULL
					item.SegmentID = (byte)row["Segment_ID"];																		// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID)
					item.Segment = (string)row["Segment_Name"];																		// NVarChar(64) NOT NULL
					if (row["Parent_Org_ID"] != DBNull.Value) item.ParentOrgID = (int)row["Parent_Org_ID"];							// Int NULL
					if (row["Name_Prefix"] != DBNull.Value) item.NamePrefix = (string)row["Name_Prefix"];							// NVarChar(16) NULL
					if (row["Name_Suffix"] != DBNull.Value) item.NameSuffix = (string)row["Name_Suffix"];							// NVarChar(16) NULL
					if (row["Alternate_Names"] != DBNull.Value) item.AlternateNames = (string)row["Alternate_Names"];				// NVarChar(1024) NULL
					if (row["Country_Of_Origin"] != DBNull.Value) item.CountryOfOrigin = (string)row["Country_Of_Origin"];			// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Site_ID_Headquarter"] != DBNull.Value) item.SiteIdHome = (int)row["Site_ID_Headquarter"];				// Int NULL
					if (row["Site_ID_Billing"] != DBNull.Value) item.SiteIdBill = (int)row["Site_ID_Billing"];						// Int NULL
					if (row["Site_ID_Shipping"] != DBNull.Value) item.SiteIdShip = (int)row["Site_ID_Shipping"];					// Int NULL
					if (row["Country"] != DBNull.Value) item.HQ_Country = (string)row["Country"];									// Char(3) NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) item.HQ_Region = (string)row["Region"];										// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) item.HQ_StateProvince = (string)row["State_Province"];				// NVarChar(128) NULL
					if (row["City"] != DBNull.Value) item.HQ_City = (string)row["City"];											// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) item.HQ_Address = (string)row["Street_Address_1"];					// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
	}
}
