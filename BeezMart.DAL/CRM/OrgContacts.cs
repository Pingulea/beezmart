﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgContacts
    {
		public static async Task<bool> SaveAsync(OrgContactModel contact)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (contact.ID.HasValue)
				{
					db.AddParam("@ContactID", SqlDbType.BigInt).Value = contact.ID.Value;					// BigInt
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = contact.OrganizationID;			// Int
					db.AddParam("@SiteID", SqlDbType.Int).Value = contact.SiteID;							// Int
					db.AddParam("@Contact", SqlDbType.NVarChar, 256).Value = contact.Contact;				// NVarChar(256)
					db.AddParam("@Comments", SqlDbType.NVarChar, 256).Value = contact.Comments;				// NVarChar(256) = NULL
					db.AddParam("@ContactTypeID", SqlDbType.TinyInt).Value = (byte)contact.Type;			// TinyInt = 1
					db.AddParam("@ContactStatusID", SqlDbType.TinyInt).Value = (byte)contact.Status;		// TinyInt = 1
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;				// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = contact.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Contacts_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@ContactID", SqlDbType.BigInt).Direction = ParameterDirection.Output;		// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = contact.OrganizationID;			// Int
					db.AddParam("@SiteID", SqlDbType.Int).Value = contact.SiteID;							// Int
					db.AddParam("@Contact", SqlDbType.NVarChar, 256).Value = contact.Contact;				// NVarChar(256)
					db.AddParam("@Comments", SqlDbType.NVarChar, 256).Value = contact.Comments;				// NVarChar(256) = NULL
					db.AddParam("@ContactTypeID", SqlDbType.TinyInt).Value = (byte)contact.Type;			// TinyInt = 1
					db.AddParam("@ContactStatusID", SqlDbType.TinyInt).Value = (byte)contact.Status;		// TinyInt = 1
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;				// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = contact.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Contacts_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) contact.ID = (long)db.Params["@ContactID"].Value;
				}
			}
			return success;
		}
		public static async Task<OrgContactModel?> RetrieveAsync(long contactID)
		{
			OrgContactModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@ContactID", SqlDbType.Int).Value = contactID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Contacts_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgContactModel();
					entity.ID = (long)row["Contact_ID"];													// BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					entity.OrganizationID = (int)row["Organization_ID"];									// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Site_ID"] != DBNull.Value) entity.SiteID = (int)row["Site_ID"];				// Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
					entity.Contact = (string)row["Contact"];												// NVarChar(256) NOT NULL
					if (row["Comments"] != DBNull.Value) entity.Comments = (string)row["Comments"];			// NVarChar(256) NULL
					entity.Type = ((ContactType)(byte)row["Contact_Type_ID"]);								// TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
					entity.Status = ((ContactStatus)(byte)row["Contact_Status_ID"]);						// TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
					entity.UpdatedOn = (DateTime)row["Updated_On"];											// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];	// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<List<OrgContactModel>> ListAsync(int organizationID)
		{
			OrgContactModel item;
			List<OrgContactModel> list = new List<OrgContactModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Contacts_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgContactModel();
					item.ID = (long)row["Contact_ID"];													// BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					item.OrganizationID = (int)row["Organization_ID"];									// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Site_ID"] != DBNull.Value) item.SiteID = (int)row["Site_ID"];				// Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
					item.Contact = (string)row["Contact"];												// NVarChar(256) NOT NULL
					if (row["Comments"] != DBNull.Value) item.Comments = (string)row["Comments"];		// NVarChar(256) NULL
					item.Type = ((ContactType)(byte)row["Contact_Type_ID"]);							// TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
					item.Status = ((ContactStatus)(byte)row["Contact_Status_ID"]);						// TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
					item.UpdatedOn = (DateTime)row["Updated_On"];										// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];	// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(long orgContactID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@ContactID", SqlDbType.BigInt).Value = orgContactID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_Contacts_Del")) > 0) success = true;
			}
			return success;
		}
	}
}
