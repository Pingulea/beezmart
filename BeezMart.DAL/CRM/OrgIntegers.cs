﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgIntegers
    {
		public static async Task<bool> SaveAsync(OrgIntegerModel orgInteger)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (orgInteger.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgInteger.ID.Value;					// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgInteger.FieldID;					// SmallInt
					db.AddParam("@FieldValue", SqlDbType.Int).Value = orgInteger.Value;						// Int
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgInteger.UpdatedOn;		// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgInteger.UpdatedBy;		// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Integer_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;		// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgInteger.OrganizationID;		// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgInteger.FieldID;					// SmallInt
					db.AddParam("@FieldValue", SqlDbType.Int).Value = orgInteger.Value;						// Int
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgInteger.UpdatedOn;		// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgInteger.UpdatedBy;		// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_Integer_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) orgInteger.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}

		public static async Task<OrgIntegerModel?> RetrieveAsync(long orgIntegerID)
		{
			OrgIntegerModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgIntegerID;
				row = rdrReader =await  db.GetSqlDataReaderAsync("CRM.Orgs_Integer_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgIntegerModel();
					entity.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					entity.OrganizationID = (int)row["Organization_ID"];											// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					entity.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					entity.Value = (int)row["Field_Value"];															// Int NOT NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<OrgIntegerModel>> ListAsync(int organizationID)
		{
			OrgIntegerModel item;
			List<OrgIntegerModel> list = new List<OrgIntegerModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_Integer_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgIntegerModel();
					item.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					item.OrganizationID = (int)row["Organization_ID"];												// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.FieldID = (short)row["Field_ID"];															// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					item.Value = (int)row["Field_Value"];															// Int NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long orgIntegerID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgIntegerID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_Integer_Fields_Del")) > 0) success = true;
			}
			return success;
		}

		/// <summary>
		///		<para>Gets the available categories of integer fields.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await OrgFields.ListCategoriesAsync(FieldType.Integer);
		}

		/// <summary>
		///		<para>Gets the available integer types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<OrgFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await OrgFields.ListAsync(FieldType.Integer);
		}
	}
}
