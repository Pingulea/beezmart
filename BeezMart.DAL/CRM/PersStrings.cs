﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class PersStrings
	{
		public static async Task<bool> SaveAsync(PersStringModel persString)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (persString.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = persString.ID.Value;				// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persString.FieldID;				// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = persString.Value;		// NVarChar(256)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persString.UpdatedOn;	// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persString.UpdatedBy;	// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_String_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;	// BigInt OUTPUT
					db.AddParam("@PersonID", SqlDbType.Int).Value = persString.PersonID;				// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = persString.FieldID;				// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = persString.Value;		// NVarChar(256)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persString.UpdatedOn;	// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persString.UpdatedBy;	// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_String_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) persString.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}

		public static async Task<PersStringModel?> RetrieveAsync(long persStringID)
		{
			PersStringModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.Int).Value = persStringID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_String_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PersStringModel();
					entity.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					entity.PersonID = (int)row["Person_ID"];														// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					entity.Value = (string)row["Field_Value"];														// NVarChar(256) NOT NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<PersStringModel>> ListAsync(int personID)
		{
			PersStringModel item;
			List<PersStringModel> list = new List<PersStringModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_String_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PersStringModel();
					item.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					item.PersonID = (int)row["Person_ID"];															// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.FieldID = (short)row["Field_ID"];															// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					item.Value = (string)row["Field_Value"];														// NVarChar(256) NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long persStringID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = persStringID;
				if ((await db.DeleteRecordAsync("CRM.Pers_String_Fields_Del")) > 0) success = true;
			}
			return success;
		}

		/// <summary>
		///		<para>Gets the available categories of string fields.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await PersFields.ListCategoriesAsync(FieldType.String);
		}

		/// <summary>
		///		<para>Gets the available string types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<PersFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await PersFields.ListAsync(FieldType.String);
		}
	}
}
