﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class Persons
	{
		public static async Task<bool> SaveAsync(PersonModel person)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (person.ID.HasValue)
				{
					db.AddParam("@PersonID", SqlDbType.Int).Value = person.ID.Value;						// Int
					db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = person.SegmentID;					// TinyInt = 0
					db.AddParam("@FirstName", SqlDbType.NVarChar, 64).Value = person.FirstName;				// NVarChar(64)
					db.AddParam("@LastName", SqlDbType.NVarChar, 64).Value = person.LastName;				// NVarChar(64)
					db.AddParam("@NamePrefix", SqlDbType.NVarChar, 16).Value = person.NamePrefix;			// NVarChar(16) = NULL
					db.AddParam("@NameSuffix", SqlDbType.NVarChar, 16).Value = person.NameSuffix;			// NVarChar(16) = NULL
					db.AddParam("@Nicknames", SqlDbType.NVarChar, 256).Value = person.Nicknames;			// NVarChar(256) = NULL
					db.AddParam("@CountryOfOrigin", SqlDbType.Char, 3).Value = person.CountryOfOrigin;		// Char(3) = 'ROM'
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = person.UpdatedOn;			// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = person.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Persons_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@PersonID", SqlDbType.Int).Direction = ParameterDirection.Output;			// Int OUTPUT
					db.AddParam("@SegmentID", SqlDbType.TinyInt).Value = person.SegmentID;					// TinyInt = 0
					db.AddParam("@FirstName", SqlDbType.NVarChar, 64).Value = person.FirstName;				// NVarChar(64)
					db.AddParam("@LastName", SqlDbType.NVarChar, 64).Value = person.LastName;				// NVarChar(64)
					db.AddParam("@NamePrefix", SqlDbType.NVarChar, 16).Value = person.NamePrefix;			// NVarChar(16) = NULL
					db.AddParam("@NameSuffix", SqlDbType.NVarChar, 16).Value = person.NameSuffix;			// NVarChar(16) = NULL
					db.AddParam("@Nicknames", SqlDbType.NVarChar, 256).Value = person.Nicknames;			// NVarChar(256) = NULL
					db.AddParam("@CountryOfOrigin", SqlDbType.Char, 3).Value = person.CountryOfOrigin;		// Char(3) = 'ROM'
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = person.UpdatedOn;			// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = person.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Persons_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) person.ID = (int)db.Params["@PersonID"].Value;
				}
			}
			return success;
		}

		public static async Task<PersonModel?> RetrieveAsync(int personID)
		{
			PersonModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Persons_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PersonModel();
                    entity.ID = (int)row["Person_ID"];                                                                                  // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    entity.FirstName = (string)row["First_Name"];                                                                       // NVarChar(128) NOT NULL
                    entity.LastName = (string)row["Last_Name"];                                                                         // NVarChar(128) NOT NULL
                    entity.SegmentID = (byte)row["Segment_ID"];                                                                         // TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID)
                    entity.Segment = (string)row["Segment_Name"];                                                                       // NVarChar(64) NOT NULL
                    if (row["Name_Prefix"] != DBNull.Value) entity.NamePrefix = (string)row["Name_Prefix"];                             // NVarChar(16) NULL
                    if (row["Name_Suffix"] != DBNull.Value) entity.NameSuffix = (string)row["Name_Suffix"];                             // NVarChar(16) NULL
                    if (row["Nicknames"] != DBNull.Value) entity.Nicknames = (string)row["Nicknames"];                                  // NVarChar(256) NULL
                    if (row["Country_Of_Origin"] != DBNull.Value) entity.CountryOfOrigin = (string)row["Country_Of_Origin"];            // Char(3) NOT NULL DEFAULT 'ROM'
                    if (row["Site_ID_Home"] != DBNull.Value) entity.SiteIdHome = (int)row["Site_ID_Home"];                              // Int NULL
                    if (row["Site_ID_Billing"] != DBNull.Value) entity.SiteIdBill = (int)row["Site_ID_Billing"];                        // Int NULL
                    if (row["Site_ID_Shipping"] != DBNull.Value) entity.SiteIdShip = (int)row["Site_ID_Shipping"];                      // Int NULL
                    if (row["Country"] != DBNull.Value) entity.Home_Country = (string)row["Country"];                                   // Char(3) NULL DEFAULT 'ROM'
                    if (row["Region"] != DBNull.Value) entity.Home_Region = (string)row["Region"];                                      // NVarChar(128) NULL
                    if (row["State_Province"] != DBNull.Value) entity.Home_StateProvince = (string)row["State_Province"];               // NVarChar(128) NULL
                    if (row["City"] != DBNull.Value) entity.Home_City = (string)row["City"];                                            // NVarChar(64) NOT NULL
                    if (row["Street_Address_1"] != DBNull.Value) entity.Home_Address = (string)row["Street_Address_1"];                 // NVarChar(128) NULL
                    entity.UpdatedOn = (DateTime)row["Updated_On"];                                                                     // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];                                // NVarChar(128) NULL
                }
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<bool> SetAddressForHomeAsync(int personID, int? siteID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				db.AddParam("@SiteID", SqlDbType.Int).Value = siteID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("CRM.Persons_Upd_1")) > 0) success = true;
			}
			return success;
		}
		public static async Task<bool> SetAddressForBillAsync(int personID, int? siteID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				db.AddParam("@SiteID", SqlDbType.Int).Value = siteID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("CRM.Persons_Upd_2")) > 0) success = true;
			}
			return success;
		}
		public static async Task<bool> SetAddressForShipAsync(int personID, int? siteID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				db.AddParam("@SiteID", SqlDbType.Int).Value = siteID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("CRM.Persons_Upd_3")) > 0) success = true;
			}
			return success;
		}

		public static async Task<bool> MergeAsync(int fromPersonID, int toPersonID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@FromPersonID", SqlDbType.Int).Value = fromPersonID;
				db.AddParam("@ToPersonID", SqlDbType.Int).Value = toPersonID;
				if ((await db.ExecuteProcedureAsync("CRM.Persons_Merge")) > 0) success = true;
			}
			return success;
		}

		public static async Task<List<PersonModel>> LookupAsync(PersonModel.LookupFilter filterCriteria, short maxRecordsCount = 1000)
		{
			PersonModel item;
			List<PersonModel> list = new List<PersonModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				StringBuilder stbTransactSqlCommand = new StringBuilder();
				#region Build Database Query
				StringBuilder stbSelectClause = new StringBuilder();
				StringBuilder stbFromClause = new StringBuilder();
				StringBuilder stbWhereClause = new StringBuilder();
				StringBuilder stbOrderByClause = new StringBuilder();
				#region Build SELECT clause
				stbSelectClause.AppendLine(filterCriteria.HasAnyCriteria ? $"SELECT TOP {maxRecordsCount}" : $"SELECT TOP {Defaults.Search_Result_Unfiltered_MaxCount}");
				stbSelectClause.AppendLine(" O.Person_ID");
				stbSelectClause.AppendLine(",O.First_Name");
				stbSelectClause.AppendLine(",O.Last_Name");
				stbSelectClause.AppendLine(",O.Segment_ID");
				stbSelectClause.AppendLine(",N.Segment_Name");
				stbSelectClause.AppendLine(",O.Name_Prefix");
				stbSelectClause.AppendLine(",O.Name_Suffix");
				stbSelectClause.AppendLine(",O.Nicknames");
				stbSelectClause.AppendLine(",O.Country_Of_Origin");
				stbSelectClause.AppendLine(",O.Site_ID_Home");
				stbSelectClause.AppendLine(",O.Site_ID_Billing");
				stbSelectClause.AppendLine(",O.Site_ID_Shipping");
				stbSelectClause.AppendLine(",S.Country");
				stbSelectClause.AppendLine(",S.Region");
				stbSelectClause.AppendLine(",S.State_Province");
				stbSelectClause.AppendLine(",S.City");
				stbSelectClause.AppendLine(",S.Street_Address_1");
				#endregion
				#region Build FROM clause
				stbFromClause.AppendLine("FROM CRM.Persons O");
				stbFromClause.AppendLine("    INNER JOIN CRM.Pers_Segment_Names N ON O.Segment_ID = N.Segment_ID");
				stbFromClause.AppendLine("    LEFT JOIN CRM.Pers_Sites S ON O.Site_ID_Home = S.Site_ID");
                #endregion
                #region Build WHERE clause
                string searchOperator = filterCriteria.SearchOperator.ToString().ToUpper();
                if (!string.IsNullOrWhiteSpace(filterCriteria.NamePattern))
				{
					StringBuilder stbTemp = new StringBuilder();
					char[] nameSeparators = { ' ', ',', ';', '-' };
					string[] names = filterCriteria.NamePattern.Split(nameSeparators, StringSplitOptions.RemoveEmptyEntries);
					for (int i = 0; i < names.Length; i++)
					{
						stbTemp.AppendLine($"AND (O.First_Name LIKE @PersName{i} OR O.Last_Name LIKE @PersName{i} OR O.Nicknames LIKE @PersName{i})");
						db.AddParam($"@PersName{i}", SqlDbType.NVarChar, 128).Value = filterCriteria.NameExactMatch ? names[i] : "%" + names[i] + "%";
					}
                    stbTemp.Remove(0, 4);
                    stbWhereClause.AppendLine($"{searchOperator} ({stbTemp})");
                }
				if (!string.IsNullOrWhiteSpace(filterCriteria.Contact))
				{
					stbWhereClause.AppendLine($"{searchOperator} O.Person_ID IN (SELECT DISTINCT Person_ID FROM CRM.Pers_Contacts WHERE Contact LIKE @Contact)");
					db.AddParam("@Contact", SqlDbType.NVarChar, 128).Value = "%" + filterCriteria.Contact + "%";
				}
				if (!string.IsNullOrWhiteSpace(filterCriteria.City))
				{
					stbWhereClause.AppendLine($"{searchOperator} O.Person_ID IN (SELECT DISTINCT Person_ID FROM CRM.Pers_Sites WHERE City LIKE @City)");
					db.AddParam("@City", SqlDbType.NVarChar, 64).Value  = "%" + filterCriteria.City + "%";
				}
				if (filterCriteria.InSegments != null && filterCriteria.InSegments.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (byte SegmentID in filterCriteria.InSegments)
					{
						stbTemp.Append(SegmentID);
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} O.Segment_ID IN ({stbTemp})");
				}
				if (filterCriteria.HasFlags != null && filterCriteria.HasFlags.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (short FlagID in filterCriteria.HasFlags)
					{
						stbTemp.Append(FlagID);
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} O.Person_ID IN (SELECT DISTINCT Person_ID FROM CRM.Pers_Flag_Fields WHERE Field_ID IN ({stbTemp}))");
				}

				if (stbWhereClause.Length > 0)
				{
					stbWhereClause.Remove(0, searchOperator.Length + 1);
					stbWhereClause.Insert(0, "WHERE ");
				}
				#endregion
				#region Build ORDER BY clause
				stbOrderByClause.AppendLine("ORDER BY O.First_Name, O.Last_Name, S.City, S.State_Province, S.Street_Address_1, O.Person_ID");
				#endregion
				stbTransactSqlCommand.AppendLine(stbSelectClause.ToString());
				stbTransactSqlCommand.AppendLine(stbFromClause.ToString());
				stbTransactSqlCommand.AppendLine(stbWhereClause.ToString());
				stbTransactSqlCommand.AppendLine(stbOrderByClause.ToString());
				#endregion
				row = rdrReader = await db.GetSqlDataReaderFromTextCommandAsync(stbTransactSqlCommand.ToString());
				while (await rdrReader.ReadAsync())
				{
					item = new PersonModel();
					item.ID = (int)row["Person_ID"];																			// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.FirstName = (string)row["First_Name"];																	// NVarChar(128) NOT NULL
					item.LastName = (string)row["Last_Name"];																	// NVarChar(128) NOT NULL
					item.SegmentID = (byte)row["Segment_ID"];																	// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID)
					item.Segment = (string)row["Segment_Name"];																	// NVarChar(64) NOT NULL
					if (row["Name_Prefix"] != DBNull.Value) item.NamePrefix = (string)row["Name_Prefix"];						// NVarChar(16) NULL
					if (row["Name_Suffix"] != DBNull.Value) item.NameSuffix = (string)row["Name_Suffix"];						// NVarChar(16) NULL
					if (row["Nicknames"] != DBNull.Value) item.Nicknames = (string)row["Nicknames"];							// NVarChar(256) NULL
					if (row["Country_Of_Origin"] != DBNull.Value) item.CountryOfOrigin = (string)row["Country_Of_Origin"];		// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Site_ID_Home"] != DBNull.Value) item.SiteIdHome = (int)row["Site_ID_Home"];						// Int NULL
					if (row["Site_ID_Billing"] != DBNull.Value) item.SiteIdBill = (int)row["Site_ID_Billing"];					// Int NULL
					if (row["Site_ID_Shipping"] != DBNull.Value) item.SiteIdShip = (int)row["Site_ID_Shipping"];				// Int NULL
					if (row["Country"] != DBNull.Value) item.Home_Country = (string)row["Country"];								// Char(3) NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) item.Home_Region = (string)row["Region"];								// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) item.Home_StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					if (row["City"] != DBNull.Value) item.Home_City = (string)row["City"];										// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) item.Home_Address = (string)row["Street_Address_1"];			// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
	}
}
