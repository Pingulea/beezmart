﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using BeezMart.Entities;
using BeezMart.Entities.CRM;

namespace BeezMart.DAL.CRM
{
	public static class OrgStrings
    {
		public static async Task<bool> SaveAsync(OrgStringModel orgString)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (orgString.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgString.ID.Value;				// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgString.FieldID;				// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = orgString.Value;		// NVarChar(256)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgString.UpdatedOn;		// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgString.UpdatedBy;		// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_String_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;	// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgString.OrganizationID;		// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgString.FieldID;				// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NVarChar, 256).Value = orgString.Value;		// NVarChar(256)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgString.UpdatedOn;		// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgString.UpdatedBy;		// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_String_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) orgString.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}

		public static async Task<OrgStringModel?> RetrieveAsync(long orgStringID)
		{
			OrgStringModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.Int).Value = orgStringID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_String_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgStringModel();
					entity.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					entity.OrganizationID = (int)row["Organization_ID"];											// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					entity.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					entity.Value = (string)row["Field_Value"];														// NVarChar(256) NOT NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<OrgStringModel>> ListAsync(int organizationID)
		{
			OrgStringModel item;
			List<OrgStringModel> list = new List<OrgStringModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_String_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgStringModel();
					item.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY
					item.OrganizationID = (int)row["Organization_ID"];												// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.FieldID = (short)row["Field_ID"];															// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];															// NVarChar(64) NOT NULL
					item.Value = (string)row["Field_Value"];														// NVarChar(256) NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long orgStringID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgStringID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_String_Fields_Del")) > 0) success = true;
			}
			return success;
		}

		/// <summary>
		///		<para>Gets the available categories of string fields.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await OrgFields.ListCategoriesAsync(FieldType.String);
		}

		/// <summary>
		///		<para>Gets the available string types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<OrgFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await OrgFields.ListAsync(FieldType.String);
		}
	}
}
