﻿using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
    public static class PersSites
	{
		public static async Task<bool> SaveAsync(PersSiteModel persSite)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (persSite.ID.HasValue)
				{
					db.AddParam("@SiteID", SqlDbType.Int).Value = persSite.ID.Value;								// Int
					db.AddParam("@PersonID", SqlDbType.Int).Value = persSite.PersonID;								// Int
					db.AddParam("@ContactStatusID", SqlDbType.TinyInt).Value = (byte)persSite.ContactStatus;		// TinyInt = 0
					db.AddParam("@SiteName", SqlDbType.NVarChar, 128).Value = persSite.SiteName;					// NVarChar(128) = NULL
					db.AddParam("@Country", SqlDbType.Char, 3).Value = persSite.Country;							// Char(3) = 'ROM'
					db.AddParam("@Region", SqlDbType.NVarChar, 128).Value = persSite.Region;						// NVarChar(128) = NULL
					db.AddParam("@StateProvince", SqlDbType.NVarChar, 128).Value = persSite.StateProvince;			// NVarChar(128) = NULL
					db.AddParam("@City", SqlDbType.NVarChar, 64).Value = persSite.City;								// NVarChar(64)
					db.AddParam("@StreetAddress1", SqlDbType.NVarChar, 128).Value = persSite.StreetAddress1;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress2", SqlDbType.NVarChar, 128).Value = persSite.StreetAddress2;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress3", SqlDbType.NVarChar, 128).Value = persSite.StreetAddress3;		// NVarChar(128) = NULL
					db.AddParam("@Department", SqlDbType.NVarChar, 128).Value = persSite.Department;				// NVarChar(128) = NULL
					db.AddParam("@Directions", SqlDbType.NVarChar, 128).Value = persSite.Directions;				// NVarChar(128) = NULL
					db.AddParam("@ZIPPostCode", SqlDbType.VarChar, 32).Value = persSite.ZipPostCode;				// VarChar(32) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persSite.UpdatedOn;					// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persSite.UpdatedBy;					// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_Sites_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@SiteID", SqlDbType.Int).Direction = ParameterDirection.Output;					// Int OUTPUT
					db.AddParam("@PersonID", SqlDbType.Int).Value = persSite.PersonID;								// Int
					db.AddParam("@ContactStatusID", SqlDbType.TinyInt).Value = (byte)persSite.ContactStatus;		// TinyInt = 0
					db.AddParam("@SiteName", SqlDbType.NVarChar, 128).Value = persSite.SiteName;					// NVarChar(128) = NULL
					db.AddParam("@Country", SqlDbType.Char, 3).Value = persSite.Country;							// Char(3) = 'ROM'
					db.AddParam("@Region", SqlDbType.NVarChar, 128).Value = persSite.Region;						// NVarChar(128) = NULL
					db.AddParam("@StateProvince", SqlDbType.NVarChar, 128).Value = persSite.StateProvince;			// NVarChar(128) = NULL
					db.AddParam("@City", SqlDbType.NVarChar, 64).Value = persSite.City;								// NVarChar(64)
					db.AddParam("@StreetAddress1", SqlDbType.NVarChar, 128).Value = persSite.StreetAddress1;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress2", SqlDbType.NVarChar, 128).Value = persSite.StreetAddress2;		// NVarChar(128) = NULL
					db.AddParam("@StreetAddress3", SqlDbType.NVarChar, 128).Value = persSite.StreetAddress3;		// NVarChar(128) = NULL
					db.AddParam("@Department", SqlDbType.NVarChar, 128).Value = persSite.Department;				// NVarChar(128) = NULL
					db.AddParam("@Directions", SqlDbType.NVarChar, 128).Value = persSite.Directions;				// NVarChar(128) = NULL
					db.AddParam("@ZIPPostCode", SqlDbType.VarChar, 32).Value = persSite.ZipPostCode;				// VarChar(32) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = persSite.UpdatedOn;					// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = persSite.UpdatedBy;					// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Pers_Sites_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) persSite.ID = (int)db.Params["@SiteID"].Value;
				}
			}
			return success;
		}

		public static async Task<PersSiteModel?> RetrieveAsync(int persSiteID)
		{
			PersSiteModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@SiteID", SqlDbType.Int).Value = persSiteID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Sites_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PersSiteModel();
					entity.ID = (int)row["Site_ID"];																			// Int NOT NULL PRIMARY KEY
					entity.PersonID = (int)row["Person_ID"];																	// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.ContactStatus = (ContactStatus)(byte)row["Contact_Status_ID"];										// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					if (row["Site_Name"] != DBNull.Value) entity.SiteName = (string)row["Site_Name"];							// NVarChar(128) NULL
					entity.Country = (string)row["Country"];																	// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) entity.Region = (string)row["Region"];									// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) entity.StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					entity.City = (string)row["City"];																			// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) entity.StreetAddress1 = (string)row["Street_Address_1"];		// NVarChar(128) NULL
					if (row["Street_Address_2"] != DBNull.Value) entity.StreetAddress2 = (string)row["Street_Address_2"];		// NVarChar(128) NULL
					if (row["Street_Address_3"] != DBNull.Value) entity.StreetAddress3 = (string)row["Street_Address_3"];		// NVarChar(128) NULL
					if (row["Department"] != DBNull.Value) entity.Department = (string)row["Department"];						// NVarChar(128) NULL
					if (row["Directions"] != DBNull.Value) entity.Directions = (string)row["Directions"];						// NVarChar(128) NULL
					if (row["ZIP_Post_Code"] != DBNull.Value) entity.ZipPostCode = (string)row["ZIP_Post_Code"];				// VarChar(32) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		/// <summary>
		///		<para>Gets all sites or offices of a person</para>
		/// </summary>
		/// <param name="personID"></param>
		/// <returns></returns>
		public static async Task<List<PersSiteModel>> ListAllAsync(int personID)
		{
			PersSiteModel item;
			List<PersSiteModel> list = new List<PersSiteModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Sites_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PersSiteModel();
					item.ID = (int)row["Site_ID"];																			// Int NOT NULL PRIMARY KEY
					item.PersonID = (int)row["Person_ID"];																	// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.ContactStatus = (ContactStatus)(byte)row["Contact_Status_ID"];										// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					if (row["Site_Name"] != DBNull.Value) item.SiteName = (string)row["Site_Name"];							// NVarChar(128) NULL
					item.Country = (string)row["Country"];																	// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) item.Region = (string)row["Region"];									// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					item.City = (string)row["City"];																		// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];		// NVarChar(128) NULL
					if (row["Street_Address_2"] != DBNull.Value) item.StreetAddress2 = (string)row["Street_Address_2"];		// NVarChar(128) NULL
					if (row["Street_Address_3"] != DBNull.Value) item.StreetAddress3 = (string)row["Street_Address_3"];		// NVarChar(128) NULL
					if (row["Department"] != DBNull.Value) item.Department = (string)row["Department"];						// NVarChar(128) NULL
					if (row["Directions"] != DBNull.Value) item.Directions = (string)row["Directions"];						// NVarChar(128) NULL
					if (row["ZIP_Post_Code"] != DBNull.Value) item.ZipPostCode = (string)row["ZIP_Post_Code"];				// VarChar(32) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		/// <summary>
		///		<para>Gets sites or offices of a person that are not marked as headquarter, billing, or shipping address</para>
		/// </summary>
		/// <param name="personID"></param>
		/// <returns></returns>
		public static async Task<List<PersSiteModel>> ListOtherAsync(int personID)
		{
			PersSiteModel item;
			List<PersSiteModel> list = new List<PersSiteModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Pers_Sites_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new PersSiteModel();
					item.ID = (int)row["Site_ID"];																			// Int NOT NULL PRIMARY KEY
					item.PersonID = (int)row["Person_ID"];																	// Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.ContactStatus = (ContactStatus)(byte)row["Contact_Status_ID"];										// TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
					if (row["Site_Name"] != DBNull.Value) item.SiteName = (string)row["Site_Name"];							// NVarChar(128) NULL
					item.Country = (string)row["Country"];																	// Char(3) NOT NULL DEFAULT 'ROM'
					if (row["Region"] != DBNull.Value) item.Region = (string)row["Region"];									// NVarChar(128) NULL
					if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];			// NVarChar(128) NULL
					item.City = (string)row["City"];																		// NVarChar(64) NOT NULL
					if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];		// NVarChar(128) NULL
					if (row["Street_Address_2"] != DBNull.Value) item.StreetAddress2 = (string)row["Street_Address_2"];		// NVarChar(128) NULL
					if (row["Street_Address_3"] != DBNull.Value) item.StreetAddress3 = (string)row["Street_Address_3"];		// NVarChar(128) NULL
					if (row["Department"] != DBNull.Value) item.Department = (string)row["Department"];						// NVarChar(128) NULL
					if (row["Directions"] != DBNull.Value) item.Directions = (string)row["Directions"];						// NVarChar(128) NULL
					if (row["ZIP_Post_Code"] != DBNull.Value) item.ZipPostCode = (string)row["ZIP_Post_Code"];				// VarChar(32) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> MarkForHomeAsync(int persSiteID, int personID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			return await Persons.SetAddressForHomeAsync(personID, persSiteID, updatedBy, updatedOn);
		}
		public static async Task<bool> MarkForBillAsync(int persSiteID, int personID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			return await Persons.SetAddressForBillAsync(personID, persSiteID, updatedBy, updatedOn);
		}
		public static async Task<bool> MarkForShipAsync(int persSiteID, int personID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			return await Persons.SetAddressForShipAsync(personID, persSiteID, updatedBy, updatedOn);
		}
	}
}
