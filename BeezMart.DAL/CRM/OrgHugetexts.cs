﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.CRM
{
	public static class OrgHugetexts
    {
		public static async Task<bool> SaveAsync(OrgHugetextModel orgHugetext)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (orgHugetext.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgHugetext.ID.Value;			// BigInt
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgHugetext.FieldID;			// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NText).Value = orgHugetext.Value;				// NVarChar(MAX)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgHugetext.UpdatedOn;	// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgHugetext.UpdatedBy;	// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_HugeText_Fields_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;	// BigInt OUTPUT
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = orgHugetext.OrganizationID;	// Int
					db.AddParam("@FieldID", SqlDbType.SmallInt).Value = orgHugetext.FieldID;			// SmallInt
					db.AddParam("@FieldValue", SqlDbType.NText).Value = orgHugetext.Value;				// NVarChar(MAX)
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = orgHugetext.UpdatedOn;	// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = orgHugetext.UpdatedBy;	// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("CRM.Orgs_HugeText_Fields_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) orgHugetext.ID = (long)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}
		public static async Task<OrgHugetextModel?> RetrieveAsync(long orgHugetextID)
		{
			OrgHugetextModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgHugetextID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_HugeText_Fields_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new OrgHugetextModel();
					entity.ID = (long)row["Record_ID"];																// BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					entity.OrganizationID = (int)row["Organization_ID"];											// Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					entity.FieldID = (short)row["Field_ID"];														// SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) entity.Category = (string)row["Field_Category"];		// NVarChar(64) NULL
					entity.Name = (string)row["Field_Name"];														// NVarChar(64) NOT NULL
					entity.Value = (string)row["Field_Value"];														// NVarChar(MAX) NOT NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<List<OrgHugetextModel>> ListAsync(int organizationID)
		{
			OrgHugetextModel item;
			List<OrgHugetextModel> list = new List<OrgHugetextModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("CRM.Orgs_HugeText_Fields_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new OrgHugetextModel();
					item.ID = (long)row["Record_ID"];															//			-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
					item.OrganizationID = (int)row["Organization_ID"];											//			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					item.FieldID = (short)row["Field_ID"];														//			-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
					if (row["Field_Category"] != DBNull.Value) item.Category = (string)row["Field_Category"];	//			-- NVarChar(64) NULL
					item.Name = (string)row["Field_Name"];														//			-- NVarChar(64) NOT NULL
					item.Value = (string)row["Field_Value"];													//			-- NVarChar(MAX) NOT NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];												//			-- SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];			//			-- NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(long orgHugetextID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = orgHugetextID;
				if ((await db.DeleteRecordAsync("CRM.Orgs_HugeText_Fields_Del")) > 0) success = true;
			}
			return success;
		}
		/// <summary>
		///		<para>Gets the available categories of hugetext fields.</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> ListCategoriesAsync()
		{
			return await OrgFields.ListCategoriesAsync(FieldType.HugeText);
		}
		/// <summary>
		///		<para>Gets the available hugetext types (or names).</para>
		///		<para>Non-cached.</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<OrgFieldModel>> ListAvailableFieldNamesAsync()
		{
			return await OrgFields.ListAsync(FieldType.HugeText);
		}
	}
}
