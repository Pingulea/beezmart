﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class InvoiceItems
    {
		public static async Task<bool> SaveAsync(InvoiceItemModel invoiceItem)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (invoiceItem.ID.HasValue)
				{
					db.AddParam("@ItemID", SqlDbType.BigInt).Value = invoiceItem.ID.Value;														// BigInt
					db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoiceItem.InvoiceID;														// Int
					db.AddParam("@ProductCatalogItemID", SqlDbType.Int).Value = invoiceItem.ProductCatalogItemID;   							// Int = NULL
					db.AddParam("@ProductCatalogCategoryID", SqlDbType.SmallInt).Value = invoiceItem.ProductCatalogCategoryID;		        	// SmallInt = NULL
					db.AddParam("@ProductOrService", SqlDbType.Char).Value = invoiceItem.ProductOrService;										// Char = 'P'	-- P for products, S for services
					db.AddParam("@SalesOrderID", SqlDbType.Int).Value = invoiceItem.SalesOrderID;										       	// Int = NULL
					db.AddParam("@SalesOrderItemID", SqlDbType.BigInt).Value = invoiceItem.SalesOrderItemID;									// BigInt = NULL
					db.AddParam("@DeliveryDocumentID", SqlDbType.Int).Value = invoiceItem.DeliveryDocumentID;							    	// Int = NULL
					db.AddParam("@DeliveryDocumentItemID", SqlDbType.BigInt).Value = invoiceItem.DeliveryDocumentItemID;						// BigInt = NULL
					db.AddParam("@ItemName", SqlDbType.NVarChar, 1024).Value = invoiceItem.Name;												// NVarChar(1024)
					db.AddParam("@UnitPrice", SqlDbType.Decimal).Value = invoiceItem.UnitPrice;													// Decimal(16, 8) = 0
					db.AddParam("@UnitMeasure", SqlDbType.NVarChar, 16).Value = invoiceItem.UnitMeasure;										// NVarChar(16) = NULL
					db.AddParam("@Quantity", SqlDbType.Int).Value = invoiceItem.Quantity;														// Int = 1
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = invoiceItem.UpdatedOn;											// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = invoiceItem.UpdatedBy;											// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Invoice_Items_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@ItemID", SqlDbType.BigInt).Direction = ParameterDirection.Output;												// BigInt
					db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoiceItem.InvoiceID;														// Int
					db.AddParam("@ProductCatalogItemID", SqlDbType.Int).Value = invoiceItem.ProductCatalogItemID;	    						// Int = NULL
					db.AddParam("@ProductCatalogCategoryID", SqlDbType.SmallInt).Value = invoiceItem.ProductCatalogCategoryID;		        	// SmallInt = NULL
					db.AddParam("@ProductOrService", SqlDbType.Char).Value = invoiceItem.ProductOrService;										// Char = 'P'	-- P for products, S for services
					db.AddParam("@SalesOrderID", SqlDbType.Int).Value = invoiceItem.SalesOrderID;											    // Int = NULL
					db.AddParam("@SalesOrderItemID", SqlDbType.BigInt).Value = invoiceItem.SalesOrderItemID;									// BigInt = NULL
					db.AddParam("@DeliveryDocumentID", SqlDbType.Int).Value = invoiceItem.DeliveryDocumentID;							    	// Int = NULL
					db.AddParam("@DeliveryDocumentItemID", SqlDbType.BigInt).Value = invoiceItem.DeliveryDocumentItemID;						// BigInt = NULL
					db.AddParam("@ItemName", SqlDbType.NVarChar, 1024).Value = invoiceItem.Name;												// NVarChar(1024)
					db.AddParam("@UnitPrice", SqlDbType.Decimal).Value = invoiceItem.UnitPrice;													// Decimal(16, 8) = 0
					db.AddParam("@UnitMeasure", SqlDbType.NVarChar, 16).Value = invoiceItem.UnitMeasure;										// NVarChar(16) = NULL
					db.AddParam("@Quantity", SqlDbType.Int).Value = invoiceItem.Quantity;														// Int = 1
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = invoiceItem.UpdatedOn;											// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = invoiceItem.UpdatedBy;											// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Invoice_Items_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) invoiceItem.ID = (long)db.Params["@ItemID"].Value;
				}
			}
			return success;
		}
		public static async Task<InvoiceItemModel?> RetrieveAsync(long invoiceItemID)
		{
			InvoiceItemModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@ItemID", SqlDbType.BigInt).Value = invoiceItemID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoice_Items_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new InvoiceItemModel();
					entity.ID = (long)row["Item_ID"];																										// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					entity.InvoiceID = (int)row["Invoice_ID"];																								// Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Product_Catalog_Item_ID"] != DBNull.Value) entity.ProductCatalogItemID = (int)row["Product_Catalog_Item_ID"];					// Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
					if (row["Product_Catalog_Category_ID"] != DBNull.Value) entity.ProductCatalogCategoryID = (short)row["Product_Catalog_Category_ID"];	// SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
					if (row["Product_Or_Service"] != DBNull.Value) entity.ProductOrService = ((string)row["Product_Or_Service"])[0];						// Char NULL DEFAULT 'P'			-- P for products, S for services
					if (row["Sales_Order_ID"] != DBNull.Value) entity.SalesOrderID = (int)row["Sales_Order_ID"];											// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Sales_Order_Item_ID"] != DBNull.Value) entity.SalesOrderItemID = (long)row["Sales_Order_Item_ID"];								// BigInt NULL FOREIGN KEY REFERENCES Sales.Order_Items(Item_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) entity.DeliveryDocumentID = (int)row["Delivery_Document_ID"];							// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Delivery_Document_Item_ID"] != DBNull.Value) entity.DeliveryDocumentItemID = (long)row["Delivery_Document_Item_ID"];			// BigInt NULL FOREIGN KEY REFERENCES Delivery.Document_Items(Item_ID)
					entity.Name = (string)row["Item_Name"];																									// NVarChar(1024) NOT NULL
					entity.UnitPrice = (decimal)row["Unit_Price"];																							// DECIMAL(16, 8) NOT NULL DEFAULT 0
					if (row["Unit_Measure"] != DBNull.Value) entity.UnitMeasure = (string)row["Unit_Measure"];												// NVarChar(16) NULL
					entity.Quantity = (int)row["Quantity"];																									// Int NOT NULL DEFAULT 1
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		/// <summary>
		///		<para>Gets the products or services from an invoice</para>
		/// </summary>
		/// <param name="invoiceID"></param>
		/// <returns>The list of InvoiceItems associated with an invoice</returns>
		public static async Task<List<InvoiceItemModel>> ListAsync(int invoiceID)
		{
			InvoiceItemModel item;
			List<InvoiceItemModel> list = new List<InvoiceItemModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoiceID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoice_Items_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new InvoiceItemModel();
					item.ID = (long)row["Item_ID"];																											// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					item.InvoiceID = (int)row["Invoice_ID"];																								// Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Product_Catalog_Item_ID"] != DBNull.Value) item.ProductCatalogItemID = (int)row["Product_Catalog_Item_ID"];					// Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
					if (row["Product_Catalog_Category_ID"] != DBNull.Value) item.ProductCatalogCategoryID = (short)row["Product_Catalog_Category_ID"];		// SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
					if (row["Product_Or_Service"] != DBNull.Value) item.ProductOrService = ((string)row["Product_Or_Service"])[0]; 							// Char NULL DEFAULT 'P'			-- P for products, S for services
					if (row["Sales_Order_ID"] != DBNull.Value) item.SalesOrderID = (int)row["Sales_Order_ID"];												// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Sales_Order_Item_ID"] != DBNull.Value) item.SalesOrderItemID = (long)row["Sales_Order_Item_ID"];								// BigInt NULL FOREIGN KEY REFERENCES Sales.Order_Items(Item_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) item.DeliveryDocumentID = (int)row["Delivery_Document_ID"];							// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Delivery_Document_Item_ID"] != DBNull.Value) item.DeliveryDocumentItemID = (long)row["Delivery_Document_Item_ID"];				// BigInt NULL FOREIGN KEY REFERENCES Delivery.Document_Items(Item_ID)
					item.Name = (string)row["Item_Name"];																									// NVarChar(1024) NOT NULL
					item.UnitPrice = (decimal)row["Unit_Price"];																							// DECIMAL(16, 8) NOT NULL DEFAULT 0
					if (row["Unit_Measure"] != DBNull.Value) item.UnitMeasure = (string)row["Unit_Measure"];												// NVarChar(16) NULL
					item.Quantity = (int)row["Quantity"];																									// Int NOT NULL DEFAULT 1
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(long invoiceItemID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@ItemID", SqlDbType.BigInt).Value = invoiceItemID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.DeleteRecordAsync("Billing.Invoice_Items_Del")) > 0) success = true;
			}
			return success;
		}
	}
}
