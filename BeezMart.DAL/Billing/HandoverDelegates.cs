﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class HandoverDelegates
    {
		public static async Task<bool> SaveAsync(HandoverDelegateModel handoverDelegate)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (handoverDelegate.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.SmallInt).Value = handoverDelegate.ID.Value;					// SmallInt
					db.AddParam("@DelegateName", SqlDbType.NVarChar, 128).Value = handoverDelegate.Name;			// NVarChar(128)
					db.AddParam("@DelegateID1", SqlDbType.NVarChar, 8).Value = handoverDelegate.IdField1;			// NVarChar(8) = NULL
					db.AddParam("@DelegateID2", SqlDbType.NVarChar, 16).Value = handoverDelegate.IdField2;			// NVarChar(16) = NULL
					db.AddParam("@DelegateID3", SqlDbType.NVarChar, 32).Value = handoverDelegate.IdField3;			// NVarChar(32) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = handoverDelegate.UpdatedOn;			// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = handoverDelegate.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Handover_Delegates_Upd_1")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;				// SmallInt
					db.AddParam("@DelegateName", SqlDbType.NVarChar, 128).Value = handoverDelegate.Name;			// NVarChar(128)
					db.AddParam("@DelegateID1", SqlDbType.NVarChar, 8).Value = handoverDelegate.IdField1;			// NVarChar(8) = NULL
					db.AddParam("@DelegateID2", SqlDbType.NVarChar, 16).Value = handoverDelegate.IdField2;			// NVarChar(16) = NULL
					db.AddParam("@DelegateID3", SqlDbType.NVarChar, 32).Value = handoverDelegate.IdField3;			// NVarChar(32) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = handoverDelegate.UpdatedOn;			// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = handoverDelegate.UpdatedBy;			// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Handover_Delegates_Ins_1", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) handoverDelegate.ID = (short)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}
		public static async Task<HandoverDelegateModel?> RetrieveAsync(short handoverDelegateID)
		{
			HandoverDelegateModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.SmallInt).Value = handoverDelegateID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Handover_Delegates_Sel_1");
				if (await rdrReader.ReadAsync())
				{
					entity = new HandoverDelegateModel();
					entity.ID = (short)row["Record_ID"];															// Int NOT NULL PRIMARY KEY
					entity.Name = (string)row["Delegate_Name"];														// NVarChar(128) NOT NULL
					if (row["Delegate_ID_1"] != DBNull.Value) entity.IdField1 = (string)row["Delegate_ID_1"];		// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) entity.IdField2 = (string)row["Delegate_ID_2"];		// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) entity.IdField3 = (string)row["Delegate_ID_3"];		// NVarChar(32) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];			// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<List<HandoverDelegateModel>> ListAsync()
		{
			HandoverDelegateModel item;
			List<HandoverDelegateModel> list = new List<HandoverDelegateModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Handover_Delegates_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new HandoverDelegateModel();
					item.ID = (short)row["Record_ID"];																// Int NOT NULL PRIMARY KEY
					item.Name = (string)row["Delegate_Name"];														// NVarChar(128) NOT NULL
					if (row["Delegate_ID_1"] != DBNull.Value) item.IdField1 = (string)row["Delegate_ID_1"];			// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) item.IdField2 = (string)row["Delegate_ID_2"];			// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) item.IdField3 = (string)row["Delegate_ID_3"];			// NVarChar(32) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];													// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(short handoverDelegateID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.SmallInt).Value = handoverDelegateID;
				if ((await db.DeleteRecordAsync("Billing.Handover_Delegates_Del_1")) > 0) success = true;
			}
			return success;
		}
	}
}
