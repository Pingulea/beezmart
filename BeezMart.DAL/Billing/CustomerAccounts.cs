﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class CustomerAccounts
	{
		public static async Task<bool> SaveAsync(CustomerAccountModel customerAccount)
		{
			if (!(customerAccount.OrganizationID.HasValue || customerAccount.PersonID.HasValue))
			{
				throw new Exception("A billing account must be associated to either an organization or person. Both OrganizationID and PersonID properties of this instance are NULL.");
			}
			bool success = false;
			using (Database db = new Database())
			{
				if (customerAccount.ID.HasValue)
				{
					db.AddParam("@AccountID", SqlDbType.Int).Value = customerAccount.ID.Value;                                              // Int
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = customerAccount.OrganizationID;                                   // Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = customerAccount.PersonID;                                               // Int = NULL
					db.AddParam("@IBAN", SqlDbType.VarChar, 32).Value = CustomerAccountModel.FormatAccountNumber(customerAccount.IBAN);     // VarChar(32)
					db.AddParam("@Bank", SqlDbType.NVarChar, 128).Value = customerAccount.Bank;                                             // NVarChar(128)
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = customerAccount.Currency;                                           // Char(3) = 'EUR'
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = customerAccount.UpdatedOn;                                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = customerAccount.UpdatedBy;                                   // NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Customer_Accounts_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@AccountID", SqlDbType.Int).Direction = ParameterDirection.Output;                                         // Int
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = customerAccount.OrganizationID;                                   // Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = customerAccount.PersonID;                                               // Int = NULL
					db.AddParam("@IBAN", SqlDbType.VarChar, 32).Value = CustomerAccountModel.FormatAccountNumber(customerAccount.IBAN);     // VarChar(32)
					db.AddParam("@Bank", SqlDbType.NVarChar, 128).Value = customerAccount.Bank;                                             // NVarChar(128)
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = customerAccount.Currency;                                           // Char(3) = 'EUR'
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = customerAccount.UpdatedOn;                                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = customerAccount.UpdatedBy;                                   // NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Customer_Accounts_Upsert", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) customerAccount.ID = (int)db.Params["@AccountID"].Value;
				}
			}
			return success;
		}
		public static async Task<CustomerAccountModel?> RetrieveAsync(int accountID)
		{
			CustomerAccountModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@AccountID", SqlDbType.Int).Value = accountID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Accounts_Sel");
				if ((await rdrReader.ReadAsync()))
				{
					entity = new CustomerAccountModel();
					entity.ID = (int)row["Account_ID"];                                                                         // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];            // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];                              // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.IBAN = (string)row["IBAN"];                                                                          // VarChar(32) NOT NULL
					entity.Bank = (string)row["Bank"];                                                                          // NVarChar(128) NOT NULL
					entity.Currency = (string)row["Currency"];																	// Char(3) NOT NULL DEFAULT 'EUR'
					entity.UpdatedOn = (DateTime)row["Updated_On"];                                                             // SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];                        // NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		/// <summary>
		///		<para>Gets all bank accounts of an organization. If Currency parameter is specified, accounts will be filtered by currency.</para>
		/// </summary>
		/// <param name="organizationID"></param>
		/// <param name="currency">Example: RON, EUR, USD</param>
		/// <returns></returns>
		public static async Task<List<CustomerAccountModel>> ListOrganizationAccountsAsync(int organizationID, string? currency = null)
		{
			CustomerAccountModel item;
			List<CustomerAccountModel> list = new List<CustomerAccountModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Accounts_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new CustomerAccountModel();
					item.ID = (int)row["Account_ID"];                                                                   // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];      // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                        // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.IBAN = (string)row["IBAN"];                                                                    // VarChar(32) NOT NULL
					item.Bank = (string)row["Bank"];                                                                    // NVarChar(128) NOT NULL
					item.Currency = (string)row["Currency"];					                                        // Char(3) NOT NULL DEFAULT 'EUR'
					item.UpdatedOn = (DateTime)row["Updated_On"];                                                       // SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                  // NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		/// <summary>
		///		<para>Gets all bank accounts of a person. If Currency parameter is specified, accounts will be filtered by currency.</para>
		/// </summary>
		/// <param name="OrganizationID"></param>
		/// <param name="currency">Example: RON, EUR, USD</param>
		/// <returns></returns>
		public static async Task<List<CustomerAccountModel>> ListPersonAccountsAsync(int personID, string? currency = null)
		{
			CustomerAccountModel item;
			List<CustomerAccountModel> list = new List<CustomerAccountModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Accounts_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new CustomerAccountModel();
					item.ID = (int)row["Account_ID"];                                                                   // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];      // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                        // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.IBAN = (string)row["IBAN"];                                                                    // VarChar(32) NOT NULL
					item.Bank = (string)row["Bank"];                                                                    // NVarChar(128) NOT NULL
					item.Currency = (string)row["Currency"];			                                                // Char(3) NOT NULL DEFAULT 'EUR'
					item.UpdatedOn = (DateTime)row["Updated_On"];                                                       // SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                  // NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
	}
}
