﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class PaymentChecks
    {
		public static async Task<bool> SaveAsync(PaymentCheckModel check)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (check.ID.HasValue)
				{
					db.AddParam("@CheckID", SqlDbType.Int).Value = check.ID.Value;							// Int
					db.AddParam("@Reference", SqlDbType.NVarChar, 128).Value = check.Reference;				// NVarChar(128)
					db.AddParam("@Amount", SqlDbType.Decimal).Value = check.Amount;							// Decimal(24, 8) = 0
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = check.Currency;						// Char(3) = 'EUR'
					db.AddParam("@DueDate", SqlDbType.SmallDateTime).Value = check.DueDate;					// SmallDateTime
					db.AddParam("@IssueDate", SqlDbType.SmallDateTime).Value = check.IssueDate;				// SmallDateTime
					db.AddParam("@CashedDate", SqlDbType.SmallDateTime).Value = check.CachedInDate;			// SmallDateTime = NULL
					db.AddParam("@Comments", SqlDbType.NText).Value = check.Comments;						// NVarChar(MAX) = NULL
					db.AddParam("@CheckType", SqlDbType.VarChar, 8).Value = check.CheckType;				// VarChar(8)
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = check.CustomerName;		// NVarChar(256)
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = check.OrganizationID;				// Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = check.PersonID;							// Int = NULL
					db.AddParam("@BankName", SqlDbType.NVarChar, 128).Value = check.BankName;				// NVarChar(128) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = check.UpdatedOn;				// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = check.UpdatedBy;				// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Payment_Checks_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@CheckID", SqlDbType.Int).Direction = ParameterDirection.Output;			// Int
					db.AddParam("@Reference", SqlDbType.NVarChar, 128).Value = check.Reference;				// NVarChar(128)
					db.AddParam("@Amount", SqlDbType.Decimal).Value = check.Amount;							// Decimal(24, 8) = 0
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = check.Currency;						// Char(3) = 'EUR'
					db.AddParam("@DueDate", SqlDbType.SmallDateTime).Value = check.DueDate;					// SmallDateTime
					db.AddParam("@IssueDate", SqlDbType.SmallDateTime).Value = check.IssueDate;				// SmallDateTime
					db.AddParam("@CashedDate", SqlDbType.SmallDateTime).Value = check.CachedInDate;			// SmallDateTime = NULL
					db.AddParam("@Comments", SqlDbType.NText).Value = check.Comments;						// NVarChar(MAX) = NULL
					db.AddParam("@CheckType", SqlDbType.VarChar, 8).Value = check.CheckType;				// VarChar(8)
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = check.CustomerName;		// NVarChar(256)
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = check.OrganizationID;				// Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = check.PersonID;							// Int = NULL
					db.AddParam("@BankName", SqlDbType.NVarChar, 128).Value = check.BankName;				// NVarChar(128) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = check.UpdatedOn;				// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = check.UpdatedBy;				// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Payment_Checks_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) check.ID = (int)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}
		public static async Task<PaymentCheckModel?> RetrieveAsync(int checkID)
		{
			PaymentCheckModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@CheckID", SqlDbType.Int).Value = checkID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Payment_Checks_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PaymentCheckModel();
					entity.ID = (int)row["Check_ID"];																		// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					entity.Reference = (string)row["Reference"];															// NVarChar(128) NOT NULL
					entity.Amount = (decimal)row["Amount"];																	// Decimal(24, 8) NOT NULL DEFAULT 0
					entity.Currency = (string)row["Currency"];																// Char(3) NOT NULL DEFAULT 'EUR'
					entity.DueDate = (DateTime)row["Due_Date"];																// SmallDateTime NOT NULL
					entity.IssueDate = (DateTime)row["Issue_Date"];															// SmallDateTime NOT NULL
					if (row["Cashed_Date"] != DBNull.Value) entity.CachedInDate = (DateTime)row["Cashed_Date"];				// SmallDateTime NULL
					if (row["Comments"] != DBNull.Value) entity.Comments = (string)row["Comments"];							// NVarChar(MAX) NULL
					entity.CheckType = (string)row["Check_Type"];															// VarChar(8) NOT NULL
					entity.CustomerName = (string)row["Customer_Name"];														// NVarChar(256) NOT NULL
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];		// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];							// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Bank_Name"] != DBNull.Value) entity.BankName = (string)row["Bank_Name"];						// NVarChar(128) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != null) entity.UpdatedBy = (string)row["Updated_By"];							// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		/// <summary>
		///		<para>Gets the list of payment checks that haven't been cached in yet</para>
		/// </summary>
		/// <returns></returns>
		public static async Task<List<PaymentCheckModel>> ListPendingAsync()
		{
			PaymentCheckModel item;
			List<PaymentCheckModel> list = new List<PaymentCheckModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Payment_Checks_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PaymentCheckModel();
					item.ID = (int)row["Check_ID"];																		// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.Reference = (string)row["Reference"];															// NVarChar(128) NOT NULL
					item.Amount = (decimal)row["Amount"];																// Decimal(24, 8) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];															// Char(3) NOT NULL DEFAULT 'EUR'
					item.DueDate = (DateTime)row["Due_Date"];															// SmallDateTime NOT NULL
					item.IssueDate = (DateTime)row["Issue_Date"];														// SmallDateTime NOT NULL
					if (row["Cashed_Date"] != DBNull.Value) item.CachedInDate = (DateTime)row["Cashed_Date"];			// SmallDateTime NULL
					if (row["Comments"] != DBNull.Value) item.Comments = (string)row["Comments"];						// NVarChar(MAX) NULL
					item.CheckType = (string)row["Check_Type"];															// VarChar(8) NOT NULL
					item.CustomerName = (string)row["Customer_Name"];													// NVarChar(256) NOT NULL
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];		// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];						// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Bank_Name"] != DBNull.Value) item.BankName = (string)row["Bank_Name"];						// NVarChar(128) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];														// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != null) item.UpdatedBy = (string)row["Updated_By"];							// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
	}
}
