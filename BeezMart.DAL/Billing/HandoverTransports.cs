﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class HandoverTransports
    {
		public static async Task<bool> SaveAsync(HandoverTransportModel handoverTransport)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (handoverTransport.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.SmallInt).Value = handoverTransport.ID.Value;					// SmallInt
					db.AddParam("@Means", SqlDbType.NVarChar, 16).Value = handoverTransport.Means;						// NVarChar(16)
					db.AddParam("@Registration", SqlDbType.NVarChar, 16).Value = handoverTransport.Registration;		// NVarChar(16) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = handoverTransport.UpdatedOn;				// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = handoverTransport.UpdatedBy;				// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Handover_Transport_Upd_1")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;					// SmallInt
					db.AddParam("@Means", SqlDbType.NVarChar, 16).Value = handoverTransport.Means;						// NVarChar(16)
					db.AddParam("@Registration", SqlDbType.NVarChar, 16).Value = handoverTransport.Registration;		// NVarChar(16) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = handoverTransport.UpdatedOn;				// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = handoverTransport.UpdatedBy;				// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Handover_Transport_Ins_1", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) handoverTransport.ID = (short)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}
		public static async Task<HandoverTransportModel?> RetrieveAsync(short recordID)
		{
			HandoverTransportModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.SmallInt).Value = recordID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Handover_Transport_Sel_1");
				if (await rdrReader.ReadAsync())
				{
					entity = new HandoverTransportModel();
					entity.ID = (short)row["Record_ID"];																// Int NOT NULL PRIMARY KEY
					entity.Means = (string)row["Means"];																// NVarChar(16) NOT NULL
					if (row["Registration"] != DBNull.Value) entity.Registration = (string)row["Registration"];			// NVarChar(16) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];														// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];				// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<List<HandoverTransportModel>> ListAsync()
		{
			HandoverTransportModel item;
			List<HandoverTransportModel> list = new List<HandoverTransportModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Handover_Transport_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new HandoverTransportModel();
					item.ID = (short)row["Record_ID"];																	// Int NOT NULL PRIMARY KEY
					item.Means = (string)row["Means"];																	// NVarChar(16) NOT NULL
					if (row["Registration"] != DBNull.Value) item.Registration = (string)row["Registration"];			// NVarChar(16) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];														// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];					// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(short recordID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.SmallInt).Value = recordID;
				if ((await db.DeleteRecordAsync("Billing.Handover_Transport_Del_1")) > 0) success = true;
			}
			return success;
		}
	}
}
