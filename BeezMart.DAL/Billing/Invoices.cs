﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class Invoices
    {
		public static async Task<bool> SaveAsync(InvoiceModel invoice)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (invoice.ID.HasValue)
				{
					db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoice.ID.Value;												// Int
					db.AddParam("@IssueDate", SqlDbType.SmallDateTime).Value = invoice.IssueDate;									// SmallDateTime = NULL
					db.AddParam("@DueDate", SqlDbType.SmallDateTime).Value = invoice.DueDate;										// SmallDateTime = NULL
					db.AddParam("@ClosingDate", SqlDbType.SmallDateTime).Value = invoice.ClosingDate;								// SmallDateTime = NULL
					db.AddParam("@AccountingID", SqlDbType.VarChar, 32).Value = invoice.AccountingID;								// VarChar(32) = NULL
					db.AddParam("@VATPercentage", SqlDbType.Decimal).Value = invoice.VATpercentage;									// Decimal(6, 4) = 0
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = invoice.Currency?.ToUpper();								// Char(3) = 'EUR'
					db.AddParam("@InvoiceTypeID", SqlDbType.TinyInt).Value = (byte)invoice.Type;									// TinyInt
					db.AddParam("@InvoiceStatusID", SqlDbType.TinyInt).Value = (byte)invoice.Status;								// TinyInt
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = invoice.OrganizationID;									// Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = invoice.PersonID;												// Int = NULL
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = invoice.CustomerName;								// NVarChar(256)
					db.AddParam("@CustomerCity", SqlDbType.NVarChar, 64).Value = invoice.CustomerCity;								// NVarChar(64) = NULL
					db.AddParam("@CustomerProvince", SqlDbType.NVarChar, 64).Value = invoice.CustomerProvince;						// NVarChar(64) = NULL
					db.AddParam("@CustomerAddress", SqlDbType.NVarChar, 256).Value = invoice.CustomerAddress;						// NVarChar(256) = NULL
					db.AddParam("@CustomerRegistrationNumber", SqlDbType.VarChar, 32).Value = invoice.CustomerRegistrationNumber;	// VarChar(32) = NULL
					db.AddParam("@CustomerTaxID", SqlDbType.VarChar, 16).Value = invoice.CustomerTaxID;								// VarChar(16) = NULL
					db.AddParam("@CustomerBankName", SqlDbType.NVarChar, 128).Value = invoice.CustomerBankName;						// NVarChar(128) = NULL
					db.AddParam("@CustomerBankAccount", SqlDbType.VarChar, 64).Value = invoice.CustomerBankAccount;					// VarChar(64) = NULL
					db.AddParam("@DelegateName", SqlDbType.NVarChar, 128).Value = invoice.DelegateName;								// NVarChar(128) = NULL
					db.AddParam("@DelegateID1", SqlDbType.NVarChar, 8).Value = invoice.DelegateID1;									// NVarChar(8) = NULL
					db.AddParam("@DelegateID2", SqlDbType.NVarChar, 16).Value = invoice.DelegateID2;								// NVarChar(16) = NULL
					db.AddParam("@DelegateID3", SqlDbType.NVarChar, 32).Value = invoice.DelegateID3;								// NVarChar(32) = NULL
					db.AddParam("@TransportName", SqlDbType.NVarChar, 32).Value = invoice.TransportName;							// NVarChar(32) = NULL
					db.AddParam("@TransportDetail1", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail1;						// NVarChar(32) = NULL
					db.AddParam("@TransportDetail2", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail2;						// NVarChar(32) = NULL
					db.AddParam("@TransportDetail3", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail3;						// NVarChar(32) = NULL
					db.AddParam("@TransportDetail4", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail4;						// NVarChar(32) = NULL
					db.AddParam("@InvoiceDetail1", SqlDbType.NVarChar, 256).Value = invoice.Detail1;								// NVarChar(256) = NULL
					db.AddParam("@InvoiceDetail2", SqlDbType.NVarChar, 256).Value = invoice.Detail2;								// NVarChar(256) = NULL
					db.AddParam("@InvoiceDetail3", SqlDbType.NVarChar, 256).Value = invoice.Detail3;								// NVarChar(256) = NULL
					db.AddParam("@InvoiceDetail4", SqlDbType.NVarChar, 256).Value = invoice.Detail4;								// NVarChar(256) = NULL
					db.AddParam("@SalesOrderID", SqlDbType.Int).Value = invoice.SalesOrderID;										// Int = NULL
					db.AddParam("@DeliveryDocumentID", SqlDbType.Int).Value = invoice.DeliveryDocumentID;							// Int = NULL
					db.AddParam("@RelatedInvoiceID", SqlDbType.Int).Value = invoice.RelatedInvoiceID;								// Int = NULL
					db.AddParam("@RenderingLayout", SqlDbType.VarChar, 256).Value = invoice.RenderingLayout;						// VarChar(256) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = invoice.UpdatedOn;									// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = invoice.UpdatedBy;									// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Invoices_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@InvoiceID", SqlDbType.Int).Direction = ParameterDirection.Output;									// Int
					db.AddParam("@IssueDate", SqlDbType.SmallDateTime).Value = invoice.IssueDate;									// SmallDateTime = NULL
					db.AddParam("@DueDate", SqlDbType.SmallDateTime).Value = invoice.DueDate;										// SmallDateTime = NULL
					db.AddParam("@ClosingDate", SqlDbType.SmallDateTime).Value = invoice.ClosingDate;								// SmallDateTime = NULL
					db.AddParam("@AccountingID", SqlDbType.VarChar, 32).Value = invoice.AccountingID;								// VarChar(32) = NULL
					db.AddParam("@VATPercentage", SqlDbType.Decimal).Value = invoice.VATpercentage;									// Decimal(6, 4) = 0
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = invoice.Currency?.ToUpper();								// Char(3) = 'EUR'
					db.AddParam("@InvoiceTypeID", SqlDbType.TinyInt).Value = (byte)invoice.Type;									// TinyInt
					db.AddParam("@InvoiceStatusID", SqlDbType.TinyInt).Value = (byte)invoice.Status;								// TinyInt
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = invoice.OrganizationID;									// Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = invoice.PersonID;												// Int = NULL
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = invoice.CustomerName;								// NVarChar(256)
					db.AddParam("@CustomerCity", SqlDbType.NVarChar, 64).Value = invoice.CustomerCity;								// NVarChar(64) = NULL
					db.AddParam("@CustomerProvince", SqlDbType.NVarChar, 64).Value = invoice.CustomerProvince;						// NVarChar(64) = NULL
					db.AddParam("@CustomerAddress", SqlDbType.NVarChar, 256).Value = invoice.CustomerAddress;						// NVarChar(256) = NULL
					db.AddParam("@CustomerRegistrationNumber", SqlDbType.VarChar, 32).Value = invoice.CustomerRegistrationNumber;	// VarChar(32) = NULL
					db.AddParam("@CustomerTaxID", SqlDbType.VarChar, 16).Value = invoice.CustomerTaxID;								// VarChar(16) = NULL
					db.AddParam("@CustomerBankName", SqlDbType.NVarChar, 128).Value = invoice.CustomerBankName;						// NVarChar(128) = NULL
					db.AddParam("@CustomerBankAccount", SqlDbType.VarChar, 64).Value = invoice.CustomerBankAccount;					// VarChar(64) = NULL
					db.AddParam("@DelegateName", SqlDbType.NVarChar, 128).Value = invoice.DelegateName;								// NVarChar(128) = NULL
					db.AddParam("@DelegateID1", SqlDbType.NVarChar, 8).Value = invoice.DelegateID1;									// NVarChar(8) = NULL
					db.AddParam("@DelegateID2", SqlDbType.NVarChar, 16).Value = invoice.DelegateID2;								// NVarChar(16) = NULL
					db.AddParam("@DelegateID3", SqlDbType.NVarChar, 32).Value = invoice.DelegateID3;								// NVarChar(32) = NULL
					db.AddParam("@TransportName", SqlDbType.NVarChar, 32).Value = invoice.TransportName;							// NVarChar(32) = NULL
					db.AddParam("@TransportDetail1", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail1;						// NVarChar(32) = NULL
					db.AddParam("@TransportDetail2", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail2;						// NVarChar(32) = NULL
					db.AddParam("@TransportDetail3", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail3;						// NVarChar(32) = NULL
					db.AddParam("@TransportDetail4", SqlDbType.NVarChar, 32).Value = invoice.TransportDetail4;						// NVarChar(32) = NULL
					db.AddParam("@InvoiceDetail1", SqlDbType.NVarChar, 256).Value = invoice.Detail1;								// NVarChar(256) = NULL
					db.AddParam("@InvoiceDetail2", SqlDbType.NVarChar, 256).Value = invoice.Detail2;								// NVarChar(256) = NULL
					db.AddParam("@InvoiceDetail3", SqlDbType.NVarChar, 256).Value = invoice.Detail3;								// NVarChar(256) = NULL
					db.AddParam("@InvoiceDetail4", SqlDbType.NVarChar, 256).Value = invoice.Detail4;								// NVarChar(256) = NULL
					db.AddParam("@SalesOrderID", SqlDbType.Int).Value = invoice.SalesOrderID;										// Int = NULL
					db.AddParam("@DeliveryDocumentID", SqlDbType.Int).Value = invoice.DeliveryDocumentID;							// Int = NULL
					db.AddParam("@RelatedInvoiceID", SqlDbType.Int).Value = invoice.RelatedInvoiceID;								// Int = NULL
					db.AddParam("@RenderingLayout", SqlDbType.VarChar, 256).Value = invoice.RenderingLayout;						// VarChar(256) = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = invoice.UpdatedOn;									// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = invoice.UpdatedBy;									// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Invoices_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) invoice.ID = (int)db.Params["@InvoiceID"].Value;
				}
			}
			return success;
		}
		public static async Task<InvoiceModel?> RetrieveAsync(int invoiceID)
		{
			InvoiceModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoiceID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new InvoiceModel();
					entity.ID = (int)row["Invoice_ID"];																											// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					entity.IssueDate = (DateTime)row["Issue_Date"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Due_Date"] != DBNull.Value) entity.DueDate = (DateTime)row["Due_Date"];															// SmallDateTime NULL
					if (row["Closing_Date"] != DBNull.Value) entity.ClosingDate = (DateTime)row["Closing_Date"];												// SmallDateTime NULL
					if (row["Accounting_ID"] != DBNull.Value) entity.AccountingID = (string)row["Accounting_ID"];												// VarChar(32) NULL
					entity.VATpercentage = (decimal)row["VAT_Percentage"];																						// Decimal(6, 4) NOT NULL DEFAULT 0
					entity.Currency = (string)row["Currency"];																									// Char(3) NOT NULL DEFAULT 'EUR'
					entity.Type = (InvoiceType)(byte)row["Invoice_Type_ID"];																					// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
					entity.Status = (InvoiceStatus)(byte)row["Invoice_Status_ID"];																				// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];											// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];																// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.CustomerName = (string)row["Customer_Name"];																							// NVarChar(256) NOT NULL
					if (row["Customer_City"] != DBNull.Value) entity.CustomerCity = (string)row["Customer_City"];												// NVarChar(64) NULL
					if (row["Customer_Province"] != DBNull.Value) entity.CustomerProvince = (string)row["Customer_Province"];									// NVarChar(64) NULL
					if (row["Customer_Address"] != DBNull.Value) entity.CustomerAddress = (string)row["Customer_Address"];										// NVarChar(256) NULL
					if (row["Customer_Registration_Number"] != DBNull.Value) entity.CustomerRegistrationNumber = (string)row["Customer_Registration_Number"];	// VarChar(32) NULL
					if (row["Customer_Tax_ID"] != DBNull.Value) entity.CustomerTaxID = (string)row["Customer_Tax_ID"];											// VarChar(16) NULL
					if (row["Customer_Bank_Name"] != DBNull.Value) entity.CustomerBankName = (string)row["Customer_Bank_Name"];									// NVarChar(128) NULL
					if (row["Customer_Bank_Account"] != DBNull.Value) entity.CustomerBankAccount = (string)row["Customer_Bank_Account"];						// VarChar(64) NULL
					if (row["Delegate_Name"] != DBNull.Value) entity.DelegateName = (string)row["Delegate_Name"];												// NVarChar(128) NULL
					if (row["Delegate_ID_1"] != DBNull.Value) entity.DelegateID1 = (string)row["Delegate_ID_1"];												// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) entity.DelegateID2 = (string)row["Delegate_ID_2"];												// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) entity.DelegateID3 = (string)row["Delegate_ID_3"];												// NVarChar(32) NULL
					if (row["Transport_Name"] != DBNull.Value) entity.TransportName = (string)row["Transport_Name"];											// NVarChar(32) NULL
					if (row["Transport_Detail_1"] != DBNull.Value) entity.TransportDetail1 = (string)row["Transport_Detail_1"];									// NVarChar(32) NULL
					if (row["Transport_Detail_2"] != DBNull.Value) entity.TransportDetail2 = (string)row["Transport_Detail_2"];									// NVarChar(32) NULL
					if (row["Transport_Detail_3"] != DBNull.Value) entity.TransportDetail3 = (string)row["Transport_Detail_3"];									// NVarChar(32) NULL
					if (row["Transport_Detail_4"] != DBNull.Value) entity.TransportDetail4 = (string)row["Transport_Detail_4"];									// NVarChar(32) NULL
					if (row["Invoice_Detail_1"] != DBNull.Value) entity.Detail1 = (string)row["Invoice_Detail_1"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_2"] != DBNull.Value) entity.Detail2 = (string)row["Invoice_Detail_2"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_3"] != DBNull.Value) entity.Detail3 = (string)row["Invoice_Detail_3"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_4"] != DBNull.Value) entity.Detail4 = (string)row["Invoice_Detail_4"];												// NVarChar(256) NULL
					if (row["Sales_Order_ID"] != DBNull.Value) entity.SalesOrderID = (int)row["Sales_Order_ID"];												// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) entity.DeliveryDocumentID = (int)row["Delivery_Document_ID"];								// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Related_Invoice_ID"] != DBNull.Value) entity.RelatedInvoiceID = (int)row["Related_Invoice_ID"];									// Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Rendering_Layout"] != DBNull.Value) entity.RenderingLayout = (string)row["Rendering_Layout"];										// VarChar(256) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];														// NVarChar(128) NULL
					entity.TotalValue = (decimal)row["Total_Value"];																							// Decimal(27,8)
					entity.TotalPayed = (decimal)row["Total_Payed"];																							// Decimal(27,8)
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<InvoiceModel?> RetrieveLatestHavingAccountingIDAsync()
		{
			InvoiceModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Sel_1");
				if (await rdrReader.ReadAsync())
				{
					entity = new InvoiceModel();
					entity.ID = (int)row["Invoice_ID"];																											// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					entity.IssueDate = (DateTime)row["Issue_Date"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Due_Date"] != DBNull.Value) entity.DueDate = (DateTime)row["Due_Date"];															// SmallDateTime NULL
					if (row["Closing_Date"] != DBNull.Value) entity.ClosingDate = (DateTime)row["Closing_Date"];												// SmallDateTime NULL
					if (row["Accounting_ID"] != DBNull.Value) entity.AccountingID = (string)row["Accounting_ID"];												// VarChar(32) NULL
					entity.VATpercentage = (decimal)row["VAT_Percentage"];																						// Decimal(6, 4) NOT NULL DEFAULT 0
					entity.Currency = (string)row["Currency"];																									// Char(3) NOT NULL DEFAULT 'EUR'
					entity.Type = (InvoiceType)(byte)row["Invoice_Type_ID"];																					// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
					entity.Status = (InvoiceStatus)(byte)row["Invoice_Status_ID"];																				// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];											// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];																// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.CustomerName = (string)row["Customer_Name"];																							// NVarChar(256) NOT NULL
					if (row["Customer_City"] != DBNull.Value) entity.CustomerCity = (string)row["Customer_City"];												// NVarChar(64) NULL
					if (row["Customer_Province"] != DBNull.Value) entity.CustomerProvince = (string)row["Customer_Province"];									// NVarChar(64) NULL
					if (row["Customer_Address"] != DBNull.Value) entity.CustomerAddress = (string)row["Customer_Address"];										// NVarChar(256) NULL
					if (row["Customer_Registration_Number"] != DBNull.Value) entity.CustomerRegistrationNumber = (string)row["Customer_Registration_Number"];	// VarChar(32) NULL
					if (row["Customer_Tax_ID"] != DBNull.Value) entity.CustomerTaxID = (string)row["Customer_Tax_ID"];											// VarChar(16) NULL
					if (row["Customer_Bank_Name"] != DBNull.Value) entity.CustomerBankName = (string)row["Customer_Bank_Name"];									// NVarChar(128) NULL
					if (row["Customer_Bank_Account"] != DBNull.Value) entity.CustomerBankAccount = (string)row["Customer_Bank_Account"];						// VarChar(64) NULL
					if (row["Delegate_Name"] != DBNull.Value) entity.DelegateName = (string)row["Delegate_Name"];												// NVarChar(128) NULL
					if (row["Delegate_ID_1"] != DBNull.Value) entity.DelegateID1 = (string)row["Delegate_ID_1"];												// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) entity.DelegateID2 = (string)row["Delegate_ID_2"];												// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) entity.DelegateID3 = (string)row["Delegate_ID_3"];												// NVarChar(32) NULL
					if (row["Transport_Name"] != DBNull.Value) entity.TransportName = (string)row["Transport_Name"];											// NVarChar(32) NULL
					if (row["Transport_Detail_1"] != DBNull.Value) entity.TransportDetail1 = (string)row["Transport_Detail_1"];									// NVarChar(32) NULL
					if (row["Transport_Detail_2"] != DBNull.Value) entity.TransportDetail2 = (string)row["Transport_Detail_2"];									// NVarChar(32) NULL
					if (row["Transport_Detail_3"] != DBNull.Value) entity.TransportDetail3 = (string)row["Transport_Detail_3"];									// NVarChar(32) NULL
					if (row["Transport_Detail_4"] != DBNull.Value) entity.TransportDetail4 = (string)row["Transport_Detail_4"];									// NVarChar(32) NULL
					if (row["Invoice_Detail_1"] != DBNull.Value) entity.Detail1 = (string)row["Invoice_Detail_1"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_2"] != DBNull.Value) entity.Detail2 = (string)row["Invoice_Detail_2"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_3"] != DBNull.Value) entity.Detail3 = (string)row["Invoice_Detail_3"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_4"] != DBNull.Value) entity.Detail4 = (string)row["Invoice_Detail_4"];												// NVarChar(256) NULL
					if (row["Sales_Order_ID"] != DBNull.Value) entity.SalesOrderID = (int)row["Sales_Order_ID"];												// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) entity.DeliveryDocumentID = (int)row["Delivery_Document_ID"];								// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Related_Invoice_ID"] != DBNull.Value) entity.RelatedInvoiceID = (int)row["Related_Invoice_ID"];									// Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Rendering_Layout"] != DBNull.Value) entity.RenderingLayout = (string)row["Rendering_Layout"];										// VarChar(256) NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];														// NVarChar(128) NULL
					entity.TotalValue = (decimal)row["Total_Value"];																							// Decimal(27,8)
					entity.TotalPayed = (decimal)row["Total_Payed"];																							// Decimal(27,8)
				}
				rdrReader.Dispose();
			}
			return entity;
		}
        public static async Task<List<InvoiceModel>> ListLatestAsync(int maximumRowCount = 1000)
        {
            InvoiceModel item;
            List<InvoiceModel> list = new List<InvoiceModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@MaximumRowCount", SqlDbType.Int).Value = maximumRowCount;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Grid_3");
                while (await rdrReader.ReadAsync())
                {
                    item = new InvoiceModel();
                    item.ID = (int)row["Invoice_ID"];                                                                                                           // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    item.IssueDate = (DateTime)row["Issue_Date"];                                                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Due_Date"] != DBNull.Value) item.DueDate = (DateTime)row["Due_Date"];                                                              // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                                                  // SmallDateTime NULL
                    if (row["Accounting_ID"] != DBNull.Value) item.AccountingID = (string)row["Accounting_ID"];                                                 // VarChar(32) NULL
                    item.VATpercentage = (decimal)row["VAT_Percentage"];                                                                                        // Decimal(6, 4) NOT NULL DEFAULT 0
                    item.Currency = (string)row["Currency"];                                                                                                    // Char(3) NOT NULL DEFAULT 'EUR'
                    item.Type = (InvoiceType)(byte)row["Invoice_Type_ID"];                                                                                      // TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
                    item.Status = (InvoiceStatus)(byte)row["Invoice_Status_ID"];                                                                                // TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                                              // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                                                // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
                    item.CustomerName = (string)row["Customer_Name"];                                                                                           // NVarChar(256) NOT NULL
                    if (row["Customer_City"] != DBNull.Value) item.CustomerCity = (string)row["Customer_City"];                                                 // NVarChar(64) NULL
                    if (row["Customer_Province"] != DBNull.Value) item.CustomerProvince = (string)row["Customer_Province"];                                     // NVarChar(64) NULL
                    if (row["Customer_Address"] != DBNull.Value) item.CustomerAddress = (string)row["Customer_Address"];                                        // NVarChar(256) NULL
                    if (row["Customer_Registration_Number"] != DBNull.Value) item.CustomerRegistrationNumber = (string)row["Customer_Registration_Number"];     // VarChar(32) NULL
                    if (row["Customer_Tax_ID"] != DBNull.Value) item.CustomerTaxID = (string)row["Customer_Tax_ID"];                                            // VarChar(16) NULL
                    if (row["Customer_Bank_Name"] != DBNull.Value) item.CustomerBankName = (string)row["Customer_Bank_Name"];                                   // NVarChar(128) NULL
                    if (row["Customer_Bank_Account"] != DBNull.Value) item.CustomerBankAccount = (string)row["Customer_Bank_Account"];                          // VarChar(64) NULL
                    if (row["Delegate_Name"] != DBNull.Value) item.DelegateName = (string)row["Delegate_Name"];                                                 // NVarChar(128) NULL
                    if (row["Delegate_ID_1"] != DBNull.Value) item.DelegateID1 = (string)row["Delegate_ID_1"];                                                  // NVarChar(8) NULL
                    if (row["Delegate_ID_2"] != DBNull.Value) item.DelegateID2 = (string)row["Delegate_ID_2"];                                                  // NVarChar(16) NULL
                    if (row["Delegate_ID_3"] != DBNull.Value) item.DelegateID3 = (string)row["Delegate_ID_3"];                                                  // NVarChar(32) NULL
                    if (row["Transport_Name"] != DBNull.Value) item.TransportName = (string)row["Transport_Name"];                                              // NVarChar(32) NULL
                    if (row["Transport_Detail_1"] != DBNull.Value) item.TransportDetail1 = (string)row["Transport_Detail_1"];                                   // NVarChar(32) NULL
                    if (row["Transport_Detail_2"] != DBNull.Value) item.TransportDetail2 = (string)row["Transport_Detail_2"];                                   // NVarChar(32) NULL
                    if (row["Transport_Detail_3"] != DBNull.Value) item.TransportDetail3 = (string)row["Transport_Detail_3"];                                   // NVarChar(32) NULL
                    if (row["Transport_Detail_4"] != DBNull.Value) item.TransportDetail4 = (string)row["Transport_Detail_4"];                                   // NVarChar(32) NULL
                    if (row["Invoice_Detail_1"] != DBNull.Value) item.Detail1 = (string)row["Invoice_Detail_1"];                                                // NVarChar(256) NULL
                    if (row["Invoice_Detail_2"] != DBNull.Value) item.Detail2 = (string)row["Invoice_Detail_2"];                                                // NVarChar(256) NULL
                    if (row["Invoice_Detail_3"] != DBNull.Value) item.Detail3 = (string)row["Invoice_Detail_3"];                                                // NVarChar(256) NULL
                    if (row["Invoice_Detail_4"] != DBNull.Value) item.Detail4 = (string)row["Invoice_Detail_4"];                                                // NVarChar(256) NULL
                    if (row["Sales_Order_ID"] != DBNull.Value) item.SalesOrderID = (int)row["Sales_Order_ID"];                                                  // Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
                    if (row["Delivery_Document_ID"] != DBNull.Value) item.DeliveryDocumentID = (int)row["Delivery_Document_ID"];                                // Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
                    if (row["Related_Invoice_ID"] != DBNull.Value) item.RelatedInvoiceID = (int)row["Related_Invoice_ID"];                                      // Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
                    if (row["Rendering_Layout"] != DBNull.Value) item.RenderingLayout = (string)row["Rendering_Layout"];                                        // VarChar(256) NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                                                          // NVarChar(128) NULL
                    item.TotalValue = (decimal)row["Total_Value"];                                                                                              // Decimal(27,8)
                    item.TotalPayed = (decimal)row["Total_Payed"];                                                                                              // Decimal(27,8)
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
		public static async Task<List<InvoiceModel>> ListFilteredByTypeAndStatusAsync(InvoiceType type, InvoiceStatus status, string? currency = null)
		{
			if (string.IsNullOrWhiteSpace(currency)) currency = BeezMart.Entities.Billing.Defaults.Currency;
			InvoiceModel item;
			List<InvoiceModel> list = new List<InvoiceModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceTypeID", SqlDbType.TinyInt).Value = type;
				db.AddParam("@InvoiceStatusID", SqlDbType.TinyInt).Value = status;
				db.AddParam("@Currency", SqlDbType.Char, 3).Value = currency;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new InvoiceModel();
					item.ID = (int)row["Invoice_ID"];																											// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.IssueDate = (DateTime)row["Issue_Date"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Due_Date"] != DBNull.Value) item.DueDate = (DateTime)row["Due_Date"];																// SmallDateTime NULL
					if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];													// SmallDateTime NULL
					if (row["Accounting_ID"] != DBNull.Value) item.AccountingID = (string)row["Accounting_ID"];													// VarChar(32) NULL
					item.VATpercentage = (decimal)row["VAT_Percentage"];																						// Decimal(6, 4) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];																									// Char(3) NOT NULL DEFAULT 'EUR'
					item.Type = (InvoiceType)(byte)row["Invoice_Type_ID"];																						// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
					item.Status = (InvoiceStatus)(byte)row["Invoice_Status_ID"];																				// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];												// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];																// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																							// NVarChar(256) NOT NULL
					if (row["Customer_City"] != DBNull.Value) item.CustomerCity = (string)row["Customer_City"];													// NVarChar(64) NULL
					if (row["Customer_Province"] != DBNull.Value) item.CustomerProvince = (string)row["Customer_Province"];										// NVarChar(64) NULL
					if (row["Customer_Address"] != DBNull.Value) item.CustomerAddress = (string)row["Customer_Address"];										// NVarChar(256) NULL
					if (row["Customer_Registration_Number"] != DBNull.Value) item.CustomerRegistrationNumber = (string)row["Customer_Registration_Number"];		// VarChar(32) NULL
					if (row["Customer_Tax_ID"] != DBNull.Value) item.CustomerTaxID = (string)row["Customer_Tax_ID"];											// VarChar(16) NULL
					if (row["Customer_Bank_Name"] != DBNull.Value) item.CustomerBankName = (string)row["Customer_Bank_Name"];									// NVarChar(128) NULL
					if (row["Customer_Bank_Account"] != DBNull.Value) item.CustomerBankAccount = (string)row["Customer_Bank_Account"];							// VarChar(64) NULL
					if (row["Delegate_Name"] != DBNull.Value) item.DelegateName = (string)row["Delegate_Name"];													// NVarChar(128) NULL
					if (row["Delegate_ID_1"] != DBNull.Value) item.DelegateID1 = (string)row["Delegate_ID_1"];													// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) item.DelegateID2 = (string)row["Delegate_ID_2"];													// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) item.DelegateID3 = (string)row["Delegate_ID_3"];													// NVarChar(32) NULL
					if (row["Transport_Name"] != DBNull.Value) item.TransportName = (string)row["Transport_Name"];												// NVarChar(32) NULL
					if (row["Transport_Detail_1"] != DBNull.Value) item.TransportDetail1 = (string)row["Transport_Detail_1"];									// NVarChar(32) NULL
					if (row["Transport_Detail_2"] != DBNull.Value) item.TransportDetail2 = (string)row["Transport_Detail_2"];									// NVarChar(32) NULL
					if (row["Transport_Detail_3"] != DBNull.Value) item.TransportDetail3 = (string)row["Transport_Detail_3"];									// NVarChar(32) NULL
					if (row["Transport_Detail_4"] != DBNull.Value) item.TransportDetail4 = (string)row["Transport_Detail_4"];									// NVarChar(32) NULL
					if (row["Invoice_Detail_1"] != DBNull.Value) item.Detail1 = (string)row["Invoice_Detail_1"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_2"] != DBNull.Value) item.Detail2 = (string)row["Invoice_Detail_2"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_3"] != DBNull.Value) item.Detail3 = (string)row["Invoice_Detail_3"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_4"] != DBNull.Value) item.Detail4 = (string)row["Invoice_Detail_4"];												// NVarChar(256) NULL
					if (row["Sales_Order_ID"] != DBNull.Value) item.SalesOrderID = (int)row["Sales_Order_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) item.DeliveryDocumentID = (int)row["Delivery_Document_ID"];								// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Related_Invoice_ID"] != DBNull.Value) item.RelatedInvoiceID = (int)row["Related_Invoice_ID"];										// Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Rendering_Layout"] != DBNull.Value) item.RenderingLayout = (string)row["Rendering_Layout"];										// VarChar(256) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];															// NVarChar(128) NULL
					item.TotalValue = (decimal)row["Total_Value"];																								// Decimal(27,8)
					item.TotalPayed = (decimal)row["Total_Payed"];																								// Decimal(27,8)
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<List<InvoiceModel>> ListOverdueFilteredByTypeAsync(InvoiceType type, string? currency = null)
		{
			if (string.IsNullOrWhiteSpace(currency)) currency = BeezMart.Entities.Billing.Defaults.Currency;
			InvoiceModel item;
			List<InvoiceModel> list = new List<InvoiceModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceTypeID", SqlDbType.TinyInt).Value = type;
				db.AddParam("@Currency", SqlDbType.Char, 3).Value = currency;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new InvoiceModel();
					item.ID = (int)row["Invoice_ID"];																											// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.IssueDate = (DateTime)row["Issue_Date"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Due_Date"] != DBNull.Value) item.DueDate = (DateTime)row["Due_Date"];																// SmallDateTime NULL
					if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];													// SmallDateTime NULL
					if (row["Accounting_ID"] != DBNull.Value) item.AccountingID = (string)row["Accounting_ID"];													// VarChar(32) NULL
					item.VATpercentage = (decimal)row["VAT_Percentage"];																						// Decimal(6, 4) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];																									// Char(3) NOT NULL DEFAULT 'EUR'
					item.Type = (InvoiceType)(byte)row["Invoice_Type_ID"];																						// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
					item.Status = (InvoiceStatus)(byte)row["Invoice_Status_ID"];																				// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];												// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];																// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																							// NVarChar(256) NOT NULL
					if (row["Customer_City"] != DBNull.Value) item.CustomerCity = (string)row["Customer_City"];													// NVarChar(64) NULL
					if (row["Customer_Province"] != DBNull.Value) item.CustomerProvince = (string)row["Customer_Province"];										// NVarChar(64) NULL
					if (row["Customer_Address"] != DBNull.Value) item.CustomerAddress = (string)row["Customer_Address"];										// NVarChar(256) NULL
					if (row["Customer_Registration_Number"] != DBNull.Value) item.CustomerRegistrationNumber = (string)row["Customer_Registration_Number"];		// VarChar(32) NULL
					if (row["Customer_Tax_ID"] != DBNull.Value) item.CustomerTaxID = (string)row["Customer_Tax_ID"];											// VarChar(16) NULL
					if (row["Customer_Bank_Name"] != DBNull.Value) item.CustomerBankName = (string)row["Customer_Bank_Name"];									// NVarChar(128) NULL
					if (row["Customer_Bank_Account"] != DBNull.Value) item.CustomerBankAccount = (string)row["Customer_Bank_Account"];							// VarChar(64) NULL
					if (row["Delegate_Name"] != DBNull.Value) item.DelegateName = (string)row["Delegate_Name"];													// NVarChar(128) NULL
					if (row["Delegate_ID_1"] != DBNull.Value) item.DelegateID1 = (string)row["Delegate_ID_1"];													// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) item.DelegateID2 = (string)row["Delegate_ID_2"];													// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) item.DelegateID3 = (string)row["Delegate_ID_3"];													// NVarChar(32) NULL
					if (row["Transport_Name"] != DBNull.Value) item.TransportName = (string)row["Transport_Name"];												// NVarChar(32) NULL
					if (row["Transport_Detail_1"] != DBNull.Value) item.TransportDetail1 = (string)row["Transport_Detail_1"];									// NVarChar(32) NULL
					if (row["Transport_Detail_2"] != DBNull.Value) item.TransportDetail2 = (string)row["Transport_Detail_2"];									// NVarChar(32) NULL
					if (row["Transport_Detail_3"] != DBNull.Value) item.TransportDetail3 = (string)row["Transport_Detail_3"];									// NVarChar(32) NULL
					if (row["Transport_Detail_4"] != DBNull.Value) item.TransportDetail4 = (string)row["Transport_Detail_4"];									// NVarChar(32) NULL
					if (row["Invoice_Detail_1"] != DBNull.Value) item.Detail1 = (string)row["Invoice_Detail_1"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_2"] != DBNull.Value) item.Detail2 = (string)row["Invoice_Detail_2"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_3"] != DBNull.Value) item.Detail3 = (string)row["Invoice_Detail_3"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_4"] != DBNull.Value) item.Detail4 = (string)row["Invoice_Detail_4"];												// NVarChar(256) NULL
					if (row["Sales_Order_ID"] != DBNull.Value) item.SalesOrderID = (int)row["Sales_Order_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) item.DeliveryDocumentID = (int)row["Delivery_Document_ID"];								// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Related_Invoice_ID"] != DBNull.Value) item.RelatedInvoiceID = (int)row["Related_Invoice_ID"];										// Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Rendering_Layout"] != DBNull.Value) item.RenderingLayout = (string)row["Rendering_Layout"];										// VarChar(256) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];															// NVarChar(128) NULL
					item.TotalValue = (decimal)row["Total_Value"];																								// Decimal(27,8)
					item.TotalPayed = (decimal)row["Total_Payed"];																								// Decimal(27,8)
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> ChangeStatusAsync(int invoiceID, InvoiceStatus newStatus, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoiceID;
				db.AddParam("@InvoiceStatusID", SqlDbType.TinyInt).Value = newStatus;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.ExecuteProcedureAsync("Billing.Invoices_Upd_1")) > 0) success = true;
			}
			return success;
		}
		/// <summary>
		///		<para>Gets the count of invoices with a specific status, grouped by currency.</para>
		///		<para>Returns a dictionary where keys are currencies and values are counts.</para>
		/// </summary>
		/// <param name="status"></param>
		/// <returns>A list of pairs like [{"RON", 253}, {"EUR", 35}, {"USD", 22}]</returns>
		public static async Task<Dictionary<string, int>> GetCountsOfInvoicesByCurrencyAsync(InvoiceStatus status)
		{
			string currency;
			int countOfInvoices;
			Dictionary<string, int> list = new Dictionary<string, int>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceStatusID", SqlDbType.TinyInt).Value = status;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Grid_1a");
				while (await rdrReader.ReadAsync())
				{
					currency = (string)row["Currency"];														// Char(3)
					countOfInvoices = (int)row["CountOfInvoices"];											// Int
					list.Add(currency, countOfInvoices);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		/// <summary>
		///		<para>Gets the count of overdue invoices, grouped by currency.</para>
		///		<para>Returns a dictionary where keys are currencies and values are counts.</para>
		/// </summary>
		/// <param name="Status"></param>
		/// <returns>A list of pairs like [{"RON", 253}, {"EUR", 35}, {"USD", 22}]</returns>
		public static async Task<Dictionary<string, int>> GetCountsOfOverdueInvoicesByCurrencyAsync()
		{
			string currency;
			int countOfInvoices;
			Dictionary<string, int> list = new Dictionary<string, int>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Invoices_Grid_2a");
				while (await rdrReader.ReadAsync())
				{
					currency = (string)row["Currency"];														// Char(3)
					countOfInvoices = (int)row["CountOfInvoices"];											// Int
					list.Add(currency, countOfInvoices);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		/// <summary>
		///		<para>Generates an Accounting ID for an invoice, if the invoice doesn't have one.</para>
		///		<para>The Accounting ID is allocated and the invoice is saved with the generated Accounting ID.</para>
		/// </summary>
		/// <param name="invoiceID"></param>
		/// <returns>The generated Accounting ID</returns>
		public static async Task<string?> AllocateAccountingIDAsync(int invoiceID)
		{
			InvoiceModel? invoice = await Invoices.RetrieveAsync(invoiceID);
			if (invoice == null) return null;
			else return await AllocateAccountingIDAsync(invoice);
		}
		/// <summary>
		///		<para>Generates an Accounting ID for an invoice, if the invoice doesn't have one.</para>
		///		<para>The Accounting ID is allocated and the invoice is saved with the generated Accounting ID.</para>
		/// </summary>
		/// <param name="invoiceID"></param>
		/// <returns>The generated Accounting ID</returns>
		public static async Task<string> AllocateAccountingIDAsync(InvoiceModel invoice)
		{
			if (!string.IsNullOrWhiteSpace(invoice.AccountingID)) return invoice.AccountingID;
			string accountingIdFormat = BeezMart.Entities.Billing.Defaults.Auto_Serials_Format;
			string serialFormat = accountingIdFormat[.. accountingIdFormat.IndexOf(' ')];
			string numberFormat = accountingIdFormat[(accountingIdFormat.IndexOf(' ') + 1) ..];
			int startingNumber = int.Parse(numberFormat);
			using (Database db = new ())
			{
				db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;						// BigInt OUTPUT
				db.AddParam("@StartingNumber", SqlDbType.Int).Value = startingNumber;									// Int
				db.AddParam("@Serial", SqlDbType.VarChar, 8).Value = serialFormat;										// VarChar(8)
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoice.ID;											// Int
				db.AddParam("@AllocatedNumber", SqlDbType.Int).Direction = ParameterDirection.Output;					// Int OUTPUT
				await db.SaveRecordAsync("Billing.Invoice_Accounting_IDs_Ins", clearParamsAfterCompletion: false);
				long recordID = (long)db.Params["@RecordID"].Value;
				int allocatedNumber = (int)db.Params["@AllocatedNumber"].Value;
				invoice.AccountingID = serialFormat + " " + allocatedNumber.ToString().PadLeft(numberFormat.Length, '0');
				db.Params.Clear();
				db.AddParam("@RecordID", SqlDbType.BigInt).Value = recordID;											// BigInt
				db.AddParam("@Number", SqlDbType.Int).Value = allocatedNumber;											// Int
				db.AddParam("@AccountingID", SqlDbType.VarChar, 32).Value = invoice.AccountingID;						// VarChar(8)
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoice.ID;											// Int
				await db.ExecuteProcedureAsync("Billing.Invoice_Accounting_IDs_Upd");
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoice.ID;											// Int
				db.AddParam("@AccountingID", SqlDbType.VarChar, 32).Value = invoice.AccountingID;						// Int
				await db.ExecuteProcedureAsync("Billing.Invoices_Upd_2");
			}
			return invoice.AccountingID;
		}
		public static async Task<List<InvoiceModel>> LookupAsync(InvoiceModel.LookupFilter filterCriteria, short maxRecordsCount = 1000)
		{
			InvoiceModel item;
			List<InvoiceModel> list = new List<InvoiceModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				StringBuilder stbTransactSqlCommand = new StringBuilder();
				#region Build Database Query
				StringBuilder stbSelectClause = new StringBuilder();
				StringBuilder stbFromClause = new StringBuilder();
				StringBuilder stbWhereClause = new StringBuilder();
				StringBuilder stbOrderByClause = new StringBuilder();
				#region Build SELECT clause
				stbSelectClause.AppendLine(filterCriteria.HasAnyCriteria ? $"SELECT TOP {maxRecordsCount}" : $"SELECT TOP {Defaults.Search_Result_Unfiltered_MaxCount}");
				stbSelectClause.AppendLine(" I.Invoice_ID");
				stbSelectClause.AppendLine(",I.Issue_Date");
				stbSelectClause.AppendLine(",I.Due_Date");
				stbSelectClause.AppendLine(",I.Closing_Date");
				stbSelectClause.AppendLine(",I.Accounting_ID");
				stbSelectClause.AppendLine(",I.VAT_Percentage");
				stbSelectClause.AppendLine(",I.Currency");
				stbSelectClause.AppendLine(",I.Invoice_Type_ID");
				stbSelectClause.AppendLine(",I.Invoice_Status_ID");
				stbSelectClause.AppendLine(",I.Customer_Name");
				stbSelectClause.AppendLine(",I.Customer_City");
				stbSelectClause.AppendLine(",I.Customer_Province");
				stbSelectClause.AppendLine(",I.Organization_ID");
				stbSelectClause.AppendLine(",I.Person_ID");
				stbSelectClause.AppendLine(",Billing.Invoice_Total_Value(Invoice_ID) AS Total_Value");
				stbSelectClause.AppendLine(",Billing.Invoice_Total_Payed(Invoice_ID) AS Total_Payed");
				#endregion
				#region Build FROM clause
				stbFromClause.AppendLine("FROM Billing.Invoices I");
                //stbFromClause.AppendLine("    LEFT JOIN CRM.Organizations O ON O.Organization_ID = I.Organization_ID");
                #endregion
                #region Build WHERE clause
                string searchOperator = filterCriteria.SearchOperator.ToString().ToUpper();
                if (!string.IsNullOrWhiteSpace(filterCriteria.CustomerNamePattern))
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Customer_Name LIKE @CustomerName");
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 128).Value = filterCriteria.CustomerNameExactMatch ? filterCriteria.CustomerNamePattern : "%" + filterCriteria.CustomerNamePattern + "%";
                }
                if (filterCriteria.OrganizationID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} I.Organization_ID = @OrganizationID");
                    db.AddParam("@OrganizationID", SqlDbType.Int).Value = filterCriteria.OrganizationID.Value;
                }
                if (filterCriteria.PersonID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} I.Person_ID = @PersonID");
                    db.AddParam("@PersonID", SqlDbType.Int).Value = filterCriteria.PersonID.Value;
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.CustomerTaxIdPattern))
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Customer_Tax_ID LIKE @CustomerTaxID");
					db.AddParam("@CustomerTaxID", SqlDbType.VarChar, 32).Value = filterCriteria.CustomerTaxIdExactMatch ? filterCriteria.CustomerTaxIdPattern : "%" + filterCriteria.CustomerTaxIdPattern + "%";
				}
				if (filterCriteria.IssuedAfter.HasValue)
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Issue_Date >= @IssuedAfter");
					db.AddParam("@IssuedAfter", SqlDbType.SmallDateTime).Value = filterCriteria.IssuedAfter.Value;
				}
				if (filterCriteria.IssuedBefore.HasValue)
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Issue_Date <= @IssuedBefore");
					db.AddParam("@IssuedBefore", SqlDbType.SmallDateTime).Value = filterCriteria.IssuedBefore.Value;
				}
				if (!string.IsNullOrWhiteSpace(filterCriteria.Currency))
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Currency = @Currency");
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = filterCriteria.Currency;
				}
				if (filterCriteria.InvoiceType.HasValue)
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Invoice_Type_ID = @InvoiceTypeID");
					db.AddParam("@InvoiceTypeID", SqlDbType.TinyInt).Value = (byte)filterCriteria.InvoiceType;
				}
				if (filterCriteria.InvoiceStatus.HasValue)
				{
					stbWhereClause.AppendLine($"{searchOperator} I.Invoice_Status_ID = @InvoiceStatusID");
					db.AddParam("@InvoiceStatusID", SqlDbType.TinyInt).Value = (byte)filterCriteria.InvoiceStatus;
				}
                if (filterCriteria.Overdue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} (Invoice_Status_ID = 60 AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE())");
                }
				if (filterCriteria.InvoiceIDs != null && filterCriteria.InvoiceIDs.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (int invoiceID in filterCriteria.InvoiceIDs)
					{
						stbTemp.Append(invoiceID);
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} I.Invoice_ID IN ({stbTemp})");
				}
				if (filterCriteria.AccountingIDs != null && filterCriteria.AccountingIDs.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (string accountingID in filterCriteria.AccountingIDs)
					{
						stbTemp.Append('\'');
						stbTemp.Append(accountingID);
						stbTemp.Append('\'');
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} I.Accounting_ID IN ({stbTemp})");
				}

				if (stbWhereClause.Length > 0)
				{
					stbWhereClause.Remove(0, searchOperator.Length + 1);
					stbWhereClause.Insert(0, "WHERE ");
				}
				#endregion
				#region Build ORDER BY clause
				stbOrderByClause.AppendLine("ORDER BY I.Issue_Date DESC, I.Customer_Name");
				#endregion
				stbTransactSqlCommand.AppendLine(stbSelectClause.ToString());
				stbTransactSqlCommand.AppendLine(stbFromClause.ToString());
				stbTransactSqlCommand.AppendLine(stbWhereClause.ToString());
				stbTransactSqlCommand.AppendLine(stbOrderByClause.ToString());
				#endregion
				row = rdrReader = await db.GetSqlDataReaderFromTextCommandAsync(stbTransactSqlCommand.ToString());
				while (await rdrReader.ReadAsync())
				{
					item = new InvoiceModel();
					item.ID = (int)row["Invoice_ID"];																			// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.IssueDate = (DateTime)row["Issue_Date"];																// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Due_Date"] != DBNull.Value) item.DueDate = (DateTime)row["Due_Date"];								// SmallDateTime NULL
					if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];					// SmallDateTime NULL
					if (row["Accounting_ID"] != DBNull.Value) item.AccountingID = (string)row["Accounting_ID"];					// VarChar(32) NULL
					item.VATpercentage = (decimal)row["VAT_Percentage"];														// Decimal(6, 4) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];																	// Char(3) NOT NULL DEFAULT 'EUR'
					item.Type = (InvoiceType)row["Invoice_Type_ID"];															// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
					item.Status = (InvoiceStatus)row["Invoice_Status_ID"];														// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
					item.CustomerName = (string)row["Customer_Name"];															// NVarChar(256) NOT NULL
					if (row["Customer_City"] != DBNull.Value) item.CustomerCity = (string)row["Customer_City"];					// NVarChar(64) NULL
					if (row["Customer_Province"] != DBNull.Value) item.CustomerProvince = (string)row["Customer_Province"];		// NVarChar(64) NULL
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];				// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];								// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.TotalValue = (decimal)row["Total_Value"];																// Decimal(27,8) NOT NULL DEFAULT 0
					item.TotalPayed = (decimal)row["Total_Payed"];																// Decimal(27,8) NOT NULL DEFAULT 0
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
	}
}
