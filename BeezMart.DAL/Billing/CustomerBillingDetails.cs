﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class CustomerBillingDetails
	{
        
        /// <summary>
        ///		<para>Gets billing details for an organization in the CRM, then billing-settings and bank-accounts, falling back to invoices.</para>
        /// </summary>
        /// <param name="OrganizationID">Organization to look for.</param>
        /// <param name="currency">Example: RON, EUR, USD. If present, will look for a corresponding bank account.</param>
        /// <param name="maxRows">Maximum number of records to look for and return.</param>
        /// <returns></returns>
        public static async Task<CustomerBillingDetailsModel?> RetrieveForOrganizationAsync(int organizationID, string? currency = null)
        {
            CustomerBillingDetailsModel? entity = null;
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
                db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Billing_Details_Organization");
                if (await rdrReader.ReadAsync())
                {
                    entity = new CustomerBillingDetailsModel();
                    if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];                        // Int
                    if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];                                          // Int
                    if (row["Customer_Name"] != DBNull.Value) entity.CustomerName = (string)row["Customer_Name"];                           // NVarChar(256)
                    if (row["City"] != DBNull.Value) entity.City = (string)row["City"];                                                     // NVarChar(64)
                    if (row["State_Province"] != DBNull.Value) entity.StateProvince = (string)row["State_Province"];                        // NVarChar(128)
                    if (row["Street_Address_1"] != DBNull.Value) entity.StreetAddress1 = (string)row["Street_Address_1"];                   // NVarChar(128)
                    if (row["Registration_Number"] != DBNull.Value) entity.RegistrationNumber = (string)row["Registration_Number"];         // VarChar(32)
                    if (row["Tax_ID"] != DBNull.Value) entity.TaxID = (string)row["Tax_ID"];                                                // VarChar(16)
                    if (row["Bank"] != DBNull.Value) entity.BankName = (string)row["Bank"];                                                 // NVarChar(128)
                    if (row["IBAN"] != DBNull.Value) entity.BankAccountIBAN = (string)row["IBAN"];										    // VarChar(32)
                    if (row["Billing_Address_ID"] != DBNull.Value) entity.BillingAddressID = (int)row["Billing_Address_ID"];                // Int
                    if (row["Bank_Account_ID"] != DBNull.Value) entity.BankAccountID = (int)row["Bank_Account_ID"];                         // Int
                    entity.Currency = currency;
                    entity.Source = "CRM.Organizations";
                }
                rdrReader.Dispose();
            }
            return entity;
        }
        
        /// <summary>
        ///		<para>Gets billing details for a person in the CRM, then billing-settings and bank-accounts, falling back to invoices.</para>
        /// </summary>
        /// <param name="PersonID">Person to look for.</param>
        /// <param name="currency">Example: RON, EUR, USD. If present, will look for a corresponding bank account.</param>
        /// <param name="maxRows">Maximum number of records to look for and return.</param>
        /// <returns></returns>
        public static async Task<CustomerBillingDetailsModel?> RetrieveForPersonAsync(int personID, string? currency = null)
        {
            CustomerBillingDetailsModel? entity = null;
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
                db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Billing_Details_Person");
                if (await rdrReader.ReadAsync())
                {
                    entity = new CustomerBillingDetailsModel();
                    if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];                        // Int
                    if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];                                          // Int
                    if (row["Customer_Name"] != DBNull.Value) entity.CustomerName = (string)row["Customer_Name"];                           // NVarChar(256)
                    if (row["City"] != DBNull.Value) entity.City = (string)row["City"];                                                     // NVarChar(64)
                    if (row["State_Province"] != DBNull.Value) entity.StateProvince = (string)row["State_Province"];                        // NVarChar(128)
                    if (row["Street_Address_1"] != DBNull.Value) entity.StreetAddress1 = (string)row["Street_Address_1"];                   // NVarChar(128)
                    if (row["Registration_Number"] != DBNull.Value) entity.RegistrationNumber = (string)row["Registration_Number"];         // VarChar(32)
                    if (row["Tax_ID"] != DBNull.Value) entity.TaxID = (string)row["Tax_ID"];                                                // VarChar(16)
                    if (row["Bank"] != DBNull.Value) entity.BankName = (string)row["Bank"];                                                 // NVarChar(128)
                    if (row["IBAN"] != DBNull.Value) entity.BankAccountIBAN = (string)row["IBAN"];										    // VarChar(32)
                    if (row["Billing_Address_ID"] != DBNull.Value) entity.BillingAddressID = (int)row["Billing_Address_ID"];                // Int
                    if (row["Bank_Account_ID"] != DBNull.Value) entity.BankAccountID = (int)row["Bank_Account_ID"];                         // Int
                    entity.Currency = currency;
                    entity.Source = "CRM.Persons";
                }
                rdrReader.Dispose();
            }
            return entity;
        }

        /// <summary>
        ///		<para>Gets billing details by looking for an organization in the CRM, then billing-settings and bank-accounts, falling back to invoices.</para>
        /// </summary>
        /// <param name="namePattern">Organization name to look for.</param>
        /// <param name="exactMatch">Look for the exact organization name, not "Organization name contains...".</param>
        /// <param name="currency">Example: RON, EUR, USD. If present, will look for a corresponding bank account.</param>
        /// <param name="maxRows">Maximum number of records to look for and return.</param>
        /// <returns></returns>
        public static async Task<List<CustomerBillingDetailsModel>> LookupOrganizationsAsync(string namePattern, bool exactMatch = false, string? currency = null, short maxRows = 10)
        {
            CustomerBillingDetailsModel item;
            List<CustomerBillingDetailsModel> list = new List<CustomerBillingDetailsModel>();
            if (string.IsNullOrWhiteSpace(namePattern)) return list;
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@NamePattern", SqlDbType.NVarChar, 128).Value = exactMatch ? namePattern : $"%{namePattern}%";
                db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
                db.AddParam("@MaxRows", SqlDbType.SmallInt).Value = maxRows;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Billing_Details_Lookup_Organizations");
                while (await rdrReader.ReadAsync())
                {
                    item = new CustomerBillingDetailsModel();
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                      // Int
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                        // Int
                    if (row["Customer_Name"] != DBNull.Value) item.CustomerName = (string)row["Customer_Name"];                         // NVarChar(256)
                    if (row["City"] != DBNull.Value) item.City = (string)row["City"];                                                   // NVarChar(64)
                    if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];                      // NVarChar(128)
                    if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];                 // NVarChar(128)
                    if (row["Registration_Number"] != DBNull.Value) item.RegistrationNumber = (string)row["Registration_Number"];       // VarChar(32)
                    if (row["Tax_ID"] != DBNull.Value) item.TaxID = (string)row["Tax_ID"];                                              // VarChar(16)
                    if (row["Bank"] != DBNull.Value) item.BankName = (string)row["Bank"];                                               // NVarChar(128)
                    if (row["IBAN"] != DBNull.Value) item.BankAccountIBAN = (string)row["IBAN"];										// VarChar(32)
                    if (row["Billing_Address_ID"] != DBNull.Value) item.BillingAddressID = (int)row["Billing_Address_ID"];              // Int
                    if (row["Bank_Account_ID"] != DBNull.Value) item.BankAccountID = (int)row["Bank_Account_ID"];                       // Int
                    item.Currency = currency;
                    item.Source = "CRM.Organizations";
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }

        /// <summary>
        ///		<para>Gets billing details by looking for a person in the CRM, then billing-settings and bank-accounts, falling back to invoices.</para>
        /// </summary>
        /// <param name="namePattern">Person first-name/last-name to look for.</param>
        /// <param name="exactMatch">Look for the exact person name, not "Person first-name/last-name contains...".</param>
        /// <param name="currency">Example: RON, EUR, USD. If present, will look for a corresponding bank account.</param>
        /// <param name="maxRows">Maximum number of records to look for and return.</param>
        /// <returns></returns>
        public static async Task<List<CustomerBillingDetailsModel>> LookupPersonsAsync(string namePattern, bool exactMatch = false, string? currency = null, short maxRows = 10)
        {
            CustomerBillingDetailsModel item;
            List<CustomerBillingDetailsModel> list = new List<CustomerBillingDetailsModel>();
            if (string.IsNullOrWhiteSpace(namePattern)) return list;
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@NamePattern", SqlDbType.NVarChar, 128).Value = exactMatch ? namePattern : $"%{namePattern}%";
                db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
                db.AddParam("@MaxRows", SqlDbType.SmallInt).Value = maxRows;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Billing_Details_Lookup_Persons");
                while (await rdrReader.ReadAsync())
                {
                    item = new CustomerBillingDetailsModel();
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                      // Int
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                        // Int
                    if (row["Customer_Name"] != DBNull.Value) item.CustomerName = (string)row["Customer_Name"];                         // NVarChar(256)
                    if (row["City"] != DBNull.Value) item.City = (string)row["City"];                                                   // NVarChar(64)
                    if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];                      // NVarChar(128)
                    if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];                 // NVarChar(128)
                    if (row["Registration_Number"] != DBNull.Value) item.RegistrationNumber = (string)row["Registration_Number"];       // VarChar(32)
                    if (row["Tax_ID"] != DBNull.Value) item.TaxID = (string)row["Tax_ID"];                                              // VarChar(16)
                    if (row["Bank"] != DBNull.Value) item.BankName = (string)row["Bank"];                                               // NVarChar(128)
                    if (row["IBAN"] != DBNull.Value) item.BankAccountIBAN = (string)row["IBAN"];										// VarChar(32)
                    if (row["Billing_Address_ID"] != DBNull.Value) item.BillingAddressID = (int)row["Billing_Address_ID"];              // Int
                    if (row["Bank_Account_ID"] != DBNull.Value) item.BankAccountID = (int)row["Bank_Account_ID"];                       // Int
                    item.Currency = currency;
                    item.Source = "CRM.Persons";
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }

        /// <summary>
        ///		<para>Gets billing details by looking for an organization/person billing settings, then takes name and address from CRM tables, falling back to invoices.</para>
        ///		<para>Bank account is taken from billing bank accounts, also falling back to invoices.</para>
        /// </summary>
        /// <param name="searchPattern">TaxID or RegistrationNumber to look for.</param>
        /// <param name="exactMatch">Look for the exact string, not "TaxID/RegistrationNumber contains...".</param>
        /// <param name="currency">Example: RON, EUR, USD. If present, will look for a corresponding bank account.</param>
        /// <param name="maxRows">Maximum number of records to look for and return.</param>
        /// <returns></returns>
        public static async Task<List<CustomerBillingDetailsModel>> LookupSettingsAsync(string searchPattern, bool exactMatch = false, string? currency = null, short maxRows = 10)
        {
            CustomerBillingDetailsModel item;
            List<CustomerBillingDetailsModel> list = new List<CustomerBillingDetailsModel>();
            if (string.IsNullOrWhiteSpace(searchPattern)) return list;
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@SearchPattern", SqlDbType.VarChar, 32).Value = exactMatch ? searchPattern : $"%{searchPattern}%";
                db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
                db.AddParam("@MaxRows", SqlDbType.SmallInt).Value = maxRows;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Billing_Details_Lookup_Settings");
                while (await rdrReader.ReadAsync())
                {
                    item = new CustomerBillingDetailsModel();
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                      // Int
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                        // Int
                    if (row["Customer_Name"] != DBNull.Value) item.CustomerName = (string)row["Customer_Name"];                         // NVarChar(256)
                    if (row["City"] != DBNull.Value) item.City = (string)row["City"];                                                   // NVarChar(64)
                    if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];                      // NVarChar(128)
                    if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];                 // NVarChar(128)
                    if (row["Registration_Number"] != DBNull.Value) item.RegistrationNumber = (string)row["Registration_Number"];       // VarChar(32)
                    if (row["Tax_ID"] != DBNull.Value) item.TaxID = (string)row["Tax_ID"];                                              // VarChar(16)
                    if (row["Bank"] != DBNull.Value) item.BankName = (string)row["Bank"];                                               // NVarChar(128)
                    if (row["IBAN"] != DBNull.Value) item.BankAccountIBAN = (string)row["IBAN"];										// VarChar(32)
                    if (row["Billing_Address_ID"] != DBNull.Value) item.BillingAddressID = (int)row["Billing_Address_ID"];              // Int
                    if (row["Bank_Account_ID"] != DBNull.Value) item.BankAccountID = (int)row["Bank_Account_ID"];                       // Int
                    item.Currency = currency;
                    item.Source = "Billing.Customer_Settings";
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }

        /// <summary>
        ///		<para>Gets billing details by looking into previous validated/pending/closed invoices.</para>
        ///		<para>Bank account might be falling back to billing accounts, if not found in invoices.</para>
        /// </summary>
        /// <param name="searchPattern">CustomerName, TaxID or RegistrationNumber to look for.</param>
        /// <param name="exactMatch">Look for the exact string, not "CustomerName/TaxID/RegistrationNumber contains...".</param>
        /// <param name="currency">Example: RON, EUR, USD. If present, will look for a corresponding invoice or bank account.</param>
        /// <param name="maxRows">Maximum number of records to look for and return.</param>
        /// <returns></returns>
        public static async Task<List<CustomerBillingDetailsModel>> LookupInvoicesAsync(string searchPattern, bool exactMatch = false, string? currency = null, short maxRows = 10)
        {
            CustomerBillingDetailsModel item;
            List<CustomerBillingDetailsModel> list = new List<CustomerBillingDetailsModel>();
            if (string.IsNullOrWhiteSpace(searchPattern)) return list;
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@SearchPattern", SqlDbType.NVarChar, 128).Value = exactMatch ? searchPattern : $"%{searchPattern}%";
                db.AddParam("@Currency", SqlDbType.Char, 3).Value = DBNull.Value; if (!string.IsNullOrEmpty(currency)) db.Params["@Currency"].Value = currency;
                db.AddParam("@MaxRows", SqlDbType.SmallInt).Value = maxRows;
                row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Billing_Details_Lookup_Invoices");
                while (await rdrReader.ReadAsync())
                {
                    item = new CustomerBillingDetailsModel();
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                      // Int
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                        // Int
                    if (row["Customer_Name"] != DBNull.Value) item.CustomerName = (string)row["Customer_Name"];                         // NVarChar(256)
                    if (row["City"] != DBNull.Value) item.City = (string)row["City"];                                                   // NVarChar(64)
                    if (row["State_Province"] != DBNull.Value) item.StateProvince = (string)row["State_Province"];                      // NVarChar(128)
                    if (row["Street_Address_1"] != DBNull.Value) item.StreetAddress1 = (string)row["Street_Address_1"];                 // NVarChar(128)
                    if (row["Registration_Number"] != DBNull.Value) item.RegistrationNumber = (string)row["Registration_Number"];       // VarChar(32)
                    if (row["Tax_ID"] != DBNull.Value) item.TaxID = (string)row["Tax_ID"];                                              // VarChar(16)
                    if (row["Bank"] != DBNull.Value) item.BankName = (string)row["Bank"];                                               // NVarChar(128)
                    if (row["IBAN"] != DBNull.Value) item.BankAccountIBAN = (string)row["IBAN"];										// VarChar(32)
                    if (row["Billing_Address_ID"] != DBNull.Value) item.BillingAddressID = (int)row["Billing_Address_ID"];              // Int
                    if (row["Bank_Account_ID"] != DBNull.Value) item.BankAccountID = (int)row["Bank_Account_ID"];                       // Int
                    item.Currency = currency;
                    item.Source = "Billing.Invoices";
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
    }
}
