﻿using BeezMart.Entities.Billing;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class CustomerSettings
    {
		public static async Task<bool> SaveAsync(CustomerSettingsModel customerSettings)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (customerSettings.ID.HasValue)
				{
					db.AddParam("@RecordID", SqlDbType.Int).Value = customerSettings.ID.Value;											// Int
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = customerSettings.OrganizationID;								// Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = customerSettings.PersonID;											// Int = NULL
					db.AddParam("@RegistrationNumber", SqlDbType.VarChar, 32).Value = customerSettings.RegistrationNumber;				// VarChar(16) = NULL
					db.AddParam("@TaxID", SqlDbType.VarChar, 16).Value = customerSettings.TaxID;										// VarChar(16) = NULL
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = customerSettings.Currency;										// Char(3) = NULL
					db.AddParam("@PreferredAccountID", SqlDbType.Int).Value = customerSettings.PreferredAccountID;						// Int = NULL
					db.AddParam("@MaximumDiscountPercentage", SqlDbType.TinyInt).Value = customerSettings.MaximumDiscountPercentage;	// TinyInt = NULL
					db.AddParam("@PaymentAllowanceInDays", SqlDbType.TinyInt).Value = customerSettings.PaymentAllowanceInDays;			// TinyInt = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = customerSettings.UpdatedOn;								// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = customerSettings.UpdatedBy;								// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Customer_Settings_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@RecordID", SqlDbType.Int).Direction = ParameterDirection.Output;										// Int
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = customerSettings.OrganizationID;								// Int = NULL
					db.AddParam("@PersonID", SqlDbType.Int).Value = customerSettings.PersonID;											// Int = NULL
					db.AddParam("@RegistrationNumber", SqlDbType.VarChar, 32).Value = customerSettings.RegistrationNumber;				// VarChar(16) = NULL
					db.AddParam("@TaxID", SqlDbType.VarChar, 16).Value = customerSettings.TaxID;										// VarChar(16) = NULL
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = customerSettings.Currency;										// Char(3) = NULL
					db.AddParam("@PreferredAccountID", SqlDbType.Int).Value = customerSettings.PreferredAccountID;						// Int = NULL
					db.AddParam("@MaximumDiscountPercentage", SqlDbType.TinyInt).Value = customerSettings.MaximumDiscountPercentage;	// TinyInt = NULL
					db.AddParam("@PaymentAllowanceInDays", SqlDbType.TinyInt).Value = customerSettings.PaymentAllowanceInDays;			// TinyInt = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = customerSettings.UpdatedOn;								// SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = customerSettings.UpdatedBy;								// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Customer_Settings_Upsert", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) customerSettings.ID = (int)db.Params["@RecordID"].Value;
				}
			}
			return success;
		}
		public static async Task<CustomerSettingsModel?> RetrieveAsync(int recordID)
		{
			CustomerSettingsModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.Int).Value = recordID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Settings_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new CustomerSettingsModel();
					entity.ID = (int)row["Record_ID"];																										// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];										// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];															// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Registration_Number"] != DBNull.Value) entity.RegistrationNumber = (string)row["Registration_Number"];							// VarChar(32) NULL
					if (row["Tax_ID"] != DBNull.Value) entity.TaxID = (string)row["Tax_ID"];																// VarChar(16) NULL
					if (row["Currency"] != DBNull.Value) entity.Currency = (string)row["Currency"];															// Char(3) NULL
					if (row["Preferred_Account_ID"] != DBNull.Value) entity.PreferredAccountID = (int)row["Preferred_Account_ID"];							// Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Record_ID)
					if (row["Maximum_Discount_Percentage"] != DBNull.Value) entity.MaximumDiscountPercentage = (byte)row["Maximum_Discount_Percentage"];	// TinyInt NULL
					if (row["Payment_Allowance_In_Days"] != DBNull.Value) entity.PaymentAllowanceInDays = (byte)row["Payment_Allowance_In_Days"];			// TinyInt NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																							// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];													// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<CustomerSettingsModel?> GetForOrganizationAsync(int organizationID)
		{
			CustomerSettingsModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@OrganizationID", SqlDbType.Int).Value = organizationID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Settings_Sel_1");
				if (await rdrReader.ReadAsync())
				{
					entity = new CustomerSettingsModel();
					entity.ID = (int)row["Record_ID"];																										// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];										// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];															// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Registration_Number"] != DBNull.Value) entity.RegistrationNumber = (string)row["Registration_Number"];							// VarChar(32) NULL
					if (row["Tax_ID"] != DBNull.Value) entity.TaxID = (string)row["Tax_ID"];																// VarChar(16) NULL
					if (row["Currency"] != DBNull.Value) entity.Currency = (string)row["Currency"];															// Char(3) NULL
					if (row["Preferred_Account_ID"] != DBNull.Value) entity.PreferredAccountID = (int)row["Preferred_Account_ID"];							// Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Record_ID)
					if (row["Maximum_Discount_Percentage"] != DBNull.Value) entity.MaximumDiscountPercentage = (byte)row["Maximum_Discount_Percentage"];	// TinyInt NULL
					if (row["Payment_Allowance_In_Days"] != DBNull.Value) entity.PaymentAllowanceInDays = (byte)row["Payment_Allowance_In_Days"];			// TinyInt NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																							// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];													// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		public static async Task<CustomerSettingsModel?> GetForPersonAsync(int personID)
		{
			CustomerSettingsModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PersonID", SqlDbType.Int).Value = personID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Customer_Settings_Sel_2");
				if (await rdrReader.ReadAsync())
				{
					entity = new CustomerSettingsModel();
					entity.ID = (int)row["Record_ID"];																										// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];										// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];															// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					if (row["Registration_Number"] != DBNull.Value) entity.RegistrationNumber = (string)row["Registration_Number"];							// VarChar(32) NULL
					if (row["Tax_ID"] != DBNull.Value) entity.TaxID = (string)row["Tax_ID"];																// VarChar(16) NULL
					if (row["Currency"] != DBNull.Value) entity.Currency = (string)row["Currency"];															// Char(3) NULL
					if (row["Preferred_Account_ID"] != DBNull.Value) entity.PreferredAccountID = (int)row["Preferred_Account_ID"];							// Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Record_ID)
					if (row["Maximum_Discount_Percentage"] != DBNull.Value) entity.MaximumDiscountPercentage = (byte)row["Maximum_Discount_Percentage"];	// TinyInt NULL
					if (row["Payment_Allowance_In_Days"] != DBNull.Value) entity.PaymentAllowanceInDays = (byte)row["Payment_Allowance_In_Days"];			// TinyInt NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																							// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];													// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
	}
}
