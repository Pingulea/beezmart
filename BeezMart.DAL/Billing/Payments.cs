﻿using BeezMart.Entities.Billing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Billing
{
	public static class Payments
    {
		public static async Task<bool> SaveAsync(PaymentModel payment)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (payment.ID.HasValue)
				{
					db.AddParam("@PaymentID", SqlDbType.Int).Value = payment.ID.Value;						    // Int
					db.AddParam("@InvoiceID", SqlDbType.Int).Value = payment.InvoiceID;						    // Int
					db.AddParam("@PaymentDate", SqlDbType.SmallDateTime).Value = payment.PaymentDate;	        // SmallDateTime
					db.AddParam("@Amount", SqlDbType.Decimal).Value = payment.Amount;						    // Decimal(24, 8) = 0
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = payment.Currency;					    // Char(3) = 'EUR'
					db.AddParam("@Comments", SqlDbType.NText).Value = payment.Comments;						    // NVarChar(MAX) = NULL
					db.AddParam("@PaymentCheckID", SqlDbType.Int).Value = payment.PaymentCheckID;	    	    // Int = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = payment.UpdatedOn;			    // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = payment.UpdatedBy;			    // NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Payments_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@PaymentID", SqlDbType.Int).Direction = ParameterDirection.Output;			    // Int
					db.AddParam("@InvoiceID", SqlDbType.Int).Value = payment.InvoiceID;						    // Int
					db.AddParam("@PaymentDate", SqlDbType.SmallDateTime).Value = payment.PaymentDate;   		// SmallDateTime
					db.AddParam("@Amount", SqlDbType.Decimal).Value = payment.Amount;						    // Decimal(24, 8) = 0
					db.AddParam("@Currency", SqlDbType.Char, 3).Value = payment.Currency;					    // Char(3) = 'EUR'
					db.AddParam("@Comments", SqlDbType.NText).Value = payment.Comments;						    // NVarChar(MAX) = NULL
					db.AddParam("@PaymentCheckID", SqlDbType.Int).Value = payment.PaymentCheckID;		        // Int = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = payment.UpdatedOn;			    // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = payment.UpdatedBy;			    // NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Billing.Payments_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) payment.ID = (int)db.Params["@PaymentID"].Value;
				}
			}
			return success;
		}
		public static async Task<PaymentModel?> RetrieveAsync(int paymentID)
		{
			PaymentModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@PaymentID", SqlDbType.Int).Value = paymentID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Payments_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new PaymentModel();
					entity.ID = (int)row["Payment_ID"];																		// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					entity.InvoiceID = (int)row["Invoice_ID"];																// Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					entity.PaymentDate = (DateTime)row["Payment_Date"];														// SmallDateTime NOT NULL DEFAULT GETDATE()
					entity.Amount = (decimal)row["Amount"];																	// Decimal(24, 8) NOT NULL DEFAULT 0
					entity.Currency = (string)row["Currency"];																// Char(3) NOT NULL DEFAULT 'EUR'
					if (row["Comments"] != DBNull.Value) entity.Comments = (string)row["Comments"];							// NVarChar(MAX) NULL
					if (row["Payment_Check_ID"] != DBNull.Value) entity.PaymentCheckID = (int)row["Payment_Check_ID"];		// Int NULL FOREIGN KEY REFERENCES Sales.Invoice_Payment_Checks(Payment_Check_ID)
					if (row["Accounting_ID"] != DBNull.Value) entity.InvoiceAccountingID = (string)row["Accounting_ID"];	// VarChar(32) NULL
					entity.InvoiceIssueDate = (DateTime)row["Issue_Date"];													// SmallDateTime NOT NULL
					entity.CustomerName = (string)row["Customer_Name"];														// NVarChar(256) NOT NULL
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];		// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];							// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];					// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}
		/// <summary>
		///		<para>Gets the payments associated with an invoice</para>
		/// </summary>
		/// <param name="invoiceID"></param>
		/// <returns></returns>
		public static async Task<List<PaymentModel>> ListInvoicePaymentsAsync(int invoiceID)
		{
			PaymentModel item;
			List<PaymentModel> list = new List<PaymentModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@InvoiceID", SqlDbType.Int).Value = invoiceID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Payments_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new PaymentModel();
					item.ID = (int)row["Payment_ID"];																		// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.InvoiceID = (int)row["Invoice_ID"];																// Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					item.PaymentDate = (DateTime)row["Payment_Date"];														// SmallDateTime NOT NULL DEFAULT GETDATE()
					item.Amount = (decimal)row["Amount"];																	// Decimal(24, 8) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];			                                                    // Char(3) NOT NULL DEFAULT 'EUR'
					if (row["Comments"] != DBNull.Value) item.Comments = (string)row["Comments"];							// NVarChar(MAX) NULL
					if (row["Payment_Check_ID"] != DBNull.Value) item.PaymentCheckID = (int)row["Payment_Check_ID"];		// Int NULL FOREIGN KEY REFERENCES Sales.Invoice_Payment_Checks(Payment_Check_ID)
					if (row["Accounting_ID"] != DBNull.Value) item.InvoiceAccountingID = (string)row["Accounting_ID"];		// VarChar(32) NULL
					item.InvoiceIssueDate = (DateTime)row["Issue_Date"];													// SmallDateTime NOT NULL
					item.CustomerName = (string)row["Customer_Name"];														// NVarChar(256) NOT NULL
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];			// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];							// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		/// <summary>
		///		<para>Get the payments recorded during a certain period</para>
		/// </summary>
		/// <param name="startingWith">The beginnig date of the period considered</param>
		/// <param name="endingWith">The ending date of the period considered</param>
		/// <returns></returns>
		public static async Task<List<PaymentModel>> ListPaymentsDuringAsync(DateTime startingWith, DateTime endingWith)
		{
			PaymentModel item;
			List<PaymentModel> list = new List<PaymentModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@StartingWith", SqlDbType.SmallDateTime).Value = startingWith;
				db.AddParam("@EndingWith", SqlDbType.SmallDateTime).Value = endingWith;
				row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Payments_Grid_2");
				while (await rdrReader.ReadAsync())
				{
					item = new PaymentModel();
					item.ID = (int)row["Payment_ID"];																		// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.InvoiceID = (int)row["Invoice_ID"];																// Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					item.PaymentDate = (DateTime)row["Payment_Date"];														// SmallDateTime NOT NULL DEFAULT GETDATE()
					item.Amount = (decimal)row["Amount"];																	// Decimal(24, 8) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];			                                                    // Char(3) NOT NULL DEFAULT 'EUR'
					if (row["Comments"] != DBNull.Value) item.Comments = (string)row["Comments"];							// NVarChar(MAX) NULL
					if (row["Payment_Check_ID"] != DBNull.Value) item.PaymentCheckID = (int)row["Payment_Check_ID"];		// Int NULL FOREIGN KEY REFERENCES Sales.Invoice_Payment_Checks(Payment_Check_ID)
					if (row["Accounting_ID"] != DBNull.Value) item.InvoiceAccountingID = (string)row["Accounting_ID"];		// VarChar(32) NULL
					item.InvoiceIssueDate = (DateTime)row["Issue_Date"];													// SmallDateTime NOT NULL
					item.CustomerName = (string)row["Customer_Name"];														// NVarChar(256) NOT NULL
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];			// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];							// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.UpdatedOn = (DateTime)row["Updated_On"];															// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
		public static async Task<bool> DeleteAsync(int paymentID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@RecordID", SqlDbType.Int).Value = paymentID;
				db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
				db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
				if ((await db.DeleteRecordAsync("Billing.Payments_Del")) > 0) success = true;
			}
			return success;
		}
	}
}
