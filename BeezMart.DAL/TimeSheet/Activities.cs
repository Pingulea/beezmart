﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BeezMart.Entities.TimeSheet;
using System.Text;

namespace BeezMart.DAL.TimeSheet
{
    public static class Activities
    {
		public static class ActivityTypes
		{
			public static async Task<bool> SaveAsync(ActivityModel.ActivityTypeModel activityType)
			{
				bool success = false;
				using (Database db = new Database())
				{
					if (activityType.ID.HasValue)
					{
						db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activityType.ID.Value;					// SmallInt
						db.AddParam("@ActivityTypeName", SqlDbType.NVarChar, 64).Value = activityType.Name;					// NVarChar(64)
						db.AddParam("@ActivityTypeCategory", SqlDbType.NVarChar, 64).Value = activityType.Category;			// NVarChar(64) = NULL
						db.AddParam("@ActivityTypeComments", SqlDbType.NText).Value = activityType.Comments;				// NVarChar(MAX) = NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = activityType.IsDiscontinued;					// Bit NOT NULL = 0
						db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;							// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
						db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = activityType.UpdatedBy;					// NVarChar(128) = NULL
						if ((await db.SaveRecordAsync("Sales.Activity_Types_Upd")) > 0) success = true;
					}
					else
					{
						db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;			// SmallInt OUTPUT
						db.AddParam("@ActivityTypeName", SqlDbType.NVarChar, 64).Value = activityType.Name;					// NVarChar(64)
						db.AddParam("@ActivityTypeCategory", SqlDbType.NVarChar, 64).Value = activityType.Category;			// NVarChar(64) = NULL
						db.AddParam("@ActivityTypeComments", SqlDbType.NText).Value = activityType.Comments;				// NVarChar(MAX) = NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = activityType.IsDiscontinued;					// Bit NOT NULL = 0
						db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = DateTime.Now;							// SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
						db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = activityType.UpdatedBy;					// NVarChar(128) = NULL
						if ((await db.SaveRecordAsync("Sales.Activity_Types_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
						if (success) activityType.ID = (short)db.Params["@ActivityTypeID"].Value;
					}
				}
				return success;
			}

			public static async Task<ActivityModel.ActivityTypeModel?> RetrieveAsync(short activityTypeID)
			{
				ActivityModel.ActivityTypeModel? entity = null;
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activityTypeID;
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Activity_Types_Sel");
					if (await rdrReader.ReadAsync())
					{
						entity = new ActivityModel.ActivityTypeModel();
						entity.ID = (short)row["Activity_Type_ID"];																		// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
						entity.Name = (string)row["Activity_Type_Name"];																// NVarChar(64) NOT NULL
						if (row["Activity_Type_Category"] != DBNull.Value) entity.Category = (string)row["Activity_Type_Category"];		// NVarChar(64) NULL
						if (row["Activity_Type_Comments"] != DBNull.Value) entity.Comments = (string)row["Activity_Type_Comments"];		// NVarChar(MAX) NULL
						entity.IsDiscontinued = (bool)row["Discontinued"];																// Bit NOT NULL DEFAULT 0
						entity.UpdatedOn = (DateTime)row["Updated_On"];																	// SmallDateTime NOT NULL DEFAULT GETDATE()
						if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];							// NVarChar(128) NULL
					}
					rdrReader.Dispose();
				}
				return entity;
			}

			/// <summary>
			///		<para>Gets the list of available activity categories.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<string>> ListCategoriesAsync()
			{
				List<string> list = new List<string>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Activity_Types_Grid_3");
					while (await rdrReader.ReadAsync())
					{
						if (row[0] != DBNull.Value) list.Add((string)row[0]);			// NVarChar(64) NULL
					}
					rdrReader.Dispose();
				}
				return list;
			}

			/// <summary>
			///		<para>Gets the list of available activity types.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<ActivityModel.ActivityTypeModel>> ListAsync()
			{
				ActivityModel.ActivityTypeModel item;
				List<ActivityModel.ActivityTypeModel> list = new List<ActivityModel.ActivityTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Activity_Types_Grid_1");
					while (await rdrReader.ReadAsync())
					{
						item = new ActivityModel.ActivityTypeModel();
						item.ID = (short)row["Activity_Type_ID"];																		// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
						item.Name = (string)row["Activity_Type_Name"];																	// NVarChar(64) NOT NULL
						if (row["Activity_Type_Category"] != DBNull.Value) item.Category = (string)row["Activity_Type_Category"];		// NVarChar(64) NULL
						if (row["Activity_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Activity_Type_Comments"];		// NVarChar(MAX) NULL
						item.IsDiscontinued = (bool)row["Discontinued"];																// Bit NOT NULL DEFAULT 0
						item.UpdatedOn = (DateTime)row["Updated_On"];																	// SmallDateTime NOT NULL DEFAULT GETDATE()
						if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];								// NVarChar(128) NULL
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			/// <summary>
			///		<para>Gets the list of all activity types, including the retired ones.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<ActivityModel.ActivityTypeModel>> ListAllAsync()
			{
				ActivityModel.ActivityTypeModel item;
				List<ActivityModel.ActivityTypeModel> list = new List<ActivityModel.ActivityTypeModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Activity_Types_Grid_2");
					while (await rdrReader.ReadAsync())
					{
						item = new ActivityModel.ActivityTypeModel();
						item.ID = (short)row["Activity_Type_ID"];																		// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
						item.Name = (string)row["Activity_Type_Name"];																	// NVarChar(64) NOT NULL
						if (row["Activity_Type_Category"] != DBNull.Value) item.Category = (string)row["Activity_Type_Category"];		// NVarChar(64) NULL
						if (row["Activity_Type_Comments"] != DBNull.Value) item.Comments = (string)row["Activity_Type_Comments"];		// NVarChar(MAX) NULL
						item.IsDiscontinued = (bool)row["Discontinued"];																// Bit NOT NULL DEFAULT 0
						item.UpdatedOn = (DateTime)row["Updated_On"];																	// SmallDateTime NOT NULL DEFAULT GETDATE()
						if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];								// NVarChar(128) NULL
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			public static async Task<bool> DeleteAsync(short activityTypeID, bool isDiscontinued = true, string? updatedBy = null, DateTime? updatedOn = null)
			{
				bool success = false;
				using (Database db = new Database())
				{
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activityTypeID;		// SmallInt
					db.AddParam("@Discontinued", SqlDbType.Bit).Value = isDiscontinued;				// Bit NOT NULL = 1
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;			// SmallDateTime DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;			// NVarChar(128) = NULL
					if ((await db.DeleteRecordAsync("Sales.Activity_Types_Del")) > 0) success = true;
				}
				return success;
			}
		}

		public static async Task<bool> SaveAsync(ActivityModel activity)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (activity.ID.HasValue)
				{
					db.AddParam("@ActivityID", SqlDbType.BigInt).Value = activity.ID;												// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					db.AddParam("@ProjectID", SqlDbType.Int).Value = activity.ProjectID;											// Int = NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = activity.OrganizationID;									// Int = NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					db.AddParam("@PersonID", SqlDbType.Int).Value = activity.PersonID;												// Int = NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = activity.CustomerName;							// NVarChar(256) NOT NULL
					db.AddParam("@EndCustomer", SqlDbType.NVarChar, 256).Value = activity.EndCustomer;								// NVarChar(256) = NULL
					db.AddParam("@ActivityStatusID", SqlDbType.TinyInt).Value = activity.Status;									// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					db.AddParam("@ActivityStatusComment", SqlDbType.NVarChar, 256).Value = activity.ActivityStatusComment;			// NVarChar(256) = NULL
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activity.ActivityTypeID;								// SmallInt = NULL NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = activity.Title;											// NVarChar(1024) NOT NULL
					db.AddParam("@Details", SqlDbType.NText).Value = activity.Details;												// NVarChar(MAX) = NULL
					db.AddParam("@ScheduledStart", SqlDbType.SmallDateTime).Value = activity.ScheduledStart;						// SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
					db.AddParam("@ScheduledEnd", SqlDbType.SmallDateTime).Value = activity.ScheduledEnd;							// SmallDateTime = NULL NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					db.AddParam("@AssignedOn", SqlDbType.SmallDateTime).Value = activity.AssignedOn;								// SmallDateTime = NULL
					db.AddParam("@AssignedTo", SqlDbType.NVarChar, 128).Value = activity.AssignedTo;								// NVarChar(128) = NULL
					db.AddParam("@DeadlineOn", SqlDbType.SmallDateTime).Value = activity.DeadlineOn;								// SmallDateTime = NULL
					db.AddParam("@StartedOn", SqlDbType.SmallDateTime).Value = activity.StartedOn;									// SmallDateTime = NULL
					db.AddParam("@EndedOn", SqlDbType.SmallDateTime).Value = activity.EndedOn;										// SmallDateTime = NULL
					db.AddParam("@ReminderOn", SqlDbType.SmallDateTime).Value = activity.ReminderOn;								// SmallDateTime = NULL
					db.AddParam("@RequestedBy", SqlDbType.NVarChar, 128).Value = activity.RequestedBy;								// NVarChar(128) = NULL
					db.AddParam("@RequestingDepartment", SqlDbType.NVarChar, 128).Value = activity.RequestingDepartment;			// NVarChar(128) = NULL
					db.AddParam("@CompletedBy", SqlDbType.NVarChar, 128).Value = activity.CompletedBy;								// NVarChar(128) = NULL
					db.AddParam("@LoggedBy", SqlDbType.NVarChar, 128).Value = activity.LoggedBy;									// NVarChar(128) = NULL
					db.AddParam("@Billable", SqlDbType.Bit).Value = activity.Billable;												// Bit NOT NULL DEFAULT 0
					db.AddParam("@InvoiceIDs", SqlDbType.VarChar, 256).Value = activity.InvoiceIDs;									// VarChar(256) = NULL
					db.AddParam("@DurationInMinutes", SqlDbType.Int).Value = activity.Duration?.TotalMinutes;						// Int = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = activity.UpdatedOn;									// SmallDateTime NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = activity.UpdatedBy;									// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Sales.Activities_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@ActivityID", SqlDbType.BigInt).Direction = ParameterDirection.Output;								// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					db.AddParam("@ProjectID", SqlDbType.Int).Value = activity.ProjectID;											// Int = NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = activity.OrganizationID;									// Int = NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					db.AddParam("@PersonID", SqlDbType.Int).Value = activity.PersonID;												// Int = NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = activity.CustomerName;							// NVarChar(256) NOT NULL
					db.AddParam("@EndCustomer", SqlDbType.NVarChar, 256).Value = activity.EndCustomer;								// NVarChar(256) = NULL
					db.AddParam("@ActivityStatusID", SqlDbType.TinyInt).Value = activity.Status;									// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					db.AddParam("@ActivityStatusComment", SqlDbType.NVarChar, 256).Value = activity.ActivityStatusComment;			// NVarChar(256) = NULL
					db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = activity.ActivityTypeID;								// SmallInt = NULL NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = activity.Title;											// NVarChar(1024) NOT NULL
					db.AddParam("@Details", SqlDbType.NText).Value = activity.Details;												// NVarChar(MAX) = NULL
					db.AddParam("@ScheduledStart", SqlDbType.SmallDateTime).Value = activity.ScheduledStart;						// SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
					db.AddParam("@ScheduledEnd", SqlDbType.SmallDateTime).Value = activity.ScheduledEnd;							// SmallDateTime = NULL NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					db.AddParam("@AssignedOn", SqlDbType.SmallDateTime).Value = activity.AssignedOn;								// SmallDateTime = NULL
					db.AddParam("@AssignedTo", SqlDbType.NVarChar, 128).Value = activity.AssignedTo;								// NVarChar(128) = NULL
					db.AddParam("@DeadlineOn", SqlDbType.SmallDateTime).Value = activity.DeadlineOn;								// SmallDateTime = NULL
					db.AddParam("@StartedOn", SqlDbType.SmallDateTime).Value = activity.StartedOn;									// SmallDateTime = NULL
					db.AddParam("@EndedOn", SqlDbType.SmallDateTime).Value = activity.EndedOn;										// SmallDateTime = NULL
					db.AddParam("@ReminderOn", SqlDbType.SmallDateTime).Value = activity.ReminderOn;								// SmallDateTime = NULL
					db.AddParam("@RequestedBy", SqlDbType.NVarChar, 128).Value = activity.RequestedBy;								// NVarChar(128) = NULL
					db.AddParam("@RequestingDepartment", SqlDbType.NVarChar, 128).Value = activity.RequestingDepartment;			// NVarChar(128) = NULL
					db.AddParam("@CompletedBy", SqlDbType.NVarChar, 128).Value = activity.CompletedBy;								// NVarChar(128) = NULL
					db.AddParam("@LoggedBy", SqlDbType.NVarChar, 128).Value = activity.LoggedBy;									// NVarChar(128) = NULL
					db.AddParam("@Billable", SqlDbType.Bit).Value = activity.Billable;												// Bit NOT NULL DEFAULT 0
					db.AddParam("@InvoiceIDs", SqlDbType.VarChar, 256).Value = activity.InvoiceIDs;									// VarChar(256) = NULL
					db.AddParam("@DurationInMinutes", SqlDbType.Int).Value = activity.Duration?.TotalMinutes;						// Int = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = activity.UpdatedOn;									// SmallDateTime NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = activity.UpdatedBy;									// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Sales.Activities_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) activity.ID = (long)db.Params["@ActivityID"].Value;
				}
			}
			return success;
		}

		public static async Task<ActivityModel?> RetrieveAsync(long activityID)
		{
			ActivityModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@ActivityID", SqlDbType.BigInt).Value = activityID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Activities_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new ActivityModel();
					entity.ID = (long)row["Activity_ID"];																							// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					if (row["Project_ID"] != DBNull.Value) entity.ProjectID = (int)row["Project_ID"];												// Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];								// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];													// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.CustomerName = (string)row["Customer_Name"];																				// NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) entity.EndCustomer = (string)row["End_Customer"];										// NVarChar(256) NULL
					entity.Status = (ActivityStatus)(byte)row["Activity_Status_ID"];																// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					if (row["Activity_Status_Comment"] != DBNull.Value) entity.ActivityStatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					if (row["Activity_Type_ID"] != DBNull.Value) entity.ActivityTypeID = (short)row["Activity_Type_ID"];							// SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					entity.Title = (string)row["Title"];																							// NVarChar(1024) NOT NULL
					if (row["Details"] != DBNull.Value) entity.Details = (string)row["Details"];													// NVarChar(MAX) NULL
					entity.ScheduledStart = (DateTime)row["Scheduled_Start"];																		// SmallDateTime NOT NULL DEFAULT GETDATE()
					entity.ScheduledEnd = (DateTime)row["Scheduled_End"];																			// SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					if (row["Assigned_On"] != DBNull.Value) entity.AssignedOn = (DateTime)row["Assigned_On"];										// SmallDateTime NULL
					if (row["Assigned_To"] != DBNull.Value) entity.AssignedTo = (string)row["Assigned_To"];											// NVarChar(128) NULL
					if (row["Deadline_On"] != DBNull.Value) entity.DeadlineOn = (DateTime)row["Deadline_On"];										// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) entity.StartedOn = (DateTime)row["Started_On"];											// SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) entity.EndedOn = (DateTime)row["Ended_On"];												// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) entity.ReminderOn = (DateTime)row["Reminder_On"];										// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) entity.RequestedBy = (string)row["Requested_By"];										// NVarChar(128) NULL
					if (row["Requesting_Department"] != DBNull.Value) entity.RequestingDepartment = (string)row["Requesting_Department"];			// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) entity.CompletedBy = (string)row["Completed_By"];										// NVarChar(128) NULL
					if (row["Logged_By"] != DBNull.Value) entity.LoggedBy = (string)row["Logged_By"];												// NVarChar(128) NULL
					entity.Billable = (bool)row["Billable"];																						// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) entity.InvoiceIDs = (string)row["Invoice_IDs"];											// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) entity.Duration = TimeSpan.FromMinutes((int)row["Duration_In_Minutes"]);        // Int NULL
                    entity.UpdatedOn = (DateTime)row["Updated_On"];																					// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];											// NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

		public static async Task<List<ActivityModel>> ListAsync(int projectID)
		{
			ActivityModel item;
			List<ActivityModel> list = new List<ActivityModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@ProjectID", SqlDbType.Int).Value = projectID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Activities_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new ActivityModel();
					item.ID = (long)row["Activity_ID"];																								// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					if (row["Project_ID"] != DBNull.Value) item.ProjectID = (int)row["Project_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];									// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];													// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																				// NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];										// NVarChar(256) NULL
					if (row["Project_Title"] != DBNull.Value) item.ProjectTitle = (string)row["Project_Title"];										// NVarChar(256) NULL
					item.Status = (ActivityStatus)(byte)row["Activity_Status_ID"];																	// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					//if (row["Activity_Status_Comment"] != DBNull.Value) item.ActivityStatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					if (row["Activity_Type_ID"] != DBNull.Value) item.ActivityTypeID = (short)row["Activity_Type_ID"];								// SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					item.Title = (string)row["Title"];																								// NVarChar(1024) NOT NULL
					//if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];														// NVarChar(MAX) NULL
					if (row["Activity_Type"] != DBNull.Value) item.Details = (string)row["Activity_Type"];											// NVarChar(64) NULL
					if (row["Activity_Category"] != DBNull.Value) item.Details = (string)row["Activity_Category"];									// NVarChar(64) NULL
					item.ScheduledStart = (DateTime)row["Scheduled_Start"];																			// SmallDateTime NOT NULL DEFAULT GETDATE()
					item.ScheduledEnd = (DateTime)row["Scheduled_End"];																				// SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					if (row["Assigned_On"] != DBNull.Value) item.AssignedOn = (DateTime)row["Assigned_On"];											// SmallDateTime NULL
					if (row["Assigned_To"] != DBNull.Value) item.AssignedTo = (string)row["Assigned_To"];											// NVarChar(128) NULL
					if (row["Deadline_On"] != DBNull.Value) item.DeadlineOn = (DateTime)row["Deadline_On"];											// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];											// SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];													// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) item.ReminderOn = (DateTime)row["Reminder_On"];											// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) item.RequestedBy = (string)row["Requested_By"];										// NVarChar(128) NULL
					if (row["Requesting_Department"] != DBNull.Value) item.RequestingDepartment = (string)row["Requesting_Department"];				// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) item.CompletedBy = (string)row["Completed_By"];										// NVarChar(128) NULL
					if (row["Logged_By"] != DBNull.Value) item.LoggedBy = (string)row["Logged_By"];													// NVarChar(128) NULL
					item.Billable = (bool)row["Billable"];																							// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) item.InvoiceIDs = (string)row["Invoice_IDs"];											// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) item.Duration = TimeSpan.FromMinutes((int)row["Duration_In_Minutes"]);          // Int NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];																					// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];												// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<List<ActivityModel>> ListAsync(DateTime? timeframeStart, DateTime? timeframeEnd, string? completedBy = null)
		{
			ActivityModel item;
			List<ActivityModel> list = new List<ActivityModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
                db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = timeframeStart;             // SmallDateTime = NULL
                db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = timeframeEnd;                 // SmallDateTime = NULL
                if (string.IsNullOrWhiteSpace(completedBy))
                {
                    row = rdrReader = db.GetSqlDataReader("Sales.Activities_Grid_2");
                }
                else
                {
                    db.AddParam("@CompletedBy", SqlDbType.NVarChar, 128).Value = completedBy;               // NVarChar(128) = NULL
                    row = rdrReader = db.GetSqlDataReader("Sales.Activities_Grid_3");
                }
                while (await rdrReader.ReadAsync())
				{
					item = new ActivityModel();
					item.ID = (long)row["Activity_ID"];																								// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					if (row["Project_ID"] != DBNull.Value) item.ProjectID = (int)row["Project_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];									// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];													// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																				// NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];										// NVarChar(256) NULL
					if (row["Project_Title"] != DBNull.Value) item.ProjectTitle = (string)row["Project_Title"];										// NVarChar(256) NULL
					item.Status = (ActivityStatus)(byte)row["Activity_Status_ID"];																	// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					//if (row["Activity_Status_Comment"] != DBNull.Value) item.ActivityStatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					if (row["Activity_Type_ID"] != DBNull.Value) item.ActivityTypeID = (short)row["Activity_Type_ID"];								// SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					item.Title = (string)row["Title"];																								// NVarChar(1024) NOT NULL
					//if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];														// NVarChar(MAX) NULL
					if (row["Activity_Type"] != DBNull.Value) item.Details = (string)row["Activity_Type"];											// NVarChar(64) NULL
					if (row["Activity_Category"] != DBNull.Value) item.Details = (string)row["Activity_Category"];									// NVarChar(64) NULL
					item.ScheduledStart = (DateTime)row["Scheduled_Start"];																			// SmallDateTime NOT NULL DEFAULT GETDATE()
					item.ScheduledEnd = (DateTime)row["Scheduled_End"];																				// SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					if (row["Assigned_On"] != DBNull.Value) item.AssignedOn = (DateTime)row["Assigned_On"];											// SmallDateTime NULL
					if (row["Assigned_To"] != DBNull.Value) item.AssignedTo = (string)row["Assigned_To"];											// NVarChar(128) NULL
					if (row["Deadline_On"] != DBNull.Value) item.DeadlineOn = (DateTime)row["Deadline_On"];											// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];											// SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];													// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) item.ReminderOn = (DateTime)row["Reminder_On"];											// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) item.RequestedBy = (string)row["Requested_By"];										// NVarChar(128) NULL
					if (row["Requesting_Department"] != DBNull.Value) item.RequestingDepartment = (string)row["Requesting_Department"];				// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) item.CompletedBy = (string)row["Completed_By"];										// NVarChar(128) NULL
					if (row["Logged_By"] != DBNull.Value) item.LoggedBy = (string)row["Logged_By"];													// NVarChar(128) NULL
					item.Billable = (bool)row["Billable"];																							// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) item.InvoiceIDs = (string)row["Invoice_IDs"];											// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) item.Duration = TimeSpan.FromMinutes((int)row["Duration_In_Minutes"]);		    // Int NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];																					// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];												// NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}

		public static async Task<bool> DeleteAsync(long activityID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@ActivityID", SqlDbType.BigInt).Value = activityID;
				if ((await db.DeleteRecordAsync("Sales.Activities_Del")) > 0) success = true;
			}
			return success;
		}

		public static async Task<List<ActivityModel>> LookupAsync(ActivityModel.LookupFilter filterCriteria, short maxRecordsCount = 1000)
		{
			ActivityModel item;
			List<ActivityModel> list = new List<ActivityModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				StringBuilder stbTransactSqlCommand = new StringBuilder();
				#region Build Database Query
				StringBuilder stbSelectClause = new StringBuilder();
				StringBuilder stbFromClause = new StringBuilder();
				StringBuilder stbWhereClause = new StringBuilder();
				StringBuilder stbOrderByClause = new StringBuilder();
				#region Build SELECT clause
				stbSelectClause.AppendLine(filterCriteria.HasAnyCriteria ? $"SELECT TOP {maxRecordsCount}" : $"SELECT TOP {Defaults.Search_Result_Unfiltered_MaxCount}");
				stbSelectClause.AppendLine(" A.Activity_ID");
				stbSelectClause.AppendLine(",A.Project_ID");
				stbSelectClause.AppendLine(",P.Title AS Project_Title");
				stbSelectClause.AppendLine(",A.Organization_ID");
				stbSelectClause.AppendLine(",A.Person_ID");
				stbSelectClause.AppendLine(",A.Customer_Name");
				stbSelectClause.AppendLine(",A.End_Customer");
				stbSelectClause.AppendLine(",A.Activity_Status_ID");
				//stbSelectClause.AppendLine(",A.Activity_Status_Comment");
				stbSelectClause.AppendLine(",A.Activity_Type_ID");
				stbSelectClause.AppendLine(",T.Activity_Type_Name");
				stbSelectClause.AppendLine(",T.Activity_Type_Category");
				stbSelectClause.AppendLine(",A.Title");
				//stbSelectClause.AppendLine(",A.Details");
				stbSelectClause.AppendLine(",A.Scheduled_Start");
				stbSelectClause.AppendLine(",A.Scheduled_End");
				stbSelectClause.AppendLine(",A.Assigned_On");
				stbSelectClause.AppendLine(",A.Assigned_To");
				stbSelectClause.AppendLine(",A.Deadline_On");
				stbSelectClause.AppendLine(",A.Started_On");
				stbSelectClause.AppendLine(",A.Ended_On");
				stbSelectClause.AppendLine(",A.Reminder_On");
				stbSelectClause.AppendLine(",A.Requested_By");
				stbSelectClause.AppendLine(",A.Requesting_Department");
				stbSelectClause.AppendLine(",A.Completed_By");
				stbSelectClause.AppendLine(",A.Logged_By");
				stbSelectClause.AppendLine(",A.Billable");
				stbSelectClause.AppendLine(",A.Invoice_IDs");
				stbSelectClause.AppendLine(",A.Duration_In_Minutes");
				stbSelectClause.AppendLine(",A.Updated_On");
				stbSelectClause.AppendLine(",A.Updated_By");
				#endregion
				#region Build FROM clause
				stbFromClause.AppendLine("FROM Sales.Activities A");
                stbFromClause.AppendLine("    LEFT JOIN Sales.Activity_Types T ON A.Activity_Type_ID = T.Activity_Type_ID");
                stbFromClause.AppendLine("    LEFT JOIN Sales.Projects P ON A.Project_ID = P.Project_ID");
                #endregion
                #region Build WHERE clause
                string searchOperator = filterCriteria.SearchOperator.ToString().ToUpper();
                if (!string.IsNullOrWhiteSpace(filterCriteria.TitlePattern))
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Title LIKE @Title");
                    db.AddParam("@Title", SqlDbType.NVarChar, 1024).Value = filterCriteria.TitleExactMatch ? filterCriteria.TitlePattern : "%" + filterCriteria.TitlePattern + "%";
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.CustomerNamePattern))
				{
					stbWhereClause.AppendLine($"{searchOperator} (A.Customer_Name LIKE @CustomerName OR A.End_Customer LIKE @CustomerName)");
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = "%" + filterCriteria.CustomerNamePattern + "%";
                }
                if (filterCriteria.OrganizationID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Organization_ID = @OrganizationID");
                    db.AddParam("@OrganizationID", SqlDbType.Int).Value = filterCriteria.OrganizationID.Value;
                }
                if (filterCriteria.PersonID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Person_ID = @PersonID");
                    db.AddParam("@PersonID", SqlDbType.Int).Value = filterCriteria.PersonID.Value;
                }
                if (filterCriteria.ActivityTypeID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Activity_Type_ID = @ActivityTypeID");
                    db.AddParam("@ActivityTypeID", SqlDbType.SmallInt).Value = filterCriteria.ActivityTypeID.Value;
                }
                if (filterCriteria.Status.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Activity_Status_ID = @ActivityStatusID");
                    db.AddParam("@ActivityStatusID", SqlDbType.TinyInt).Value = (byte)filterCriteria.Status;
                }
                if (filterCriteria.ProjectID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Project_ID = @ProjectID");
                    db.AddParam("@ProjectID", SqlDbType.Int).Value = filterCriteria.ProjectID.Value;
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.ProjectTitlePattern))
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Project_ID IN (SELECT P1.Project_ID FROM Sales.Projects P1 WHERE P1.Title LIKE @ProjectTitle)");
                    db.AddParam("@ProjectTitle", SqlDbType.NVarChar, 256).Value = "%" + filterCriteria.ProjectTitlePattern + "%";
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.RequestorNamePattern))
				{
					stbWhereClause.AppendLine($"{searchOperator} A.Requested_By LIKE @RequestorNamePattern");
					db.AddParam("@RequestorNamePattern", SqlDbType.NVarChar, 128).Value = "%" + filterCriteria.RequestorNamePattern + "%";
				}
                if (!string.IsNullOrWhiteSpace(filterCriteria.RequestingDepartment))
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Requesting_Department LIKE @RequestingDepartment");
                    db.AddParam("@RequestingDepartment", SqlDbType.NVarChar, 128).Value = "%" + filterCriteria.RequestingDepartment + "%";
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.CompletedBy))
                {
                    stbWhereClause.AppendLine($"{searchOperator} A.Completed_By LIKE @CompletedBy");
                    db.AddParam("@CompletedBy", SqlDbType.NVarChar, 128).Value = "%" + filterCriteria.CompletedBy + "%";
                }
                if (filterCriteria.TimeframeStart.HasValue)
				{
					stbWhereClause.AppendLine($"{searchOperator} (A.Scheduled_Start >= @TimeframeStart OR A.Started_On >= @TimeframeStart)");
					db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = filterCriteria.TimeframeStart.Value;
				}
				if (filterCriteria.TimeframeEnd.HasValue)
				{
					stbWhereClause.AppendLine($"{searchOperator} (A.Scheduled_End <= @TimeframeEnd OR A.Ended_On <= @TimeframeEnd)");
					db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = filterCriteria.TimeframeEnd.Value;
				}

				if (stbWhereClause.Length > 0)
				{
					stbWhereClause.Remove(0, searchOperator.Length + 1);
					stbWhereClause.Insert(0, "WHERE ");
				}
				#endregion
				#region Build ORDER BY clause
				stbOrderByClause.AppendLine("ORDER BY A.Project_ID DESC");
				#endregion
				stbTransactSqlCommand.AppendLine(stbSelectClause.ToString());
				stbTransactSqlCommand.AppendLine(stbFromClause.ToString());
				stbTransactSqlCommand.AppendLine(stbWhereClause.ToString());
				stbTransactSqlCommand.AppendLine(stbOrderByClause.ToString());
				#endregion
				row = rdrReader = await db.GetSqlDataReaderFromTextCommandAsync(stbTransactSqlCommand.ToString());
				while (await rdrReader.ReadAsync())
				{
					item = new ActivityModel();
                    item.ID = (long)row["Activity_ID"];																								// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					if (row["Project_ID"] != DBNull.Value) item.ProjectID = (int)row["Project_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					if (row["Project_Title"] != DBNull.Value) item.ProjectTitle = (string)row["Project_Title"];										// NVarChar(256)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];									// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];													// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																				// NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];										// NVarChar(256) NULL
					item.Status = (ActivityStatus)(byte)row["Activity_Status_ID"];																	// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					//if (row["Activity_Status_Comment"] != DBNull.Value) item.ActivityStatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					if (row["Activity_Type_ID"] != DBNull.Value) item.ActivityTypeID = (short)row["Activity_Type_ID"];								// SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					if (row["Activity_Type_Name"] != DBNull.Value) item.ActivityTypeName = (string)row["Activity_Type_Name"];							// NVarChar(64)
					if (row["Activity_Type_Category"] != DBNull.Value) item.ActivityCategory = (string)row["Activity_Type_Category"];				// NVarChar(64)
					item.Title = (string)row["Title"];																								// NVarChar(1024) NOT NULL
					//if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];													// NVarChar(MAX) NULL
					item.ScheduledStart = (DateTime)row["Scheduled_Start"];																			// SmallDateTime NOT NULL DEFAULT GETDATE()
					item.ScheduledEnd = (DateTime)row["Scheduled_End"];																				// SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					if (row["Assigned_On"] != DBNull.Value) item.AssignedOn = (DateTime)row["Assigned_On"];											// SmallDateTime NULL
					if (row["Assigned_To"] != DBNull.Value) item.AssignedTo = (string)row["Assigned_To"];											// NVarChar(128) NULL
					if (row["Deadline_On"] != DBNull.Value) item.DeadlineOn = (DateTime)row["Deadline_On"];											// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];											// SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];													// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) item.ReminderOn = (DateTime)row["Reminder_On"];											// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) item.RequestedBy = (string)row["Requested_By"];										// NVarChar(128) NULL
					if (row["Requesting_Department"] != DBNull.Value) item.RequestingDepartment = (string)row["Requesting_Department"];				// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) item.CompletedBy = (string)row["Completed_By"];										// NVarChar(128) NULL
					if (row["Logged_By"] != DBNull.Value) item.LoggedBy = (string)row["Logged_By"];													// NVarChar(128) NULL
					item.Billable = (bool)row["Billable"];																							// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) item.InvoiceIDs = (string)row["Invoice_IDs"];											// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) item.Duration = TimeSpan.FromMinutes((int)row["Duration_In_Minutes"]);			// Int NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];																					// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                              				// NVarChar(128) NULL
                    list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
        }
    }
}
