﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BeezMart.Entities.TimeSheet;
using System.Text;

namespace BeezMart.DAL.TimeSheet
{
    public static class Projects
    {
		public class Categories
		{
			public static async Task<bool> SaveAsync(ProjectModel.CategoryModel category)
			{
				bool success = false;
				using (Database db = new Database())
				{
					if (category.ID.HasValue)
					{
						db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = category.ID;								// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
						db.AddParam("@CategoryName", SqlDbType.NVarChar, 128).Value = category.Name;					// NVarChar(128) NOT NULL
						db.AddParam("@SubCategory", SqlDbType.NVarChar, 128).Value = category.SubCategory;				// NVarChar(128) NOT NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = category.IsDiscontinued;					// Bit = 0 NOT NULL DEFAULT 0
						if ((await db.SaveRecordAsync("Sales.Project_Categories_Upd")) > 0) success = true;
					}
					else
					{
						db.AddParam("@CategoryID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;			// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
						db.AddParam("@CategoryName", SqlDbType.NVarChar, 128).Value = category.Name;					// NVarChar(128) NOT NULL
						db.AddParam("@SubCategory", SqlDbType.NVarChar, 128).Value = category.SubCategory;				// NVarChar(128) NOT NULL
						db.AddParam("@Discontinued", SqlDbType.Bit).Value = category.IsDiscontinued;					// Bit = 0 NOT NULL DEFAULT 0
						if ((await db.SaveRecordAsync("Sales.Project_Categories_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
						if (success) category.ID = (short)db.Params["@CategoryID"].Value;
					}
				}
				return success;
			}

			public static async Task<ProjectModel.CategoryModel?> RetrieveAsync(short categoryID)
			{
				ProjectModel.CategoryModel? entity = null;
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = categoryID;
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Project_Categories_Sel");
					if (await rdrReader.ReadAsync())
					{
						entity = new ProjectModel.CategoryModel();
						entity.ID = (short)row["Category_ID"];							// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
						entity.Name = (string)row["Category_Name"];						// NVarChar(128) NOT NULL
						entity.SubCategory = (string)row["Sub_Category"];				// NVarChar(128) NOT NULL
						entity.IsDiscontinued = (bool)row["Discontinued"];				// Bit NOT NULL DEFAULT 0
					}
					rdrReader.Dispose();
				}
				return entity;
			}

			/// <summary>
			///		<para>Gets the list of available project categories.</para>
			///		<para>Non-cached.</para>
			/// </summary>
			/// <returns></returns>
			public static async Task<List<string>> ListAsync()
			{
				List<string> list = new List<string>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Project_Categories_Grid_3");
					while (await rdrReader.ReadAsync())
					{
						if (row[0] != DBNull.Value) list.Add((string)row[0]);			// NVarChar(128) NOT NULL
					}
					rdrReader.Dispose();
				}
				return list;
			}

			public static async Task<List<ProjectModel.CategoryModel>> ListAvailableAsync()
			{
				ProjectModel.CategoryModel item;
				List<ProjectModel.CategoryModel> list = new List<ProjectModel.CategoryModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Project_Categories_Grid_1");
					while (await rdrReader.ReadAsync())
					{
						item = new ProjectModel.CategoryModel();
						item.ID = (short)row["Category_ID"];								// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
						item.Name = (string)row["Category_Name"];							// NVarChar(128) NOT NULL
						item.SubCategory = (string)row["Sub_Category"];						// NVarChar(128) NOT NULL
						item.IsDiscontinued = (bool)row["Discontinued"];					// Bit NOT NULL DEFAULT 0
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			public static async Task<List<ProjectModel.CategoryModel>> ListAllAsync()
			{
				ProjectModel.CategoryModel item;
				List<ProjectModel.CategoryModel> list = new List<ProjectModel.CategoryModel>();
				SqlDataReader rdrReader, row;
				using (Database db = new Database())
				{
					row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Project_Categories_Grid_2");
					while (await rdrReader.ReadAsync())
					{
						item = new ProjectModel.CategoryModel();
						item.ID = (short)row["Category_ID"];								// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
						item.Name = (string)row["Category_Name"];							// NVarChar(128) NOT NULL
						item.SubCategory = (string)row["Sub_Category"];						// NVarChar(128) NOT NULL
						item.IsDiscontinued = (bool)row["Discontinued"];					// Bit NOT NULL DEFAULT 0
						list.Add(item);
					}
					rdrReader.Dispose();
				}
				return list;
			}

			public static async Task<bool> DeleteAsync(short categoryID, bool discontinued = true, string? updatedBy = null, DateTime? updatedOn = null)
			{
				bool success = false;
				using (Database db = new Database())
				{
					db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = categoryID;			// SmallInt				-- NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
					db.AddParam("@Discontinued", SqlDbType.Bit).Value = discontinued;			// Bit = 1				-- NOT NULL DEFAULT 0
					if ((await db.DeleteRecordAsync("Sales.Project_Categories_Del")) > 0) success = true;
				}
				return success;
			}
		}

		public static async Task<bool> SaveAsync(ProjectModel project)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (project.ID.HasValue)
				{
					db.AddParam("@ProjectID", SqlDbType.Int).Value = project.ID;									// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = project.Title;							// NVarChar(256) NOT NULL
					db.AddParam("@ProjectManager", SqlDbType.NVarChar, 256).Value = project.ProjectManager;			// NVarChar(256)	= NULL
					db.AddParam("@ContactDetails", SqlDbType.NVarChar, 256).Value = project.ContactDetails;			// NVarChar(256)	= NULL
					db.AddParam("@Details", SqlDbType.NText).Value = project.Details;								// NVarChar(MAX) = NULL
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = project.OrganizationID;					// Int = NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					db.AddParam("@PersonID", SqlDbType.Int).Value = project.PersonID;								// Int = NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = project.CustomerName;				// NVarChar(256) NOT NULL
					db.AddParam("@EndCustomer", SqlDbType.NVarChar, 256).Value = project.EndCustomer;				// NVarChar(256) = NULL
					db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = project.CategoryID;						// SmallInt = NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
					db.AddParam("@ProjectStatusID", SqlDbType.TinyInt).Value = project.Status;						// TinyInt = NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
					db.AddParam("@ScheduledStart", SqlDbType.SmallDateTime).Value = project.ScheduledStart;			// SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
					db.AddParam("@ScheduledEnd", SqlDbType.SmallDateTime).Value = project.ScheduledEnd;				// SmallDateTime = NULL NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
					db.AddParam("@StartedOn", SqlDbType.SmallDateTime).Value = project.StartedOn;					// SmallDateTime = NULL
					db.AddParam("@EndedOn", SqlDbType.SmallDateTime).Value = project.EndedOn;						// SmallDateTime = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = project.UpdatedOn;					// SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = project.UpdatedBy;					// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Sales.Projects_Upd")) > 0) success = true;
				}
				else
				{
					db.AddParam("@ProjectID", SqlDbType.Int).Direction = ParameterDirection.Output;					// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = project.Title;							// NVarChar(256) NOT NULL
					db.AddParam("@ProjectManager", SqlDbType.NVarChar, 256).Value = project.ProjectManager;			// NVarChar(256)	= NULL
					db.AddParam("@ContactDetails", SqlDbType.NVarChar, 256).Value = project.ContactDetails;			// NVarChar(256)	= NULL
					db.AddParam("@Details", SqlDbType.NText).Value = project.Details;								// NVarChar(MAX) = NULL
					db.AddParam("@OrganizationID", SqlDbType.Int).Value = project.OrganizationID;					// Int = NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					db.AddParam("@PersonID", SqlDbType.Int).Value = project.PersonID;								// Int = NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = project.CustomerName;				// NVarChar(256) NOT NULL
					db.AddParam("@EndCustomer", SqlDbType.NVarChar, 256).Value = project.EndCustomer;				// NVarChar(256) = NULL
					db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = project.CategoryID;						// SmallInt = NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
					db.AddParam("@ProjectStatusID", SqlDbType.TinyInt).Value = project.Status;						// TinyInt = NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
					db.AddParam("@ScheduledStart", SqlDbType.SmallDateTime).Value = project.ScheduledStart;			// SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
					db.AddParam("@ScheduledEnd", SqlDbType.SmallDateTime).Value = project.ScheduledEnd;				// SmallDateTime = NULL NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
					db.AddParam("@StartedOn", SqlDbType.SmallDateTime).Value = project.StartedOn;					// SmallDateTime = NULL
					db.AddParam("@EndedOn", SqlDbType.SmallDateTime).Value = project.EndedOn;						// SmallDateTime = NULL
					db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = project.UpdatedOn;					// SmallDateTime = NULL NOT NULL DEFAULT GETDATE()
					db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = project.UpdatedBy;					// NVarChar(128) = NULL
					if ((await db.SaveRecordAsync("Sales.Projects_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) project.ID = (int)db.Params["@ProjectID"].Value;
				}
			}
			return success;
		}

		public static async Task<ProjectModel?> RetrieveAsync(int projectID)
		{
			ProjectModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@ProjectID", SqlDbType.Int).Value = projectID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Projects_Sel");
				if (await rdrReader.ReadAsync())
				{
					entity = new ProjectModel();
					entity.ID = (int)row["Project_ID"];																			        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					entity.Title = (string)row["Title"];																		        // NVarChar(256) NOT NULL
					if (row["Project_Manager"] != DBNull.Value) entity.ProjectManager = (string)row["Project_Manager"];			        // NVarChar(256) NULL
					if (row["Contact_Details"] != DBNull.Value) entity.ContactDetails = (string)row["Contact_Details"];			        // NVarChar(256) NULL
					if (row["Details"] != DBNull.Value) entity.Details = (string)row["Details"];								        // NVarChar(MAX) NULL
					if (row["Organization_ID"] != DBNull.Value) entity.OrganizationID = (int)row["Organization_ID"];			        // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) entity.PersonID = (int)row["Person_ID"];								        // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					entity.CustomerName = (string)row["Customer_Name"];															        // NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) entity.EndCustomer = (string)row["End_Customer"];					        // NVarChar(256) NULL
					if (row["Category_ID"] != DBNull.Value) entity.CategoryID = (short)row["Category_ID"];						        // SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
					if (row["Project_Status_ID"] != DBNull.Value) entity.Status = (ProjectStatus)(byte)row["Project_Status_ID"];        // TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
					entity.ScheduledStart = (DateTime)row["Scheduled_Start"];													        // SmallDateTime NOT NULL DEFAULT GETDATE()
					entity.ScheduledEnd = (DateTime)row["Scheduled_End"];														        // SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
					if (row["Started_On"] != DBNull.Value) entity.StartedOn = (DateTime)row["Started_On"];						        // SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) entity.EndedOn = (DateTime)row["Ended_On"];							        // SmallDateTime NULL
					entity.UpdatedOn = (DateTime)row["Updated_On"];																        // SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];						        // NVarChar(128) NULL
				}
				rdrReader.Dispose();
			}
			return entity;
		}

        /// <summary>
        ///     <para>Gets the projects that have a scheduled or execution timeframe intersecting the timeframe defined by paramaters</para>
        /// </summary>
        /// <param name="timeframeStart">The starting date of the timeframe to look for</param>
        /// <param name="timeframeEnd">The ending date of the timeframe to look for</param>
        /// <returns></returns>
		public static async Task<List<ProjectModel>> ListAsync(DateTime? timeframeStart, DateTime? timeframeEnd)
		{
			ProjectModel item;
			List<ProjectModel> list = new List<ProjectModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = timeframeStart;			// SmallDateTime = NULL
				db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = timeframeEnd;				// SmallDateTime = NULL
				row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Projects_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new ProjectModel();
					item.ID = (int)row["Project_ID"];																		        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.Title = (string)row["Title"];																		        // NVarChar(256) NOT NULL
					if (row["Project_Manager"] != DBNull.Value) item.ProjectManager = (string)row["Project_Manager"];		        // NVarChar(256) NULL
					if (row["Contact_Details"] != DBNull.Value) item.ContactDetails = (string)row["Contact_Details"];		        // NVarChar(256) NULL
					//if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];								    // NVarChar(MAX) NULL
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];			        // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];							        // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];														        // NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];				        // NVarChar(256) NULL
					if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];					        // SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
					if (row["Project_Status_ID"] != DBNull.Value) item.Status = (ProjectStatus)(byte)row["Project_Status_ID"];		// TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
					item.ScheduledStart = (DateTime)row["Scheduled_Start"];													        // SmallDateTime NOT NULL DEFAULT GETDATE()
					item.ScheduledEnd = (DateTime)row["Scheduled_End"];														        // SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];					        // SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];							        // SmallDateTime NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															        // SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						        // NVarChar(128) NULL
					list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
        }

        /// <summary>
        ///     <para>Looks up projects by Title, Customer Name, End Customer, or Project Manager</para>
        /// </summary>
        /// <param name="TitleOrNamePattern">The string to look in project title, customer or project manager names</param>
        /// <param name="ExactMatch">When true, wildcard * will pe prepended and appended to the search TitleOrNamePattern</param>
        /// <returns></returns>
		public static async Task<List<ProjectModel>> ListAsync(string TitleOrNamePattern, bool ExactMatch = false)
        {
            ProjectModel item;
            List<ProjectModel> list = new List<ProjectModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@NamePattern", SqlDbType.NVarChar, 256).Value = ExactMatch ? TitleOrNamePattern : $"%{TitleOrNamePattern}%";
                row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Projects_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new ProjectModel();
                    item.ID = (int)row["Project_ID"];                                                                               // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    item.Title = (string)row["Title"];                                                                              // NVarChar(256) NOT NULL
                    if (row["Project_Manager"] != DBNull.Value) item.ProjectManager = (string)row["Project_Manager"];               // NVarChar(256) NULL
                    if (row["Contact_Details"] != DBNull.Value) item.ContactDetails = (string)row["Contact_Details"];               // NVarChar(256) NULL
                    //if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];                                    // NVarChar(MAX) NULL
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                  // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                    // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
                    item.CustomerName = (string)row["Customer_Name"];                                                               // NVarChar(256) NOT NULL
                    if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];                        // NVarChar(256) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                            // SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
                    if (row["Project_Status_ID"] != DBNull.Value) item.Status = (ProjectStatus)(byte)row["Project_Status_ID"];      // TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
                    item.ScheduledStart = (DateTime)row["Scheduled_Start"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.ScheduledEnd = (DateTime)row["Scheduled_End"];                                                             // SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
                    if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];                            // SmallDateTime NULL
                    if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];                                  // SmallDateTime NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                              // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }

        /// <summary>
        ///     <para>Gets the latest N project records that are not ClosedAndArchived or Cancelled</para>
        /// </summary>
        /// <param name="maximumRowCount">Maximum number of records to retrieve from database table</param>
        /// <returns></returns>
		public static async Task<List<ProjectModel>> ListLatestAsync(int maximumRowCount = 1000)
        {
            ProjectModel item;
            List<ProjectModel> list = new List<ProjectModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@MaximumRowCount", SqlDbType.Int).Value = maximumRowCount;         // Int = 1000
                row = rdrReader = await db.GetSqlDataReaderAsync("Sales.Projects_Grid_3");
                while (await rdrReader.ReadAsync())
                {
                    item = new ProjectModel();
                    item.ID = (int)row["Project_ID"];                                                                               // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    item.Title = (string)row["Title"];                                                                              // NVarChar(256) NOT NULL
                    if (row["Project_Manager"] != DBNull.Value) item.ProjectManager = (string)row["Project_Manager"];               // NVarChar(256) NULL
                    if (row["Contact_Details"] != DBNull.Value) item.ContactDetails = (string)row["Contact_Details"];               // NVarChar(256) NULL
                    //if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];								    // NVarChar(MAX) NULL
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                  // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                    // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
                    item.CustomerName = (string)row["Customer_Name"];                                                               // NVarChar(256) NOT NULL
                    if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];                        // NVarChar(256) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                            // SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
                    if (row["Project_Status_ID"] != DBNull.Value) item.Status = (ProjectStatus)(byte)row["Project_Status_ID"];      // TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
                    item.ScheduledStart = (DateTime)row["Scheduled_Start"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.ScheduledEnd = (DateTime)row["Scheduled_End"];                                                             // SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
                    if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];                            // SmallDateTime NULL
                    if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];                                  // SmallDateTime NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                              // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }

		public static async Task<bool> DeleteAsync(int projectID, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
				db.AddParam("@ProjectID", SqlDbType.Int).Value = projectID;
				if ((await db.DeleteRecordAsync("Sales.Projects_Del")) > 0) success = true;
			}
			return success;
		}

        public static async Task<List<ProjectModel>> LookupAsync(ProjectModel.LookupFilter filterCriteria, short maxRecordsCount = 1000)
        {
            ProjectModel item;
            List<ProjectModel> list = new List<ProjectModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                StringBuilder stbTransactSqlCommand = new StringBuilder();
                #region Build Database Query
                StringBuilder stbSelectClause = new StringBuilder();
                StringBuilder stbFromClause = new StringBuilder();
                StringBuilder stbWhereClause = new StringBuilder();
                StringBuilder stbOrderByClause = new StringBuilder();
                #region Build SELECT clause
                stbSelectClause.AppendLine(filterCriteria.HasAnyCriteria ? $"SELECT TOP {maxRecordsCount}" : $"SELECT TOP {Defaults.Search_Result_Unfiltered_MaxCount}");
                stbSelectClause.AppendLine(" P.Project_ID");
                stbSelectClause.AppendLine(",P.Title");
                stbSelectClause.AppendLine(",P.Project_Manager");
                stbSelectClause.AppendLine(",P.Contact_Details");
                //stbSelectClause.AppendLine(",P.Details");
                stbSelectClause.AppendLine(",P.Organization_ID");
                stbSelectClause.AppendLine(",P.Person_ID");
                stbSelectClause.AppendLine(",P.Customer_Name");
                stbSelectClause.AppendLine(",P.End_Customer");
                stbSelectClause.AppendLine(",P.Category_ID");
				stbSelectClause.AppendLine(",C.Category_Name");
				stbSelectClause.AppendLine(",C.Sub_Category");
                stbSelectClause.AppendLine(",P.Project_Status_ID");
                stbSelectClause.AppendLine(",P.Scheduled_Start");
                stbSelectClause.AppendLine(",P.Scheduled_End");
                stbSelectClause.AppendLine(",P.Started_On");
                stbSelectClause.AppendLine(",P.Ended_On");
                stbSelectClause.AppendLine(",P.Updated_On");
                stbSelectClause.AppendLine(",P.Updated_By");
                #endregion
                #region Build FROM clause
                stbFromClause.AppendLine("FROM Sales.Projects P");
                stbFromClause.AppendLine("    LEFT JOIN Sales.Project_Categories C ON P.Category_ID = C.Category_ID");
                #endregion
                #region Build WHERE clause
                string searchOperator = filterCriteria.SearchOperator.ToString().ToUpper();
                if (!string.IsNullOrWhiteSpace(filterCriteria.TitlePattern))
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Title LIKE @Title");
                    db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = filterCriteria.TitleExactMatch ? filterCriteria.TitlePattern : "%" + filterCriteria.TitlePattern + "%";
                }
                if (filterCriteria.CategoryID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Category_ID = @CategoryID");
                    db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = filterCriteria.CategoryID.Value;
                }
                if (filterCriteria.Status.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Project_Status_ID = @ProjectStatusID");
                    db.AddParam("@ProjectStatusID", SqlDbType.TinyInt).Value = (byte)filterCriteria.Status;
                }
                if (filterCriteria.ProjectIDs != null && filterCriteria.ProjectIDs.Count > 0)
                {
                    StringBuilder stbTemp = new StringBuilder();
                    foreach (int projectID in filterCriteria.ProjectIDs)
                    {
                        stbTemp.Append(projectID);
                        stbTemp.Append(',');
                    }
                    stbTemp.Remove(stbTemp.Length - 1, 1);
                    stbWhereClause.AppendLine($"{searchOperator} P.Project_ID IN ({stbTemp})");
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.CustomerNamePattern))
                {
                    stbWhereClause.AppendLine($"{searchOperator} (P.Customer_Name LIKE @CustomerName OR P.End_Customer LIKE @CustomerName)");
                    db.AddParam("@CustomerName", SqlDbType.NVarChar, 256).Value = "%" + filterCriteria.CustomerNamePattern + "%";
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.ProjectManager))
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Project_Manager LIKE @ProjectManager");
                    db.AddParam("@ProjectManager", SqlDbType.NVarChar, 256).Value = "%" + filterCriteria.ProjectManager + "%";
                }
                if (!string.IsNullOrWhiteSpace(filterCriteria.ContactDetails))
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Contact_Details LIKE @ContactDetails");
                    db.AddParam("@ContactDetails", SqlDbType.NVarChar, 256).Value = "%" + filterCriteria.ContactDetails + "%";
                }
                if (filterCriteria.OrganizationID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Organization_ID = @OrganizationID");
                    db.AddParam("@OrganizationID", SqlDbType.Int).Value = filterCriteria.OrganizationID.Value;
                }
                if (filterCriteria.PersonID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} P.Person_ID = @PersonID");
                    db.AddParam("@PersonID", SqlDbType.Int).Value = filterCriteria.PersonID.Value;
                }
                if (filterCriteria.TimeframeStart.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} (P.Scheduled_Start >= @TimeframeStart OR P.Started_On >= @TimeframeStart)");
                    db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = filterCriteria.TimeframeStart.Value;
                }
                if (filterCriteria.TimeframeEnd.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} (P.Scheduled_End <= @TimeframeEnd OR P.Ended_On <= @TimeframeEnd)");
                    db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = filterCriteria.TimeframeEnd.Value;
                }

                if (stbWhereClause.Length > 0)
                {
                    stbWhereClause.Remove(0, searchOperator.Length + 1);
                    stbWhereClause.Insert(0, "WHERE ");
                }
                #endregion
                #region Build ORDER BY clause
                stbOrderByClause.AppendLine("ORDER BY P.Project_ID DESC");
                #endregion
                stbTransactSqlCommand.AppendLine(stbSelectClause.ToString());
                stbTransactSqlCommand.AppendLine(stbFromClause.ToString());
                stbTransactSqlCommand.AppendLine(stbWhereClause.ToString());
                stbTransactSqlCommand.AppendLine(stbOrderByClause.ToString());
                #endregion
                row = rdrReader = await db.GetSqlDataReaderFromTextCommandAsync(stbTransactSqlCommand.ToString());
                while (await rdrReader.ReadAsync())
                {
                    item = new ProjectModel();
                    item.ID = (int)row["Project_ID"];                                                                               // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    item.Title = (string)row["Title"];                                                                              // NVarChar(256) NOT NULL
                    if (row["Project_Manager"] != DBNull.Value) item.ProjectManager = (string)row["Project_Manager"];               // NVarChar(256) NULL
                    if (row["Contact_Details"] != DBNull.Value) item.ContactDetails = (string)row["Contact_Details"];               // NVarChar(256) NULL
                    //if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];                                    // NVarChar(MAX) NULL
                    if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];                  // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                    if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];                                    // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
                    item.CustomerName = (string)row["Customer_Name"];                                                               // NVarChar(256) NOT NULL
                    if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];                        // NVarChar(256) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                            // SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
					if (row["Category_Name"] != DBNull.Value) item.CategoryName = (string)row["Category_Name"];                     // NVarChar(128) NULL
					if (row["Sub_Category"] != DBNull.Value) item.SubCategory = (string)row["Sub_Category"];                     	// NVarChar(128) NULL
                    if (row["Project_Status_ID"] != DBNull.Value) item.Status = (ProjectStatus)(byte)row["Project_Status_ID"];      // TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
                    item.ScheduledStart = (DateTime)row["Scheduled_Start"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.ScheduledEnd = (DateTime)row["Scheduled_End"];                                                             // SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
                    if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];                            // SmallDateTime NULL
                    if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];                                  // SmallDateTime NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                              // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
    }
}
