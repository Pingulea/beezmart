﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Entities;
public static class Entities
{
    public static async Task<bool> SaveAsync(EntityModel entity)
    {
        bool success = false;
        using (Database db = new Database())
        {
            if (entity.ID.HasValue)
            {
                db.AddParam("@RecordID", SqlDbType.Int).Value = entity.ID.Value;                // Int -- NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                db.AddParam("@MasterID", SqlDbType.Int).Value = entity.MasterID;                // Int NULL FOREIGN KEY REFERENCES Master_Entities(Master_ID)
                db.AddParam("@Name", SqlDbType.NVarChar, 128).Value = entity.Name;              // NVarChar(128)
                db.AddParam("@Category", SqlDbType.NVarChar, 128).Value = entity.Category;      // NVarChar(128)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = entity.UpdatedOn;    // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = entity.UpdatedBy;    // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("_______Dta.Stored_Procedure_UPDATE_______")) > 0) success = true;
            }
            else
            {
                db.AddParam("@RecordID", SqlDbType.Int).Direction = ParameterDirection.Output;  // Int OUTPUT -- NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                db.AddParam("@MasterID", SqlDbType.Int).Value = entity.MasterID;                // Int NULL FOREIGN KEY REFERENCES Master_Entities(Master_ID)
                db.AddParam("@Name", SqlDbType.NVarChar, 128).Value = entity.Name;              // NVarChar(128)
                db.AddParam("@Category", SqlDbType.NVarChar, 128).Value = entity.Category;      // NVarChar(128)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = entity.UpdatedOn;    // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = entity.UpdatedBy;    // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("_______Dta.Stored_Procedure_INSERT_______", clearParamsAfterCompletion: false)) > 0) success = true;
                if (success) entity.ID = (int)db.Params["@RecordID"].Value;
            }
        }
        return success;
    }

    public static async Task<EntityModel?> RetrieveAsync(int entityID)
    {
        EntityModel? entity = null;
        SqlDataReader rdrReader, row;
        using (Database db = new Database())
        {
            db.AddParam("@RecordID", SqlDbType.Int).Value = entityID;
            row = rdrReader = await db.GetSqlDataReaderAsync("_______Dta.Stored_Procedure_SELECT_______");
            if ((await rdrReader.ReadAsync()))
            {
                entity = new EntityModel();
                entity.ID = (int)row["Record_ID"];                                                      // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                if (row["Master_ID"] != DBNull.Value) entity.MasterID = (int)row["Master_ID"];          // Int NULL FOREIGN KEY REFERENCES Dta.Master_Entities(Master_ID)
                entity.Name = (string)row["Name"];                                                      // NVarChar(128) NOT NULL
                if (row["Category"] != DBNull.Value) entity.Category = (string)row["Category"];         // NVarChar(128) NULL
                entity.UpdatedOn = (DateTime)row["Updated_On"];                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];    // NVarChar(128) NULL
            }
            rdrReader.Dispose();
        }
        return entity;
    }

    public static async Task<List<EntityModel>> ListAsync(int masterID)
    {
        EntityModel item;
        List<EntityModel> list = new List<EntityModel>();
        SqlDataReader rdrReader, row;
        using (Database db = new Database())
        {
            db.AddParam("@MasterID", SqlDbType.Int).Value = masterID;
            row = rdrReader = await db.GetSqlDataReaderAsync("_______Dta.Stored_Procedure_GRID_______");
            while (await rdrReader.ReadAsync())
            {
                item = new EntityModel();
                item.ID = (int)row["Record_ID"];                                                        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                if (row["Master_ID"] != DBNull.Value) item.MasterID = (int)row["Master_ID"];            // Int NULL FOREIGN KEY REFERENCES Dta.Master_Entities(Master_ID)
                item.Name = (string)row["Name"];                                                        // NVarChar(128) NOT NULL
                if (row["Category"] != DBNull.Value) item.Category = (string)row["Category"];           // NVarChar(128) NULL
                item.UpdatedOn = (DateTime)row["Updated_On"];                                           // SmallDateTime NOT NULL DEFAULT GETDATE()
                if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];      // NVarChar(128) NULL
                list.Add(item);
            }
            rdrReader.Dispose();
        }
        return list;
    }

    public static async Task<bool> DeleteAsync(long entityID, string? updatedBy = null, DateTime? updatedOn = null)
    {
        bool success = false;
        using (Database db = new Database())
        {
            db.AddParam("@RecordID", SqlDbType.Int).Value = entityID;
            db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;
            db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
            if ((await db.DeleteRecordAsync("_______Dta.Stored_Procedure_DELETE_______")) > 0) success = true;
        }
        return success;
    }
}
public class EntityModel
{
    public int? ID { get; set; }
    public int? MasterID { get; set; }
    public string? Name { get; set; }
    public string? Category { get; set; }
    public DateTime UpdatedOn { get; set; } = DateTime.Now;
    public string? UpdatedBy { get; set; }
}