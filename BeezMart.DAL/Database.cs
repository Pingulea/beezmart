﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.DAL
{
	public class DatabaseRow : Dictionary<string, object?>
	{

	}

	public class Database : IDisposable
	{
		public static string? ConnectionString = null;
		public SqlConnection Connection;
		public SqlCommand Command;
		public SqlParameterCollection Params { get { return this.Command.Parameters; } }

		public Database()
		{
			this.Connection = new SqlConnection(Database.ConnectionString);
			this.Command = new SqlCommand();
			this.Command.Connection = this.Connection;
			this.Command.CommandType = CommandType.StoredProcedure;
		}

		public SqlParameter AddParam(SqlParameter parameter)
		{
			SqlParameter param = this.Command.Parameters.Add(parameter);
			//param.Value = DBNull.Value; 
			return param;
		}
		public SqlParameter AddParam(string parameterName, SqlDbType parameterType)
		{
			SqlParameter param = this.Command.Parameters.Add(parameterName, parameterType);
			//param.Value = DBNull.Value;
			return param;
		}
		public SqlParameter AddParam(string parameterName, SqlDbType parameterType, int parameterSize)
		{
			SqlParameter param = this.Command.Parameters.Add(parameterName, parameterType, parameterSize);
			//param.Value = DBNull.Value;
			return param;
		}
		public void SetParam(string parameterName, string parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.NVarChar).Value = parameterValue;
		}
		public void SetParam(string parameterName, char? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.Char).Value = parameterValue;
		}
		public void SetParam(string parameterName, byte? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.TinyInt).Value = parameterValue;
		}
		public void SetParam(string parameterName, short? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.SmallInt).Value = parameterValue;
		}
		public void SetParam(string parameterName, int? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.Int).Value = parameterValue;
		}
		public void SetParam(string parameterName, long? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.BigInt).Value = parameterValue;
		}
		public void SetParam(string parameterName, decimal? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.Decimal).Value = parameterValue;
		}
		public void SetParam(string parameterName, DateTime? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.DateTime).Value = parameterValue;
		}
		public void SetParam(string parameterName, Guid? parameterValue)
		{
			if (this.Params.Contains(parameterName)) this.Params[parameterName].Value = parameterValue;
			else this.Params.Add(parameterName, SqlDbType.UniqueIdentifier).Value = parameterValue;
		}

		/// <summary>
		/// 	<para>Retrieves a row from a database table. The field types are .NET native: int, string, DateTime, long, byte, bool etc.</para>
		/// </summary>
		///		<param name="storedProcedureName">The params for the stored procedure must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public DatabaseRow? GetDataRow(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			SqlDataReader rdrReader;
			this.Command.CommandText = storedProcedureName;
			rdrReader = this.Command.ExecuteReader();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				rdrReader.Read();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					switch (rdrReader.GetDataTypeName(i))
					{
						case "uniqueidentifier": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlGuid(i).Value); break;

						case "bit": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBoolean(i).Value); break;
						case "tinyint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlByte(i).Value); break;
						case "smallint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt16(i).Value); break;
						case "int": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt32(i).Value); break;
						case "bigint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt64(i).Value); break;
						case "decimal": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDecimal(i).Value); break;
						case "money": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;
						case "smallmoney": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;

						case "real": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlSingle(i).Value); break;
						case "float": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDouble(i).Value); break;

						case "date": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "datetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "smalldatetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "time": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;

						case "char": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlChars(i).Value); break;
						case "nchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "varchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "text": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "nvarchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "ntext": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "binary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "varbinary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "image": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;

						case "xml": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlXml(i).Value); break;

						default: if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i)); break;
					}
				}
			}
			rdrReader.Dispose();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return row;
		}
		/// <summary>
		/// 	<para>Retrieves a row from a database table. The field types are .NET native: int, string, DateTime, long, byte, bool etc.</para>
		/// </summary>
		///		<param name="storedProcedureName">The params for the stored procedure must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<DatabaseRow?> GetDataRowAsync(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			SqlDataReader rdrReader;
			this.Command.CommandText = storedProcedureName;
			rdrReader = await this.Command.ExecuteReaderAsync();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				await rdrReader.ReadAsync();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					switch (rdrReader.GetDataTypeName(i))
					{
						case "uniqueidentifier": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlGuid(i).Value); break;

						case "bit": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBoolean(i).Value); break;
						case "tinyint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlByte(i).Value); break;
						case "smallint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt16(i).Value); break;
						case "int": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt32(i).Value); break;
						case "bigint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt64(i).Value); break;
						case "decimal": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDecimal(i).Value); break;
						case "money": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;
						case "smallmoney": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;

						case "real": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlSingle(i).Value); break;
						case "float": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDouble(i).Value); break;

						case "date": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "datetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "smalldatetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "time": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;

						case "char": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlChars(i).Value); break;
						case "nchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "varchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "text": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "nvarchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "ntext": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "binary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "varbinary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "image": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;

						case "xml": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlXml(i).Value); break;

						default: if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i)); break;
					}
				}
			}
			rdrReader.Dispose();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return row;
		}
		/// <summary>
		/// 	<para>Retrieves a row from a database table as a result from a text command. The field types are .NET native: int, string, DateTime, long, byte, bool etc.</para>
		/// </summary>
		///		<param name="commandText">The params for command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public DatabaseRow? GetDataRowFromTextCommand(string commandText, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			rdrReader = this.Command.ExecuteReader();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				rdrReader.Read();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					switch (rdrReader.GetDataTypeName(i))
					{
						case "uniqueidentifier": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlGuid(i).Value); break;

						case "bit": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBoolean(i).Value); break;
						case "tinyint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlByte(i).Value); break;
						case "smallint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt16(i).Value); break;
						case "int": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt32(i).Value); break;
						case "bigint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt64(i).Value); break;
						case "decimal": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDecimal(i).Value); break;
						case "money": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;
						case "smallmoney": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;

						case "real": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlSingle(i).Value); break;
						case "float": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDouble(i).Value); break;

						case "date": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "datetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "smalldatetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "time": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;

						case "char": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlChars(i).Value); break;
						case "nchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "varchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "text": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "nvarchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "ntext": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "binary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "varbinary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "image": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;

						case "xml": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlXml(i).Value); break;

						default: if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i)); break;
					}
				}
			}
			rdrReader.Dispose();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return row;
		}
		/// <summary>
		/// 	<para>Retrieves a row from a database table as a result from a text command. The field types are .NET native: int, string, DateTime, long, byte, bool etc.</para>
		/// </summary>
		///		<param name="commandText">The params for command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<DatabaseRow?> GetDataRowFromTextCommandAsync(string commandText, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			rdrReader = await this.Command.ExecuteReaderAsync();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				await rdrReader.ReadAsync();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					switch (rdrReader.GetDataTypeName(i))
					{
						case "uniqueidentifier": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlGuid(i).Value); break;

						case "bit": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBoolean(i).Value); break;
						case "tinyint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlByte(i).Value); break;
						case "smallint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt16(i).Value); break;
						case "int": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt32(i).Value); break;
						case "bigint": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlInt64(i).Value); break;
						case "decimal": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDecimal(i).Value); break;
						case "money": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;
						case "smallmoney": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlMoney(i).Value); break;

						case "real": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlSingle(i).Value); break;
						case "float": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDouble(i).Value); break;

						case "date": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "datetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "smalldatetime": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;
						case "time": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlDateTime(i).Value); break;

						case "char": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlChars(i).Value); break;
						case "nchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "varchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "text": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "nvarchar": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;
						case "ntext": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlString(i).Value); break;

						case "binary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "varbinary": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;
						case "image": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlBinary(i).Value); break;

						case "xml": if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlXml(i).Value); break;

						default: if (rdrReader.IsDBNull(i)) row.Add(rdrReader.GetName(i), null); else row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i)); break;
					}
				}
			}
			rdrReader.Dispose();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return row;
		}

		/// <summary>
		/// 	<para>Gets the result set of a stored procedure as a SqlDataReader. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="storedProcedureName">The params for the stored procedure must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public SqlDataReader GetSqlDataReader(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandText = storedProcedureName;
			rdrReader = this.Command.ExecuteReader();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			return rdrReader;
		}
		/// <summary>
		/// 	<para>Gets the result set of a stored procedure as a SqlDataReader. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="storedProcedureName">The params for the stored procedure must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<SqlDataReader> GetSqlDataReaderAsync(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandText = storedProcedureName;
			rdrReader = await this.Command.ExecuteReaderAsync();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			return rdrReader;
		}
		/// <summary>
		/// 	<para>Gets the result set of a text command as a SqlDataReader. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="commandText">The params for the command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public SqlDataReader GetSqlDataReaderFromTextCommand(string commandText, bool clearParamsAfterCompletion = true)
		{
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			rdrReader = this.Command.ExecuteReader();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			return rdrReader;
		}
		/// <summary>
		/// 	<para>Gets the result set of a text command as a SqlDataReader. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="commandText">The params for the command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<SqlDataReader> GetSqlDataReaderFromTextCommandAsync(string commandText, bool clearParamsAfterCompletion = true)
		{
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			rdrReader = await this.Command.ExecuteReaderAsync();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			return rdrReader;
		}

		/// <summary>
		/// 	<para>Retrieves a row from a database table. The field types are those found in System.Data.SqlClient: SqlInt32, SqlString, SqlDateTime etc.</para>
		/// </summary>
		///		<param name="storedProcedureName">The params for the stored procedure must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public DatabaseRow? GetSqlDataRecord(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandText = storedProcedureName;
			rdrReader = this.Command.ExecuteReader();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				rdrReader.Read();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i));
				}
			}
			rdrReader.Dispose();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			return row;
		}
		/// <summary>
		/// 	<para>Retrieves a row from a database table. The field types are those found in System.Data.SqlClient: SqlInt32, SqlString, SqlDateTime etc.</para>
		/// </summary>
		///		<param name="storedProcedureName">The params for the stored procedure must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<DatabaseRow?> GetSqlDataRecordAsync(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandText = storedProcedureName;
			rdrReader = await this.Command.ExecuteReaderAsync();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				await rdrReader.ReadAsync();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i));
				}
			}
			rdrReader.Dispose();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			return row;
		}
		/// <summary>
		///		<para>Retrieves a row from a database table as a result from a text command. The field types are those found in System.Data.SqlClient: SqlInt32, SqlString, SqlDateTime etc.</para>
		/// </summary>
		///		<param name="commandText">The params for the command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public DatabaseRow? GetSqlDataRecordFromTextCommand(string commandText, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			rdrReader = this.Command.ExecuteReader();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				rdrReader.Read();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i));
				}
			}
			rdrReader.Dispose();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return row;
		}
		/// <summary>
		///		<para>Retrieves a row from a database table as a result from a text command. The field types are those found in System.Data.SqlClient: SqlInt32, SqlString, SqlDateTime etc.</para>
		/// </summary>
		///		<param name="commandText">The params for the command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<DatabaseRow?> GetSqlDataRecordFromTextCommandAsync(string commandText, bool clearParamsAfterCompletion = true)
		{
			DatabaseRow? row = null;
			SqlDataReader rdrReader;
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			rdrReader = await this.Command.ExecuteReaderAsync();
			if (rdrReader.HasRows)
			{
				row = new DatabaseRow();
				await rdrReader.ReadAsync();
				for (int i = 0; i < rdrReader.FieldCount; i++)
				{
					row.Add(rdrReader.GetName(i), rdrReader.GetSqlValue(i));
				}
			}
			rdrReader.Dispose();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return row;
		}

		/// <summary>
		///		<para>Gets the result of a SQL scalar function. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="functionName">The params for the database function must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public object? GetScalar(string functionName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			StringBuilder stb = new StringBuilder();
			stb.Append("SELECT ");
			stb.Append(functionName.Contains(".") ? functionName : "dbo." + functionName);
			if (this.Command.Parameters.Count > 0)
			{
				stb.Append("(");
				foreach (SqlParameter param in this.Command.Parameters) stb.AppendFormat("{0}, ", param.ParameterName);
				stb.Remove(stb.Length - 2, 2);
				stb.Append(")");
			}
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = stb.ToString();
			object? retVal = this.Command.ExecuteScalar();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return retVal;
		}
		/// <summary>
		///		<para>Gets the result of a SQL scalar function. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="functionName">The params for the database function must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<object?> GetScalarAsync(string functionName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			StringBuilder stb = new StringBuilder();
			stb.Append("SELECT ");
			stb.Append(functionName.Contains(".") ? functionName : "dbo." + functionName);
			if (this.Command.Parameters.Count > 0)
			{
				stb.Append("(");
				foreach (SqlParameter param in this.Command.Parameters) stb.AppendFormat("{0}, ", param.ParameterName);
				stb.Remove(stb.Length - 2, 2);
				stb.Append(")");
			}
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = stb.ToString();
			object? retVal = await this.Command.ExecuteScalarAsync();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return retVal;
		}
		/// <summary>
		///		<para>Gets the scalar result of a text command. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="commandText">The params for the command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public object? GetScalarFromTextCommand(string commandText, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			object? retVal = this.Command.ExecuteScalar();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return retVal;
		}
		/// <summary>
		///		<para>Gets the scalar result of a text command. Then, if ClearParamsAfterCompletion is true, deletes the Params collection from the SqlCommand.</para>
		/// </summary>
		///		<param name="commandText">The params for the command must be added before calling this method, using AddParam().</param>
		///		<param name="clearParamsAfterCompletion">Command parameters are dropped after the execution (the collection is cleared), if true (the default).</param>
		/// <returns></returns>
		public async Task<object?> GetScalarFromTextCommandAsync(string commandText, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = commandText;
			object? retVal = await this.Command.ExecuteScalarAsync();
			this.Command.CommandType = CommandType.StoredProcedure;
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return retVal;
		}

		public int ExecuteProcedure(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandText = storedProcedureName;
			int NumberOfRecordsAffected = this.Command.ExecuteNonQuery();
			if (clearParamsAfterCompletion)
			{
				this.Command.Parameters.Clear();
			}
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}
		public async Task<int> ExecuteProcedureAsync(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandText = storedProcedureName;
			int NumberOfRecordsAffected = await this.Command.ExecuteNonQueryAsync();
			if (clearParamsAfterCompletion)
			{
				this.Command.Parameters.Clear();
			}
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}
		public int ExecuteTextCommand(string transactSqlCommandText, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = transactSqlCommandText;
			int NumberOfRecordsAffected = this.Command.ExecuteNonQuery();
			if (clearParamsAfterCompletion)
			{
				this.Command.Parameters.Clear();
			}
			this.Command.CommandType = CommandType.StoredProcedure;
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}
		public async Task<int> ExecuteTextCommandAsync(string transactSqlCommandText, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandType = CommandType.Text;
			this.Command.CommandText = transactSqlCommandText;
			int NumberOfRecordsAffected = await this.Command.ExecuteNonQueryAsync();
			if (clearParamsAfterCompletion)
			{
				this.Command.Parameters.Clear();
			}
			this.Command.CommandType = CommandType.StoredProcedure;
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}

		public int SaveRecord(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandText = storedProcedureName;
			int NumberOfRecordsAffected = this.Command.ExecuteNonQuery();
			if (clearParamsAfterCompletion)
			{
				this.Command.Parameters.Clear();
			}
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}
		public async Task<int> SaveRecordAsync(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandText = storedProcedureName;
			int NumberOfRecordsAffected = await this.Command.ExecuteNonQueryAsync();
			if (clearParamsAfterCompletion)
			{
				this.Command.Parameters.Clear();
			}
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}
		public int DeleteRecord(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) this.Connection.Open();
			this.Command.CommandText = storedProcedureName;
			int NumberOfRecordsAffected = this.Command.ExecuteNonQuery();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}
		public async Task<int> DeleteRecordAsync(string storedProcedureName, bool clearParamsAfterCompletion = true)
		{
			if (this.Connection.State != ConnectionState.Open) await this.Connection.OpenAsync();
			this.Command.CommandText = storedProcedureName;
			int NumberOfRecordsAffected = await this.Command.ExecuteNonQueryAsync();
			if (clearParamsAfterCompletion) this.Command.Parameters.Clear();
			this.Connection.Close();
			return NumberOfRecordsAffected;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Params.Clear();
				if (this.Connection != null && this.Connection.State == ConnectionState.Open)
				{
					this.Connection.Close();
					this.Connection.Dispose();
				}
				this.Command.Dispose();
			}
		}
	}
}