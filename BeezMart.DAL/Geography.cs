﻿using BeezMart.Entities.Geography;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Geography;

public static partial class Countries
{
    /// <summary>
    ///		<para>Returns the 10-minutes cached list of available countries, those set to be shown.</para>
    /// </summary>
    /// <returns></returns>
    public static async Task<List<Country>> ListAsync()
    {
        Country country;
        if (_countries == null || !_countries_timestamp.HasValue || _countries_timestamp.Value.AddMinutes(10) < DateTime.Now)
        {
            _countries = new List<Country>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                row = rdrReader = await db.GetSqlDataReaderAsync("CRM.App_Countries_Grid_1");
                while (await rdrReader.ReadAsync())
                {
                    country = new Country();
                    country.ID = (string)row["Country_ID"];                                             // Char(3) NOT NULL PRIMARY KEY
                    if (row["Code"] != DBNull.Value) country.Code = (string)row["Code"];                // Char(2) NULL
                    if (row["Code_UN"] != DBNull.Value) country.CodeUN = (string)row["Code_UN"];        // VarChar(4) NULL
                    country.Name = (string)row["Country_Name"];                                         // NVarChar(128) NOT NULL
                    country.Show = (bool)row["Show"];                                                   // Bit NOT NULL DEFAULT 0
                    _countries.Add(country);
                }
                rdrReader.Dispose();
            }
            _countries_timestamp = DateTime.Now;
        }
        return _countries;
    }

    /// <summary>
    ///		<para>Returns the 10-minutes cached list of all countries, no matter if set to be shown or not.</para>
    /// </summary>
    /// <returns></returns>
    public static async Task<List<Country>> ListAllAsync()
    {
        Country country;
        if (_allCountries == null || !_allCountries_timestamp.HasValue || _allCountries_timestamp.Value.AddMinutes(10) < DateTime.Now)
        {
            _allCountries = new List<Country>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                row = rdrReader = await db.GetSqlDataReaderAsync("CRM.App_Countries_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    country = new Country();
                    country.ID = (string)row["Country_ID"];                                             // Char(3) NOT NULL PRIMARY KEY
                    if (row["Code"] != DBNull.Value) country.Code = (string)row["Code"];                // Char(2) NULL
                    if (row["Code_UN"] != DBNull.Value) country.CodeUN = (string)row["Code_UN"];        // VarChar(4) NULL
                    country.Name = (string)row["Country_Name"];                                         // NVarChar(128) NOT NULL
                    country.Show = (bool)row["Show"];                                                   // Bit NOT NULL DEFAULT 0
                    _allCountries.Add(country);
                }
            }
            _allCountries_timestamp = DateTime.Now;
        }
        return _allCountries;
    }

    private static List<Country>? _countries = null;
    private static DateTime? _countries_timestamp = null;
    private static List<Country>? _allCountries = null;
    private static DateTime? _allCountries_timestamp = null;
}

public static class StatesProvinces
{
    /// <summary>
    ///		<para>Returns the non-cached list of states/provinces from a country.</para>
    /// </summary>
    /// <param name="countryID">The 3-chars code of the country. Example: 'ROM' for Romania.</param>
    /// <returns></returns>
    public static async Task<List<StateProvince>> ListAsync(string countryID)
    {
        List<StateProvince> list = new List<StateProvince>();
        StateProvince item;
        SqlDataReader rdrReader, row;
        using (Database db = new Database())
        {
            db.AddParam("@CountryID", SqlDbType.Char, 3).Value = countryID;
            row = rdrReader = await db.GetSqlDataReaderAsync("CRM.App_States_Provinces_Grid_1");
            while (await rdrReader.ReadAsync())
            {
                item = new StateProvince();
                item.ID = (short)row["State_Province_ID"];                  // SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
                item.CountryID = (string)row["Country_ID"];                 // Char(3) NOT NULL DEFAULT 'ROM'
                item.Region = (string)row["Region"];                        // NVarChar(128) NOT NULL
                item.Abbr = (string)row["State_Province_Abbr"];             // NVarChar(8) NOT NULL
                item.Name = (string)row["State_Province_Name"];             // NVarChar(128) NOT NULL
                list.Add(item);
            }
        }
        return list;
    }
}