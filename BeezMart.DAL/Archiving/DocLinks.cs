﻿using BeezMart.Entities.Archiving;
using BeezMart.Entities.CRM;
using BeezMart.Entities.Billing;
using BeezMart.Entities.TimeSheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezMart.DAL.Archiving
{
	public static class DocLinks
    {
        #region Add Link to an Entity
        public static async Task<bool> AddAsync(LinkToOrganizationModel link)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = link.DocumentID;				            // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@OrganizationID", SqlDbType.Int).Value = link.OrganizationID;                      // Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = link.UpdatedOn;                      // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = link.UpdatedBy;                      // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Organizations_Upsert")) > 0) success = true;
                if (success) link.ID = (short)db.Params["@RecordID"].Value;
            }
            return success;
        }
        public static async Task<bool> AddAsync(LinkToPersonModel link)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = link.DocumentID;				            // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@PersonID", SqlDbType.Int).Value = link.PersonID;                                  // Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = link.UpdatedOn;                      // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = link.UpdatedBy;                      // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Persons_Upsert")) > 0) success = true;
                if (success) link.ID = (short)db.Params["@RecordID"].Value;
            }
            return success;
        }
        public static async Task<bool> AddAsync(LinkToInvoiceModel link)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = link.DocumentID;				            // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@InvoiceID", SqlDbType.Int).Value = link.InvoiceID;                                // Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = link.UpdatedOn;                      // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = link.UpdatedBy;                      // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Invoices_Upsert")) > 0) success = true;
                if (success) link.ID = (short)db.Params["@RecordID"].Value;
            }
            return success;
        }
        public static async Task<bool> AddAsync(LinkToProjectModel link)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = link.DocumentID;				            // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@ProjectID", SqlDbType.Int).Value = link.ProjectID;                                // Int NOT NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = link.UpdatedOn;                      // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = link.UpdatedBy;                      // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Projects_Upsert")) > 0) success = true;
                if (success) link.ID = (short)db.Params["@RecordID"].Value;
            }
            return success;
        }
        public static async Task<bool> AddAsync(LinkToActivityModel link)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = link.DocumentID;				            // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@ActivityID", SqlDbType.BigInt).Value = link.ActivityID;                           // BigInt NOT NULL FOREIGN KEY REFERENCES Sales.Activities(Activity_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = link.UpdatedOn;                      // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = link.UpdatedBy;                      // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Activities_Upsert")) > 0) success = true;
                if (success) link.ID = (short)db.Params["@RecordID"].Value;
            }
            return success;
        }
		public static async Task<bool> AddOrganizationAsync(long DocumentID, int OrganizationID, DateTime? updatedOn, string? updatedBy)
		{
            if (!updatedOn.HasValue) updatedOn = DateTime.Now;
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;       // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				        // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@OrganizationID", SqlDbType.Int).Value = OrganizationID;                   // Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;                   // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Organizations_Upsert")) > 0) success = true;
            }
            return success;
        }
		public static async Task<bool> AddPersonAsync(long DocumentID, int PersonID, DateTime? updatedOn, string? updatedBy)
		{
            if (!updatedOn.HasValue) updatedOn = DateTime.Now;
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;       // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				        // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@PersonID", SqlDbType.Int).Value = PersonID;                               // Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;                   // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Persons_Upsert")) > 0) success = true;
            }
            return success;
        }
		public static async Task<bool> AddInvoiceAsync(long DocumentID, int InvoiceID, DateTime? updatedOn, string? updatedBy)
		{
            if (!updatedOn.HasValue) updatedOn = DateTime.Now;
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;       // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				        // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@InvoiceID", SqlDbType.Int).Value = InvoiceID;                             // Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;                   // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Invoices_Upsert")) > 0) success = true;
            }
            return success;
        }
		public static async Task<bool> AddProjectAsync(long DocumentID, int ProjectID, DateTime? updatedOn, string? updatedBy)
		{
            if (!updatedOn.HasValue) updatedOn = DateTime.Now;
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;       // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				        // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@ProjectID", SqlDbType.Int).Value = ProjectID;                             // Int NOT NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;                   // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Projects_Upsert")) > 0) success = true;
            }
            return success;
        }
		public static async Task<bool> AddActivityAsync(long DocumentID, long ActivityID, DateTime? updatedOn, string? updatedBy)
		{
            if (!updatedOn.HasValue) updatedOn = DateTime.Now;
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Direction = ParameterDirection.Output;       // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				        // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                db.AddParam("@ActivityID", SqlDbType.BigInt).Value = ActivityID;                        // BigInt NOT NULL FOREIGN KEY REFERENCES Sales.Activities(Activity_ID)
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;                   // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;                   // NVarChar(128) = NULL
                if ((await db.SaveRecordAsync("Archiving.Documents_To_Activities_Upsert")) > 0) success = true;
            }
            return success;
        }
        #endregion
        #region Delete Link to an Entity
        public static async Task<bool> DeleteLinkToOrganizationAsync(long RecordID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Value = RecordID;				        // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Organizations_Del")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToOrganizationAsync(int OrganizationID, long DocumentID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@OrganizationID", SqlDbType.Int).Value = OrganizationID;
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Organizations_Del_1")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToPersonAsync(long RecordID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Value = RecordID;				        // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Persons_Del")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToPersonAsync(int PersonID, long DocumentID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@PersonID", SqlDbType.Int).Value = PersonID;
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Persons_Del_1")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToInvoiceAsync(long RecordID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Value = RecordID;				        // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Invoices_Del")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToInvoiceAsync(int InvoiceID, long DocumentID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@InvoiceID", SqlDbType.Int).Value = InvoiceID;
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Invoices_Del_1")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToProjectAsync(long RecordID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Value = RecordID;				        // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Projects_Del")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToProjectAsync(int ProjectID, long DocumentID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@ProjectID", SqlDbType.Int).Value = ProjectID;
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Projects_Del_1")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToActivityAsync(long RecordID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@RecordID", SqlDbType.BigInt).Value = RecordID;				        // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Activities_Del")) > 0) success = true;
            }
            return success;
        }
        public static async Task<bool> DeleteLinkToActivityAsync(long ActivityID, long DocumentID)
        {
            bool success = false;
            using (Database db = new Database())
            {
                db.AddParam("@ActivityID", SqlDbType.BigInt).Value = ActivityID;
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;
                if ((await db.DeleteRecordAsync("Archiving.Documents_To_Activities_Del_1")) > 0) success = true;
            }
            return success;
        }
        #endregion
        #region List Entities Linked to a Document
        /// <summary>
        ///     <para>Lists CRM Organizations that are related to a document</para>
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public static async Task<List<OrganizationModel>> ListOrganizationsAsync(long DocumentID)
        {
            OrganizationModel item;
            List<OrganizationModel> list = new List<OrganizationModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Organizations_Grid_1");
                while (await rdrReader.ReadAsync())
                {
                    item = new OrganizationModel();
                    item.ID = (int)row["Organization_ID"];                                                                          // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    item.SegmentID = (byte)row["Segment_ID"];                                                                       // TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID)
                    item.Segment = (string)row["Segment_Name"];                                                                     // NVarChar(64) NOT NULL
                    if (row["Parent_Org_ID"] != DBNull.Value) item.ParentOrgID = (int)row["Parent_Org_ID"];                         // Int NULL
                    item.Name = (string)row["Org_Name"];                                                                            // NVarChar(256) NOT NULL
                    if (row["Name_Prefix"] != DBNull.Value) item.NamePrefix = (string)row["Name_Prefix"];                           // NVarChar(16) NULL
                    if (row["Name_Suffix"] != DBNull.Value) item.NameSuffix = (string)row["Name_Suffix"];                           // NVarChar(16) NULL
                    if (row["Alternate_Names"] != DBNull.Value) item.AlternateNames = (string)row["Alternate_Names"];               // NVarChar(1024) NULL
                    if (row["Country_Of_Origin"] != DBNull.Value) item.CountryOfOrigin = (string)row["Country_Of_Origin"];          // Char(3) NOT NULL DEFAULT 'ROM'
                    if (row["Site_ID_Headquarter"] != DBNull.Value) item.SiteIdHome = (int)row["Site_ID_Headquarter"];              // Int NULL
                    if (row["Site_ID_Billing"] != DBNull.Value) item.SiteIdBill = (int)row["Site_ID_Billing"];                      // Int NULL
                    if (row["Site_ID_Shipping"] != DBNull.Value) item.SiteIdShip = (int)row["Site_ID_Shipping"];                    // Int NULL
                    if (row["Country"] != DBNull.Value) item.HQ_Country = (string)row["Country"];                                   // Char(3) NULL DEFAULT 'ROM'
                    if (row["Region"] != DBNull.Value) item.HQ_Region = (string)row["Region"];                                      // NVarChar(128) NULL
                    if (row["State_Province"] != DBNull.Value) item.HQ_StateProvince = (string)row["State_Province"];               // NVarChar(128) NULL
                    if (row["City"] != DBNull.Value) item.HQ_City = (string)row["City"];                                            // NVarChar(64) NOT NULL
                    if (row["Street_Address_1"] != DBNull.Value) item.HQ_Address = (string)row["Street_Address_1"];                 // NVarChar(128) NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];			                    // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Lists CRM Persons that are related to a document</para>
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public static async Task<List<PersonModel>> ListPersonsAsync(long DocumentID)
        {
            PersonModel item;
            List<PersonModel> list = new List<PersonModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Persons_Grid_1");
                while (await rdrReader.ReadAsync())
                {
                    item = new PersonModel();
                    item.ID = (int)row["Person_ID"];                                                                                // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                    item.SegmentID = (byte)row["Segment_ID"];                                                                       // TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID)
                    item.Segment = (string)row["Segment_Name"];                                                                     // NVarChar(64) NOT NULL
                    item.FirstName = (string)row["First_Name"];                                                                     // NVarChar(64) NOT NULL
                    item.LastName = (string)row["Last_Name"];                                                                       // NVarChar(64) NOT NULL
                    if (row["Name_Prefix"] != DBNull.Value) item.NamePrefix = (string)row["Name_Prefix"];                           // NVarChar(16) NULL
                    if (row["Name_Suffix"] != DBNull.Value) item.NameSuffix = (string)row["Name_Suffix"];                           // NVarChar(16) NULL
                    if (row["Nicknames"] != DBNull.Value) item.Nicknames = (string)row["Nicknames"];                                // NVarChar(256) NULL
                    if (row["Country_Of_Origin"] != DBNull.Value) item.CountryOfOrigin = (string)row["Country_Of_Origin"];          // Char(3) NOT NULL DEFAULT 'ROM'
                    if (row["Site_ID_Home"] != DBNull.Value) item.SiteIdHome = (int)row["Site_ID_Home"];                            // Int NULL
                    if (row["Site_ID_Billing"] != DBNull.Value) item.SiteIdBill = (int)row["Site_ID_Billing"];                      // Int NULL
                    if (row["Site_ID_Shipping"] != DBNull.Value) item.SiteIdShip = (int)row["Site_ID_Shipping"];                    // Int NULL
                    if (row["Country"] != DBNull.Value) item.Home_Country = (string)row["Country"];                                 // Char(3) NULL DEFAULT 'ROM'
                    if (row["Region"] != DBNull.Value) item.Home_Region = (string)row["Region"];                                    // NVarChar(128) NULL
                    if (row["State_Province"] != DBNull.Value) item.Home_StateProvince = (string)row["State_Province"];             // NVarChar(128) NULL
                    if (row["City"] != DBNull.Value) item.Home_City = (string)row["City"];                                          // NVarChar(64) NOT NULL
                    if (row["Street_Address_1"] != DBNull.Value) item.Home_Address = (string)row["Street_Address_1"];               // NVarChar(128) NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];			                    // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Lists Billing Invoices that are related to a document</para>
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public static async Task<List<InvoiceModel>> ListInvoicesAsync(long DocumentID)
        {
            InvoiceModel item;
            List<InvoiceModel> list = new List<InvoiceModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Invoices_Grid_1");
                while (await rdrReader.ReadAsync())
                {
                    item = new InvoiceModel();
					item.ID = (int)row["Invoice_ID"];																											// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.IssueDate = (DateTime)row["Issue_Date"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Due_Date"] != DBNull.Value) item.DueDate = (DateTime)row["Due_Date"];																// SmallDateTime NULL
					if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];													// SmallDateTime NULL
					if (row["Accounting_ID"] != DBNull.Value) item.AccountingID = (string)row["Accounting_ID"];													// VarChar(32) NULL
					item.VATpercentage = (decimal)row["VAT_Percentage"];																						// Decimal(6, 4) NOT NULL DEFAULT 0
					item.Currency = (string)row["Currency"];																									// Char(3) NOT NULL DEFAULT 'EUR'
					item.Type = (InvoiceType)(byte)row["Invoice_Type_ID"];																						// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
					item.Status = (InvoiceStatus)(byte)row["Invoice_Status_ID"];																				// TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];												// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];																// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																							// NVarChar(256) NOT NULL
					if (row["Customer_City"] != DBNull.Value) item.CustomerCity = (string)row["Customer_City"];													// NVarChar(64) NULL
					if (row["Customer_Province"] != DBNull.Value) item.CustomerProvince = (string)row["Customer_Province"];										// NVarChar(64) NULL
					if (row["Customer_Address"] != DBNull.Value) item.CustomerAddress = (string)row["Customer_Address"];										// NVarChar(256) NULL
					if (row["Customer_Registration_Number"] != DBNull.Value) item.CustomerRegistrationNumber = (string)row["Customer_Registration_Number"];		// VarChar(32) NULL
					if (row["Customer_Tax_ID"] != DBNull.Value) item.CustomerTaxID = (string)row["Customer_Tax_ID"];											// VarChar(16) NULL
					if (row["Customer_Bank_Name"] != DBNull.Value) item.CustomerBankName = (string)row["Customer_Bank_Name"];									// NVarChar(128) NULL
					if (row["Customer_Bank_Account"] != DBNull.Value) item.CustomerBankAccount = (string)row["Customer_Bank_Account"];							// VarChar(64) NULL
					if (row["Delegate_Name"] != DBNull.Value) item.DelegateName = (string)row["Delegate_Name"];													// NVarChar(128) NULL
					if (row["Delegate_ID_1"] != DBNull.Value) item.DelegateID1 = (string)row["Delegate_ID_1"];													// NVarChar(8) NULL
					if (row["Delegate_ID_2"] != DBNull.Value) item.DelegateID2 = (string)row["Delegate_ID_2"];													// NVarChar(16) NULL
					if (row["Delegate_ID_3"] != DBNull.Value) item.DelegateID3 = (string)row["Delegate_ID_3"];													// NVarChar(32) NULL
					if (row["Transport_Name"] != DBNull.Value) item.TransportName = (string)row["Transport_Name"];												// NVarChar(32) NULL
					if (row["Transport_Detail_1"] != DBNull.Value) item.TransportDetail1 = (string)row["Transport_Detail_1"];									// NVarChar(32) NULL
					if (row["Transport_Detail_2"] != DBNull.Value) item.TransportDetail2 = (string)row["Transport_Detail_2"];									// NVarChar(32) NULL
					if (row["Transport_Detail_3"] != DBNull.Value) item.TransportDetail3 = (string)row["Transport_Detail_3"];									// NVarChar(32) NULL
					if (row["Transport_Detail_4"] != DBNull.Value) item.TransportDetail4 = (string)row["Transport_Detail_4"];									// NVarChar(32) NULL
					if (row["Invoice_Detail_1"] != DBNull.Value) item.Detail1 = (string)row["Invoice_Detail_1"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_2"] != DBNull.Value) item.Detail2 = (string)row["Invoice_Detail_2"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_3"] != DBNull.Value) item.Detail3 = (string)row["Invoice_Detail_3"];												// NVarChar(256) NULL
					if (row["Invoice_Detail_4"] != DBNull.Value) item.Detail4 = (string)row["Invoice_Detail_4"];												// NVarChar(256) NULL
					if (row["Sales_Order_ID"] != DBNull.Value) item.SalesOrderID = (int)row["Sales_Order_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
					if (row["Delivery_Document_ID"] != DBNull.Value) item.DeliveryDocumentID = (int)row["Delivery_Document_ID"];								// Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
					if (row["Related_Invoice_ID"] != DBNull.Value) item.RelatedInvoiceID = (int)row["Related_Invoice_ID"];										// Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
					if (row["Rendering_Layout"] != DBNull.Value) item.RenderingLayout = (string)row["Rendering_Layout"];										// VarChar(256) NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];																								// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];															// NVarChar(128) NULL
					item.TotalValue = (decimal)row["Total_Value"];																								// Decimal(27,8)
					item.TotalPayed = (decimal)row["Total_Payed"];																								// Decimal(27,8)
					list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Lists Sales Projects that are related to a document</para>
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public static async Task<List<ProjectModel>> ListProjectsAsync(long DocumentID)
        {
            ProjectModel item;
            List<ProjectModel> list = new List<ProjectModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Projects_Grid_1");
                while (await rdrReader.ReadAsync())
                {
                    item = new ProjectModel();
					item.ID = (int)row["Project_ID"];																		        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
					item.Title = (string)row["Title"];																		        // NVarChar(256) NOT NULL
					if (row["Project_Manager"] != DBNull.Value) item.ProjectManager = (string)row["Project_Manager"];		        // NVarChar(256) NULL
					if (row["Contact_Details"] != DBNull.Value) item.ContactDetails = (string)row["Contact_Details"];		        // NVarChar(256) NULL
					//if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];								        // NVarChar(MAX) NULL
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];			        // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];							        // Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];														        // NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];				        // NVarChar(256) NULL
					if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];					        // SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
					if (row["Project_Status_ID"] != DBNull.Value) item.Status = (ProjectStatus)(byte)row["Project_Status_ID"];		// TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)
					item.ScheduledStart = (DateTime)row["Scheduled_Start"];													        // SmallDateTime NOT NULL DEFAULT GETDATE()
					item.ScheduledEnd = (DateTime)row["Scheduled_End"];														        // SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];					        // SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];							        // SmallDateTime NULL
					item.UpdatedOn = (DateTime)row["Updated_On"];															        // SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];						        // NVarChar(128) NULL
					list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Lists Sales Project Activities that are related to a document</para>
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public static async Task<List<ActivityModel>> ListActivitiesAsync(long DocumentID)
        {
            ActivityModel item;
            List<ActivityModel> list = new List<ActivityModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = DocumentID;				                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Activities_Grid_1");
                while (await rdrReader.ReadAsync())
                {
                    item = new ActivityModel();
					item.ID = (long)row["Activity_ID"];																								// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
					if (row["Project_ID"] != DBNull.Value) item.ProjectID = (int)row["Project_ID"];													// Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
					if (row["Organization_ID"] != DBNull.Value) item.OrganizationID = (int)row["Organization_ID"];									// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
					if (row["Person_ID"] != DBNull.Value) item.PersonID = (int)row["Person_ID"];													// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
					item.CustomerName = (string)row["Customer_Name"];																				// NVarChar(256) NOT NULL
					if (row["End_Customer"] != DBNull.Value) item.EndCustomer = (string)row["End_Customer"];										// NVarChar(256) NULL
					if (row["Project_Title"] != DBNull.Value) item.ProjectTitle = (string)row["Project_Title"];										// NVarChar(256) NULL
					item.Status = (BeezMart.Entities.TimeSheet.ActivityStatus)(byte)row["Activity_Status_ID"];																	// TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
					//if (row["Activity_Status_Comment"] != DBNull.Value) item.ActivityStatusComment = (string)row["Activity_Status_Comment"];		// NVarChar(256) NULL
					if (row["Activity_Type_ID"] != DBNull.Value) item.ActivityTypeID = (short)row["Activity_Type_ID"];								// SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
					item.Title = (string)row["Title"];																								// NVarChar(1024) NOT NULL
					//if (row["Details"] != DBNull.Value) item.Details = (string)row["Details"];														// NVarChar(MAX) NULL
					if (row["Activity_Type"] != DBNull.Value) item.Details = (string)row["Activity_Type"];											// NVarChar(64) NULL
					if (row["Activity_Category"] != DBNull.Value) item.Details = (string)row["Activity_Category"];									// NVarChar(64) NULL
					item.ScheduledStart = (DateTime)row["Scheduled_Start"];																			// SmallDateTime NOT NULL DEFAULT GETDATE()
					item.ScheduledEnd = (DateTime)row["Scheduled_End"];																				// SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
					if (row["Assigned_On"] != DBNull.Value) item.AssignedOn = (DateTime)row["Assigned_On"];											// SmallDateTime NULL
					if (row["Assigned_To"] != DBNull.Value) item.AssignedTo = (string)row["Assigned_To"];											// NVarChar(128) NULL
					if (row["Deadline_On"] != DBNull.Value) item.DeadlineOn = (DateTime)row["Deadline_On"];											// SmallDateTime NULL
					if (row["Started_On"] != DBNull.Value) item.StartedOn = (DateTime)row["Started_On"];											// SmallDateTime NULL
					if (row["Ended_On"] != DBNull.Value) item.EndedOn = (DateTime)row["Ended_On"];													// SmallDateTime NULL
					if (row["Reminder_On"] != DBNull.Value) item.ReminderOn = (DateTime)row["Reminder_On"];											// SmallDateTime NULL
					if (row["Requested_By"] != DBNull.Value) item.RequestedBy = (string)row["Requested_By"];										// NVarChar(128) NULL
					if (row["Requesting_Department"] != DBNull.Value) item.RequestingDepartment = (string)row["Requesting_Department"];				// NVarChar(128) NULL
					if (row["Completed_By"] != DBNull.Value) item.CompletedBy = (string)row["Completed_By"];										// NVarChar(128) NULL
					if (row["Logged_By"] != DBNull.Value) item.LoggedBy = (string)row["Logged_By"];													// NVarChar(128) NULL
					item.Billable = (bool)row["Billable"];																							// Bit NOT NULL DEFAULT 0
					if (row["Invoice_IDs"] != DBNull.Value) item.InvoiceIDs = (string)row["Invoice_IDs"];											// VarChar(256) NULL
					if (row["Duration_In_Minutes"] != DBNull.Value) item.Duration = TimeSpan.FromMinutes((int)row["Duration_In_Minutes"]);          // Int NULL
                    item.UpdatedOn = (DateTime)row["Updated_On"];																					// SmallDateTime NOT NULL DEFAULT GETDATE()
					if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];												// NVarChar(128) NULL
					list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        #endregion
        #region List Documents Linked to an Entity
        /// <summary>
        ///     <para>Gets the Documents associated to a specific CRM Organization</para>
        /// </summary>
        /// <param name="OrganizationID"></param>
        /// <returns></returns>
        public static async Task<List<DocumentModel>> ListForOrganizationAsync(int OrganizationID)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@OrganizationID", SqlDbType.Int).Value = OrganizationID;				                        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Organizations_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                         // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                          // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];                                         // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                    // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];               // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                  // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                       // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                        // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                          // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Gets the Documents associated to a specific CRM Person</para>
        /// </summary>
        /// <param name="PersonID"></param>
        /// <returns></returns>
        public static async Task<List<DocumentModel>> ListForPersonAsync(int PersonID)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@PersonID", SqlDbType.Int).Value = PersonID;				                        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Persons_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                         // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                          // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];                                         // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                    // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];               // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                  // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                       // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                        // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                          // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Gets the Documents associated to a specific Billing Invoice</para>
        /// </summary>
        /// <param name="InvoiceID"></param>
        /// <returns></returns>
        public static async Task<List<DocumentModel>> ListForInvoiceAsync(int InvoiceID)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@InvoiceID", SqlDbType.Int).Value = InvoiceID;				                        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Invoices_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                         // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                          // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];                                         // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                    // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];               // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                  // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                       // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                        // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                          // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Gets the Documents associated to a specific Sales Project</para>
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <returns></returns>
        public static async Task<List<DocumentModel>> ListForProjectAsync(int ProjectID)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@ProjectID", SqlDbType.Int).Value = ProjectID;				                        // Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Projects_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                         // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                          // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];                                         // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                    // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];               // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                  // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                       // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                        // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                          // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Gets the Documents associated to a specific Sales Project Activity</para>
        /// </summary>
        /// <param name="ActivityID"></param>
        /// <returns></returns>
        public static async Task<List<DocumentModel>> ListForActivityAsync(long ActivityID)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@ActivityID", SqlDbType.BigInt).Value = ActivityID;				                                // BigInt NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_To_Activities_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                         // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                          // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];                                         // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                    // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];               // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                  // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                       // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                        // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                   // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                          // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        #endregion
    }
}
