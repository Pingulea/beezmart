﻿using BeezMart.Entities.Archiving;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.DAL.Archiving
{
	public static class Documents
    {
        public class Categories
        {
            public static async Task<bool> SaveAsync(DocumentModel.CategoryModel category)
            {
                bool success = false;
                using (Database db = new Database())
                {
                    if (category.ID.HasValue)
                    {
                        db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = category.ID;                             // SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
                        db.AddParam("@CategoryName", SqlDbType.NVarChar, 128).Value = category.Name;                    // NVarChar(128) NOT NULL
                        db.AddParam("@SubCategory", SqlDbType.NVarChar, 128).Value = category.SubCategory;              // NVarChar(128) NOT NULL
                        db.AddParam("@Discontinued", SqlDbType.Bit).Value = category.IsDiscontinued;                    // Bit = 0 NOT NULL DEFAULT 0
                        if ((await db.SaveRecordAsync("Archiving.Document_Categories_Upd")) > 0) success = true;
                    }
                    else
                    {
                        db.AddParam("@CategoryID", SqlDbType.SmallInt).Direction = ParameterDirection.Output;           // SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
                        db.AddParam("@CategoryName", SqlDbType.NVarChar, 128).Value = category.Name;                    // NVarChar(128) NOT NULL
                        db.AddParam("@SubCategory", SqlDbType.NVarChar, 128).Value = category.SubCategory;              // NVarChar(128) NOT NULL
                        db.AddParam("@Discontinued", SqlDbType.Bit).Value = category.IsDiscontinued;                    // Bit = 0 NOT NULL DEFAULT 0
                        if ((await db.SaveRecordAsync("Archiving.Document_Categories_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
                        if (success) category.ID = (short)db.Params["@CategoryID"].Value;
                    }
                }
                return success;
            }
            public static async Task<DocumentModel.CategoryModel?> RetrieveAsync(short categoryID)
            {
                DocumentModel.CategoryModel? entity = null;
                SqlDataReader rdrReader, row;
                using (Database db = new Database())
                {
                    db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = categoryID;
                    row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Document_Categories_Sel");
                    if (await rdrReader.ReadAsync())
                    {
                        entity = new DocumentModel.CategoryModel();
                        entity.ID = (short)row["Category_ID"];                          // SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
                        entity.Name = (string)row["Category_Name"];                     // NVarChar(128) NOT NULL
                        entity.SubCategory = (string)row["Sub_Category"];               // NVarChar(128) NOT NULL
                        entity.IsDiscontinued = (bool)row["Discontinued"];              // Bit NOT NULL DEFAULT 0
                    }
                    rdrReader.Dispose();
                }
                return entity;
            }
            /// <summary>
            ///		<para>Gets the list of available document categories.</para>
            ///		<para>Non-cached.</para>
            /// </summary>
            /// <returns></returns>
            public static async Task<List<string>> ListAsync()
            {
                List<string> list = new List<string>();
                SqlDataReader rdrReader, row;
                using (Database db = new Database())
                {
                    row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Document_Categories_Grid_3");
                    while (await rdrReader.ReadAsync())
                    {
                        if (row[0] != DBNull.Value) list.Add((string)row[0]);           // NVarChar(128) NOT NULL
                    }
                    rdrReader.Dispose();
                }
                return list;
            }
            public static async Task<List<DocumentModel.CategoryModel>> ListAvailableAsync()
            {
                DocumentModel.CategoryModel item;
                List<DocumentModel.CategoryModel> list = new List<DocumentModel.CategoryModel>();
                SqlDataReader rdrReader, row;
                using (Database db = new Database())
                {
                    row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Document_Categories_Grid_1");
                    while (await rdrReader.ReadAsync())
                    {
                        item = new DocumentModel.CategoryModel();
                        item.ID = (short)row["Category_ID"];                                // SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
                        item.Name = (string)row["Category_Name"];                           // NVarChar(128) NOT NULL
                        item.SubCategory = (string)row["Sub_Category"];                     // NVarChar(128) NOT NULL
                        item.IsDiscontinued = (bool)row["Discontinued"];                    // Bit NOT NULL DEFAULT 0
                        list.Add(item);
                    }
                    rdrReader.Dispose();
                }
                return list;
            }
            public static async Task<List<DocumentModel.CategoryModel>> ListAllAsync()
            {
                DocumentModel.CategoryModel item;
                List<DocumentModel.CategoryModel> list = new List<DocumentModel.CategoryModel>();
                SqlDataReader rdrReader, row;
                using (Database db = new Database())
                {
                    row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Document_Categories_Grid_2");
                    while (await rdrReader.ReadAsync())
                    {
                        item = new DocumentModel.CategoryModel();
                        item.ID = (short)row["Category_ID"];                                // SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
                        item.Name = (string)row["Category_Name"];                           // NVarChar(128) NOT NULL
                        item.SubCategory = (string)row["Sub_Category"];                     // NVarChar(128) NOT NULL
                        item.IsDiscontinued = (bool)row["Discontinued"];                    // Bit NOT NULL DEFAULT 0
                        list.Add(item);
                    }
                    rdrReader.Dispose();
                }
                return list;
            }
            public static async Task<bool> DeleteAsync(short categoryID, bool discontinued = true, string? updatedBy = null, DateTime? updatedOn = null)
            {
                bool success = false;
                using (Database db = new Database())
                {
                    db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = categoryID;          // SmallInt			 -- NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
                    db.AddParam("@Discontinued", SqlDbType.Bit).Value = discontinued;           // Bit = 1			 -- NOT NULL DEFAULT 0
                    if ((await db.DeleteRecordAsync("Archiving.Document_Categories_Del")) > 0) success = true;
                }
                return success;
            }
        }
		public static async Task<bool> SaveAsync(DocumentModel document)
		{
			bool success = false;
			using (Database db = new Database())
			{
				if (document.ID.HasValue)
				{
                    db.AddParam("@DocumentID", SqlDbType.BigInt).Value = document.ID.Value;				        // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = document.Title;                      // NVarChar(256) NOT NULL
                    db.AddParam("@Body", SqlDbType.NText).Value = document.Body;				                // NVarChar(MAX) = NULL
                    db.AddParam("@ExternalURL", SqlDbType.VarChar, 256).Value = document.ExternalUrl;           // VarChar(256) = NULL
                    db.AddParam("@DocumentDate", SqlDbType.SmallDateTime).Value = document.DocumentDate;        // SmallDateTime = NULL -- NOT NULL DEFAULT GETDATE()
                    db.AddParam("@ExpiringDate", SqlDbType.SmallDateTime).Value = document.ExpiringDate;        // SmallDateTime = NULL
                    db.AddParam("@ClosingDate", SqlDbType.SmallDateTime).Value = document.ClosingDate;          // SmallDateTime = NULL
                    db.AddParam("@ExternalID", SqlDbType.VarChar, 128).Value = document.ExternalID;             // VarChar(128) = NULL
                    db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = document.CategoryID;                 // SmallInt = NULL -- FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    db.AddParam("@DocumentStatusID", SqlDbType.TinyInt).Value = document.Status;                // TinyInt = NULL -- FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    db.AddParam("@AddedOn", SqlDbType.SmallDateTime).Value = document.AddedOn;                  // SmallDateTime = NULL -- NOT NULL DEFAULT GETDATE()
                    db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = document.UpdatedOn;              // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                    db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = document.UpdatedBy;              // NVarChar(128) = NULL
                    if ((await db.SaveRecordAsync("Archiving.Documents_Upd")) > 0) success = true;
				}
				else
				{
                    db.AddParam("@DocumentID", SqlDbType.BigInt).Direction = ParameterDirection.Output;         // BigInt OUTPUT NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = document.Title;                      // NVarChar(256) NOT NULL
                    db.AddParam("@Body", SqlDbType.NText).Value = document.Body;				                // NVarChar(MAX) = NULL
                    db.AddParam("@ExternalURL", SqlDbType.VarChar, 256).Value = document.ExternalUrl;           // VarChar(256) = NULL
                    db.AddParam("@DocumentDate", SqlDbType.SmallDateTime).Value = document.DocumentDate;        // SmallDateTime = NULL -- NOT NULL DEFAULT GETDATE()
                    db.AddParam("@ExpiringDate", SqlDbType.SmallDateTime).Value = document.ExpiringDate;        // SmallDateTime = NULL
                    db.AddParam("@ClosingDate", SqlDbType.SmallDateTime).Value = document.ClosingDate;          // SmallDateTime = NULL
                    db.AddParam("@ExternalID", SqlDbType.VarChar, 128).Value = document.ExternalID;             // VarChar(128) = NULL
                    db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = document.CategoryID;                 // SmallInt = NULL -- FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    db.AddParam("@DocumentStatusID", SqlDbType.TinyInt).Value = document.Status;                // TinyInt = NULL -- FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    db.AddParam("@AddedOn", SqlDbType.SmallDateTime).Value = document.AddedOn;                  // SmallDateTime = NULL -- NOT NULL DEFAULT GETDATE()
                    db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = document.UpdatedOn;              // SmallDateTime = NULL	NOT NULL DEFAULT GETDATE()
                    db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = document.UpdatedBy;              // NVarChar(128) = NULL
                    if ((await db.SaveRecordAsync("Archiving.Documents_Ins", clearParamsAfterCompletion: false)) > 0) success = true;
					if (success) document.ID = (long)db.Params["@DocumentID"].Value;
				}
			}
			return success;
		}
		public static async Task<DocumentModel?> RetrieveAsync(long documentID)
		{
			DocumentModel? entity = null;
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
				db.AddParam("@DocumentID", SqlDbType.BigInt).Value = documentID;
				row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_Sel");
				if ((await rdrReader.ReadAsync()))
				{
                    entity = new DocumentModel();
                    entity.ID = (long)row["Document_ID"];                                                                               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    entity.Title = (string)row["Title"];                                                                                // NVarChar(256) NOT NULL
                    if (row["Body"] != DBNull.Value) entity.Body = (string)row["Body"];			                                        // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) entity.ExternalUrl = (string)row["External_URL"];                          // VarChar(256) NULL
                    entity.DocumentDate = (DateTime)row["Document_Date"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) entity.ExpiringDate = (DateTime)row["Expiring_Date"];                     // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) entity.ClosingDate = (DateTime)row["Closing_Date"];                        // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) entity.ExternalID = (string)row["External_ID"];                             // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) entity.CategoryID = (short)row["Category_ID"];                              // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Category_Name"] != DBNull.Value) entity.CategoryName = (string)row["Category_Name"];                       // NVarChar(128) NULL
                    if (row["Sub_Category"] != DBNull.Value) entity.SubCategory = (string)row["Sub_Category"];                          // NVarChar(128) NULL
                    if (row["Document_Status_ID"] != DBNull.Value) entity.Status = (DocumentStatus)(byte)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    entity.AddedOn = (DateTime)row["Added_On"];                                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    entity.UpdatedOn = (DateTime)row["Updated_On"];                                                                     // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) entity.UpdatedBy = (string)row["Updated_By"];                                // NVarChar(128) NULL
                }
				rdrReader.Dispose();
			}
			return entity;
		}
        /// <summary>
        ///     <para>Lists documents added to archive (or having a document date) in a certain timeframe</para>
        /// </summary>
        /// <param name="timeframeStart"></param>
        /// <param name="timeframeEnd"></param>
        /// <returns></returns>
		public static async Task<List<DocumentModel>> ListByDocumentDateAsync(DateTime? timeframeStart, DateTime? timeframeEnd)
		{
			DocumentModel item;
			List<DocumentModel> list = new List<DocumentModel>();
			SqlDataReader rdrReader, row;
			using (Database db = new Database())
			{
                db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = timeframeStart;     // SmallDateTime = NULL
                db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = timeframeEnd;         // SmallDateTime = NULL
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_Grid_1");
				while (await rdrReader.ReadAsync())
				{
					item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                                // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];			                                        // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                          // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];                     // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                        // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                             // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                              // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Category_Name"] != DBNull.Value) item.CategoryName = (string)row["Category_Name"];                       // NVarChar(128) NULL
                    if (row["Sub_Category"] != DBNull.Value) item.SubCategory = (string)row["Sub_Category"];                          // NVarChar(128) NULL
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)(byte)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                     // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                                // NVarChar(128) NULL
                    list.Add(item);
				}
				rdrReader.Dispose();
			}
			return list;
		}
        /// <summary>
        ///     <para>Lists documents that have expired in a certain timeframe</para>
        /// </summary>
        /// <param name="timeframeStart"></param>
        /// <param name="timeframeEnd"></param>
        /// <returns></returns>
		public static async Task<List<DocumentModel>> ListByExpirationDateAsync(DateTime? timeframeStart, DateTime? timeframeEnd)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = timeframeStart;     // SmallDateTime = NULL
                db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = timeframeEnd;         // SmallDateTime = NULL
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_Grid_2");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                                // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];			                                        // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                          // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];                     // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                        // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                             // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                              // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Category_Name"] != DBNull.Value) item.CategoryName = (string)row["Category_Name"];                       // NVarChar(128) NULL
                    if (row["Sub_Category"] != DBNull.Value) item.SubCategory = (string)row["Sub_Category"];                          // NVarChar(128) NULL
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)(byte)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                     // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                                // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Gets the latest N document records that are not Superseded or Expired</para>
        /// </summary>
        /// <param name="maximumRowCount">Maximum number of records to retrieve from database table</param>
        /// <returns></returns>
		public static async Task<List<DocumentModel>> ListLatestAsync(int maximumRowCount = 1000)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                db.AddParam("@MaximumRowCount", SqlDbType.Int).Value = maximumRowCount;         // Int = 1000
                row = rdrReader = await db.GetSqlDataReaderAsync("Archiving.Documents_Grid_3");
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                               // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                                // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];			                                        // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                          // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                               // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];                     // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                        // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                             // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                              // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Category_Name"] != DBNull.Value) item.CategoryName = (string)row["Category_Name"];                       // NVarChar(128) NULL
                    if (row["Sub_Category"] != DBNull.Value) item.SubCategory = (string)row["Sub_Category"];                          // NVarChar(128) NULL
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)(byte)row["Document_Status_ID"];     // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                         // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                     // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                                // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
        /// <summary>
        ///     <para>Deletes (or recovers) a document in Archive</para>
        /// </summary>
        /// <param name="documentID"></param>
        /// <param name="status"></param>
        /// <param name="updatedBy"></param>
        /// <param name="updatedOn"></param>
        /// <returns></returns>
		public static async Task<bool> DeleteAsync(long documentID, DocumentStatus status = DocumentStatus.Deleted, string? updatedBy = null, DateTime? updatedOn = null)
		{
			bool success = false;
			using (Database db = new Database())
			{
                db.AddParam("@DocumentID", SqlDbType.BigInt).Value = documentID;
                db.AddParam("@DocumentStatusID", SqlDbType.TinyInt).Value = status;         // TinyInt = 200        -- DELETED, 'Can be considered as deleted, in Recycle Bin.'
                db.AddParam("@UpdatedBy", SqlDbType.NVarChar, 128).Value = updatedBy;       // NOT NULL DEFAULT GETDATE()
                db.AddParam("@UpdatedOn", SqlDbType.SmallDateTime).Value = updatedOn.HasValue ? updatedOn.Value : DateTime.Now;
                if ((await db.DeleteRecordAsync("Archiving.Documents_Del")) > 0) success = true;
			}
			return success;
		}
        public static async Task<List<DocumentModel>> LookupAsync(DocumentModel.LookupFilter filterCriteria, short maxRecordsCount = 1000)
        {
            DocumentModel item;
            List<DocumentModel> list = new List<DocumentModel>();
            SqlDataReader rdrReader, row;
            using (Database db = new Database())
            {
                StringBuilder stbTransactSqlCommand = new StringBuilder();
                #region Build Database Query
                StringBuilder stbSelectClause = new StringBuilder();
                StringBuilder stbFromClause = new StringBuilder();
                StringBuilder stbWhereClause = new StringBuilder();
                StringBuilder stbOrderByClause = new StringBuilder();
                #region Build SELECT clause
                stbSelectClause.AppendLine(filterCriteria.HasAnyCriteria ? $"SELECT TOP {maxRecordsCount}" : $"SELECT TOP {Defaults.Search_Result_Unfiltered_MaxCount}");
                stbSelectClause.AppendLine(" D.Document_ID");
                stbSelectClause.AppendLine(",D.Title");
                //stbSelectClause.AppendLine(",D.Body");
                stbSelectClause.AppendLine(",D.External_URL");
                stbSelectClause.AppendLine(",D.Document_Date");
                stbSelectClause.AppendLine(",D.Expiring_Date");
                stbSelectClause.AppendLine(",D.Closing_Date");
                stbSelectClause.AppendLine(",D.External_ID");
                stbSelectClause.AppendLine(",D.Category_ID");
                stbSelectClause.AppendLine(",C.Category_Name");
                stbSelectClause.AppendLine(",C.Sub_Category");
                stbSelectClause.AppendLine(",D.Document_Status_ID");
                stbSelectClause.AppendLine(",D.Added_On");
                stbSelectClause.AppendLine(",D.Updated_On");
                stbSelectClause.AppendLine(",D.Updated_By");
                #endregion
                #region Build FROM clause
                stbFromClause.AppendLine("FROM Archiving.Documents D");
                stbFromClause.AppendLine("    LEFT JOIN Archiving.Document_Categories C ON D.Category_ID = C.Category_ID");
                #endregion
                #region Build WHERE clause
                string searchOperator = filterCriteria.SearchOperator.ToString().ToUpper();
                if (!string.IsNullOrEmpty(filterCriteria.TitlePattern))
                {
                    stbWhereClause.AppendLine($"{searchOperator} D.Title LIKE @Title");
                    db.AddParam("@Title", SqlDbType.NVarChar, 256).Value = filterCriteria.TitleExactMatch ? filterCriteria.TitlePattern : "%" + filterCriteria.TitlePattern + "%";
                }
                if (filterCriteria.CategoryID.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} D.Category_ID = @CategoryID");
                    db.AddParam("@CategoryID", SqlDbType.SmallInt).Value = filterCriteria.CategoryID.Value;
                }
                if (filterCriteria.Status.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} D.Document_Status_ID = @DocumentStatusID");
                    db.AddParam("@DocumentStatusID", SqlDbType.TinyInt).Value = (byte)filterCriteria.Status;
                }
                if (!string.IsNullOrEmpty(filterCriteria.ExternalUrl))
                {
                    stbWhereClause.AppendLine($"{searchOperator} D.External_URL LIKE @ExternalURL");
                    db.AddParam("@ExternalURL", SqlDbType.VarChar, 256).Value = "%" + filterCriteria.ExternalUrl + "%";
                }
                if (!string.IsNullOrEmpty(filterCriteria.ExternalID))
                {
                    stbWhereClause.AppendLine($"{searchOperator} (D.External_ID LIKE @ExternalID)");
                    db.AddParam("@ExternalID", SqlDbType.VarChar, 128).Value = "%" + filterCriteria.ExternalID + "%";
                }
                if (filterCriteria.TimeframeStart.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} (D.Document_Date >= @TimeframeStart OR D.Added_On >= @TimeframeStart)");
                    db.AddParam("@TimeframeStart", SqlDbType.SmallDateTime).Value = filterCriteria.TimeframeStart.Value;
                }
                if (filterCriteria.TimeframeEnd.HasValue)
                {
                    stbWhereClause.AppendLine($"{searchOperator} (D.Document_Date <= @TimeframeEnd OR D.Added_On <= @TimeframeEnd)");
                    db.AddParam("@TimeframeEnd", SqlDbType.SmallDateTime).Value = filterCriteria.TimeframeEnd.Value;
                }
				if (filterCriteria.DocumentIDs != null && filterCriteria.DocumentIDs.Count > 0)
				{
					StringBuilder stbTemp = new StringBuilder();
					foreach (long documentID in filterCriteria.DocumentIDs)
					{
						stbTemp.Append(documentID);
						stbTemp.Append(',');
					}
					stbTemp.Remove(stbTemp.Length - 1, 1);
					stbWhereClause.AppendLine($"{searchOperator} D.Document_ID IN ({stbTemp})");
				}

                if (stbWhereClause.Length > 0)
                {
                    stbWhereClause.Remove(0, searchOperator.Length + 1);
                    stbWhereClause.Insert(0, "WHERE ");
                }
                #endregion
                #region Build ORDER BY clause
                stbOrderByClause.AppendLine("ORDER BY D.Document_ID DESC");
                #endregion
                stbTransactSqlCommand.AppendLine(stbSelectClause.ToString());
                stbTransactSqlCommand.AppendLine(stbFromClause.ToString());
                stbTransactSqlCommand.AppendLine(stbWhereClause.ToString());
                stbTransactSqlCommand.AppendLine(stbOrderByClause.ToString());
                #endregion
                row = rdrReader = await db.GetSqlDataReaderFromTextCommandAsync(stbTransactSqlCommand.ToString());
                while (await rdrReader.ReadAsync())
                {
                    item = new DocumentModel();
                    item.ID = (long)row["Document_ID"];                                                                                 // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
                    item.Title = (string)row["Title"];                                                                                  // NVarChar(256) NOT NULL
                    //if (row["Body"] != DBNull.Value) item.Body = (string)row["Body"];                                                 // NVarChar(MAX) NULL
                    if (row["External_URL"] != DBNull.Value) item.ExternalUrl = (string)row["External_URL"];                            // VarChar(256) NULL
                    item.DocumentDate = (DateTime)row["Document_Date"];                                                                 // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Expiring_Date"] != DBNull.Value) item.ExpiringDate = (DateTime)row["Expiring_Date"];                       // SmallDateTime NULL
                    if (row["Closing_Date"] != DBNull.Value) item.ClosingDate = (DateTime)row["Closing_Date"];                          // SmallDateTime NULL
                    if (row["External_ID"] != DBNull.Value) item.ExternalID = (string)row["External_ID"];                               // VarChar(128) NULL
                    if (row["Category_ID"] != DBNull.Value) item.CategoryID = (short)row["Category_ID"];                                // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
                    if (row["Category_Name"] != DBNull.Value) item.CategoryName = (string)row["Category_Name"];                         // NVarChar(128) NULL
                    if (row["Sub_Category"] != DBNull.Value) item.SubCategory = (string)row["Sub_Category"];                            // NVarChar(128) NULL
                    if (row["Document_Status_ID"] != DBNull.Value) item.Status = (DocumentStatus)(byte)row["Document_Status_ID"];       // TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
                    item.AddedOn = (DateTime)row["Added_On"];                                                                           // SmallDateTime NOT NULL DEFAULT GETDATE()
                    item.UpdatedOn = (DateTime)row["Updated_On"];                                                                       // SmallDateTime NOT NULL DEFAULT GETDATE()
                    if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];                                      // NVarChar(128) NULL
                    list.Add(item);
                }
                rdrReader.Dispose();
            }
            return list;
        }
    }
}
