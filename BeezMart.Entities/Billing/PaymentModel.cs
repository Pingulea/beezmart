﻿using System;

namespace BeezMart.Entities.Billing
{
	public class PaymentModel
	{
		public int? ID { get; set; }
		public int InvoiceID { get; set; }
		public DateTime? PaymentDate { get; set; }
		public decimal Amount { get; set; }
		public string? Currency { get; set; }
		public string? Comments { get; set; }
		public int? PaymentCheckID { get; set; }
		public string? InvoiceAccountingID { get; set; }
		public DateTime InvoiceIssueDate { get; set; }
		public string? CustomerName { get; set; }
		public int? OrganizationID { get; set; }
		public int? PersonID { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}