﻿using System;

namespace BeezMart.Entities.Billing
{
	public class InvoiceItemModel
	{
		public long? ID { get; set; }
		public int InvoiceID { get; set; }
		public int? ProductCatalogItemID { get; set; }
		public short? ProductCatalogCategoryID { get; set; }
		public char ProductOrService { get; set; } = 'P';
		public int? SalesOrderID { get; set; }
		public long? SalesOrderItemID { get; set; }
		public int? DeliveryDocumentID { get; set; }
		public long? DeliveryDocumentItemID { get; set; }
		public string? Name { get; set; }
		public decimal UnitPrice { get; set; }
		public string? UnitMeasure { get; set; }
		public int Quantity { get; set; }
		public decimal TotalPrice { get { return this.UnitPrice * (decimal)this.Quantity; } }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}