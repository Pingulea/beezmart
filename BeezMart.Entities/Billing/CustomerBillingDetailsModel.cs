﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeezMart.Entities.Billing
{
    public class CustomerBillingDetailsModel
    {
        public int? OrganizationID { get; set; }
        public int? PersonID { get; set; }
        public string? CustomerName { get; set; }
        public string? City { get; set; }
        public string? StateProvince { get; set; }
        public string? StreetAddress1 { get; set; }
        public string? RegistrationNumber { get; set; }
        public string? TaxID { get; set; }
        public string? BankName { get; set; }
        public string? BankAccountIBAN { get; set; }
        public string? Currency { get; set; }
        public string? Source { get; set; }

        public int? BillingAddressID { get; set; }
        public int? BankAccountID { get; set; }
    }
}
