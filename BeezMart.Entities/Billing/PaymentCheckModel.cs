﻿using System;

namespace BeezMart.Entities.Billing
{
	public class PaymentCheckModel
	{
		public int? ID { get; set; }
		public string? Reference { get; set; }
		public decimal Amount { get; set; }
		public string? Currency { get; set; }
		public DateTime IssueDate { get; set; } = DateTime.Now;
		public DateTime DueDate { get; set; } = DateTime.Now.AddDays(7);
		public DateTime? CachedInDate { get; set; }
		public string? Comments { get; set; }
		public string CheckType { get; set; } = "Check";
		public string? CustomerName { get; set; }
		public int? OrganizationID { get; set; }
		public int? PersonID { get; set; }
		public string? BankName { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}