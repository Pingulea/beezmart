﻿using System;

namespace BeezMart.Entities.Billing
{
	public class HandoverDelegateModel
	{
		public short? ID { get; set; }
		public string? Name { get; set; }
		public string? IdField1 { get; set; }
		public string? IdField2 { get; set; }
		public string? IdField3 { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}