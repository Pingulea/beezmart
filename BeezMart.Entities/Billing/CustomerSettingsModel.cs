﻿using System;

namespace BeezMart.Entities.Billing
{
	public class CustomerSettingsModel
	{
		public int? ID { get; set; }
		public int? OrganizationID { get; set; }
		public int? PersonID { get; set; }
		public string? RegistrationNumber { get; set; }
		public string? TaxID { get; set; }
		public string? Currency { get; set; }
		public int? PreferredAccountID { get; set; }
		public byte? MaximumDiscountPercentage { get; set; }
		public byte? PaymentAllowanceInDays { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}