﻿using System;

namespace BeezMart.Entities.Billing
{
	public class HandoverTransportModel
	{
		public short? ID { get; set; }
		public string? Means { get; set; }
		public string? Registration { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}