namespace BeezMart.Entities.Billing
{
    public static class Defaults
    {
        public static string Currency = "EUR";
        public static string Auto_Serials_Format = "BZMRT 00100";
    }
}
