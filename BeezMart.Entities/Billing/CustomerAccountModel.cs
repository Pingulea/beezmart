﻿using System;
using System.Text;

namespace BeezMart.Entities.Billing
{
	public class CustomerAccountModel
	{
		public int? ID { get; set; }
		public int? OrganizationID { get; set; }
		public int? PersonID { get; set; }
		public string? IBAN { get; set; }
		public string? Bank { get; set; }
		public string? Currency { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }

		

		public static string? FormatAccountNumber(string? IBAN)
		{
			if (string.IsNullOrEmpty(IBAN)) return null;
			IBAN = IBAN.Replace(" ", string.Empty);
			StringBuilder stb = new StringBuilder();
			for (int i = 0; i < IBAN.Length; i++)
			{
				stb.Append(IBAN[i]);
				if ( (i+1) % 4 == 0) stb.Append(" ");
			}
			return stb.ToString().Trim();
		}

		public static bool IsValidIBAN(string? accountIBAN)
		{
			if (string.IsNullOrEmpty(accountIBAN)) return false;
			accountIBAN = accountIBAN.Replace(" ", string.Empty);
			if (accountIBAN.Length != 24) return false;
			return true;
		}
	}
}