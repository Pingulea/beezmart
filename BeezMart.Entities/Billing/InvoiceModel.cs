﻿using System;
using System.Collections.Generic;
using BeezMart.Utils;
using Security = BeezMart.Utils.Security;

namespace BeezMart.Entities.Billing
{
	public enum InvoiceStatus : byte
	{
        /// <summary>
        ///     <para>This status should not be used.</para>
        /// </summary>
		NotSet = 0,
        /// <summary>
        ///     <para>The invoice is still being built, this is a temporary status.</para>
        /// </summary>
		Draft = 20,
        /// <summary>
        ///     <para>All fields are filled in according to rules defind by company.</para>
        /// </summary>
		Validated = 40,
        /// <summary>
        ///     <para>The invoice has been sent to customer. No further edits are allowed.</para>
        /// </summary>
		SentToCustomer = 60,
        /// <summary>
        ///     <para>The invoice has been cancelled. No further edits are allowed.</para>
        /// </summary>
		Cancelled = 80,
        /// <summary>
        ///     <para>The invoice has been paid and/or archived. No further edits are allowed.</para>
        /// </summary>
		Closed = 100
	}

	public enum InvoiceType : byte
	{
		NotSet = 0,
		Regular = 20,
		Storno = 40,
		Advance = 60,
		Risturn = 80,
		ProForma = 100
	}

	public class InvoiceModel
	{
		public int? ID { get; set; }
		public DateTime IssueDate { get; set; } = DateTime.Now;
		public DateTime? DueDate { get; set; }
		public DateTime? ClosingDate { get; set; }
		public string? AccountingID { get; set; }
		public decimal VATpercentage { get; set; }
		public string? Currency { get; set; }
		public InvoiceType Type { get; set; } = InvoiceType.Regular;
		public InvoiceStatus Status { get; set; } = InvoiceStatus.Draft;
		public int? OrganizationID { get; set; }
		public int? PersonID { get; set; }
		public string? CustomerName { get; set; }
		public string? CustomerCity { get; set; }
		public string? CustomerProvince { get; set; }
		public string? CustomerAddress { get; set; }
		public string? CustomerRegistrationNumber { get; set; }
		public string? CustomerTaxID { get; set; }
		public string? CustomerBankName { get; set; }
		public string? CustomerBankAccount { get; set; }
		public string? DelegateName { get; set; }
		public string? DelegateID1 { get; set; }
		public string? DelegateID2 { get; set; }
		public string? DelegateID3 { get; set; }
		public string? TransportName { get; set; }
		public string? TransportDetail1 { get; set; }
		public string? TransportDetail2 { get; set; }
		public string? TransportDetail3 { get; set; }
		public string? TransportDetail4 { get; set; }
		public string? Detail1 { get; set; }
		public string? Detail2 { get; set; }
		public string? Detail3 { get; set; }
		public string? Detail4 { get; set; }
		public int? SalesOrderID { get; set; }
		public int? DeliveryDocumentID { get; set; }
		public int? RelatedInvoiceID { get; set; }
		public string? RenderingLayout { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }

		public decimal TotalValue { get; set; }
		public decimal TotalVAT { get { return (this.TotalValue * (this.VATpercentage / 100M)); } }
		public decimal TotalDue { get { return (this.TotalValue * (1 + this.VATpercentage / 100M)); } }
		public decimal TotalPayed { get; set; }
		public decimal RemainingToBePayed { get { return this.Status == InvoiceStatus.Cancelled ? 0 : (this.TotalDue - this.TotalPayed); } }

		public class LookupFilter
		{
			public string? CustomerNamePattern
			{
				get { return this._customerNamePattern; }
				set { this._customerNamePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public bool CustomerNameExactMatch = false;
            public int? OrganizationID = null;
            public int? PersonID = null;
            public string? CustomerTaxIdPattern
			{
				get { return this._customerTaxIdPattern; }
				set { this._customerTaxIdPattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public bool CustomerTaxIdExactMatch = false;
			public DateTime? IssuedAfter = null;
			public DateTime? IssuedBefore = null;
			public string? Currency = null;
			public List<int>? InvoiceIDs = null;
			public List<string>? AccountingIDs = null;
			public InvoiceType? InvoiceType = null;
			public InvoiceStatus? InvoiceStatus = null;
            public bool Overdue = false;

			public bool HasAnyCriteria
			{
				get
				{
					if (!string.IsNullOrEmpty(this.CustomerNamePattern)) return true;
                    if (this.OrganizationID.HasValue) return true;
                    if (this.PersonID.HasValue) return true;
                    if (!string.IsNullOrEmpty(this.CustomerTaxIdPattern)) return true;
					if (this.IssuedAfter.HasValue) return true;
					if (this.IssuedBefore.HasValue) return true;
					if (!string.IsNullOrEmpty(this.Currency)) return true;
					if (this.InvoiceIDs != null && this.InvoiceIDs.Count > 0) return true;
					if (this.AccountingIDs != null && this.AccountingIDs.Count > 0) return true;
					if (this.InvoiceType.HasValue) return true;
					if (this.InvoiceStatus.HasValue) return true;
					return false;
				}
            }
            public SearchOperator SearchOperator { get; set; } = SearchOperator.And;

            private string? _customerNamePattern = null;
			private string? _customerTaxIdPattern = null;
		}
	}
}