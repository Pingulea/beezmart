﻿using BeezMart.Utils;
using System;
using System.Collections.Generic;
using Security = BeezMart.Utils.Security;

namespace BeezMart.Entities.CRM
{
    public class OrganizationModel
    {
        public int? ID { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NamePrefix { get; set; }
        public string? NameSuffix { get; set; }
        public string? AlternateNames { get; set; }
        public int? ParentOrgID { get; set; }
        public byte SegmentID { get; set; } = 0;
        public string? Segment { get; set; }
        public string? CountryOfOrigin { get; set; }
        public int? SiteIdHome { get; set; }
        public int? SiteIdBill { get; set; }
        public int? SiteIdShip { get; set; }
        public string? HQ_Country { get; set; }
        public string? HQ_Region { get; set; }
        public string? HQ_StateProvince { get; set; }
        public string? HQ_City { get; set; }
        public string? HQ_Address { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public string? UpdatedBy { get; set; }

        public class LookupFilter
        {
            public string? NamePattern
            {
                get { return this._namePattern; }
                set { this._namePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public bool NameExactMatch = false;
            public string? City
            {
                get { return this._city; }
                set { this._city = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public string? Contact
            {
                get { return this._contact; }
                set { this._contact = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public List<byte>? InSegments = null;
            public List<short>? HasFlags = null;

            public bool HasAnyCriteria
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(this.NamePattern)) return true;
                    if (!string.IsNullOrWhiteSpace(this.City)) return true;
                    if (!string.IsNullOrWhiteSpace(this.Contact)) return true;
                    if (this.InSegments != null && this.InSegments.Count > 0) return true;
                    if (this.HasFlags != null && this.HasFlags.Count > 0) return true;
                    return false;
                }
            }
            public SearchOperator SearchOperator { get; set; } = SearchOperator.And;

            private string? _namePattern = null;
            private string? _city = null;
            private string? _contact = null;
        }

        public static List<string> NamePrefixes
        {
            get
            {
                return new List<string> {
                    "A.F.",
                    "P.F.A.",
                    "R.A.",
                    "S.C.",
                    "S.C.S.",
                    "S.N.C."
                    };
            }
        }
        public static List<string> NameSuffixes
        {
            get
            {
                List<string> list = new List<string> { };
                list.Add("S.A.");
                list.Add("S.R.L.");
                list.Add("S.C.A.");
                list.Add("C.I.A.");
                return list;
            }
        }
    }
}
