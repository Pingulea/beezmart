﻿using System;

namespace BeezMart.Entities.CRM
{
	public class OrgSegmentModel
	{

		public byte? ID { get; set; }
		public string? Name { get; set; }
		public string? Comments { get; set; }
		public int OrgsCount { get; set; }
	}
}