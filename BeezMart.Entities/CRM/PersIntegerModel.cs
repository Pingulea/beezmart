﻿using System;

namespace BeezMart.Entities.CRM
{
	public class PersIntegerModel
	{

		public long? ID { get; set; }
		public int PersonID { get; set; }
		public short FieldID { get; set; }
		public FieldType FieldType { get { return FieldType.Integer; } }
		public string? Category { get; set; }
		public string? Name { get; set; }
		public int Value { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}