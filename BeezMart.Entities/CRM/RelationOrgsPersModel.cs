﻿using System;

namespace BeezMart.Entities.CRM
{
	public class RelationOrgsPersModel
	{
		public long? ID { get; set; }

		public int OrganizationID { get; set; }
		public int PersonID { get; set; }
		public string? PositionTitle { get; set; }

		public short? JobTypeID { get; set; }
		public string? JobTypeName { get; set; }
		public string? JobTypeCategory { get; set; }

		public string? PersFirstName { get; set; }
		public string? PersLastName { get; set; }
		public string? PersPrefix { get; set; }
		public string? PersSuffix { get; set; }
		public string? PersNicknames { get; set; }
		public string? PersSegment { get; set; }
		public byte PersSegmentID { get; set; }

		public string? OrgName { get; set; }
		public string? OrgPrefix { get; set; }
		public string? OrgSuffix { get; set; }
		public string? OrgAltnames { get; set; }
		public string? OrgSegment { get; set; }
		public byte OrgSegmentID { get; set; }

		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }

		public class RelationTypeModel
		{
			public short? ID { get; set; }
			public string? Category { get; set; }
			public string? Name { get; set; }
			public string? Comments { get; set; }
			public bool Discontinued { get; set; } = false;
			public DateTime UpdatedOn { get; set; } = DateTime.Now;
			public string? UpdatedBy { get; set; }
		}
	}
}