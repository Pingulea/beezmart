﻿using System;

namespace BeezMart.Entities.CRM
{
	public class PersSegmentModel
	{

		public byte? ID { get; set; }
		public string? Name { get; set; }
		public string? Comments { get; set; }
		public int PersCount { get; set; }
	}
}