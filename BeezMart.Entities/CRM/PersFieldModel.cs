﻿using System;

namespace BeezMart.Entities.CRM
{

	public class PersFieldModel
	{

		public short? ID { get; set; }
		public string? Category { get; set; }
		public string? Name { get; set; }
		public string? Comments { get; set; }
		public bool IsUnique { get; set; } = false;
		public bool IsRetired { get; set; } = false;
		public FieldType FieldType { get; set; }
	}
}