﻿using System;

namespace BeezMart.Entities.CRM
{

	public class Segment
	{
		public byte ID { get; set; }
		public string? Name { get; set; }
		public string? Comment { get; set; }
	}

	public enum ContactStatus : byte
	{
		NotSet = 0,
		Unconfirmed = 1,
		Confirmed = 2,
		Incomplete = 3,
		Invalid = 4,
		NonExisting = 5,
		ToBeChanged = 6,
		HavingProblems = 7
	}

	public enum ContactType : byte
	{
		NotSet = 0,
		Email = 1,
		Phone = 2,
		Mobile = 3,
		Fax = 4,
		Web = 5,
		Telex = 6,
		Postal = 7,
		Chat = 8
	}

	public enum ActivityStatus : byte
	{
		NotSet = 0,
		Pending = 10,
		Cancelled = 20,
		InProgress = 50,
		InProgress_1_percent = 51,
		InProgress_5_percent = 52,
		InProgress_10_percent = 53,
		InProgress_15_percent = 54,
		InProgress_20_percent = 55,
		InProgress_25_percent = 56,
		InProgress_30_percent = 57,
		InProgress_35_percent = 58,
		InProgress_40_percent = 59,
		InProgress_45_percent = 60,
		InProgress_50_percent = 61,
		InProgress_55_percent = 62,
		InProgress_60_percent = 63,
		InProgress_65_percent = 64,
		InProgress_70_percent = 65,
		InProgress_75_percent = 66,
		InProgress_80_percent = 67,
		InProgress_85_percent = 68,
		InProgress_90_percent = 69,
		InProgress_95_percent = 70,
		InProgress_99_percent = 71,
		Completed = 250
	}
}
