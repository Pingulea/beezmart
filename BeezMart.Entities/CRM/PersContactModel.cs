﻿using System;

namespace BeezMart.Entities.CRM
{
	public class PersContactModel
	{

		public long? ID { get; set; }
		public int PersonID { get; set; }
		public int? SiteID { get; set; }
		public string? Contact { get; set; }
		public string? Comments { get; set; }
		public ContactType Type { get; set; } = ContactType.NotSet;
		public ContactStatus Status { get; set; } = ContactStatus.NotSet;
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}