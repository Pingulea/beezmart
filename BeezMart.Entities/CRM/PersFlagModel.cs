﻿using System;


namespace BeezMart.Entities.CRM
{
	public class PersFlagModel
	{
				
		public long? ID { get; set; }
		public int PersonID { get; set; }
		public short FieldID { get; set; }
		public FieldType FieldType { get { return FieldType.Flag; } }
		public string? Name { get; set; }
		public string? Category { get; set; }
		public string? Comments { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}