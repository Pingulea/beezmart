﻿using System;

namespace BeezMart.Entities.CRM
{

	public class OrgSiteModel
	{

		public int? ID { get; set; }
		public int OrganizationID { get; set; }
		public ContactStatus ContactStatus { get; set; } = ContactStatus.Unconfirmed;
		public string? SiteName { get; set; }
		public string? Country { get; set; }
		public string? Region { get; set; }
		public string? StateProvince { get; set; }
		public string? City { get; set; }
		public string? StreetAddress1 { get; set; }
		public string? StreetAddress2 { get; set; }
		public string? StreetAddress3 { get; set; }
		public string? Department { get; set; }
		public string? Directions { get; set; }
		public string? ZipPostCode { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}