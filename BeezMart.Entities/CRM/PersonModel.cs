﻿using BeezMart.Utils;
using System;
using System.Collections.Generic;
using Security = BeezMart.Utils.Security;

namespace BeezMart.Entities.CRM
{
    public class PersonModel
    {
        public int? ID { get; set; }
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? NamePrefix { get; set; }
        public string? NameSuffix { get; set; }
        public string? Nicknames { get; set; }
        public byte SegmentID { get; set; } = 0;
        public string? Segment { get; set; }
        public string? CountryOfOrigin { get; set; }
        public int? SiteIdHome { get; set; }
        public int? SiteIdBill { get; set; }
        public int? SiteIdShip { get; set; }

        public string? Home_Country { get; set; }
        public string? Home_Region { get; set; }
        public string? Home_StateProvince { get; set; }
        public string? Home_City { get; set; }
        public string? Home_Address { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public string? UpdatedBy { get; set; }

        public class LookupFilter
        {
            public string? NamePattern
            {
                get { return this._namePattern; }
                set { this._namePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public bool NameExactMatch = false;
            public string? City
            {
                get { return this._city; }
                set { this._city = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public string? Contact
            {
                get { return this._contact; }
                set { this._contact = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public List<byte>? InSegments = null;
            public List<short>? HasFlags = null;

            public bool HasAnyCriteria
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(this.NamePattern)) return true;
                    if (!string.IsNullOrWhiteSpace(this.City)) return true;
                    if (!string.IsNullOrWhiteSpace(this.Contact)) return true;
                    if (this.InSegments != null && this.InSegments.Count > 0) return true;
                    if (this.HasFlags != null && this.HasFlags.Count > 0) return true;
                    return false;
                }
            }
            public SearchOperator SearchOperator { get; set; } = SearchOperator.And;

            private string? _namePattern = null;
            private string? _city = null;
            private string? _contact = null;
        }

        public static List<string> NamePrefixes
        {
            get
            {
                return new List<string> {
                    "Ms.",
                    "Mr.",
                    "Mrs.",
                    "Dr.",
                    "Prof."
                    };
            }
        }
        public static List<string> NameSuffixes
        {
            get
            {
                List<string> list = new List<string> { };
                list.Add("Phd.");
                list.Add("Jr.");
                list.Add("Sr.");
                return list;
            }
        }
    }
}
