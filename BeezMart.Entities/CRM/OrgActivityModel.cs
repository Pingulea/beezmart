﻿using System;

namespace BeezMart.Entities.CRM
{
    public class OrgActivityModel
    {
        public long? ID { get; set; }
        public int OrganizationID { get; set; }
        public ActivityStatus Status { get; set; } = ActivityStatus.NotSet;         // TinyInt = 10
        public string? StatusComment { get; set; }                                  // NVarChar(256) = NULL
        public short TypeID { get; set; }                                           // SmallInt
        public string? Title { get; set; }                                          // NVarChar(1024) NOT NULL
        public string? Details { get; set; }                                        // NVarChar(MAX) = NULL
        public DateTime? ScheduledFor { get; set; }                                 // SmallDateTime = NULL
        public DateTime? ReminderOn { get; set; }                                   // SmallDateTime = NULL
        public DateTime? StartedOn { get; set; }                                    // SmallDateTime = NULL
        public DateTime? CompletedOn { get; set; }                                  // SmallDateTime = NULL
        public string? RequestedBy { get; set; }                                    // NVarChar(128) = NULL
        public string? CompletedBy { get; set; }                                    // NVarChar(128) = NULL
        public bool IsBillable { get; set; } = false;                               // Bit = 0
        public string? InvoiceIDs { get; set; }                                     // VarChar(256) = NULL
        public int? DurationInMinutes { get; set; }                                 // Int = NULL
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public string? UpdatedBy { get; set; }

        public class ActivityTypeModel
        {
            public short? ID { get; set; }                                              // SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
            public string? Name { get; set; }                                           // NVarChar(64) NOT NULL
            public string? Category { get; set; }                                       // NVarChar(64) NULL
            public string? Comments { get; set; }                                       // NVarChar(MAX) NULL
            public bool IsDiscontinued { get; set; }                                    // Bit NOT NULL DEFAULT 0
            public DateTime UpdatedOn { get; set; } = DateTime.Now;                     // SmallDateTime NOT NULL DEFAULT GETDATE()
            public string? UpdatedBy { get; set; }                                      // NVarChar(128) NULL
        }
    }
}
