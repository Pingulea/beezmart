﻿using BeezMart.Entities;
using System;

namespace BeezMart.Entities.CRM
{
	public class OrgDecimalModel
	{

		public long? ID { get; set; }
		public int OrganizationID { get; set; }
		public short FieldID { get; set; }
		public FieldType FieldType { get { return FieldType.Decimal; } }
		public string? Category { get; set; }
		public string? Name { get; set; }
		public decimal Value { get; set; }
		public DateTime UpdatedOn { get; set; } = DateTime.Now;
		public string? UpdatedBy { get; set; }
	}
}