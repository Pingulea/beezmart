﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeezMart.Utils;
using Security = BeezMart.Utils.Security;

namespace BeezMart.Entities.TimeSheet
{
	public class ActivityModel
	{
		public class ActivityTypeModel
		{
			public short? ID { get; set; }													// SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
			public string? Name { get; set; }												// NVarChar(64) NOT NULL
			public string? Category { get; set; }											// NVarChar(64) NULL
			public string? Comments { get; set; }											// NVarChar(MAX) NULL
			public bool IsDiscontinued { get; set; } = false;								// Bit NOT NULL DEFAULT 0
			public DateTime UpdatedOn { get; set; } = DateTime.Now;							// SmallDateTime NOT NULL DEFAULT GETDATE()
			public string? UpdatedBy { get; set; }											// NVarChar(128) NULL
		}

		public long? ID { get; set; }													// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		public int? ProjectID { get; set; }                                             // Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
		public string? ProjectTitle { get; set; }										// NVarChar(256) NULL
		public int? OrganizationID { get; set; }										// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		public int? PersonID { get; set; }												// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		public string? CustomerName { get; set; }										// NVarChar(256) NOT NULL
		public string? EndCustomer { get; set; }                                         // NVarChar(256) NULL
		public ActivityStatus Status { get; set; } = ActivityStatus.NotSet;				// TinyInt NOT NULL FOREIGN KEY REFERENCES CRM.App_Activity_Status(Activity_Status_ID)
		public string? ActivityStatusComment { get; set; }								// NVarChar(256) NULL
		public short? ActivityTypeID { get; set; }                                      // SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
		public string? ActivityTypeName { get; set; }									// NVarChar(64) NULL
		public string? ActivityCategory { get; set; }									// NVarChar(64) NULL
		public string? Title { get; set; }												// NVarChar(1024) NOT NULL
		public string? Details { get; set; }												// NVarChar(MAX) NULL
		public DateTime ScheduledStart { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public DateTime ScheduledEnd { get; set; } = DateTime.Now.AddHours(24);			// SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
		public DateTime? AssignedOn { get; set; }										// SmallDateTime NULL
		public string? AssignedTo { get; set; }											// NVarChar(128) NULL
		public DateTime? DeadlineOn { get; set; }										// SmallDateTime NULL
		public DateTime? StartedOn { get; set; }										// SmallDateTime NULL
		public DateTime? EndedOn { get; set; }											// SmallDateTime NULL
		public DateTime? ReminderOn { get; set; }										// SmallDateTime NULL
		public string? RequestedBy { get; set; }											// NVarChar(128) NULL
		public string? RequestingDepartment { get; set; }								// NVarChar(128) NULL
		public string? CompletedBy { get; set; }											// NVarChar(128) NULL
		public string? LoggedBy { get; set; }											// NVarChar(128) NULL
		public bool Billable { get; set; } = false;										// Bit NOT NULL DEFAULT 0
		public string? InvoiceIDs { get; set; }											// VarChar(256) NULL
		public TimeSpan? Duration { get; set; }										    // Int NULL
		public DateTime UpdatedOn { get; set; } = DateTime.Now;							// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }											// NVarChar(128) NULL
		
		public class LookupFilter
		{
			public string? TitlePattern 
			{
				get { return this._titlePattern; }
				set { this._titlePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public bool TitleExactMatch = false;
			public string? CustomerNamePattern
			{
				get { return this._customerNamePattern; }
				set { this._customerNamePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public int? OrganizationID = null;
            public int? PersonID = null;
            public short? ActivityTypeID = null;
			public ActivityStatus? Status;
			public int? ProjectID { get; set; }
            public string? ProjectTitlePattern
            {
                get { return this._projectTitlePattern; }
                set { this._projectTitlePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public string? RequestorNamePattern
			{
				get { return this._requestorNamePattern; }
				set { this._requestorNamePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public string? RequestingDepartment
			{
				get { return this._requestingDepartmentNamePattern; }
				set { this._requestingDepartmentNamePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public string? CompletedBy
			{
				get { return this._completedByPattern; }
				set { this._completedByPattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public DateTime? TimeframeStart = null;
			public DateTime? TimeframeEnd = null;
			public bool HasAnyCriteria
			{
				get
				{
					if (!string.IsNullOrWhiteSpace(this.TitlePattern)) return true;
					if (!string.IsNullOrWhiteSpace(this.CustomerNamePattern)) return true;
                    if (this.OrganizationID.HasValue) return true;
                    if (this.PersonID.HasValue) return true;
                    if (this.ActivityTypeID.HasValue) return true;
                    if (this.Status.HasValue) return true;
					if (this.ProjectID.HasValue) return true;
					if (!string.IsNullOrWhiteSpace(this.ProjectTitlePattern)) return true;
                    if (!string.IsNullOrWhiteSpace(this.RequestorNamePattern)) return true;
					if (!string.IsNullOrWhiteSpace(this.RequestingDepartment)) return true;
					if (!string.IsNullOrWhiteSpace(this.CompletedBy)) return true;
					if (this.TimeframeStart.HasValue) return true;
					if (this.TimeframeEnd.HasValue) return true;
					return false;
				}
            }
            public SearchOperator SearchOperator { get; set; } = SearchOperator.And;

            private string? _customerNamePattern = null;
            private string? _requestorNamePattern = null;
			private string? _titlePattern = null;
            private string? _projectTitlePattern = null;
            private string? _requestingDepartmentNamePattern = null;
			private string? _completedByPattern = null;
		}
	}
}
