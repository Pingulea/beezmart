﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeezMart.Utils;
using Security = BeezMart.Utils.Security;

namespace BeezMart.Entities.TimeSheet
{
	public class ProjectModel
	{
		public class CategoryModel
		{
			public short? ID { get; set; }												// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
			public string? Name { get; set; }											// NVarChar(128) NOT NULL
			public string? SubCategory { get; set; }										// NVarChar(128) NOT NULL
			public bool IsDiscontinued { get; set; } = false;							// Bit NOT NULL DEFAULT 0
		}

		public int? ID { get; set; }													// Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		public string? Title { get; set; }												// NVarChar(256) NOT NULL
		public string? ProjectManager { get; set; }										// NVarChar(256) NULL
		public string? ContactDetails { get; set; }										// NVarChar(256) NULL
		public string? Details { get; set; }												// NVarChar(MAX) NULL
		public int? OrganizationID { get; set; }										// Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		public int? PersonID { get; set; }												// Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		public string? CustomerName { get; set; }										// NVarChar(256) NOT NULL
		public string? EndCustomer { get; set; }											// NVarChar(256) NULL
		public short? CategoryID { get; set; }											// SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
		public string? CategoryName { get; set; }										// NVarChar(128) NULL
		public string? SubCategory { get; set; }											// NVarChar(128) NULL
		public ProjectStatus Status { get; set; }										// TinyInt NULL FOREIGN KEY REFERENCES Sales.Project_Status_Names(Status_ID)
		public DateTime ScheduledStart { get; set; } = DateTime.Now;				    // SmallDateTime NOT NULL DEFAULT GETDATE()
		public DateTime ScheduledEnd { get; set; } = DateTime.Now.AddMonths(3);			// SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
		public DateTime? StartedOn { get; set; }										// SmallDateTime NULL
		public DateTime? EndedOn { get; set; }											// SmallDateTime NULL
		public DateTime UpdatedOn { get; set; } = DateTime.Now;							// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }											// NVarChar(128) NULL
		
		public class LookupFilter
		{
			public string? TitlePattern 
			{
				get { return this._titlePattern; }
				set { this._titlePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public bool TitleExactMatch = false;
			public short? CategoryID = null;
			public ProjectStatus? Status;
			public List<int>? ProjectIDs { get; set; }
			public string? CustomerNamePattern
			{
				get { return this._customerNamePattern; }
				set { this._customerNamePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public string? ProjectManager
			{
				get { return this._projectManagerNamePattern; }
				set { this._projectManagerNamePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public string? ContactDetails
			{
				get { return this._contactDetailsPattern; }
				set { this._contactDetailsPattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public int? OrganizationID = null;
			public int? PersonID = null;
			public DateTime? TimeframeStart = null;
			public DateTime? TimeframeEnd = null;
			public bool HasAnyCriteria
			{
				get
				{
					if (!string.IsNullOrWhiteSpace(this.TitlePattern)) return true;
                    if (this.CategoryID.HasValue) return true;
                    if (this.Status.HasValue) return true;
                    if (this.ProjectIDs != null && this.ProjectIDs.Count > 0) return true;
					if (!string.IsNullOrWhiteSpace(this.CustomerNamePattern)) return true;
                    if (!string.IsNullOrWhiteSpace(this.ProjectManager)) return true;
					if (!string.IsNullOrWhiteSpace(this.ContactDetails)) return true;
                    if (this.OrganizationID.HasValue) return true;
                    if (this.PersonID.HasValue) return true;
                    if (this.TimeframeStart.HasValue) return true;
                    if (this.TimeframeEnd.HasValue) return true;
                    return false;
				}
            }
            public SearchOperator SearchOperator { get; set; } = SearchOperator.And;

            private string? _customerNamePattern = null;
			private string? _titlePattern = null;
			private string? _projectManagerNamePattern = null;
			private string? _contactDetailsPattern = null;
		}
	}
}
