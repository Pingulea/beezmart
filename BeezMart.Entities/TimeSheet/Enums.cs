﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeezMart.Entities.TimeSheet
{
	public enum ProjectStatus : byte
	{
		Undefined = 0,
		Dreaming = 10,
		Concept = 20,
		Planned = 30,
		InExecution = 40,
		Finalized = 50,
		CustomerApproved = 60,
		Billed = 70,
		Paid = 80,
		ClosedAndArchived = 90,
		Cancelled = 100
	}

	public enum ActivityStatus : byte
	{
		NotSet = 0,
		Pending = 10,
		Cancelled = 20,
		InProgress = 50,
		InProgress_1_percent = 51,
		InProgress_5_percent = 52,
		InProgress_10_percent = 53,
		InProgress_15_percent = 54,
		InProgress_20_percent = 55,
		InProgress_25_percent = 56,
		InProgress_30_percent = 57,
		InProgress_35_percent = 58,
		InProgress_40_percent = 59,
		InProgress_45_percent = 60,
		InProgress_50_percent = 61,
		InProgress_55_percent = 62,
		InProgress_60_percent = 63,
		InProgress_65_percent = 64,
		InProgress_70_percent = 65,
		InProgress_75_percent = 66,
		InProgress_80_percent = 67,
		InProgress_85_percent = 68,
		InProgress_90_percent = 69,
		InProgress_95_percent = 70,
		InProgress_99_percent = 71,
		Completed = 250
	}
}
