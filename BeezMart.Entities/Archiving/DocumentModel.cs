﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeezMart.Utils;
using Security = BeezMart.Utils.Security;

namespace BeezMart.Entities.Archiving
{
    public class DocumentModel
    {
		public class CategoryModel
		{
			public short? ID { get; set; }												// SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
			public string? Name { get; set; }											// NVarChar(128) NOT NULL
			public string? SubCategory { get; set; }										// NVarChar(128) NOT NULL
			public bool IsDiscontinued { get; set; } = false;							// Bit NOT NULL DEFAULT 0
		}

		public long? ID { get; set; }											// BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		public string? Title { get; set; }										// NVarChar(256) NOT NULL
		public string? Body { get; set; }										// NVarChar(MAX) NULL
		public string? ExternalUrl { get; set; }									// VarChar(256) NULL
		public DateTime DocumentDate { get; set; } = DateTime.Now;				// SmallDateTime NOT NULL DEFAULT GETDATE()
		public DateTime? ExpiringDate { get; set; }								// SmallDateTime NULL
		public DateTime? ClosingDate { get; set; }								// SmallDateTime NULL
		public string? ExternalID { get; set; }									// VarChar(128) NULL
		public short? CategoryID { get; set; }                                  // SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
		public string? CategoryName { get; set; }								// NVarChar(128) NULL
		public string? SubCategory { get; set; }									// NVarChar(128) NULL
        public DocumentStatus Status { get; set; } = DocumentStatus.Final;      // TinyInt NULL FOREIGN KEY REFERENCES Archiving.Document_Status_Names(Status_ID)
		public DateTime AddedOn { get; set; } = DateTime.UtcNow;				// SmallDateTime NOT NULL DEFAULT GETDATE()
		public DateTime UpdatedOn { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }									// NVarChar(128) NULL

		public class LookupFilter
		 {
			public List<long>? DocumentIDs = null;
			public string? TitlePattern 
			{
				get { return this._titlePattern; }
				set { this._titlePattern = Security.Helper.CleanupSqlSearchTextFilter(value); }
			}
			public bool TitleExactMatch = false;
			public short? CategoryID = null;
			public DocumentStatus? Status;
            public string? ExternalUrl
            {
                get { return this._externalUrl; }
                set { this._externalUrl = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public string? ExternalID
            {
                get { return this._externalID; }
                set { this._externalID = Security.Helper.CleanupSqlSearchTextFilter(value); }
            }
            public DateTime? TimeframeStart = null;
			public DateTime? TimeframeEnd = null;
			public bool HasAnyCriteria
			{
				get
				{
					if (!string.IsNullOrWhiteSpace(this.TitlePattern)) return true;
					if (!string.IsNullOrWhiteSpace(this.ExternalUrl)) return true;
					if (!string.IsNullOrWhiteSpace(this.ExternalID)) return true;
					//if (!string.IsNullOrWhiteSpace(this.ContactDetails)) return true;
					//if (this.ProjectIDs != null && this.ProjectIDs.Count > 0) return true;
					return false;
				}
            }
            public SearchOperator SearchOperator { get; set; } = SearchOperator.And;

			private string? _titlePattern = null;
			private string? _externalUrl = null;
            private string? _externalID = null;
			//private string _contactDetailsPattern = null;
		}
    }
}
