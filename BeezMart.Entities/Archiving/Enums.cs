using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeezMart.Entities.Archiving
{
	public enum DocumentStatus : byte
	{
		Undefined = 0,
        /// <summary>
        ///     <para>The document with this status is a work in progress</para>
        /// </summary>
		Draft = 20,
        /// <summary>
        ///     <para>This is the final, in effect, version of the document</para>
        /// </summary>
        Final = 40,
        /// <summary>
        ///     <para>The document is not applicable anymore because it is superseded by another similar document</para>
        /// </summary>
        Superseded = 60,
        /// <summary>
        ///     <para>The document is not applicable anymore because it is expired</para>
        /// </summary>
        Expired = 70,
        /// <summary>
        ///     <para>No longer valid or in effect</para>
        /// </summary>
        Cancelled = 200,
        /// <summary>
        ///     <para>Can be considered as deleted, in Recycle Bin</para>
        /// </summary>
        Deleted = 250
	}
}
