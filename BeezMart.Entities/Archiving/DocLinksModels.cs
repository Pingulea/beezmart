﻿using System;

namespace BeezMart.Entities.Archiving
{
    public class LinkToOrganizationModel
    {
		public long? ID { get; set; }                                           // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
        public long DocumentID { get; set; }                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
        public int OrganizationID { get; set; }                                 // Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
        public DateTime UpdatedOn { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }									// NVarChar(128) NULL
    }
    public class LinkToPersonModel
    {
		public long? ID { get; set; }                                           // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
        public long DocumentID { get; set; }                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
        public int PersonID { get; set; }                                       // Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
        public DateTime UpdatedOn { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }									// NVarChar(128) NULL
    }
    public class LinkToInvoiceModel
    {
		public long? ID { get; set; }                                           // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
        public long DocumentID { get; set; }                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
        public int InvoiceID { get; set; }                                      // Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
        public DateTime UpdatedOn { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }									// NVarChar(128) NULL
    }
    public class LinkToProjectModel
    {
		public long? ID { get; set; }                                           // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
        public long DocumentID { get; set; }                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
        public int ProjectID { get; set; }                                      // Int NOT NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
        public DateTime UpdatedOn { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }									// NVarChar(128) NULL
    }
    public class LinkToActivityModel
    {
		public long? ID { get; set; }                                           // BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
        public long DocumentID { get; set; }                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
        public long ActivityID { get; set; }                                    // BigInt NOT NULL FOREIGN KEY REFERENCES Sales.Activities(Activity_ID)
        public DateTime UpdatedOn { get; set; } = DateTime.Now;					// SmallDateTime NOT NULL DEFAULT GETDATE()
		public string? UpdatedBy { get; set; }									// NVarChar(128) NULL
    }
}
