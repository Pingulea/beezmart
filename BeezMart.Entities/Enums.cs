﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.Entities
{
    public enum FieldType : byte
    {
        NotDefined = 0,
        String = 1,
        Integer = 2,
        DateTime = 3,
        HugeText = 4,
        TimeInterval = 5,
        Decimal = 6,
        TableOrXml = 7,
        Flag = 8
    }

    public enum EntityType : byte
    {
        NotDefined = 0,

        CrmOrganization = 10,
        CrmPerson = 11,
        CrmOrgs_Site = 12,
        CrmPers_Site = 13,
        CrmOrgs_Activity = 14,
        CrmPers_Activity = 15,

        CrmOrgs_Contact = 20,
        CrmPers_Contact = 21,
        CrmOrgs_NumberAttribute = 22,
        CrmPers_NumberAttribute = 23,
        CrmOrgs_TextAttribute = 24,
        CrmPers_TextAttribute = 25,
        CrmOrgs_Flag = 26,
        CrmPers_Flag = 27,
        CrmOrgs_HugeText = 28,
        CrmPers_HugeText = 29,

        CrmRelation_OrgsPers = 40,

        SalesProject = 50,
        SalesActivity = 51,
        SalesOrder = 52,
        SalesOrderItem = 53,

        BillingInvoice = 70,
        BillingInvoiceItem = 71,
        BillingPayment = 72,
        BillingPaymentCheck = 73,
        BillingCustomerIBAN = 74,
        BillingInvoicingDetail = 75,
        BillingHandoverDelegate = 76,
        BillingHandoverTransport = 77,

        ArchivingDocument = 240
    }
}
