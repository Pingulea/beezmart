﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeezMart.Entities.Geography;

public class Country
{
    public string? ID { get; set; }
    public string? Code { get; set; }
    public string? CodeUN { get; set; }
    public string? Name { get; set; }
    public bool Show { get; set; }
}

public class StateProvince
{
    public short ID { get; set; }
    public string? CountryID { get; set; }
    public string? Region { get; set; }
    public string? Abbr { get; set; }
    public string? Name { get; set; }
}
