--#############################################################################
--#  This script is intended solely for development purposes.
--#  The script is deletng all object from the database:
--#      tables, views, stored procedures, functions etc.
--#  Hence, all data from the database gets wiped out
--#  USE WITH CAUTION!
--#############################################################################

USE [BeezMart-Dev]
GO

IF DB_NAME() <> 'BeezMart-Dev'
   RAISERROR('Error: ''USE [BeezMart-Dev]'' failed! Maybe wrong database name was given? Killing the SPID now.', 22, 127)
GO

SET NOCOUNT OFF

--=============================================================================
--=  Delete the functions and stored procedures using cursors
--=  https://docs.microsoft.com/en-us/sql/t-sql/language-elements/declare-cursor-transact-sql
--=============================================================================
DECLARE @BeezMartObjectSchema NVARCHAR(128)
DECLARE @BeezMartObjectName NVARCHAR(128)
DECLARE @TsqlCommand NVARCHAR(1024)
-------------------------------------------------------------------------------
DECLARE BeezMart_User_Functions CURSOR READ_ONLY FOR
    SELECT SCHEMA_NAME([Schema_ID]) AS [Schema], [Name]
    FROM [Sys].[Objects]
    WHERE [Type_Desc] = 'SQL_Scalar_Function';
OPEN BeezMart_User_Functions
FETCH NEXT FROM BeezMart_User_Functions INTO @BeezMartObjectSchema, @BeezMartObjectName
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @TsqlCommand = 'DROP FUNCTION [' + @BeezMartObjectSchema + '].[' + @BeezMartObjectName + ']'
    EXEC(@TsqlCommand)
    FETCH NEXT FROM BeezMart_User_Functions INTO @BeezMartObjectSchema, @BeezMartObjectName
END
CLOSE BeezMart_User_Functions
DEALLOCATE BeezMart_User_Functions;
-------------------------------------------------------------------------------
DECLARE BeezMart_Stored_Procedures CURSOR READ_ONLY FOR
    SELECT SCHEMA_NAME([Schema_ID]) AS [Schema], [Name]
    FROM [Sys].[Objects]
    WHERE [Type_Desc] = 'SQL_Stored_Procedure';
OPEN BeezMart_Stored_Procedures
FETCH NEXT FROM BeezMart_Stored_Procedures INTO @BeezMartObjectSchema, @BeezMartObjectName
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @TsqlCommand = 'DROP PROCEDURE [' + @BeezMartObjectSchema + '].[' + @BeezMartObjectName + ']'
    EXEC(@TsqlCommand)
    FETCH NEXT FROM BeezMart_Stored_Procedures INTO @BeezMartObjectSchema, @BeezMartObjectName
END
CLOSE BeezMart_Stored_Procedures
DEALLOCATE BeezMart_Stored_Procedures;
-------------------------------------------------------------------------------

--=============================================================================
--=  Delete the app-specific tables created by installer.
--=  This will preserve the common tables: Asp.Net Identity and the Settings.
--=============================================================================
DROP TABLE IF EXISTS [Archiving].[Documents_To_Activities];
DROP TABLE IF EXISTS [Archiving].[Documents_To_Projects];
DROP TABLE IF EXISTS [Archiving].[Documents_To_Organizations];
DROP TABLE IF EXISTS [Archiving].[Documents_To_Persons];
DROP TABLE IF EXISTS [Archiving].[Documents_To_Invoices];
DROP TABLE IF EXISTS [Archiving].[Documents];
DROP TABLE IF EXISTS [Archiving].[App_Document_Status];
DROP TABLE IF EXISTS [Archiving].[Document_Categories];
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS [Billing].[Handover_Delegates];
DROP TABLE IF EXISTS [Billing].[Handover_Transport];
DROP TABLE IF EXISTS [Billing].[Payments];
DROP TABLE IF EXISTS [Billing].[Payment_Checks];
DROP TABLE IF EXISTS [Billing].[Invoice_Items];
DROP TABLE IF EXISTS [Billing].[Invoice_Accounting_IDs];
DROP TABLE IF EXISTS [Billing].[Invoices];
DROP TABLE IF EXISTS [Billing].[Customer_Settings];
DROP TABLE IF EXISTS [Billing].[Customer_Accounts];
DROP TABLE IF EXISTS [Billing].[App_Invoice_Status];
DROP TABLE IF EXISTS [Billing].[App_Invoice_Type];
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS [Delivery].[Document_Items];
DROP TABLE IF EXISTS [Delivery].[Delegates];
DROP TABLE IF EXISTS [Delivery].[Fleet_Vehicles];
DROP TABLE IF EXISTS [Delivery].[Documents];
DROP TABLE IF EXISTS [Delivery].[App_Document_Status];
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS [Sales].[Activities];
DROP TABLE IF EXISTS [Sales].[Activity_Types];
DROP TABLE IF EXISTS [Sales].[Projects];
DROP TABLE IF EXISTS [Sales].[Project_Categories];
DROP TABLE IF EXISTS [Sales].[Order_Items];
DROP TABLE IF EXISTS [Sales].[Orders];
DROP TABLE IF EXISTS [Sales].[Product_Catalog_Items];
DROP TABLE IF EXISTS [Sales].[Product_Catalog_Categories];
DROP TABLE IF EXISTS [Sales].[App_Activity_Status];
DROP TABLE IF EXISTS [Sales].[App_Project_Status];
DROP TABLE IF EXISTS [Sales].[App_Order_Delivery_Methods];
DROP TABLE IF EXISTS [Sales].[App_Order_Status];
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS [CRM].[Relation_Orgs_Pers];
DROP TABLE IF EXISTS [CRM].[Relation_Orgs_Pers_Job_Types];
DROP TABLE IF EXISTS [CRM].[Orgs_Activities];
DROP TABLE IF EXISTS [CRM].[Orgs_Activity_Types];
DROP TABLE IF EXISTS [CRM].[Orgs_Contacts];
DROP TABLE IF EXISTS [CRM].[Orgs_DateTime_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_Decimal_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_Flag_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_HugeText_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_Integer_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_String_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_TableOrXml_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_TimeInterval_Fields];
DROP TABLE IF EXISTS [CRM].[Orgs_Field_Names];
DROP TABLE IF EXISTS [CRM].[Orgs_Sites];
DROP TABLE IF EXISTS [CRM].[Organizations];
DROP TABLE IF EXISTS [CRM].[Orgs_Segment_Names];
DROP TABLE IF EXISTS [CRM].[Pers_Activities];
DROP TABLE IF EXISTS [CRM].[Pers_Activity_Types];
DROP TABLE IF EXISTS [CRM].[Pers_Contacts];
DROP TABLE IF EXISTS [CRM].[Pers_DateTime_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_Decimal_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_Flag_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_HugeText_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_Integer_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_String_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_TableOrXml_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_TimeInterval_Fields];
DROP TABLE IF EXISTS [CRM].[Pers_Field_Names];
DROP TABLE IF EXISTS [CRM].[Pers_Sites];
DROP TABLE IF EXISTS [CRM].[Persons];
DROP TABLE IF EXISTS [CRM].[Pers_Segment_Names];
DROP TABLE IF EXISTS [CRM].[App_Activity_Status];
DROP TABLE IF EXISTS [CRM].[App_Contact_Status];
DROP TABLE IF EXISTS [CRM].[App_Contact_Types];
DROP TABLE IF EXISTS [CRM].[App_States_Provinces];
DROP TABLE IF EXISTS [CRM].[App_Countries];
-------------------------------------------------------------------------------

--=============================================================================
--=  Delete the remaining tables: Asp.Net Identity and the Settings.
--=============================================================================
/*
DROP TABLE IF EXISTS [dbo].[AspNetRoleClaims];
DROP TABLE IF EXISTS [dbo].[AspNetUserClaims];
DROP TABLE IF EXISTS [dbo].[AspNetUserRoles];
DROP TABLE IF EXISTS [dbo].[AspNetUserLogins];
DROP TABLE IF EXISTS [dbo].[AspNetRoles];
DROP TABLE IF EXISTS [dbo].[AspNetUsers];
*/
-------------------------------------------------------------------------------
/*
DROP TABLE IF EXISTS [App].[Entity_Types];
DROP TABLE IF EXISTS [App].[Field_Types];
DROP TABLE IF EXISTS [App].[Settings];
*/
-------------------------------------------------------------------------------

--=============================================================================
--=  Cleanup statistics, to display what has been done.
--=============================================================================
DECLARE @RemainingFunctions SMALLINT
DECLARE @RemainingProcedures SMALLINT
DECLARE @RemainingViews SMALLINT
DECLARE @RemainingTables SMALLINT
DECLARE @RemainingAppTables SMALLINT
-------------------------------------------------------------------------------
SET @RemainingFunctions =
    (SELECT COUNT(*) FROM [Sys].[Objects] WHERE [Type_Desc] = 'SQL_Scalar_Function');
SET @RemainingProcedures =
    (SELECT COUNT(*) FROM [Sys].[Objects] WHERE [Type_Desc] = 'SQL_Stored_Procedure');
SET @RemainingViews =
    (SELECT COUNT(*) FROM [Sys].[Objects] WHERE [Type_Desc] = 'View');
SET @RemainingTables =
    (SELECT COUNT(*) FROM [Sys].[Objects] WHERE [Type_Desc] = 'User_Table');
SET @RemainingAppTables =
    (
    SELECT COUNT(*) FROM [Sys].[Objects]
    WHERE SCHEMA_NAME(SCHEMA_ID) IN ('CRM', 'Billing', 'Sales', 'Delivery', 'Archiving')
        AND [Type_Desc] = 'User_Table'
    );
-------------------------------------------------------------------------------
PRINT 'Remaining functions: ' + CAST(@RemainingFunctions AS VARCHAR)
PRINT 'Remaining procedures: ' + CAST(@RemainingProcedures AS VARCHAR)
PRINT 'Remaining views: ' + CAST(@RemainingViews AS VARCHAR)
PRINT 'Remaining tables: ' + CAST(@RemainingTables AS VARCHAR)
PRINT 'Remaining app tables: ' + CAST(@RemainingAppTables AS VARCHAR)
PRINT ''
PRINT 'DONE'
PRINT ''
--=============================================================================

SET NOCOUNT ON
GO