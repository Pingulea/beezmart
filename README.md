# BeezMart

Business management application for small companies. It includes functionalities for:
+   CRM: managing customers, with contact info, segmentation, flags, history.
+   Invoicing: keep track of issued invoices and associated payments.
+   TimeSheet: keep track of work items and activities performed during projects.
+   Document Archiving: upload to an Azure Storage Account while linking to customers, invoices, projects.

##  Quick start

1.  Clone this repository to your machine.
2.  Build the [BeezMart](./BeezMart) project to obtain the executable code: [see details here](./wiki/Building-with-.NET-SDK).
3.  Deploy the executable code to your web server or [Azure App Service](./wiki/Publish-in-Azure-App-Service).
4.  Prepare an empty SQL Server database for the app and build the Connection String.
4.  prepare and create the [expected configuration settings for the app](./wiki/Hosting-configuration-settings).
5.  Prepare the [user access management](./wiki/User-access-management), if `Azure Entra ID` is to be used: [full details here](./wiki/Prepare-Azure-Entra-tenant).
6.  Initialize BeezMart, by accessing for a first time.
    +   If `Azure Entra ID` is being used, the first-time user would need to have the role `BeezMart.Application-Administrator` assigned.
7.  Start using BeezMart.



