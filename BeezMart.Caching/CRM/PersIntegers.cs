using BeezMart.Entities.CRM;
using Repo = BeezMart.DAL.CRM.PersIntegers;

namespace BeezMart.Caching.CRM;

public static class PersIntegers
{
	/// <summary>
	///		<para>Gets the available categories of integer fields, 10-minutes cached</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<IEnumerable<string>> GetCategoriesAsync()
	{
		if (_categories == null)
		{
			_categories = await Repo.ListCategoriesAsync();
			_categories_lastUpdate = DateTime.Now;
		}
		if (_categories_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveCategoriesAsync();
		}
		return _categories;
	}
	/// <summary>
	///		<para>Gets the available integer types (or names), 10-minutes cached</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<IEnumerable<PersFieldModel>> GetFieldTypesAsync()
	{
		if (_fieldTypes == null)
		{
			_fieldTypes = await Repo.ListAvailableFieldNamesAsync();
			_fieldTypes_lastUpdate = DateTime.Now;
		}
		if (_fieldTypes_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveFieldTypesAsync();
		}
		return _fieldTypes;
	}

	private static async Task RetrieveCategoriesAsync()
	{
		_categories = await Repo.ListCategoriesAsync();
		_categories_lastUpdate = DateTime.Now;
	}
	private static IEnumerable<string>? _categories = null;
	private static DateTime _categories_lastUpdate;

	private static async Task RetrieveFieldTypesAsync()
	{
		_fieldTypes = await Repo.ListAvailableFieldNamesAsync();
		_fieldTypes_lastUpdate = DateTime.Now;
	}
	private static IEnumerable<PersFieldModel>? _fieldTypes = null;
	private static DateTime _fieldTypes_lastUpdate;
}