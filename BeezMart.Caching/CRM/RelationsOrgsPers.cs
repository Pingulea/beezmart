﻿using Relations = BeezMart.Entities.CRM.RelationOrgsPersModel;
using Repo = BeezMart.DAL.CRM.RelationsOrgsPers;

namespace BeezMart.Caching.CRM;

public class RelationsOrgsPers
{
	/// <summary>
	///		<para>Gets the available categories of relations (or job types), 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<string>> GetCategoriesAsync()
	{
		if (_categories == null)
		{
			_categories = await Repo.RelationTypes.ListCategoriesAsync();
			_categories_lastUpdate = DateTime.Now;
			return _categories;
		}
		if (_categories_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveCategoriesAsync();
		}
		return _categories;
	}
	/// <summary>
	///		<para>Gets the available relations (or job types), 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<Relations.RelationTypeModel>> GetRelationTypesAsync()
	{
		if (_relationTypes == null)
		{
			_relationTypes = await Repo.RelationTypes.ListAsync();
			_relationTypes_lastUpdate = DateTime.Now;
		}
		if (_relationTypes_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveRelationTypesAsync();
		}
		return _relationTypes;
	}
	/// <summary>
	///		<para>Gets the available relations (or job types) corresponding to an (optional) category. Non cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<Relations.RelationTypeModel>> GetRelationTypesAsync(string? category = null)
	{
		if (string.IsNullOrEmpty(category)) return await Repo.RelationTypes.ListAsync();
		else return await Repo.RelationTypes.ListAsync(category);
	}

	private static async Task RetrieveCategoriesAsync()
	{
		_categories = await Repo.RelationTypes.ListCategoriesAsync();
		_categories_lastUpdate = DateTime.Now;
	}
	private static List<string>? _categories = null;
	private static DateTime _categories_lastUpdate;

	private static async Task RetrieveRelationTypesAsync()
	{
		_relationTypes = await Repo.RelationTypes.ListAsync();
		_relationTypes_lastUpdate = DateTime.Now;
	}
	private static List<Relations.RelationTypeModel>? _relationTypes = null;
	private static DateTime _relationTypes_lastUpdate;
}
