﻿using BeezMart.Entities.CRM;
using Repo = BeezMart.DAL.CRM.OrgSegments;

namespace BeezMart.Caching.CRM;

public class OrgSegments
{
	/// <summary>
	///		<para>Gets the available segments, without associated records count, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<OrgSegmentModel>> GetListAsync()
	{
		if (_segments == null)
		{
			_segments = await Repo.ListAsync();
			_segments_lastUpdate = DateTime.Now;
		}
		if (_segments_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveSegments();
		}
		return _segments;
	}

	private static async Task RetrieveSegments()
	{
		_segments = await Repo.ListAsync();
		_segments_lastUpdate = DateTime.Now;
	}
	private static List<OrgSegmentModel>? _segments = null;
	private static DateTime _segments_lastUpdate;
}
