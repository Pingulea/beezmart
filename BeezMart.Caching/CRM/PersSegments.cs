﻿using BeezMart.Entities.CRM;
using Repo = BeezMart.DAL.CRM.PersSegments;

namespace BeezMart.Caching.CRM;

public class PersSegments
{
	/// <summary>
	///		<para>Gets the available segments, without associated records count, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<PersSegmentModel>> GetListAsync()
	{
		if (_segments == null)
		{
			_segments = await Repo.ListAsync();
			_segments_lastUpdate = DateTime.Now;
		}
		if (_segments_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveSegmentsAsync();
		}
		return _segments;
	}

	private static async Task RetrieveSegmentsAsync()
	{
		_segments = await Repo.ListAsync();
		_segments_lastUpdate = DateTime.Now;
	}
	private static List<PersSegmentModel>? _segments = null;
	private static DateTime _segments_lastUpdate;
}