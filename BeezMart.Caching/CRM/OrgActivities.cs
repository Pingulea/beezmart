﻿using BeezMart.Entities.CRM;
using Repo = BeezMart.DAL.CRM.OrgActivities;

namespace BeezMart.Caching.CRM;

public static class OrgActivities
{
	/// <summary>
	///		<para>Gets the available categories of activity types, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<string>> GetCategoriesAsync()
	{
		if (_categories == null)
		{
			_categories = await Repo.ActivityTypes.ListCategoriesAsync();
			_categories_lastUpdate = DateTime.Now;
		}
		if (_categories_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveCategoriesAsync();
		}
		return _categories;
	}
	/// <summary>
	///		<para>Gets the available activity types, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<OrgActivityModel.ActivityTypeModel>> GetActivityTypesAsync()
	{
		if (_activityTypes == null)
		{
			_activityTypes = await Repo.ActivityTypes.ListAsync();
			_activityTypes_lastUpdate = DateTime.Now;
		}
		if (_activityTypes_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveActivityTypesAsync();
		}
		return _activityTypes;
	}

	private static async Task RetrieveCategoriesAsync()
	{
		_categories = await Repo.ActivityTypes.ListCategoriesAsync();
		_categories_lastUpdate = DateTime.Now;
	}
	private static List<string>? _categories = null;
	private static DateTime _categories_lastUpdate;

	private static async Task RetrieveActivityTypesAsync()
	{
		_activityTypes = await Repo.ActivityTypes.ListAsync();
		_activityTypes_lastUpdate = DateTime.Now;
	}
	private static List<OrgActivityModel.ActivityTypeModel>? _activityTypes = null;
	private static DateTime _activityTypes_lastUpdate;
}
