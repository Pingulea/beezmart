using BeezMart.Entities.CRM;
using Repo = BeezMart.DAL.CRM.OrgFlags;

namespace BeezMart.Caching.CRM;

public static class OrgFlags
{
	/// <summary>
	///		<para>Gets the available categories of flags, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<string>> GetCategoriesAsync()
	{
		if (_categories == null)
		{
			_categories = await Repo.ListCategoriesAsync();
			_categories_lastUpdate = DateTime.Now;
		}
		if (_categories_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveCategoriesAsync();
		}
		return _categories;
	}
	/// <summary>
	///		<para>Gets the available flag types (or names), 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<OrgFieldModel>> GetFlagTypesAsync()
	{
		if (_flagTypes == null)
		{
			_flagTypes = await Repo.ListAvailableFieldNamesAsync();
			_flagTypes_lastUpdate = DateTime.Now;
		}
		if (_flagTypes_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveFlagTypesAsync();
		}
		return _flagTypes;
	}

	private static async Task RetrieveCategoriesAsync()
	{
		_categories = await Repo.ListCategoriesAsync();
		_categories_lastUpdate = DateTime.Now;
	}
	private static List<string>? _categories = null;
	private static DateTime _categories_lastUpdate;

	private static async Task RetrieveFlagTypesAsync()
	{
		_flagTypes = await Repo.ListAvailableFieldNamesAsync();
		_flagTypes_lastUpdate = DateTime.Now;
	}
	private static List<OrgFieldModel>? _flagTypes = null;
	private static DateTime _flagTypes_lastUpdate;
}
