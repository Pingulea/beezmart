﻿using BeezMart.Entities.Archiving;
using Repo = BeezMart.DAL.Archiving.Documents;

namespace BeezMart.Caching.Archiving;

public class DocumentCategories
{
	/// <summary>
	///		<para>Gets the available categories of activity types, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<string>> GetCategoriesAsync()
	{
		if (_categories == null)
		{
			_categories = await Repo.Categories.ListAsync();
			_categories_lastUpdate = DateTime.Now;
		}
		if (_categories_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveCategoriesAsync();
		}
		return _categories;
	}
	/// <summary>
	///		<para>Gets the available activity types, 10-minutes cached.</para>
	/// </summary>
	/// <returns></returns>
	public static async Task<List<DocumentModel.CategoryModel>> GetSubCategoriesAsync()
	{
		if (_subCategories == null)
		{
			_subCategories = await Repo.Categories.ListAvailableAsync();
			_subCategories_lastUpdate = DateTime.Now;
		}
		if (_subCategories_lastUpdate.AddSeconds(600) < DateTime.Now)
		{
			await RetrieveSubCategoriesAsync();
		}
		return _subCategories;
	}

	private static async Task RetrieveCategoriesAsync()
	{
		_categories = await Repo.Categories.ListAsync();
		_categories_lastUpdate = DateTime.Now;
	}
	private static List<string>? _categories;
	private static DateTime _categories_lastUpdate;

	private static async Task RetrieveSubCategoriesAsync()
	{
		_subCategories = await Repo.Categories.ListAvailableAsync();
		_subCategories_lastUpdate = DateTime.Now;
	}
	private static List<DocumentModel.CategoryModel>? _subCategories;
	private static DateTime _subCategories_lastUpdate;
}