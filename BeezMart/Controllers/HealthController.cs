using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BeezMart.ViewModels.Home;
using System.Threading.Tasks;
using System.Collections.Generic;
using BeezMart.Entities.CRM;
using BeezMart.Entities.Billing;
using BeezMart.Entities.TimeSheet;
using BeezMart;

namespace BeezMart.Controllers;

[AllowAnonymous]
public class HealthController : Controller
{
    public IActionResult Index()
    {
        return View();
    }

    public string Check()
    {
        return "OK";
    }

    public IActionResult Test()
    {
        //  More comprehensive health-check should be performed, like DB and Azure Storage connectivity
        return View();
    }
}
