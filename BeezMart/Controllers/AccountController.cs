﻿using BeezMart.Services;
using BeezMart.Utils;
using BeezMart.ViewModels.Account;
using BeezMart.Security;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace BeezMart.Controllers;

[Authorize]
public class AccountController : Controller
{
    public async Task<IActionResult> Index()
    {
        Index_ViewModel viewModel = new Index_ViewModel { };
        viewModel.UserProfile = await this.GetCurrentUserAsync();
        if (viewModel.UserProfile == null)
        {
            AppMessage msg = new AppMessage { };
            msg.AppendMessage("The user account does not exist.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        viewModel.UserRoles = new List<string>();
        IList<Claim> claims = await this._usersManager.GetClaimsAsync(viewModel.UserProfile);
        foreach (Claim claim in claims)
        {
            if (claim.Type == ClaimTypes.Role) viewModel.UserRoles.Add(claim.Value.ToString());
        }
        viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> Index_ChangePassword(string OldPassword, string NewPassword, string PasswordConfirm, bool SendReminder)
    {
        AppMessage msg = new AppMessage { };
        #region validate the input
        if (string.IsNullOrEmpty(OldPassword))
        {
            msg.AppendMessage("You must provide the current password of the account.");
        }
        if (string.IsNullOrEmpty(NewPassword))
        {
            msg.AppendMessage("You must provide a new password for the account.");
        }
        else
        {
            if (NewPassword != PasswordConfirm)
            {
                msg.AppendMessage("Please check your password input. The two passwords don't match.");
            }
        }
        if (msg.Count > 0)
        {
            ViewBag.AppMessage = msg.ToHtmlError();
            return PartialView("_Message");
        }
        #endregion
        ApplicationUser? currentUser = await this.GetCurrentUserAsync();
        if (currentUser == null)
        {
            msg.AppendMessage("Could not find a user profile for current user.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return PartialView("_Message");
        }
        IdentityResult identityResult = await this._usersManager.ChangePasswordAsync(currentUser, OldPassword, NewPassword);
        if (identityResult.Succeeded)
        {
            msg.AppendMessage("Account password was successfully changed.");
            bool SendingEmailSucceeded = true;
            #region send e-mail remnder if required
            if (SendReminder)
            {
                if (string.IsNullOrWhiteSpace(currentUser.Email)) throw new NullReferenceException("User profile for current user has no e-mail address attached.");
                string emailMessageTemplate = "Account/Index/ChangePassword.html";
                    EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, emailMessageTemplate);
                if (emailLayout.Subject == null)
                    throw new ArgumentNullException($"Can't send the e-mail notification because the message template seems to be missing the subject line: {emailMessageTemplate}");
                if (emailLayout.Body == null)
                    throw new ArgumentNullException($"Can't send the e-mail notification because the message template seems to be missing the body content: {emailMessageTemplate}");
                if (EmailAddressValidator.IsValidEmailAddress(currentUser.Email))
                {
                    Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?> { };
                    personalizationStrings.Add("{# Application Full URL #}", EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext));
                    personalizationStrings.Add("{# User Name #}", currentUser.UserName);
                    personalizationStrings.Add("{# Password #}", NewPassword);
                    personalizationStrings.Add("{# First Name #}", currentUser.FirstName);
                    personalizationStrings.Add("{# Last Name #}", currentUser.LastName);
                    personalizationStrings.Add("{# eMail Address #}", currentUser.Email);
                    personalizationStrings.Add("{# Parameterized Message To Receivers #}", "WARNING: Please use the new password for login from now on; the old password is no longer accepted.");
                    emailLayout.Personalize(personalizationStrings);
                    List<string>? potentialErrors = await this._emailSender.SendEmailAsync(currentUser.Email, emailLayout.Subject, emailLayout.Body);
                    if (potentialErrors != null && potentialErrors.Count > 0)
                    {
                        msg.AppendMessage("Errors occured while the application tried to send you an automated e-mail notification with the new login password:");
                        msg.AppendRaw("<ul>");
                        foreach (string error in potentialErrors)
                        {
                            msg.AppendRaw($"<li>{error}</li>");
                        }
                        msg.AppendRaw("</ul>");
                        SendingEmailSucceeded = false;
                    }
                }
            }
            #endregion
            ViewBag.AppMessage = SendingEmailSucceeded ? msg.ToHtmlWarning() : msg.ToHtmlSuccess();
            return PartialView("_Message");
        }
        else
        {
            msg.AppendMessage("Password change failed. Please consult the error messages below:");
            foreach (IdentityError error in identityResult.Errors)
            {
                msg.AppendMessage(error.Description);
            }
            ViewBag.AppMessage = msg.ToHtmlError();
            return PartialView("_Message");
        }
    }

    [AllowAnonymous]
    public ActionResult Login(string? ReturnURL)
    {
        Login_ViewModel viewModel = new Login_ViewModel();
        ViewBag.ReturnUrl = string.IsNullOrWhiteSpace(ReturnURL) ? "/" : ReturnURL;
        return View(viewModel);
    }
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login(Login_ViewModel viewModel, string returnUrl)
    {
        if (!ModelState.IsValid)
        {
            return View(viewModel);
        }
        if (string.IsNullOrWhiteSpace(viewModel.Username))
        {
            ModelState.AddModelError(string.Empty, "Invalid login attempt: no user name.");
            return View(viewModel);
        }
        if (string.IsNullOrWhiteSpace(viewModel.Password))
        {
            ModelState.AddModelError(string.Empty, "Invalid login attempt: please provide a password.");
            return View(viewModel);
        }
        ApplicationUser? userProfile = await this._usersManager.FindByNameAsync(viewModel.Username);
        if (userProfile == null)
        {
            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            return View(viewModel);
        }
        if (userProfile.IsBlocked)
        {
            ModelState.AddModelError(string.Empty, "The account is currently blocked.");
            return View(viewModel);
        }
        Microsoft.AspNetCore.Identity.SignInResult signInResult = await this._signInManager.PasswordSignInAsync(viewModel.Username, viewModel.Password, viewModel.RememberMe, lockoutOnFailure: true);
        if (signInResult.Succeeded)
        {
            return RedirectToLocal(returnUrl);
        }
        if (signInResult.RequiresTwoFactor)
        {
            return RedirectToAction(nameof(this.SendCode), new { ReturnUrl = returnUrl, RememberMe = viewModel.RememberMe });
        }
        if (signInResult.IsLockedOut)
        {
            return View(nameof(this.LockedOut));
        }
        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Logout()
    {
        await this._signInManager.SignOutAsync();
        return RedirectToAction(nameof(HomeController.Index), "Home");
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<ActionResult> SendCode(string? returnUrl = null, bool rememberMe = false)
    {
        ApplicationUser? currentUser = await _signInManager.GetTwoFactorAuthenticationUserAsync();
        if (currentUser == null)
        {
            AppMessage msg = new AppMessage { };
            msg.AppendMessage("The current user could not be determined on two-factor authentication step.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Error");
        }
        IList<string> userFactors = await this._usersManager.GetValidTwoFactorProvidersAsync(currentUser);
        return View(new SendCode_ViewModel { Providers = userFactors, ReturnUrl = returnUrl, RememberMe = rememberMe });
    }
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> SendCode(SendCode_ViewModel viewModel)
    {
        if (!ModelState.IsValid)
        {
            return View();
        }
        ApplicationUser? currentUser = await _signInManager.GetTwoFactorAuthenticationUserAsync();
        AppMessage msg = new AppMessage { };
        if (currentUser == null)
        {
            msg.AppendMessage("The current user could not be determined on two-factor authentication step.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Error");
        }
        if (string.IsNullOrWhiteSpace(viewModel.SelectedProvider)) throw new NullReferenceException("ViewModel.SelectedProvider is NULL or white-space!");
        string code = await this._usersManager.GenerateTwoFactorTokenAsync(currentUser, viewModel.SelectedProvider);
        if (string.IsNullOrWhiteSpace(code))
        {
            msg.AppendMessage("The two-factor authentication code was not properly generated.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Error");
        }
        string message = "Your security code is: " + code;
        if (viewModel.SelectedProvider == "Email")
        {
            throw new NotImplementedException("Sending two-factor verification code via e-mail is not yet implemented");
            //await _emailSender.SendEmailAsync(await this._usersManager.GetEmailAsync(currentUser), "Security Code", message);
        }
        else if (viewModel.SelectedProvider == "Phone")
        {
            throw new NotImplementedException("Sending two-factor verification code via SMS is not yet implemented");
            //await _smsSender.SendSmsAsync(await this._usersManager.GetPhoneNumberAsync(currentUser), message);
        }

        return RedirectToAction(nameof(this.VerifyCode), new { Provider = viewModel.SelectedProvider, ReturnUrl = viewModel.ReturnUrl, RememberMe = viewModel.RememberMe });
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> VerifyCode(string provider, bool rememberMe, string? returnUrl = null)
    {
        // Require that the user has already logged in via username/password or external login
        ApplicationUser? user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
        if (user == null)
        {
            AppMessage msg = new AppMessage { };
            msg.AppendMessage("The current user could not be determined on two-factor authentication step.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Error");
        }
        return View(new VerifyCode_ViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> VerifyCode(VerifyCode_ViewModel viewModel)
    {
        if (string.IsNullOrWhiteSpace(viewModel.Provider))
        {
            ModelState.AddModelError(string.Empty, "Two-factor authentication provider is not provided.");
        }
        if (string.IsNullOrWhiteSpace(viewModel.Code))
        {
            ModelState.AddModelError(string.Empty, "Two-factor authentication code is not provided.");
        }
        if (!ModelState.IsValid)
        {
            return View(viewModel);
        }
        // The following code protects for brute force attacks against the two factor codes.
        // If a user enters incorrect codes for a specified amount of time then the user account will be locked out for a specified amount of time.
        if (string.IsNullOrWhiteSpace(viewModel.Provider)) throw new NullReferenceException("ViewModel.Provider is NULL or white-space!");
        if (string.IsNullOrWhiteSpace(viewModel.Code)) throw new NullReferenceException("ViewModel.Code is NULL or white-space!");
        Microsoft.AspNetCore.Identity.SignInResult signInResult = await _signInManager.TwoFactorSignInAsync(viewModel.Provider, viewModel.Code, viewModel.RememberMe, viewModel.RememberBrowser);
        if (signInResult.Succeeded)
        {
            viewModel.ReturnUrl ??= "/";
            return RedirectToLocal(viewModel.ReturnUrl);
        }
        if (signInResult.IsLockedOut)
        {
            //_logger.LogWarning(7, "User account locked out.");
            return View("Lockout");
        }
        else
        {
            ModelState.AddModelError(string.Empty, "Invalid code.");
            return View(viewModel);
        }
    }

    [AllowAnonymous]
    public ViewResult LockedOut()
    {
        return View();
    }

    [HttpGet]
    [AllowAnonymous]
    public ActionResult ForgotPassword()
    {
        return View();
    }
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ForgotPassword(ForgotPassword_ViewModel viewModel)
    {
        if (string.IsNullOrWhiteSpace(viewModel.EmailAddress))
        {
            AppMessage msg = new AppMessage { };
            msg.AppendMessage("E-mail Address field is null or whitespace!");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("_Message");
        }
        ApplicationUser? currentUser = await this._usersManager.FindByNameAsync(viewModel.EmailAddress);
        if (currentUser == null || !(await this._usersManager.IsEmailConfirmedAsync(currentUser)))
        {
            currentUser = await this._usersManager.FindByEmailAsync(viewModel.EmailAddress);
            if (currentUser == null || !(await this._usersManager.IsEmailConfirmedAsync(currentUser)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return View(nameof(this.ForgotPasswordConfirmation));
            }
        }
        if (string.IsNullOrWhiteSpace(currentUser.Email)) throw new NullReferenceException("User profile for current user has no e-mail address attached.");
        string emailMessageTemplate = "Account/ForgotPassword/PasswordResetToken.html";
        EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, emailMessageTemplate);
        if (emailLayout.Subject == null)
            throw new ArgumentNullException($"Can't send the e-mail notification because the message template seems to be missing the subject line: {emailMessageTemplate}");
        if (emailLayout.Body == null)
            throw new ArgumentNullException($"Can't send the e-mail notification because the message template seems to be missing the body content: {emailMessageTemplate}");
        string PasswordResetToken = await this._usersManager.GeneratePasswordResetTokenAsync(currentUser);
        string? PasswordResetTokenSplit = BeezMart.Utils.Security.Helper.ChunkifyString(PasswordResetToken, 40, "<wbr />");
        string appRootURL = EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext);
        string? passwordResetPage = Url.Action(nameof(this.ResetPassword), "Account", null, Request.Scheme);
        string? passwordResetURL = Url.Action(nameof(this.ResetPassword), "Account", new { User = currentUser.Id, ResetToken = PasswordResetToken }, protocol: Request.Scheme);
        Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?> { };
        personalizationStrings.Add("{# Application Full URL #}", appRootURL);
        personalizationStrings.Add("{# User Name #}", currentUser.UserName);
        personalizationStrings.Add("{# Password Reset Token #}", PasswordResetTokenSplit);
        personalizationStrings.Add("{# First Name #}", currentUser.FirstName);
        personalizationStrings.Add("{# Last Name #}", currentUser.LastName);
        personalizationStrings.Add("{# eMail Address #}", currentUser.Email);
        personalizationStrings.Add("{# Password Reset URL #}", passwordResetURL);
        personalizationStrings.Add("{# Password Reset Page #}", passwordResetPage);
        personalizationStrings.Add("{# Parameterized Message To Receivers #}", "WARNING: Please disregard this message if you are not aware of a password reset request!");
        emailLayout.Personalize(personalizationStrings);
        List<string>? potentialErrors = await this._emailSender.SendEmailAsync(currentUser.Email, emailLayout.Subject, emailLayout.Body);
        if (potentialErrors != null && potentialErrors.Count > 0)
        {
            AppMessage msg = new AppMessage { };
            msg.AppendMessage("The application failed to send you an automated e-mail with the password reset token:");
            msg.AppendRaw("<ul>");
            foreach (string error in potentialErrors)
            {
                msg.AppendRaw($"<li>{error}</li>");
            }
            msg.AppendRaw("</ul>");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("_Message");
        }
        else
        {
            return RedirectToAction(nameof(this.ForgotPasswordConfirmation));
        }
    }
    [HttpGet]
    [AllowAnonymous]
    public ViewResult ForgotPasswordConfirmation()
    {
        return View(nameof(this.ForgotPasswordConfirmation));
    }

    [HttpGet]
    [AllowAnonymous]
    public ViewResult ResetPassword(string? ResetToken)
    {
        ResetPassword_ViewModel viewModel = new ResetPassword_ViewModel { };
        viewModel.PasswordResetToken = ResetToken;
        return View(viewModel);
    }
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ResetPassword(ResetPassword_ViewModel viewModel)
    {
        if (string.IsNullOrEmpty(viewModel.Username))
        {
            ModelState.AddModelError(string.Empty, "User-name is null or white-space.");
        }
        if (string.IsNullOrEmpty(viewModel.PasswordResetToken))
        {
            ModelState.AddModelError(string.Empty, "The Password Reset Token is null or white-space.");
        }
        if (string.IsNullOrEmpty(viewModel.NewPassword) || viewModel.NewPassword != viewModel.PasswordConfirm)
        {
            ModelState.AddModelError(string.Empty, "The new password is missing or the two passwords don't match.");
        }
        if (!ModelState.IsValid)
        {
            return View(viewModel);
        }
        if (string.IsNullOrWhiteSpace(viewModel.Username)) throw new NullReferenceException("ViewModel.Username is NULL or white-space!");
        ApplicationUser? currentUser = await this._usersManager.FindByNameAsync(viewModel.Username);
        if (currentUser == null)
        {
            // Don't reveal that the user does not exist
            return RedirectToAction(nameof(this.ResetPasswordConfirmation));
        }
        if (string.IsNullOrWhiteSpace(viewModel.PasswordResetToken)) throw new NullReferenceException("ViewModel.PasswordResetToken is NULL or white-space!");
        string resetToken = viewModel.PasswordResetToken.Replace(" ", string.Empty);
        resetToken = resetToken.Replace("\r", string.Empty);
        resetToken = resetToken.Replace("\n", string.Empty);
        if (string.IsNullOrWhiteSpace(viewModel.NewPassword)) throw new NullReferenceException("ViewModel.NewPassword is NULL or white-space!");
        IdentityResult result = await this._usersManager.ResetPasswordAsync(currentUser, resetToken, viewModel.NewPassword);
        if (result.Succeeded)
        {
            #region send e-mail reminder if required
            if (viewModel.SendReminder)
            {
                string emailMessageTemplate = "Account/ResetPassword/NewPassword.html";
                EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, emailMessageTemplate);
                if (emailLayout.Subject == null)
                    throw new ArgumentNullException($"Can't send the e-mail notification because the message template seems to be missing the subject line: {emailMessageTemplate}");
                if (emailLayout.Body == null)
                    throw new ArgumentNullException($"Can't send the e-mail notification because the message template seems to be missing the body content: {emailMessageTemplate}");
                if (EmailAddressValidator.IsValidEmailAddress(currentUser.Email))
                {
                    if (string.IsNullOrWhiteSpace(currentUser.Email)) throw new NullReferenceException("User profile for current user has no e-mail address attached.");
                    Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?> { };
                    personalizationStrings.Add("{# Application Full URL #}", EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext));
                    personalizationStrings.Add("{# User Name #}", currentUser.UserName);
                    personalizationStrings.Add("{# Password #}", viewModel.NewPassword);
                    personalizationStrings.Add("{# First Name #}", currentUser.FirstName);
                    personalizationStrings.Add("{# Last Name #}", currentUser.LastName);
                    personalizationStrings.Add("{# eMail Address #}", currentUser.Email);
                    personalizationStrings.Add("{# Parameterized Message To Receivers #}", "WARNING: Use the new password for login from now on.");
                    emailLayout.Personalize(personalizationStrings);
                    List<string>? potentialErrors = await this._emailSender.SendEmailAsync(currentUser.Email, emailLayout.Subject, emailLayout.Body);
                    if (potentialErrors != null && potentialErrors.Count > 0)
                    {
                        AppMessage msg = new AppMessage { };
                        msg.AppendMessage("The application failed to send you an automated e-mail with the new login password:");
                        msg.AppendRaw("<ul>");
                        foreach (string error in potentialErrors)
                        {
                            msg.AppendRaw($"<li>{error}</li>");
                        }
                        msg.AppendRaw("</ul>");
                        ViewBag.AppMessage = msg.ToHtmlWarning();
                    }
                }
            }
            #endregion
            return RedirectToAction(nameof(this.ResetPasswordConfirmation));
        }
        foreach (IdentityError error in result.Errors)
        {
            ModelState.AddModelError(string.Empty, error.Description);
        }
        return View(viewModel);
    }
    [AllowAnonymous]
    public ActionResult ResetPasswordConfirmation()
    {
        return View();
    }
        
    [AllowAnonymous]
    public ViewResult UnauthorizedAccess(string? ReturnURL)
    {
        UnauthorizedAccess_ViewModel viewModel = new UnauthorizedAccess_ViewModel();
        viewModel.ReturnUrl = ReturnURL ?? "/";
        return View(nameof(this.UnauthorizedAccess), viewModel);
    }   
    [AllowAnonymous]
    public ViewResult Unauthorized(string? ReturnURL)
    {
        UnauthorizedAccess_ViewModel viewModel = new UnauthorizedAccess_ViewModel();
        viewModel.ReturnUrl = ReturnURL ?? "/";
        return View(nameof(this.UnauthorizedAccess), viewModel);
    }
    [AllowAnonymous]
    public ViewResult AccessDenied(string? ReturnURL)
    {
        AccessDenied_ViewModel viewModel = new AccessDenied_ViewModel();
        viewModel.ReturnUrl = ReturnURL ?? "/";
        return View(nameof(this.AccessDenied), viewModel);
    }

    [AllowAnonymous]
    public ViewResult Forbidden(string? ReturnURL)
    {
        Forbidden_ViewModel viewModel = new Forbidden_ViewModel();
        viewModel.ReturnUrl = ReturnURL ?? "/";
        return View(nameof(this.Forbidden), viewModel);
    }

    #region constructors, properties or fields, and helpers
    public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IAntiforgery antiforgery, IEmailSender emailSender)
    {
        this._usersManager = userManager;
        this._antiforgery = antiforgery;
        this._signInManager = signInManager;
        this._emailSender = emailSender;
    }
    private readonly IAntiforgery _antiforgery;
    private readonly UserManager<ApplicationUser> _usersManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IEmailSender _emailSender;
    private IActionResult RedirectToLocal(string returnUrl)
    {
        if (Url.IsLocalUrl(returnUrl))
        {
            return Redirect(returnUrl);
        }
        return RedirectToAction(nameof(HomeController.Index), "Home", routeValues: new { Area = string.Empty });
    }
    private async Task<ApplicationUser?> GetCurrentUserAsync()
    {
        ApplicationUser? appUser = null;
        string? identityName = this.User.Identity?.Name;
        if (!string.IsNullOrWhiteSpace(identityName))
            appUser = await this._usersManager.FindByNameAsync(identityName);
        return appUser;
    }
    #endregion
}
