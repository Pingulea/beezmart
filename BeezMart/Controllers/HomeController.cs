﻿using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BeezMart.ViewModels.Home;
using System.Threading.Tasks;
using System.Collections.Generic;
using BeezMart.Entities.CRM;
using BeezMart.Entities.Billing;
using BeezMart.Entities.TimeSheet;
using BeezMart;

namespace BeezMart.Controllers
{
	[AllowAnonymous]
	public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (MyApp.MissingHostingConfigurationSettings != null && MyApp.MissingHostingConfigurationSettings.Count > 0)
            {
                return RedirectToAction(controllerName: "Setup", actionName: "MissingConfigSettings", routeValues: new { area = "Admin" });
            }
            else 
            {
                if (MyApp.DatabaseIsSetUp) return View();
                else return RedirectToAction(controllerName: "Setup", actionName: "Index", routeValues: new { area = "Admin" });
            }
        }

		[Authorize]
		public async Task<ViewResult> Search(string q)
		{
            Search_ViewModel viewModel = new Search_ViewModel { };
            #region CRM Organizations or Persons
            BeezMart.Entities.CRM.OrganizationModel.LookupFilter lookupFilterCrmOrgs = new OrganizationModel.LookupFilter {
                NamePattern = q,
                NameExactMatch = false,
                Contact = q,
                SearchOperator = SearchOperator.Or
                };
            viewModel.CrmOrganizations = await BeezMart.DAL.CRM.Organizations.LookupAsync(lookupFilterCrmOrgs);
            BeezMart.Entities.CRM.PersonModel.LookupFilter lookupFilterCrmPers = new PersonModel.LookupFilter {
                NamePattern = q,
                NameExactMatch = false,
                Contact = q,
                SearchOperator = SearchOperator.Or
                };
            viewModel.CrmPersons = await BeezMart.DAL.CRM.Persons.LookupAsync(lookupFilterCrmPers);
            #endregion
            #region Billing Invoices
            List<string> accountingIDs = new List<string> { q };
            BeezMart.Entities.Billing.InvoiceModel.LookupFilter lookupFilterBillingInvoices = new InvoiceModel.LookupFilter {
                CustomerNamePattern = q,
                CustomerNameExactMatch = false,
                AccountingIDs = accountingIDs,
                SearchOperator = SearchOperator.Or
                };
            viewModel.BillingInvoices = await BeezMart.DAL.Billing.Invoices.LookupAsync(lookupFilterBillingInvoices);
            #endregion
            #region TimeSheet Projects
            BeezMart.Entities.TimeSheet.ProjectModel.LookupFilter lookupFilterProjects = new ProjectModel.LookupFilter {
                CustomerNamePattern = q,
                TitlePattern = q,
                TitleExactMatch = false,
                ProjectManager = q,
                ContactDetails = q,
                SearchOperator = SearchOperator.Or
                };
            viewModel.TimeSheetProjects = await BeezMart.DAL.TimeSheet.Projects.LookupAsync(lookupFilterProjects);
            #endregion
            return View(viewModel);
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
		}

		public ViewResult Message(AppMessage message)
		{
			if (message == null)
			{
				message = new AppMessage { };
				message.Title = "Unusual behavior";
				message.Add("Some exception has occured during the current operation, but there are no details available at the moment...");
				message.Type = AppMessageType.Warning;
			}
			return View(message);
		}

		public ViewResult Test()
		{
			return View();
		}
	}
}
