﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeezMart.Utils;

public enum AppMessageType : byte
{
    Info = 1,
    Success = 2,
    Warning = 3,
    Danger = 4,
    Error = 5
}

public class AppMessage : List<string>
{
    public AppMessageType Type;
    public bool IsEmpty { get { return this.Count == 0 ? true : false; } }
    public int Length { get { return this.Count; } }
    public string? Title;
    public List<string>? Buttons;
    public bool IsDismisable;
    public string? CssClassButtonsWrapper = "submit";
    public string? CssClass
    {
        get
        {
            if (string.IsNullOrEmpty(this._cssClass))
            {
                switch (this.Type)
                {
                    case AppMessageType.Info: return "alert-info";
                    case AppMessageType.Success: return "alert-success";
                    case AppMessageType.Warning: return "alert-warning";
                    case AppMessageType.Danger: return "alert-danger";
                    case AppMessageType.Error: return "alert-danger";
                    default: return this._cssClass;
                }
            }
            else return this._cssClass;
        }
        set
        {
            this._cssClass = value;
        }
    }

    public AppMessage()
    {
        this.Type = AppMessageType.Info;
        this.IsDismisable = true;
        this.CssClassButtonsWrapper = "submit";
    }

    public AppMessage(AppMessageType MessageType, bool IsDismisable = true, string ButtonWrapperCssClass = "submit")
    {
        this.Type = MessageType;
        this.IsDismisable = IsDismisable;
        this.CssClassButtonsWrapper = ButtonWrapperCssClass;
    }

    public int AppendMessage(string message)
    {
        this.Add($"<p>{message}</p>");
        return this.Count;
    }
    public int AppendRaw(object rawObject)
    {
        string? msg = rawObject.ToString();
        if (!string.IsNullOrWhiteSpace(msg))
            this.Add(msg);
        return this.Count;
    }
    /// <summary>
    ///		<para>Appends a link button <a class='[buttonClass]'> inside a <div class='[CssClassButtonsWrapper]'> at the bottom of the message box</para>
    /// </summary>
    ///		<param name="linkButtonText">Gets replaced with 'GO' if it's null or empty</param>
    ///		<param name="linkButtonUrl">Gets replaced with '/' if it's null or empty</param>
    ///		<param name="buttonClass"></param>
    /// <returns></returns>
    public int AppendLinkButton(string? linkButtonText, string? linkButtonUrl, string buttonClass = "btn btn-sm btn-primary")
    {
        if (string.IsNullOrWhiteSpace(linkButtonText)) linkButtonText = "GO";
        if (string.IsNullOrWhiteSpace(linkButtonUrl)) linkButtonUrl = "/";
        if (this.Buttons == null) this.Buttons = new List<string>();
        this.Buttons.Add($"<a href='{linkButtonUrl}' class='{buttonClass}'>{linkButtonText}</a>");
        return this.Buttons.Count;
    }
    /// <summary>
    ///		<para>Appends a simple link <a class='[buttonClass]'> inside a <div class='[CssClassButtonsWrapper]'> to the list of messages</para>
    /// </summary>
    ///		<param name="linkButtonText">Gets replaced with 'GO' if it's null or empty</param>
    ///		<param name="linkButtonUrl">Gets replaced with '/' if it's null or empty</param>
    ///		<param name="buttonClass"></param>
    /// <returns></returns>
    public int AppendLink(string? linkText, string? linkUrl, string linkClass = "btn btn-sm btn-light")
    {
        if (string.IsNullOrWhiteSpace(linkText)) linkText = "GO";
        if (string.IsNullOrWhiteSpace(linkUrl)) linkUrl = "/";
        if (this.Buttons == null) this.Buttons = new List<string>();
        this.Buttons.Add($"<a href='{linkUrl}' class='{linkClass}'>{linkText}</a>");
        return this.Buttons.Count;
    }

    public string? GetString()
    {
        if (this.Count == 0) return null;
        StringBuilder stb = new StringBuilder();
        foreach (string msg in this) stb.Append(msg);
        return stb.ToString();
    }
    public string? ToHtml()
    {
        if (string.IsNullOrEmpty(this.Title))
        {
            switch (this.Type)
            {
                case AppMessageType.Success: this.Title = AppMessage.strSuccess; break;
                case AppMessageType.Error: this.Title = AppMessage.strError; break;
                case AppMessageType.Warning: this.Title = AppMessage.strWarning; break;
                case AppMessageType.Danger: this.Title = AppMessage.strDanger; break;
                default: this.Title = AppMessage.strInfo; break;
            }
        }
        return this.ToHtml(this.Title);
    }
    public string? ToHtml(string? title)
    {
        if (this.Count == 0) return null;
        StringBuilder stb = new StringBuilder();
        stb.Append($"<div class='alert {this.CssClass}'>");
        if (this.IsDismisable) stb.Append("<button type='button' class='btn-close float-end' data-bs-dismiss='alert' aria-label='Close'></button>");
        if (!string.IsNullOrEmpty(title)) stb.Append($"<h4>{title}</h4>");
        foreach (string msg in this) stb.Append(msg);
        if (this.Buttons != null && this.Buttons.Count > 0)
        {
            stb.Append($"<div class='{this.CssClassButtonsWrapper}'>");
            foreach (string msg in this.Buttons)
            {
                stb.Append(msg);
            }
            stb.Append("</div>");
        }
        stb.Append("</div>");
        return stb.ToString();
    }
    public string? ToHtmlSuccess()
    {
        this.Type = AppMessageType.Success;
        return this.ToHtml(string.IsNullOrEmpty(this.Title) ? strSuccess : this.Title);
    }
    public string? ToHtmlSuccess(string title)
    {
        this.Type = AppMessageType.Success;
        return this.ToHtml(title);
    }
    public string? ToHtmlWarning()
    {
        this.Type = AppMessageType.Warning;
        return this.ToHtml(string.IsNullOrEmpty(this.Title) ? strWarning : this.Title);
    }
    public string? ToHtmlWarning(string title)
    {
        this.Type = AppMessageType.Warning;
        return this.ToHtml(title);
    }
    public string? ToHtmlDanger()
    {
        this.Type = AppMessageType.Danger;
        return this.ToHtml(string.IsNullOrEmpty(this.Title) ? strDanger : this.Title);
    }
    public string? ToHtmlDanger(string title)
    {
        this.Type = AppMessageType.Danger;
        return this.ToHtml(title);
    }
    public string? ToHtmlError()
    {
        this.Type = AppMessageType.Error;
        return this.ToHtml(string.IsNullOrEmpty(this.Title) ? strError : this.Title);
    }
    public string? ToHtmlError(string title)
    {
        this.Type = AppMessageType.Error;
        return this.ToHtml(title);
    }

    public string? ToHtmlError(ModelStateDictionary ModelStateContainer, string? title = null)
    {
        this.Type = AppMessageType.Error;
        foreach (string key in ModelStateContainer.Keys)
        {
            if (ModelStateContainer == null) continue;
            ModelStateEntry? modelStateEntry = ModelStateContainer[key];
            if (modelStateEntry == null) continue;
            foreach (ModelError modelError in modelStateEntry.Errors)
            {
                this.AppendMessage(modelError.ErrorMessage);
            }
        }
        return this.ToHtml(title);
    }


    private string? _cssClass;
    private static string strSuccess = "Success";
    private static string strError = "Error";
    private static string strWarning = "Warning";
    private static string strDanger = "Danger";
    private static string strInfo = "Info";
}