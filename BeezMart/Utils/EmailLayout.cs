﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BeezMart.Utils;

/// <summary>
///		<para>Represents an e-mail HTML template that can be personalized via replacing some fields or placeholders with actual custom text.</para>
///		<para>This class assumes that template HTML files will be placed under ~/Content/Email_Templates/</para>
/// </summary>
public class EmailLayout
{
	public static string MasterTemplateFile = "Master_Layout.html";
	public static string TemplatesDirectory = "Content/Email_Templates";

	public string? Subject
	{
		get
		{
			if (this._subject == null || this._subject.Length == 0) return null;
			return this._subject.ToString();
		}
		set { this._subject = new StringBuilder(value); }
	}
	public string? Body
	{
		get
		{
			if (this._body == null || this._body.Length == 0) return null;
			return this._body.ToString();
		}
		set { this._body = new StringBuilder(value); }
	}

	public EmailLayout() { }

	/// <summary>
	///		<para>Creates a separate copy of an instance.</para>
	/// </summary>
	///		<param name="emailLayout"></param>
	public EmailLayout(EmailLayout emailLayout)
	{
		this._subject = new StringBuilder(emailLayout.Subject);
		this._body = new StringBuilder(emailLayout.Body);
	}

	/// <summary>
	///		<para>Builds an e-mail layout starting from a template HTML file. Subject line should be placed in the HEAD/TITLE tag.</para>
	/// </summary>
	///		<param name="TemplateFileLocation">The relative path of a file containing a valid XHTML document. The Subject line will be taken from TITLE tag.</param>
	public EmailLayout(string TemplateFileLocation)
	{
		if (string.IsNullOrEmpty(TemplateFileLocation)) throw new Exception("'TemplateFileLocation' is null or empty. 'Nurbiosis.Utils.EmailLayout(...)' needs an e-mail template file to send the e-mail.");
		string templateFilePath = Path.Combine(EmailLayout.TemplatesDirectory, TemplateFileLocation);
		FileInfo templateFileInfo = new FileInfo(TemplateFileLocation);
		if (!templateFileInfo.Exists) throw new Exception("File does not exist. The relative path referenced by 'TemplateFileLocation' when calling 'Nurbiosis.Utils.EmailLayout(...)' does not point to a physical file.");
		StreamReader templateFileStreamReader = new StreamReader(new FileStream(templateFileInfo.FullName, FileMode.Open, FileAccess.Read));
		this._body = new StringBuilder(templateFileStreamReader.ReadToEnd());
		templateFileStreamReader.Dispose();
		string body = this._body.ToString();
		int StringStartIndex = body.IndexOf("<title>") + ("<title>").Length;
		int StringFinishIndex = body.IndexOf("</title>");
		if (StringStartIndex > 7 && StringFinishIndex > StringStartIndex)
		{
			this._subject = new StringBuilder(body.Substring(StringStartIndex, StringFinishIndex - StringStartIndex));
		}
	}

	/// <summary>
	///		<para>Builds an e-mail message based on a template file that is injected into a master layout template, taken from an HTML file too.</para>
	/// </summary>
	///		<param name="MasterLayoutFileLocation">The relative path of a file containing a valid XHTML document. Must contain an [#_Content_Placeholder_#] and an [#_Title_Placeholder_#]. Its TITLE tag will be replaced with the TITLE tag from MessageContentFile.</param>
	///		<param name="MessageContentFileLocation">The relative path of a file containing a valid XHTML document. The Subject line will be taken from TITLE tag.</param>
	public EmailLayout(string MasterLayoutFileLocation, string MessageContentFileLocation)
	{
		if (string.IsNullOrEmpty(MasterLayoutFileLocation)) throw new ArgumentException("'MasterLayoutFileLocation' is null or empty. 'Nurbiosis.Utils.EmailLayout(...)' needs an e-mail master layout file to send the e-mail.");
		if (string.IsNullOrEmpty(MessageContentFileLocation)) throw new ArgumentException("'MessageContentFileLocation' is null or empty. 'Nurbiosis.Utils.EmailLayout(...)' needs an e-mail message content template file to send the e-mail.");
		FileInfo masterLayoutFileInfo = new FileInfo(Path.Combine(EmailLayout.TemplatesDirectory, MasterLayoutFileLocation));
		FileInfo messageContentFileInfo = new FileInfo(Path.Combine(EmailLayout.TemplatesDirectory, MessageContentFileLocation));
		if (!masterLayoutFileInfo.Exists) throw new Exception("File does not exist. The path referenced by 'MasterLayoutFileLocation' when calling 'Nurbiosis.Utils.EmailLayout(...)' does not point to a physical file.");
		if (!messageContentFileInfo.Exists) throw new Exception("File does not exist. The path referenced by 'MessageContentFileLocation' when calling 'Nurbiosis.Utils.EmailLayout(...)' does not point to a physical file.");
		StreamReader masterLayoutFileStreamReader = new StreamReader(new FileStream(masterLayoutFileInfo.FullName, FileMode.Open, FileAccess.Read));
		string masterLayoutHtmlCode = masterLayoutFileStreamReader.ReadToEnd();
		masterLayoutFileStreamReader.Dispose();
		StreamReader messageContentFileStreamReader = new StreamReader(new FileStream(messageContentFileInfo.FullName, FileMode.Open, FileAccess.Read));
		string messageContentHtmlCode = messageContentFileStreamReader.ReadToEnd();
		messageContentFileStreamReader.Dispose();

		this._body = new StringBuilder(masterLayoutHtmlCode);
		string messageContentTitleElement;
		string masterLayoutTitleElement;
		string MessageContent;
		int StringStartIndex = messageContentHtmlCode.IndexOf("<title>") + ("<title>").Length;
		int StringFinishIndex = messageContentHtmlCode.IndexOf("</title>");
		if (StringStartIndex > 7 && StringFinishIndex > StringStartIndex)
		{
			this._subject = new StringBuilder(messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex));
			this._body.Replace("[#_Title_Placeholder_#]", this._subject.ToString());

			StringStartIndex = messageContentHtmlCode.IndexOf("<title>");
			StringFinishIndex = messageContentHtmlCode.IndexOf("</title>") + ("</title>").Length;
			messageContentTitleElement = messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
			StringStartIndex = masterLayoutHtmlCode.IndexOf("<title>");
			StringFinishIndex = masterLayoutHtmlCode.IndexOf("</title>") + ("</title>").Length;
			if (StringStartIndex > 0 && StringFinishIndex > StringStartIndex)
			{
				masterLayoutTitleElement = masterLayoutHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
				this._body.Replace(masterLayoutTitleElement, messageContentTitleElement);
			}
		}
		StringStartIndex = messageContentHtmlCode.IndexOf("<body>") + ("<body>").Length;
		StringFinishIndex = messageContentHtmlCode.IndexOf("</body>");
		if (StringStartIndex > 6 && StringFinishIndex > StringStartIndex)
		{
			MessageContent = messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
			this._body.Replace("[#_Content_Placeholder_#]", MessageContent);
		}
	}

	/// <summary>
	///		<para>Builds an e-mail layout starting from a template HTML file. Subject line should be placed in the HEAD/TITLE tag.</para>
	/// </summary>
	///		<param name="TemplateFileLocation">The relative path of a file containing a valid XHTML document. The Subject line will be taken from TITLE tag.</param>
	public static async Task<EmailLayout> BuildAsync(string TemplateFileLocation)
	{
		EmailLayout email = new EmailLayout();
		if (string.IsNullOrEmpty(TemplateFileLocation)) throw new Exception("'TemplateFileLocation' is null or empty. 'Nurbiosis.Utils.EmailLayout.Build(...)' needs an e-mail template file to send the e-mail.");
		string templateFilePath = Path.Combine(EmailLayout.TemplatesDirectory, TemplateFileLocation);
		FileInfo templateFileInfo = new FileInfo(TemplateFileLocation);
		if (!templateFileInfo.Exists) throw new Exception("File does not exist. The relative path referenced by 'TemplateFileLocation' when calling 'Nurbiosis.Utils.EmailLayout.Build(...)' does not point to a physical file.");
		StreamReader templateFileStreamReader = new StreamReader(new FileStream(templateFileInfo.FullName, FileMode.Open, FileAccess.Read));
		email._body = new StringBuilder(await templateFileStreamReader.ReadToEndAsync());
		templateFileStreamReader.Dispose();
		string body = email._body.ToString();
		int StringStartIndex = body.IndexOf("<title>") + ("<title>").Length;
		int StringFinishIndex = body.IndexOf("</title>");
		if (StringStartIndex > 7 && StringFinishIndex > StringStartIndex)
		{
			email._subject = new StringBuilder(body.Substring(StringStartIndex, StringFinishIndex - StringStartIndex));
		}
		return email;
	}

	/// <summary>
	///		<para>Builds an e-mail message based on a template file that is injected into a master layout template, taken from an HTML file too.</para>
	/// </summary>
	///		<param name="MasterLayoutFileLocation">The relative path of a file containing a valid XHTML document. Must contain an [#_Content_Placeholder_#] and an [#_Title_Placeholder_#]. Its TITLE tag will be replaced with the TITLE tag from MessageContentFile.</param>
	///		<param name="MessageContentFileLocation">The relative path of a file containing a valid XHTML document. The Subject line will be taken from TITLE tag.</param>
	public static async Task<EmailLayout> BuildAsync(string MasterLayoutFileLocation, string MessageContentFileLocation)
	{
		EmailLayout email = new EmailLayout();
		if (string.IsNullOrEmpty(MasterLayoutFileLocation)) throw new ArgumentException("'MasterLayoutFileLocation' is null or empty. 'Nurbiosis.Utils.EmailLayout.Build(...)' needs an e-mail master layout file to send the e-mail.");
		if (string.IsNullOrEmpty(MessageContentFileLocation)) throw new ArgumentException("'MessageContentFileLocation' is null or empty. 'Nurbiosis.Utils.EmailLayout.Build(...)' needs an e-mail message content template file to send the e-mail.");
		FileInfo masterLayoutFileInfo = new FileInfo(Path.Combine(EmailLayout.TemplatesDirectory, MasterLayoutFileLocation));
		FileInfo messageContentFileInfo = new FileInfo(Path.Combine(EmailLayout.TemplatesDirectory, MessageContentFileLocation));
		if (!masterLayoutFileInfo.Exists) throw new Exception("File does not exist. The path referenced by 'MasterLayoutFileLocation' when calling 'Nurbiosis.Utils.EmailLayout.Build(...)' does not point to a physical file.");
		if (!messageContentFileInfo.Exists) throw new Exception("File does not exist. The path referenced by 'MessageContentFileLocation' when calling 'Nurbiosis.Utils.EmailLayout.Build(...)' does not point to a physical file.");
		StreamReader masterLayoutFileStreamReader = new StreamReader(new FileStream(masterLayoutFileInfo.FullName, FileMode.Open, FileAccess.Read));
		string masterLayoutHtmlCode = await masterLayoutFileStreamReader.ReadToEndAsync();
		masterLayoutFileStreamReader.Dispose();
		StreamReader messageContentFileStreamReader = new StreamReader(new FileStream(messageContentFileInfo.FullName, FileMode.Open, FileAccess.Read));
		string messageContentHtmlCode = await messageContentFileStreamReader.ReadToEndAsync();
		messageContentFileStreamReader.Dispose();

		email._body = new StringBuilder(masterLayoutHtmlCode);
		string messageContentTitleElement;
		string masterLayoutTitleElement;
		string MessageContent;
		int StringStartIndex = messageContentHtmlCode.IndexOf("<title>") + ("<title>").Length;
		int StringFinishIndex = messageContentHtmlCode.IndexOf("</title>");
		if (StringStartIndex > 7 && StringFinishIndex > StringStartIndex)
		{
			email._subject = new StringBuilder(messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex));
			email._body.Replace("[#_Title_Placeholder_#]", email._subject.ToString());

			StringStartIndex = messageContentHtmlCode.IndexOf("<title>");
			StringFinishIndex = messageContentHtmlCode.IndexOf("</title>") + ("</title>").Length;
			messageContentTitleElement = messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
			StringStartIndex = masterLayoutHtmlCode.IndexOf("<title>");
			StringFinishIndex = masterLayoutHtmlCode.IndexOf("</title>") + ("</title>").Length;
			if (StringStartIndex > 0 && StringFinishIndex > StringStartIndex)
			{
				masterLayoutTitleElement = masterLayoutHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
				email._body.Replace(masterLayoutTitleElement, messageContentTitleElement);
			}
		}
		StringStartIndex = messageContentHtmlCode.IndexOf("<body>") + ("<body>").Length;
		StringFinishIndex = messageContentHtmlCode.IndexOf("</body>");
		if (StringStartIndex > 6 && StringFinishIndex > StringStartIndex)
		{
			MessageContent = messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
			email._body.Replace("[#_Content_Placeholder_#]", MessageContent);
		}
		return email;
	}

	/// <summary>
	///		<para>Applies a master layout to a message template.</para>
	/// </summary>
	///		<param name="message">The message to be wrapped in a layout.</param>
	///		<param name="layout">The master layout template that will be applied.</param>
	/// <returns></returns>
	public static EmailLayout Assemble(EmailLayout message, EmailLayout layout)
	{
		if (message == null || layout == null)
		    throw new ArgumentNullException("EmailLayout 'message' or 'layout' parameters cannot be null.");
		if (string.IsNullOrEmpty(message.Subject))
			throw new ArgumentNullException("EmailLayout message subject cannot be null or empty.");
		if (string.IsNullOrEmpty(message.Body))
			throw new ArgumentNullException("EmailLayout message body cannot be null or empty.");
		EmailLayout result = new EmailLayout() { Subject = message.Subject, Body = layout.Body };
		string messageContentHtmlCode = message.Body;
		int StringStartIndex = messageContentHtmlCode.IndexOf("<body>") + ("<body>").Length;
		int StringFinishIndex = messageContentHtmlCode.IndexOf("</body>");
		if (StringStartIndex > 6 && StringFinishIndex > StringStartIndex)
		{
			string messageContent = messageContentHtmlCode.Substring(StringStartIndex, StringFinishIndex - StringStartIndex);
			if (result._body != null)
			{
				result._body.Replace("[#_Title_Placeholder_#]", message.Subject);
				result._body.Replace("[#_Content_Placeholder_#]", messageContent);
			}
		}
		return result;
	}

	/// <summary>
	///		<para>Changes current instance: Replaces personalization fields with custom strings, resulting in the final HTML e-mail.</para>
	/// </summary>
	///		<param name="FieldReplacements">Key strings found in the e-mail template layout will be replaced with corresponding Value strings.</param>
	public EmailLayout Personalize(Dictionary<string, string?> FieldReplacements)
	{
		if (FieldReplacements == null || FieldReplacements.Count == 0) return this;
		if (this._body != null)
		{
			foreach (string key in FieldReplacements.Keys)
			{
				this._body.Replace(key, FieldReplacements[key]);
			}
		}
		if (this._subject != null)
		{
			foreach (string key in FieldReplacements.Keys)
			{
				this._subject.Replace(key, FieldReplacements[key]);
			}
		}
		return this;
	}

	protected StringBuilder? _body = null;
	protected StringBuilder? _subject = null;
}
