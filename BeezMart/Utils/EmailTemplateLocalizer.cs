﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BeezMart.Utils;

public class EmailTemplateLocalizer
{
	public static string GetApplicationRootURL(HttpContext httpContext)
	{
		HttpRequest req = httpContext.Request;
		string appPath = $"{req.Scheme}://{req.Host}{req.PathBase}";
		return appPath;
	}

	/// <summary>
	///		<para>Gets the localized equivalent of the e-mail template specified.</para>
	///		<para>For instance, if 'C:\Templates\EmailTemplate.html' is given, it could return something like 'C:\Templates\EmailTemplate.FR.html'</para>
	/// </summary>
	///		<param name="TemplateFileLocation">The full file-system path to template file location. Example: 'C:\Templates\EmailTemplate.html'</param>
	/// <returns></returns>
	public string GetLocalizedEmailTemplate(string TemplateFileLocation)
	{
		return this.GetLocalizedEmailTemplate(TemplateFileLocation, CultureInfo.CurrentUICulture.Name);
	}
	/// <summary>
	///		<para>Gets the localized equivalent of the e-mail template specified.</para>
	///		<para>For instance, if 'C:\Templates\EmailTemplate.html' is given, it could return something like 'C:\Templates\EmailTemplate.FR.html'</para>
	/// </summary>
	///		<param name="TemplateFileLocation">The full file-system path to template file location. Example: 'C:\Templates\EmailTemplate.html'</param>
	/// <returns></returns>
	public string GetLocalizedEmailTemplate(string TemplateFileLocation, string LanguageID)
	{
		if (TemplateFileLocation.EndsWith("." + LanguageID + ".htm", StringComparison.CurrentCultureIgnoreCase) || TemplateFileLocation.EndsWith("." + LanguageID + ".html", StringComparison.CurrentCultureIgnoreCase))
		{
			return TemplateFileLocation;
		}
		FileInfo templateFileInfo;
		string retVal;
		retVal = TemplateFileLocation.Replace(".htm", "." + LanguageID + ".htm");
		templateFileInfo = new FileInfo(retVal);
		if (templateFileInfo.Exists) return retVal;
		retVal = TemplateFileLocation.Replace(".html", "." + LanguageID + ".html");
		templateFileInfo = new FileInfo(retVal);
		if (templateFileInfo.Exists) return retVal;
		retVal = TemplateFileLocation.Replace(".htm", ".en.htm");
		templateFileInfo = new FileInfo(retVal);
		if (templateFileInfo.Exists) return retVal;
		retVal = TemplateFileLocation.Replace(".html", ".en.html");
		templateFileInfo = new FileInfo(retVal);
		if (templateFileInfo.Exists) return retVal;
		templateFileInfo = new FileInfo(TemplateFileLocation);
		if (templateFileInfo.Exists) return TemplateFileLocation;
		else throw new Exception("File does not exist. The file-system path referenced by 'TemplateFileLocation' when calling 'Nurbiosis.Utils.EmailHelper.GetLocalizedEmailTemplate(TemplateFileLocation)' does not point to a physical file.");
	}
}