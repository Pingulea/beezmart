﻿using BeezMart;
using BeezMart.Security;
using BeezMart.Services;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

WebApplicationBuilder appBuilder = WebApplication.CreateBuilder(args);
IConfiguration appConfig = appBuilder.Configuration;
appConfig.Bind(MyApp.EnvSettings);
#region Add services to the Dependency Injection container.
appBuilder.Services.AddApplicationInsightsTelemetry();
#if LOCAL_ACCOUNTS
    string? userAccountsDatabase = MyApp.EnvSettings.ConnectionStrings.MyAppDatabase;
    if (!string.IsNullOrWhiteSpace(userAccountsDatabase))
    {
        appBuilder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(userAccountsDatabase));
        appBuilder.Services.AddIdentity<ApplicationUser, ApplicationRole>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
    }
    appBuilder.Services.AddControllersWithViews();
#else
    appBuilder.Services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme)
        .AddMicrosoftIdentityWebApp(appConfig.GetSection("AzureEntraAuthentication"));
    appBuilder.Services.AddAuthorization(options => {
        options.FallbackPolicy = options.DefaultPolicy; // By default, all incoming requests will be authorized according to the default policy
    });
    appBuilder.Services.AddControllersWithViews(options =>
        {
            AuthorizationPolicy policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
            options.Filters.Add(new AuthorizeFilter(policy));
        });
    appBuilder.Services.AddRazorPages()
        .AddMicrosoftIdentityUI();
#endif
appBuilder.Services.AddTransient<IEmailSender, MailKitService>();
#endregion
WebApplication webApp = appBuilder.Build();
MyApp.PhysicalDirSourceRoot = webApp.Environment.ContentRootPath;
MyApp.PhysicalDirWwwRoot = webApp.Environment.WebRootPath;
EmailSeviceConfig.FromEmailAddress = MyApp.EnvSettings.MessagingServices.FromEmailAddress;
EmailSeviceConfig.SmtpMailServer.HostName = MyApp.EnvSettings.MessagingServices.SmtpMailServer.HostName;
int portNumber;
if (!int.TryParse(MyApp.EnvSettings.MessagingServices.SmtpMailServer.PortNumber, out portNumber)) portNumber = 25;
EmailSeviceConfig.SmtpMailServer.PortNumber = portNumber;
EmailSeviceConfig.SmtpMailServer.UserName = MyApp.EnvSettings.MessagingServices.SmtpMailServer.UserName;
EmailSeviceConfig.SmtpMailServer.Password = MyApp.EnvSettings.MessagingServices.SmtpMailServer.Password;
AzureBlobService.BlobStorageConfig = MyApp.EnvSettings.FileArchiving.AzureStorage;
MyApp.CheckIfRequiredHostingConfigurationsSettingsAreInPlace(webApp.Logger, appConfig, webApp.Environment);
if (MyApp.MissingHostingConfigurationSettings?.Count == 0)
{
    MyApp.CheckIfDatabaseIsSetUp(webApp.Logger, webApp.Environment);
}
await MyApp.LoadConfigSettingsAsync();
#region Configure the HTTP request pipeline.
if (webApp.Environment.IsDevelopment())
{
    webApp.UseDeveloperExceptionPage();
}
else
{
    webApp.UseExceptionHandler("/Home/Error");
    webApp.UseHsts(); // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
}
webApp.UseHttpsRedirection();
webApp.UseStaticFiles();
webApp.UseRouting();
#if LOCAL_ACCOUNTS
    webApp.UseAuthentication();
    webApp.UseAuthorization();
#else
    webApp.UseAuthorization();
#endif
webApp.MapControllerRoute(name: "areas", pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
webApp.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
#if !LOCAL_ACCOUNTS
    webApp.MapRazorPages();
#endif
#endregion
webApp.Run();
