﻿namespace BeezMart.Security;

public static class UserRoles
{
    public const string ApplicationAdministrator    = "BeezMart.Application-Administrator";
    public const string UserForCRM                  = "BeezMart.CRM-User";
    public const string UserForFinance              = "BeezMart.Finance-User";
    public const string UserForSales                = "BeezMart.Sales-User";
    public const string UserForDelivery             = "BeezMart.Delivery-User";
    public const string UserForPurchasing           = "BeezMart.Purchasing-User";
    public const string UserForProjectManagement    = "BeezMart.Project-Management-User";
    public const string UserForProjectContribution  = "BeezMart.Project-Contribution-User";
    public const string UserForDocumentManagement   = "BeezMart.Document-Management-User";
}
