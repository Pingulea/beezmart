﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace BeezMart.Security;

public class ApplicationUser : IdentityUser<int>
{

    public bool IsBlocked { get; set; }

    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Organization { get; set; }
    public int? OrganizationId { get; set; }
    public string? JobTitle { get; set; }
    public string? Department { get; set; }
    public string? Phone { get; set; }
    public string? Mobile { get; set; }
    public string? City { get; set; }
    public string? StateProvince { get; set; }
    public string? StreetAddress1 { get; set; }
    public string? StreetAddress2 { get; set; }
    public string? StreetAddress3 { get; set; }
    public string? PostalCode { get; set; }

    public Guid? UserGUID { get; set; }
}
