﻿using BeezMart.DAL;
using System;

namespace BeezMart.Models;

public class DashboardStatsModel
{
    public DashboardStatsModel_CRM CRM
    {
        get
        {
            if (_CRM == null) _CRM = new DashboardStatsModel_CRM();
            if (_CRM.LastLoaded.AddSeconds(SecondsToCacheStats_CRM) < DateTime.Now) _CRM.Load();
            return _CRM;
        }
    }
    public DashboardStatsModel_Billing Billing
    {
        get
        {
            if (_Billing == null) _Billing = new DashboardStatsModel_Billing();
            if (_Billing.LastLoaded.AddSeconds(SecondsToCacheStats_Billing) < DateTime.Now) _Billing.Load();
            return _Billing;
        }
    }

    protected int SecondsToCacheStats_CRM = 600;
    protected int SecondsToCacheStats_Billing = 600;

    protected DashboardStatsModel_CRM _CRM = new DashboardStatsModel_CRM();
    protected DashboardStatsModel_Billing _Billing = new DashboardStatsModel_Billing();

    void DashboardStats()
    {
        string? strSecondsToCacheStats_CRM = MyApp.GetConfigSetting("CRM.Dashboard.Stats.RefreshRate");
        if (!string.IsNullOrEmpty(strSecondsToCacheStats_CRM)) this.SecondsToCacheStats_CRM = int.Parse(strSecondsToCacheStats_CRM);
        string? strSecondsToCacheStats_Billing = MyApp.GetConfigSetting("Billing.Dashboard.Stats.RefreshRate");
        if (!string.IsNullOrEmpty(strSecondsToCacheStats_Billing)) this.SecondsToCacheStats_Billing = int.Parse(strSecondsToCacheStats_Billing);
    }
}

public class DashboardStatsModel_CRM
{
    public int CountOfOrgs { get; set; }
    public int CountOfPers { get; set; }
    public int CountOfEmails { get; set; }

    public DateTime LastLoaded = new DateTime(2000, 1, 1);

    public void Load()
    {
        using (Database db = new Database())
        {
            DatabaseRow? record = db.GetDataRow("CRM.Dashboard_Statistics_1");
            if (record != null)
            {
#pragma warning disable CS8605 // Unboxing a possibly null value.
                if (record["Count_Of_Organizations"] != null) this.CountOfOrgs = (int)record["Count_Of_Organizations"];             // Int
                if (record["Count_Of_Persons"] != null) this.CountOfPers = (int)record["Count_Of_Persons"];                         // Int
                if (record["Count_Of_Email_Addresses"] != null) this.CountOfEmails = (int)record["Count_Of_Email_Addresses"];       // Int
#pragma warning restore CS8605 // Unboxing a possibly null value.
            }
        }
        this.LastLoaded = DateTime.Now;
    }
}

public class DashboardStatsModel_Billing
{
    public int ValidatedInvoicesCount { get; set; }
    public decimal ValidatedInvoicesValue { get; set; }
    public decimal ValidatedInvoicesPayed { get; set; }
    public int PendingInvoicesCount { get; set; }
    public decimal PendingInvoicesValue { get; set; }
    public decimal PendingInvoicesPayed { get; set; }
    public int OverdueInvoicesCount { get; set; }
    public decimal OverdueInvoicesValue { get; set; }
    public decimal OverdueInvoicesPayed { get; set; }
    public int TotalOverdueInvoices { get; set; }
    public int TotalOverdueCustomers { get; set; }

    public DateTime LastLoaded = new DateTime(2000, 1, 1);

    public void Load()
    {
        DatabaseRow? record;
        using (Database db = new Database())
        {
            db.AddParam("@Currency", System.Data.SqlDbType.Char, 3).Value = MyApp.ConfigSettings["Billing.Default-Currency"];
            record = db.GetDataRow("Billing.Dashboard_Statistics_1");
            if (record != null)
            {
#pragma warning disable CS8605 // Unboxing a possibly null value.
                this.ValidatedInvoicesCount = (int)record["Validated_Invoices_Count"];          // Int = 0
                this.ValidatedInvoicesValue = (decimal)record["Validated_Invoices_Value"];      // Decimal(27,8) = 0
                this.ValidatedInvoicesPayed = (decimal)record["Validated_Invoices_Payed"];      // Decimal(27,8) = 0
                this.PendingInvoicesCount = (int)record["Pending_Invoices_Count"];              // Int = 0
                this.PendingInvoicesValue = (decimal)record["Pending_Invoices_Value"];          // Decimal(27,8) = 0
                this.PendingInvoicesPayed = (decimal)record["Pending_Invoices_Payed"];          // Decimal(27,8) = 0
                this.OverdueInvoicesCount = (int)record["Overdue_Invoices_Count"];              // Int = 0
                this.OverdueInvoicesValue = (decimal)record["Overdue_Invoices_Value"];          // Decimal(27,8) = 0
                this.OverdueInvoicesPayed = (decimal)record["Overdue_Invoices_Payed"];          // Decimal(27,8) = 0
#pragma warning restore CS8605 // Unboxing a possibly null value.
            }
            record = db.GetDataRow("Billing.Dashboard_Statistics_2");
            if (record != null)
            {
#pragma warning disable CS8605 // Unboxing a possibly null value.
                if (record["Overdue_Invoices_Count"] != null) this.TotalOverdueInvoices = (int)record["Overdue_Invoices_Count"];
                if (record["Overdue_CRM_Customers_Count"] != null) this.TotalOverdueCustomers = (int)record["Overdue_CRM_Customers_Count"];
#pragma warning restore CS8605 // Unboxing a possibly null value.
            }

        }
        this.LastLoaded = DateTime.Now;
    }
}