﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BeezMart.Services
{
    public interface IDocumentFilesService
    {

        Task<IEnumerable<string>> GetFileNamesAsync(long documentID, DateTime addedOn);

        Task<bool> FileExistsAsync(string fileName, long documentID, DateTime addedOn);

        Task<bool> DeleteFileAsync(string fileName, long documentID, DateTime addedOn);

        Task<bool> RenameFileAsync(string fileName, string newFileName, long documentID, DateTime addedOn);

        Task<bool> SaveFileAsync(Stream fileStream, string fileName, long documentID, DateTime addedOn);

        Task<bool> ReadToStreamAsync(string fileName, Stream targetStream, long documentID, DateTime addedOn);
    }

    public enum FileServiceResult : byte
    {
        Success = 0,
        FileNameAlreadyExists = 10,
        FileNameDoesNotExist = 11,
        FileNameIsInvalid = 12,
        FileNameIsNullOrEmpty = 13,
        SaveFailed = 20,
        DeleteFailed = 21,
        RenameFailed = 22
    }
}
