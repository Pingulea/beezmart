﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit;
using MimeKit;
using MailKit.Net.Smtp;
using BeezMart.Utils;

namespace BeezMart.Services
{
	public class MailKitService : IEmailSender
    {
        /// <summary>
        ///     <para>Tries to send an e-mail message.</para>
        ///     <para>Uses SMTP settings configured in static class EmailSeviceConfig.</para>
        ///     <para>Returns null if everything was OK; or a list of error messages, if exceptions occured during the SMTP sending phase.</para>
        /// </summary>
        /// <param name="ToEmailAddress"></param>
        /// <param name="SubjectLine"></param>
        /// <param name="MessageBody"></param>
        /// <returns>A list of error messages, if exceptions occured during the SMTP sending phase. Returns null if everything was OK.</returns>
		public async Task<List<string>?> SendEmailAsync(string ToEmailAddress, string SubjectLine, string MessageBody)
		{
			List<string>? errors = null;
			if (string.IsNullOrWhiteSpace(ToEmailAddress)) throw new ArgumentException("Invalid parameter ToEmailAddress on calling BeezMart.Services.MailKitService.SendEmailAsync - missing destination e-mail address; ToEmailAddres is null, empty, or whitespace.");
			if (string.IsNullOrWhiteSpace(SubjectLine)) throw new ArgumentException("Invalid parameter SubjectLine on calling BeezMart.Services.MailKitService.SendEmailAsync - missing the e-mail subject line; SubjectLine is null, empty, or whitespace.");
			if (string.IsNullOrWhiteSpace(MessageBody)) throw new ArgumentException("Invalid parameter MessageBody on calling BeezMart.Services.MailKitService.SendEmailAsync - missing the e-mail message body; MessageBody is null, empty, or whitespace.");
			MimeMessage emailMessage = new MimeMessage();
			if (!EmailAddressValidator.IsValidEmailAddress(ToEmailAddress)) throw new ArgumentException("Invalid parameter ToEmailAddress on calling BeezMart.Services.MailKitService.SendEmailAsync - invalid destination e-mail address; ToEmailAddres is not a valid e-mail address.");
			emailMessage.From.Add(new MailboxAddress("BeezMart App", EmailSeviceConfig.FromEmailAddress));
			emailMessage.To.Add(new MailboxAddress(ToEmailAddress, ToEmailAddress));
			emailMessage.Subject = SubjectLine;
			TextPart textPart = new TextPart(MimeKit.Text.TextFormat.Html) { Text = MessageBody };
			emailMessage.Body = textPart;
			using (SmtpClient smtpClient = new SmtpClient())
			{
				try
				{
					await smtpClient.ConnectAsync(
						host: EmailSeviceConfig.SmtpMailServer.HostName,
						port: EmailSeviceConfig.SmtpMailServer.PortNumber,
						useSsl: false);
					smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
					await smtpClient.AuthenticateAsync(
						EmailSeviceConfig.SmtpMailServer.UserName,
						EmailSeviceConfig.SmtpMailServer.Password
						);
					await smtpClient.SendAsync(emailMessage);
					await smtpClient.DisconnectAsync(true);
				}
				catch (Exception exception)
				{
					errors = new List<string> { };
					errors.Add(exception.Message);
				}
			}
			return errors;
		}
        /// <summary>
        ///		<para>Sends a message to multiple recipients at once, placing multiple e-mail addresses in the TO line</para>
        ///     <para>Uses SMTP settings configured in static class EmailSeviceConfig.</para>
        ///     <para>Returns null if everything was OK; or a list of error messages, if exceptions occured during the SMTP sending phase.</para>
        /// </summary>
        /// <param name="ToEmailAddresses"></param>
        /// <param name="SubjectLine"></param>
        /// <param name="MessageBody"></param>
        /// <param name="CcEmailAddresses"></param>
        /// <param name="BccEmailAddresses"></param>
        /// <returns>A list of error messages, if exceptions occured during the SMTP sending phase. Returns null if everything was OK.</returns>
        /// <exception cref="ToEmailAddresses">Throws exception if ToEmailAddresses parameter is null or empty</exception>
        /// <exception cref="SubjectLine">Throws exception if SubjectLine parameter is null or whitespace</exception>
        /// <exception cref="MessageBody">Throws exception if MessageBody parameter is null or whitespace</exception>
        public async Task<List<string>?> SendEmailAsync(string[] ToEmailAddresses, string SubjectLine, string MessageBody, string[]? CcEmailAddresses = null, string[]? BccEmailAddresses = null)
		{
			List<string>? errors = null;
			if (ToEmailAddresses == null || ToEmailAddresses.Length == 0) throw new ArgumentException("Invalid parameter ToEmailAddres on calling BeezMart.Services.MailKitService.SendEmailAsync - missing destination e-mail addresses; ToEmailAddres is null, empty, or whitespace.");
			if (string.IsNullOrWhiteSpace(SubjectLine)) throw new ArgumentException("Invalid parameter SubjectLine on calling BeezMart.Services.MailKitService.SendEmailAsync - missing the e-mail subject line; SubjectLine is null, empty, or whitespace.");
			if (string.IsNullOrWhiteSpace(MessageBody)) throw new ArgumentException("Invalid parameter MessageBody on calling BeezMart.Services.MailKitService.SendEmailAsync - missing the e-mail message body; MessageBody is null, empty, or whitespace.");
			MimeMessage emailMessage = new MimeMessage();
			emailMessage.From.Add(new MailboxAddress("BeezMart App", EmailSeviceConfig.FromEmailAddress));
			foreach (string toEmailAddress in ToEmailAddresses)
			{
				if (EmailAddressValidator.IsValidEmailAddress(toEmailAddress))
					emailMessage.To.Add(new MailboxAddress(toEmailAddress, toEmailAddress));
			}
			if (emailMessage.To.Count == 0) return errors;
			if (CcEmailAddresses != null)
			{
				foreach (string ccEmailAddress in CcEmailAddresses)
				{
					if (EmailAddressValidator.IsValidEmailAddress(ccEmailAddress))
					{
						emailMessage.Cc.Add(new MailboxAddress(ccEmailAddress, ccEmailAddress));
					}
				}
			}
			if (BccEmailAddresses != null)
			{
				foreach (string bccEmailAddress in BccEmailAddresses)
				{
					if (EmailAddressValidator.IsValidEmailAddress(bccEmailAddress))
					{
						emailMessage.Bcc.Add(new MailboxAddress(bccEmailAddress, bccEmailAddress));
					}
				}
			}
			emailMessage.Subject = SubjectLine;
			TextPart textPart = new TextPart(MimeKit.Text.TextFormat.Html) { Text = MessageBody };
			emailMessage.Body = textPart;
			using (SmtpClient smtpClient = new SmtpClient())
			{
				try
				{
					await smtpClient.ConnectAsync(
						host: EmailSeviceConfig.SmtpMailServer.HostName,
						port: EmailSeviceConfig.SmtpMailServer.PortNumber,
						useSsl: false);
					smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
					await smtpClient.AuthenticateAsync(
						EmailSeviceConfig.SmtpMailServer.UserName,
						EmailSeviceConfig.SmtpMailServer.Password
						);
					await smtpClient.SendAsync(emailMessage);
					await smtpClient.DisconnectAsync(true);
				}
				catch (Exception exception)
				{
					errors = new List<string> { };
					errors.Add(exception.Message);
				}
			}
			return errors;
		}
        /// <summary>
        ///		<para>Sends a message multiple times to several recipients, separately for each TO address - only one address in the TO line at a time</para>
        ///     <para>Uses SMTP settings configured in static class EmailSeviceConfig.</para>
        ///     <para>Returns null if everything was OK; or a list of error messages, if exceptions occured during the SMTP sending phase.</para>
        /// </summary>
        /// <param name="ToEmailAddresses"></param>
        /// <param name="SubjectLine"></param>
        /// <param name="MessageBody"></param>
        /// <param name="CcEmailAddresses"></param>
        /// <param name="BccEmailAddresses"></param>
        /// <returns>A list of error messages, if exceptions occured during the SMTP sending phase. Returns null if everything was OK.</returns>
        /// <exception cref="ToEmailAddresses">Throws exception if ToEmailAddresses parameter is null or empty</exception>
        /// <exception cref="SubjectLine">Throws exception if SubjectLine parameter is null or whitespace</exception>
        /// <exception cref="MessageBody">Throws exception if MessageBody parameter is null or whitespace</exception>
        public async Task<List<string>?> SendEmailsAsync(string[] ToEmailAddresses, string SubjectLine, string MessageBody, string[]? CcEmailAddresses = null, string[]? BccEmailAddresses = null)
		{
			List<string>? errors = null;
			if (ToEmailAddresses == null || ToEmailAddresses.Length == 0) throw new ArgumentException("Invalid parameter ToEmailAddres on calling BeezMart.Services.MailKitService.SendEmailAsync - missing destination e-mail addresses; ToEmailAddres is null, empty, or whitespace.");
			if (string.IsNullOrWhiteSpace(SubjectLine)) throw new ArgumentException("Invalid parameter SubjectLine on calling BeezMart.Services.MailKitService.SendEmailAsync - missing the e-mail subject line; SubjectLine is null, empty, or whitespace.");
			if (string.IsNullOrWhiteSpace(MessageBody)) throw new ArgumentException("Invalid parameter MessageBody on calling BeezMart.Services.MailKitService.SendEmailAsync - missing the e-mail message body; MessageBody is null, empty, or whitespace.");
			MimeMessage emailMessage = new MimeMessage();
			emailMessage.From.Add(new MailboxAddress("BeezMart App", EmailSeviceConfig.FromEmailAddress));
			if (CcEmailAddresses != null)
			{
				foreach (string ccEmailAddress in CcEmailAddresses)
				{
					if (EmailAddressValidator.IsValidEmailAddress(ccEmailAddress))
					{
						emailMessage.Cc.Add(new MailboxAddress(ccEmailAddress, ccEmailAddress));
					}
				}
			}
			if (BccEmailAddresses != null)
			{
				foreach (string bccEmailAddress in BccEmailAddresses)
				{
					if (EmailAddressValidator.IsValidEmailAddress(bccEmailAddress))
					{
						emailMessage.Bcc.Add(new MailboxAddress(bccEmailAddress, bccEmailAddress));
					}
				}
			}
			emailMessage.Subject = SubjectLine;
			TextPart textPart = new TextPart(MimeKit.Text.TextFormat.Html) { Text = MessageBody };
			emailMessage.Body = textPart;
			using (SmtpClient smtpClient = new SmtpClient())
			{
				try
				{
					await smtpClient.ConnectAsync(
						host: EmailSeviceConfig.SmtpMailServer.HostName,
						port: EmailSeviceConfig.SmtpMailServer.PortNumber,
						useSsl: false);
					smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
					await smtpClient.AuthenticateAsync(
						EmailSeviceConfig.SmtpMailServer.UserName,
						EmailSeviceConfig.SmtpMailServer.Password
						);
					foreach (string toEmailAddress in ToEmailAddresses)
					{
						if (EmailAddressValidator.IsValidEmailAddress(toEmailAddress))
						{
							emailMessage.To.Clear();
							emailMessage.To.Add(new MailboxAddress(toEmailAddress, toEmailAddress));
							await smtpClient.SendAsync(emailMessage);
						}
					}
					await smtpClient.DisconnectAsync(true);
				}
				catch (Exception exception)
				{
					errors = new List<string> { };
					errors.Add(exception.Message);
				}
			}
			return errors;
		}

		/// <summary>
		///		<para>Connects a SMTP e-mail client to the server, in preparation for sending messages with Send() method.</para>
		///		<para>Do not forget to call Disconnect() once the e-mail messages are sent!</para>
		/// </summary>
		/// <returns>A string containing error details, if occurred; returns null if everything was OK.</returns>
		public async Task<string?> Connect()
		{
			string? error = null;
			if (this._smtpClient == null) this._smtpClient = new SmtpClient();
			try
			{
				await this._smtpClient.ConnectAsync(
					host: EmailSeviceConfig.SmtpMailServer.HostName,
					port: EmailSeviceConfig.SmtpMailServer.PortNumber,
					useSsl: false);
				this._smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
				await this._smtpClient.AuthenticateAsync(
					EmailSeviceConfig.SmtpMailServer.UserName,
					EmailSeviceConfig.SmtpMailServer.Password
					);
			}
			catch (Exception exception)
			{
				error = exception.Message;
			}
			return error;
		}

		/// <summary>
		///		<para>Sends an e-mail message through a previously opened SMTP client: make sure to call the Connect() method first!</para>
		///		<para>Do not forget to call Disconnect() once the e-mail messages are sent!</para>
		/// </summary>
		/// <param name="ToEmailAddress"></param>
		/// <param name="SubjectLine"></param>
		/// <param name="MessageBody"></param>
		/// <returns>A string containing error details, if occurred; returns null if everything was OK.</returns>
		public async Task<string?> Send(string ToEmailAddress, string SubjectLine, string MessageBody)
		{
			if (this._smtpClient == null) throw new Exception("E-mail client is not initialized. Try calling Connect() first.");
			if (!this._smtpClient.IsConnected) throw new Exception("E-mail client is not connected to SMTP server. Try calling Connect() first.");
			MimeMessage emailMessage = new MimeMessage();
			if (!EmailAddressValidator.IsValidEmailAddress(ToEmailAddress)) return $"Improper ToEmailAddress: '{ToEmailAddress}' is not a valid e-mail address.";
			emailMessage.From.Add(new MailboxAddress("BeezMart App", EmailSeviceConfig.FromEmailAddress));
			emailMessage.To.Add(new MailboxAddress(ToEmailAddress, ToEmailAddress));
			emailMessage.Subject = SubjectLine;
			TextPart textPart = new TextPart(MimeKit.Text.TextFormat.Html) { Text = MessageBody };
			emailMessage.Body = textPart;
			string? error = null;
			try
			{
				await this._smtpClient.SendAsync(emailMessage);
			}
			catch (Exception exception)
			{
				error = exception.Message;
			}
			return error;
		}

		/// <summary>
		///		<para>Disconnects and disposes a previously opened SMTP client via the Connect() method, to free up resources.</para>
		/// </summary>
		/// <returns></returns>
		public async Task Disconnect()
		{
			if (this._smtpClient == null) return;
			if (this._smtpClient.IsConnected)
				await _smtpClient.DisconnectAsync(true);
			this._smtpClient.Dispose();
			this._smtpClient = null;
		}

		protected SmtpClient? _smtpClient = null;
	}
}
