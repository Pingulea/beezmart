﻿using BeezMart.Models;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using BeezMart;

namespace BeezMart.Services;

public class AzureBlobService : IDocumentFilesService
{
    public AzureBlobService()
    {
        if (_blobStorageConfig == null)
            throw new MissingFieldException("The AzureBlobService.BlobStorageConfig is not set. Check the EnvironmentSettings.FileArchivingSettings.AzureStorageSettings; it may be missing.");
        this._blobServiceClient = new BlobServiceClient(_connectionString);
        this._containerClient = this._blobServiceClient.GetBlobContainerClient(_blobStorageConfig.ContainerName);
    }
    public static EnvironmentSettings.FileArchivingSettings.AzureStorageSettings? BlobStorageConfig
    {
        get { return _blobStorageConfig; }
        set
        {
            _blobStorageConfig = value;
            if (value != null)
                _connectionString = $"DefaultEndpointsProtocol=https;AccountName={value.AccountName};AccountKey={value.AccountKey}";
        }
    }
    
    /// <summary>
    ///     <para>The folder names for document files will be like "[rootContainerName]/documents/2018-03-24/id--293794719467657"</para>
    /// </summary>
    private static string GetAbsoluteBlobDirectoryPath(DateTime addedOn, long documentID)
    {
        return $"{BlobStorageConfig?.ContainerName}/documents/{addedOn.ToString("yyyy-MM-dd")}/id-{documentID}";
    }
    /// <summary>
    ///     <para>The folder names for document files will be like "/documents/2018-03-24/id--293794719467657"</para>
    /// </summary>
    private static string GetRelativeBlobDirectoryPath(DateTime addedOn, long documentID)
    {
        return $"documents/{addedOn.ToString("yyyy-MM-dd")}/id-{documentID}";
    }

    /// <summary>
    ///     <para>The document full names will be like "[rootContainerName]/documents/2018-03-24/id--293794719467657/fileName.ext"</para>
    /// </summary>
    private static string GetAbsoluteBlobPath(DateTime addedOn, long documentID, string fileName)
    {
        return $"{BlobStorageConfig?.ContainerName}/documents/{addedOn.ToString("yyyy-MM-dd")}/id-{documentID}/{fileName}";
    }
    /// <summary>
    ///     <para>The document full names will be like "/documents/2018-03-24/id--293794719467657/fileName.ext"</para>
    /// </summary>
    private static string GetRelativeBlobPath(DateTime addedOn, long documentID, string fileName)
    {
        return $"documents/{addedOn.ToString("yyyy-MM-dd")}/id-{documentID}/{fileName}";
    }

    public async Task<IEnumerable<string>> GetFileNamesAsync(long documentID, DateTime addedOn)
    {
        List<string> fileNames = new List<string>();
        string pathPrefix = GetRelativeBlobDirectoryPath(addedOn, documentID);
        Azure.AsyncPageable<BlobItem> blobItemList = this._containerClient.GetBlobsAsync(BlobTraits.None, BlobStates.None, pathPrefix);
        string fileName;
        int ndx;
        await foreach (BlobItem blobItem in blobItemList)
        {
            ndx = blobItem.Name.LastIndexOf('/');
            fileName = blobItem.Name.Substring(ndx + 1);
            fileNames.Add(fileName);
        };
        return fileNames;
    }

    public async Task<bool> DeleteFileAsync(string fileName, long documentID, DateTime addedOn)
    {
        if (string.IsNullOrWhiteSpace(fileName))
            throw new ArgumentNullException("fileName", "The 'fileName' parameter cannot be null or empty.");
        string blobName = GetRelativeBlobPath(addedOn, documentID, fileName);
        BlobClient blobClient = this._containerClient.GetBlobClient(blobName);
        bool exists = await blobClient.ExistsAsync();
        if (!exists) return false;
        await blobClient.DeleteAsync(DeleteSnapshotsOption.IncludeSnapshots);
        return true;
    }

    public async Task<bool> RenameFileAsync(string oldFileName, string newFileName, long documentID, DateTime addedOn)
    {
        if (string.IsNullOrWhiteSpace(oldFileName))
            throw new ArgumentNullException("oldFileName", "The 'oldFileName' parameter cannot be null or empty.");
        if (string.IsNullOrWhiteSpace(oldFileName))
            throw new ArgumentNullException("newFileName", "The 'newFileName' parameter cannot be null or empty.");
        string oldBlobName = GetRelativeBlobPath(addedOn, documentID, oldFileName);
        string newBlobName = GetRelativeBlobPath(addedOn, documentID, newFileName);
        BlobClient oldBlobClient = this._containerClient.GetBlobClient(oldBlobName);
        BlobClient newBlobClient = this._containerClient.GetBlobClient(newBlobName);
        if (!(await oldBlobClient.ExistsAsync())) return false;
        if (await newBlobClient.ExistsAsync()) return false;
        DateTimeOffset timeOffset = new DateTimeOffset(DateTime.Now.ToUniversalTime().AddMinutes(30));
        Uri oldBlobUri = oldBlobClient.GenerateSasUri(Azure.Storage.Sas.BlobSasPermissions.Read, timeOffset);
        await newBlobClient.SyncCopyFromUriAsync(oldBlobUri);
        await oldBlobClient.DeleteIfExistsAsync(DeleteSnapshotsOption.None);
        return true;
    }

    public async Task<bool> FileExistsAsync(string fileName, long documentID, DateTime addedOn)
    {
        if (string.IsNullOrWhiteSpace(fileName))
            throw new ArgumentNullException("fileName", "The 'fileName' parameter cannot be null or empty.");
        string blobName = GetRelativeBlobPath(addedOn, documentID, fileName);
        BlobClient blobClient = this._containerClient.GetBlobClient(blobName);
        return await blobClient.ExistsAsync();
    }

    public async Task<bool> SaveFileAsync(Stream fileStream, string fileName, long documentID, DateTime addedOn)
    {
        if (string.IsNullOrWhiteSpace(fileName))
            throw new ArgumentNullException("fileName", "The 'fileName' parameter cannot be null or empty.");
        string blobName = GetRelativeBlobPath(addedOn, documentID, fileName);
        BlobClient blobClient = this._containerClient.GetBlobClient(blobName);
        if (await blobClient.ExistsAsync()) return false;
        await blobClient.UploadAsync(fileStream);
        return true;
    }

    public async Task<bool> ReadToStreamAsync(string fileName, Stream targetStream, long documentID, DateTime addedOn)
    {
        if (string.IsNullOrWhiteSpace(fileName))
            throw new ArgumentNullException("fileName", "The 'fileName' parameter cannot be null or empty.");
        string blobName = GetRelativeBlobPath(addedOn, documentID, fileName);
        BlobClient blobClient = this._containerClient.GetBlobClient(blobName);
        if (!(await blobClient.ExistsAsync())) return false;
        await blobClient.DownloadToAsync(targetStream);
        return true;
    }

    private BlobContainerClient _containerClient;
    private static EnvironmentSettings.FileArchivingSettings.AzureStorageSettings? _blobStorageConfig;
    private static string? _connectionString;
    private BlobServiceClient _blobServiceClient;
}