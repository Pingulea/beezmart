﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeezMart.Services
{
	public interface IEmailSender
	{
		/// <summary>
		///		<para>Send an e-mail message</para>
		/// </summary>
		///		<param name="ToEmailAddress"></param>
		///		<param name="SubjectLine"></param>
		///		<param name="MessageBody"></param>
		/// <returns>A list of errors, if any occured; returns null if everything was OK.</returns>
		Task<List<string>?> SendEmailAsync(string ToEmailAddress, string SubjectLine, string MessageBody);

		/// <summary>
		///		<para>Connects a SMTP e-mail client to the server, in preparation for sending messages with Send() method.</para>
		///		<para>Do not forget to call Disconnect() once the e-mail messages are sent!</para>
		/// </summary>
		/// <returns>A string containing error details, if occurred; returns null if everything was OK.</returns>
		Task<string?> Connect();

		/// <summary>
		///		<para>Sends an e-mail message through a previously opened SMTP client: make sure to call the Connect() method first!</para>
		///		<para>Do not forget to call Disconnect() once the e-mail messages are sent!</para>
		/// </summary>
		/// <param name="ToEmailAddress"></param>
		/// <param name="SubjectLine"></param>
		/// <param name="MessageBody"></param>
		/// <returns>A string containing error details, if occurred; returns null if everything was OK.</returns>
		Task<string?> Send(string ToEmailAddress, string SubjectLine, string MessageBody);

		/// <summary>
		///		<para>Disconnects and disposes a previously opened SMTP client via the Connect() method, to free up resources.</para>
		/// </summary>
		/// <returns></returns>
		Task Disconnect();
	}
}
