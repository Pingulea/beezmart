﻿namespace BeezMart.Services;

public static class EmailSeviceConfig
{
	public static string? FromEmailAddress;
	public struct SmtpMailServer
	{
		public static string? HostName;
		public static int PortNumber;
		public static string? UserName;
		public static string? Password;
	}
}
