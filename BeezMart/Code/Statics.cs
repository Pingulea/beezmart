﻿using Microsoft.AspNetCore.Mvc;

namespace BeezMart;
public static class Statics
{
    public static class Problems
    {
        public static ProblemDetails CouldNotSaveDbRecord = new ProblemDetails{
            Status = 500,
            Title = "Could not save",
            Detail = "The corresponding record could not be saved into the database.",
            Type = null,    //   An URI reference could be placed here to the description of the problem
            Instance = null //   An URI reference could be placed here to the instance of the problem
        };
    }
}
