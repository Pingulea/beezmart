﻿namespace BeezMart;

public class EnvironmentSettings
{
	public HostingSettings Hosting { get; set; } = new HostingSettings();
	public ConnectionStringsSettings ConnectionStrings { get; set; } = new ConnectionStringsSettings();
	public MessagingServicesSettings MessagingServices { get; set; } = new MessagingServicesSettings();
	public FileArchivingSettings FileArchiving { get; set; } = new FileArchivingSettings();
	public AzureEntraAuthenticationSettings AzureEntraAuthentication { get; set; } = new AzureEntraAuthenticationSettings();

	public class HostingSettings
	{
		public string? RootURL { get; set; }
	}
	public class ConnectionStringsSettings
	{
		public string? MyAppDatabase { get; set; }
	}
	public class MessagingServicesSettings
	{
		public string? FromEmailAddress { get; set; }
		public SmtpMailServerSettings SmtpMailServer { get; set; } = new SmtpMailServerSettings();

		public class SmtpMailServerSettings
		{
			public string? HostName { get; set; }
			public string? PortNumber { get; set; }
			public string? UserName { get; set; }
			public string? Password { get; set; }
		}
	}
	public class FileArchivingSettings
	{
		public AzureStorageSettings AzureStorage { get; set; } = new AzureStorageSettings();

		public class AzureStorageSettings
		{
			public string? AccountName { get; set; }
			public string? AccountKey { get; set; }
			public string? ContainerName { get; set; }
		}
	}

	public class AzureEntraAuthenticationSettings
	{
#if LOCAL_ACCOUNTS
		public bool Enabled { get; } = false;
#else
		public bool Enabled { get; } = true;
#endif
		public string? Instance { get; set; }     = "https://login.microsoftonline.com/";
        public string? Domain { get; set; }
        public string? TenantId { get; set; }
        public string? ClientId { get; set; }
		public string? CallbackPath { get; set; } = "/signin-oidc";
    }
}