using BeezMart.DAL;
using BeezMart.Models;
using System.Data.SqlClient;

namespace BeezMart;

public static class MyApp
{
    public static string IdentityIssuer = "Nurbiosis";
    public static bool DatabaseIsSetUp { get; set; }
    public static string? PhysicalDirSourceRoot { get; set; }
    public static string? PhysicalDirWwwRoot { get; set; }
    public static Dictionary<string, string> ConfigSettings = new Dictionary<string, string>();
    public static EnvironmentSettings EnvSettings { get; set; } = new EnvironmentSettings { };

    public static List<string>? MissingHostingConfigurationSettings { get; set; }

    /// <summary>
    ///		<para>Verifies if the configuration settings are in place.</para>
    /// 	<para>Missing settings, if any, are placed in App.MissingConfigurationSettings.</para>
    ///		<para>The application might want to display a page warning about the missing configuration settings.</para>
    /// </summary>
    /// <returns></returns>
    public static void CheckIfRequiredHostingConfigurationsSettingsAreInPlace(ILogger logger, IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
    {
        logger.LogDebug("Entered " + nameof(CheckIfRequiredHostingConfigurationsSettingsAreInPlace) + "()");
        MissingHostingConfigurationSettings = new List<string> { };
        List<string> expectedConfigSettings = new List<string> {
            "ConnectionStrings:MyAppDatabase",
            "Hosting:RootURL",
            "MessagingServices:FromEmailAddress",
            "MessagingServices:SmtpMailServer:HostName",
            "MessagingServices:SmtpMailServer:PortNumber",
            "MessagingServices:SmtpMailServer:UserName",
            "MessagingServices:SmtpMailServer:Password",
            "FileArchiving:AzureStorage:AccountName",
            "FileArchiving:AzureStorage:AccountKey",
            "FileArchiving:AzureStorage:ContainerName",
            "ApplicationInsights:InstrumentationKey"
        };
#if !LOCAL_ACCOUNTS
        expectedConfigSettings.Add("AzureEntraAuthentication:Instance");
        expectedConfigSettings.Add("AzureEntraAuthentication:Domain");
        expectedConfigSettings.Add("AzureEntraAuthentication:TenantId");
        expectedConfigSettings.Add("AzureEntraAuthentication:ClientId");
        expectedConfigSettings.Add("AzureEntraAuthentication:CallbackPath");
#endif
        foreach (string expectedSetting in expectedConfigSettings)
        {
            if (hostingEnvironment.IsDevelopment())
                logger.LogInformation($"{expectedSetting} = {configuration[expectedSetting]}");
            if (EnvSettingIsMissing(configuration[expectedSetting]))
            {
                MissingHostingConfigurationSettings.Add(expectedSetting);
                logger.LogCritical($"Expected config setting is missing: {expectedSetting}");
            }
        }
    }

    /// <summary>
    ///		<para>Looks in to the database and tries to figure out if the application went through the installation procedure.</para>
    ///		<para>If the installation was done, then an 'App.Settings' table should be present in the database.</para>
    /// </summary>
    /// <returns>No return value, but sets MyApp.DatabaseIsSetUp = true/false</returns>
    public static void CheckIfDatabaseIsSetUp(ILogger logger, IWebHostEnvironment hostingEnvironment)
    {
        logger.LogDebug("Entered " + nameof(CheckIfDatabaseIsSetUp) + "()");
        if (hostingEnvironment.IsDevelopment())
            logger.LogInformation($"Initializing the Database.ConnectionString to App.ConfigSettings.ConnectionStrings.MyAppDatabase: {MyApp.EnvSettings.ConnectionStrings.MyAppDatabase}");
        Database.ConnectionString = MyApp.EnvSettings.ConnectionStrings.MyAppDatabase;
        MyApp.DatabaseIsSetUp = false;
        using (Database db = new Database())
        {
            string TransactSqlCmd_HowManyTablesAreInPlace = @"
                SELECT COUNT(*) FROM [Sys].[Objects]
                WHERE [Type_Desc] = 'User_Table'
                    AND SCHEMA_NAME(SCHEMA_ID) IN ('CRM', 'Billing', 'Sales', 'Delivery', 'Archiving')";
            logger.LogDebug($"DB TSQL: {TransactSqlCmd_HowManyTablesAreInPlace}");
            var dbResult = db.GetScalarFromTextCommand(TransactSqlCmd_HowManyTablesAreInPlace);
            if (dbResult == null) throw new NullReferenceException("Testing if tables are in DB failed; a NULL valua was returned by query.");
            int numberOfTables = (int)dbResult;
            if (numberOfTables > 0)
            {
                MyApp.DatabaseIsSetUp = true;
                logger.LogDebug("DB TSQL: Tables were found in DB's [Sys].[Objects] having app-specific schema like 'CRM', 'Billing', 'Sales', 'Delivery', 'Archiving'.");
            }
            else
            {
                logger.LogWarning("DB TSQL: No app specific tables were found in DB's [Sys].[Objects]; the database is not initialized.");
            }
        }
    }


    /// <summary>
    ///		<para>Returns the string value of the setting or a NULL if the settingName is not present</para>
    /// </summary>
    ///		<param name="settingName"></param>
    /// <returns></returns>
    public static string? GetConfigSetting(string settingName)
    {
        if (MyApp.ConfigSettings == null || MyApp.ConfigSettings.Count == 0) return null;
        if (MyApp.ConfigSettings.Keys.Contains(settingName))
        {
            return MyApp.ConfigSettings[settingName];
        }
        return null;
    }
    public static async Task LoadConfigSettingsAsync()
    {
        MyApp.ConfigSettings.Clear();
        MyApp.ConfigSettings = await AppSettings.ListAsync();
    }
    public static async Task SaveConfigSettingsAsync(Dictionary<string, string> settings)
    {
        await AppSettings.SaveAsync(settings);
    }

    public static DashboardStatsModel DashboardStats = new DashboardStatsModel();

    public static bool EnvSettingIsMissing(string? setting)
    {
        if (string.IsNullOrWhiteSpace(setting)) return true;
        if (setting.StartsWith("Example_", StringComparison.CurrentCultureIgnoreCase)) return true;
        return false;
    }
}