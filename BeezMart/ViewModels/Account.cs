﻿using BeezMart.Security;

namespace BeezMart.ViewModels.Account;

public class Index_ViewModel
{
	public ApplicationUser? UserProfile { get; set; }
	public string? SuggestedPassword { get; set; }
	public List<string>? UserRoles { get; set; }
	public bool SendReminder { get; set; }
}
public class Login_ViewModel
{
	public string? Username { get; set; }
	public string? Password { get; set; }
	public bool RememberMe { get; set; }
}
public class ForgotPassword_ViewModel
{
	public string? EmailAddress { get; set; }
}
public class ResetPassword_ViewModel
{
	public string? Username { get; set; }
	public string? NewPassword { get; set; }
	public string? PasswordConfirm { get; set; }
	public string? PasswordResetToken { get; set; }
	public bool SendReminder { get; set; }
}
public class SendCode_ViewModel
{
	public string? SelectedProvider { get; set; }
    public IEnumerable<string>? Providers { get; set; }
    public string? ReturnUrl { get; set; }
    public bool RememberMe { get; set; }
}
public class VerifyCode_ViewModel
{
	public string? Provider { get; set; }
	public string? Code { get; set; }
	public string? ReturnUrl { get; set; }
	public bool RememberBrowser { get; set; }
	public bool RememberMe { get; set; }
}
public class AccessDenied_ViewModel
{
    public string? ReturnUrl { get; set; }
}
public class UnauthorizedAccess_ViewModel
{
    public string? ReturnUrl { get; set; }
}
public class Forbidden_ViewModel
{
    public string? ReturnUrl { get; set; }
}