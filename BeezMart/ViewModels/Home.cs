﻿using BeezMart.Entities.Billing;
using BeezMart.Entities.CRM;
using BeezMart.Entities.TimeSheet;

namespace BeezMart.ViewModels.Home;

public class Search_ViewModel
{
    public List<OrganizationModel>? CrmOrganizations { get; set; }
    public List<PersonModel>? CrmPersons { get; set; }
    public List<InvoiceModel>? BillingInvoices { get; set; }
    public List<ProjectModel>? TimeSheetProjects { get; set; }
}
