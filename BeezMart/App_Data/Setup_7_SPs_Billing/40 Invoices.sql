﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Ins')
	DROP PROCEDURE Billing.Invoices_Ins
go
CREATE PROCEDURE Billing.Invoices_Ins
(
	@InvoiceID Int OUTPUT

	,@IssueDate SmallDateTime = NULL
	,@DueDate SmallDateTime = NULL
	,@ClosingDate SmallDateTime = NULL
	,@AccountingID VarChar(32) = NULL
	,@VATPercentage Decimal(6, 4) = 0
	,@Currency Char(3) = 'EUR'

	,@InvoiceTypeID TinyInt
	,@InvoiceStatusID TinyInt

	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@CustomerName NVarChar(256)
	,@CustomerCity NVarChar(64) = NULL
	,@CustomerProvince NVarChar(64) = NULL
	,@CustomerAddress NVarChar(256) = NULL
	,@CustomerRegistrationNumber VarChar(32) = NULL
	,@CustomerTaxID VarChar(16) = NULL
	,@CustomerBankName NVarChar(128) = NULL
	,@CustomerBankAccount VarChar(64) = NULL

	,@DelegateName NVarChar(128) = NULL
	,@DelegateID1 NVarChar(8) = NULL
	,@DelegateID2 NVarChar(16) = NULL
	,@DelegateID3 NVarChar(32) = NULL

	,@TransportName NVarChar(32) = NULL
	,@TransportDetail1 NVarChar(32) = NULL
	,@TransportDetail2 NVarChar(32) = NULL
	,@TransportDetail3 NVarChar(32) = NULL
	,@TransportDetail4 NVarChar(32) = NULL

	,@InvoiceDetail1 NVarChar(256) = NULL
	,@InvoiceDetail2 NVarChar(256) = NULL
	,@InvoiceDetail3 NVarChar(256) = NULL
	,@InvoiceDetail4 NVarChar(256) = NULL

	,@SalesOrderID Int = NULL
	,@DeliveryDocumentID Int = NULL
	,@RelatedInvoiceID Int = NULL
	,@RenderingLayout VarChar(256) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @IssueDate = ISNULL(@IssueDate, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Billing.Invoices
		(
		Issue_Date
		,Due_Date
		,Closing_Date
		,Accounting_ID
		,VAT_Percentage
		,Currency

		,Invoice_Type_ID
		,Invoice_Status_ID

		,Organization_ID
		,Person_ID
		,Customer_Name
		,Customer_City
		,Customer_Province
		,Customer_Address
		,Customer_Registration_Number
		,Customer_Tax_ID
		,Customer_Bank_Name
		,Customer_Bank_Account

		,Delegate_Name
		,Delegate_ID_1
		,Delegate_ID_2
		,Delegate_ID_3

		,Transport_Name
		,Transport_Detail_1
		,Transport_Detail_2
		,Transport_Detail_3
		,Transport_Detail_4

		,Invoice_Detail_1
		,Invoice_Detail_2
		,Invoice_Detail_3
		,Invoice_Detail_4

		,Sales_Order_ID
		,Delivery_Document_ID
		,Related_Invoice_ID
		,Rendering_Layout

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@IssueDate
		,@DueDate
		,@ClosingDate
		,@AccountingID
		,@VATPercentage
		,@Currency

		,@InvoiceTypeID
		,@InvoiceStatusID

		,@OrganizationID
		,@PersonID
		,@CustomerName
		,@CustomerCity
		,@CustomerProvince
		,@CustomerAddress
		,@CustomerRegistrationNumber
		,@CustomerTaxID
		,@CustomerBankName
		,@CustomerBankAccount

		,@DelegateName
		,@DelegateID1
		,@DelegateID2
		,@DelegateID3

		,@TransportName
		,@TransportDetail1
		,@TransportDetail2
		,@TransportDetail3
		,@TransportDetail4

		,@InvoiceDetail1
		,@InvoiceDetail2
		,@InvoiceDetail3
		,@InvoiceDetail4

		,@SalesOrderID
		,@DeliveryDocumentID
		,@RelatedInvoiceID
		,@RenderingLayout

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @InvoiceID = SCOPE_IDENTITY()

	SELECT CAST(@InvoiceID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Upd')
	DROP PROCEDURE Billing.Invoices_Upd
go
CREATE PROCEDURE Billing.Invoices_Upd
(
	@InvoiceID Int

	,@IssueDate SmallDateTime = NULL
	,@DueDate SmallDateTime = NULL
	,@ClosingDate SmallDateTime = NULL
	,@AccountingID VarChar(32) = NULL
	,@VATPercentage Decimal(6, 4) = 0
	,@Currency Char(3) = 'EUR'

	,@InvoiceTypeID TinyInt
	,@InvoiceStatusID TinyInt

	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@CustomerName NVarChar(256)
	,@CustomerCity NVarChar(64) = NULL
	,@CustomerProvince NVarChar(64) = NULL
	,@CustomerAddress NVarChar(256) = NULL
	,@CustomerRegistrationNumber VarChar(32) = NULL
	,@CustomerTaxID VarChar(16) = NULL
	,@CustomerBankName NVarChar(128) = NULL
	,@CustomerBankAccount VarChar(64) = NULL

	,@DelegateName NVarChar(128) = NULL
	,@DelegateID1 NVarChar(8) = NULL
	,@DelegateID2 NVarChar(16) = NULL
	,@DelegateID3 NVarChar(32) = NULL

	,@TransportName NVarChar(32) = NULL
	,@TransportDetail1 NVarChar(32) = NULL
	,@TransportDetail2 NVarChar(32) = NULL
	,@TransportDetail3 NVarChar(32) = NULL
	,@TransportDetail4 NVarChar(32) = NULL

	,@InvoiceDetail1 NVarChar(256) = NULL
	,@InvoiceDetail2 NVarChar(256) = NULL
	,@InvoiceDetail3 NVarChar(256) = NULL
	,@InvoiceDetail4 NVarChar(256) = NULL

	,@SalesOrderID Int = NULL
	,@DeliveryDocumentID Int = NULL
	,@RelatedInvoiceID Int = NULL
	,@RenderingLayout VarChar(256) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @IssueDate = ISNULL(@IssueDate, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Invoices
	SET
		Issue_Date = @IssueDate
		,Due_Date = @DueDate
		,Closing_Date = @ClosingDate
		,Accounting_ID = @AccountingID
		,VAT_Percentage = @VATPercentage
		,Currency = @Currency

		,Invoice_Type_ID = @InvoiceTypeID
		,Invoice_Status_ID = @InvoiceStatusID

		,Organization_ID = @OrganizationID
		,Person_ID = @PersonID
		,Customer_Name = @CustomerName
		,Customer_City = @CustomerCity
		,Customer_Province = @CustomerProvince
		,Customer_Address = @CustomerAddress
		,Customer_Registration_Number = @CustomerRegistrationNumber
		,Customer_Tax_ID = @CustomerTaxID
		,Customer_Bank_Name = @CustomerBankName
		,Customer_Bank_Account = @CustomerBankAccount

		,Delegate_Name = @DelegateName
		,Delegate_ID_1 = @DelegateID1
		,Delegate_ID_2 = @DelegateID2
		,Delegate_ID_3 = @DelegateID3

		,Transport_Name = @TransportName
		,Transport_Detail_1 = @TransportDetail1
		,Transport_Detail_2 = @TransportDetail2
		,Transport_Detail_3 = @TransportDetail3
		,Transport_Detail_4 = @TransportDetail4

		,Invoice_Detail_1 = @InvoiceDetail1
		,Invoice_Detail_2 = @InvoiceDetail2
		,Invoice_Detail_3 = @InvoiceDetail3
		,Invoice_Detail_4 = @InvoiceDetail4

		,Sales_Order_ID = @SalesOrderID
		,Delivery_Document_ID = @DeliveryDocumentID
		,Related_Invoice_ID = @RelatedInvoiceID
		,Rendering_Layout = @RenderingLayout

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Invoice_ID = @InvoiceID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Upd_1')
	DROP PROCEDURE Billing.Invoices_Upd_1
go
CREATE PROCEDURE Billing.Invoices_Upd_1
(
	@InvoiceID Int
	,@InvoiceStatusID TinyInt
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	IF (@InvoiceStatusID = 100)
		UPDATE Billing.Invoices
		SET
			Invoice_Status_ID = @InvoiceStatusID
			,Closing_Date = @UpdatedOn
			,Updated_On = @UpdatedOn
			,Updated_By = @UpdatedBy
		WHERE Invoice_ID = @InvoiceID
	ELSE
		UPDATE Billing.Invoices
		SET
			Invoice_Status_ID = @InvoiceStatusID
			,Updated_On = @UpdatedOn
			,Updated_By = @UpdatedBy
		WHERE Invoice_ID = @InvoiceID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Upd_2')
	DROP PROCEDURE Billing.Invoices_Upd_2
go
CREATE PROCEDURE Billing.Invoices_Upd_2
(
	@InvoiceID Int
	,@AccountingID VarChar(32)
)
AS
	UPDATE Billing.Invoices
	SET Accounting_ID = @AccountingID
	WHERE Invoice_ID = @InvoiceID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Sel')
	DROP PROCEDURE Billing.Invoices_Sel
go
CREATE PROCEDURE Billing.Invoices_Sel
(
	@InvoiceID Int
)
AS
	SELECT TOP 1
		Invoice_ID								-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

		,Issue_Date								-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Due_Date								-- SmallDateTime NULL
		,Closing_Date							-- SmallDateTime NULL
		,Accounting_ID							-- VarChar(32) NULL
		,VAT_Percentage							-- Decimal(6, 4) NOT NULL DEFAULT 0
		,Currency								-- Char(3) NOT NULL DEFAULT 'EUR'

		,Invoice_Type_ID						-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
		,Invoice_Status_ID						-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)

		,Organization_ID						-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name							-- NVarChar(256) NOT NULL
		,Customer_City							-- NVarChar(64) NULL
		,Customer_Province						-- NVarChar(64) NULL
		,Customer_Address						-- NVarChar(256) NULL
		,Customer_Registration_Number			-- VarChar(32) NULL
		,Customer_Tax_ID						-- VarChar(16) NULL
		,Customer_Bank_Name						-- NVarChar(128) NULL
		,Customer_Bank_Account					-- VarChar(64) NULL

		,Delegate_Name							-- NVarChar(128) NULL
		,Delegate_ID_1							-- NVarChar(8) NULL
		,Delegate_ID_2							-- NVarChar(16) NULL
		,Delegate_ID_3							-- NVarChar(32) NULL

		,Transport_Name							-- NVarChar(32) NULL
		,Transport_Detail_1						-- NVarChar(32) NULL
		,Transport_Detail_2						-- NVarChar(32) NULL
		,Transport_Detail_3						-- NVarChar(32) NULL
		,Transport_Detail_4						-- NVarChar(32) NULL

		,Invoice_Detail_1						-- NVarChar(256) NULL
		,Invoice_Detail_2						-- NVarChar(256) NULL
		,Invoice_Detail_3						-- NVarChar(256) NULL
		,Invoice_Detail_4						-- NVarChar(256) NULL

		,Sales_Order_ID							-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Delivery_Document_ID					-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Related_Invoice_ID						-- Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Rendering_Layout						-- VarChar(256) NULL

		,Updated_On								-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By								-- NVarChar(128) NULL

		,Billing.Invoice_Total_Value(@InvoiceID)	AS Total_Value		-- Decimal(278)
		,Billing.Invoice_Total_Payed(@InvoiceID)	AS Total_Payed		-- Decimal(278)
	FROM Billing.Invoices
	WHERE Invoice_ID = @InvoiceID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Sel_1')
	DROP PROCEDURE Billing.Invoices_Sel_1
go
CREATE PROCEDURE Billing.Invoices_Sel_1
AS
	SELECT TOP 1
		Invoice_ID										-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

		,Issue_Date										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Due_Date										-- SmallDateTime NULL
		,Closing_Date									-- SmallDateTime NULL
		,Accounting_ID									-- VarChar(32) NULL
		,VAT_Percentage									-- Decimal(6, 4) NOT NULL DEFAULT 0
		,Currency										-- Char(3) NOT NULL DEFAULT 'EUR'

		,Invoice_Type_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
		,Invoice_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)

		,Organization_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name									-- NVarChar(256) NOT NULL
		,Customer_City									-- NVarChar(64) NULL
		,Customer_Province								-- NVarChar(64) NULL
		,Customer_Address								-- NVarChar(256) NULL
		,Customer_Registration_Number					-- VarChar(32) NULL
		,Customer_Tax_ID								-- VarChar(16) NULL
		,Customer_Bank_Name								-- NVarChar(128) NULL
		,Customer_Bank_Account							-- VarChar(64) NULL

		,Delegate_Name									-- NVarChar(128) NULL
		,Delegate_ID_1									-- NVarChar(8) NULL
		,Delegate_ID_2									-- NVarChar(16) NULL
		,Delegate_ID_3									-- NVarChar(32) NULL

		,Transport_Name									-- NVarChar(32) NULL
		,Transport_Detail_1								-- NVarChar(32) NULL
		,Transport_Detail_2								-- NVarChar(32) NULL
		,Transport_Detail_3								-- NVarChar(32) NULL
		,Transport_Detail_4								-- NVarChar(32) NULL

		,Invoice_Detail_1								-- NVarChar(256) NULL
		,Invoice_Detail_2								-- NVarChar(256) NULL
		,Invoice_Detail_3								-- NVarChar(256) NULL
		,Invoice_Detail_4								-- NVarChar(256) NULL

		,Sales_Order_ID									-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Delivery_Document_ID							-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Related_Invoice_ID								-- Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Rendering_Layout								-- VarChar(256) NULL

		,Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By										-- NVarChar(128) NULL

		,Billing.Invoice_Total_Value(Invoice_ID)	AS Total_Value		-- Decimal(278)
		,Billing.Invoice_Total_Payed(Invoice_ID)	AS Total_Payed		-- Decimal(278)
	FROM Billing.Invoices
	WHERE Accounting_ID IS NOT NULL
	ORDER BY Issue_Date DESC
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Grid_1')
	DROP PROCEDURE Billing.Invoices_Grid_1
go
CREATE PROCEDURE Billing.Invoices_Grid_1
(
	@InvoiceTypeID TinyInt
	,@InvoiceStatusID TinyInt
	,@Currency Char(3)
)
AS
	SELECT
		 Invoice_ID										-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

		,Issue_Date										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Due_Date										-- SmallDateTime NULL
		,Closing_Date									-- SmallDateTime NULL
		,Accounting_ID									-- VarChar(32) NULL
		,VAT_Percentage									-- Decimal(6, 4) NOT NULL DEFAULT 0
		,Currency										-- Char(3) NOT NULL DEFAULT 'EUR'

		,Invoice_Type_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
		,Invoice_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)

		,Organization_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name									-- NVarChar(256) NOT NULL
		,Customer_City									-- NVarChar(64) NULL
		,Customer_Province								-- NVarChar(64) NULL
		,Customer_Address								-- NVarChar(256) NULL
		,Customer_Registration_Number					-- VarChar(32) NULL
		,Customer_Tax_ID								-- VarChar(16) NULL
		,Customer_Bank_Name								-- NVarChar(128) NULL
		,Customer_Bank_Account							-- VarChar(64) NULL

		,Delegate_Name									-- NVarChar(128) NULL
		,Delegate_ID_1									-- NVarChar(8) NULL
		,Delegate_ID_2									-- NVarChar(16) NULL
		,Delegate_ID_3									-- NVarChar(32) NULL

		,Transport_Name									-- NVarChar(32) NULL
		,Transport_Detail_1								-- NVarChar(32) NULL
		,Transport_Detail_2								-- NVarChar(32) NULL
		,Transport_Detail_3								-- NVarChar(32) NULL
		,Transport_Detail_4								-- NVarChar(32) NULL

		,Invoice_Detail_1								-- NVarChar(256) NULL
		,Invoice_Detail_2								-- NVarChar(256) NULL
		,Invoice_Detail_3								-- NVarChar(256) NULL
		,Invoice_Detail_4								-- NVarChar(256) NULL

		,Sales_Order_ID									-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Delivery_Document_ID							-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Related_Invoice_ID								-- Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Rendering_Layout								-- VarChar(256) NULL

		,Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By										-- NVarChar(128) NULL

		,Billing.Invoice_Total_Value(Invoice_ID)	AS Total_Value		-- Decimal(278)
		,Billing.Invoice_Total_Payed(Invoice_ID)	AS Total_Payed		-- Decimal(278)
	FROM Billing.Invoices
	WHERE Currency = @Currency
		AND Invoice_Type_ID = @InvoiceTypeID
		AND Invoice_Status_ID = @InvoiceStatusID
	ORDER BY Issue_Date, Due_Date
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Grid_1a')
	DROP PROCEDURE Billing.Invoices_Grid_1a
go
CREATE PROCEDURE Billing.Invoices_Grid_1a
(
	@InvoiceStatusID TinyInt
)
AS
	SELECT
		Currency			AS Currency
		,COUNT(*)			AS CountOfInvoices
	FROM Billing.Invoices
	WHERE Invoice_Status_ID = @InvoiceStatusID
	GROUP BY Currency
	ORDER BY Currency
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Grid_2')
	DROP PROCEDURE Billing.Invoices_Grid_2
go
CREATE PROCEDURE Billing.Invoices_Grid_2
(
	@InvoiceTypeID TinyInt
	,@Currency Char(3)
)
AS
	SELECT
		Invoice_ID										-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

		,Issue_Date										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Due_Date										-- SmallDateTime NULL
		,Closing_Date									-- SmallDateTime NULL
		,Accounting_ID									-- VarChar(32) NULL
		,VAT_Percentage									-- Decimal(6, 4) NOT NULL DEFAULT 0
		,Currency										-- Char(3) NOT NULL DEFAULT 'EUR'

		,Invoice_Type_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
		,Invoice_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)

		,Organization_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name									-- NVarChar(256) NOT NULL
		,Customer_City									-- NVarChar(64) NULL
		,Customer_Province								-- NVarChar(64) NULL
		,Customer_Address								-- NVarChar(256) NULL
		,Customer_Registration_Number					-- VarChar(32) NULL
		,Customer_Tax_ID								-- VarChar(16) NULL
		,Customer_Bank_Name								-- NVarChar(128) NULL
		,Customer_Bank_Account							-- VarChar(64) NULL

		,Delegate_Name									-- NVarChar(128) NULL
		,Delegate_ID_1									-- NVarChar(8) NULL
		,Delegate_ID_2									-- NVarChar(16) NULL
		,Delegate_ID_3									-- NVarChar(32) NULL

		,Transport_Name									-- NVarChar(32) NULL
		,Transport_Detail_1								-- NVarChar(32) NULL
		,Transport_Detail_2								-- NVarChar(32) NULL
		,Transport_Detail_3								-- NVarChar(32) NULL
		,Transport_Detail_4								-- NVarChar(32) NULL

		,Invoice_Detail_1								-- NVarChar(256) NULL
		,Invoice_Detail_2								-- NVarChar(256) NULL
		,Invoice_Detail_3								-- NVarChar(256) NULL
		,Invoice_Detail_4								-- NVarChar(256) NULL

		,Sales_Order_ID									-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Delivery_Document_ID							-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Related_Invoice_ID								-- Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Rendering_Layout								-- VarChar(256) NULL

		,Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By										-- NVarChar(128) NULL

		,Billing.Invoice_Total_Value(Invoice_ID)	AS Total_Value		-- Decimal(278)
		,Billing.Invoice_Total_Payed(Invoice_ID)	AS Total_Payed		-- Decimal(278)
	FROM Billing.Invoices
	WHERE Currency = @Currency
		AND Invoice_Type_ID = @InvoiceTypeID			-- REGULAR = 20, ADVANCE = 60, PRO-FORMA = 100
		AND Invoice_Status_ID = 60			-- SENT TO CUSTOMER
		AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()
	ORDER BY Due_Date, Issue_Date
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Grid_2a')
	DROP PROCEDURE Billing.Invoices_Grid_2a
go
CREATE PROCEDURE Billing.Invoices_Grid_2a
AS
	SELECT
		Currency			AS Currency
		,COUNT(*)			AS CountOfInvoices
	FROM Billing.Invoices
	WHERE Invoice_Type_ID IN (20, 60, 100)			-- REGULAR, ADVANCE, PRO-FORMA
		AND Invoice_Status_ID = 60			-- SENT TO CUSTOMER
		AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()
	GROUP BY Currency
	ORDER BY Currency
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices_Grid_3')
	DROP PROCEDURE Billing.Invoices_Grid_3
go
CREATE PROCEDURE Billing.Invoices_Grid_3
(
	@MaximumRowCount Int = 1000
)
AS
	SELECT TOP (@MaximumRowCount)
		Invoice_ID										-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

		,Issue_Date										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Due_Date										-- SmallDateTime NULL
		,Closing_Date									-- SmallDateTime NULL
		,Accounting_ID									-- VarChar(32) NULL
		,VAT_Percentage									-- Decimal(6, 4) NOT NULL DEFAULT 0
		,Currency										-- Char(3) NOT NULL DEFAULT 'EUR'

		,Invoice_Type_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
		,Invoice_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)

		,Organization_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name									-- NVarChar(256) NOT NULL
		,Customer_City									-- NVarChar(64) NULL
		,Customer_Province								-- NVarChar(64) NULL
		,Customer_Address								-- NVarChar(256) NULL
		,Customer_Registration_Number					-- VarChar(32) NULL
		,Customer_Tax_ID								-- VarChar(16) NULL
		,Customer_Bank_Name								-- NVarChar(128) NULL
		,Customer_Bank_Account							-- VarChar(64) NULL

		,Delegate_Name									-- NVarChar(128) NULL
		,Delegate_ID_1									-- NVarChar(8) NULL
		,Delegate_ID_2									-- NVarChar(16) NULL
		,Delegate_ID_3									-- NVarChar(32) NULL

		,Transport_Name									-- NVarChar(32) NULL
		,Transport_Detail_1								-- NVarChar(32) NULL
		,Transport_Detail_2								-- NVarChar(32) NULL
		,Transport_Detail_3								-- NVarChar(32) NULL
		,Transport_Detail_4								-- NVarChar(32) NULL

		,Invoice_Detail_1								-- NVarChar(256) NULL
		,Invoice_Detail_2								-- NVarChar(256) NULL
		,Invoice_Detail_3								-- NVarChar(256) NULL
		,Invoice_Detail_4								-- NVarChar(256) NULL

		,Sales_Order_ID									-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Delivery_Document_ID							-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Related_Invoice_ID								-- Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Rendering_Layout								-- VarChar(256) NULL

		,Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By										-- NVarChar(128) NULL

		,Billing.Invoice_Total_Value(Invoice_ID)	AS Total_Value		-- Decimal(278)
		,Billing.Invoice_Total_Payed(Invoice_ID)	AS Total_Payed		-- Decimal(278)
	FROM Billing.Invoices
	ORDER BY Invoice_ID DESC
go


