﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Accounts_Upsert')
	DROP PROCEDURE Billing.Customer_Accounts_Upsert
go
CREATE PROCEDURE Billing.Customer_Accounts_Upsert
(
	@AccountID Int OUTPUT
	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@IBAN VarChar(32)
	,@Bank NVarChar(128)
	,@Currency Char(3) = 'EUR'

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	IF (@OrganizationID IS NULL AND @PersonID IS NULL) RETURN
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	IF (@OrganizationID IS NOT NULL)
		BEGIN
			SET @AccountID = (SELECT TOP 1 Account_ID FROM Billing.Customer_Accounts WHERE Organization_ID = @OrganizationID AND Currency = @Currency AND IBAN = @IBAN)
			IF (@AccountID IS NOT NULL)
				BEGIN
					UPDATE Billing.Customer_Accounts
					SET Bank = @Bank
						,Updated_On = @UpdatedOn
						,Updated_By = @UpdatedBy
					WHERE Account_ID = @AccountID

					SELECT CAST(@AccountID AS Int) AS ID
					RETURN
				END
		END
	IF (@PersonID IS NOT NULL)
		BEGIN
			SET @AccountID = (SELECT TOP 1 Account_ID FROM Billing.Customer_Accounts WHERE Person_ID = @PersonID AND Currency = @Currency AND IBAN = @IBAN)
			IF (@AccountID IS NOT NULL)
				BEGIN
					UPDATE Billing.Customer_Accounts
					SET Bank = @Bank
						,Updated_On = @UpdatedOn
						,Updated_By = @UpdatedBy
					WHERE Account_ID = @AccountID

					SELECT CAST(@AccountID AS Int) AS ID
					RETURN
				END
		END
	INSERT INTO Billing.Customer_Accounts
		(
		Organization_ID
		,Person_ID
		,IBAN
		,Bank
		,Currency

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@OrganizationID
		,@PersonID
		,@IBAN
		,@Bank
		,@Currency

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @AccountID = SCOPE_IDENTITY()

	SELECT CAST(@AccountID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Accounts_Upd')
	DROP PROCEDURE Billing.Customer_Accounts_Upd
go
CREATE PROCEDURE Billing.Customer_Accounts_Upd
(
	@AccountID Int
	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@IBAN VarChar(32)
	,@Bank NVarChar(128)
	,@Currency Char(3) = 'EUR'

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	IF (@OrganizationID IS NULL AND @PersonID IS NULL) RETURN
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Customer_Accounts
	SET
		Organization_ID = @OrganizationID
		,Person_ID = @PersonID
		,IBAN = @IBAN
		,Bank = @Bank
		,Currency = @Currency

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Account_ID = @AccountID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Accounts_Sel')
	DROP PROCEDURE Billing.Customer_Accounts_Sel
go
CREATE PROCEDURE Billing.Customer_Accounts_Sel
(
	@AccountID Int
)
AS
	SELECT TOP 1
		Account_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,IBAN						-- VarChar(32) NOT NULL
		,Bank						-- NVarChar(128) NOT NULL
		,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
									
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Billing.Customer_Accounts
	WHERE Account_ID = @AccountID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Accounts_Grid_1')
	DROP PROCEDURE Billing.Customer_Accounts_Grid_1
go
CREATE PROCEDURE Billing.Customer_Accounts_Grid_1
(
	@OrganizationID Int
	,@Currency Char(3) = NULL
)
AS
	IF (@Currency IS NULL)
		SELECT
			Account_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
			,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
			,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
			,IBAN						-- VarChar(32) NOT NULL
			,Bank						-- NVarChar(128) NOT NULL
			,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
									
			,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
			,Updated_By					-- NVarChar(128) NULL
		FROM Billing.Customer_Accounts
		WHERE Organization_ID = @OrganizationID
		ORDER BY Currency, Bank, IBAN
	ELSE
		SELECT
			Account_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
			,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
			,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
			,IBAN						-- VarChar(32) NOT NULL
			,Bank						-- NVarChar(128) NOT NULL
			,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
									
			,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
			,Updated_By					-- NVarChar(128) NULL
		FROM Billing.Customer_Accounts
		WHERE Currency = @Currency
			AND Organization_ID = @OrganizationID
		ORDER BY Currency, Bank, IBAN
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Accounts_Grid_2')
	DROP PROCEDURE Billing.Customer_Accounts_Grid_2
go
CREATE PROCEDURE Billing.Customer_Accounts_Grid_2
(
	@PersonID Int
	,@Currency Char(3) = NULL
)
AS
	IF (@Currency IS NULL)
		SELECT
			Account_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
			,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
			,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
			,IBAN						-- VarChar(32) NOT NULL
			,Bank						-- NVarChar(128) NOT NULL
			,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
									
			,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
			,Updated_By					-- NVarChar(128) NULL
		FROM Billing.Customer_Accounts
		WHERE Person_ID = @PersonID
		ORDER BY Currency, Bank, IBAN
	ELSE
		SELECT
			Account_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
			,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
			,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
			,IBAN						-- VarChar(32) NOT NULL
			,Bank						-- NVarChar(128) NOT NULL
			,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
									
			,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
			,Updated_By					-- NVarChar(128) NULL
		FROM Billing.Customer_Accounts
		WHERE Currency = @Currency
			AND Person_ID = @PersonID
		ORDER BY Currency, Bank, IBAN
go


