﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Settings_Upsert')
	DROP PROCEDURE Billing.Customer_Settings_Upsert
go
CREATE PROCEDURE Billing.Customer_Settings_Upsert
(
	@RecordID Int OUTPUT
	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@RegistrationNumber VarChar(32) = NULL
	,@TaxID VarChar(16) = NULL
	,@Currency Char(3) = NULL
	,@PreferredAccountID Int = NULL

	,@MaximumDiscountPercentage TinyInt = NULL
	,@PaymentAllowanceInDays TinyInt = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	IF (@OrganizationID IS NULL AND @PersonID IS NULL) RETURN
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	IF (@OrganizationID IS NOT NULL)
		BEGIN
			SET @RecordID = (SELECT TOP 1 Record_ID FROM Billing.Customer_Settings WHERE Organization_ID = @OrganizationID)
			IF (@RecordID IS NOT NULL)
				BEGIN
					UPDATE Billing.Customer_Settings
					SET
						Organization_ID = @OrganizationID
						,Person_ID = @PersonID
						,Registration_Number = @RegistrationNumber
						,Tax_ID = @TaxID
						,Currency = @Currency
						,Preferred_Account_ID = @PreferredAccountID
		
						,Maximum_Discount_Percentage = @MaximumDiscountPercentage
						,Payment_Allowance_In_Days = @PaymentAllowanceInDays

						,Updated_On = @UpdatedOn
						,Updated_By = @UpdatedBy
					WHERE Record_ID = @RecordID

					SELECT CAST(@RecordID AS Int) AS ID
					RETURN
				END
		END
	IF (@PersonID IS NOT NULL)
		BEGIN
			SET @RecordID = (SELECT TOP 1 Record_ID FROM Billing.Customer_Settings WHERE Person_ID = @PersonID)
			IF (@RecordID IS NOT NULL)
				BEGIN
					UPDATE Billing.Customer_Settings
					SET
						Organization_ID = @OrganizationID
						,Person_ID = @PersonID
						,Registration_Number = @RegistrationNumber
						,Tax_ID = @TaxID
						,Currency = @Currency
						,Preferred_Account_ID = @PreferredAccountID
		
						,Maximum_Discount_Percentage = @MaximumDiscountPercentage
						,Payment_Allowance_In_Days = @PaymentAllowanceInDays

						,Updated_On = @UpdatedOn
						,Updated_By = @UpdatedBy
					WHERE Record_ID = @RecordID

					SELECT CAST(@RecordID AS Int) AS ID
					RETURN
				END
		END
	INSERT INTO Billing.Customer_Settings
		(
		Organization_ID
		,Person_ID
		,Registration_Number
		,Tax_ID
		,Currency
		,Preferred_Account_ID

		,Maximum_Discount_Percentage
		,Payment_Allowance_In_Days

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@OrganizationID
		,@PersonID
		,@RegistrationNumber
		,@TaxID
		,@Currency
		,@PreferredAccountID

		,@MaximumDiscountPercentage
		,@PaymentAllowanceInDays

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @RecordID = SCOPE_IDENTITY()

	SELECT CAST(@RecordID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Settings_Upd')
	DROP PROCEDURE Billing.Customer_Settings_Upd
go
CREATE PROCEDURE Billing.Customer_Settings_Upd
(
	@RecordID Int
	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@RegistrationNumber VarChar(32) = NULL
	,@TaxID VarChar(16) = NULL
	,@Currency Char(3) = NULL
	,@PreferredAccountID Int = NULL

	,@MaximumDiscountPercentage TinyInt = NULL
	,@PaymentAllowanceInDays TinyInt = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	IF (@OrganizationID IS NULL AND @PersonID IS NULL) RETURN
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Customer_Settings
	SET
		Organization_ID = @OrganizationID
		,Person_ID = @PersonID
		,Registration_Number = @RegistrationNumber
		,Tax_ID = @TaxID
		,Currency = @Currency
		,Preferred_Account_ID = @PreferredAccountID
		
		,Maximum_Discount_Percentage = @MaximumDiscountPercentage
		,Payment_Allowance_In_Days = @PaymentAllowanceInDays

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Settings_Sel')
	DROP PROCEDURE Billing.Customer_Settings_Sel
go
CREATE PROCEDURE Billing.Customer_Settings_Sel
(
	@RecordID Int
)
AS
	SELECT TOP 1
		Record_ID								-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Organization_ID						-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Registration_Number					-- VarChar(32) NULL
		,Tax_ID									-- VarChar(16) NULL
		,Currency								-- Char(3) NULL
		,Preferred_Account_ID					-- Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Record_ID)
		,Maximum_Discount_Percentage			-- TinyInt NULL
		,Payment_Allowance_In_Days				-- TinyInt NULL
												
		,Updated_On								-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By								-- NVarChar(128) NULL
	FROM Billing.Customer_Settings
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Settings_Sel_1')
	DROP PROCEDURE Billing.Customer_Settings_Sel_1
go
CREATE PROCEDURE Billing.Customer_Settings_Sel_1
(
	@OrganizationID Int
)
AS
	SELECT TOP 1
		Record_ID								-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Organization_ID						-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Registration_Number					-- VarChar(32) NULL
		,Tax_ID									-- VarChar(16) NULL
		,Currency								-- Char(3) NULL
		,Preferred_Account_ID					-- Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Record_ID)
		,Maximum_Discount_Percentage			-- TinyInt NULL
		,Payment_Allowance_In_Days				-- TinyInt NULL
												
		,Updated_On								-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By								-- NVarChar(128) NULL
	FROM Billing.Customer_Settings
	WHERE Organization_ID = @OrganizationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Settings_Sel_2')
	DROP PROCEDURE Billing.Customer_Settings_Sel_2
go
CREATE PROCEDURE Billing.Customer_Settings_Sel_2
(
	@PersonID Int
)
AS
	SELECT TOP 1
		Record_ID								-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Organization_ID						-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Registration_Number					-- VarChar(32) NULL
		,Tax_ID									-- VarChar(16) NULL
		,Currency								-- Char(3) NULL
		,Preferred_Account_ID					-- Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Record_ID)
		,Maximum_Discount_Percentage			-- TinyInt NULL
		,Payment_Allowance_In_Days				-- TinyInt NULL
												
		,Updated_On								-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By								-- NVarChar(128) NULL
	FROM Billing.Customer_Settings
	WHERE Person_ID = @PersonID
go


