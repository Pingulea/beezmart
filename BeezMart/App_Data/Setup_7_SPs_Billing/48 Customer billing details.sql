﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Billing_Details_Organization')
	DROP PROCEDURE Billing.Customer_Billing_Details_Organization
go
CREATE PROCEDURE Billing.Customer_Billing_Details_Organization
(
	 @OrganizationID Int
	,@Currency Char(3) = NULL
)
AS
	DECLARE @CustomerName NVarChar(256);
	DECLARE @City NVarChar(64);
	DECLARE @StateProvince NVarChar(128);
	DECLARE @StreetAddress1 NVarChar(128);
	DECLARE @RegistrationNumber VarChar(32);
	DECLARE @TaxID VarChar(16);
	DECLARE @Bank NVarChar(128);
	DECLARE @IBAN VarChar(32);
	DECLARE @BillingAddressID Int;
	DECLARE @BankAccountID Int;

	DECLARE @NamePrefix NVarChar(16);
	DECLARE @OrgName NVarChar(256);
	DECLARE @NameSuffix NVarChar(16);
	DECLARE @SiteIDHome Int;
	DECLARE @SiteIDBilling Int;
	DECLARE @SiteIDShipping Int;
	DECLARE @SiteID Int;

    SELECT TOP 1
             @OrganizationID = Organization_ID
            ,@NamePrefix = Name_Prefix
		    ,@OrgName = Org_Name
		    ,@NameSuffix = Name_Suffix
		    ,@SiteIDHome = Site_ID_Headquarter
		    ,@SiteIDBilling = Site_ID_Billing
		    ,@SiteIDShipping = Site_ID_Shipping
	    FROM CRM.Organizations
	    WHERE Organization_ID = @OrganizationID;
		
	SET @CustomerName = ISNULL(@NamePrefix, '') + ' ' + @OrgName + ' ' + ISNULL(@NameSuffix, '');
	SET @CustomerName = LTRIM(RTRIM(@CustomerName));
    SET @BillingAddressID = @SiteIDBilling;
	SET @SiteID = @SiteIDBilling;
	IF (@SiteID IS NULL) SET @SiteID = @SiteIDHome;
	IF (@SiteID IS NULL) SET @SiteID = @SiteIDShipping;
	IF (@SiteID IS NOT NULL)
		SELECT TOP 1 @City = City, @StateProvince = State_Province, @StreetAddress1 = Street_Address_1
			FROM CRM.Orgs_Sites WHERE Site_ID = @SiteID;
	SELECT TOP 1 @RegistrationNumber = Registration_Number, @TaxID = Tax_ID
		FROM Billing.Customer_Settings WHERE Organization_ID = @OrganizationID;
	IF (@TaxID IS NULL)
		SELECT TOP 1 @RegistrationNumber = Customer_Registration_Number, @TaxID = Customer_Tax_ID
			FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Organization_ID = @OrganizationID ORDER BY Invoice_ID DESC;
	IF (@Currency IS NULL)
		BEGIN
			SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
				FROM Billing.Customer_Accounts WHERE Organization_ID = @OrganizationID;
			IF (@IBAN IS NULL)
				SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
					FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Organization_ID = @OrganizationID;
		END
	ELSE
		BEGIN
			SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
				FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Organization_ID = @OrganizationID;
			IF (@IBAN IS NULL)
				SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
					FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Currency = @Currency AND Organization_ID = @OrganizationID;
		END
	SELECT
         @OrganizationID AS Organization_ID
		,NULL AS Person_ID
		,@CustomerName AS Customer_Name
		,@City AS City
		,@StateProvince AS State_Province
		,@StreetAddress1 AS Street_Address_1
		,@RegistrationNumber AS Registration_Number
		,@TaxID AS Tax_ID
		,@Bank AS Bank
		,@IBAN AS IBAN
        ,@BillingAddressID AS Billing_Address_ID
        ,@BankAccountID AS Bank_Account_ID;
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Billing_Details_Person')
	DROP PROCEDURE Billing.Customer_Billing_Details_Person
go
CREATE PROCEDURE Billing.Customer_Billing_Details_Person
(
	 @PersonID Int
	,@Currency Char(3) = NULL
)
AS
	DECLARE @CustomerName NVarChar(256);
	DECLARE @City NVarChar(64);
	DECLARE @StateProvince NVarChar(128);
	DECLARE @StreetAddress1 NVarChar(128);
	DECLARE @RegistrationNumber VarChar(32);
	DECLARE @TaxID VarChar(16);
	DECLARE @Bank NVarChar(128);
	DECLARE @IBAN VarChar(32);
	DECLARE @BillingAddressID Int;
	DECLARE @BankAccountID Int;

	DECLARE @NamePrefix NVarChar(16);
	DECLARE @FirstName NVarChar(128);
	DECLARE @LastName NVarChar(128);
	DECLARE @NameSuffix NVarChar(16);
	DECLARE @SiteIDHome Int;
	DECLARE @SiteIDBilling Int;
	DECLARE @SiteIDShipping Int;
	DECLARE @SiteID Int;

    SELECT TOP 1
             @PersonID = Person_ID
            ,@NamePrefix = Name_Prefix
		    ,@FirstName = First_Name
		    ,@LastName = Last_Name
		    ,@NameSuffix = Name_Suffix
		    ,@SiteIDHome = Site_ID_Home
		    ,@SiteIDBilling = Site_ID_Billing
		    ,@SiteIDShipping = Site_ID_Shipping
	    FROM CRM.Persons
	    WHERE Person_ID = @PersonID;
		
	SET @CustomerName = ISNULL(@NamePrefix, '') + ' ' + @FirstName + ' ' + @LastName + ' ' + ISNULL(@NameSuffix, '');
	SET @CustomerName = LTRIM(RTRIM(@CustomerName));
    SET @BillingAddressID = @SiteIDBilling;
	SET @SiteID = @SiteIDBilling;
	IF (@SiteID IS NULL) SET @SiteID = @SiteIDHome;
	IF (@SiteID IS NULL) SET @SiteID = @SiteIDShipping;
	IF (@SiteID IS NOT NULL)
		SELECT TOP 1 @City = City, @StateProvince = State_Province, @StreetAddress1 = Street_Address_1
			FROM CRM.Orgs_Sites WHERE Site_ID = @SiteID;
	SELECT TOP 1 @RegistrationNumber = Registration_Number, @TaxID = Tax_ID
		FROM Billing.Customer_Settings WHERE Person_ID = @PersonID;
	IF (@TaxID IS NULL)
		SELECT TOP 1 @RegistrationNumber = Customer_Registration_Number, @TaxID = Customer_Tax_ID
			FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Person_ID = @PersonID ORDER BY Invoice_ID DESC;
	IF (@Currency IS NULL)
		BEGIN
			SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
				FROM Billing.Customer_Accounts WHERE Person_ID = @PersonID;
			IF (@IBAN IS NULL)
				SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
					FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Person_ID = @PersonID;
		END
	ELSE
		BEGIN
			SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
				FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Person_ID = @PersonID;
			IF (@IBAN IS NULL)
				SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
					FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Currency = @Currency AND Person_ID = @PersonID;
		END
	SELECT
         NULL AS Organization_ID
		,@PersonID AS Person_ID
		,@CustomerName AS Customer_Name
		,@City AS City
		,@StateProvince AS State_Province
		,@StreetAddress1 AS Street_Address_1
		,@RegistrationNumber AS Registration_Number
		,@TaxID AS Tax_ID
		,@Bank AS Bank
		,@IBAN AS IBAN
        ,@BillingAddressID AS Billing_Address_ID
        ,@BankAccountID AS Bank_Account_ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Billing_Details_Lookup_Organizations')
	DROP PROCEDURE Billing.Customer_Billing_Details_Lookup_Organizations
go
CREATE PROCEDURE Billing.Customer_Billing_Details_Lookup_Organizations
(
	 @NamePattern NVarChar(128)
	,@Currency Char(3) = NULL
	,@MaxRows SmallInt = 10
)
AS
	DECLARE @SearchResults TABLE
		(
			 Organization_ID Int
			,Person_ID Int
			,Customer_Name NVarChar(256)
			,City NVarChar(64)
			,State_Province NVarChar(128)
			,Street_Address_1 NVarChar(128)
			,Registration_Number VarChar(32)
			,Tax_ID VarChar(16)
			,Bank NVarChar(128)
			,IBAN VarChar(32)
            ,Billing_Address_ID Int
            ,Bank_Account_ID Int
		);
	
	DECLARE @OrganizationID Int;
	DECLARE @PersonID Int;
	DECLARE @CustomerName NVarChar(256);
	DECLARE @City NVarChar(64);
	DECLARE @StateProvince NVarChar(128);
	DECLARE @StreetAddress1 NVarChar(128);
	DECLARE @RegistrationNumber VarChar(32);
	DECLARE @TaxID VarChar(16);
	DECLARE @Bank NVarChar(128);
	DECLARE @IBAN VarChar(32);
	DECLARE @BillingAddressID Int;
	DECLARE @BankAccountID Int;

	DECLARE @NamePrefix NVarChar(16);
	DECLARE @OrgName NVarChar(256);
	DECLARE @NameSuffix NVarChar(16);
	DECLARE @SiteIDHome Int;
	DECLARE @SiteIDBilling Int;
	DECLARE @SiteIDShipping Int;
	DECLARE @SiteID Int;
		
	DECLARE Customers CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
		SELECT
			 Organization_ID
			,Name_Prefix
			,Org_Name
			,Name_Suffix
			,Site_ID_Headquarter
			,Site_ID_Billing
			,Site_ID_Shipping
		FROM CRM.Organizations
		WHERE Org_Name LIKE @NamePattern
			OR Alternate_Names LIKE @NamePattern;
        
    DECLARE @RowCount SmallInt = 0;
	OPEN Customers;
	FETCH NEXT FROM Customers INTO @OrganizationID, @NamePrefix, @OrgName, @NameSuffix, @SiteIDHome, @SiteIDBilling, @SiteIDShipping;
	WHILE (@@FETCH_STATUS = 0 AND @RowCount < @MaxRows)
		BEGIN
            SET @RowCount = @RowCount + 1;
			SET @CustomerName = ISNULL(@NamePrefix, '') + ' ' + @OrgName + ' ' + ISNULL(@NameSuffix, '');
			SET @CustomerName = LTRIM(RTRIM(@CustomerName));
            SET @BillingAddressID = @SiteIDBilling;
			SET @SiteID = @SiteIDBilling;
			IF (@SiteID IS NULL) SET @SiteID = @SiteIDHome;
			IF (@SiteID IS NULL) SET @SiteID = @SiteIDShipping;
			IF (@SiteID IS NOT NULL)
				SELECT TOP 1 @City = City, @StateProvince = State_Province, @StreetAddress1 = Street_Address_1
				    FROM CRM.Orgs_Sites WHERE Site_ID = @SiteID;
			SELECT TOP 1 @RegistrationNumber = Registration_Number, @TaxID = Tax_ID
				FROM Billing.Customer_Settings WHERE Organization_ID = @OrganizationID;
			IF (@TaxID IS NULL)
				SELECT TOP 1 @RegistrationNumber = Customer_Registration_Number, @TaxID = Customer_Tax_ID
				    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Organization_ID = @OrganizationID ORDER BY Invoice_ID DESC;
			IF (@Currency IS NULL)
				BEGIN
					SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
						FROM Billing.Customer_Accounts WHERE Organization_ID = @OrganizationID;
					IF (@IBAN IS NULL)
						SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
						    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Organization_ID = @OrganizationID;
				END
			ELSE
				BEGIN
					SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
						FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Organization_ID = @OrganizationID;
					IF (@IBAN IS NULL)
						SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
						    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Currency = @Currency AND Organization_ID = @OrganizationID;
				END
			INSERT INTO @SearchResults
				(
					 Organization_ID
					,Person_ID
					,Customer_Name
					,City
					,State_Province
					,Street_Address_1
					,Registration_Number
					,Tax_ID
					,Bank
					,IBAN
                    ,Billing_Address_ID
                    ,Bank_Account_ID
				)
			VALUES
				(
					 @OrganizationID
					,NULL
					,@CustomerName
					,@City
					,@StateProvince
					,@StreetAddress1
					,@RegistrationNumber
					,@TaxID
					,@Bank
					,@IBAN
			        ,@BillingAddressID
			        ,@BankAccountID
				);
		    SET @OrganizationID = NULL;
		    SET @PersonID = NULL;
		    SET @CustomerName = NULL;
		    SET @City = NULL;
		    SET @StateProvince = NULL;
		    SET @StreetAddress1 = NULL;
		    SET @RegistrationNumber = NULL;
		    SET @TaxID = NULL;
		    SET @Bank = NULL;
		    SET @IBAN = NULL;
		    SET @NamePrefix = NULL;
		    SET @OrgName = NULL;
		    SET @NameSuffix = NULL;
		    SET @SiteIDHome = NULL;
		    SET @SiteIDBilling = NULL;
		    SET @SiteIDShipping = NULL;
		    SET @SiteID = NULL;
			SET @BillingAddressID = NULL;
			SET @BankAccountID = NULL;
			FETCH NEXT FROM Customers INTO @OrganizationID, @NamePrefix, @OrgName, @NameSuffix, @SiteIDHome, @SiteIDBilling, @SiteIDShipping;
		END
	CLOSE Customers;
	DEALLOCATE Customers;
	
	SELECT * FROM @SearchResults;
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Billing_Details_Lookup_Persons')
	DROP PROCEDURE Billing.Customer_Billing_Details_Lookup_Persons
go
CREATE PROCEDURE Billing.Customer_Billing_Details_Lookup_Persons
(
	 @NamePattern NVarChar(128)
	,@Currency Char(3) = NULL
	,@MaxRows SmallInt = 10
)
AS
	DECLARE @SearchResults TABLE
		(
			 Organization_ID Int
			,Person_ID Int
			,Customer_Name NVarChar(256)
			,City NVarChar(64)
			,State_Province NVarChar(128)
			,Street_Address_1 NVarChar(128)
			,Registration_Number VarChar(32)
			,Tax_ID VarChar(16)
			,Bank NVarChar(128)
			,IBAN VarChar(32)
            ,Billing_Address_ID Int
            ,Bank_Account_ID Int
		);
	
	DECLARE @OrganizationID Int;
	DECLARE @PersonID Int;
	DECLARE @CustomerName NVarChar(256);
	DECLARE @City NVarChar(64);
	DECLARE @StateProvince NVarChar(128);
	DECLARE @StreetAddress1 NVarChar(128);
	DECLARE @RegistrationNumber VarChar(32);
	DECLARE @TaxID VarChar(16);
	DECLARE @Bank NVarChar(128);
	DECLARE @IBAN VarChar(32);
	DECLARE @BillingAddressID Int;
	DECLARE @BankAccountID Int;

	DECLARE @NamePrefix NVarChar(16);
	DECLARE @FirstName NVarChar(128);
	DECLARE @LastName NVarChar(128);
	DECLARE @NameSuffix NVarChar(16);
	DECLARE @SiteIDHome Int;
	DECLARE @SiteIDBilling Int;
	DECLARE @SiteIDShipping Int;
	DECLARE @SiteID Int;
		
	DECLARE Customers CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
		SELECT
			 Person_ID
			,Name_Prefix
			,First_Name
			,Last_Name
			,Name_Suffix
			,Site_ID_Home
			,Site_ID_Billing
			,Site_ID_Shipping
		FROM CRM.Persons
		WHERE First_Name LIKE @NamePattern OR Last_Name LIKE @NamePattern
			OR Nicknames LIKE @NamePattern;
        
    DECLARE @RowCount SmallInt = 0;
	OPEN Customers;
	FETCH NEXT FROM Customers INTO @PersonID, @NamePrefix, @FirstName, @LastName, @NameSuffix, @SiteIDHome, @SiteIDBilling, @SiteIDShipping;
	WHILE (@@FETCH_STATUS = 0 AND @RowCount < @MaxRows)
		BEGIN
            SET @RowCount = @RowCount + 1;
			SET @CustomerName = ISNULL(@NamePrefix, '') + ' ' + @FirstName + ' ' + @LastName + ' ' + ISNULL(@NameSuffix, '');
			SET @CustomerName = LTRIM(RTRIM(@CustomerName));
            SET @BillingAddressID = @SiteIDBilling;
			SET @SiteID = @SiteIDBilling;
			IF (@SiteID IS NULL) SET @SiteID = @SiteIDHome;
			IF (@SiteID IS NULL) SET @SiteID = @SiteIDShipping;
			IF (@SiteID IS NOT NULL)
				SELECT TOP 1 @City = City, @StateProvince = State_Province, @StreetAddress1 = Street_Address_1
				    FROM CRM.Pers_Sites WHERE Site_ID = @SiteID;
			SELECT TOP 1 @RegistrationNumber = Registration_Number, @TaxID = Tax_ID
				FROM Billing.Customer_Settings WHERE Person_ID = @PersonID;
			IF (@TaxID IS NULL)
				SELECT TOP 1 @RegistrationNumber = Customer_Registration_Number, @TaxID = Customer_Tax_ID
				    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Person_ID = @PersonID ORDER BY Invoice_ID DESC;
			IF (@Currency IS NULL)
				BEGIN
					SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
						FROM Billing.Customer_Accounts WHERE Person_ID = @PersonID;
					IF (@IBAN IS NULL)
						SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
						    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Person_ID = @PersonID;
				END
			ELSE
				BEGIN
					SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
						FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Person_ID = @PersonID;
					IF (@IBAN IS NULL)
						SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
						    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Currency = @Currency AND Person_ID = @PersonID;
				END
			INSERT INTO @SearchResults
				(
					 Organization_ID
					,Person_ID
					,Customer_Name
					,City
					,State_Province
					,Street_Address_1
					,Registration_Number
					,Tax_ID
					,Bank
					,IBAN
                    ,Billing_Address_ID
                    ,Bank_Account_ID
				)
			VALUES
				(
					 NULL
					,@PersonID
					,@CustomerName
					,@City
					,@StateProvince
					,@StreetAddress1
					,@RegistrationNumber
					,@TaxID
					,@Bank
					,@IBAN
			        ,@BillingAddressID
			        ,@BankAccountID
				);
		    SET @OrganizationID = NULL;
		    SET @PersonID = NULL;
		    SET @CustomerName = NULL;
		    SET @City = NULL;
		    SET @StateProvince = NULL;
		    SET @StreetAddress1 = NULL;
		    SET @RegistrationNumber = NULL;
		    SET @TaxID = NULL;
		    SET @Bank = NULL;
		    SET @IBAN = NULL;
		    SET @NamePrefix = NULL;
		    SET @FirstName = NULL;
		    SET @LastName = NULL;
		    SET @NameSuffix = NULL;
		    SET @SiteIDHome = NULL;
		    SET @SiteIDBilling = NULL;
		    SET @SiteIDShipping = NULL;
		    SET @SiteID = NULL;
			SET @BillingAddressID = NULL;
			SET @BankAccountID = NULL;
			FETCH NEXT FROM Customers INTO @PersonID, @NamePrefix, @FirstName, @LastName, @NameSuffix, @SiteIDHome, @SiteIDBilling, @SiteIDShipping;
		END
	CLOSE Customers;
	DEALLOCATE Customers;
	
	SELECT * FROM @SearchResults;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Billing_Details_Lookup_Settings')
	DROP PROCEDURE Billing.Customer_Billing_Details_Lookup_Settings
go
CREATE PROCEDURE Billing.Customer_Billing_Details_Lookup_Settings
(
	 @SearchPattern VarChar(32)
	,@Currency Char(3) = NULL
	,@MaxRows SmallInt = 10
)
AS
	DECLARE @SearchResults TABLE
		(
			 Organization_ID Int
			,Person_ID Int
			,Customer_Name NVarChar(256)
			,City NVarChar(64)
			,State_Province NVarChar(128)
			,Street_Address_1 NVarChar(128)
			,Registration_Number VarChar(32)
			,Tax_ID VarChar(16)
			,Bank NVarChar(128)
			,IBAN VarChar(32)
            ,Billing_Address_ID Int
            ,Bank_Account_ID Int
		);
	
	DECLARE @OrganizationID Int;
	DECLARE @PersonID Int;
	DECLARE @CustomerName NVarChar(256);
	DECLARE @City NVarChar(64);
	DECLARE @StateProvince NVarChar(128);
	DECLARE @StreetAddress1 NVarChar(128);
	DECLARE @RegistrationNumber VarChar(32);
	DECLARE @TaxID VarChar(16);
	DECLARE @Bank NVarChar(128);
	DECLARE @IBAN VarChar(32);
	DECLARE @BillingAddressID Int;
	DECLARE @BankAccountID Int;

	DECLARE @NamePrefix NVarChar(16);
	DECLARE @OrgName NVarChar(128);
	DECLARE @FirstName NVarChar(128);
	DECLARE @LastName NVarChar(128);
	DECLARE @NameSuffix NVarChar(16);
	DECLARE @SiteIDHome Int;
	DECLARE @SiteIDBilling Int;
	DECLARE @SiteIDShipping Int;
	DECLARE @SiteID Int;
		
	DECLARE Settings CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
		SELECT
			 Organization_ID
			,Person_ID
			,Registration_Number
			,Tax_ID
		FROM Billing.Customer_Settings
		WHERE Registration_Number LIKE @SearchPattern OR Tax_ID LIKE @SearchPattern;
        
    DECLARE @RowCount SmallInt = 0;
	OPEN Settings;
	FETCH NEXT FROM Settings INTO @OrganizationID, @PersonID, @RegistrationNumber, @TaxID;
	WHILE (@@FETCH_STATUS = 0 AND @RowCount < @MaxRows)
		BEGIN
            SET @RowCount = @RowCount + 1;
			IF (@PersonID IS NOT NULL)
				BEGIN
					SELECT TOP 1 
							 @NamePrefix = Name_Prefix
							,@FirstName = First_Name
							,@LastName = Last_Name
							,@NameSuffix = Name_Suffix
							,@SiteIDHome = Site_ID_Home
							,@SiteIDBilling = Site_ID_Billing
							,@SiteIDShipping = Site_ID_Shipping
						FROM CRM.Persons WHERE Person_ID = @PersonID;
					SET @CustomerName = ISNULL(@NamePrefix, '') + ' ' + @FirstName + ' ' + @LastName + ' ' + ISNULL(@NameSuffix, '');
					SET @CustomerName = LTRIM(RTRIM(@CustomerName));
                    SET @BillingAddressID = @SiteIDBilling;
					SET @SiteID = @SiteIDBilling;
					IF (@SiteID IS NULL) SET @SiteID = @SiteIDHome;
					IF (@SiteID IS NULL) SET @SiteID = @SiteIDShipping;
					IF (@SiteID IS NOT NULL)
						SELECT TOP 1 @City = City, @StateProvince = State_Province, @StreetAddress1 = Street_Address_1
						    FROM CRM.Pers_Sites WHERE Site_ID = @SiteID;
                    IF (@City IS NULL)
                        SELECT TOP 1 @City = Customer_City, @StateProvince = Customer_Province, @StreetAddress1 = Customer_Address
						    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Person_ID = @PersonID;
					IF (@Currency IS NULL)
						BEGIN
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Person_ID = @PersonID;
							IF (@IBAN IS NULL)
								SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
								    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Person_ID = @PersonID;
						END
					ELSE
						BEGIN
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Person_ID = @PersonID;
							IF (@IBAN IS NULL)
								SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
								    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Currency = @Currency AND Person_ID = @PersonID;
						END
					INSERT INTO @SearchResults
						(
							 Organization_ID
							,Person_ID
							,Customer_Name
							,City
							,State_Province
							,Street_Address_1
							,Registration_Number
							,Tax_ID
							,Bank
							,IBAN
                            ,Billing_Address_ID
                            ,Bank_Account_ID
						)
					VALUES
						(
							 NULL
							,@PersonID
							,@CustomerName
							,@City
							,@StateProvince
							,@StreetAddress1
							,@RegistrationNumber
							,@TaxID
							,@Bank
							,@IBAN
			                ,@BillingAddressID
			                ,@BankAccountID
						);
				END
			IF (@OrganizationID IS NOT NULL)
				BEGIN
					SELECT TOP 1 
							 @NamePrefix = Name_Prefix
							,@OrgName = Org_Name
							,@NameSuffix = Name_Suffix
							,@SiteIDHome = Site_ID_Headquarter
							,@SiteIDBilling = Site_ID_Billing
							,@SiteIDShipping = Site_ID_Shipping
						FROM CRM.Organizations WHERE Organization_ID = @OrganizationID;
					SET @CustomerName = ISNULL(@NamePrefix, '') + ' ' + @OrgName + ' ' + ISNULL(@NameSuffix, '');
					SET @CustomerName = LTRIM(RTRIM(@CustomerName));
                    SET @BillingAddressID = @SiteIDBilling;
					SET @SiteID = @SiteIDBilling;
					IF (@SiteID IS NULL) SET @SiteID = @SiteIDHome;
					IF (@SiteID IS NULL) SET @SiteID = @SiteIDShipping;
					IF (@SiteID IS NOT NULL)
						SELECT TOP 1 @City = City, @StateProvince = State_Province, @StreetAddress1 = Street_Address_1
						    FROM CRM.Orgs_Sites WHERE Site_ID = @SiteID;
                    IF (@City IS NULL)
                        SELECT TOP 1 @City = Customer_City, @StateProvince = Customer_Province, @StreetAddress1 = Customer_Address
						    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Organization_ID = @OrganizationID;
					IF (@Currency IS NULL)
						BEGIN
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Organization_ID = @OrganizationID;
							IF (@IBAN IS NULL)
								SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
								    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Organization_ID = @OrganizationID;
						END
					ELSE
						BEGIN
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Organization_ID = @OrganizationID;
							IF (@IBAN IS NULL)
								SELECT TOP 1 @Bank = Customer_Bank_Name, @IBAN = Customer_Bank_Account
								    FROM Billing.Invoices WHERE Invoice_Status_ID IN (40, 60, 100) AND Currency = @Currency AND Organization_ID = @OrganizationID;
						END
					INSERT INTO @SearchResults
						(
							 Organization_ID
							,Person_ID
							,Customer_Name
							,City
							,State_Province
							,Street_Address_1
							,Registration_Number
							,Tax_ID
							,Bank
							,IBAN
                            ,Billing_Address_ID
                            ,Bank_Account_ID
						)
					VALUES
						(
							 @OrganizationID
							,NULL
							,@CustomerName
							,@City
							,@StateProvince
							,@StreetAddress1
							,@RegistrationNumber
							,@TaxID
							,@Bank
							,@IBAN
			                ,@BillingAddressID
			                ,@BankAccountID
						);
				END
		    SET @OrganizationID = NULL;
		    SET @PersonID = NULL;
		    SET @CustomerName = NULL;
		    SET @City = NULL;
		    SET @StateProvince = NULL;
		    SET @StreetAddress1 = NULL;
		    SET @RegistrationNumber = NULL;
		    SET @TaxID = NULL;
		    SET @Bank = NULL;
		    SET @IBAN = NULL;
		    SET @NamePrefix = NULL;
		    SET @OrgName = NULL;
		    SET @FirstName = NULL;
		    SET @LastName = NULL;
		    SET @NameSuffix = NULL;
		    SET @SiteIDHome = NULL;
		    SET @SiteIDBilling = NULL;
		    SET @SiteIDShipping = NULL;
		    SET @SiteID = NULL;
			SET @BillingAddressID = NULL;
			SET @BankAccountID = NULL;
			FETCH NEXT FROM Settings INTO @OrganizationID, @PersonID, @RegistrationNumber, @TaxID;
		END
	CLOSE Settings;
	DEALLOCATE Settings;
	
	SELECT * FROM @SearchResults;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Billing_Details_Lookup_Invoices')
	DROP PROCEDURE Billing.Customer_Billing_Details_Lookup_Invoices
go
CREATE PROCEDURE Billing.Customer_Billing_Details_Lookup_Invoices
(
	 @SearchPattern NVarChar(128)
	,@Currency Char(3) = NULL
	,@MaxRows SmallInt = 10
)
AS
	DECLARE @SearchResults TABLE
		(
			 Organization_ID Int
			,Person_ID Int
			,Customer_Name NVarChar(256)
			,City NVarChar(64)
			,State_Province NVarChar(128)
			,Street_Address_1 NVarChar(128)
			,Registration_Number VarChar(32)
			,Tax_ID VarChar(16)
			,Bank NVarChar(128)
			,IBAN VarChar(32)
            ,Billing_Address_ID Int
            ,Bank_Account_ID Int
		);
	
	DECLARE @OrganizationID Int;
	DECLARE @PersonID Int;
	DECLARE @CustomerName NVarChar(256);
	DECLARE @City NVarChar(64);
	DECLARE @StateProvince NVarChar(128);
	DECLARE @StreetAddress1 NVarChar(128);
	DECLARE @RegistrationNumber VarChar(32);
	DECLARE @TaxID VarChar(16);
	DECLARE @Bank NVarChar(128);
	DECLARE @IBAN VarChar(32);
	DECLARE @BillingAddressID Int;
	DECLARE @BankAccountID Int;
		
	DECLARE Invoices CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
		SELECT DISTINCT
			 Organization_ID
			,Person_ID
			,Customer_Name
			,Customer_City
			,Customer_Province
			,Customer_Address
			,Customer_Registration_Number
			,Customer_Tax_ID
			,Customer_Bank_Name
			,Customer_Bank_Account
		FROM Billing.Invoices
		WHERE Invoice_Status_ID IN (40, 60, 100) AND		-- Validated, Pending, or Closed
			(
            Accounting_ID LIKE @SearchPattern OR
			Customer_Name LIKE @SearchPattern OR
			Customer_Registration_Number LIKE @SearchPattern OR
			Customer_Tax_ID LIKE @SearchPattern
			);
            
    DECLARE @RowCount SmallInt = 0;
	OPEN Invoices;
	FETCH NEXT FROM Invoices INTO
		@OrganizationID, @PersonID, @CustomerName, @City, @StateProvince, @StreetAddress1,
		@RegistrationNumber, @TaxID, @Bank, @IBAN;
	WHILE (@@FETCH_STATUS = 0 AND @RowCount < @MaxRows)
		BEGIN
            SET @RowCount = @RowCount + 1;
			IF (@PersonID IS NOT NULL)
				BEGIN
					IF (@TaxID IS NULL)
						SELECT TOP 1 @RegistrationNumber = Registration_Number, @TaxID = Tax_ID
						FROM Billing.Customer_Settings WHERE Person_ID = @PersonID;
					IF (@IBAN IS NULL)
						IF (@Currency IS NULL)
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Person_ID = @PersonID;
						ELSE
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Person_ID = @PersonID;
				END
			IF (@OrganizationID IS NOT NULL)
				BEGIN
					IF (@TaxID IS NULL)
						SELECT TOP 1 @RegistrationNumber = Registration_Number, @TaxID = Tax_ID
						FROM Billing.Customer_Settings WHERE Organization_ID = @OrganizationID;
					IF (@IBAN IS NULL)
						IF (@Currency IS NULL)
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Organization_ID = @OrganizationID;
						ELSE
							SELECT TOP 1 @BankAccountID = Account_ID, @Bank = Bank, @IBAN = IBAN
								FROM Billing.Customer_Accounts WHERE Currency = @Currency AND Organization_ID = @OrganizationID;
				END
			INSERT INTO @SearchResults
				(
					 Organization_ID
					,Person_ID
					,Customer_Name
					,City
					,State_Province
					,Street_Address_1
					,Registration_Number
					,Tax_ID
					,Bank
					,IBAN
                    ,Billing_Address_ID
                    ,Bank_Account_ID
				)
			VALUES
				(
					 @OrganizationID
					,@PersonID
					,@CustomerName
					,@City
					,@StateProvince
					,@StreetAddress1
					,@RegistrationNumber
					,@TaxID
					,@Bank
					,@IBAN
			        ,@BillingAddressID
			        ,@BankAccountID
				);
		    SET @OrganizationID = NULL;
		    SET @PersonID = NULL;
		    SET @CustomerName = NULL;
		    SET @City = NULL;
		    SET @StateProvince = NULL;
		    SET @StreetAddress1 = NULL;
		    SET @RegistrationNumber = NULL;
		    SET @TaxID = NULL;
		    SET @Bank = NULL;
		    SET @IBAN = NULL;
			SET @BillingAddressID = NULL;
			SET @BankAccountID = NULL;
			FETCH NEXT FROM Invoices INTO
				@OrganizationID, @PersonID, @CustomerName, @City, @StateProvince, @StreetAddress1,
				@RegistrationNumber, @TaxID, @Bank, @IBAN;
		END
	CLOSE Invoices;
	DEALLOCATE Invoices;
	
	SELECT * FROM @SearchResults;
go
