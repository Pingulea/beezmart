﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Transport_Grid_1')
	DROP PROCEDURE Billing.Handover_Transport_Grid_1
go
CREATE PROCEDURE Billing.Handover_Transport_Grid_1
AS
	SELECT
		Record_ID				-- SmallInt
		,Means					-- NVarChar(16) NOT NULL
		,Registration			-- NVarChar(16) NULL
		,Updated_On				-- SmallDateTime = NULL
		,Updated_By				-- NVarChar(128) = NULL
	FROM Billing.Handover_Transport
	ORDER BY Means, Registration DESC
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Transport_Sel_1')
	DROP PROCEDURE Billing.Handover_Transport_Sel_1
go
CREATE PROCEDURE Billing.Handover_Transport_Sel_1
	(
	@RecordID SmallInt
	)
AS
	SELECT TOP 1
		Record_ID				-- SmallInt
		,Means					-- NVarChar(16) NOT NULL
		,Registration			-- NVarChar(16) NULL
		,Updated_On				-- SmallDateTime = NULL
		,Updated_By				-- NVarChar(128) = NULL
	FROM Billing.Handover_Transport
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Transport_Ins_1')
	DROP PROCEDURE Billing.Handover_Transport_Ins_1
go
CREATE PROCEDURE Billing.Handover_Transport_Ins_1
	(
	@RecordID SmallInt OUTPUT
	,@Means NVarChar(16)
	,@Registration NVarChar(16) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Billing.Handover_Transport
		(
		Means
		,Registration

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@Means
		,@Registration

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @RecordID = SCOPE_IDENTITY()

	SELECT CAST(@RecordID AS SmallInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Transport_Upd_1')
	DROP PROCEDURE Billing.Handover_Transport_Upd_1
go
CREATE PROCEDURE Billing.Handover_Transport_Upd_1
	(
	@RecordID SmallInt
	,@Means NVarChar(16)
	,@Registration NVarChar(16) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Handover_Transport
	SET 
		Means = @Means
		,Registration = @Registration

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Transport_Del_1')
	DROP PROCEDURE Billing.Handover_Transport_Del_1
go
CREATE PROCEDURE Billing.Handover_Transport_Del_1
	(
	@RecordID SmallInt
	)
AS
	DELETE FROM Billing.Handover_Transport
	WHERE Record_ID = @RecordID
go