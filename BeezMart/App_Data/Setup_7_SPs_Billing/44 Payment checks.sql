﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payment_Checks_Ins')
	DROP PROCEDURE Billing.Payment_Checks_Ins
go
CREATE PROCEDURE Billing.Payment_Checks_Ins
(
	@CheckID Int OUTPUT
	,@Reference NVarChar(128)
	,@Amount Decimal(24, 8) = 0
	,@Currency Char(3) = 'EUR'
	,@DueDate SmallDateTime
	,@IssueDate SmallDateTime
	,@CashedDate SmallDateTime = NULL
	,@Comments NVarChar(MAX) = NULL
	,@Type VarChar(8)
	,@CustomerName NVarChar(256)
	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@BankName NVarChar(128) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Billing.Payment_Checks
		(
		Reference
		,Amount
		,Currency
		,Due_Date
		,Issue_Date
		,Cashed_Date
		,Comments
		,Check_Type
		,Customer_Name
		,Organization_ID
		,Person_ID
		,Bank_Name

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@Reference
		,@Amount
		,@Currency
		,@DueDate
		,@IssueDate
		,@CashedDate
		,@Comments
		,@Type
		,@CustomerName
		,@OrganizationID
		,@PersonID
		,@BankName

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @CheckID = SCOPE_IDENTITY()

	SELECT CAST(@CheckID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payment_Checks_Upd')
	DROP PROCEDURE Billing.Payment_Checks_Upd
go
CREATE PROCEDURE Billing.Payment_Checks_Upd
(
	@CheckID Int
	,@Reference NVarChar(128)
	,@Amount Decimal(24, 8) = 0
	,@Currency Char(3) = 'EUR'
	,@DueDate SmallDateTime
	,@IssueDate SmallDateTime
	,@CashedDate SmallDateTime = NULL
	,@Comments NVarChar(MAX) = NULL
	,@Type VarChar(8)
	,@CustomerName NVarChar(256)
	,@OrganizationID Int = NULL
	,@PersonID Int = NULL
	,@BankName NVarChar(128) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Payment_Checks
	SET
		Reference = @Reference
		,Amount = @Amount
		,Currency = @Currency
		,Due_Date = @DueDate
		,Issue_Date = @IssueDate
		,Cashed_Date = @CashedDate
		,Comments = @Comments
		,Check_Type = @Type
		,Customer_Name = @CustomerName
		,Organization_ID = @OrganizationID
		,Person_ID = @PersonID
		,Bank_Name = @BankName

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Check_ID = @CheckID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payment_Checks_Sel')
	DROP PROCEDURE Billing.Payment_Checks_Sel
go
CREATE PROCEDURE Billing.Payment_Checks_Sel
(
	@CheckID Int
)
AS
	SELECT TOP 1
		Check_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Reference					-- NVarChar(128) NOT NULL
		,Amount						-- Decimal(24, 8) NOT NULL DEFAULT 0
		,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
		,Due_Date					-- SmallDateTime NOT NULL
		,Issue_Date					-- SmallDateTime NOT NULL
		,Cashed_Date				-- SmallDateTime NULL
		,Comments					-- NVarChar(MAX) NULL
		,Check_Type					-- VarChar(8) NOT NULL
		,Customer_Name				-- NVarChar(256) NOT NULL
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Bank_Name					-- NVarChar(128) NULL
									
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Billing.Payment_Checks
	WHERE Check_ID = @CheckID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payment_Checks_Grid_1')
	DROP PROCEDURE Billing.Payment_Checks_Grid_1
go
CREATE PROCEDURE Billing.Payment_Checks_Grid_1
AS
	SELECT
		Check_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Reference					-- NVarChar(128) NOT NULL
		,Amount						-- Decimal(24, 8) NOT NULL DEFAULT 0
		,Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
		,Due_Date					-- SmallDateTime NOT NULL
		,Issue_Date					-- SmallDateTime NOT NULL
		,Cashed_Date				-- SmallDateTime NULL
		,Comments					-- NVarChar(MAX) NULL
		,Check_Type					-- VarChar(8) NOT NULL
		,Customer_Name				-- NVarChar(256) NOT NULL
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Bank_Name					-- NVarChar(128) NULL
									
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Billing.Payment_Checks
	WHERE Cashed_Date IS NULL
	ORDER BY Due_Date
go


