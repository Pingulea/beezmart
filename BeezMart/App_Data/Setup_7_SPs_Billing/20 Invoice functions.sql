﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Scalar_Function' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Total_Value')
	DROP FUNCTION Billing.Invoice_Total_Value
go
CREATE FUNCTION Billing.Invoice_Total_Value
(
	@InvoiceID Int
)
RETURNS DECIMAL(27,8)
AS
BEGIN
	DECLARE @RetVal DECIMAL(27,8)
	SET @RetVal = (SELECT SUM(Total_Price) FROM Billing.Invoice_Items I WHERE I.Invoice_ID = @InvoiceID)
	RETURN ISNULL(@RetVal, 0)
END
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Scalar_Function' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Invoice_Total_Payed')
	DROP FUNCTION Billing.Invoice_Total_Payed
go
CREATE FUNCTION Billing.Invoice_Total_Payed
	(
	@InvoiceID Int
	)
RETURNS DECIMAL(27,8)
AS
BEGIN
	DECLARE @RetVal DECIMAL(27,8)
	SET @RetVal = (SELECT SUM(Amount) FROM Billing.Payments I WHERE I.Invoice_ID = @InvoiceID)
	RETURN ISNULL(@RetVal, 0)
END
go