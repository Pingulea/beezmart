﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Dashboard_Statistics_1')
    DROP PROCEDURE Billing.Dashboard_Statistics_1
go
CREATE PROCEDURE Billing.Dashboard_Statistics_1
    @Currency Char(3)
AS
    DECLARE @ValidatedInvoicesCount Int = 0;
    DECLARE @ValidatedInvoicesValue Decimal(27,8) = 0;
    DECLARE @ValidatedInvoicesPayed Decimal(27,8) = 0;
    DECLARE @PendingInvoicesCount Int = 0;
    DECLARE @PendingInvoicesValue Decimal(27,8) = 0;
    DECLARE @PendingInvoicesPayed Decimal(27,8) = 0;
    DECLARE @OverdueInvoicesCount Int = 0;
    DECLARE @OverdueInvoicesValue Decimal(27,8) = 0;
    DECLARE @OverdueInvoicesPayed Decimal(27,8) = 0;

    DECLARE @InvoiceID Int;

    DECLARE Cursor_Invoices CURSOR FAST_FORWARD READ_ONLY FOR
        SELECT Invoice_ID FROM Billing.Invoices
        WHERE Currency = @Currency
            AND Invoice_Status_ID = 40            -- VALIDATED
            AND Invoice_Type_ID IN (20, 60, 100);            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
    OPEN Cursor_Invoices;
    FETCH NEXT FROM Cursor_Invoices INTO @InvoiceID;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @ValidatedInvoicesCount = @ValidatedInvoicesCount + 1;
            SET @ValidatedInvoicesValue = @ValidatedInvoicesValue + Billing.Invoice_Total_Value(@InvoiceID);
            SET @ValidatedInvoicesPayed = @ValidatedInvoicesPayed + Billing.Invoice_Total_Payed(@InvoiceID);
            FETCH NEXT FROM Cursor_Invoices INTO @InvoiceID;
        END
    CLOSE Cursor_Invoices;
    DEALLOCATE Cursor_Invoices;

    DECLARE Cursor_Invoices CURSOR FAST_FORWARD READ_ONLY FOR
        SELECT Invoice_ID FROM Billing.Invoices
        WHERE Currency = @Currency
            AND Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100);            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
    OPEN Cursor_Invoices;
    FETCH NEXT FROM Cursor_Invoices INTO @InvoiceID;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @PendingInvoicesCount = @PendingInvoicesCount + 1;
            SET @PendingInvoicesValue = @PendingInvoicesValue + Billing.Invoice_Total_Value(@InvoiceID);
            SET @PendingInvoicesPayed = @PendingInvoicesPayed + Billing.Invoice_Total_Payed(@InvoiceID);
            FETCH NEXT FROM Cursor_Invoices INTO @InvoiceID;
        END
    CLOSE Cursor_Invoices;
    DEALLOCATE Cursor_Invoices;

    DECLARE Cursor_Invoices CURSOR FAST_FORWARD READ_ONLY FOR
        SELECT Invoice_ID FROM Billing.Invoices
        WHERE Currency = @Currency
            AND Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100)            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
            AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE();            -- OVERDUE: DUE DATE IS PAST
    OPEN Cursor_Invoices;
    FETCH NEXT FROM Cursor_Invoices INTO @InvoiceID;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @OverdueInvoicesCount = @OverdueInvoicesCount + 1;
            SET @OverdueInvoicesValue = @OverdueInvoicesValue + Billing.Invoice_Total_Value(@InvoiceID);
            SET @OverdueInvoicesPayed = @OverdueInvoicesPayed + Billing.Invoice_Total_Payed(@InvoiceID);
            FETCH NEXT FROM Cursor_Invoices INTO @InvoiceID;
        END
    CLOSE Cursor_Invoices;
    DEALLOCATE Cursor_Invoices;

    SELECT
         @ValidatedInvoicesCount            AS Validated_Invoices_Count         -- Int = 0
        ,@ValidatedInvoicesValue            AS Validated_Invoices_Value         -- Decimal(27,8) = 0
        ,@ValidatedInvoicesPayed            AS Validated_Invoices_Payed         -- Decimal(27,8) = 0
        ,@PendingInvoicesCount              AS Pending_Invoices_Count           -- Int = 0
        ,@PendingInvoicesValue              AS Pending_Invoices_Value           -- Decimal(27,8) = 0
        ,@PendingInvoicesPayed              AS Pending_Invoices_Payed           -- Decimal(27,8) = 0
        ,@OverdueInvoicesCount              AS Overdue_Invoices_Count           -- Int = 0
        ,@OverdueInvoicesValue              AS Overdue_Invoices_Value           -- Decimal(27,8) = 0
        ,@OverdueInvoicesPayed              AS Overdue_Invoices_Payed           -- Decimal(27,8) = 0
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Dashboard_Statistics_2')
    DROP PROCEDURE Billing.Dashboard_Statistics_2
go
CREATE PROCEDURE Billing.Dashboard_Statistics_2
AS
    DECLARE @OverdueInvoicesCount Int = 0;
    DECLARE @OverdueCrmCustomersCount Int = 0;

    DECLARE @OrganizationsCount Int = 0;
    DECLARE @PersonsCount Int = 0;

    SET @OverdueInvoicesCount = 
        (
        SELECT COUNT(Invoice_ID) FROM Billing.Invoices
        WHERE Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100)            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
            AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()            -- OVERDUE: DUE DATE IS PAST
        );
    SET @OrganizationsCount = 
        (
        SELECT COUNT(DISTINCT(Organization_ID)) FROM Billing.Invoices
        WHERE Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100)            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
            AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()            -- OVERDUE: DUE DATE IS PAST
            AND Organization_ID IS NOT NULL
        );
    SET @PersonsCount = 
        (
        SELECT COUNT(DISTINCT(Person_ID)) FROM Billing.Invoices
        WHERE Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100)            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
            AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()            -- OVERDUE: DUE DATE IS PAST
            AND Person_ID IS NOT NULL
        );
    SET @OverdueCrmCustomersCount = @OrganizationsCount + @PersonsCount;

    SELECT
         @OverdueInvoicesCount              AS Overdue_Invoices_Count           -- Int = 0
        ,@OverdueCrmCustomersCount          AS Overdue_CRM_Customers_Count      -- Int = 0
go






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Dashboard_Statistics_2a')
    DROP PROCEDURE Billing.Dashboard_Statistics_2a
go
CREATE PROCEDURE Billing.Dashboard_Statistics_2a
AS
    WITH Overdue_Invoices AS
    (
        SELECT Invoice_ID, Issue_Date, Accounting_ID, Organization_ID
        FROM Billing.Invoices
        WHERE Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100)            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
            AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()            -- OVERDUE: DUE DATE IS PAST
            AND Organization_ID IS NOT NULL
    )
    SELECT
		Organization_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,O.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,Segment_Name					-- NVarChar(256) NOT NULL
		,Parent_Org_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Org_Name						-- NVarChar(256) NOT NULL
		,Name_Prefix					-- NVarChar(16) NULL
		,Name_Suffix					-- NVarChar(16) NULL
		,Alternate_Names				-- NVarChar(1024) NULL
		,Country_Of_Origin				-- Char(3) NOT NULL DEFAULT 'ROM'
		,Site_ID_Headquarter			-- Int NULL
		,Site_ID_Billing				-- Int NULL
		,Site_ID_Shipping				-- Int NULL
		,Updated_On						-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By						-- NVarChar(128) NULL
        ,Invoices_Count = (SELECT COUNT(*) FROM Overdue_Invoices I WHERE O.Organization_ID = I.Organization_ID)
	FROM CRM.Organizations O
		INNER JOIN CRM.Orgs_Segment_Names S ON O.Segment_ID = S.Segment_ID
	WHERE Organization_ID IN (SELECT DISTINCT Organization_ID FROM Overdue_Invoices);
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Dashboard_Statistics_2b')
    DROP PROCEDURE Billing.Dashboard_Statistics_2b
go
CREATE PROCEDURE Billing.Dashboard_Statistics_2b
AS
    WITH Overdue_Invoices AS
    (
        SELECT Invoice_ID, Issue_Date, Accounting_ID, Person_ID
        FROM Billing.Invoices
        WHERE Invoice_Status_ID = 60            -- SENT TO CUSTOMER
            AND Invoice_Type_ID IN (20, 60, 100)            -- REGULAR = 20, ADVANCE = 60, PRO FORMA = 100
            AND ISNULL(Due_Date, DATEADD(MONTH, 1, GETDATE())) < GETDATE()            -- OVERDUE: DUE DATE IS PAST
            AND Person_ID IS NOT NULL
    )
    SELECT
		Person_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,P.Segment_ID				-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,Segment_Name				-- NVarChar(256) NOT NULL
		,First_Name					-- NVarChar(64) NOT NULL
		,Last_Name					-- NVarChar(64) NOT NULL
		,Name_Prefix				-- NVarChar(16) NULL
		,Name_Suffix				-- NVarChar(16) NULL
		,Nicknames					-- NVarChar(256) NULL
		,Country_Of_Origin			-- Char(3) NOT NULL DEFAULT 'ROM'
		,Site_ID_Home				-- Int NULL
		,Site_ID_Billing			-- Int NULL
		,Site_ID_Shipping			-- Int NULL
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
        ,Invoices_Count = (SELECT COUNT(*) FROM Overdue_Invoices I WHERE P.Person_ID = I.Person_ID)
	FROM CRM.Persons P
		INNER JOIN CRM.Pers_Segment_Names S ON P.Segment_ID = S.Segment_ID
	WHERE Person_ID IN (SELECT DISTINCT Person_ID FROM Overdue_Invoices);
go