﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Items_Ins')
	DROP PROCEDURE Billing.Invoice_Items_Ins
go
CREATE PROCEDURE Billing.Invoice_Items_Ins
(
	 @ItemID BigInt OUTPUT
	,@InvoiceID Int
	,@ProductCatalogItemID Int = NULL
	,@ProductCatalogCategoryID SmallInt = NULL
	,@ProductOrService Char = 'P'			-- P for products, S for services
	,@SalesOrderID Int = NULL
	,@SalesOrderItemID BigInt = NULL
	,@DeliveryDocumentID Int = NULL
	,@DeliveryDocumentItemID BigInt = NULL

	,@ItemName NVarChar(1024)
	,@UnitPrice Decimal(16, 8) = 0
	,@UnitMeasure NVarChar(16) = NULL
	,@Quantity Int = 1

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Billing.Invoice_Items
		(
		Invoice_ID
		,Product_Catalog_Item_ID
		,Product_Catalog_Category_ID
		,Product_Or_Service
		,Sales_Order_ID
		,Sales_Order_Item_ID
		,Delivery_Document_ID
		,Delivery_Document_Item_ID

		,Item_Name
		,Unit_Price
		,Unit_Measure
		,Quantity
		)
	VALUES
		(
		@InvoiceID
		,@ProductCatalogItemID
		,@ProductCatalogCategoryID
		,@ProductOrService
		,@SalesOrderID
		,@SalesOrderItemID
		,@DeliveryDocumentID
		,@DeliveryDocumentItemID

		,@ItemName
		,@UnitPrice
		,@UnitMeasure
		,@Quantity
		);
	SET @ItemID = SCOPE_IDENTITY();

	UPDATE Billing.Invoices SET Updated_On = @UpdatedOn, Updated_By = @UpdatedBy WHERE Invoice_ID = @InvoiceID;

	SELECT CAST(@ItemID AS BigInt) AS ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Items_Upd')
	DROP PROCEDURE Billing.Invoice_Items_Upd
go
CREATE PROCEDURE Billing.Invoice_Items_Upd
(
	 @ItemID BigInt
	,@InvoiceID Int
	,@ProductCatalogItemID Int = NULL
	,@ProductCatalogCategoryID SmallInt = NULL
	,@ProductOrService Char = 'P'			-- P for products, S for services
	,@SalesOrderID Int = NULL
	,@SalesOrderItemID BigInt = NULL
	,@DeliveryDocumentID Int = NULL
	,@DeliveryDocumentItemID BigInt = NULL

	,@ItemName NVarChar(1024)
	,@UnitPrice Decimal(16, 8) = 0
	,@UnitMeasure NVarChar(16) = NULL
	,@Quantity Int = 1

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
	UPDATE Billing.Invoice_Items
	SET
		Invoice_ID = @InvoiceID
		,Product_Catalog_Item_ID = @ProductCatalogItemID
		,Product_Catalog_Category_ID = @ProductCatalogCategoryID
		,Product_Or_Service = @ProductOrService
		,Sales_Order_ID = @SalesOrderID
		,Sales_Order_Item_ID = @SalesOrderItemID
		,Delivery_Document_ID = @DeliveryDocumentID
		,Delivery_Document_Item_ID = @DeliveryDocumentItemID

		,Item_Name = @ItemName
		,Unit_Price = @UnitPrice
		,Unit_Measure = @UnitMeasure
		,Quantity = @Quantity
	WHERE Item_ID = @ItemID;

	UPDATE Billing.Invoices SET Updated_On = @UpdatedOn, Updated_By = @UpdatedBy WHERE Invoice_ID = @InvoiceID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Items_Sel')
	DROP PROCEDURE Billing.Invoice_Items_Sel
go
CREATE PROCEDURE Billing.Invoice_Items_Sel
(
	@ItemID BigInt
)
AS
	SELECT TOP 1
		 Item_ID									-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Invoice_ID								-- Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Product_Catalog_Item_ID				-- Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
		,Product_Catalog_Category_ID			-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
		,Product_Or_Service						-- Char NULL DEFAULT 'P'			-- P for products, S for services
		,Sales_Order_ID							-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Sales_Order_Item_ID					-- BigInt NULL FOREIGN KEY REFERENCES Sales.Order_Items(Item_ID)
		,Delivery_Document_ID					-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Delivery_Document_Item_ID				-- BigInt NULL FOREIGN KEY REFERENCES Delivery.Document_Items(Item_ID)
												 
		,Item_Name								-- NVarChar(1024) NOT NULL
		,Unit_Price								-- Decimal(16, 8) NOT NULL DEFAULT 0
		,Unit_Measure							-- NVarChar(16) NULL
		,Quantity								-- Int NOT NULL DEFAULT 1
		,Total_Price							-- AS Unit_Price * Quantity
	FROM Billing.Invoice_Items
	WHERE Item_ID = @ItemID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Items_Grid_1')
	DROP PROCEDURE Billing.Invoice_Items_Grid_1
go
CREATE PROCEDURE Billing.Invoice_Items_Grid_1
(
	@InvoiceID Int
)
AS
	SELECT
		 Item_ID									-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Invoice_ID								-- Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Product_Catalog_Item_ID				-- Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
		,Product_Catalog_Category_ID			-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
		,Product_Or_Service						-- Char NULL DEFAULT 'P'			-- P for products, S for services
		,Sales_Order_ID							-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Sales_Order_Item_ID					-- BigInt NULL FOREIGN KEY REFERENCES Sales.Order_Items(Item_ID)
		,Delivery_Document_ID					-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Delivery_Document_Item_ID				-- BigInt NULL FOREIGN KEY REFERENCES Delivery.Document_Items(Item_ID)
												 
		,Item_Name								-- NVarChar(1024) NOT NULL
		,Unit_Price								-- Decimal(16, 8) NOT NULL DEFAULT 0
		,Unit_Measure							-- NVarChar(16) NULL
		,Quantity								-- Int NOT NULL DEFAULT 1
		,Total_Price							-- AS Unit_Price * Quantity
	FROM Billing.Invoice_Items
	WHERE Invoice_ID = @InvoiceID
	ORDER BY Item_Name, Quantity, Item_ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Items_Del')
	DROP PROCEDURE Billing.Invoice_Items_Del
go
CREATE PROCEDURE Billing.Invoice_Items_Del
(
	 @ItemID BigInt
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
	DECLARE @InvoiceID Int;
	SET @InvoiceID = (SELECT TOP 1 Invoice_ID FROM Billing.Invoice_Items WHERE Item_ID = @ItemID);
	DELETE FROM Billing.Invoice_Items WHERE Item_ID = @ItemID;
	UPDATE Billing.Invoices SET Updated_On = @UpdatedOn, Updated_By = @UpdatedBy WHERE Invoice_ID = @InvoiceID;
go


