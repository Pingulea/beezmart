﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments_Ins')
	DROP PROCEDURE Billing.Payments_Ins
go
CREATE PROCEDURE Billing.Payments_Ins
(
	@PaymentID Int OUTPUT
	,@InvoiceID Int
	,@PaymentDate SmallDateTime
	,@Amount Decimal(24, 8) = 0
	,@Currency Char(3) = 'EUR'
	,@Comments NVarChar(MAX) = NULL
	,@PaymentCheckID Int = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @PaymentDate = ISNULL(@PaymentDate, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Billing.Payments
		(
		Invoice_ID
		,Payment_Date
		,Amount
		,Currency
		,Comments
		,Payment_Check_ID

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@InvoiceID
		,@PaymentDate
		,@Amount
		,@Currency
		,@Comments
		,@PaymentCheckID

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @PaymentID = SCOPE_IDENTITY()

	SELECT CAST(@PaymentID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments_Upd')
	DROP PROCEDURE Billing.Payments_Upd
go
CREATE PROCEDURE Billing.Payments_Upd
(
	@PaymentID Int
	,@InvoiceID Int
	,@PaymentDate SmallDateTime
	,@Amount Decimal(24, 8) = 0
	,@Currency Char(3) = 'EUR'
	,@Comments NVarChar(MAX) = NULL
	,@PaymentCheckID Int = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	SET @PaymentDate = ISNULL(@PaymentDate, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Payments
	SET
		Invoice_ID = @InvoiceID
		,Payment_Date = @PaymentDate
		,Amount = @Amount
		,Currency = @Currency
		,Comments = @Comments
		,Payment_Check_ID = @PaymentCheckID

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Payment_ID = @PaymentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments_Sel')
	DROP PROCEDURE Billing.Payments_Sel
go
CREATE PROCEDURE Billing.Payments_Sel
(
	@PaymentID Int
)
AS
	SELECT TOP 1
		P.Payment_ID				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,P.Invoice_ID				-- Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,P.Payment_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Amount					-- Decimal(24, 8) NOT NULL DEFAULT 0
		,P.Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
		,P.Comments					-- NVarChar(MAX) NULL
		,P.Payment_Check_ID			-- Int NULL FOREIGN KEY REFERENCES Billing.Payment_Checks(Check_ID)
									
		,I.Accounting_ID			-- VarChar(32) NULL
		,I.Issue_Date				-- SmallDateTime NOT NULL
		,I.Customer_Name			-- NVarChar(256) NOT NULL
		,I.Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,I.Person_ID				-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
									 
		,P.Updated_On				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Updated_By				-- NVarChar(128) NULL
	FROM Billing.Payments P
		INNER JOIN Billing.Invoices I ON P.Invoice_ID = I.Invoice_ID
	WHERE Payment_ID = @PaymentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments_Grid_1')
	DROP PROCEDURE Billing.Payments_Grid_1
go
CREATE PROCEDURE Billing.Payments_Grid_1
(
	@InvoiceID Int
)
AS
	SELECT
		P.Payment_ID				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,P.Invoice_ID				-- Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,P.Payment_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Amount					-- Decimal(24, 8) NOT NULL DEFAULT 0
		,P.Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
		,P.Comments					-- NVarChar(MAX) NULL
		,P.Payment_Check_ID			-- Int NULL FOREIGN KEY REFERENCES Billing.Payment_Checks(Check_ID)
									
		,I.Accounting_ID			-- VarChar(32) NULL
		,I.Issue_Date				-- SmallDateTime NOT NULL
		,I.Customer_Name			-- NVarChar(256) NOT NULL
		,I.Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,I.Person_ID				-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
									 
		,P.Updated_On				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Updated_By				-- NVarChar(128) NULL
	FROM Billing.Payments P
		INNER JOIN Billing.Invoices I ON P.Invoice_ID = I.Invoice_ID
	WHERE P.Invoice_ID = @InvoiceID
	ORDER BY Payment_Date, Amount, P.Invoice_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments_Grid_2')
	DROP PROCEDURE Billing.Payments_Grid_2
go
CREATE PROCEDURE Billing.Payments_Grid_2
(
	@StartingWith SmallDateTime = NULL
	,@EndingWith SmallDateTime = NULL
)
AS
	SET @StartingWith = ISNULL(@StartingWith, DATEADD(MONTH, -3, GETDATE()))
	SET @EndingWith = ISNULL(@EndingWith, DATEADD(MONTH, 1, GETDATE()))
	SELECT
		P.Payment_ID				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,P.Invoice_ID				-- Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,P.Payment_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Amount					-- Decimal(24, 8) NOT NULL DEFAULT 0
		,P.Currency					-- Char(3) NOT NULL DEFAULT 'EUR'
		,P.Comments					-- NVarChar(MAX) NULL
		,P.Payment_Check_ID			-- Int NULL FOREIGN KEY REFERENCES Billing.Payment_Checks(Check_ID)
									
		,I.Accounting_ID			-- VarChar(32) NULL
		,I.Issue_Date				-- SmallDateTime NOT NULL
		,I.Customer_Name			-- NVarChar(256) NOT NULL
		,I.Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,I.Person_ID				-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
									 
		,P.Updated_On				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Updated_By				-- NVarChar(128) NULL
	FROM Billing.Payments P
		INNER JOIN Billing.Invoices I ON P.Invoice_ID = I.Invoice_ID
	WHERE Payment_Date BETWEEN @StartingWith AND @EndingWith
	ORDER BY Payment_Date, Amount, P.Invoice_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments_Del')
	DROP PROCEDURE Billing.Payments_Del
go
CREATE PROCEDURE Billing.Payments_Del
(
	@PaymentID Int

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
)
AS
	DELETE FROM Billing.Payments
	WHERE Payment_ID = @PaymentID
go

