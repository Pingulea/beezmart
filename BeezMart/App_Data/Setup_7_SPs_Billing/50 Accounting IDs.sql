﻿





--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Accounting_IDs_Ins')
	DROP PROCEDURE Billing.Invoice_Accounting_IDs_Ins
go
CREATE PROCEDURE Billing.Invoice_Accounting_IDs_Ins
(
	@RecordID BigInt OUTPUT
	,@StartingNumber Int
	,@Serial VarChar(8)
	,@InvoiceID Int
	,@AllocatedNumber Int OUTPUT
)
AS
	DECLARE @CurrentMaximumNumber Int
	SET @CurrentMaximumNumber = 
		(
			SELECT TOP 1 Number
			FROM Billing.Invoice_Accounting_IDs
			WHERE Number >= @StartingNumber
				AND Accounting_ID LIKE (@Serial + '%')
			ORDER BY Number DESC
		)
	IF (@CurrentMaximumNumber IS NULL) SET @AllocatedNumber = @StartingNumber
	ELSE SET @AllocatedNumber = @CurrentMaximumNumber + 1
	INSERT INTO Billing.Invoice_Accounting_IDs
		(
		Number
		,Accounting_ID
		,Invoice_ID
		)
	VALUES
		(
		@AllocatedNumber
		,@Serial + ' ' + CAST(@AllocatedNumber AS VarChar(24))
		,@InvoiceID
		)
	SET @RecordID = SCOPE_IDENTITY()
	SELECT CAST(@RecordID AS BigInt) AS ID, CAST(@AllocatedNumber AS Int) AS AllocatedNumber
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Accounting_IDs_Upd')
	DROP PROCEDURE Billing.Invoice_Accounting_IDs_Upd
go
CREATE PROCEDURE Billing.Invoice_Accounting_IDs_Upd
(
	@RecordID BigInt
	,@Number Int
	,@AccountingID VarChar(32)
	,@InvoiceID Int
)
AS
	UPDATE Billing.Invoice_Accounting_IDs
	SET
		Number = @Number
		,Accounting_ID = @AccountingID
		,Invoice_ID = @InvoiceID
	WHERE Record_ID = @RecordID
go