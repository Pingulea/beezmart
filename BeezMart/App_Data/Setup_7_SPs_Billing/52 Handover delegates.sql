﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Delegates_Grid_1')
	DROP PROCEDURE Billing.Handover_Delegates_Grid_1
go
CREATE PROCEDURE Billing.Handover_Delegates_Grid_1
AS
	SELECT
		Record_ID				-- SmallInt
		,Delegate_Name			-- NVarChar(128) NOT NULL
		,Delegate_ID_1			-- NVarChar(8) NULL
		,Delegate_ID_2			-- NVarChar(16) NULL
		,Delegate_ID_3			-- NVarChar(32) NULL

		,Updated_On				-- SmallDateTime = NULL
		,Updated_By				-- NVarChar(128) = NULL
	FROM Billing.Handover_Delegates
	ORDER BY Delegate_Name, Delegate_ID_1 DESC
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Delegates_Sel_1')
	DROP PROCEDURE Billing.Handover_Delegates_Sel_1
go
CREATE PROCEDURE Billing.Handover_Delegates_Sel_1
	(
	@RecordID SmallInt
	)
AS
	SELECT TOP 1
		Record_ID				-- SmallInt
		,Delegate_Name			-- NVarChar(128) NOT NULL
		,Delegate_ID_1			-- NVarChar(8) NULL
		,Delegate_ID_2			-- NVarChar(16) NULL
		,Delegate_ID_3			-- NVarChar(32) NULL

		,Updated_On				-- SmallDateTime = NULL
		,Updated_By				-- NVarChar(128) = NULL
	FROM Billing.Handover_Delegates
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Delegates_Ins_1')
	DROP PROCEDURE Billing.Handover_Delegates_Ins_1
go
CREATE PROCEDURE Billing.Handover_Delegates_Ins_1
	(
	@RecordID SmallInt OUTPUT
	,@DelegateName NVarChar(128)
	,@DelegateID1 NVarChar(8) = NULL
	,@DelegateID2 NVarChar(16) = NULL
	,@DelegateID3 NVarChar(32) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Billing.Handover_Delegates
		(
		Delegate_Name
		,Delegate_ID_1
		,Delegate_ID_2
		,Delegate_ID_3

		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@DelegateName
		,@DelegateID1
		,@DelegateID2
		,@DelegateID3

		,@UpdatedOn
		,@UpdatedBy
		)
	SET @RecordID = SCOPE_IDENTITY()

	SELECT CAST(@RecordID AS SmallInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Delegates_Upd_1')
	DROP PROCEDURE Billing.Handover_Delegates_Upd_1
go
CREATE PROCEDURE Billing.Handover_Delegates_Upd_1
	(
	@RecordID SmallInt
	,@DelegateName NVarChar(128)
	,@DelegateID1 NVarChar(8) = NULL
	,@DelegateID2 NVarChar(16) = NULL
	,@DelegateID3 NVarChar(32) = NULL

	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Billing.Handover_Delegates
	SET 
		Delegate_Name = @DelegateName
		,Delegate_ID_1 = @DelegateID1
		,Delegate_ID_2 = @DelegateID2
		,Delegate_ID_3 = @DelegateID3

		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'Billing' AND Name = N'Handover_Delegates_Del_1')
	DROP PROCEDURE Billing.Handover_Delegates_Del_1
go
CREATE PROCEDURE Billing.Handover_Delegates_Del_1
	(
	@RecordID SmallInt
	)
AS
	DELETE FROM Billing.Handover_Delegates
	WHERE Record_ID = @RecordID
go