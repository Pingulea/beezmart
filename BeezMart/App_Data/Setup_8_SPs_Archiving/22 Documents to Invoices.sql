






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices_Upsert')
	DROP PROCEDURE Archiving.Documents_To_Invoices_Upsert
go
CREATE PROCEDURE Archiving.Documents_To_Invoices_Upsert
	(
	 @RecordID BigInt OUTPUT            -- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@DocumentID BigInt                 -- NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,@InvoiceID Int				-- NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	,@UpdatedOn SmallDateTime = NULL    -- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
    DECLARE @ExistingRecordsCount Int;
    SET @ExistingRecordsCount = (SELECT COUNT(*) FROM Archiving.Documents_To_Invoices WHERE Document_ID = @DocumentID AND Invoice_ID = @InvoiceID);
    IF (@ExistingRecordsCount = 0)
        BEGIN
            INSERT INTO Archiving.Documents_To_Invoices
		            (
			         Document_ID
	                ,Invoice_ID
	                ,Updated_On
	                ,Updated_By
		            )
	            VALUES
		            (
			         @DocumentID
	                ,@InvoiceID
	                ,@UpdatedOn
	                ,@UpdatedBy
		            );
	        SET @RecordID = SCOPE_IDENTITY();	
	        SELECT CAST(@RecordID AS BigInt) AS ID;
            RETURN;
        END
	SET @RecordID = (SELECT TOP 1 Record_ID FROM Archiving.Documents_To_Invoices WHERE Document_ID = @DocumentID AND Invoice_ID = @InvoiceID);
    IF (@ExistingRecordsCount > 1)
        BEGIN
            DELETE FROM Archiving.Documents_To_Invoices
                WHERE Document_ID = @DocumentID
                    AND Invoice_ID = @InvoiceID
                    AND Record_ID != @RecordID;
        END
    SELECT @RecordID AS ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices_Del')
	DROP PROCEDURE Archiving.Documents_To_Invoices_Del
go
CREATE PROCEDURE Archiving.Documents_To_Invoices_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM Archiving.Documents_To_Invoices WHERE Record_ID = @RecordID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices_Del_1')
	DROP PROCEDURE Archiving.Documents_To_Invoices_Del_1
go
CREATE PROCEDURE Archiving.Documents_To_Invoices_Del_1
	(
	 @DocumentID BigInt
	,@InvoiceID Int
	)
AS
	DELETE FROM Archiving.Documents_To_Invoices WHERE Document_ID = @DocumentID AND Invoice_ID = @InvoiceID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices_Sel')
	DROP PROCEDURE Archiving.Documents_To_Invoices_Sel
go
CREATE PROCEDURE Archiving.Documents_To_Invoices_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		 Record_ID                 -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,Document_ID               -- BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	    ,Invoice_ID	       -- Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	    ,Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents_To_Invoices
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices_Grid_1')
	DROP PROCEDURE Archiving.Documents_To_Invoices_Grid_1
go
CREATE PROCEDURE Archiving.Documents_To_Invoices_Grid_1
	(
	@DocumentID BigInt
	)
AS
	SELECT
		 Invoice_ID										-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)

		,Issue_Date										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Due_Date										-- SmallDateTime NULL
		,Closing_Date									-- SmallDateTime NULL
		,Accounting_ID									-- VarChar(32) NULL
		,VAT_Percentage									-- Decimal(6, 4) NOT NULL DEFAULT 0
		,Currency										-- Char(3) NOT NULL DEFAULT 'EUR'

		,Invoice_Type_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
		,Invoice_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)

		,Organization_ID								-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name									-- NVarChar(256) NOT NULL
		,Customer_City									-- NVarChar(64) NULL
		,Customer_Province								-- NVarChar(64) NULL
		,Customer_Address								-- NVarChar(256) NULL
		,Customer_Registration_Number					-- VarChar(32) NULL
		,Customer_Tax_ID								-- VarChar(16) NULL
		,Customer_Bank_Name								-- NVarChar(128) NULL
		,Customer_Bank_Account							-- VarChar(64) NULL

		,Delegate_Name									-- NVarChar(128) NULL
		,Delegate_ID_1									-- NVarChar(8) NULL
		,Delegate_ID_2									-- NVarChar(16) NULL
		,Delegate_ID_3									-- NVarChar(32) NULL

		,Transport_Name									-- NVarChar(32) NULL
		,Transport_Detail_1								-- NVarChar(32) NULL
		,Transport_Detail_2								-- NVarChar(32) NULL
		,Transport_Detail_3								-- NVarChar(32) NULL
		,Transport_Detail_4								-- NVarChar(32) NULL

		,Invoice_Detail_1								-- NVarChar(256) NULL
		,Invoice_Detail_2								-- NVarChar(256) NULL
		,Invoice_Detail_3								-- NVarChar(256) NULL
		,Invoice_Detail_4								-- NVarChar(256) NULL

		,Sales_Order_ID									-- Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
		,Delivery_Document_ID							-- Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
		,Related_Invoice_ID								-- Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
		,Rendering_Layout								-- VarChar(256) NULL

		,Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By										-- NVarChar(128) NULL

		,Billing.Invoice_Total_Value(Invoice_ID)	AS Total_Value		-- Decimal(278)
		,Billing.Invoice_Total_Payed(Invoice_ID)	AS Total_Payed		-- Decimal(278)
	FROM Billing.Invoices
	WHERE Invoice_ID IN (SELECT DISTINCT Invoice_ID FROM Archiving.Documents_To_Invoices WHERE Document_ID = @DocumentID)
    ORDER BY Invoice_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices_Grid_2')
	DROP PROCEDURE Archiving.Documents_To_Invoices_Grid_2
go
CREATE PROCEDURE Archiving.Documents_To_Invoices_Grid_2
	(
	@InvoiceID Int				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	)
AS
	SELECT
		 Document_ID				-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Title						-- NVarChar(256) NOT NULL
		--,Body						-- NVarChar(MAX) NULL
		,External_URL				-- VarChar(256) NULL
		,Document_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Expiring_Date				-- SmallDateTime NULL
		,Closing_Date				-- SmallDateTime NULL
		,External_ID				-- VarChar(128) NULL
		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
		,Document_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
		,Added_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Archiving.Documents
	WHERE Document_ID IN (SELECT DISTINCT Document_ID FROM Archiving.Documents_To_Invoices WHERE Invoice_ID = @InvoiceID)
    ORDER BY Title, Document_ID
go
