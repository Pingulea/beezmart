






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities_Upsert')
	DROP PROCEDURE Archiving.Documents_To_Activities_Upsert
go
CREATE PROCEDURE Archiving.Documents_To_Activities_Upsert
	(
	 @RecordID BigInt OUTPUT            -- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@DocumentID BigInt                 -- NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,@ActivityID BigInt						-- NOT NULL FOREIGN KEY REFERENCES Sales.Activities(Activity_ID)
	,@UpdatedOn SmallDateTime = NULL    -- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
    DECLARE @ExistingRecordsCount Int;
    SET @ExistingRecordsCount = (SELECT COUNT(*) FROM Archiving.Documents_To_Activities WHERE Document_ID = @DocumentID AND Activity_ID = @ActivityID);
    IF (@ExistingRecordsCount = 0)
        BEGIN
            INSERT INTO Archiving.Documents_To_Activities
		            (
			         Document_ID
	                ,Activity_ID
	                ,Updated_On
	                ,Updated_By
		            )
	            VALUES
		            (
			         @DocumentID
	                ,@ActivityID
	                ,@UpdatedOn
	                ,@UpdatedBy
		            );
	        SET @RecordID = SCOPE_IDENTITY();	
	        SELECT CAST(@RecordID AS BigInt) AS ID;
            RETURN;
        END
	SET @RecordID = (SELECT TOP 1 Record_ID FROM Archiving.Documents_To_Activities WHERE Document_ID = @DocumentID AND Activity_ID = @ActivityID);
    IF (@ExistingRecordsCount > 1)
        BEGIN
            DELETE FROM Archiving.Documents_To_Activities
                WHERE Document_ID = @DocumentID
                    AND Activity_ID = @ActivityID
                    AND Record_ID != @RecordID;
        END
    SELECT @RecordID AS ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities_Del')
	DROP PROCEDURE Archiving.Documents_To_Activities_Del
go
CREATE PROCEDURE Archiving.Documents_To_Activities_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM Archiving.Documents_To_Activities WHERE Record_ID = @RecordID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities_Del_1')
	DROP PROCEDURE Archiving.Documents_To_Activities_Del_1
go
CREATE PROCEDURE Archiving.Documents_To_Activities_Del_1
	(
	 @DocumentID BigInt
	,@ActivityID BigInt
	)
AS
	DELETE FROM Archiving.Documents_To_Activities WHERE Document_ID = @DocumentID AND Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities_Sel')
	DROP PROCEDURE Archiving.Documents_To_Activities_Sel
go
CREATE PROCEDURE Archiving.Documents_To_Activities_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		 Record_ID                 -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,Document_ID               -- BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	    ,Activity_ID	       	   -- BigInt NOT NULL FOREIGN KEY REFERENCES Sales.Activities(Activity_ID)
	    ,Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents_To_Activities
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities_Grid_1')
	DROP PROCEDURE Archiving.Documents_To_Activities_Grid_1
go
CREATE PROCEDURE Archiving.Documents_To_Activities_Grid_1
	(
	@DocumentID BigInt
	)
AS
	SELECT
		 A.Activity_ID										-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,A.Project_ID										-- Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
		,A.Organization_ID									-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,A.Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,A.Customer_Name									-- NVarChar(256) NOT NULL
		,A.End_Customer										-- NVarChar(256) NULL

		,P.Title AS Project_Title							-- NVarChar(256) NULL

		,A.Activity_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
		--,A.Activity_Status_Comment							-- NVarChar(256) NULL
		,A.Activity_Type_ID									-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
		,A.Title											-- NVarChar(1024) NOT NULL
		--,A.Details											-- NVarChar(MAX) NULL

		,C.Activity_Type_Name AS Activity_Type				-- NVarChar(64) NULL
		,C.Activity_Type_Category AS Activity_Category		-- NVarChar(64) NULL

		,A.Scheduled_Start									-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Scheduled_End									-- SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
		,A.Assigned_On										-- SmallDateTime NULL
		,A.Assigned_To										-- NVarChar(128) NULL
		,A.Deadline_On										-- SmallDateTime NULL
		,A.Started_On										-- SmallDateTime NULL
		,A.Ended_On											-- SmallDateTime NULL
		,A.Reminder_On										-- SmallDateTime NULL

		,A.Requested_By										-- NVarChar(128) NULL
		,A.Requesting_Department							-- NVarChar(128) NULL
		,A.Completed_By										-- NVarChar(128) NULL
		,A.Logged_By										-- NVarChar(128) NULL
		,A.Billable											-- Bit NOT NULL DEFAULT 0
		,A.Invoice_IDs										-- VarChar(256) NULL
		,A.Duration_In_Minutes								-- Int NULL

		,A.Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Updated_By										-- NVarChar(128) NULL
	FROM Sales.Activities A
        LEFT JOIN Sales.Activity_Types C ON A.Activity_Type_ID = C.Activity_Type_ID
        LEFT JOIN Sales.Projects P ON A.Project_ID = P.Project_ID
	WHERE Activity_ID IN (SELECT DISTINCT Activity_ID FROM Archiving.Documents_To_Activities	WHERE Document_ID = @DocumentID)
    ORDER BY Customer_Name, Activity_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities_Grid_2')
	DROP PROCEDURE Archiving.Documents_To_Activities_Grid_2
go
CREATE PROCEDURE Archiving.Documents_To_Activities_Grid_2
	(
	@ActivityID BigInt				-- BigInt NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	)
AS
	SELECT
		 Document_ID				-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Title						-- NVarChar(256) NOT NULL
		--,Body						-- NVarChar(MAX) NULL
		,External_URL				-- VarChar(256) NULL
		,Document_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Expiring_Date				-- SmallDateTime NULL
		,Closing_Date				-- SmallDateTime NULL
		,External_ID				-- VarChar(128) NULL
		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
		,Document_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
		,Added_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Archiving.Documents
	WHERE Document_ID IN (SELECT DISTINCT Document_ID FROM Archiving.Documents_To_Activities WHERE Activity_ID = @ActivityID)
    ORDER BY Title, Document_ID
go
