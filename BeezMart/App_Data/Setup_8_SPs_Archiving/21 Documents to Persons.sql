






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons_Upsert')
	DROP PROCEDURE Archiving.Documents_To_Persons_Upsert
go
CREATE PROCEDURE Archiving.Documents_To_Persons_Upsert
	(
	 @RecordID BigInt OUTPUT            -- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@DocumentID BigInt                 -- NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,@PersonID Int						-- NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,@UpdatedOn SmallDateTime = NULL    -- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
    DECLARE @ExistingRecordsCount Int;
    SET @ExistingRecordsCount = (SELECT COUNT(*) FROM Archiving.Documents_To_Persons WHERE Document_ID = @DocumentID AND Person_ID = @PersonID);
    IF (@ExistingRecordsCount = 0)
        BEGIN
            INSERT INTO Archiving.Documents_To_Persons
		            (
			         Document_ID
	                ,Person_ID
	                ,Updated_On
	                ,Updated_By
		            )
	            VALUES
		            (
			         @DocumentID
	                ,@PersonID
	                ,@UpdatedOn
	                ,@UpdatedBy
		            );
	        SET @RecordID = SCOPE_IDENTITY();	
	        SELECT CAST(@RecordID AS BigInt) AS ID;
            RETURN;
        END
	SET @RecordID = (SELECT TOP 1 Record_ID FROM Archiving.Documents_To_Persons WHERE Document_ID = @DocumentID AND Person_ID = @PersonID);
    IF (@ExistingRecordsCount > 1)
        BEGIN
            DELETE FROM Archiving.Documents_To_Persons
                WHERE Document_ID = @DocumentID
                    AND Person_ID = @PersonID
                    AND Record_ID != @RecordID;
        END
    SELECT @RecordID AS ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons_Del')
	DROP PROCEDURE Archiving.Documents_To_Persons_Del
go
CREATE PROCEDURE Archiving.Documents_To_Persons_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM Archiving.Documents_To_Persons WHERE Record_ID = @RecordID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons_Del_1')
	DROP PROCEDURE Archiving.Documents_To_Persons_Del_1
go
CREATE PROCEDURE Archiving.Documents_To_Persons_Del_1
	(
	 @DocumentID BigInt
	,@PersonID Int
	)
AS
	DELETE FROM Archiving.Documents_To_Persons WHERE Document_ID = @DocumentID AND Person_ID = @PersonID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons_Sel')
	DROP PROCEDURE Archiving.Documents_To_Persons_Sel
go
CREATE PROCEDURE Archiving.Documents_To_Persons_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		 Record_ID                 -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,Document_ID               -- BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	    ,Person_ID	       		   -- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	    ,Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents_To_Persons
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons_Grid_1')
	DROP PROCEDURE Archiving.Documents_To_Persons_Grid_1
go
CREATE PROCEDURE Archiving.Documents_To_Persons_Grid_1
	(
	@DocumentID BigInt
	)
AS
	SELECT
		 P.Person_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,P.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,N.Segment_Name                 -- NVarChar(64) NOT NULL
		,P.First_Name					-- NVarChar(64) NOT NULL
		,P.Last_Name					-- NVarChar(64) NOT NULL
		,P.Name_Prefix				    -- NVarChar(16) NULL
		,P.Name_Suffix				    -- NVarChar(16) NULL
		,P.Nicknames					-- NVarChar(256) NULL
		,P.Country_Of_Origin			-- Char(3) NOT NULL DEFAULT 'ROM'
		,P.Site_ID_Home				    -- Int NULL
		,P.Site_ID_Billing			    -- Int NULL
		,P.Site_ID_Shipping			    -- Int NULL
		,P.Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Updated_By					-- NVarChar(128) NULL
		,S.Country                      -- Char(3) NOT NULL DEFAULT 'ROM'
		,S.Region                       -- NVarChar(128) NULL
		,S.State_Province               -- NVarChar(128) NULL
		,S.City                         -- NVarChar(64) NOT NULL
		,S.Street_Address_1             -- NVarChar(128) NULL
	FROM CRM.Persons P
		INNER JOIN CRM.Pers_Segment_Names N ON P.Segment_ID = N.Segment_ID
        LEFT JOIN CRM.Pers_Sites S ON P.Site_ID_Home = S.Site_ID
	WHERE P.Person_ID IN (SELECT DISTINCT Person_ID FROM Archiving.Documents_To_Persons	WHERE Document_ID = @DocumentID)
    ORDER BY First_Name, Last_Name, Person_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons_Grid_2')
	DROP PROCEDURE Archiving.Documents_To_Persons_Grid_2
go
CREATE PROCEDURE Archiving.Documents_To_Persons_Grid_2
	(
	@PersonID Int				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	)
AS
	SELECT
		 Document_ID				-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Title						-- NVarChar(256) NOT NULL
		--,Body						-- NVarChar(MAX) NULL
		,External_URL				-- VarChar(256) NULL
		,Document_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Expiring_Date				-- SmallDateTime NULL
		,Closing_Date				-- SmallDateTime NULL
		,External_ID				-- VarChar(128) NULL
		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
		,Document_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
		,Added_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Archiving.Documents
	WHERE Document_ID IN (SELECT DISTINCT Document_ID FROM Archiving.Documents_To_Persons WHERE Person_ID = @PersonID)
    ORDER BY Title, Document_ID
go
