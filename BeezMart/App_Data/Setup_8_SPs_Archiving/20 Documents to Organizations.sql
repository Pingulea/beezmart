






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations_Upsert')
	DROP PROCEDURE Archiving.Documents_To_Organizations_Upsert
go
CREATE PROCEDURE Archiving.Documents_To_Organizations_Upsert
	(
	 @RecordID BigInt OUTPUT            -- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@DocumentID BigInt                 -- NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,@OrganizationID Int				-- NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,@UpdatedOn SmallDateTime = NULL    -- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
    DECLARE @ExistingRecordsCount Int;
    SET @ExistingRecordsCount = (SELECT COUNT(*) FROM Archiving.Documents_To_Organizations WHERE Document_ID = @DocumentID AND Organization_ID = @OrganizationID);
    IF (@ExistingRecordsCount = 0)
        BEGIN
            INSERT INTO Archiving.Documents_To_Organizations
		            (
			         Document_ID
	                ,Organization_ID
	                ,Updated_On
	                ,Updated_By
		            )
	            VALUES
		            (
			         @DocumentID
	                ,@OrganizationID
	                ,@UpdatedOn
	                ,@UpdatedBy
		            );
	        SET @RecordID = SCOPE_IDENTITY();	
	        SELECT CAST(@RecordID AS BigInt) AS ID;
            RETURN;
        END
	SET @RecordID = (SELECT TOP 1 Record_ID FROM Archiving.Documents_To_Organizations WHERE Document_ID = @DocumentID AND Organization_ID = @OrganizationID);
    IF (@ExistingRecordsCount > 1)
        BEGIN
            DELETE FROM Archiving.Documents_To_Organizations
                WHERE Document_ID = @DocumentID
                    AND Organization_ID = @OrganizationID
                    AND Record_ID != @RecordID;
        END
    SELECT @RecordID AS ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations_Del')
	DROP PROCEDURE Archiving.Documents_To_Organizations_Del
go
CREATE PROCEDURE Archiving.Documents_To_Organizations_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM Archiving.Documents_To_Organizations WHERE Record_ID = @RecordID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations_Del_1')
	DROP PROCEDURE Archiving.Documents_To_Organizations_Del_1
go
CREATE PROCEDURE Archiving.Documents_To_Organizations_Del_1
	(
	 @DocumentID BigInt
	,@OrganizationID Int
	)
AS
	DELETE FROM Archiving.Documents_To_Organizations WHERE Document_ID = @DocumentID AND Organization_ID = @OrganizationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations_Sel')
	DROP PROCEDURE Archiving.Documents_To_Organizations_Sel
go
CREATE PROCEDURE Archiving.Documents_To_Organizations_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		 Record_ID                 -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,Document_ID               -- BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	    ,Organization_ID	       -- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	    ,Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents_To_Organizations
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations_Grid_1')
	DROP PROCEDURE Archiving.Documents_To_Organizations_Grid_1
go
CREATE PROCEDURE Archiving.Documents_To_Organizations_Grid_1
	(
	@DocumentID BigInt
	)
AS
	SELECT
		 O.Organization_ID				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,O.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
        ,N.Segment_Name                 -- NVarChar(64) NOT NULL
		,O.Parent_Org_ID				-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,O.Org_Name						-- NVarChar(256) NOT NULL
		,O.Name_Prefix					-- NVarChar(16) NULL
		,O.Name_Suffix					-- NVarChar(16) NULL
		,O.Alternate_Names				-- NVarChar(1024) NULL
		,O.Country_Of_Origin			-- Char(3) NOT NULL DEFAULT 'ROM'
		,O.Site_ID_Headquarter			-- Int NULL
		,O.Site_ID_Billing				-- Int NULL
		,O.Site_ID_Shipping				-- Int NULL
		,O.Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,O.Updated_By					-- NVarChar(128) NULL
		,S.Country                      -- Char(3) NOT NULL DEFAULT 'ROM'
		,S.Region                       -- NVarChar(128) NULL
		,S.State_Province               -- NVarChar(128) NULL
		,S.City                         -- NVarChar(64) NOT NULL
		,S.Street_Address_1             -- NVarChar(128) NULL
	FROM CRM.Organizations O
        INNER JOIN CRM.Orgs_Segment_Names N ON O.Segment_ID = N.Segment_ID
        LEFT JOIN CRM.Orgs_Sites S ON O.Site_ID_Headquarter = S.Site_ID
	WHERE O.Organization_ID IN (SELECT DISTINCT Organization_ID FROM Archiving.Documents_To_Organizations WHERE Document_ID = @DocumentID)
    ORDER BY Org_Name, Organization_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations_Grid_2')
	DROP PROCEDURE Archiving.Documents_To_Organizations_Grid_2
go
CREATE PROCEDURE Archiving.Documents_To_Organizations_Grid_2
	(
	@OrganizationID Int				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	)
AS
	SELECT
		 Document_ID				-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Title						-- NVarChar(256) NOT NULL
		--,Body						-- NVarChar(MAX) NULL
		,External_URL				-- VarChar(256) NULL
		,Document_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Expiring_Date				-- SmallDateTime NULL
		,Closing_Date				-- SmallDateTime NULL
		,External_ID				-- VarChar(128) NULL
		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
		,Document_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
		,Added_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Archiving.Documents
	WHERE Document_ID IN (SELECT DISTINCT Document_ID FROM Archiving.Documents_To_Organizations WHERE Organization_ID = @OrganizationID)
    ORDER BY Title, Document_ID
go
