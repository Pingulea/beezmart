






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Ins')
	DROP PROCEDURE Archiving.Documents_Ins
go
CREATE PROCEDURE Archiving.Documents_Ins
	(
	 @DocumentID BigInt OUTPUT				-- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@Title NVarChar(256)					-- NOT NULL
	,@Body NVarChar(MAX) = NULL
    ,@ExternalURL VarChar(256) = NULL

	,@DocumentDate SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@ExpiringDate SmallDateTime = NULL
	,@ClosingDate SmallDateTime = NULL
	,@ExternalID VarChar(128) = NULL
	
	,@CategoryID SmallInt = NULL			-- FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	,@DocumentStatusID TinyInt = NULL		-- FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	,@AddedOn SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
	,@UpdatedOn SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @DocumentDate = ISNULL(@DocumentDate, GETDATE())
	SET @AddedOn = ISNULL(@AddedOn, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Archiving.Documents
		(
			 Title
	        ,Body
            ,External_URL

	        ,Document_Date
	        ,Expiring_Date
	        ,Closing_Date
	        ,External_ID
	
	        ,Category_ID
	        ,Document_Status_ID
	
	        ,Added_On
	        ,Updated_On
	        ,Updated_By
		)
	VALUES
		(
			 @Title
			,@Body
            ,@ExternalURL

	        ,@DocumentDate
	        ,@ExpiringDate
	        ,@ClosingDate
	        ,@ExternalID
	
	        ,@CategoryID
	        ,@DocumentStatusID
	
	        ,@AddedOn
	        ,@UpdatedOn
	        ,@UpdatedBy
		)
	SET @DocumentID = SCOPE_IDENTITY()	
	SELECT CAST(@DocumentID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Upd')
	DROP PROCEDURE Archiving.Documents_Upd
go
CREATE PROCEDURE Archiving.Documents_Upd
	(
	 @DocumentID BigInt     				-- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@Title NVarChar(256)					-- NOT NULL
	,@Body NVarChar(MAX) = NULL
    ,@ExternalURL VarChar(256) = NULL

	,@DocumentDate SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@ExpiringDate SmallDateTime = NULL
	,@ClosingDate SmallDateTime = NULL
	,@ExternalID VarChar(128) = NULL
	
	,@CategoryID SmallInt = NULL			-- FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	,@DocumentStatusID TinyInt = NULL		-- FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	,@AddedOn SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
	,@UpdatedOn SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @DocumentDate = ISNULL(@DocumentDate, GETDATE())
	SET @AddedOn = ISNULL(@AddedOn, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Archiving.Documents
	SET
		 Title                 = @Title
	    ,Body                  = @Body
        ,External_URL          = @ExternalURL

	    ,Document_Date         = @DocumentDate
	    ,Expiring_Date         = @ExpiringDate
	    ,Closing_Date          = @ClosingDate
	    ,External_ID           = @ExternalID

	    ,Category_ID           = @CategoryID
	    ,Document_Status_ID    = @DocumentStatusID

	    --,Added_On              = @AddedOn
	    ,Updated_On            = @UpdatedOn
	    ,Updated_By            = @UpdatedBy
	WHERE Document_ID = @DocumentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Del')
	DROP PROCEDURE Archiving.Documents_Del
go
CREATE PROCEDURE Archiving.Documents_Del
	(
	 @DocumentID BigInt
    ,@DocumentStatusID TinyInt = 200        -- DELETED, 'Can be considered as deleted, in Recycle Bin.'
	,@UpdatedOn SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Archiving.Documents
	SET
         Document_Status_ID    = @DocumentStatusID
	    ,Updated_On            = @UpdatedOn
	    ,Updated_By            = @UpdatedBy
	WHERE Document_ID = @DocumentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Sel')
	DROP PROCEDURE Archiving.Documents_Sel
go
CREATE PROCEDURE Archiving.Documents_Sel
	(
	@DocumentID BigInt
	)
AS
	SELECT TOP 1
		 D.Document_ID               -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,D.Title                     -- NVarChar(256) NOT NULL
	    ,D.Body                    -- NVarChar(MAX) NULL
        ,D.External_URL              -- VarChar(256) NULL

	    ,D.Document_Date             -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Expiring_Date             -- SmallDateTime NULL
	    ,D.Closing_Date              -- SmallDateTime NULL
	    ,D.External_ID               -- VarChar(128) NULL
	
	    ,D.Category_ID               -- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	    ,C.Category_Name             -- NVarChar(128) NULL
        ,C.Sub_Category              -- NVarChar(128) NULL
	    ,D.Document_Status_ID        -- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	    ,D.Added_On                  -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents D
		LEFT JOIN Archiving.Document_Categories C ON D.Category_ID = C.Category_ID
	WHERE D.Document_ID = @DocumentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Grid_1')
	DROP PROCEDURE Archiving.Documents_Grid_1
go
CREATE PROCEDURE Archiving.Documents_Grid_1
	(
	 @TimeframeStart SmallDateTime = NULL
	,@TimeframeEnd SmallDateTime = NULL
	)
AS
	SET @TimeframeStart = ISNULL(@TimeframeStart, DATEADD(MONTH, -3, GETDATE()))
	SET @TimeframeEnd = ISNULL(@TimeframeEnd, DATEADD(MONTH, 3, GETDATE()))
	SELECT
		 D.Document_ID               -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,D.Title                     -- NVarChar(256) NOT NULL
	    --,D.Body                    -- NVarChar(MAX) NULL
        ,D.External_URL              -- VarChar(256) NULL

	    ,D.Document_Date             -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Expiring_Date             -- SmallDateTime NULL
	    ,D.Closing_Date              -- SmallDateTime NULL
	    ,D.External_ID               -- VarChar(128) NULL
	
	    ,D.Category_ID               -- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	    ,C.Category_Name             -- NVarChar(128) NULL
        ,C.Sub_Category              -- NVarChar(128) NULL
	    ,D.Document_Status_ID        -- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	    ,D.Added_On                  -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents D
		LEFT JOIN Archiving.Document_Categories C ON D.Category_ID = C.Category_ID
	WHERE D.Document_Date BETWEEN @TimeframeStart AND @TimeframeEnd
		OR D.Added_On BETWEEN @TimeframeStart AND @TimeframeEnd
	ORDER BY D.Document_ID DESC
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Grid_2')
	DROP PROCEDURE Archiving.Documents_Grid_2
go
CREATE PROCEDURE Archiving.Documents_Grid_2
	(
	 @TimeframeStart SmallDateTime = NULL
	,@TimeframeEnd SmallDateTime = NULL
	)
AS
	SET @TimeframeStart = ISNULL(@TimeframeStart, DATEADD(MONTH, -3, GETDATE()))
	SET @TimeframeEnd = ISNULL(@TimeframeEnd, DATEADD(MONTH, 3, GETDATE()))
	SELECT
		 D.Document_ID               -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,D.Title                     -- NVarChar(256) NOT NULL
	    --,D.Body                    -- NVarChar(MAX) NULL
        ,D.External_URL              -- VarChar(256) NULL

	    ,D.Document_Date             -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Expiring_Date             -- SmallDateTime NULL
	    ,D.Closing_Date              -- SmallDateTime NULL
	    ,D.External_ID               -- VarChar(128) NULL
	
	    ,D.Category_ID               -- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	    ,C.Category_Name             -- NVarChar(128) NULL
        ,C.Sub_Category              -- NVarChar(128) NULL
	    ,D.Document_Status_ID        -- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	    ,D.Added_On                  -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents D
		LEFT JOIN Archiving.Document_Categories C ON D.Category_ID = C.Category_ID
	WHERE ISNULL(D.Expiring_Date, DATEADD(YEAR, 20, GETDATE())) BETWEEN @TimeframeStart AND @TimeframeEnd
	ORDER BY D.Document_ID DESC
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_Grid_3')
	DROP PROCEDURE Archiving.Documents_Grid_3
go
CREATE PROCEDURE Archiving.Documents_Grid_3
	(
	@MaximumRowCount Int = 1000
	)
AS
	SELECT
		 D.Document_ID               -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,D.Title                     -- NVarChar(256) NOT NULL
	    --,D.Body                    -- NVarChar(MAX) NULL
        ,D.External_URL              -- VarChar(256) NULL

	    ,D.Document_Date             -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Expiring_Date             -- SmallDateTime NULL
	    ,D.Closing_Date              -- SmallDateTime NULL
	    ,D.External_ID               -- VarChar(128) NULL
	
	    ,D.Category_ID               -- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	    ,C.Category_Name             -- NVarChar(128) NULL
        ,C.Sub_Category              -- NVarChar(128) NULL
	    ,D.Document_Status_ID        -- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	    ,D.Added_On                  -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,D.Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents D
		LEFT JOIN Archiving.Document_Categories C ON D.Category_ID = C.Category_ID
	WHERE D.Document_Status_ID NOT IN
		(
			 0			-- : NOT SET :', NULL)
			--,20			-- DRAFT', 'The document with this status is a work in progress.')
			--,40			-- FINAL', 'This is the final, in effect, version of the document.')
			,60			-- SUPERSEDED', 'The document is not applicable anymore because it is superseded by another similar document.')
            ,70			-- EXPIRED', 'The document is not applicable anymore because it is expired.')
			--,200		-- CANCELLED', 'No longer valid or in effect.')
            --,250		-- DELETED', 'Can be considered as deleted, in Recycle Bin.')
		)
	ORDER BY D.Document_ID DESC
go
