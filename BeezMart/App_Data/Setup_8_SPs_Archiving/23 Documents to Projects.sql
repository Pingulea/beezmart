






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects_Upsert')
	DROP PROCEDURE Archiving.Documents_To_Projects_Upsert
go
CREATE PROCEDURE Archiving.Documents_To_Projects_Upsert
	(
	 @RecordID BigInt OUTPUT            -- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@DocumentID BigInt                 -- NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,@ProjectID Int						-- NOT NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
	,@UpdatedOn SmallDateTime = NULL    -- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE());
    DECLARE @ExistingRecordsCount Int;
    SET @ExistingRecordsCount = (SELECT COUNT(*) FROM Archiving.Documents_To_Projects WHERE Document_ID = @DocumentID AND Project_ID = @ProjectID);
    IF (@ExistingRecordsCount = 0)
        BEGIN
            INSERT INTO Archiving.Documents_To_Projects
		            (
			         Document_ID
	                ,Project_ID
	                ,Updated_On
	                ,Updated_By
		            )
	            VALUES
		            (
			         @DocumentID
	                ,@ProjectID
	                ,@UpdatedOn
	                ,@UpdatedBy
		            );
	        SET @RecordID = SCOPE_IDENTITY();	
	        SELECT CAST(@RecordID AS BigInt) AS ID;
            RETURN;
        END
	SET @RecordID = (SELECT TOP 1 Record_ID FROM Archiving.Documents_To_Projects WHERE Document_ID = @DocumentID AND Project_ID = @ProjectID);
    IF (@ExistingRecordsCount > 1)
        BEGIN
            DELETE FROM Archiving.Documents_To_Projects
                WHERE Document_ID = @DocumentID
                    AND Project_ID = @ProjectID
                    AND Record_ID != @RecordID;
        END
    SELECT @RecordID AS ID;
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects_Del')
	DROP PROCEDURE Archiving.Documents_To_Projects_Del
go
CREATE PROCEDURE Archiving.Documents_To_Projects_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM Archiving.Documents_To_Projects WHERE Record_ID = @RecordID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects_Del_1')
	DROP PROCEDURE Archiving.Documents_To_Projects_Del_1
go
CREATE PROCEDURE Archiving.Documents_To_Projects_Del_1
	(
	 @DocumentID BigInt
	,@ProjectID Int
	)
AS
	DELETE FROM Archiving.Documents_To_Projects WHERE Document_ID = @DocumentID AND Project_ID = @ProjectID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects_Sel')
	DROP PROCEDURE Archiving.Documents_To_Projects_Sel
go
CREATE PROCEDURE Archiving.Documents_To_Projects_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		 Record_ID                 -- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	    ,Document_ID               -- BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	    ,Project_ID	       		   -- Int NOT NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
	    ,Updated_On                -- SmallDateTime NOT NULL DEFAULT GETDATE()
	    ,Updated_By                -- NVarChar(128) NULL
	FROM Archiving.Documents_To_Projects
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects_Grid_1')
	DROP PROCEDURE Archiving.Documents_To_Projects_Grid_1
go
CREATE PROCEDURE Archiving.Documents_To_Projects_Grid_1
	(
	@DocumentID BigInt
	)
AS
	SELECT
		 Project_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Title						-- NVarChar(256) NOT NULL
		,Project_Manager			-- NVarChar(256) NULL
		,Contact_Details			-- NVarChar(256) NULL
		--,Details					-- NVarChar(MAX) NULL
		
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name				-- NVarChar(256) NOT NULL
		,End_Customer				-- NVarChar(256) NULL

		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
		,Project_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

		,Scheduled_Start			-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Scheduled_End				-- SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
		,Started_On					-- SmallDateTime NULL
		,Ended_On					-- SmallDateTime NULL
		
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Sales.Projects
	WHERE Project_ID IN (SELECT DISTINCT Project_ID FROM Archiving.Documents_To_Projects WHERE Document_ID = @DocumentID)
    ORDER BY Project_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects_Grid_2')
	DROP PROCEDURE Archiving.Documents_To_Projects_Grid_2
go
CREATE PROCEDURE Archiving.Documents_To_Projects_Grid_2
	(
	@ProjectID Int				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	)
AS
	SELECT
		 Document_ID				-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Title						-- NVarChar(256) NOT NULL
		--,Body						-- NVarChar(MAX) NULL
		,External_URL				-- VarChar(256) NULL
		,Document_Date				-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Expiring_Date				-- SmallDateTime NULL
		,Closing_Date				-- SmallDateTime NULL
		,External_ID				-- VarChar(128) NULL
		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
		,Document_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
		,Added_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Archiving.Documents
	WHERE Document_ID IN (SELECT DISTINCT Document_ID FROM Archiving.Documents_To_Projects WHERE Project_ID = @ProjectID)
    ORDER BY Title, Document_ID
go
