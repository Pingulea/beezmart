






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Ins')
	DROP PROCEDURE Archiving.Document_Categories_Ins
go
CREATE PROCEDURE Archiving.Document_Categories_Ins
	(
	 @CategoryID SmallInt OUTPUT		-- NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
	,@CategoryName NVarChar(128)		-- NOT NULL
	,@SubCategory NVarChar(128)			-- NOT NULL
	,@Discontinued Bit = 0				-- NOT NULL DEFAULT 0
	)
AS
	INSERT INTO Archiving.Document_Categories
		(
			 Category_Name
			,Sub_Category
			,Discontinued
		)
	VALUES
		(
			 @CategoryName
			,@SubCategory
			,@Discontinued
		)
	SET @CategoryID = SCOPE_IDENTITY()	
	SELECT CAST(@CategoryID AS SmallInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Upd')
	DROP PROCEDURE Archiving.Document_Categories_Upd
go
CREATE PROCEDURE Archiving.Document_Categories_Upd
	(
	 @CategoryID SmallInt				-- NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
	,@CategoryName NVarChar(128)		-- NOT NULL
	,@SubCategory NVarChar(128)			-- NOT NULL
	,@Discontinued Bit = 0				-- NOT NULL DEFAULT 0
	)
AS
	UPDATE Archiving.Document_Categories
	SET
		 Category_Name		= @CategoryName
		,Sub_Category		= @SubCategory
		,Discontinued		= @Discontinued
	WHERE Category_ID = @CategoryID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Del')
	DROP PROCEDURE Archiving.Document_Categories_Del
go
CREATE PROCEDURE Archiving.Document_Categories_Del
	(
	 @CategoryID SmallInt				-- NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
	,@Discontinued Bit = 1				-- NOT NULL DEFAULT 0
	)
AS
	UPDATE Archiving.Document_Categories
	SET	Discontinued = @Discontinued
	WHERE Category_ID = @CategoryID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Sel')
	DROP PROCEDURE Archiving.Document_Categories_Sel
go
CREATE PROCEDURE Archiving.Document_Categories_Sel
	(
	@CategoryID SmallInt		-- NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
	)
AS
	SELECT TOP 1
		 Category_ID			-- SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
		,Category_Name			-- NVarChar(128) NOT NULL
		,Sub_Category			-- NVarChar(128) NOT NULL
		,Discontinued			-- Bit NOT NULL DEFAULT 0
	FROM Archiving.Document_Categories
	WHERE Category_ID = @CategoryID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Grid_1')
	DROP PROCEDURE Archiving.Document_Categories_Grid_1
go
CREATE PROCEDURE Archiving.Document_Categories_Grid_1
AS
	SELECT
		 Category_ID			-- SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
		,Category_Name			-- NVarChar(128) NOT NULL
		,Sub_Category			-- NVarChar(128) NOT NULL
		,Discontinued			-- Bit NOT NULL DEFAULT 0
	FROM Archiving.Document_Categories
	WHERE Discontinued = 0
	ORDER BY Category_Name, Sub_Category, Category_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Grid_2')
	DROP PROCEDURE Archiving.Document_Categories_Grid_2
go
CREATE PROCEDURE Archiving.Document_Categories_Grid_2
AS
	SELECT
		 Category_ID			-- SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
		,Category_Name			-- NVarChar(128) NOT NULL
		,Sub_Category			-- NVarChar(128) NOT NULL
		,Discontinued			-- Bit NOT NULL DEFAULT 0
	FROM Archiving.Document_Categories
	ORDER BY Category_Name, Sub_Category, Category_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories_Grid_3')
	DROP PROCEDURE Archiving.Document_Categories_Grid_3
go
CREATE PROCEDURE Archiving.Document_Categories_Grid_3
AS
	SELECT DISTINCT 
		Category_Name			-- NVarChar(128) NOT NULL
	FROM Archiving.Document_Categories
	WHERE Discontinued = 0
	ORDER BY Category_Name
go