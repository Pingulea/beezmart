﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Product_Catalog_Categories')
CREATE TABLE Sales.Product_Catalog_Categories
	(
	 Category_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Category_Name NVarChar(128) NOT NULL
	,Category_Description NVarChar(MAX) NULL
	,Parent_Category_ID SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
	,Retired Bit NOT NULL DEFAULT 0
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Product_Catalog_Items')
CREATE TABLE Sales.Product_Catalog_Items
	(
	 Item_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Category_ID SmallInt NOT NULL REFERENCES Sales.Product_Catalog_Categories(Category_ID)
	,Product_Name NVarChar(256) NOT NULL
	,Product_Or_Service Char NULL DEFAULT 'P'			-- P for products, S for services
	,Manufacturer NVarChar(128) NULL
	,Brand NVarChar(64) NULL
	,Model NVarChar(64) NULL
	,Product_Description NVarChar(MAX) NULL
	,Manufacturer_Part_Number NVarChar(64) NULL
	
	,Unit_Cost Decimal(16, 8) NOT NULL DEFAULT 0
	,Unit_Price Decimal(16, 8) NOT NULL DEFAULT 0
	,Unit_Measure NVarChar(16) NULL
	,Minimum_Stock_Quantity Int NOT NULL DEFAULT 0			-- If too few units in warehouse, trigger alert
	,Warranty_Period_Duration SmallInt NOT NULL DEFAULT 0
	,Retired Bit NOT NULL DEFAULT 0
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--CREATE TABLE Sales.Product_Catalog_Components
--	(
	
--	)
--go







--CREATE TABLE Sales.Product_Catalog_Variants
--	(
	
--	)
--go
