﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Delivery' AND Name = N'Delegates')
CREATE TABLE Delivery.Delegates
	(
	Record_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Delegate_Name NVarChar(128) NULL
	,ID_Field_1 NVarChar(8) NULL
	,ID_Field_2 NVarChar(16) NULL
	,ID_Field_3 NVarChar(32) NULL
	,Transport_Field_1 NVarChar(32) NULL
	,Transport_Field_2 NVarChar(32) NULL
	,Other_Details NVarChar(256) NULL
	,Comments NVarChar(256) NULL
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Delivery' AND Name = N'Fleet_Vehicles')
CREATE TABLE Delivery.Fleet_Vehicles
	(
	Record_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Vehicle_Name NVarChar(32) NULL
	,Registration NVarChar(32) NULL
	,Other_Details NVarChar(256) NULL
	,Comments NVarChar(256) NULL
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Delivery' AND Name = N'Documents')
CREATE TABLE Delivery.Documents
	(
	Document_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Issue_Date SmallDateTime NOT NULL DEFAULT GETDATE()
	,Accounting_ID VarChar(32) NULL
	
	,Document_Status_ID TinyInt NOT NULL FOREIGN KEY REFERENCES Delivery.App_Document_Status(Document_Status_ID)
	
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Customer_Name NVarChar(256) NOT NULL
	,Customer_City NVarChar(64) NULL
	,Customer_Province NVarChar(64) NULL
	,Customer_Address NVarChar(256) NULL
	,Customer_Registration_Number VarChar(16) NULL
	,Customer_Tax_ID VarChar(16) NULL
	,Customer_Bank_Name NVarChar(128) NULL
	,Customer_Bank_Account VarChar(64) NULL
	
	,Delegate_Name NVarChar(128) NULL
	,Delegate_ID_1 NVarChar(8) NULL
	,Delegate_ID_2 NVarChar(16) NULL
	,Delegate_ID_3 NVarChar(32) NULL
	
	,Transport_Name NVarChar(32) NULL
	,Transport_Detail_1 NVarChar(32) NULL
	,Transport_Detail_2 NVarChar(32) NULL
	,Transport_Detail_3 NVarChar(32) NULL
	,Transport_Detail_4 NVarChar(32) NULL
	
	,Document_Detail_1 NVarChar(256) NULL
	,Document_Detail_2 NVarChar(256) NULL
	,Document_Detail_3 NVarChar(256) NULL
	,Document_Detail_4 NVarChar(256) NULL
	
	,Sales_Order_ID Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
	,Rendering_Layout VarChar(256) NULL
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Delivery' AND Name = N'Document_Items')
CREATE TABLE Delivery.Document_Items
	(
	Item_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Document_ID Int NOT NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
	,Product_Catalog_Item_ID Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
	,Product_Catalog_Category_ID SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
	,Sales_Order_ID Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
	,Sales_Order_Item_ID BigInt NULL FOREIGN KEY REFERENCES Sales.Order_Items(Item_ID)

	,Item_Name NVarChar(1024) NOT NULL
	,Unit_Measure NVarChar(16) NULL
	,Quantity Int NOT NULL DEFAULT 1
	)
go
