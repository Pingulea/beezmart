﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'App_Order_Status')
CREATE TABLE Sales.App_Order_Status
	(
	Order_Status_ID TinyInt PRIMARY KEY
	,Order_Status_Name VarChar(64) NOT NULL
	,Order_Status_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM Sales.App_Order_Status) = 0)
BEGIN
	INSERT INTO Sales.App_Order_Status (Order_Status_ID, Order_Status_Name, Order_Status_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(20, 'SAVED, DRAFT', NULL)
			,(40, 'IN REVIEW', NULL)
			,(60, 'SALES OFFER', NULL)
			,(80, 'REJECTED OFFER', NULL)
			,(100, 'IN EXECUTION', NULL)
			,(120, 'ABORTED. CANCELLED', NULL)
			,(140, 'EXECUTED', NULL)
			,(160, 'DELIVERED', NULL)
			,(180, 'INVOICED', NULL)
			,(200, 'CLOSED', NULL)
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'App_Order_Delivery_Methods')
CREATE TABLE Sales.App_Order_Delivery_Methods
	(
	Delivery_Method_ID TinyInt NOT NULL PRIMARY KEY
	,Delivery_Method_Name NVarChar(64) NOT NULL
	,Delivery_Method_Description NVarChar(Max) NULL
	)
go
IF ((SELECT COUNT(*) FROM Sales.App_Order_Delivery_Methods) = 0)
BEGIN
	INSERT INTO Sales.App_Order_Delivery_Methods(Delivery_Method_ID, Delivery_Method_Name, Delivery_Method_Description)
		VALUES
			 (0, ': NOT SET :', 'Delivery method is not yet chosen.')
			,(20, 'CUSTOMER', 'The customer comes and picks up the order items.')
			,(40, 'FLEET', 'Sales order items will be delivered by our own company vehicles.')
			,(60, 'COURIER', 'Delivery will be done with the help of a courier.')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'App_Project_Status')
CREATE TABLE Sales.App_Project_Status
	(
	Project_Status_ID TinyInt NOT NULL PRIMARY KEY
	,Project_Status_Name NVarChar(128) NOT NULL
	,Discontinued BIT NOT NULL DEFAULT 0
	)
go
IF ((SELECT COUNT(*) FROM Sales.App_Project_Status) = 0)
BEGIN
	INSERT INTO Sales.App_Project_Status(Project_Status_ID, Project_Status_Name)
		VALUES
			 (0, 'Undefined')
			,(10, 'Dreaming')
			,(20, 'Concept')
			,(30, 'Planned')
			,(40, 'In execution')
			,(50, 'Finalized')
			,(60, 'Customer-approved')
			,(70, 'Billed')
			,(80, 'Paid')
			,(90, 'Closed and archived')
			,(100, 'Cancelled')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'App_Activity_Status')
CREATE TABLE Sales.App_Activity_Status
	(
	Activity_Status_ID TinyInt PRIMARY KEY
	,Activity_Status_Name VarChar(64) NOT NULL
	,Activity_Status_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM Sales.App_Activity_Status) = 0)
BEGIN
	INSERT INTO Sales.App_Activity_Status (Activity_Status_ID, Activity_Status_Name, Activity_Status_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(10, 'Pending', NULL)
			,(20, 'Cancelled', NULL)
			,(50, 'In progress', NULL)
			,(51, 'In progress 1%', NULL)
			,(52, 'In progress 5%', NULL)
			,(53, 'In progress 10%', NULL)
			,(54, 'In progress 15%', NULL)
			,(55, 'In progress 20%', NULL)
			,(56, 'In progress 25%', NULL)
			,(57, 'In progress 30%', NULL)
			,(58, 'In progress 35%', NULL)
			,(59, 'In progress 40%', NULL)
			,(60, 'In progress 45%', NULL)
			,(61, 'In progress 50%', NULL)
			,(62, 'In progress 55%', NULL)
			,(63, 'In progress 60%', NULL)
			,(64, 'In progress 65%', NULL)
			,(65, 'In progress 70%', NULL)
			,(66, 'In progress 75%', NULL)
			,(67, 'In progress 80%', NULL)
			,(68, 'In progress 85%', NULL)
			,(69, 'In progress 90%', NULL)
			,(70, 'In progress 95%', NULL)
			,(71, 'In progress 99%', NULL)
			,(250, 'Completed', NULL)
END
go

