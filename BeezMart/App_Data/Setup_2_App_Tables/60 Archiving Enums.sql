﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'App_Document_Status')
CREATE TABLE Archiving.App_Document_Status
	(
	 Document_Status_ID TinyInt PRIMARY KEY
	,Document_Status_Name VarChar(64) NOT NULL
	,Document_Status_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM Archiving.App_Document_Status) = 0)
BEGIN
	INSERT INTO Archiving.App_Document_Status(Document_Status_ID, Document_Status_Name, Document_Status_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(20, 'DRAFT', 'The document with this status is a work in progress.')
			,(40, 'FINAL', 'This is the final, in effect, version of the document.')
			,(60, 'SUPERSEDED', 'The document is not applicable anymore because it is superseded by another similar document.')
            ,(70, 'EXPIRED', 'The document is not applicable anymore because it is expired.')
			,(200, 'CANCELLED', 'No longer valid or in effect.')
            ,(250, 'DELETED', 'Can be considered as deleted, in Recycle Bin.')
END
go