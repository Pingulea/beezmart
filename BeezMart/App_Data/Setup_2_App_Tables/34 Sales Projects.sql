﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Project_Categories')
CREATE TABLE Sales.Project_Categories
	(
	Category_ID SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
	,Category_Name NVarChar(128) NOT NULL
	,Sub_Category NVarChar(128) NOT NULL
	,Discontinued Bit NOT NULL DEFAULT 0
	)
go
IF ((SELECT COUNT(*) FROM Sales.Project_Categories) = 0)
BEGIN
    INSERT INTO Sales.Project_Categories (Category_Name, Sub_Category)
        VALUES
            ('Generic category', 'No sub-category')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects')
CREATE TABLE Sales.Projects
	(
	 Project_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Title NVarChar(256) NOT NULL
	,Project_Manager NVarChar(256) NULL
	,Contact_Details NVarChar(256) NULL
	,Details NVarChar(MAX) NULL
	
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Customer_Name NVarChar(256) NOT NULL
	,End_Customer NVarChar(256) NULL

	,Category_ID SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
	,Project_Status_ID TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

	,Scheduled_Start SmallDateTime NOT NULL DEFAULT GETDATE()
	,Scheduled_End SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
	,Started_On SmallDateTime NULL
	,Ended_On SmallDateTime NULL
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types')
CREATE TABLE Sales.Activity_Types
	(
	Activity_Type_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Activity_Type_Name NVarChar(64) NOT NULL
	,Activity_Type_Category NVarChar(64) NULL
	,Activity_Type_Comments NVarChar(MAX) NULL
	,Discontinued Bit NOT NULL DEFAULT 0
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
IF ((SELECT COUNT(*) FROM Sales.Activity_Types) = 0)
BEGIN
    INSERT INTO Sales.Activity_Types(Activity_Type_Name, Activity_Type_Comments)
        VALUES
            ('Generic activity type', 'To be renamed and repurposed by TimeSheet administrator')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities')
CREATE TABLE Sales.Activities
	(
	 Activity_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Project_ID Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Customer_Name NVarChar(256) NOT NULL
	,End_Customer NVarChar(256) NULL

	,Activity_Status_ID TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
	,Activity_Status_Comment NVarChar(256) NULL
	,Activity_Type_ID SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
	,Title NVarChar(1024) NOT NULL
	,Details NVarChar(MAX) NULL

	,Scheduled_Start SmallDateTime NOT NULL DEFAULT GETDATE()
	,Scheduled_End SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
	,Assigned_On SmallDateTime NULL
	,Assigned_To NVarChar(128) NULL
	,Deadline_On SmallDateTime NULL
	,Started_On SmallDateTime NULL
	,Ended_On SmallDateTime NULL
	,Reminder_On SmallDateTime NULL

	,Requested_By NVarChar(128) NULL
	,Requesting_Department NVarChar(128) NULL
	,Completed_By NVarChar(128) NULL
	,Logged_By NVarChar(128) NULL
	,Billable Bit NOT NULL DEFAULT 0
	,Invoice_IDs VarChar(256) NULL
	,Duration_In_Minutes Int NULL

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
