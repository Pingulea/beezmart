﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoices')
CREATE TABLE Billing.Invoices
	(
	Invoice_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	
	,Issue_Date SmallDateTime NOT NULL DEFAULT GETDATE()
	,Due_Date SmallDateTime NULL
	,Closing_Date SmallDateTime NULL
	,Accounting_ID VarChar(32) NULL
	,VAT_Percentage Decimal(6, 4) NOT NULL DEFAULT 0
	,Currency Char(3) NOT NULL DEFAULT 'EUR'
	
	,Invoice_Type_ID TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Type(Invoice_Type_ID)
	,Invoice_Status_ID TinyInt NOT NULL FOREIGN KEY REFERENCES Billing.App_Invoice_Status(Invoice_Status_ID)
	
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Customer_Name NVarChar(256) NOT NULL
	,Customer_City NVarChar(64) NULL
	,Customer_Province NVarChar(64) NULL
	,Customer_Address NVarChar(256) NULL
	,Customer_Registration_Number VarChar(32) NULL
	,Customer_Tax_ID VarChar(16) NULL
	,Customer_Bank_Name NVarChar(128) NULL
	,Customer_Bank_Account VarChar(64) NULL
	
	,Delegate_Name NVarChar(128) NULL
	,Delegate_ID_1 NVarChar(8) NULL
	,Delegate_ID_2 NVarChar(16) NULL
	,Delegate_ID_3 NVarChar(32) NULL
	
	,Transport_Name NVarChar(32) NULL
	,Transport_Detail_1 NVarChar(32) NULL
	,Transport_Detail_2 NVarChar(32) NULL
	,Transport_Detail_3 NVarChar(32) NULL
	,Transport_Detail_4 NVarChar(32) NULL
	
	,Invoice_Detail_1 NVarChar(256) NULL
	,Invoice_Detail_2 NVarChar(256) NULL
	,Invoice_Detail_3 NVarChar(256) NULL
	,Invoice_Detail_4 NVarChar(256) NULL

	,Sales_Order_ID Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
	,Delivery_Document_ID Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
	,Related_Invoice_ID Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	,Rendering_Layout VarChar(256) NULL
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Items')
CREATE TABLE Billing.Invoice_Items
	(
	Item_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Invoice_ID Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	,Product_Catalog_Item_ID Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
	,Product_Catalog_Category_ID SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
	,Product_Or_Service Char NULL DEFAULT 'P'			-- P for products, S for services
	,Sales_Order_ID Int NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
	,Sales_Order_Item_ID BigInt NULL FOREIGN KEY REFERENCES Sales.Order_Items(Item_ID)
	,Delivery_Document_ID Int NULL FOREIGN KEY REFERENCES Delivery.Documents(Document_ID)
	,Delivery_Document_Item_ID BigInt NULL FOREIGN KEY REFERENCES Delivery.Document_Items(Item_ID)

	,Item_Name NVarChar(1024) NOT NULL
	,Unit_Price Decimal(16, 8) NOT NULL DEFAULT 0
	,Unit_Measure NVarChar(16) NULL
	,Quantity Int NOT NULL DEFAULT 1
	,Total_Price AS Unit_Price * Quantity
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Invoice_Accounting_IDs')
CREATE TABLE Billing.Invoice_Accounting_IDs
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Number Int NOT NULL
	,Accounting_ID VarChar(32) NOT NULL
	,Invoice_ID Int NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payment_Checks')
CREATE TABLE Billing.Payment_Checks
	(
	Check_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Reference NVarChar(128) NOT NULL
	,Amount Decimal(24, 8) NOT NULL DEFAULT 0
	,Currency Char(3) NOT NULL DEFAULT 'EUR'
	,Due_Date SmallDateTime NOT NULL
	,Issue_Date SmallDateTime NOT NULL
	,Cashed_Date SmallDateTime NULL
	,Comments NVarChar(MAX) NULL
	,Check_Type VarChar(8) NOT NULL
	,Customer_Name NVarChar(256) NOT NULL
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Bank_Name NVarChar(128) NULL

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Payments')
CREATE TABLE Billing.Payments
	(
	Payment_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Invoice_ID Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	,Payment_Date SmallDateTime NOT NULL DEFAULT GETDATE()
	,Amount Decimal(24, 8) NOT NULL DEFAULT 0
	,Currency Char(3) NOT NULL DEFAULT 'EUR'
	,Comments NVarChar(MAX) NULL
	,Payment_Check_ID Int NULL FOREIGN KEY REFERENCES Billing.Payment_Checks(Check_ID)

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Accounts')
CREATE TABLE Billing.Customer_Accounts
	(
	Account_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,IBAN VarChar(32) NOT NULL
	,Bank NVarChar(128) NOT NULL
	,Currency Char(3) NOT NULL DEFAULT 'EUR'

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Customer_Settings')
CREATE TABLE Billing.Customer_Settings
	(
	Record_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Registration_Number VarChar(32) NULL
	,Tax_ID VarChar(16) NULL
	,Currency Char(3) NULL
	,Preferred_Account_ID Int NULL FOREIGN KEY REFERENCES Billing.Customer_Accounts(Account_ID)
	,Maximum_Discount_Percentage TinyInt NULL
	,Payment_Allowance_In_Days TinyInt NULL

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Handover_Delegates')
CREATE TABLE Billing.Handover_Delegates
	(
	Record_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Delegate_Name NVarChar(128) NOT NULL
	,Delegate_ID_1 NVarChar(8) NULL
	,Delegate_ID_2 NVarChar(16) NULL
	,Delegate_ID_3 NVarChar(32) NULL

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'Handover_Transport')
CREATE TABLE Billing.Handover_Transport
	(
	Record_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Means NVarChar(16) NOT NULL
	,Registration NVarChar(16) NULL

	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go

