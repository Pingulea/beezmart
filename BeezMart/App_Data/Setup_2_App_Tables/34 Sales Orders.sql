﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Orders')
CREATE TABLE Sales.Orders
	(
	 Order_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Title NVarChar(256) NOT NULL
	,Details NVarChar(MAX) NULL 
	,Sales_Person NVarChar(256) NULL
	,Issue_Date SmallDateTime NOT NULL DEFAULT GETDATE()
	,Due_Date SmallDateTime NULL
	,Closing_Date SmallDateTime NULL
	,Order_Status_ID TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Order_Status(Order_Status_ID)
	
	,Organization_ID Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Customer_Name NVarChar(256) NOT NULL
	,End_Customer NVarChar(256) NULL
	,Currency Char(3) NOT NULL DEFAULT 'EUR'
	,Project_Manager NVarChar(256) NULL
	,Review_Date SmallDateTime NULL
	,Review_Done Bit NOT NULL DEFAULT 0
	,Delivery_Method_ID TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES Sales.App_Order_Delivery_Methods(Delivery_Method_ID)
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Order_Items')
CREATE TABLE Sales.Order_Items
	(
	Item_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Order_ID Int NOT NULL FOREIGN KEY REFERENCES Sales.Orders(Order_ID)
	,Product_Catalog_Item_ID Int NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Items(Item_ID)
	,Product_Catalog_Category_ID SmallInt NULL FOREIGN KEY REFERENCES Sales.Product_Catalog_Categories(Category_ID)
	,Product_Or_Service Char NULL DEFAULT 'P'			-- P for products, S for services

	,Item_Name NVarChar(1024) NOT NULL
	,Unit_Cost Decimal(16, 8) NOT NULL DEFAULT 0
	,Unit_Price Decimal(16, 8) NOT NULL DEFAULT 0
	,Unit_Measure NVarChar(16) NULL
	,Quantity Int NOT NULL DEFAULT 1
	,Total_Cost AS Unit_Cost * Quantity
	,Total_Price AS Unit_Price * Quantity
	,Due_Date SmallDateTime NULL
	)
go
