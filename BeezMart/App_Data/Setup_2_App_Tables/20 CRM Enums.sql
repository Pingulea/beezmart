﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_Contact_Status')
CREATE TABLE CRM.App_Contact_Status
	(
	Contact_Status_ID TinyInt PRIMARY KEY
	,Contact_Status_Name VarChar(64) NOT NULL
	,Contact_Status_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM CRM.App_Contact_Status) = 0)
BEGIN
	INSERT INTO CRM.App_Contact_Status (Contact_Status_ID, Contact_Status_Name, Contact_Status_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(1, 'Unconfirmed', NULL)
			,(2, 'Confirmed', NULL)
			,(3, 'Incomplete', NULL)
			,(4, 'Invalid', NULL)
			,(5, 'No longer exists', NULL)
			,(6, 'To be changed', NULL)
			,(7, 'Problems', NULL)
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_Contact_Types')
CREATE TABLE CRM.App_Contact_Types
	(
	Contact_Type_ID TinyInt PRIMARY KEY
	,Contact_Type_Name VarChar(64) NOT NULL
	,Contact_Type_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM CRM.App_Contact_Types) = 0)
BEGIN
	INSERT INTO CRM.App_Contact_Types (Contact_Type_ID, Contact_Type_Name, Contact_Type_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(1, 'E-mail', NULL)
			,(2, 'Phone', NULL)
			,(3, 'Mobile', NULL)
			,(4, 'Fax', NULL)
			,(5, 'Web', NULL)
			,(6, 'Telex', NULL)
			,(7, 'Postal', NULL)
			,(8, 'Chat, Instant-Messaging ID', NULL)
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_Activity_Status')
CREATE TABLE CRM.App_Activity_Status
	(
	Activity_Status_ID TinyInt PRIMARY KEY
	,Activity_Status_Name VarChar(64) NOT NULL
	,Activity_Status_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM CRM.App_Activity_Status) = 0)
BEGIN
	INSERT INTO CRM.App_Activity_Status (Activity_Status_ID, Activity_Status_Name, Activity_Status_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(10, 'Pending', NULL)
			,(20, 'Cancelled', NULL)
			,(50, 'In progress', NULL)
			,(51, 'In progress 1%', NULL)
			,(52, 'In progress 5%', NULL)
			,(53, 'In progress 10%', NULL)
			,(54, 'In progress 15%', NULL)
			,(55, 'In progress 20%', NULL)
			,(56, 'In progress 25%', NULL)
			,(57, 'In progress 30%', NULL)
			,(58, 'In progress 35%', NULL)
			,(59, 'In progress 40%', NULL)
			,(60, 'In progress 45%', NULL)
			,(61, 'In progress 50%', NULL)
			,(62, 'In progress 55%', NULL)
			,(63, 'In progress 60%', NULL)
			,(64, 'In progress 65%', NULL)
			,(65, 'In progress 70%', NULL)
			,(66, 'In progress 75%', NULL)
			,(67, 'In progress 80%', NULL)
			,(68, 'In progress 85%', NULL)
			,(69, 'In progress 90%', NULL)
			,(70, 'In progress 95%', NULL)
			,(71, 'In progress 99%', NULL)
			,(250, 'Completed', NULL)
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_Countries')
CREATE TABLE CRM.App_Countries
	(
	Country_ID Char(3) NOT NULL PRIMARY KEY
	,Code Char(2) NULL
	,Code_UN VarChar(4) NULL
	,Country_Name NVarChar(128) NOT NULL
	,Show BIT NOT NULL DEFAULT 0
	)
go
IF ((SELECT COUNT(*) FROM CRM.App_Countries) = 0)
BEGIN
	INSERT INTO CRM.App_Countries (Country_ID, Country_Name, Code, Code_UN, Show)
		VALUES
			 ('AFG', N'AFGHANISTAN', 'AF', '4', 0)
			,('ALB', N'ALBANIA', 'AL', '8', 1)
			,('DZA', N'ALGERIA', 'DZ', '12', 0)
			,('ASM', N'AMERICAN SAMOA', 'AS', '16', 0)
			,('AND', N'ANDORRA', 'AD', '20', 0)
			,('AGO', N'ANGOLA', 'AO', '24', 0)
			,('AIA', N'ANGUILLA', 'AI', '660', 0)
			,('ATA', N'ANTARCTICA', 'AQ', '10', 0)
			,('ATG', N'ANTIGUA AND BARBUDA', 'AG', '28', 0)
			,('ARG', N'ARGENTINA', 'AR', '32', 0)
			,('ARM', N'ARMENIA', 'AM', '51', 0)
			,('ABW', N'ARUBA', 'AW', '533', 0)
			,('AUS', N'AUSTRALIA', 'AU', '36', 0)
			,('AUT', N'AUSTRIA', 'AT', '40', 1)
			,('AZE', N'AZERBAIJAN', 'AZ', '31', 0)
			,('BHS', N'BAHAMAS', 'BS', '44', 0)
			,('BHR', N'BAHRAIN', 'BH', '48', 0)
			,('BGD', N'BANGLADESH', 'BD', '50', 0)
			,('BRB', N'BARBADOS', 'BB', '52', 0)
			,('BLR', N'BELARUS', 'BY', '112', 0)
			,('BEL', N'BELGIUM', 'BE', '56', 1)
			,('BLZ', N'BELIZE', 'BZ', '84', 0)
			,('BEN', N'BENIN', 'BJ', '204', 0)
			,('BMU', N'BERMUDA', 'BM', '60', 0)
			,('BTN', N'BHUTAN', 'BT', '64', 0)
			,('BOL', N'BOLIVIA', 'BO', '68', 0)
			,('BIH', N'BOSNIA AND HERZEGOWINA', 'BA', '70', 1)
			,('BWA', N'BOTSWANA', 'BW', '72', 0)
			,('BVT', N'BOUVET ISLAND', 'BV', '74', 0)
			,('BRA', N'BRAZIL', 'BR', '76', 0)
			,('IOT', N'BRITISH INDIAN OCEAN TERRITORY', 'IO', '86', 0)
			,('BRN', N'BRUNEI DARUSSALAM', 'BN', '96', 0)
			,('BGR', N'BULGARIA', 'BG', '100', 1)
			,('BFA', N'BURKINA FASO', 'BF', '854', 0)
			,('BDI', N'BURUNDI', 'BI', '108', 0)
			,('KHM', N'CAMBODIA', 'KH', '116', 0)
			,('CMR', N'CAMEROON', 'CM', '120', 0)
			,('CAN', N'CANADA', 'CA', '124', 1)
			,('CPV', N'CAPE VERDE', 'CV', '132', 0)
			,('CYM', N'CAYMAN ISLANDS', 'KY', '136', 0)
			,('CAF', N'CENTRAL AFRICAN REPUBLIC', 'CF', '140', 0)
			,('TCD', N'CHAD', 'TD', '148', 0)
			,('CHL', N'CHILE', 'CL', '152', 0)
			,('CHN', N'CHINA', 'CN', '156', 1)
			,('CXR', N'CHRISTMAS ISLAND', 'CX', '162', 0)
			,('CCK', N'COCOS (KEELING) ISLANDS', 'CC', '166', 0)
			,('COL', N'COLOMBIA', 'CO', '170', 0)
			,('COM', N'COMOROS', 'KM', '174', 0)
			,('COG', N'CONGO', 'CG', '178', 0)
			,('COD', N'CONGO, THE DRC', 'CD', '180', 0)
			,('COK', N'COOK ISLANDS', 'CK', '184', 0)
			,('CRI', N'COSTA RICA', 'CR', '188', 0)
			,('CIV', N'COTE D''IVOIRE', 'CI', '384', 0)
			,('HRV', N'CROATIA (local name: Hrvatska)', 'HR', '191', 1)
			,('CUB', N'CUBA', 'CU', '192', 0)
			,('CYP', N'CYPRUS', 'CY', '196', 1)
			,('CZE', N'CZECH REPUBLIC', 'CZ', '203', 1)
			,('DNK', N'DENMARK', 'DK', '208', 1)
			,('DJI', N'DJIBOUTI', 'DJ', '262', 0)
			,('DMA', N'DOMINICA', 'DM', '212', 0)
			,('DOM', N'DOMINICAN REPUBLIC', 'DO', '214', 0)
			,('TMP', N'EAST TIMOR', 'TP', '626', 0)
			,('ECU', N'ECUADOR', 'EC', '218', 0)
			,('EGY', N'EGYPT', 'EG', '818', 1)
			,('SLV', N'EL SALVADOR', 'SV', '222', 0)
			,('GNQ', N'EQUATORIAL GUINEA', 'GQ', '226', 0)
			,('ERI', N'ERITREA', 'ER', '232', 0)
			,('EST', N'ESTONIA', 'EE', '233', 1)
			,('ETH', N'ETHIOPIA', 'ET', '231', 0)
			,('FLK', N'FALKLAND ISLANDS (MALVINAS)', 'FK', '238', 0)
			,('FRO', N'FAROE ISLANDS', 'FO', '234', 0)
			,('FJI', N'FIJI', 'FJ', '242', 0)
			,('FIN', N'FINLAND', 'FI', '246', 1)
			,('FRA', N'FRANCE', 'FR', '250', 1)
			,('FXX', N'FRANCE, METROPOLITAN', 'FX', '249', 0)
			,('GUF', N'FRENCH GUIANA', 'GF', '254', 0)
			,('PYF', N'FRENCH POLYNESIA', 'PF', '258', 0)
			,('ATF', N'FRENCH SOUTHERN TERRITORIES', 'TF', '260', 0)
			,('GAB', N'GABON', 'GA', '266', 0)
			,('GMB', N'GAMBIA', 'GM', '270', 0)
			,('GEO', N'GEORGIA', 'GE', '268', 0)
			,('DEU', N'GERMANY', 'DE', '276', 1)
			,('GHA', N'GHANA', 'GH', '288', 0)
			,('GIB', N'GIBRALTAR', 'GI', '292', 0)
			,('GRC', N'GREECE', 'GR', '300', 1)
			,('GRL', N'GREENLAND', 'GL', '304', 0)
			,('GRD', N'GRENADA', 'GD', '308', 0)
			,('GLP', N'GUADELOUPE', 'GP', '312', 0)
			,('GUM', N'GUAM', 'GU', '316', 0)
			,('GTM', N'GUATEMALA', 'GT', '320', 0)
			,('GIN', N'GUINEA', 'GN', '324', 0)
			,('GNB', N'GUINEA-BISSAU', 'GW', '624', 0)
			,('GUY', N'GUYANA', 'GY', '328', 0)
			,('HTI', N'HAITI', 'HT', '332', 0)
			,('HMD', N'HEARD AND MC DONALD ISLANDS', 'HM', '334', 0)
			,('VAT', N'HOLY SEE (VATICAN CITY STATE)', 'VA', '336', 0)
			,('HND', N'HONDURAS', 'HN', '340', 0)
			,('HKG', N'HONG KONG', 'HK', '344', 0)
			,('HUN', N'HUNGARY', 'HU', '348', 1)
			,('ISL', N'ICELAND', 'IS', '352', 1)
			,('IND', N'INDIA', 'IN', '356', 0)
			,('IDN', N'INDONESIA', 'ID', '360', 0)
			,('IRN', N'IRAN (ISLAMIC REPUBLIC OF)', 'IR', '364', 0)
			,('IRQ', N'IRAQ', 'IQ', '368', 0)
			,('IRL', N'IRELAND', 'IE', '372', 1)
			,('ISR', N'ISRAEL', 'IL', '376', 1)
			,('ITA', N'ITALY', 'IT', '380', 1)
			,('JAM', N'JAMAICA', 'JM', '388', 0)
			,('JPN', N'JAPAN', 'JP', '392', 0)
			,('JOR', N'JORDAN', 'JO', '400', 0)
			,('KAZ', N'KAZAKHSTAN', 'KZ', '398', 0)
			,('KEN', N'KENYA', 'KE', '404', 0)
			,('KIR', N'KIRIBATI', 'KI', '296', 0)
			,('PRK', N'KOREA, D.P.R.O.', 'KP', '408', 0)
			,('KOR', N'KOREA, REPUBLIC OF', 'KR', '410', 0)
			,('KWT', N'KUWAIT', 'KW', '414', 0)
			,('KGZ', N'KYRGYZSTAN', 'KG', '417', 0)
			,('LAO', N'LAOS', 'LA', '418', 0)
			,('LVA', N'LATVIA', 'LV', '428', 0)
			,('LBN', N'LEBANON', 'LB', '422', 0)
			,('LSO', N'LESOTHO', 'LS', '426', 0)
			,('LBR', N'LIBERIA', 'LR', '430', 0)
			,('LBY', N'LIBYAN ARAB JAMAHIRIYA', 'LY', '434', 0)
			,('LIE', N'LIECHTENSTEIN', 'LI', '438', 0)
			,('LTU', N'LITHUANIA', 'LT', '440', 0)
			,('LUX', N'LUXEMBOURG', 'LU', '442', 0)
			,('MAC', N'MACAU', 'MO', '446', 0)
			,('MKD', N'MACEDONIA', 'MK', '807', 1)
			,('MDG', N'MADAGASCAR', 'MG', '450', 0)
			,('MWI', N'MALAWI', 'MW', '454', 0)
			,('MYS', N'MALAYSIA', 'MY', '458', 0)
			,('MDV', N'MALDIVES', 'MV', '462', 0)
			,('MLI', N'MALI', 'ML', '466', 0)
			,('MLT', N'MALTA', 'MT', '470', 1)
			,('MHL', N'MARSHALL ISLANDS', 'MH', '584', 0)
			,('MTQ', N'MARTINIQUE', 'MQ', '474', 0)
			,('MRT', N'MAURITANIA', 'MR', '478', 0)
			,('MUS', N'MAURITIUS', 'MU', '480', 0)
			,('MYT', N'MAYOTTE', 'YT', '175', 0)
			,('MEX', N'MEXICO', 'MX', '484', 0)
			,('FSM', N'MICRONESIA, FEDERATED STATES OF', 'FM', '583', 0)
			,('MDA', N'MOLDOVA, REPUBLIC OF', 'MD', '498', 1)
			,('MCO', N'MONACO', 'MC', '492', 0)
			,('MNG', N'MONGOLIA', 'MN', '496', 0)
			,('MNE', N'MONTENEGRO', 'ME', '499', 1)
			,('MSR', N'MONTSERRAT', 'MS', '500', 0)
			,('MAR', N'MOROCCO', 'MA', '504', 0)
			,('MOZ', N'MOZAMBIQUE', 'MZ', '508', 0)
			,('MMR', N'MYANMAR (Burma)', 'MM', '104', 0)
			,('NAM', N'NAMIBIA', 'NA', '516', 0)
			,('NRU', N'NAURU', 'NR', '520', 0)
			,('NPL', N'NEPAL', 'NP', '524', 0)
			,('NLD', N'NETHERLANDS', 'NL', '528', 1)
			,('ANT', N'NETHERLANDS ANTILLES', 'AN', '530', 0)
			,('NCL', N'NEW CALEDONIA', 'NC', '540', 0)
			,('NZL', N'NEW ZEALAND', 'NZ', '554', 0)
			,('NIC', N'NICARAGUA', 'NI', '558', 0)
			,('NER', N'NIGER', 'NE', '562', 0)
			,('NGA', N'NIGERIA', 'NG', '566', 0)
			,('NIU', N'NIUE', 'NU', '570', 0)
			,('NFK', N'NORFOLK ISLAND', 'NF', '574', 0)
			,('MNP', N'NORTHERN MARIANA ISLANDS', 'MP', '580', 0)
			,('NOR', N'NORWAY', 'NO', '578', 1)
			,('OMN', N'OMAN', 'OM', '512', 0)
			,('PAK', N'PAKISTAN', 'PK', '586', 0)
			,('PLW', N'PALAU', 'PW', '585', 0)
			,('PAN', N'PANAMA', 'PA', '591', 0)
			,('PNG', N'PAPUA NEW GUINEA', 'PG', '598', 0)
			,('PRY', N'PARAGUAY', 'PY', '600', 0)
			,('PER', N'PERU', 'PE', '604', 0)
			,('PHL', N'PHILIPPINES', 'PH', '608', 0)
			,('PCN', N'PITCAIRN', 'PN', '612', 0)
			,('POL', N'POLAND', 'PL', '616', 1)
			,('PRT', N'PORTUGAL', 'PT', '620', 1)
			,('PRI', N'PUERTO RICO', 'PR', '630', 0)
			,('QAT', N'QATAR', 'QA', '634', 0)
			,('REU', N'REUNION', 'RE', '638', 0)
			,('ROM', N'ROMANIA', 'RO', '642', 1)
			,('RUS', N'RUSSIAN FEDERATION', 'RU', '643', 1)
			,('RWA', N'RWANDA', 'RW', '646', 0)
			,('KNA', N'SAINT KITTS AND NEVIS', 'KN', '659', 0)
			,('LCA', N'SAINT LUCIA', 'LC', '662', 0)
			,('VCT', N'SAINT VINCENT AND THE GRENADINES', 'VC', '670', 0)
			,('WSM', N'SAMOA', 'WS', '882', 0)
			,('SMR', N'SAN MARINO', 'SM', '674', 0)
			,('STP', N'SAO TOME AND PRINCIPE', 'ST', '678', 0)
			,('SAU', N'SAUDI ARABIA', 'SA', '682', 0)
			,('SEN', N'SENEGAL', 'SN', '686', 0)
			,('SRB', N'SERBIA', 'RS', '688', 1)
			,('SYC', N'SEYCHELLES', 'SC', '690', 0)
			,('SLE', N'SIERRA LEONE', 'SL', '694', 0)
			,('SGP', N'SINGAPORE', 'SG', '702', 1)
			,('SVK', N'SLOVAKIA (Slovak Republic)', 'SK', '703', 1)
			,('SVN', N'SLOVENIA', 'SI', '705', 1)
			,('SLB', N'SOLOMON ISLANDS', 'SB', '90', 0)
			,('SOM', N'SOMALIA', 'SO', '706', 0)
			,('ZAF', N'SOUTH AFRICA', 'ZA', '729', 0)
			,('SSD', N'SOUTH SUDAN', 'SS', '710', 0)
			,('SGS', N'SOUTH GEORGIA AND SOUTH S.S.', 'GS', '239', 0)
			,('ESP', N'SPAIN', 'ES', '724', 1)
			,('LKA', N'SRI LANKA', 'LK', '144', 0)
			,('SHN', N'ST. HELENA', 'SH', '654', 0)
			,('SPM', N'ST. PIERRE AND MIQUELON', 'PM', '666', 0)
			,('SDN', N'SUDAN', 'SD', '736', 0)
			,('SUR', N'SURINAME', 'SR', '740', 0)
			,('SJM', N'SVALBARD AND JAN MAYEN ISLANDS', 'SJ', '744', 0)
			,('SWZ', N'SWAZILAND', 'SZ', '748', 0)
			,('SWE', N'SWEDEN', 'SE', '752', 1)
			,('CHE', N'SWITZERLAND', 'CH', '756', 1)
			,('SYR', N'SYRIAN ARAB REPUBLIC', 'SY', '760', 0)
			,('TWN', N'TAIWAN, PROVINCE OF CHINA', 'TW', '158', 0)
			,('TJK', N'TAJIKISTAN', 'TJ', '762', 0)
			,('TZA', N'TANZANIA, UNITED REPUBLIC OF', 'TZ', '834', 0)
			,('THA', N'THAILAND', 'TH', '764', 0)
			,('TGO', N'TOGO', 'TG', '768', 0)
			,('TKL', N'TOKELAU', 'TK', '772', 0)
			,('TON', N'TONGA', 'TO', '776', 0)
			,('TTO', N'TRINIDAD AND TOBAGO', 'TT', '780', 0)
			,('TUN', N'TUNISIA', 'TN', '788', 0)
			,('TUR', N'TURKEY', 'TR', '792', 1)
			,('TKM', N'TURKMENISTAN', 'TM', '795', 0)
			,('TCA', N'TURKS AND CAICOS ISLANDS', 'TC', '796', 0)
			,('TUV', N'TUVALU', 'TV', '798', 0)
			,('UGA', N'UGANDA', 'UG', '800', 0)
			,('UKR', N'UKRAINE', 'UA', '804', 1)
			,('ARE', N'UNITED ARAB EMIRATES', 'AE', '784', 0)
			,('GBR', N'UNITED KINGDOM', 'GB', '826', 1)
			,('USA', N'UNITED STATES', 'US', '840', 1)
			,('UMI', N'U.S. MINOR ISLANDS', 'UM', '581', 0)
			,('URY', N'URUGUAY', 'UY', '858', 0)
			,('UZB', N'UZBEKISTAN', 'UZ', '860', 0)
			,('VUT', N'VANUATU', 'VU', '548', 0)
			,('VEN', N'VENEZUELA', 'VE', '862', 0)
			,('VNM', N'VIET NAM', 'VN', '704', 0)
			,('VGB', N'VIRGIN ISLANDS (BRITISH)', 'VG', '92', 0)
			,('VIR', N'VIRGIN ISLANDS (U.S.)', 'VI', '850', 0)
			,('WLF', N'WALLIS AND FUTUNA ISLANDS', 'WF', '876', 0)
			,('ESH', N'WESTERN SAHARA', 'EH', '732', 0)
			,('YEM', N'YEMEN', 'YE', '887', 0)
			,('ZMB', N'ZAMBIA', 'ZM', '894', 0)
			,('ZWE', N'ZIMBABWE', 'ZW', '716', 0)
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_States_Provinces')
CREATE TABLE CRM.App_States_Provinces
	(
	State_Province_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Country_ID Char(3) NOT NULL DEFAULT 'ROM'
	,Region NVarChar(128) NULL
	,State_Province_Abbr NVarChar(8) NOT NULL
	,State_Province_Name NVarChar(128) NOT NULL
	)
go
IF ((SELECT COUNT(*) FROM CRM.App_States_Provinces) = 0)
BEGIN
	INSERT INTO CRM.App_States_Provinces (Country_ID, State_Province_Abbr, State_Province_Name)
		VALUES
			 ('ROM', 'AB', N'Alba')
			,('ROM', 'AR', N'Arad')
			,('ROM', 'AG', N'Argeș')
			,('ROM', 'BC', N'Bacău')
			,('ROM', 'BH', N'Bihor')
			,('ROM', 'BN', N'Bistrița-Năsăud')
			,('ROM', 'BT', N'Botoșani')
			,('ROM', 'BV', N'Brașov')
			,('ROM', 'BR', N'Brăila')
			,('ROM', 'BU', N'București')
			,('ROM', 'B1', N'Sector 1')
			,('ROM', 'B2', N'Sector 2')
			,('ROM', 'B3', N'Sector 3')
			,('ROM', 'B4', N'Sector 4')
			,('ROM', 'B5', N'Sector 5')
			,('ROM', 'B6', N'Sector 6')
			,('ROM', 'BZ', N'Buzău')
			,('ROM', 'CS', N'Caraș-Severin')
			,('ROM', 'CL', N'Călărași')
			,('ROM', 'CJ', N'Cluj')
			,('ROM', 'CT', N'Constanța')
			,('ROM', 'CV', N'Covasna')
			,('ROM', 'DB', N'Dâmbovița')
			,('ROM', 'DJ', N'Dolj')
			,('ROM', 'GL', N'Galați')
			,('ROM', 'GR', N'Giurgiu')
			,('ROM', 'GJ', N'Gorj')
			,('ROM', 'HR', N'Harghita')
			,('ROM', 'HD', N'Hunedoara')
			,('ROM', 'IF', N'Ilfov')
			,('ROM', 'IL', N'Ialomița')
			,('ROM', 'IS', N'Iași')
			,('ROM', 'MM', N'Maramureș')
			,('ROM', 'MH', N'Mehedinți')
			,('ROM', 'MS', N'Mureș')
			,('ROM', 'NT', N'Neamț')
			,('ROM', 'OT', N'Olt')
			,('ROM', 'PH', N'Prahova')
			,('ROM', 'SM', N'Satu Mare')
			,('ROM', 'SJ', N'Sălaj')
			,('ROM', 'SB', N'Sibiu')
			,('ROM', 'SV', N'Suceava')
			,('ROM', 'TR', N'Teleorman')
			,('ROM', 'TM', N'Timiș')
			,('ROM', 'TL', N'Tulcea')
			,('ROM', 'VL', N'Vâlcea')
			,('ROM', 'VS', N'Vaslui')
			,('ROM', 'VN', N'Vrancea')

			,('MDA', 'MD-01', N'Anenii Noi')
			,('MDA', 'MD-02', N'Basarabeasca')
			,('MDA', 'MD-03', N'Briceni')
			,('MDA', 'MD-04', N'Cahul')
			,('MDA', 'MD-05', N'Cantemir')
			,('MDA', 'MD-06', N'Călărași')
			,('MDA', 'MD-07', N'Căușeni')
			,('MDA', 'MD-08', N'Cimișlia')
			,('MDA', 'MD-09', N'Criuleni')
			,('MDA', 'MD-10', N'Dondușeni')
			,('MDA', 'MD-11', N'Drochia')
			,('MDA', 'MD-12', N'Dubăsari')
			,('MDA', 'MD-13', N'Edineț')
			,('MDA', 'MD-14', N'Fălești')
			,('MDA', 'MD-15', N'Florești')
			,('MDA', 'MD-16', N'Glodeni')
			,('MDA', 'MD-17', N'Hîncești')
			,('MDA', 'MD-18', N'Ialoveni')
			,('MDA', 'MD-19', N'Leova')
			,('MDA', 'MD-20', N'Nisporeni')
			,('MDA', 'MD-21', N'Ocnița')
			,('MDA', 'MD-22', N'Orhei')
			,('MDA', 'MD-23', N'Rezina')
			,('MDA', 'MD-24', N'Rîșcani')
			,('MDA', 'MD-25', N'Sîngerei')
			,('MDA', 'MD-26', N'Soroca')
			,('MDA', 'MD-27', N'Șoldănești')
			,('MDA', 'MD-28', N'Ștefan Vodă')
			,('MDA', 'MD-29', N'Taraclia')
			,('MDA', 'MD-30', N'Telenești')
			,('MDA', 'MD-31', N'Ungheni')
END
go

