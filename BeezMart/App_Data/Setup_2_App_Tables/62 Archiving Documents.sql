﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Document_Categories')
CREATE TABLE Archiving.Document_Categories
	(
	Category_ID SmallInt NOT NULL PRIMARY KEY IDENTITY (-32768, 1)
	,Category_Name NVarChar(128) NOT NULL
	,Sub_Category NVarChar(128) NOT NULL
	,Discontinued Bit NOT NULL DEFAULT 0
	)
go
IF ((SELECT COUNT(*) FROM Archiving.Document_Categories) = 0)
BEGIN
    INSERT INTO Archiving.Document_Categories (Category_Name, Sub_Category)
        VALUES
            ('Generic category', 'No sub-category')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents')
CREATE TABLE Archiving.Documents
	(
	 Document_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Title NVarChar(256) NOT NULL
	,Body NVarChar(MAX) NULL
    ,External_URL VarChar(256) NULL

	,Document_Date SmallDateTime NOT NULL DEFAULT GETDATE()
	,Expiring_Date SmallDateTime NULL
	,Closing_Date SmallDateTime NULL
	,External_ID VarChar(128) NULL
	
	,Category_ID SmallInt NULL FOREIGN KEY REFERENCES Archiving.Document_Categories(Category_ID)
	,Document_Status_ID TinyInt NULL FOREIGN KEY REFERENCES Archiving.App_Document_Status(Document_Status_ID)
	
	,Added_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Organizations')
CREATE TABLE Archiving.Documents_To_Organizations
	(
	 Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Document_ID BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,Organization_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Persons')
CREATE TABLE Archiving.Documents_To_Persons
	(
	 Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Document_ID BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Invoices')
CREATE TABLE Archiving.Documents_To_Invoices
	(
	 Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Document_ID BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,Invoice_ID Int NOT NULL FOREIGN KEY REFERENCES Billing.Invoices(Invoice_ID)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Projects')
CREATE TABLE Archiving.Documents_To_Projects
	(
	 Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Document_ID BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,Project_ID Int NOT NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Archiving' AND Name = N'Documents_To_Activities')
CREATE TABLE Archiving.Documents_To_Activities
	(
	 Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Document_ID BigInt NOT NULL FOREIGN KEY REFERENCES Archiving.Documents(Document_ID)
	,Activity_ID BigInt NOT NULL FOREIGN KEY REFERENCES Sales.Activities(Activity_ID)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
