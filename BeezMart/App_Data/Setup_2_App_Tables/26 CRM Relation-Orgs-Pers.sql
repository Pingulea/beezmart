﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types')
CREATE TABLE CRM.Relation_Orgs_Pers_Job_Types
	(
	Job_Type_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Job_Type_Category NVarChar(64) NULL
	,Job_Type_Name NVarChar(64) NOT NULL
	,Job_Type_Comments NVarChar(MAX) NULL
	,Discontinued Bit NOT NULL DEFAULT 0
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
IF ((SELECT COUNT(*) FROM CRM.Relation_Orgs_Pers_Job_Types) = 0)
BEGIN
	INSERT INTO CRM.Relation_Orgs_Pers_Job_Types (Job_Type_Category, Job_Type_Name)
		VALUES
			 (NULL, 'Employee')
			,(NULL, 'Engineer')
			,('Middle Management', 'IT Manager')
			,('Middle Management', 'Marketing Manager')
			,('Middle Management', 'Quality Assurance')
			,('Upper Management', 'General Manager')
			,('Upper Management', 'Financial Manager')
			,('Upper Management', 'Technical Manager')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Relation_Orgs_Pers')
CREATE TABLE CRM.Relation_Orgs_Pers
	(
	Relation_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Organization_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Position_Title NVarChar(256) NULL
	,Job_Type_ID SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
	
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go

