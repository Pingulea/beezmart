﻿






IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'App'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [App] AUTHORIZATION [dbo]'
	END
go
IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'CRM'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [CRM] AUTHORIZATION [dbo]'
	END
go
IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'Sales'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [Sales] AUTHORIZATION [dbo]'
	END
go
IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'Delivery'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [Delivery] AUTHORIZATION [dbo]'
	END
go
IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'Billing'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [Billing] AUTHORIZATION [dbo]'
	END
go
IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'Purchasing'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [Purchasing] AUTHORIZATION [dbo]'
	END
go
IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'Archiving'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [Archiving] AUTHORIZATION [dbo]'
	END
go

