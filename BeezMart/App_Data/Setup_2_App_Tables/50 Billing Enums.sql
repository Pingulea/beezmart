﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'App_Invoice_Status')
CREATE TABLE Billing.App_Invoice_Status
	(
	 Invoice_Status_ID TinyInt PRIMARY KEY
	,Invoice_Status_Name VarChar(64) NOT NULL
	,Invoice_Status_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM Billing.App_Invoice_Status) = 0)
BEGIN
	INSERT INTO Billing.App_Invoice_Status(Invoice_Status_ID, Invoice_Status_Name, Invoice_Status_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(20, 'DRAFT', 'The operators are still building the invoice; it hasn''t been released or issued.')
			,(40, 'VALIDATED', 'Invoice is ready to print and send. All fields are validated against law regulations and internal process of company or customer.')
			,(60, 'SENT TO CUSTOMER', 'Invoice is awaiting payment after having been issued and sent to customer.')
			,(80, 'CANCELLED', 'The invoice has been rejected either by customer or by company. Fields or items were invalidated for some reason.')
			,(100, 'CLOSED', 'The customer has payed the invoice and money have been cashed in.')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Billing' AND Name = N'App_Invoice_Type')
CREATE TABLE Billing.App_Invoice_Type
	(
	 Invoice_Type_ID TinyInt PRIMARY KEY
	,Invoice_Type_Name VarChar(64) NOT NULL
	,Invoice_Type_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM Billing.App_Invoice_Type) = 0)
BEGIN
	INSERT INTO Billing.App_Invoice_Type(Invoice_Type_ID, Invoice_Type_Name, Invoice_Type_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)
			,(20, 'REGULAR', 'Charges the customer after the products or services were delivered.')
			,(40, 'STORNO', 'The customer returns some of the products that were delivered.')
			,(60, 'ADVANCE', 'Customer is paying in advance for products or services.')
			,(80, 'RISTURN', 'Some or all of the advance payment is returned.')
			,(100, 'PRO FORMA', 'More like an offer or a draft informing on prices and quantities.')
END
go
