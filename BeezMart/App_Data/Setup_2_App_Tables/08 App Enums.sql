﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'App' AND Name = N'Entity_Types')
CREATE TABLE App.Entity_Types
	(
	 Entity_Type_ID TinyInt PRIMARY KEY
	,Entity_Type_Name VarChar(64) NOT NULL
	,Entity_Type_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM App.Entity_Types) = 0)
BEGIN
	INSERT INTO App.Entity_Types (Entity_Type_ID, Entity_Type_Name, Entity_Type_Comments)
		VALUES
			 (0, ': NOT SET :', NULL)

			,(10, 'CRM Organization', NULL)
			,(11, 'CRM Person', NULL)
			,(12, 'CRM Orgs Site', NULL)
			,(13, 'CRM Pers Site', NULL)
			,(14, 'CRM Orgs Activity', NULL)
			,(15, 'CRM Pers Activity', NULL)

			,(20, 'CRM Orgs Contact', NULL)
			,(21, 'CRM Pers Contact', NULL)
			,(22, 'CRM Orgs Number Attribute', NULL)
			,(23, 'CRM Pers Number Attribute', NULL)
			,(24, 'CRM Orgs Text Attribute', NULL)
			,(25, 'CRM Pers Text Attribute', NULL)
			,(26, 'CRM Orgs Flag', NULL)
			,(27, 'CRM Pers Flag', NULL)
			,(28, 'CRM Orgs HugeText', NULL)
			,(29, 'CRM Pers HugeText', NULL)

			,(40, 'CRM Relation Orgs-Pers', NULL)

			,(50, 'Sales Project', NULL)
			,(51, 'Sales Project Activity', NULL)
			,(52, 'Sales Order', NULL)
			,(53, 'Sales Order Item', NULL)

			,(70, 'Billing Invoice', NULL)
			,(71, 'Billing Invoice Item', NULL)
			,(72, 'Billing Payment', NULL)
			,(73, 'Billing Payment Check', NULL)
			,(74, 'Billing Customer IBAN', NULL)
			,(75, 'Billing Invoicing Detail', NULL)
			,(76, 'Billing Handover Delegate', NULL)
			,(77, 'Billing Handover Transport', NULL)

			,(240, 'Archiving Document', NULL)
END
go