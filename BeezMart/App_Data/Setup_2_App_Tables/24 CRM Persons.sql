﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Segment_Names')
CREATE TABLE CRM.Pers_Segment_Names
	(
	Segment_ID TinyInt NOT NULL PRIMARY KEY
	,Segment_Name NVarChar(64) NOT NULL
	,Segment_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM CRM.Pers_Segment_Names) = 0)
BEGIN
	INSERT INTO CRM.Pers_Segment_Names (Segment_ID, Segment_Name, Segment_Comments)
		VALUES
			 (0, ': UNSEGMENTED :', 'Needs further discutions')
			,(1, 'Customer', 'This person is one of our customers')
			,(2, 'Employee', 'This person is a current employee of our organization')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons')
CREATE TABLE CRM.Persons
	(
	Person_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Segment_ID TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
	,First_Name NVarChar(64) NOT NULL
	,Last_Name NVarChar(64) NOT NULL
	,Name_Prefix NVarChar(16) NULL
	,Name_Suffix NVarChar(16) NULL
	,Nicknames NVarChar(256) NULL
	,Country_Of_Origin Char(3) NOT NULL DEFAULT 'ROM'
	,Site_ID_Home Int NULL
	,Site_ID_Billing Int NULL
	,Site_ID_Shipping Int NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Sites')
CREATE TABLE CRM.Pers_Sites
	(
	Site_ID Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Contact_Status_ID TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
	,Site_Name NVarChar(128) NULL
	,Country Char(3) NOT NULL DEFAULT 'ROM'
	,Region NVarChar(128) NULL
	,State_Province NVarChar(128) NULL
	,City NVarChar(64) NOT NULL
	,Street_Address_1 NVarChar(128) NULL
	,Street_Address_2 NVarChar(128) NULL
	,Street_Address_3 NVarChar(128) NULL
	,Department NVarChar(128) NULL
	,Directions NVarChar(128) NULL
	,ZIP_Post_Code VarChar(32) NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts')
CREATE TABLE CRM.Pers_Contacts
	(
	Contact_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Site_ID Int NULL FOREIGN KEY REFERENCES CRM.Pers_Sites(Site_ID)
	,Contact NVarChar(256) NOT NULL
	,Comments NVarChar(256) NULL
	,Contact_Type_ID TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
	,Contact_Status_ID TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Field_Names')
CREATE TABLE CRM.Pers_Field_Names
	(
	Field_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Field_Category NVarChar(64) NULL
	,Field_Name NVarChar(64) NOT NULL
	,Field_Comments NVarChar(MAX) NULL
	,Is_Unique Bit NOT NULL DEFAULT 0
	,Is_Retired Bit NOT NULL DEFAULT 0
	,Field_Type_ID TinyInt NOT NULL FOREIGN KEY REFERENCES App.Field_Types(Field_Type_ID)
	)
go
IF ((SELECT COUNT(*) FROM CRM.Pers_Field_Names) = 0)
BEGIN
	INSERT INTO CRM.Pers_Field_Names(Field_Type_ID, Field_Name, Field_Comments)
		VALUES
			 (0, 'Generic field', 'To be renamed and repurposed by BeezMart administrator')
			,(1, 'Generic String', 'To be renamed and repurposed by BeezMart administrator')
			,(2, 'Generic Integer', 'To be renamed and repurposed by BeezMart administrator')
			,(3, 'Generic DateTime', 'To be renamed and repurposed by BeezMart administrator')
			,(4, 'Generic HugeText', 'To be renamed and repurposed by BeezMart administrator')
			,(5, 'Generic TimeInterval', 'To be renamed and repurposed by BeezMart administrator')
			,(6, 'Generic Decimal', 'To be renamed and repurposed by BeezMart administrator')
			,(7, 'Generic TableOrXml', 'To be renamed and repurposed by BeezMart administrator')
			,(8, 'Generic Flag', 'To be renamed and repurposed by BeezMart administrator')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_String_Fields')
CREATE TABLE CRM.Pers_String_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value NVarChar(256) NOT NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Integer_Fields')
CREATE TABLE CRM.Pers_Integer_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value Int NOT NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_DateTime_Fields')
CREATE TABLE CRM.Pers_DateTime_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value DateTime NOT NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_HugeText_Fields')
CREATE TABLE CRM.Pers_HugeText_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value NVarChar(MAX)
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_TimeInterval_Fields')
CREATE TABLE CRM.Pers_TimeInterval_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value Time NOT NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Decimal_Fields')
CREATE TABLE CRM.Pers_Decimal_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value Decimal(24, 8) NOT NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_TableOrXml_Fields')
CREATE TABLE CRM.Pers_TableOrXml_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value Xml NOT NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Flag_Fields')
CREATE TABLE CRM.Pers_Flag_Fields
	(
	Record_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Field_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Field_Names(Field_ID)
	,Field_Value NVarChar(256) NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activity_Types')
CREATE TABLE CRM.Pers_Activity_Types
	(
	Activity_Type_ID SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
	,Activity_Type_Name NVarChar(64) NOT NULL
	,Activity_Type_Category NVarChar(64) NULL
	,Activity_Type_Comments NVarChar(MAX) NULL
	,Discontinued Bit NOT NULL DEFAULT 0
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go
BEGIN
INSERT INTO CRM.Pers_Activity_Types(Activity_Type_Name, Activity_Type_Comments) VALUES ('Generic activity type', 'To be renamed and repurposed by BeezMart administrator')
END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activities')
CREATE TABLE CRM.Pers_Activities
	(
	Activity_ID BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,Person_ID Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,Activity_Status_ID TinyInt NOT NULL FOREIGN KEY REFERENCES CRM.App_Activity_Status(Activity_Status_ID)
	,Activity_Status_Comment NVarChar(256) NULL
	,Activity_Type_ID SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Activity_Types(Activity_Type_ID)
	,Title NVarChar(1024) NOT NULL
	,Details NVarChar(MAX) NULL
	,Scheduled_For SmallDateTime NULL
	,Reminder_On SmallDateTime NULL
	,Started_On SmallDateTime NULL
	,Completed_On SmallDateTime NULL
	,Requested_By NVarChar(128) NULL
	,Completed_By NVarChar(128) NULL
	,Billable Bit NOT NULL DEFAULT 0
	,Invoice_IDs VarChar(256) NULL
	,Duration_In_Minutes Int NULL
	,Updated_On SmallDateTime NOT NULL DEFAULT GETDATE()
	,Updated_By NVarChar(128) NULL
	)
go

