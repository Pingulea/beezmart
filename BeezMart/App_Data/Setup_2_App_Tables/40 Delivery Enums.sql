﻿






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'Delivery' AND Name = N'App_Document_Status')
CREATE TABLE Delivery.App_Document_Status
	(
	Document_Status_ID TinyInt PRIMARY KEY
	,Document_Status_Name VarChar(64) NOT NULL
	,Document_Status_Description NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM Delivery.App_Document_Status) = 0)
BEGIN
	INSERT INTO Delivery.App_Document_Status (Document_Status_ID, Document_Status_Name, Document_Status_Description)
		VALUES
			 (0, ': NOT SET :', 'The document is barely saved as a record in database.')
			,(20, 'DRAFT', 'The operator is still working on the document to finish.')
			,(40, 'VALIDATED', 'Document fields have been checked and validated against regulations and company policies.')
			,(60, 'CANCELLED', 'The delivery will no longer be done, at least not by company fleet.')
			,(80, 'CLOSED', 'Delivery was done and the document is archived.')
			,(200, 'STORNO', 'Returned quantities: the customer has sent some sales order items or products back.')
END
go

