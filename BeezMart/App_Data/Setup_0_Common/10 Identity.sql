






--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetUsers')
CREATE TABLE [dbo].[AspNetUsers]
(
	[Id] Int NOT NULL CONSTRAINT [PK_AspNetUsers] PRIMARY KEY IDENTITY(-2147483648, 1),
	[UserName] NVarChar(256) NULL,
	[UserGUID] UniqueIdentifier NOT NULL DEFAULT NEWID(),
	[NormalizedUserName] NVarChar(256) NULL,
	[Email] NVarChar(256) NULL,
	[NormalizedEmail] NVarChar(256) NULL,
	[EmailConfirmed] Bit NOT NULL,
	[AccessFailedCount] Int NOT NULL,
	[ConcurrencyStamp] NVarChar(MAX) NULL,
	[LockoutEnabled] Bit NOT NULL,
	[LockoutEnd] DateTimeOffset(7) NULL,
	[PasswordHash] NVarChar(256) NULL,
	[PhoneNumber] VarChar(128) NULL,
	[PhoneNumberConfirmed] Bit NOT NULL,
	[SecurityStamp] NVarChar(128) NULL,
	[TwoFactorEnabled] Bit NOT NULL,

	[IsBlocked] Bit NOT NULL DEFAULT 0,

	[FirstName] NVarChar(64) NULL,
	[LastName] NVarChar(64) NULL,
	[Organization] NVarChar(128) NULL,
	[OrganizationId] Int NULL,
	[JobTitle] NVarChar(128) NULL,
	[Department] NVarChar(128) NULL,
	[Phone] VarChar(64) NULL,
	[Mobile] VarChar(64) NULL,
	[City] NVarChar(64) NULL,
	[StateProvince] NVarChar(64) NULL,
	[StreetAddress1] NVarChar(128) NULL,
	[StreetAddress2] NVarChar(128) NULL,
	[StreetAddress3] NVarChar(128) NULL,
	[PostalCode] NVarChar(32) NULL
)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetUserLogins')
CREATE TABLE [dbo].[AspNetUserLogins]
(
	[LoginProvider] NVarChar(128) NOT NULL,
	[ProviderKey] NVarChar(128) NOT NULL,
	[UserId] Int NOT NULL CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY REFERENCES [dbo].[AspNetUsers]([Id]) ON DELETE CASCADE,
	[ProviderDisplayName] NVarChar(256) NULL
	
	CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetUserLogins')
CREATE TABLE [dbo].[AspNetUserTokens]
(
	[UserId] Int NOT NULL CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY REFERENCES [dbo].[AspNetUsers]([Id]) ON DELETE CASCADE,
	[LoginProvider] NVarChar(128) NOT NULL,
	[Name] NVarChar(128) NOT NULL,
	[Value] NVarChar(MAX) NULL
	
	CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED ([UserId] ASC, [LoginProvider] ASC, [Name] ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetRoles')
CREATE TABLE [dbo].[AspNetRoles]
	(
	[Id] Int NOT NULL CONSTRAINT [PK_AspNetRoles] PRIMARY KEY IDENTITY(-2147483648, 1),
	[Name] NVarChar(256) NOT NULL,
	[NormalizedName] NVarChar(256) NULL,
	[ConcurrencyStamp] NVarChar(MAX) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetUserRoles')
CREATE TABLE [dbo].[AspNetUserRoles]
	(
	[UserId] Int NOT NULL CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY REFERENCES [dbo].[AspNetUsers]([Id]) ON DELETE CASCADE,
	[RoleId] Int NOT NULL CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY REFERENCES [dbo].[AspNetRoles]([Id]) ON DELETE CASCADE

	CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetUserClaims')
CREATE TABLE [dbo].[AspNetUserClaims]
	(
	[Id] Int NOT NULL CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY IDENTITY(-2147483648,1),
	[UserId] Int NULL CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY REFERENCES [dbo].[AspNetUsers]([Id]),
	[ClaimType] NVarChar(256) NULL,
	[ClaimValue] NVarChar(1024) NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'dbo' AND Name = N'AspNetRoleClaims')
CREATE TABLE [dbo].[AspNetRoleClaims]
	(
	[Id] Int NOT NULL CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY IDENTITY(-2147483648,1),
	[RoleId] Int NULL CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY REFERENCES [dbo].[AspNetRoles]([Id]),
	[ClaimType] NVarChar(256) NULL,
	[ClaimValue] NVarChar(1024) NULL
	)
go