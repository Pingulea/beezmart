






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'App' AND Name = N'Settings_Grid_1')
	DROP PROCEDURE App.Settings_Grid_1
go
CREATE PROCEDURE App.Settings_Grid_1
AS
	SELECT
		Setting_Key				-- VarChar(256)
		,Setting_Value			-- NVarChar(1024)
	FROM App.Settings
	ORDER BY Setting_Key, Setting_Value
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'App' AND Name = N'Settings_Sel_1')
	DROP PROCEDURE App.Settings_Sel_1
go
CREATE PROCEDURE App.Settings_Sel_1
	(
	@SettingKey VarChar(256)
	)
AS
	SELECT TOP 1
		Setting_Key				-- VarChar(256)
		,Setting_Value			-- NVarChar(1024)
	FROM App.Settings
	WHERE Setting_Key = @SettingKey
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'App' AND Name = N'Settings_Upsert')
	DROP PROCEDURE App.Settings_Upsert
go
CREATE PROCEDURE App.Settings_Upsert
	(
	@SettingKey VarChar(256)
	,@SettingValue NVarChar(1024)
	)
AS
	IF EXISTS (SELECT * FROM App.Settings WHERE Setting_Key = @SettingKey)
		UPDATE App.Settings 
		SET Setting_Value = @SettingValue
		WHERE Setting_Key = @SettingKey
	ELSE
	INSERT INTO App.Settings(Setting_Key, Setting_Value)
	VALUES (@SettingKey, @SettingValue)
go