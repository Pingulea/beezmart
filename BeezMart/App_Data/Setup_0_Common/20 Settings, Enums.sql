﻿





IF NOT EXISTS
		(
			SELECT	Schema_Name
			FROM	Information_Schema.Schemata
			WHERE	Schema_Name = 'App'
		) 
	BEGIN
		EXEC SP_ExecuteSQL N'CREATE SCHEMA [App] AUTHORIZATION [dbo]'
	END
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'App' AND Name = N'Settings')
CREATE --       DROP
TABLE App.Settings
	(
	Setting_Key VarChar(256) NOT NULL PRIMARY KEY
	,Setting_Value NVarChar(1024) NOT NULL
	)
go







--=============================================================================
IF NOT EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'User_Table' AND SCHEMA_NAME(SCHEMA_ID) = 'App' AND Name = N'Field_Types')
CREATE --       DROP
TABLE App.Field_Types
	(
	Field_Type_ID TinyInt NOT NULL PRIMARY KEY
	,Field_Type_Name NVarChar(64) NOT NULL
	,Field_Type_Comments NVarChar(MAX) NULL
	)
go
IF ((SELECT COUNT(*) FROM App.Field_Types) = 0)
BEGIN
    INSERT INTO App.Field_Types (Field_Type_ID, Field_Type_Name, Field_Type_Comments)
        VALUES
             (0, ': NOT DEFINED :', NULL)
            ,(1, 'String', NULL)
            ,(2, 'Integer', NULL)
            ,(3, 'DateTime', NULL)
            ,(4, 'HugeText', NULL)
            ,(5, 'TimeInterval', NULL)
            ,(6, 'Decimal', NULL)
            ,(7, 'TableOrXml', NULL)
            ,(8, 'Flag', NULL)
END
go


