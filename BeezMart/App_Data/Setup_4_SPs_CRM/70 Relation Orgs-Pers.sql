﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Grid_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Grid_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Grid_1
	(
	@OrganizationID Int
	)
AS
	SELECT
		R.Relation_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,R.Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,R.Position_Title				-- NVarChar(256) NULL
		,C.Job_Type_ID					-- SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
		,C.Job_Type_Name				-- NVarChar(64) NULL
		,C.Job_Type_Category			-- NVarChar(64) NULL
		,P.First_Name					-- NVarChar(64) NOT NULL
		,P.Last_Name					-- NVarChar(64) NOT NULL
		,P.Name_Prefix					-- NVarChar(16) NULL
		,P.Name_Suffix					-- NVarChar(16) NULL
		,P.Nicknames					-- NVarChar(256) NULL
		,P.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,S.Segment_Name					-- NVarChar(256) NOT NULL
		,R.Updated_On					-- SmallDateTime = NULL
		,R.Updated_By					-- NVarChar(128) = NULL
	FROM CRM.Relation_Orgs_Pers R
		INNER JOIN CRM.Persons P ON R.Person_ID = P.Person_ID
		INNER JOIN CRM.Pers_Segment_Names S ON P.Segment_ID = S.Segment_ID
		LEFT JOIN CRM.Relation_Orgs_Pers_Job_Types C ON R.Job_Type_ID = C.Job_Type_ID
	WHERE R.Organization_ID = @OrganizationID
	ORDER BY P.First_Name, P.Last_Name, P.Nicknames, P.Name_Prefix, R.Relation_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Grid_2')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Grid_2
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Grid_2
	(
	@PersonID Int
	)
AS
	SELECT
		R.Relation_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,R.Organization_ID				-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,R.Position_Title				-- NVarChar(256) NULL
		,C.Job_Type_ID					-- SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
		,C.Job_Type_Name				-- NVarChar(64) NULL
		,C.Job_Type_Category			-- NVarChar(64) NULL
		,O.Org_Name						-- NVarChar(256) NOT NULL
		,O.Name_Prefix					-- NVarChar(16) NULL
		,O.Name_Suffix					-- NVarChar(16) NULL
		,O.Alternate_Names				-- NVarChar(1024) NULL
		,O.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,S.Segment_Name					-- NVarChar(256) NOT NULL
		,R.Updated_On					-- SmallDateTime = NULL
		,R.Updated_By					-- NVarChar(128) = NULL
	FROM CRM.Relation_Orgs_Pers R
		INNER JOIN CRM.Organizations O ON R.Organization_ID = O.Organization_ID
		INNER JOIN CRM.Orgs_Segment_Names S ON O.Segment_ID = S.Segment_ID
		LEFT JOIN CRM.Relation_Orgs_Pers_Job_Types C ON R.Job_Type_ID = C.Job_Type_ID
	WHERE R.Person_ID = @PersonID
	ORDER BY O.Org_Name, O.Name_Prefix, O.Alternate_Names, R.Relation_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Sel_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Sel_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Sel_1
	(
	@RelationID BigInt
	)
AS
	SELECT TOP 1
		R.Relation_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,R.Organization_ID				-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,R.Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,R.Position_Title				-- NVarChar(256) NULL
		,C.Job_Type_ID					-- SmallInt NULL FOREIGN KEY REFERENCES CRM.Relation_Orgs_Pers_Job_Types(Job_Type_ID)
		,C.Job_Type_Name				-- NVarChar(64) NULL
		,C.Job_Type_Category			-- NVarChar(64) NULL
		,O.Org_Name						-- NVarChar(256) NOT NULL
		,O.Name_Prefix					-- NVarChar(16) NULL
		,O.Name_Suffix					-- NVarChar(16) NULL
		,O.Alternate_Names				-- NVarChar(1024) NULL
		,OS.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,OS.Segment_Name				-- NVarChar(256) NOT NULL
		,P.First_Name					-- NVarChar(64) NOT NULL
		,P.Last_Name					-- NVarChar(64) NOT NULL
		,P.Name_Prefix					-- NVarChar(16) NULL
		,P.Name_Suffix					-- NVarChar(16) NULL
		,P.Nicknames					-- NVarChar(256) NULL
		,PS.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,PS.Segment_Name				-- NVarChar(256) NOT NULL
		,R.Updated_On					-- SmallDateTime = NULL
		,R.Updated_By					-- NVarChar(128) = NULL
	FROM CRM.Relation_Orgs_Pers R
		INNER JOIN CRM.Organizations O ON R.Organization_ID = O.Organization_ID
		INNER JOIN CRM.Persons P ON R.Person_ID = P.Person_ID
		INNER JOIN CRM.Orgs_Segment_Names OS ON O.Segment_ID = OS.Segment_ID
		INNER JOIN CRM.Pers_Segment_Names PS ON P.Segment_ID = PS.Segment_ID
		LEFT JOIN CRM.Relation_Orgs_Pers_Job_Types C ON R.Job_Type_ID = C.Job_Type_ID
	WHERE Relation_ID = @RelationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Ins_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Ins_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Ins_1
	(
	@RelationID BigInt OUTPUT
	,@OrganizationID Int
	,@PersonID Int
	,@PositionTitle NVarChar(256) = NULL
	,@JobTypeID SmallInt = NULL
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	IF (1 < (SELECT COUNT(*) FROM CRM.Relation_Orgs_Pers WHERE Organization_ID = @OrganizationID AND Person_ID = @PersonID))
		DELETE FROM CRM.Relation_Orgs_Pers WHERE Organization_ID = @OrganizationID AND Person_ID = @PersonID
	SET @RelationID = (SELECT TOP 1 Relation_ID FROM CRM.Relation_Orgs_Pers WHERE Organization_ID = @OrganizationID AND Person_ID = @PersonID)
	IF (@RelationID IS NULL)
		BEGIN
			INSERT INTO CRM.Relation_Orgs_Pers
				(
				Organization_ID
				,Person_ID
				,Position_Title
				,Job_Type_ID
				,Updated_On
				,Updated_By
				)
			VALUES
				(
				@OrganizationID
				,@PersonID
				,@PositionTitle
				,@JobTypeID
				,@UpdatedOn
				,@UpdatedBy
				)
			SET @RelationID = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE CRM.Relation_Orgs_Pers
			SET
				Position_Title = @PositionTitle
				,Job_Type_ID = @JobTypeID
				,Updated_On = @UpdatedOn
				,Updated_By = @UpdatedBy
			WHERE Relation_ID = @RelationID
		END
	SELECT CAST(@RelationID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Upd_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Upd_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Upd_1
	(
	@RelationID BigInt OUTPUT
	,@OrganizationID Int
	,@PersonID Int
	,@PositionTitle NVarChar(256) = NULL
	,@JobTypeID SmallInt = NULL
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Relation_Orgs_Pers
	SET 
		Organization_ID = @OrganizationID
		,Person_ID = @PersonID
		,Position_Title = @PositionTitle
		,Job_Type_ID = @JobTypeID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Relation_ID = @RelationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Del_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Del_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Del_1
	(
	@RelationID BigInt
	)
AS
	DELETE FROM CRM.Relation_Orgs_Pers
	WHERE Relation_ID = @RelationID
go