﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Field_Names_Ins')
	DROP PROCEDURE CRM.Orgs_Field_Names_Ins
go
CREATE PROCEDURE CRM.Orgs_Field_Names_Ins
	(
	@FieldID SmallInt OUTPUT
	,@FieldCategory NVarChar(64) = NULL
	,@FieldName NVarChar(64)
	,@FieldComments NVarChar(MAX) = NULL
	,@IsUnique Bit = 0
	,@IsRetired Bit = 0
	,@FieldTypeID TinyInt
	)
AS
	INSERT INTO CRM.Orgs_Field_Names
		(
		Field_Category
		,Field_Name
		,Field_Comments
		,Is_Unique
		,Is_Retired
		,Field_Type_ID
		)
	VALUES
		(
		@FieldCategory
		,@FieldName
		,@FieldComments
		,@IsUnique
		,@IsRetired
		,@FieldTypeID
		)
	SET @FieldID = SCOPE_IDENTITY()
	SELECT CAST(@FieldID AS SmallInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Field_Names_Upd')
	DROP PROCEDURE CRM.Orgs_Field_Names_Upd
go
CREATE PROCEDURE CRM.Orgs_Field_Names_Upd
	(
	@FieldID SmallInt
	,@FieldCategory NVarChar(64) = NULL
	,@FieldName NVarChar(64)
	,@FieldComments NVarChar(MAX) = NULL
	,@IsUnique Bit = 0
	,@IsRetired Bit = 0
	,@FieldTypeID TinyInt
	)
AS
	UPDATE CRM.Orgs_Field_Names
	SET
		Field_Category = @FieldCategory
		,Field_Name = @FieldName
		,Field_Comments = @FieldComments
		,Is_Unique = @IsUnique
		,Is_Retired = @IsRetired
		,Field_Type_ID = @FieldTypeID
	WHERE Field_ID = @FieldID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Field_Names_Sel')
	DROP PROCEDURE CRM.Orgs_Field_Names_Sel
go
CREATE PROCEDURE CRM.Orgs_Field_Names_Sel
	(
	@FieldID SmallInt
	)
AS
	SELECT TOP 1
		Field_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Field_Category				-- NVarChar(64) NULL
		,Field_Name					-- NVarChar(64) NOT NULL
		,Field_Comments				-- NVarChar(MAX) NULL
		,Is_Unique					-- Bit NOT NULL DEFAULT 0
		,Is_Retired					-- Bit NOT NULL DEFAULT 0
		,Field_Type_ID				-- TinyInt NOT NULL FOREIGN KEY REFERENCES App.Field_Types(Field_Type_ID)
	FROM CRM.Orgs_Field_Names
	WHERE Field_ID = @FieldID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Field_Names_Grid_1')
	DROP PROCEDURE CRM.Orgs_Field_Names_Grid_1
go
CREATE PROCEDURE CRM.Orgs_Field_Names_Grid_1
	(
	@FieldTypeID TinyInt = NULL
	)
AS
	IF (@FieldTypeID IS NULL)
		SELECT
			Field_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
			,Field_Category				-- NVarChar(64) NULL
			,Field_Name					-- NVarChar(64) NOT NULL
			,Field_Comments				-- NVarChar(MAX) NULL
			,Is_Unique					-- Bit NOT NULL DEFAULT 0
			,Is_Retired					-- Bit NOT NULL DEFAULT 0
			,Field_Type_ID				-- TinyInt NOT NULL FOREIGN KEY REFERENCES App.Field_Types(Field_Type_ID)
		FROM CRM.Orgs_Field_Names
		ORDER BY Field_Type_ID, Field_Category, Field_Name, Field_ID
	ELSE
		SELECT
			Field_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
			,Field_Category				-- NVarChar(64) NULL
			,Field_Name					-- NVarChar(64) NOT NULL
			,Field_Comments				-- NVarChar(MAX) NULL
			,Is_Unique					-- Bit NOT NULL DEFAULT 0
			,Is_Retired					-- Bit NOT NULL DEFAULT 0
			,Field_Type_ID				-- TinyInt NOT NULL FOREIGN KEY REFERENCES App.Field_Types(Field_Type_ID)
		FROM CRM.Orgs_Field_Names
		WHERE Field_Type_ID = @FieldTypeID
		ORDER BY Field_Category, Field_Name, Field_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Field_Names_Grid_2')
	DROP PROCEDURE CRM.Orgs_Field_Names_Grid_2
go
CREATE PROCEDURE CRM.Orgs_Field_Names_Grid_2
	(
	@FieldTypeID TinyInt
	)
AS
	SELECT
		Field_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Field_Category				-- NVarChar(64) NULL
		,Field_Name					-- NVarChar(64) NOT NULL
		,Field_Comments				-- NVarChar(MAX) NULL
		,Is_Unique					-- Bit NOT NULL DEFAULT 0
		,Is_Retired					-- Bit NOT NULL DEFAULT 0
		,Field_Type_ID				-- TinyInt NOT NULL FOREIGN KEY REFERENCES App.Field_Types(Field_Type_ID)
	FROM CRM.Orgs_Field_Names
	WHERE Is_Retired = 0
		AND Field_Type_ID = @FieldTypeID
	ORDER BY Field_Category, Field_Name, Field_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Field_Names_Grid_3')
	DROP PROCEDURE CRM.Orgs_Field_Names_Grid_3
go
CREATE PROCEDURE CRM.Orgs_Field_Names_Grid_3
	(
	@FieldTypeID TinyInt
	)
AS
	SELECT DISTINCT
		Field_Category			-- NVarChar(64) NULL
	FROM CRM.Orgs_Field_Names
	WHERE Is_Retired = 0
		AND Field_Type_ID = @FieldTypeID
	ORDER BY Field_Category
go