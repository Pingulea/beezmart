﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Segment_Names_Grid_1')
	DROP PROCEDURE CRM.Orgs_Segment_Names_Grid_1
go
CREATE PROCEDURE CRM.Orgs_Segment_Names_Grid_1
AS
	SELECT
		Segment_ID					-- TinyInt NOT NULL PRIMARY KEY
		,Segment_Name				-- NVarChar(64) NOT NULL
		,Segment_Comments			-- NVarChar(MAX) NULL
	FROM CRM.Orgs_Segment_Names
	ORDER BY Segment_Name
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Segment_Names_Grid_2')
	DROP PROCEDURE CRM.Orgs_Segment_Names_Grid_2
go
CREATE PROCEDURE CRM.Orgs_Segment_Names_Grid_2
AS
	SELECT
		Segment_ID					-- TinyInt NOT NULL PRIMARY KEY
		,Segment_Name				-- NVarChar(64) NOT NULL
		,Segment_Comments			-- NVarChar(MAX) NULL
		,Organizations_Count = ISNULL((SELECT COUNT(*) FROM CRM.Organizations O WHERE O.Segment_ID = S.Segment_ID), 0)
	FROM CRM.Orgs_Segment_Names S
	ORDER BY Segment_Name, Segment_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Segment_Names_Sel')
	DROP PROCEDURE CRM.Orgs_Segment_Names_Sel
go
CREATE PROCEDURE CRM.Orgs_Segment_Names_Sel
	(
	@SegmentID TinyInt
	)
AS
	SELECT TOP 1
		Segment_ID					-- TinyInt NOT NULL PRIMARY KEY
		,Segment_Name				-- NVarChar(64) NOT NULL
		,Segment_Comments			-- NVarChar(MAX) NULL
		,Organizations_Count = ISNULL((SELECT COUNT(*) FROM CRM.Organizations O WHERE O.Segment_ID = @SegmentID), 0)
	FROM CRM.Orgs_Segment_Names S
	WHERE S.Segment_ID = @SegmentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Segment_Names_Ins')
	DROP PROCEDURE CRM.Orgs_Segment_Names_Ins
go
CREATE PROCEDURE CRM.Orgs_Segment_Names_Ins
	(
	@SegmentID TinyInt OUTPUT
	,@SegmentName NVarChar(64)
	,@SegmentComments NVarChar(MAX) = NULL
	)
AS
	SET @SegmentID = 0
	WHILE (EXISTS (SELECT * FROM CRM.Orgs_Segment_Names WHERE Segment_ID = @SegmentID))
		SET @SegmentID = @SegmentID + 1
	IF (@SegmentID < 256)
		INSERT INTO CRM.Orgs_Segment_Names
			(
			Segment_ID
			,Segment_Name
			,Segment_Comments
			)
		VALUES
			(
			@SegmentID
			,@SegmentName
			,@SegmentComments
			)
	SELECT CAST(@SegmentID AS TinyInt)
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Segment_Names_Upd')
	DROP PROCEDURE CRM.Orgs_Segment_Names_Upd
go
CREATE PROCEDURE CRM.Orgs_Segment_Names_Upd
	(
	@SegmentID TinyInt
	,@SegmentName NVarChar(64)
	,@SegmentComments NVarChar(MAX) = NULL
	)
AS
	UPDATE CRM.Orgs_Segment_Names
	SET
		Segment_Name = @SegmentName
		,Segment_Comments = @SegmentComments
	WHERE Segment_ID = @SegmentID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Segment_Names_Del')
	DROP PROCEDURE CRM.Orgs_Segment_Names_Del
go
CREATE PROCEDURE CRM.Orgs_Segment_Names_Del
	(
	@SegmentID TinyInt
	)
AS
	IF (NOT EXISTS(SELECT * FROM CRM.Organizations WHERE Segment_ID = @SegmentID))
		DELETE FROM CRM.Orgs_Segment_Names WHERE Segment_ID = @SegmentID
go