﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_States_Provinces_Grid_1')
	DROP PROCEDURE CRM.App_States_Provinces_Grid_1
go
CREATE PROCEDURE CRM.App_States_Provinces_Grid_1
(
	@CountryID Char(3)
)
AS
	SELECT
		State_Province_ID								                    -- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Country_ID										                    -- Char(3) NOT NULL DEFAULT 'ROM'
		,ISNULL(Region, State_Province_Name) AS Region          			-- NVarChar(128) NOT NULL
		,State_Province_Abbr							                    -- NVarChar(8) NOT NULL
		,State_Province_Name							                    -- NVarChar(128) NOT NULL
	FROM CRM.App_States_Provinces
	WHERE Country_ID = @CountryID
	ORDER BY State_Province_Name, State_Province_Abbr, State_Province_ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_States_Provinces_Grid_2')
	DROP PROCEDURE CRM.App_States_Provinces_Grid_2
go
CREATE PROCEDURE CRM.App_States_Provinces_Grid_2
(
	@CountryID Char(3)
	,@StateProvinceName NVarChar(128) = '%'
)
AS
	SELECT
		State_Province_ID								                    -- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Country_ID										                    -- Char(3) NOT NULL DEFAULT 'ROM'
		,ISNULL(Region, State_Province_Name) AS Region          			-- NVarChar(128) NOT NULL
		,State_Province_Abbr							                    -- NVarChar(8) NOT NULL
		,State_Province_Name							                    -- NVarChar(128) NOT NULL
	FROM CRM.App_States_Provinces
	WHERE Country_ID = @CountryID
		AND State_Province_Name LIKE @StateProvinceName
	ORDER BY State_Province_Name, State_Province_Abbr, State_Province_ID
go