﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_Countries_Grid_1')
	DROP PROCEDURE CRM.App_Countries_Grid_1
go
CREATE PROCEDURE CRM.App_Countries_Grid_1
AS
	SELECT
		Country_ID				-- Char(3) NOT NULL PRIMARY KEY
		,Code					-- Char(2) NULL
		,Code_UN				-- VarChar(4) NULL
		,Country_Name			-- NVarChar(128) NOT NULL
		,Show					-- Bit NOT NULL DEFAULT 0
	FROM CRM.App_Countries
	WHERE Show = 1
	ORDER BY Country_Name
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'App_Countries_Grid_2')
	DROP PROCEDURE CRM.App_Countries_Grid_2
go
CREATE PROCEDURE CRM.App_Countries_Grid_2
AS
	SELECT
		Country_ID				-- Char(3) NOT NULL PRIMARY KEY
		,Code					-- Char(2) NULL
		,Code_UN				-- VarChar(4) NULL
		,Country_Name			-- NVarChar(128) NOT NULL
		,Show					-- Bit NOT NULL DEFAULT 0
	FROM CRM.App_Countries
	ORDER BY Country_Name
go