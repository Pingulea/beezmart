﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Grid_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_1
AS
	SELECT DISTINCT
		Job_Type_Category			-- NVarChar(64) NULL
	FROM CRM.Relation_Orgs_Pers_Job_Types
	WHERE Discontinued = 0
	ORDER BY Job_Type_Category
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Grid_2')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_2
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_2
	(
	@JobTypeCategory NVarChar(64) = NULL
	)
AS
	IF (@JobTypeCategory IS NULL)
		SELECT
			Job_Type_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
			,Job_Type_Category			-- NVarChar(64) NULL
			,Job_Type_Name				-- NVarChar(64) NOT NULL
			,Job_Type_Comments			-- NVarChar(MAX) NULL
			,Discontinued				-- Bit NOT NULL DEFAULT 0
			,Updated_On					-- SmallDateTime = NULL
			,Updated_By					-- NVarChar(128) = NULL
		FROM Relation_Orgs_Pers_Job_Types
		WHERE Discontinued = 0
			AND Job_Type_Category IS NULL
		ORDER BY Job_Type_Name
	ELSE
		SELECT
			Job_Type_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
			,Job_Type_Category			-- NVarChar(64) NULL
			,Job_Type_Name				-- NVarChar(64) NOT NULL
			,Job_Type_Comments			-- NVarChar(MAX) NULL
			,Discontinued				-- Bit NOT NULL DEFAULT 0
			,Updated_On					-- SmallDateTime = NULL
			,Updated_By					-- NVarChar(128) = NULL
		FROM Relation_Orgs_Pers_Job_Types
		WHERE Discontinued = 0
			AND Job_Type_Category = @JobTypeCategory
		ORDER BY Job_Type_Name
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Grid_3')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_3
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_3
AS
	SELECT
		Job_Type_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Job_Type_Category			-- NVarChar(64) NULL
		,Job_Type_Name				-- NVarChar(64) NOT NULL
		,Job_Type_Comments			-- NVarChar(MAX) NULL
		,Discontinued				-- Bit NOT NULL DEFAULT 0
		,Updated_On					-- SmallDateTime = NULL
		,Updated_By					-- NVarChar(128) = NULL
	FROM Relation_Orgs_Pers_Job_Types
		WHERE Discontinued = 0
	ORDER BY Job_Type_Category, Job_Type_Name
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Grid_4')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_4
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Grid_4
AS
	SELECT
		Job_Type_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Job_Type_Category			-- NVarChar(64) NULL
		,Job_Type_Name				-- NVarChar(64) NOT NULL
		,Job_Type_Comments			-- NVarChar(MAX) NULL
		,Discontinued				-- Bit NOT NULL DEFAULT 0
		,Updated_On					-- SmallDateTime = NULL
		,Updated_By					-- NVarChar(128) = NULL
	FROM Relation_Orgs_Pers_Job_Types
	ORDER BY Job_Type_Category, Job_Type_Name
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Sel_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Sel_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Sel_1
	(
	@JobTypeID SmallInt
	)
AS
	SELECT TOP 1
		Job_Type_ID					-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Job_Type_Category			-- NVarChar(64) NULL
		,Job_Type_Name				-- NVarChar(64) NOT NULL
		,Job_Type_Comments			-- NVarChar(MAX) NULL
		,Discontinued				-- Bit NOT NULL DEFAULT 0
		,Updated_On					-- SmallDateTime = NULL
		,Updated_By					-- NVarChar(128) = NULL
	FROM CRM.Relation_Orgs_Pers_Job_Types R
	WHERE Job_Type_ID = @JobTypeID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Ins_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Ins_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Ins_1
	(
	@JobTypeID SmallInt OUTPUT
	,@JobTypeCategory NVarChar(64) = NULL
	,@JobTypeName NVarChar(64)
	,@JobTypeComments NVarChar(MAX) = NULL
	,@Discontinued Bit = 0
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Relation_Orgs_Pers_Job_Types
		(
		Job_Type_Category
		,Job_Type_Name
		,Job_Type_Comments
		,Discontinued
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@JobTypeCategory
		,@JobTypeName
		,@JobTypeComments
		,@Discontinued
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @JobTypeID = SCOPE_IDENTITY()
	SELECT CAST(@JobTypeID AS SmallInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Upd_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Upd_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Upd_1
	(
	@JobTypeID SmallInt
	,@JobTypeCategory NVarChar(64) = NULL
	,@JobTypeName NVarChar(64)
	,@JobTypeComments NVarChar(MAX) = NULL
	,@Discontinued Bit = 0
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Relation_Orgs_Pers_Job_Types
	SET
		Job_Type_Category = @JobTypeCategory
		,Job_Type_Name = @JobTypeName
		,Job_Type_Comments = @JobTypeComments
		,Discontinued = @Discontinued
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Job_Type_ID = @JobTypeID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Relation_Orgs_Pers_Job_Types_Del_1')
	DROP PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Del_1
go
CREATE PROCEDURE CRM.Relation_Orgs_Pers_Job_Types_Del_1
	(
	@JobTypeID BigInt
	,@Discontinued Bit = 1
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Relation_Orgs_Pers_Job_Types
	SET
		Discontinued = @Discontinued
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Job_Type_ID = @JobTypeID
go