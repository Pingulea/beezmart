﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Reports_Email_Addresses_Orgs')
	DROP PROCEDURE CRM.Reports_Email_Addresses_Orgs
go
CREATE PROCEDURE CRM.Reports_Email_Addresses_Orgs
AS
	SELECT
		E.Contact_ID				
		,E.Organization_ID				
		,O.Org_Name								
		,E.Site_ID								
		,S.Site_Name						
		,S.City										
		,E.Contact					
		,T.Contact_Type_Name		
		,E.Comments					
		,E.Updated_On				
		,E.Updated_By				
	FROM CRM.Orgs_Contacts E
		INNER JOIN CRM.App_Contact_Types T ON E.Contact_Type_ID = T.Contact_Type_ID
		INNER JOIN CRM.Organizations O ON E.Organization_ID = O.Organization_ID
		LEFT JOIN CRM.Orgs_Sites S ON E.Organization_ID = S.Organization_ID
	WHERE E.Contact_Type_ID = 1
		AND E.Contact_Status_ID IN (1, 2, 6, 7)		-- Unconfirmed, Confirmed, To-be-changed, Problematic
go