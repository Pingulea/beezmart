﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_String_Fields_Ins')
	DROP PROCEDURE CRM.Pers_String_Fields_Ins
go
CREATE PROCEDURE CRM.Pers_String_Fields_Ins
	(
	@RecordID BigInt OUTPUT
	,@PersonID Int
	,@FieldID SmallInt
	,@FieldValue NVarChar(256)
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())

	DECLARE @IsUnique BIT
	SET @IsUnique = (SELECT TOP 1 Is_Unique FROM CRM.Pers_Field_Names WHERE Field_ID = @FieldID)
	IF (@IsUnique = 1) SET @RecordID = (SELECT TOP 1 Record_ID FROM CRM.Pers_String_Fields WHERE Person_ID = @PersonID AND Field_ID = @FieldID)

	IF (@RecordID IS NULL)
		BEGIN
			INSERT INTO CRM.Pers_String_Fields
				(
				Person_ID
				,Field_ID
				,Field_Value
				,Updated_On
				,Updated_By
				)
			VALUES
				(
				@PersonID
				,@FieldID
				,@FieldValue
				,@UpdatedOn
				,@UpdatedBy
				)
			SET @RecordID = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE CRM.Pers_String_Fields
			SET
				Field_Value = @FieldValue
				,Updated_On = @UpdatedOn
				,Updated_By = @UpdatedBy
			WHERE Record_ID = @RecordID
		END

	SELECT CAST(@RecordID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_String_Fields_Upd')
	DROP PROCEDURE CRM.Pers_String_Fields_Upd
go
CREATE PROCEDURE CRM.Pers_String_Fields_Upd
	(
	@RecordID BigInt
	,@FieldID SmallInt
	,@FieldValue NVarChar(256)
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())

	DECLARE @IsUnique BIT
	SET @IsUnique = (SELECT TOP 1 Is_Unique FROM CRM.Pers_Field_Names WHERE Field_ID = @FieldID)

	IF (@IsUnique = 1)
		BEGIN
			DECLARE @PersonID Int
			SET @PersonID = (SELECT TOP 1 Person_ID FROM CRM.Pers_String_Fields WHERE Record_ID = @RecordID)
			DELETE FROM CRM.Pers_String_Fields
			WHERE Person_ID = @PersonID
				AND Field_ID = @FieldID
				AND Record_ID != @RecordID
		END

	UPDATE CRM.Pers_String_Fields
	SET
		Field_ID = @FieldID
		,Field_Value = @FieldValue
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_String_Fields_Del')
	DROP PROCEDURE CRM.Pers_String_Fields_Del
go
CREATE PROCEDURE CRM.Pers_String_Fields_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM CRM.Pers_String_Fields
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_String_Fields_Sel')
	DROP PROCEDURE CRM.Pers_String_Fields_Sel
go
CREATE PROCEDURE CRM.Pers_String_Fields_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		Record_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,V.Field_ID					-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
		,Field_Category				-- NVarChar(64) NULL
		,Field_Name					-- NVarChar(64) NOT NULL
		,Field_Value				-- NVarChar(256) NOT NULL
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_String_Fields V
		INNER JOIN CRM.Pers_Field_Names N ON V.Field_ID = N.Field_ID
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_String_Fields_Grid_1')
	DROP PROCEDURE CRM.Pers_String_Fields_Grid_1
go
CREATE PROCEDURE CRM.Pers_String_Fields_Grid_1
	(
	@PersonID Int
	)
AS
	SELECT
		Record_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,V.Field_ID					-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
		,Field_Category				-- NVarChar(64) NULL
		,Field_Name					-- NVarChar(64) NOT NULL
		,Field_Value				-- NVarChar(256) NOT NULL
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_String_Fields V
		INNER JOIN CRM.Pers_Field_Names N ON V.Field_ID = N.Field_ID
	WHERE Person_ID = @PersonID
	ORDER BY Field_Category, Field_Name, Updated_On DESC
go