﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts_Ins')
	DROP PROCEDURE CRM.Pers_Contacts_Ins
go
CREATE PROCEDURE CRM.Pers_Contacts_Ins
	(
	@ContactID BigInt OUTPUT
	,@PersonID Int
	,@SiteID Int = NULL
	,@Contact NVarChar(256)
	,@Comments NVarChar(256) = NULL
	,@ContactTypeID TinyInt = 1
	,@ContactStatusID TinyInt = 1
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Pers_Contacts
		(
		Person_ID
		,Site_ID
		,Contact
		,Comments
		,Contact_Type_ID
		,Contact_Status_ID
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@PersonID
		,@SiteID
		,@Contact
		,@Comments
		,@ContactTypeID
		,@ContactStatusID
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @ContactID = SCOPE_IDENTITY()
	SELECT CAST(@ContactID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts_Upd')
	DROP PROCEDURE CRM.Pers_Contacts_Upd
go
CREATE PROCEDURE CRM.Pers_Contacts_Upd
	(
	@ContactID BigInt
	,@PersonID Int
	,@SiteID Int = NULL
	,@Contact NVarChar(256)
	,@Comments NVarChar(256) = NULL
	,@ContactTypeID TinyInt = 1
	,@ContactStatusID TinyInt = 1
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Pers_Contacts
	SET
		Person_ID = @PersonID
		,Site_ID = @SiteID
		,Contact = @Contact
		,Comments = @Comments
		,Contact_Type_ID = @ContactTypeID
		,Contact_Status_ID = @ContactStatusID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Contact_ID = @ContactID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts_Del')
	DROP PROCEDURE CRM.Pers_Contacts_Del
go
CREATE PROCEDURE CRM.Pers_Contacts_Del
	(
	@ContactID BigInt
	)
AS
	DELETE FROM CRM.Pers_Contacts
	WHERE Contact_ID = @ContactID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts_Sel')
	DROP PROCEDURE CRM.Pers_Contacts_Sel
go
CREATE PROCEDURE CRM.Pers_Contacts_Sel
	(
	@ContactID BigInt
	)
AS
	SELECT TOP 1
		Contact_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Site_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
		,Contact					-- NVarChar(256) NOT NULL
		,Comments					-- NVarChar(256) NULL
		,Contact_Type_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_Contacts C
	WHERE Contact_ID = @ContactID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts_Grid_1')
	DROP PROCEDURE CRM.Pers_Contacts_Grid_1
go
CREATE PROCEDURE CRM.Pers_Contacts_Grid_1
	(
	@PersonID Int
	)
AS
	SELECT
		Contact_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Site_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
		,Contact					-- NVarChar(256) NOT NULL
		,Comments					-- NVarChar(256) NULL
		,Contact_Type_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_Contacts C
	WHERE Person_ID = @PersonID
	ORDER BY Contact_Type_ID, Updated_On DESC
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Contacts_Grid_2')
	DROP PROCEDURE CRM.Pers_Contacts_Grid_2
go
CREATE PROCEDURE CRM.Pers_Contacts_Grid_2
	(
	@PersonID Int
	)
AS
	DECLARE @SiteIdHome Int, @SiteIdBilling Int, @SiteIdShipping Int
	SELECT @SiteIdHome = Site_ID_Home, @SiteIdBilling = Site_ID_Billing, @SiteIdShipping = Site_ID_Shipping
	FROM CRM.Persons WHERE Person_ID = @PersonID

	SELECT TOP 1
		Contact_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Site_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
		,Contact					-- NVarChar(256) NOT NULL
		,Comments					-- NVarChar(256) NULL
		,Contact_Type_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_Contacts C
	WHERE Person_ID = @PersonID
		AND Site_ID NOT IN (@SiteIdHome, @SiteIdBilling, @SiteIdShipping)
	ORDER BY Contact_Type_ID, Updated_On DESC
go