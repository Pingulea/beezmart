﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = N'CRM' AND Name = N'Dashboard_Statistics_1')
	DROP PROCEDURE CRM.Dashboard_Statistics_1
go
CREATE PROCEDURE CRM.Dashboard_Statistics_1
AS
	DECLARE @CountOfOrgs Int
	DECLARE @CountOfPers Int
	DECLARE @CountOfEmails Int

	SET @CountOfOrgs = ISNULL((SELECT COUNT(*) FROM CRM.Organizations), 0)
	SET @CountOfPers = ISNULL((SELECT COUNT(*) FROM CRM.Persons), 0)
	--	Count 'confirmed' and 'unconfirmed' e-mails, 'to-be-changed' or 'problematic'
	SET @CountOfEmails = 
		ISNULL((SELECT COUNT(*) FROM CRM.Orgs_Contacts WHERE Contact_Type_ID = 1 AND Contact_Status_ID IN (1, 2, 6, 7)), 0)		-- Unconfirmed, Confirmed, To-be-changed, Problematic
		+
		ISNULL((SELECT COUNT(*) FROM CRM.Pers_Contacts WHERE Contact_Type_ID = 1 AND Contact_Status_ID IN (1, 2, 6, 7)), 0)		-- Unconfirmed, Confirmed, To-be-changed, Problematic

	SELECT
		@CountOfOrgs			AS Count_Of_Organizations	 		-- Int
		,@CountOfPers			AS Count_Of_Persons	 				-- Int
		,@CountOfEmails			AS Count_Of_Email_Addresses 		-- Int
