﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activities_Ins')
	DROP PROCEDURE CRM.Pers_Activities_Ins
go
CREATE PROCEDURE CRM.Pers_Activities_Ins
	(
	@ActivityID BigInt OUTPUT
	,@PersonID Int
	,@ActivityStatusID TinyInt = 10
	,@ActivityStatusComment NVarChar(256) = NULL
	,@ActivityTypeID SmallInt
	,@Title NVarChar(1024)
	,@Details NVarChar(MAX) = NULL
	,@ScheduledFor SmallDateTime = NULL
	,@ReminderOn SmallDateTime = NULL
	,@StartedOn SmallDateTime = NULL
	,@CompletedOn SmallDateTime = NULL
	,@RequestedBy NVarChar(128) = NULL
	,@CompletedBy NVarChar(128) = NULL
	,@Billable Bit = 0
	,@InvoiceIDs VarChar(256) = NULL
	,@DurationInMinutes Int = NULL
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Pers_Activities
		(
		Person_ID
		,Activity_Status_ID
		,Activity_Status_Comment
		,Activity_Type_ID
		,Title
		,Details
		,Scheduled_For
		,Reminder_On
		,Started_On
		,Completed_On
		,Requested_By
		,Completed_By
		,Billable
		,Invoice_IDs
		,Duration_In_Minutes
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@PersonID
		,@ActivityStatusID
		,@ActivityStatusComment
		,@ActivityTypeID
		,@Title
		,@Details
		,@ScheduledFor
		,@ReminderOn
		,@StartedOn
		,@CompletedOn
		,@RequestedBy
		,@CompletedBy
		,@Billable
		,@InvoiceIDs
		,@DurationInMinutes
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @ActivityID = SCOPE_IDENTITY()
	SELECT CAST(@ActivityID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activities_Upd')
	DROP PROCEDURE CRM.Pers_Activities_Upd
go
CREATE PROCEDURE CRM.Pers_Activities_Upd
	(
	@ActivityID BigInt
	,@PersonID Int
	,@ActivityStatusID TinyInt = 10
	,@ActivityStatusComment NVarChar(256) = NULL
	,@ActivityTypeID SmallInt
	,@Title NVarChar(1024)
	,@Details NVarChar(MAX) = NULL
	,@ScheduledFor SmallDateTime = NULL
	,@ReminderOn SmallDateTime = NULL
	,@StartedOn SmallDateTime = NULL
	,@CompletedOn SmallDateTime = NULL
	,@RequestedBy NVarChar(128) = NULL
	,@CompletedBy NVarChar(128) = NULL
	,@Billable Bit = 0
	,@InvoiceIDs VarChar(256) = NULL
	,@DurationInMinutes Int = NULL
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Pers_Activities
	SET
		Person_ID = @PersonID
		,Activity_Status_ID = @ActivityStatusID
		,Activity_Status_Comment = @ActivityStatusComment
		,Activity_Type_ID = @ActivityTypeID
		,Title = @Title
		,Details = @Details
		,Scheduled_For = @ScheduledFor
		,Reminder_On = @ReminderOn
		,Started_On = @StartedOn
		,Completed_On = @CompletedOn
		,Requested_By = @RequestedBy
		,Completed_By = @CompletedBy
		,Billable = @Billable
		,Invoice_IDs = @InvoiceIDs
		,Duration_In_Minutes = @DurationInMinutes
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activities_Del')
	DROP PROCEDURE CRM.Pers_Activities_Del
go
CREATE PROCEDURE CRM.Pers_Activities_Del
	(
	@ActivityID BigInt
	)
AS
	DELETE FROM CRM.Pers_Activities
	WHERE Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activities_Sel')
	DROP PROCEDURE CRM.Pers_Activities_Sel
go
CREATE PROCEDURE CRM.Pers_Activities_Sel
	(
	@ActivityID BigInt
	)
AS
	SELECT TOP 1
		Activity_ID							-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Person_ID							-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Activity_Status_ID					-- TinyInt NOT NULL FOREIGN KEY REFERENCES CRM.App_Activity_Status(Activity_Status_ID)
		,Activity_Status_Comment			-- NVarChar(256) NULL
		,Activity_Type_ID					-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Activity_Types(Activity_Type_ID)
		,Title								-- NVarChar(1024) NOT NULL
		,Details							-- NVarChar(MAX) NULL
		,Scheduled_For						-- SmallDateTime NULL
		,Reminder_On						-- SmallDateTime NULL
		,Started_On							-- SmallDateTime NULL
		,Completed_On						-- SmallDateTime NULL
		,Requested_By						-- NVarChar(128) NULL
		,Completed_By						-- NVarChar(128) NULL
		,Billable							-- Bit NOT NULL DEFAULT 0
		,Invoice_IDs						-- VarChar(256) NULL
		,Duration_In_Minutes				-- Int NULL
		,Updated_On							-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By							-- NVarChar(128) NULL
	FROM CRM.Pers_Activities C
	WHERE Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Activities_Grid_1')
	DROP PROCEDURE CRM.Pers_Activities_Grid_1
go
CREATE PROCEDURE CRM.Pers_Activities_Grid_1
	(
	@PersonID Int
	)
AS
	SELECT
		Activity_ID							-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,Person_ID							-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Activity_Status_ID					-- TinyInt NOT NULL FOREIGN KEY REFERENCES CRM.App_Activity_Status(Activity_Status_ID)
		,Activity_Status_Comment			-- NVarChar(256) NULL
		,Activity_Type_ID					-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Pers_Activity_Types(Activity_Type_ID)
		,Title								-- NVarChar(1024) NOT NULL
		,Details							-- NVarChar(MAX) NULL
		,Scheduled_For						-- SmallDateTime NULL
		,Reminder_On						-- SmallDateTime NULL
		,Started_On							-- SmallDateTime NULL
		,Completed_On						-- SmallDateTime NULL
		,Requested_By						-- NVarChar(128) NULL
		,Completed_By						-- NVarChar(128) NULL
		,Billable							-- Bit NOT NULL DEFAULT 0
		,Invoice_IDs						-- VarChar(256) NULL
		,Duration_In_Minutes				-- Int NULL
		,Updated_On							-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By							-- NVarChar(128) NULL
	FROM CRM.Pers_Activities C
	WHERE Person_ID = @PersonID
	ORDER BY Updated_On DESC
go