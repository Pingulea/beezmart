﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Flag_Fields_Ins')
	DROP PROCEDURE CRM.Orgs_Flag_Fields_Ins
go
CREATE PROCEDURE CRM.Orgs_Flag_Fields_Ins
	(
	@RecordID BigInt OUTPUT
	,@OrganizationID Int
	,@FieldID SmallInt
	,@FieldValue NVarChar(256) = NULL
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	SET @RecordID = (SELECT TOP 1 Record_ID FROM CRM.Orgs_Flag_Fields WHERE Organization_ID = @OrganizationID AND Field_ID = @FieldID)

	IF (@RecordID IS NULL)
		BEGIN
			INSERT INTO CRM.Orgs_Flag_Fields
				(
				Organization_ID
				,Field_ID
				,Field_Value
				,Updated_On
				,Updated_By
				)
			VALUES
				(
				@OrganizationID
				,@FieldID
				,@FieldValue
				,@UpdatedOn
				,@UpdatedBy
				)
			SET @RecordID = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE CRM.Orgs_Flag_Fields
			SET
				Field_Value = @FieldValue
				,Updated_On = @UpdatedOn
				,Updated_By = @UpdatedBy
			WHERE Record_ID = @RecordID
		END

	SELECT CAST(@RecordID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Flag_Fields_Upd')
	DROP PROCEDURE CRM.Orgs_Flag_Fields_Upd
go
CREATE PROCEDURE CRM.Orgs_Flag_Fields_Upd
	(
	@RecordID BigInt
	,@FieldID SmallInt
	,@FieldValue NVarChar(256) = NULL
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())

	DECLARE @OrganizationID Int
	SET @OrganizationID = (SELECT TOP 1 Organization_ID FROM CRM.Orgs_Flag_Fields WHERE Record_ID = @RecordID)

	DELETE FROM CRM.Orgs_Flag_Fields
	WHERE Organization_ID = @OrganizationID
		AND Field_ID = @FieldID
		AND Record_ID != @RecordID

	UPDATE CRM.Orgs_Flag_Fields
	SET
		Field_ID = @FieldID
		,Field_Value = @FieldValue
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Flag_Fields_Del')
	DROP PROCEDURE CRM.Orgs_Flag_Fields_Del
go
CREATE PROCEDURE CRM.Orgs_Flag_Fields_Del
	(
	@RecordID BigInt
	)
AS
	DELETE FROM CRM.Orgs_Flag_Fields
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Flag_Fields_Sel')
	DROP PROCEDURE CRM.Orgs_Flag_Fields_Sel
go
CREATE PROCEDURE CRM.Orgs_Flag_Fields_Sel
	(
	@RecordID BigInt
	)
AS
	SELECT TOP 1
		Record_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Organization_ID			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,V.Field_ID					-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
		,Field_Category				-- NVarChar(64) NULL
		,Field_Name					-- NVarChar(64) NOT NULL
		,Field_Value				-- NVarChar(256) NULL
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Orgs_Flag_Fields V
		INNER JOIN CRM.Orgs_Field_Names N ON V.Field_ID = N.Field_ID
	WHERE Record_ID = @RecordID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Flag_Fields_Grid_1')
	DROP PROCEDURE CRM.Orgs_Flag_Fields_Grid_1
go
CREATE PROCEDURE CRM.Orgs_Flag_Fields_Grid_1
	(
	@OrganizationID Int
	)
AS
	SELECT
		Record_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Organization_ID			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,V.Field_ID					-- SmallInt NOT NULL FOREIGN KEY REFERENCES CRM.Orgs_Field_Names(Field_ID)
		,Field_Category				-- NVarChar(64) NULL
		,Field_Name					-- NVarChar(64) NOT NULL
		,Field_Value				-- NVarChar(256) NULL
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Orgs_Flag_Fields V
		INNER JOIN CRM.Orgs_Field_Names N ON V.Field_ID = N.Field_ID
	WHERE Organization_ID = @OrganizationID
	ORDER BY Field_Category, Field_Name, Updated_On DESC
go