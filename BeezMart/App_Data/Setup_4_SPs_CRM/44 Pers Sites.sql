﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Sites_Ins')
	DROP PROCEDURE CRM.Pers_Sites_Ins
go
CREATE PROCEDURE CRM.Pers_Sites_Ins
	(
	@SiteID Int OUTPUT
	,@PersonID Int
	,@ContactStatusID TinyInt = 0

	,@SiteName NVarChar(128) = NULL
	,@Country Char(3) = 'ROM'
	,@Region NVarChar(128) = NULL
	,@StateProvince NVarChar(128) = NULL
	,@City NVarChar(64)
	,@StreetAddress1 NVarChar(128) = NULL
	,@StreetAddress2 NVarChar(128) = NULL
	,@StreetAddress3 NVarChar(128) = NULL
	,@Department NVarChar(128) = NULL
	,@Directions NVarChar(128) = NULL
	,@ZIPPostCode VarChar(32) = NULL
	
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Pers_Sites
		(
		Person_ID
		,Contact_Status_ID

		,Site_Name
		,Country
		,Region
		,State_Province
		,City
		,Street_Address_1
		,Street_Address_2
		,Street_Address_3
		,Department
		,Directions
		,ZIP_Post_Code
	
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@PersonID
		,@ContactStatusID

		,@SiteName
		,@Country
		,@Region
		,@StateProvince
		,@City
		,@StreetAddress1
		,@StreetAddress2
		,@StreetAddress3
		,@Department
		,@Directions
		,@ZIPPostCode
	
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @SiteID = SCOPE_IDENTITY()
	SELECT CAST(@SiteID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Sites_Upd')
	DROP PROCEDURE CRM.Pers_Sites_Upd
go
CREATE PROCEDURE CRM.Pers_Sites_Upd
	(
	@SiteID Int
	,@PersonID Int
	,@ContactStatusID TinyInt = 0

	,@SiteName NVarChar(128) = NULL
	,@Country Char(3) = 'ROM'
	,@Region NVarChar(128) = NULL
	,@StateProvince NVarChar(128) = NULL
	,@City NVarChar(64)
	,@StreetAddress1 NVarChar(128) = NULL
	,@StreetAddress2 NVarChar(128) = NULL
	,@StreetAddress3 NVarChar(128) = NULL
	,@Department NVarChar(128) = NULL
	,@Directions NVarChar(128) = NULL
	,@ZIPPostCode VarChar(32) = NULL
	
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Pers_Sites
	SET
		Person_ID = @PersonID
		,Contact_Status_ID = @ContactStatusID

		,Site_Name = @SiteName
		,Country = @Country
		,Region = @Region
		,State_Province = @StateProvince
		,City = @City
		,Street_Address_1 = @StreetAddress1
		,Street_Address_2 = @StreetAddress2
		,Street_Address_3 = @StreetAddress3
		,Department = @Department
		,Directions = @Directions
		,ZIP_Post_Code = @ZIPPostCode
	
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Site_ID = @SiteID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Sites_Sel')
	DROP PROCEDURE CRM.Pers_Sites_Sel
go
CREATE PROCEDURE CRM.Pers_Sites_Sel
	(
	@SiteID Int
	)
AS
	SELECT TOP 1
		Site_ID						-- Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
									
		,Site_Name					-- NVarChar(128) NULL
		,Country					-- Char(3) NOT NULL DEFAULT 'ROM'
		,Region						-- NVarChar(128) NULL
		,State_Province				-- NVarChar(128) NULL
		,City						-- NVarChar(64) NOT NULL
		,Street_Address_1			-- NVarChar(128) NULL
		,Street_Address_2			-- NVarChar(128) NULL
		,Street_Address_3			-- NVarChar(128) NULL
		,Department					-- NVarChar(128) NULL
		,Directions					-- NVarChar(128) NULL
		,ZIP_Post_Code				-- VarChar(32) NULL
									 
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_Sites
	WHERE Site_ID = @SiteID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Sites_Grid_1')
	DROP PROCEDURE CRM.Pers_Sites_Grid_1
go
CREATE PROCEDURE CRM.Pers_Sites_Grid_1
	(
	@PersonID Int
	)
AS
	SELECT
		Site_ID						-- Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
									
		,Site_Name					-- NVarChar(128) NULL
		,Country					-- Char(3) NOT NULL DEFAULT 'ROM'
		,Region						-- NVarChar(128) NULL
		,State_Province				-- NVarChar(128) NULL
		,City						-- NVarChar(64) NOT NULL
		,Street_Address_1			-- NVarChar(128) NULL
		,Street_Address_2			-- NVarChar(128) NULL
		,Street_Address_3			-- NVarChar(128) NULL
		,Department					-- NVarChar(128) NULL
		,Directions					-- NVarChar(128) NULL
		,ZIP_Post_Code				-- VarChar(32) NULL
									 
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_Sites
	WHERE Person_ID = @PersonID
	ORDER BY Site_Name, State_Province, City, Street_Address_1
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Pers_Sites_Grid_2')
	DROP PROCEDURE CRM.Pers_Sites_Grid_2
go
CREATE PROCEDURE CRM.Pers_Sites_Grid_2
	(
	@PersonID Int
	)
AS
	DECLARE @SiteIdHome Int, @SiteIdBilling Int, @SiteIdShipping Int
	SELECT @SiteIdHome = Site_ID_Home, @SiteIdBilling = Site_ID_Billing, @SiteIdShipping = Site_ID_Shipping
	FROM CRM.Persons WHERE Person_ID = @PersonID

	SELECT
		Site_ID						-- Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Person_ID					-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID) ON DELETE NO ACTION ON UPDATE CASCADE
									
		,Site_Name					-- NVarChar(128) NULL
		,Country					-- Char(3) NOT NULL DEFAULT 'ROM'
		,Region						-- NVarChar(128) NULL
		,State_Province				-- NVarChar(128) NULL
		,City						-- NVarChar(64) NOT NULL
		,Street_Address_1			-- NVarChar(128) NULL
		,Street_Address_2			-- NVarChar(128) NULL
		,Street_Address_3			-- NVarChar(128) NULL
		,Department					-- NVarChar(128) NULL
		,Directions					-- NVarChar(128) NULL
		,ZIP_Post_Code				-- VarChar(32) NULL
									 
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Pers_Sites
	WHERE Person_ID = @PersonID
		AND Site_ID NOT IN (SELECT S1.Site_ID FROM CRM.Pers_Sites S1 WHERE Person_ID = @PersonID AND S1.Site_ID IN (@SiteIdHome, @SiteIdBilling, @SiteIdShipping))
	ORDER BY Site_Name, State_Province, City, Street_Address_1
go