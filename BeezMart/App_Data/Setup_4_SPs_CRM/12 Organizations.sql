﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Ins')
	DROP PROCEDURE CRM.Organizations_Ins
go
CREATE PROCEDURE CRM.Organizations_Ins
	(
	@OrganizationID				Int OUTPUT
	,@SegmentID					TinyInt = 0
	,@ParentOrgID				Int = NULL
	,@OrgName					NVarChar(256)
	,@NamePrefix				NVarChar(16) = NULL
	,@NameSuffix				NVarChar(16) = NULL
	,@AlternateNames			NVarChar(1024) = NULL
	,@CountryOfOrigin			Char(3) = 'ROM'
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Organizations
		(
		Segment_ID
		,Parent_Org_ID
		,Org_Name
		,Name_Prefix
		,Name_Suffix
		,Alternate_Names
		,Country_Of_Origin
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@SegmentID
		,@ParentOrgID
		,@OrgName
		,@NamePrefix
		,@NameSuffix
		,@AlternateNames
		,@CountryOfOrigin
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @OrganizationID = SCOPE_IDENTITY()
	SELECT CAST(@OrganizationID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Upd')
	DROP PROCEDURE CRM.Organizations_Upd
go
CREATE PROCEDURE CRM.Organizations_Upd
	(
	@OrganizationID				Int
	,@SegmentID					TinyInt = 0
	,@ParentOrgID				Int = NULL
	,@OrgName					NVarChar(256)
	,@NamePrefix				NVarChar(16) = NULL
	,@NameSuffix				NVarChar(16) = NULL
	,@AlternateNames			NVarChar(1024) = NULL
	,@CountryOfOrigin			Char(3) = 'ROM'
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Organizations
	SET
		Segment_ID = @SegmentID
		,Parent_Org_ID = @ParentOrgID
		,Org_Name = @OrgName
		,Name_Prefix = @NamePrefix
		,Name_Suffix = @NameSuffix
		,Alternate_Names = @AlternateNames
		,Country_Of_Origin = @CountryOfOrigin
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Organization_ID = @OrganizationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Upd_1')
	DROP PROCEDURE CRM.Organizations_Upd_1
go
CREATE PROCEDURE CRM.Organizations_Upd_1
	(
	@OrganizationID			Int
	,@SiteID				Int = NULL
	,@UpdatedOn				SmallDateTime = NULL
	,@UpdatedBy				NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Organizations
	SET Site_ID_Headquarter = @SiteID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Organization_ID = @OrganizationID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Upd_2')
	DROP PROCEDURE CRM.Organizations_Upd_2
go
CREATE PROCEDURE CRM.Organizations_Upd_2
	(
	@OrganizationID			Int
	,@SiteID				Int = NULL
	,@UpdatedOn				SmallDateTime = NULL
	,@UpdatedBy				NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Organizations
	SET	Site_ID_Billing = @SiteID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Organization_ID = @OrganizationID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Upd_3')
	DROP PROCEDURE CRM.Organizations_Upd_3
go
CREATE PROCEDURE CRM.Organizations_Upd_3
	(
	@OrganizationID			Int
	,@SiteID				Int = NULL
	,@UpdatedOn				SmallDateTime = NULL
	,@UpdatedBy				NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Organizations
	SET Site_ID_Shipping = @SiteID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Organization_ID = @OrganizationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Sel')
	DROP PROCEDURE CRM.Organizations_Sel
go
CREATE PROCEDURE CRM.Organizations_Sel
	(
	@OrganizationID			Int
	)
AS
	SELECT TOP 1
		 O.Organization_ID				-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,O.Segment_ID					-- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
        ,N.Segment_Name                 -- NVarChar(64) NOT NULL
		,O.Parent_Org_ID				-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,O.Org_Name						-- NVarChar(256) NOT NULL
		,O.Name_Prefix					-- NVarChar(16) NULL
		,O.Name_Suffix					-- NVarChar(16) NULL
		,O.Alternate_Names				-- NVarChar(1024) NULL
		,O.Country_Of_Origin			-- Char(3) NOT NULL DEFAULT 'ROM'
		,O.Site_ID_Headquarter			-- Int NULL
		,O.Site_ID_Billing				-- Int NULL
		,O.Site_ID_Shipping				-- Int NULL
		,O.Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,O.Updated_By					-- NVarChar(128) NULL
		,S.Country                      -- Char(3) NOT NULL DEFAULT 'ROM'
		,S.Region                       -- NVarChar(128) NULL
		,S.State_Province               -- NVarChar(128) NULL
		,S.City                         -- NVarChar(64) NOT NULL
		,S.Street_Address_1             -- NVarChar(128) NULL
	FROM CRM.Organizations O
        INNER JOIN CRM.Orgs_Segment_Names N ON O.Segment_ID = N.Segment_ID
        LEFT JOIN CRM.Orgs_Sites S ON O.Site_ID_Headquarter = S.Site_ID
	WHERE O.Organization_ID = @OrganizationID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Organizations_Merge')
	DROP PROCEDURE CRM.Organizations_Merge
go
CREATE PROCEDURE CRM.Organizations_Merge
	(
	@FromOrganizationID			Int
	,@ToOrganizationID			Int
	)
AS
	UPDATE Archiving.Documents_To_Organizations SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE Sales.Orders SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE Sales.Projects SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE Delivery.Documents SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE Billing.Invoices SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE Billing.Customer_Accounts SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Relation_Orgs_Pers SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_TimeInterval_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_TableOrXml_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_String_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_Sites SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_Integer_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_HugeText_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_Flag_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_Decimal_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_DateTime_Fields SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_Contacts SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Orgs_Activities SET Organization_ID = @ToOrganizationID WHERE Organization_ID = @FromOrganizationID

	DECLARE @SegmentID TinyInt = (SELECT TOP 1 Segment_ID FROM CRM.Organizations WHERE Organization_ID = @ToOrganizationID)
	IF (@SegmentID = 0) SET @SegmentID = (SELECT TOP 1 Segment_ID FROM CRM.Organizations WHERE Organization_ID = @FromOrganizationID)
	DECLARE @ParentOrgID Int
	DECLARE @NamePrefix NVarChar(16)
	DECLARE @NameSuffix NVarChar(16)
	DECLARE @AlternateNames NVarChar(1024)
	DECLARE @SiteIdHome Int
	DECLARE @SiteIdBilling Int
	DECLARE @SiteIdShipping Int
	SELECT TOP 1
		@ParentOrgID = Parent_Org_ID
		,@NamePrefix = Name_Prefix
		,@NameSuffix = Name_Suffix
		,@AlternateNames = Alternate_Names
		,@SiteIdHome = Site_ID_Headquarter
		,@SiteIdBilling = Site_ID_Billing
		,@SiteIdShipping = Site_ID_Shipping
	FROM CRM.Organizations
	WHERE Organization_ID = @FromOrganizationID
	UPDATE CRM.Organizations SET
		Segment_ID = @SegmentID
		,Parent_Org_ID = ISNULL(Parent_Org_ID, @ParentOrgID)
		,Name_Prefix = ISNULL(Name_Prefix, @NamePrefix)
		,Name_Suffix = ISNULL(Name_Suffix, @NameSuffix)
		,Alternate_Names = ISNULL(Alternate_Names, @AlternateNames)
		,Site_ID_Headquarter = ISNULL(Site_ID_Headquarter, @SiteIdHome)
		,Site_ID_Billing = ISNULL(Site_ID_Billing, @SiteIdBilling)
		,Site_ID_Shipping = ISNULL(Site_ID_Shipping, @SiteIdShipping)
	WHERE Organization_ID = @ToOrganizationID

	DELETE FROM CRM.Organizations WHERE Organization_ID = @FromOrganizationID
go