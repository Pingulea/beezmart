﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Contacts_Ins')
	DROP PROCEDURE CRM.Orgs_Contacts_Ins
go
CREATE PROCEDURE CRM.Orgs_Contacts_Ins
	(
	@ContactID BigInt OUTPUT
	,@OrganizationID Int
	,@SiteID Int = NULL
	,@Contact NVarChar(256)
	,@Comments NVarChar(256) = NULL
	,@ContactTypeID TinyInt = 1
	,@ContactStatusID TinyInt = 1
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Orgs_Contacts
		(
		Organization_ID
		,Site_ID
		,Contact
		,Comments
		,Contact_Type_ID
		,Contact_Status_ID
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@OrganizationID
		,@SiteID
		,@Contact
		,@Comments
		,@ContactTypeID
		,@ContactStatusID
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @ContactID = SCOPE_IDENTITY()
	SELECT CAST(@ContactID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Contacts_Upd')
	DROP PROCEDURE CRM.Orgs_Contacts_Upd
go
CREATE PROCEDURE CRM.Orgs_Contacts_Upd
	(
	@ContactID BigInt
	,@OrganizationID Int
	,@SiteID Int = NULL
	,@Contact NVarChar(256)
	,@Comments NVarChar(256) = NULL
	,@ContactTypeID TinyInt = 1
	,@ContactStatusID TinyInt = 1
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Orgs_Contacts
	SET
		Organization_ID = @OrganizationID
		,Site_ID = @SiteID
		,Contact = @Contact
		,Comments = @Comments
		,Contact_Type_ID = @ContactTypeID
		,Contact_Status_ID = @ContactStatusID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Contact_ID = @ContactID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Contacts_Del')
	DROP PROCEDURE CRM.Orgs_Contacts_Del
go
CREATE PROCEDURE CRM.Orgs_Contacts_Del
	(
	@ContactID BigInt
	)
AS
	DELETE FROM CRM.Orgs_Contacts
	WHERE Contact_ID = @ContactID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Contacts_Sel')
	DROP PROCEDURE CRM.Orgs_Contacts_Sel
go
CREATE PROCEDURE CRM.Orgs_Contacts_Sel
	(
	@ContactID BigInt
	)
AS
	SELECT TOP 1
		Contact_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Organization_ID			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Site_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
		,Contact					-- NVarChar(256) NOT NULL
		,Comments					-- NVarChar(256) NULL
		,Contact_Type_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Orgs_Contacts C
	WHERE Contact_ID = @ContactID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Contacts_Grid_1')
	DROP PROCEDURE CRM.Orgs_Contacts_Grid_1
go
CREATE PROCEDURE CRM.Orgs_Contacts_Grid_1
	(
	@OrganizationID Int
	)
AS
	SELECT
		Contact_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Organization_ID			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Site_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
		,Contact					-- NVarChar(256) NOT NULL
		,Comments					-- NVarChar(256) NULL
		,Contact_Type_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Orgs_Contacts C
	WHERE Organization_ID = @OrganizationID
	ORDER BY Contact_Type_ID, Updated_On DESC
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type='P' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Orgs_Contacts_Grid_2')
	DROP PROCEDURE CRM.Orgs_Contacts_Grid_2
go
CREATE PROCEDURE CRM.Orgs_Contacts_Grid_2
	(
	@OrganizationID Int
	)
AS
	DECLARE @SiteIdHeadquarter Int, @SiteIdBilling Int, @SiteIdShipping Int
	SELECT @SiteIdHeadquarter = Site_ID_Headquarter, @SiteIdBilling = Site_ID_Billing, @SiteIdShipping = Site_ID_Shipping
	FROM CRM.Organizations WHERE Organization_ID = @OrganizationID

	SELECT TOP 1
		Contact_ID					-- BigInt NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,Organization_ID			-- Int NOT NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Site_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Orgs_Sites(Site_ID)
		,Contact					-- NVarChar(256) NOT NULL
		,Comments					-- NVarChar(256) NULL
		,Contact_Type_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Types(Contact_Type_ID)
		,Contact_Status_ID			-- TinyInt NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES CRM.App_Contact_Status(Contact_Status_ID)
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM CRM.Orgs_Contacts C
	WHERE Organization_ID = @OrganizationID
		AND Site_ID NOT IN (@SiteIdHeadquarter, @SiteIdBilling, @SiteIdShipping)
	ORDER BY Contact_Type_ID, Updated_On DESC
go