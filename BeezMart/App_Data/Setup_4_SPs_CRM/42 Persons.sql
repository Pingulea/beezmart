﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Ins')
	DROP PROCEDURE CRM.Persons_Ins
go
CREATE PROCEDURE CRM.Persons_Ins
	(
	@PersonID					Int OUTPUT
	,@SegmentID					TinyInt = 0
	,@FirstName					NVarChar(64)
	,@LastName					NVarChar(64)
	,@NamePrefix				NVarChar(16) = NULL
	,@NameSuffix				NVarChar(16) = NULL
	,@Nicknames					NVarChar(256) = NULL
	,@CountryOfOrigin			Char(3) = 'ROM'
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO CRM.Persons
		(
		Segment_ID
		,First_Name
		,Last_Name
		,Name_Prefix
		,Name_Suffix
		,Nicknames
		,Country_Of_Origin
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		@SegmentID
		,@FirstName
		,@LastName
		,@NamePrefix
		,@NameSuffix
		,@Nicknames
		,@CountryOfOrigin
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @PersonID = SCOPE_IDENTITY()
	SELECT CAST(@PersonID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Upd')
	DROP PROCEDURE CRM.Persons_Upd
go
CREATE PROCEDURE CRM.Persons_Upd
	(
	@PersonID					Int
	,@SegmentID					TinyInt = 0
	,@FirstName					NVarChar(64)
	,@LastName					NVarChar(64)
	,@NamePrefix				NVarChar(16) = NULL
	,@NameSuffix				NVarChar(16) = NULL
	,@Nicknames					NVarChar(256) = NULL
	,@CountryOfOrigin			Char(3) = 'ROM'
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Persons
	SET
		Segment_ID = @SegmentID
		,First_Name = @FirstName
		,Last_Name = @LastName
		,Name_Prefix = @NamePrefix
		,Name_Suffix = @NameSuffix
		,Nicknames = @Nicknames
		,Country_Of_Origin = @CountryOfOrigin
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Person_ID = @PersonID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Upd_1')
	DROP PROCEDURE CRM.Persons_Upd_1
go
CREATE PROCEDURE CRM.Persons_Upd_1
	(
	@PersonID					Int
	,@SiteID					Int = NULL
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Persons
	SET Site_ID_Home = @SiteID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Person_ID = @PersonID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Upd_2')
	DROP PROCEDURE CRM.Persons_Upd_2
go
CREATE PROCEDURE CRM.Persons_Upd_2
	(
	@PersonID					Int
	,@SiteID					Int = NULL
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Persons
	SET Site_ID_Billing = @SiteID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Person_ID = @PersonID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Upd_3')
	DROP PROCEDURE CRM.Persons_Upd_3
go
CREATE PROCEDURE CRM.Persons_Upd_3
	(
	@PersonID					Int
	,@SiteID					Int = NULL
	,@UpdatedOn					SmallDateTime = NULL
	,@UpdatedBy					NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE CRM.Persons
	SET Site_ID_Shipping = @SiteID
		,Updated_On = @UpdatedOn
		,Updated_By = @UpdatedBy
	WHERE Person_ID = @PersonID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Sel')
	DROP PROCEDURE CRM.Persons_Sel
go
CREATE PROCEDURE CRM.Persons_Sel
	(
	@PersonID			Int
	)
AS
	SELECT TOP 1
		 P.Person_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		,P.Segment_ID				    -- TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
		,N.Segment_Name				    -- NVarChar(256) NOT NULL
		,P.First_Name					-- NVarChar(64) NOT NULL
		,P.Last_Name					-- NVarChar(64) NOT NULL
		,P.Name_Prefix				    -- NVarChar(16) NULL
		,P.Name_Suffix				    -- NVarChar(16) NULL
		,P.Nicknames					-- NVarChar(256) NULL
		,P.Country_Of_Origin			-- Char(3) NOT NULL DEFAULT 'ROM'
		,P.Site_ID_Home				    -- Int NULL
		,P.Site_ID_Billing			    -- Int NULL
		,P.Site_ID_Shipping			    -- Int NULL
		,P.Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,P.Updated_By					-- NVarChar(128) NULL
		,S.Country                      -- Char(3) NOT NULL DEFAULT 'ROM'
		,S.Region                       -- NVarChar(128) NULL
		,S.State_Province               -- NVarChar(128) NULL
		,S.City                         -- NVarChar(64) NOT NULL
		,S.Street_Address_1             -- NVarChar(128) NULL
	FROM CRM.Persons P
		INNER JOIN CRM.Pers_Segment_Names N ON P.Segment_ID = N.Segment_ID
        LEFT JOIN CRM.Pers_Sites S ON P.Site_ID_Home = S.Site_ID
	WHERE P.Person_ID = @PersonID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type_Desc = 'SQL_Stored_Procedure' AND SCHEMA_NAME(SCHEMA_ID) = 'CRM' AND Name = N'Persons_Merge')
	DROP PROCEDURE CRM.Persons_Merge
go
CREATE PROCEDURE CRM.Persons_Merge
	(
	@FromPersonID			Int
	,@ToPersonID			Int
	)
AS
	UPDATE Archiving.Documents_To_Persons SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE Sales.Orders SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE Sales.Projects SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE Delivery.Documents SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE Billing.Invoices SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE Billing.Customer_Accounts SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Relation_Orgs_Pers SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_TimeInterval_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_TableOrXml_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_String_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_Sites SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_Integer_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_HugeText_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_Flag_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_Decimal_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_DateTime_Fields SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_Contacts SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID
	UPDATE CRM.Pers_Activities SET Person_ID = @ToPersonID WHERE Person_ID = @FromPersonID

	DECLARE @SegmentID TinyInt = (SELECT TOP 1 Segment_ID FROM CRM.Persons WHERE Person_ID = @ToPersonID)
	IF (@SegmentID = 0) SET @SegmentID = (SELECT TOP 1 Segment_ID FROM CRM.Persons WHERE Person_ID = @FromPersonID)
	DECLARE @NamePrefix NVarChar(16)
	DECLARE @NameSuffix NVarChar(16)
	DECLARE @Nicknames NVarChar(256)
	DECLARE @SiteIDHome Int
	DECLARE @SiteIDBilling Int
	DECLARE @SiteIDShipping Int
	SELECT TOP 1
		@NamePrefix = Name_Prefix
		,@NameSuffix = Name_Suffix
		,@Nicknames = Nicknames
		,@SiteIDHome = Site_ID_Home
		,@SiteIDBilling = Site_ID_Billing
		,@SiteIDShipping = Site_ID_Shipping
	FROM CRM.Persons
	WHERE Person_ID = @FromPersonID
	UPDATE CRM.Persons SET
		Segment_ID = @SegmentID
		,Name_Prefix = ISNULL(Name_Prefix, @NamePrefix)
		,Name_Suffix = ISNULL(Name_Suffix, @NameSuffix)
		,Nicknames = ISNULL(Nicknames, @Nicknames)
		,Site_ID_Home = ISNULL(Site_ID_Home, @SiteIDHome)
		,Site_ID_Billing = ISNULL(Site_ID_Billing, @SiteIDBilling)
		,Site_ID_Shipping = ISNULL(Site_ID_Shipping, @SiteIDShipping)
	WHERE Person_ID = @ToPersonID

	DELETE FROM CRM.Persons WHERE Person_ID = @FromPersonID
go
