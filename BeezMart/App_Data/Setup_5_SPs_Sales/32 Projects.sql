






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Ins')
	DROP PROCEDURE Sales.Projects_Ins
go
CREATE PROCEDURE Sales.Projects_Ins
	(
	 @ProjectID Int OUTPUT						-- NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,@Title NVarChar(256)						-- NOT NULL
	,@ProjectManager NVarChar(256)	= NULL
	,@ContactDetails NVarChar(256)	= NULL
	,@Details NVarChar(MAX) = NULL
	
	,@OrganizationID Int = NULL					-- FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,@PersonID Int = NULL						-- FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,@CustomerName NVarChar(256)				-- NOT NULL
	,@EndCustomer NVarChar(256) = NULL

	,@CategoryID SmallInt = NULL				-- FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
	,@ProjectStatusID TinyInt = NULL			-- FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

	,@ScheduledStart SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@ScheduledEnd SmallDateTime = NULL			-- NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
	,@StartedOn SmallDateTime = NULL
	,@EndedOn SmallDateTime = NULL
	
	,@UpdatedOn SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @ScheduledStart = ISNULL(@ScheduledStart, GETDATE())
	SET @ScheduledEnd = ISNULL(@ScheduledEnd, DATEADD(MONTH, 3, GETDATE()))
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Sales.Projects
		(
			 Title
			,Project_Manager
			,Contact_Details
			,Details
			
			,Organization_ID
			,Person_ID
			,Customer_Name
			,End_Customer

			,Category_ID
			,Project_Status_ID

			,Scheduled_Start
			,Scheduled_End
			,Started_On
			,Ended_On
			
			,Updated_On
			,Updated_By
		)
	VALUES
		(
			 @Title
			,@ProjectManager
			,@ContactDetails
			,@Details
			
			,@OrganizationID
			,@PersonID
			,@CustomerName
			,@EndCustomer

			,@CategoryID
			,@ProjectStatusID

			,@ScheduledStart
			,@ScheduledEnd
			,@StartedOn
			,@EndedOn
			
			,@UpdatedOn
			,@UpdatedBy
		)
	SET @ProjectID = SCOPE_IDENTITY()	
	SELECT CAST(@ProjectID AS Int) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Upd')
	DROP PROCEDURE Sales.Projects_Upd
go
CREATE PROCEDURE Sales.Projects_Upd
	(
	 @ProjectID Int								-- NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
	,@Title NVarChar(256)						-- NOT NULL
	,@ProjectManager NVarChar(256)	= NULL
	,@ContactDetails NVarChar(256)	= NULL
	,@Details NVarChar(MAX) = NULL
	
	,@OrganizationID Int = NULL					-- FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,@PersonID Int = NULL						-- FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,@CustomerName NVarChar(256)				-- NOT NULL
	,@EndCustomer NVarChar(256) = NULL

	,@CategoryID SmallInt = NULL				-- FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
	,@ProjectStatusID TinyInt = NULL			-- FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

	,@ScheduledStart SmallDateTime = NULL		-- NOT NULL DEFAULT GETDATE()
	,@ScheduledEnd SmallDateTime = NULL			-- NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
	,@StartedOn SmallDateTime = NULL
	,@EndedOn SmallDateTime = NULL
	
	,@UpdatedOn SmallDateTime = NULL			-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @ScheduledStart = ISNULL(@ScheduledStart, GETDATE())
	SET @ScheduledEnd = ISNULL(@ScheduledEnd, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Sales.Projects
	SET
		 Title				=	@Title
		,Project_Manager	=	@ProjectManager
		,Contact_Details	=	@ContactDetails
		,Details			=	@Details
		
		,Organization_ID	=	@OrganizationID
		,Person_ID			=	@PersonID
		,Customer_Name		=	@CustomerName
		,End_Customer		=	@EndCustomer

		,Category_ID		=	@CategoryID
		,Project_Status_ID	=	@ProjectStatusID

		,Scheduled_Start	=	@ScheduledStart
		,Scheduled_End		=	@ScheduledEnd
		,Started_On			=	@StartedOn
		,Ended_On			=	@EndedOn
		
		,Updated_On			=	@UpdatedOn
		,Updated_By			=	@UpdatedBy
	WHERE Project_ID = @ProjectID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Del')
	DROP PROCEDURE Sales.Projects_Del
go
CREATE PROCEDURE Sales.Projects_Del
	(
	@ProjectID Int
	)
AS
	DELETE FROM Sales.Project_Activities WHERE Project_ID = @ProjectID
	DELETE FROM Sales.Projects WHERE Project_ID = @ProjectID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Sel')
	DROP PROCEDURE Sales.Projects_Sel
go
CREATE PROCEDURE Sales.Projects_Sel
	(
	@ProjectID Int
	)
AS
	SELECT TOP 1
		 Project_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Title						-- NVarChar(256) NOT NULL
		,Project_Manager			-- NVarChar(256) NULL
		,Contact_Details			-- NVarChar(256) NULL
		,Details					-- NVarChar(MAX) NULL
		
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name				-- NVarChar(256) NOT NULL
		,End_Customer				-- NVarChar(256) NULL

		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
		,Project_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

		,Scheduled_Start			-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Scheduled_End				-- SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
		,Started_On					-- SmallDateTime NULL
		,Ended_On					-- SmallDateTime NULL
		
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Sales.Projects
	WHERE Project_ID = @ProjectID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Grid_1')
	DROP PROCEDURE Sales.Projects_Grid_1
go
CREATE PROCEDURE Sales.Projects_Grid_1
	(
	 @TimeframeStart SmallDateTime = NULL
	,@TimeframeEnd SmallDateTime = NULL
	)
AS
	SET @TimeframeStart = ISNULL(@TimeframeStart, DATEADD(MONTH, -3, GETDATE()))
	SET @TimeframeEnd = ISNULL(@TimeframeEnd, DATEADD(MONTH, 3, GETDATE()))
	SELECT
		 Project_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Title						-- NVarChar(256) NOT NULL
		,Project_Manager			-- NVarChar(256) NULL
		,Contact_Details			-- NVarChar(256) NULL
		--,Details					-- NVarChar(MAX) NULL
		
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name				-- NVarChar(256) NOT NULL
		,End_Customer				-- NVarChar(256) NULL

		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
		,Project_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

		,Scheduled_Start			-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Scheduled_End				-- SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
		,Started_On					-- SmallDateTime NULL
		,Ended_On					-- SmallDateTime NULL
		
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Sales.Projects
	WHERE Scheduled_Start BETWEEN @TimeframeStart AND @TimeframeEnd
		OR Scheduled_End BETWEEN @TimeframeStart AND @TimeframeEnd
		OR @TimeframeStart BETWEEN Scheduled_Start AND Scheduled_End
		OR @TimeframeEnd BETWEEN Scheduled_Start AND Scheduled_End
		OR Started_On BETWEEN @TimeframeStart AND @TimeframeEnd
		OR Ended_On BETWEEN @TimeframeStart AND @TimeframeEnd
		OR @TimeframeStart BETWEEN Started_On AND Ended_On
		OR @TimeframeEnd BETWEEN Started_On AND Ended_On
	ORDER BY Project_ID DESC
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Grid_2')
	DROP PROCEDURE Sales.Projects_Grid_2
go
CREATE PROCEDURE Sales.Projects_Grid_2
	(
	@NamePattern NVarChar(256)
	)
AS
	SELECT
		 Project_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Title						-- NVarChar(256) NOT NULL
		,Project_Manager			-- NVarChar(256) NULL
		,Contact_Details			-- NVarChar(256) NULL
		--,Details					-- NVarChar(MAX) NULL
		
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name				-- NVarChar(256) NOT NULL
		,End_Customer				-- NVarChar(256) NULL

		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
		,Project_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

		,Scheduled_Start			-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Scheduled_End				-- SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
		,Started_On					-- SmallDateTime NULL
		,Ended_On					-- SmallDateTime NULL
		
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Sales.Projects
	WHERE Project_Status_ID NOT IN
        (
             0        -- Undefined
	        --,10       -- Dreaming
	        --,20       -- Concept
	        --,30       -- Planned
	        --,40       -- In execution
	        --,50       -- Finalized
	        --,60       -- Customer-approved
	        --,70       -- Billed
	        --,80       -- Paid
	        ,90       -- Closed and archived
	        ,100      -- Cancelled
        )
        AND
        (
            Title LIKE @NamePattern
		    OR Customer_Name LIKE @NamePattern
		    OR End_Customer LIKE @NamePattern
		    OR Project_Manager LIKE @NamePattern
        )
	ORDER BY Project_ID DESC
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Projects_Grid_3')
	DROP PROCEDURE Sales.Projects_Grid_3
go
CREATE PROCEDURE Sales.Projects_Grid_3
	(
	@MaximumRowCount Int = 1000
	)
AS
	SELECT TOP (@MaximumRowCount)
		 Project_ID					-- Int NOT NULL PRIMARY KEY IDENTITY(-2147483648, 1)
		,Title						-- NVarChar(256) NOT NULL
		,Project_Manager			-- NVarChar(256) NULL
		,Contact_Details			-- NVarChar(256) NULL
		--,Details					-- NVarChar(MAX) NULL
		
		,Organization_ID			-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,Person_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,Customer_Name				-- NVarChar(256) NOT NULL
		,End_Customer				-- NVarChar(256) NULL

		,Category_ID				-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Project_Categories(Category_ID)
		,Project_Status_ID			-- TinyInt NULL FOREIGN KEY REFERENCES Sales.App_Project_Status(Project_Status_ID)

		,Scheduled_Start			-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Scheduled_End				-- SmallDateTime NOT NULL DEFAULT DATEADD(MONTH, 3, GETDATE())
		,Started_On					-- SmallDateTime NULL
		,Ended_On					-- SmallDateTime NULL
		
		,Updated_On					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By					-- NVarChar(128) NULL
	FROM Sales.Projects
	WHERE Project_Status_ID NOT IN
        (
             0        -- Undefined
	        --,10       -- Dreaming
	        --,20       -- Concept
	        --,30       -- Planned
	        --,40       -- In execution
	        --,50       -- Finalized
	        --,60       -- Customer-approved
	        --,70       -- Billed
	        --,80       -- Paid
	        ,90       -- Closed and archived
	        ,100      -- Cancelled
        )
	ORDER BY Project_ID DESC
go
