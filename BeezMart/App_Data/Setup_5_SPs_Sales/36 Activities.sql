






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Ins')
	DROP PROCEDURE Sales.Activities_Ins
go
CREATE PROCEDURE Sales.Activities_Ins
	(
	 @ActivityID BigInt OUTPUT								-- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@ProjectID Int = NULL									-- FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
	,@OrganizationID Int = NULL								-- FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,@PersonID Int = NULL									-- FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,@CustomerName NVarChar(256)							-- NOT NULL
	,@EndCustomer NVarChar(256) = NULL

	,@ActivityStatusID TinyInt								-- NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
	,@ActivityStatusComment NVarChar(256) = NULL
	,@ActivityTypeID SmallInt = NULL						-- NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
	,@Title NVarChar(1024)									-- NOT NULL
	,@Details NVarChar(MAX) = NULL

	,@ScheduledStart SmallDateTime = NULL					-- NOT NULL DEFAULT GETDATE()
	,@ScheduledEnd SmallDateTime = NULL						-- NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
	,@AssignedOn SmallDateTime = NULL
	,@AssignedTo NVarChar(128) = NULL
	,@DeadlineOn SmallDateTime = NULL
	,@StartedOn SmallDateTime = NULL
	,@EndedOn SmallDateTime = NULL
	,@ReminderOn SmallDateTime = NULL

	,@RequestedBy NVarChar(128) = NULL
	,@RequestingDepartment NVarChar(128) = NULL
	,@CompletedBy NVarChar(128) = NULL
	,@LoggedBy NVarChar(128) = NULL
	,@Billable Bit											-- NOT NULL DEFAULT 0
	,@InvoiceIDs VarChar(256) = NULL
	,@DurationInMinutes Int = NULL

	,@UpdatedOn SmallDateTime								-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @ScheduledStart = ISNULL(@ScheduledStart, GETDATE())
	SET @ScheduledEnd = ISNULL(@ScheduledEnd, DATEADD(HOUR, 3, GETDATE()))
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Sales.Activities
		(
			 Project_ID
			,Organization_ID
			,Person_ID
			,Customer_Name
			,End_Customer

			,Activity_Status_ID
			,Activity_Status_Comment
			,Activity_Type_ID
			,Title
			,Details

			,Scheduled_Start
			,Scheduled_End
			,Assigned_On
			,Assigned_To
			,Deadline_On
			,Started_On
			,Ended_On
			,Reminder_On

			,Requested_By
			,Requesting_Department
			,Completed_By
			,Logged_By
			,Billable
			,Invoice_IDs
			,Duration_In_Minutes

			,Updated_On
			,Updated_By
		)
	VALUES
		(
			 @ProjectID
			,@OrganizationID
			,@PersonID
			,@CustomerName
			,@EndCustomer

			,@ActivityStatusID
			,@ActivityStatusComment
			,@ActivityTypeID
			,@Title
			,@Details

			,@ScheduledStart
			,@ScheduledEnd
			,@AssignedOn
			,@AssignedTo
			,@DeadlineOn
			,@StartedOn
			,@EndedOn
			,@ReminderOn

			,@RequestedBy
			,@RequestingDepartment
			,@CompletedBy
			,@LoggedBy
			,@Billable
			,@InvoiceIDs
			,@DurationInMinutes

			,@UpdatedOn
			,@UpdatedBy
		)
	SET @ActivityID = SCOPE_IDENTITY()	
	SELECT CAST(@ActivityID AS BigInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Upd')
	DROP PROCEDURE Sales.Activities_Upd
go
CREATE PROCEDURE Sales.Activities_Upd
	(
	 @ActivityID BigInt										-- NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
	,@ProjectID Int = NULL									-- FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
	,@OrganizationID Int = NULL								-- FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
	,@PersonID Int = NULL									-- FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
	,@CustomerName NVarChar(256)							-- NOT NULL
	,@EndCustomer NVarChar(256) = NULL

	,@ActivityStatusID TinyInt								-- NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
	,@ActivityStatusComment NVarChar(256) = NULL
	,@ActivityTypeID SmallInt = NULL						-- NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
	,@Title NVarChar(1024)									-- NOT NULL
	,@Details NVarChar(MAX) = NULL

	,@ScheduledStart SmallDateTime = NULL					-- NOT NULL DEFAULT GETDATE()
	,@ScheduledEnd SmallDateTime = NULL						-- NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
	,@AssignedOn SmallDateTime = NULL
	,@AssignedTo NVarChar(128) = NULL
	,@DeadlineOn SmallDateTime = NULL
	,@StartedOn SmallDateTime = NULL
	,@EndedOn SmallDateTime = NULL
	,@ReminderOn SmallDateTime = NULL

	,@RequestedBy NVarChar(128) = NULL
	,@RequestingDepartment NVarChar(128) = NULL
	,@CompletedBy NVarChar(128) = NULL
	,@LoggedBy NVarChar(128) = NULL
	,@Billable Bit											-- NOT NULL DEFAULT 0
	,@InvoiceIDs VarChar(256) = NULL
	,@DurationInMinutes Int = NULL

	,@UpdatedOn SmallDateTime								-- NOT NULL DEFAULT GETDATE()
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @ScheduledStart = ISNULL(@ScheduledStart, GETDATE())
	SET @ScheduledEnd = ISNULL(@ScheduledEnd, GETDATE())
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Sales.Activities
	SET
		 Project_ID						  = @ProjectID
		,Organization_ID				  = @OrganizationID
		,Person_ID						  = @PersonID
		,Customer_Name					  = @CustomerName
		,End_Customer					  = @EndCustomer

		,Activity_Status_ID				  = @ActivityStatusID
		,Activity_Status_Comment		  = @ActivityStatusComment
		,Activity_Type_ID				  = @ActivityTypeID
		,Title							  = @Title
		,Details						  = @Details

		,Scheduled_Start				  = @ScheduledStart
		,Scheduled_End					  = @ScheduledEnd
		,Assigned_On					  = @AssignedOn
		,Assigned_To					  = @AssignedTo
		,Deadline_On					  = @DeadlineOn
		,Started_On						  = @StartedOn
		,Ended_On						  = @EndedOn
		,Reminder_On					  = @ReminderOn

		,Requested_By					  = @RequestedBy
		,Requesting_Department			  = @RequestingDepartment
		,Completed_By					  = @CompletedBy
		,Logged_By						  = @LoggedBy
		,Billable						  = @Billable
		,Invoice_IDs					  = @InvoiceIDs
		,Duration_In_Minutes			  = @DurationInMinutes

		,Updated_On						  = @UpdatedOn
		,Updated_By						  = @UpdatedBy
	WHERE Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Del')
	DROP PROCEDURE Sales.Activities_Del
go
CREATE PROCEDURE Sales.Activities_Del
	(
	@ActivityID BigInt
	)
AS
	DELETE FROM Sales.Activities WHERE Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Sel')
	DROP PROCEDURE Sales.Activities_Sel
go
CREATE PROCEDURE Sales.Activities_Sel
	(
	@ActivityID BigInt
	)
AS
	SELECT TOP 1
		 A.Activity_ID						-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,A.Project_ID						-- Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
		,A.Organization_ID					-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,A.Person_ID						-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,A.Customer_Name					-- NVarChar(256) NOT NULL
		,A.End_Customer						-- NVarChar(256) NULL

		,A.Activity_Status_ID				-- TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
		,A.Activity_Status_Comment			-- NVarChar(256) NULL
		,A.Activity_Type_ID					-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
		,A.Title							-- NVarChar(1024) NOT NULL
		,A.Details							-- NVarChar(MAX) NULL

		,A.Scheduled_Start					-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Scheduled_End					-- SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
		,A.Assigned_On						-- SmallDateTime NULL
		,A.Assigned_To						-- NVarChar(128) NULL
		,A.Deadline_On						-- SmallDateTime NULL
		,A.Started_On						-- SmallDateTime NULL
		,A.Ended_On							-- SmallDateTime NULL
		,A.Reminder_On						-- SmallDateTime NULL

		,A.Requested_By						-- NVarChar(128) NULL
		,A.Requesting_Department			-- NVarChar(128) NULL
		,A.Completed_By						-- NVarChar(128) NULL
		,A.Logged_By						-- NVarChar(128) NULL
		,A.Billable							-- Bit NOT NULL DEFAULT 0
		,A.Invoice_IDs						-- VarChar(256) NULL
		,A.Duration_In_Minutes				-- Int NULL

		,A.Updated_On						-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Updated_By						-- NVarChar(128) NULL
	FROM Sales.Activities A
	WHERE Activity_ID = @ActivityID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Grid_1')
	DROP PROCEDURE Sales.Activities_Grid_1
go
CREATE PROCEDURE Sales.Activities_Grid_1
	(
	@ProjectID Int
	)
AS
	SELECT
		 A.Activity_ID										-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,A.Project_ID										-- Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
		,A.Organization_ID									-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,A.Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,A.Customer_Name									-- NVarChar(256) NOT NULL
		,A.End_Customer										-- NVarChar(256) NULL

		,P.Title AS Project_Title							-- NVarChar(256) NULL

		,A.Activity_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
		--,A.Activity_Status_Comment						-- NVarChar(256) NULL
		,A.Activity_Type_ID									-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
		,A.Title											-- NVarChar(1024) NOT NULL
		--,A.Details										-- NVarChar(MAX) NULL

		,C.Activity_Type_Name AS Activity_Type				-- NVarChar(64) NULL
		,C.Activity_Type_Category AS Activity_Category		-- NVarChar(64) NULL

		,A.Scheduled_Start									-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Scheduled_End									-- SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
		,A.Assigned_On										-- SmallDateTime NULL
		,A.Assigned_To										-- NVarChar(128) NULL
		,A.Deadline_On										-- SmallDateTime NULL
		,A.Started_On										-- SmallDateTime NULL
		,A.Ended_On											-- SmallDateTime NULL
		,A.Reminder_On										-- SmallDateTime NULL

		,A.Requested_By										-- NVarChar(128) NULL
		,A.Requesting_Department							-- NVarChar(128) NULL
		,A.Completed_By										-- NVarChar(128) NULL
		,A.Logged_By										-- NVarChar(128) NULL
		,A.Billable											-- Bit NOT NULL DEFAULT 0
		,A.Invoice_IDs										-- VarChar(256) NULL
		,A.Duration_In_Minutes								-- Int NULL

		,A.Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Updated_By										-- NVarChar(128) NULL
	FROM Sales.Activities A
		LEFT JOIN Sales.Projects P ON A.Project_ID = P.Project_ID
		LEFT JOIN Sales.Activity_Types C ON A.Activity_Type_ID = C.Activity_Type_ID
	WHERE A.Project_ID = @ProjectID
	ORDER BY Scheduled_Start DESC
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Grid_2')
	DROP PROCEDURE Sales.Activities_Grid_2
go
CREATE PROCEDURE Sales.Activities_Grid_2
	(
	 @TimeframeStart SmallDateTime = NULL
	,@TimeframeEnd SmallDateTime = NULL
	)
AS
	SET @TimeframeStart = ISNULL(@TimeframeStart, DATEADD(MONTH, -3, GETDATE()));
	SET @TimeframeEnd = ISNULL(@TimeframeEnd, DATEADD(MONTH, 3, GETDATE()));
	SELECT
		 A.Activity_ID										-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,A.Project_ID										-- Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
		,A.Organization_ID									-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,A.Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,A.Customer_Name									-- NVarChar(256) NOT NULL
		,A.End_Customer										-- NVarChar(256) NULL

		,P.Title AS Project_Title							-- NVarChar(256) NULL

		,A.Activity_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
		--,A.Activity_Status_Comment						-- NVarChar(256) NULL
		,A.Activity_Type_ID									-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
		,A.Title											-- NVarChar(1024) NOT NULL
		--,A.Details										-- NVarChar(MAX) NULL

		,C.Activity_Type_Name AS Activity_Type				-- NVarChar(64) NULL
		,C.Activity_Type_Category AS Activity_Category		-- NVarChar(64) NULL

		,A.Scheduled_Start									-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Scheduled_End									-- SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
		,A.Assigned_On										-- SmallDateTime NULL
		,A.Assigned_To										-- NVarChar(128) NULL
		,A.Deadline_On										-- SmallDateTime NULL
		,A.Started_On										-- SmallDateTime NULL
		,A.Ended_On											-- SmallDateTime NULL
		,A.Reminder_On										-- SmallDateTime NULL

		,A.Requested_By										-- NVarChar(128) NULL
		,A.Requesting_Department							-- NVarChar(128) NULL
		,A.Completed_By										-- NVarChar(128) NULL
		,A.Logged_By										-- NVarChar(128) NULL
		,A.Billable											-- Bit NOT NULL DEFAULT 0
		,A.Invoice_IDs										-- VarChar(256) NULL
		,A.Duration_In_Minutes								-- Int NULL

		,A.Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Updated_By										-- NVarChar(128) NULL
	FROM Sales.Activities A
		LEFT JOIN Sales.Projects P ON A.Project_ID = P.Project_ID
		LEFT JOIN Sales.Activity_Types C ON A.Activity_Type_ID = C.Activity_Type_ID
	WHERE A.Scheduled_Start BETWEEN @TimeframeStart AND @TimeframeEnd
		OR A.Scheduled_End BETWEEN @TimeframeStart AND @TimeframeEnd
		OR @TimeframeStart BETWEEN A.Scheduled_Start AND A.Scheduled_End
		OR @TimeframeEnd BETWEEN A.Scheduled_Start AND A.Scheduled_End
		OR A.Started_On BETWEEN @TimeframeStart AND @TimeframeEnd
		OR A.Ended_On BETWEEN @TimeframeStart AND @TimeframeEnd
		OR @TimeframeStart BETWEEN A.Started_On AND A.Ended_On
		OR @TimeframeEnd BETWEEN A.Started_On AND A.Ended_On
	ORDER BY A.Scheduled_Start DESC
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activities_Grid_3')
	DROP PROCEDURE Sales.Activities_Grid_3
go
CREATE PROCEDURE Sales.Activities_Grid_3
	(
	 @TimeframeStart SmallDateTime = NULL
	,@TimeframeEnd SmallDateTime = NULL
    ,@CompletedBy NVarChar(128)
	)
AS
	SET @TimeframeStart = ISNULL(@TimeframeStart, DATEADD(MONTH, -3, GETDATE()));
	SET @TimeframeEnd = ISNULL(@TimeframeEnd, DATEADD(MONTH, 3, GETDATE()));
	SELECT
		 A.Activity_ID										-- BigInt NOT NULL PRIMARY KEY IDENTITY(-9007199254740990, 1)
		,A.Project_ID										-- Int NULL FOREIGN KEY REFERENCES Sales.Projects(Project_ID)
		,A.Organization_ID									-- Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
		,A.Person_ID										-- Int NULL FOREIGN KEY REFERENCES CRM.Persons(Person_ID)
		,A.Customer_Name									-- NVarChar(256) NOT NULL
		,A.End_Customer										-- NVarChar(256) NULL

		,P.Title AS Project_Title							-- NVarChar(256) NULL

		,A.Activity_Status_ID								-- TinyInt NOT NULL FOREIGN KEY REFERENCES Sales.App_Activity_Status(Activity_Status_ID)
		--,A.Activity_Status_Comment						-- NVarChar(256) NULL
		,A.Activity_Type_ID									-- SmallInt NULL FOREIGN KEY REFERENCES Sales.Activity_Types(Activity_Type_ID)
		,A.Title											-- NVarChar(1024) NOT NULL
		--,A.Details										-- NVarChar(MAX) NULL

		,C.Activity_Type_Name AS Activity_Type				-- NVarChar(64) NULL
		,C.Activity_Type_Category AS Activity_Category		-- NVarChar(64) NULL

		,A.Scheduled_Start									-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Scheduled_End									-- SmallDateTime NOT NULL DEFAULT DATEADD(HOUR, 2, GETDATE())
		,A.Assigned_On										-- SmallDateTime NULL
		,A.Assigned_To										-- NVarChar(128) NULL
		,A.Deadline_On										-- SmallDateTime NULL
		,A.Started_On										-- SmallDateTime NULL
		,A.Ended_On											-- SmallDateTime NULL
		,A.Reminder_On										-- SmallDateTime NULL

		,A.Requested_By										-- NVarChar(128) NULL
		,A.Requesting_Department							-- NVarChar(128) NULL
		,A.Completed_By										-- NVarChar(128) NULL
		,A.Logged_By										-- NVarChar(128) NULL
		,A.Billable											-- Bit NOT NULL DEFAULT 0
		,A.Invoice_IDs										-- VarChar(256) NULL
		,A.Duration_In_Minutes								-- Int NULL

		,A.Updated_On										-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,A.Updated_By										-- NVarChar(128) NULL
	FROM Sales.Activities A
		LEFT JOIN Sales.Projects P ON A.Project_ID = P.Project_ID
		LEFT JOIN Sales.Activity_Types C ON A.Activity_Type_ID = C.Activity_Type_ID
	WHERE Completed_By LIKE @CompletedBy
        AND
            (
            A.Scheduled_Start BETWEEN @TimeframeStart AND @TimeframeEnd
		    OR A.Scheduled_End BETWEEN @TimeframeStart AND @TimeframeEnd
		    OR @TimeframeStart BETWEEN A.Scheduled_Start AND A.Scheduled_End
		    OR @TimeframeEnd BETWEEN A.Scheduled_Start AND A.Scheduled_End
		    OR A.Started_On BETWEEN @TimeframeStart AND @TimeframeEnd
		    OR A.Ended_On BETWEEN @TimeframeStart AND @TimeframeEnd
		    OR @TimeframeStart BETWEEN A.Started_On AND A.Ended_On
		    OR @TimeframeEnd BETWEEN A.Started_On AND A.Ended_On
            )
	ORDER BY A.Scheduled_Start DESC
go
