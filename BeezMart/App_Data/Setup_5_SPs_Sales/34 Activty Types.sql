﻿






--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Ins')
	DROP PROCEDURE Sales.Activity_Types_Ins
go
CREATE PROCEDURE Sales.Activity_Types_Ins
	(
	 @ActivityTypeID SmallInt OUTPUT
	,@ActivityTypeName NVarChar(64)
	,@ActivityTypeCategory NVarChar(64) = NULL
	,@ActivityTypeComments NVarChar(MAX) = NULL
	,@Discontinued Bit = 0
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	INSERT INTO Sales.Activity_Types
		(
		 Activity_Type_Name
		,Activity_Type_Category
		,Activity_Type_Comments
		,Discontinued
		,Updated_On
		,Updated_By
		)
	VALUES
		(
		 @ActivityTypeName
		,@ActivityTypeCategory
		,@ActivityTypeComments
		,@Discontinued
		,@UpdatedOn
		,@UpdatedBy
		)
	SET @ActivityTypeID = SCOPE_IDENTITY()
	SELECT CAST(@ActivityTypeID AS SmallInt) AS ID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Upd')
	DROP PROCEDURE Sales.Activity_Types_Upd
go
CREATE PROCEDURE Sales.Activity_Types_Upd
	(
	 @ActivityTypeID SmallInt
	,@ActivityTypeName NVarChar(64)
	,@ActivityTypeCategory NVarChar(64) = NULL
	,@ActivityTypeComments NVarChar(MAX) = NULL
	,@Discontinued Bit = 0
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Sales.Activity_Types
	SET
		 Activity_Type_Name         = @ActivityTypeName
		,Activity_Type_Category     = @ActivityTypeCategory
		,Activity_Type_Comments     = @ActivityTypeComments
		,Discontinued               = @Discontinued
		,Updated_On                 = @UpdatedOn
		,Updated_By                 = @UpdatedBy
	WHERE Activity_Type_ID = @ActivityTypeID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Del')
	DROP PROCEDURE Sales.Activity_Types_Del
go
CREATE PROCEDURE Sales.Activity_Types_Del
	(
	 @ActivityTypeID SmallInt
	,@Discontinued Bit = 1
	,@UpdatedOn SmallDateTime = NULL
	,@UpdatedBy NVarChar(128) = NULL
	)
AS
	SET @UpdatedOn = ISNULL(@UpdatedOn, GETDATE())
	UPDATE Sales.Activity_Types
	SET
		 Discontinued   = @Discontinued
		,Updated_On     = @UpdatedOn
		,Updated_By     = @UpdatedBy
	WHERE Activity_Type_ID = @ActivityTypeID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Sel')
	DROP PROCEDURE Sales.Activity_Types_Sel
go
CREATE PROCEDURE Sales.Activity_Types_Sel
	(
	@ActivityTypeID SmallInt
	)
AS
	SELECT TOP 1
		 Activity_Type_ID				-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Activity_Type_Name				-- NVarChar(64) NOT NULL
		,Activity_Type_Category			-- NVarChar(64) NULL
		,Activity_Type_Comments			-- NVarChar(MAX) NULL
		,Discontinued					-- Bit NOT NULL DEFAULT 0
		,Updated_On						-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By						-- NVarChar(128) NULL
	FROM Sales.Activity_Types C
	WHERE Activity_Type_ID = @ActivityTypeID
go







--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Grid_1')
	DROP PROCEDURE Sales.Activity_Types_Grid_1
go
CREATE PROCEDURE Sales.Activity_Types_Grid_1
AS
	SELECT
		 Activity_Type_ID				-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Activity_Type_Name				-- NVarChar(64) NOT NULL
		,Activity_Type_Category			-- NVarChar(64) NULL
		,Activity_Type_Comments			-- NVarChar(MAX) NULL
		,Discontinued					-- Bit NOT NULL DEFAULT 0
		,Updated_On						-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By						-- NVarChar(128) NULL
	FROM Sales.Activity_Types
	WHERE Discontinued = 0
	ORDER BY Activity_Type_Category, Activity_Type_Name, Activity_Type_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Grid_2')
	DROP PROCEDURE Sales.Activity_Types_Grid_2
go
CREATE PROCEDURE Sales.Activity_Types_Grid_2
AS
	SELECT
		 Activity_Type_ID				-- SmallInt NOT NULL PRIMARY KEY IDENTITY(-32768, 1)
		,Activity_Type_Name				-- NVarChar(64) NOT NULL
		,Activity_Type_Category			-- NVarChar(64) NULL
		,Activity_Type_Comments			-- NVarChar(MAX) NULL
		,Discontinued					-- Bit NOT NULL DEFAULT 0
		,Updated_On						-- SmallDateTime NOT NULL DEFAULT GETDATE()
		,Updated_By						-- NVarChar(128) NULL
	FROM Sales.Activity_Types
	ORDER BY Activity_Type_Category, Activity_Type_Name, Activity_Type_ID
go
--=============================================================================
IF EXISTS (SELECT * FROM Sys.Objects WHERE Type = 'P' AND SCHEMA_NAME(SCHEMA_ID) = 'Sales' AND Name = N'Activity_Types_Grid_3')
	DROP PROCEDURE Sales.Activity_Types_Grid_3
go
CREATE PROCEDURE Sales.Activity_Types_Grid_3
AS
	SELECT DISTINCT
		Activity_Type_Category			-- NVarChar(64) NULL
	FROM Sales.Activity_Types
	WHERE Discontinued = 0
	ORDER BY Activity_Type_Category
go