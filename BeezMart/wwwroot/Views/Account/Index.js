﻿var MyPageDOM = {};



function Page_Start()
{
	MyPageDOM.ModalResetPassword = jQuery("#ModalResetPassword form");
	jQuery("input[name='NewPassword']").siblings("span").children("a").click(
			function()
			{
				let suggestedPassword = jQuery(this).text();
				jQuery("input[name='NewPassword']").val(suggestedPassword);
				jQuery("input[name='PasswordConfirm']").val(suggestedPassword);
			}
		);
	Nurbiosis.AjaxifyForm(MyPageDOM.ModalResetPassword).Target("#ResetPasswordResultMessage").Progress("#AccountFields progress");
}



jQuery(Page_Start);