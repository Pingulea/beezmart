﻿


if (!Nurbiosis)
{
	var Nurbiosis = {};
}



Nurbiosis.ColorTheme = {
	ThemeCheckbox: null,
	GetStoredTheme: () => localStorage.getItem("colorTheme"),
	SetStoredTheme: theme => localStorage.setItem("colorTheme", theme),
	GetPreferredTheme: () => {
		const storedTheme = Nurbiosis.ColorTheme.GetStoredTheme();
		if (storedTheme) { return storedTheme; }
		return window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
	},
	SetTheme: theme => {
		if (theme === "auto" && window.matchMedia("(prefers-color-scheme: dark)").matches) {
			document.documentElement.setAttribute("data-bs-theme", "dark");
			Nurbiosis.ColorTheme.ThemeCheckbox.checked = true;
		}
		else {
			document.documentElement.setAttribute("data-bs-theme", theme);
			Nurbiosis.ColorTheme.ThemeCheckbox.checked = (theme === "dark");
		}
	},
	Startup: () => {
		Nurbiosis.ColorTheme.ThemeCheckbox = document.querySelector("[data-bs-theme-value]");
		if (!Nurbiosis.ColorTheme.ThemeCheckbox) {
			console.warn("Theme checkbox not found. User would not be able to change color theme.");
		}
		Nurbiosis.ColorTheme.ThemeCheckbox.addEventListener("change", () => {
			const pickedTheme = Nurbiosis.ColorTheme.ThemeCheckbox.checked ? "dark" : "light";
			console.log("Application's color theme was changed to: " + pickedTheme);
			Nurbiosis.ColorTheme.SetStoredTheme(pickedTheme);
			document.documentElement.setAttribute("data-bs-theme", pickedTheme);
		});
		window.matchMedia("(prefers-color-scheme: dark)").addEventListener("change", () => {
			const storedTheme = Nurbiosis.ColorTheme.GetStoredTheme();
			if (storedTheme !== "light" && storedTheme !== "dark") {
				Nurbiosis.ColorTheme.SetTheme(Nurbiosis.ColorTheme.GetPreferredTheme());
			}
		});
		Nurbiosis.ColorTheme.SetTheme(Nurbiosis.ColorTheme.GetPreferredTheme());
	}
}



Nurbiosis.SideBar = {
	Toggler: null,
	IsExpanded: false,
	Container: null,
	GetStoredIsExpanded: () => localStorage.getItem("SideBarIsExpanded"),
	SetStoredIsExpanded: expanded => localStorage.setItem("SideBarIsExpanded", expanded),
	Toggle: () => {
		Nurbiosis.SideBar.IsExpanded = !Nurbiosis.SideBar.IsExpanded;
		Nurbiosis.SideBar.SetStoredIsExpanded(Nurbiosis.SideBar.IsExpanded.toString());
		Nurbiosis.SideBar.Show();
	},
	Show: () => {
		if (Nurbiosis.SideBar.IsExpanded) {
			Nurbiosis.SideBar.Toggler.firstElementChild.classList.remove("bi-arrow-bar-right");
			Nurbiosis.SideBar.Toggler.firstElementChild.classList.add("bi-arrow-bar-left");
			Nurbiosis.SideBar.Container.classList.add("sidebar-wide");
			setTimeout(() => { Nurbiosis.SideBar.Container.classList.remove("sidebar-slim"); }, 300);
		}
		else {
			Nurbiosis.SideBar.Toggler.firstElementChild.classList.remove("bi-arrow-bar-left");
			Nurbiosis.SideBar.Toggler.firstElementChild.classList.add("bi-arrow-bar-right");
			Nurbiosis.SideBar.Container.classList.remove("sidebar-wide");
			Nurbiosis.SideBar.Container.classList.add("sidebar-slim");
		}
	},
	Startup: () => {
		Nurbiosis.SideBar.Toggler = document.querySelector(".sidebar-toggler button");
		if (!Nurbiosis.SideBar.Toggler) return;
		Nurbiosis.SideBar.Container = document.querySelector(".sidebar");
		if (!Nurbiosis.SideBar.Container) return;
		let buttons = Nurbiosis.SideBar.Container.querySelectorAll("a.nav-link");
		for (let i = 0; i < buttons.length; i++) {
			buttons[i].classList.add("rounded-0");
			buttons[i].classList.add("py-3");
			buttons[i].classList.add("border-bottom");
		}
		Nurbiosis.SideBar.Toggler.addEventListener("click", Nurbiosis.SideBar.Toggle);
		Nurbiosis.SideBar.IsExpanded = (Nurbiosis.SideBar.GetStoredIsExpanded() === "true");
		Nurbiosis.SideBar.Show();
	}
}



Nurbiosis.EnableTabs = function (TabsListSelector, ActiveTabNumber) {
    /// <summary>
    ///		<para>Turns a list of sibling elements, like LIs in a UL, into selectors for tab pages.</para>
    ///		<para>Use the attribute 'data-target' on the parent of the sibling-selectors to determine the container of the tab-pages.</para>
    ///		<para>Use '.active' css class at a selector level to determine the tab initially selected.</para>
    /// </summary>
    /// <param name="TabsListSelector" type="String">The jQuery selector of the parent element holding the tab selectors. Example: an UL or OL</param>
    /// <returns type="Number">The index of the currently selected tab, or -1 if no tab is selected</returns>
    var TabButtonsParent = jQuery(TabsListSelector);
    var TabPages = jQuery(TabButtonsParent.attr("data-target")).children();
    TabPages.each(
        function () {
            var yePage = jQuery(this);
            yePage.hide();
            if (yePage.hasClass("active")) yePage.removeClass("active");
        }
    );
    var CurrentSelectorIndex = -1;
    if (ActiveTabNumber && !isNaN(ActiveTabNumber) && ActiveTabNumber <= TabButtonsParent.children().length) {
        ActiveTabNumber = ActiveTabNumber - 1;
    }
    if (ActiveTabNumber >= 0) {
        TabButtonsParent.children().each(
            function () {
                var yeSelector = jQuery(this);
                var yePage = TabPages.eq(yeSelector.index());
                if (yeSelector.hasClass("active")) yeSelector.removeClass("active");
            }
        );
        TabPages.eq(ActiveTabNumber).addClass("active").show();
        TabButtonsParent.children().eq(ActiveTabNumber).addClass("active");
    }
    else {
        TabButtonsParent.children().each(
            function () {
                var yeSelector = jQuery(this);
                var yePage = TabPages.eq(yeSelector.index());
                if (yeSelector.hasClass("active") && CurrentSelectorIndex < 0) {
                    yePage.show();
                    yePage.addClass("active");
                    CurrentSelectorIndex = yeSelector.index();
                }
            }
        );
    }
    var CurrentTabPage = null;
    TabButtonsParent.children().click(
        function () {
            var ClickedSelector = jQuery(this);
            if (ClickedSelector.hasClass("active")) return;
            TabButtonsParent.children().each(function () { jQuery(this).removeClass("active"); });
            ClickedSelector.addClass("active");
            var ClickedSelectorIndex = ClickedSelector.index();
            if (ClickedSelectorIndex >= TabPages.length) {
                toastr.warning("The " + (ClickedSelectorIndex + 1) + "-th tab page is not yet implemented!", "Not available");
                return;
            }
            TabPages.each(
                function () {
                    if (jQuery(this).hasClass("active")) CurrentTabPage = jQuery(this);
                }
            );
            if (CurrentTabPage === null) {
                TabPages.eq(ClickedSelectorIndex).show();
                TabPages.eq(ClickedSelectorIndex).addClass("active");
            }
            else {
                CurrentTabPage.removeClass("active");
                CurrentTabPage.hide(
                    function () {
                        TabPages.eq(ClickedSelectorIndex).show();
                        TabPages.eq(ClickedSelectorIndex).addClass("active");
                    }
                );
            }
        }
    );
    return CurrentSelectorIndex;
};



Nurbiosis.EnableTinyMCE = function (TextareaSelector) {
    /// <summary>
    ///		<para>Turns a <textarea> element into a WYSIWYG HTML editor.</para>
    /// </summary>
    /// <param name="TextareaSelector" type="String">The CSS selector of the textarea element</param>
    /// <returns type="void">does not return anything</returns>
    let TinyMceEditorSettings =
    {
        selector: TextareaSelector ? TextareaSelector : "textarea",
        content_css: "../../Views/Shared/TinyMCE_Content.css",
        menubar: false,
        plugins: "charmap code emoticons hr insertdatetime link paste searchreplace table textcolor visualchars",
        toolbar1: "bold italic underline strikethrough subscript superscript | bullist numlist | link unlink | forecolor backcolor | hr table charmap emoticons",
        toolbar2: "undo redo cut copy paste removeformat searchreplace | alignleft aligncenter alignright alignjustify outdent indent visualchars"
    };
    tinymce.init(TinyMceEditorSettings);
};



Nurbiosis.SyncronizeDropdown = function (DropdownsSelector) {
    /// <summary>
    ///		<para>Takes a dropdown and ensures its SelectedValue matches the "data-selected" attribute.</para>
    ///		<para>If the DropdownsSelectoris missing, all drop-downs having "data-selected" attribute will be sincronized.</para>
    /// </summary>
    /// <param name="DropdownsSelector" type="String">The CSS selector of the drop-downs to sincronize.</param>
    /// <returns type="void">does not return anything</returns>
    var dropdowns;
    if (DropdownsSelector) {
        dropdowns = jQuery(DropdownsSelector);
    }
    else {
        dropdowns = jQuery("select[data-selected]");
    }
    function SyncronizeOneDropdown(dropdown) {
        var valueToSelect = dropdown.attr("data-selected");
        dropdown.val(valueToSelect);
    }
    dropdowns.each(function () { SyncronizeOneDropdown(jQuery(this)); });
};



Nurbiosis.CascadeDropdowns = function (DropdownSourceID, DropdownTargetID, CategoryAttributeName) {
    /// <summary>
    ///		<para>Turns two SELECT controls in cascading drop-downs:</para>
    ///		<para>the options displayed by the target control depend on the options selected in the source control.</para>
    /// </summary>
    /// <param name="DropdownSourceID" type="String">The ID of the 'master' drop-down SELECT control</param>
    /// <param name="DropdownTargetID" type="String">The ID of the 'child' drop-down SELECT control</param>
    /// <param name="CategoryAttributeName" type="String">The name of the attribute in the child SELECT options holding the category</param>
    /// <returns type="void"></returns>
    // source inspiration: http://www.weberdev.com/get_example.php3?ExampleID=4505
    let sourceControl = document.getElementById(DropdownSourceID);
    let targetControl = document.getElementById(DropdownTargetID);
    sourceControl.childSelect = targetControl;
    targetControl.categoryAttributeName = CategoryAttributeName ? CategoryAttributeName : "data-val-category";
    if (sourceControl.options.length === 0 || targetControl.options.length === 0) return;
    function DisplayChildOptionsByCategoryName(selectElement, categoryName) {
        if (!sourceControl || !targetControl) return;
        if (!selectElement.backup) {
            selectElement.backup = selectElement.cloneNode(true);
        }
        var options = selectElement.getElementsByTagName("option");
        for (let i = 0, length = options.length; i < length; i++) {
            selectElement.removeChild(options[0]);
        }
        var backupOptions = selectElement.backup.getElementsByTagName("option");
        for (let i = 0, length = backupOptions.length; i < length; i++) {
            if (backupOptions[i].getAttribute(selectElement.categoryAttributeName) === categoryName)
                selectElement.appendChild(backupOptions[i].cloneNode(true));
        }
    }
    sourceControl.onchange = function () {
        let selectedOption = sourceControl.options[sourceControl.selectedIndex];
        let selectedCategory = selectedOption.text;
        if (selectedOption.hasAttribute("value")) {
            selectedCategory = selectedOption.getAttribute("value");
        }
        DisplayChildOptionsByCategoryName(this.childSelect, selectedCategory);
    };
    sourceControl.onchange();
};



Nurbiosis.AttachDatepicker = function (TextboxSelector) {
    ///	<summary>
    ///		<para>Attaches a jQueryUI DatePicker to an input control, if specified, or to all input[type='date'], input[class*='Calendar'].</para>
    ///		<para>If the input control has a sibling with class .Calendar, that sibling will show the DatePicker on click.</para>
    ///		<para>This will not work for controls inside a bootstrap modal; use Nurbiosis.InitializeCalendar() instead.</para>
    ///		<para></para>
    ///		<para></para>
    /// </summary>
    /// <param name="TextboxSelector" type="String">The jQuery selector of the target input element</param>
    /// <returns type="void"></returns>
    function InitializeCalendarDatePicker(jQueryInputControl, jQueryButton) {
        var format = "yy-mm-dd";
        if (jQueryInputControl.attr("placeholder")) format = jQueryInputControl.attr("placeholder").toLowerCase().replace("yyyy", "yy");
        var DatePickerSettings = {
            showOn: "focus",
            buttonImageOnly: true,
            dateFormat: format,
            altFormat: format,
            duration: "normal"
        };
        if (jQueryInputControl) jQueryInputControl.datepicker(DatePickerSettings);
        if (jQueryButton) jQueryButton.click(
            function () {
                if (jQueryButton.hasClass("Calendar-Shown")) {
                    jQueryButton.removeClass("Calendar-Shown");
                    jQueryInputControl.datepicker("hide");
                }
                else {
                    jQueryInputControl.datepicker("show");
                    jQueryButton.addClass("Calendar-Shown");
                }
            }
        );
    }
    if (TextboxSelector) {
        var inputCtl = jQuery(TextboxSelector);
        if (inputCtl.length !== 1) {
            console.error("Nurbiosis.AttachDatepicker(): Could not resolve TextboxSelector to a jQuery element.");
            return;
        }
        var buttonCtl = inputCtl.siblings(".Calendar");
        buttonCtl.css("cursor", "pointer");
        InitializeCalendarDatePicker(inputCtl, buttonCtl);
    }
    else {
        var inputCtls = jQuery("input[type='date'], input[class*='Calendar']");
        inputCtls.each(
            function () {
                var inputCtl = jQuery(this);
                var buttonCtl = inputCtl.siblings(".Calendar");
                buttonCtl.css("cursor", "pointer");
                InitializeCalendarDatePicker(inputCtl, buttonCtl);
            }
        );
    }
};



Nurbiosis.InitializeCalendar = function(ElementSelector)
{
	///	<summary>
	///		<para>Transforms an element into a jQuery UI calendar datepicker.</para>
	///		<para>The calendar can be bound to an input control; when a date is selected, the input control will hold the selected value.</para>
	///		<para>This will not work for controls inside a bootstrap modal; use Nurbiosis.InitializeCalendar() instead.</para>
	/// </summary>
	/// <param name="TextboxSelector" type="String">The jQuery selector of the target input element</param>
	/// <returns type="void"></returns>
	var that = this;
	this.Options =
		{
			UpdateTarget: null,
			OnSelect: null,
			DatepickerSettings: 
				{
					dateFormat: "yy-mm-dd",
					firstDay: 1,
					gotoCurrent: true
				}
		};
	this.CalendarElement = jQuery(ElementSelector);
	if (this.CalendarElement.length !== 1)
	{
		console.error("Nurbiosis.InitializeCalendar(): Could not resolve ElementSelector to a jQuery element.");
		return null;
	}
	var CalendarOptions = this.Options;
	this.Target = function(targetSelector)
		{
			/// <summary>
			///		<para>Sets the target input element that will have its value updated with the selected date.</para>
			/// </summary>
			var target = jQuery(targetSelector);
			if (target.length > 0)
			{
				CalendarOptions.UpdateTarget = target;
			}
			return this;
		};
	this.OnSelect = function(functionName)
		{
			CalendarOptions.OnSelect = functionName;
			return this;
		};
	this.SetDate = function(DateToSet)
		{
			if (CalendarOptions.UpdateTarget)
			{
				CalendarOptions.UpdateTarget.val(DateToSet);
			}
			this.CalendarElement.datepicker("setDate", DateToSet);
		};
	this.go = function()
		{
			CalendarOptions.DatepickerSettings.onSelect = function(selectedDate)
				{
					if (CalendarOptions.UpdateTarget) CalendarOptions.UpdateTarget.val(selectedDate);
					if (CalendarOptions.OnSelect) CalendarOptions.OnSelect(selectedDate);
				};
			this.CalendarElement.datepicker(CalendarOptions.DatepickerSettings);
			if (CalendarOptions.UpdateTarget)
			{
				if (CalendarOptions.UpdateTarget.val())
				{
					this.CalendarElement.datepicker("setDate", CalendarOptions.UpdateTarget.val());
				}
			}
			return this;
		};
	this.go();
	return this;
};



Nurbiosis.MessageBox = function(AlertBoxSelector)
{
	///	<summary>
	///		<para>Displays notification messages in an Bootstrap alert component, pointed at through a CSS selector as parameter.</para>
	///		<para>If AlertBoxSelector is missing, the first .AppMessage.alert element in the body will be used (or created).</para>
	///		<para>The .AppMessage.alert element should have a div inside as the message wrapper. May have optional class .sliding.</para>
	/// </summary>
	/// <param name="AlertBoxSelector" type="String">The jQuery selector of the target alert element</param>
	/// <returns type="void"></returns>
	var self = this;
	if (AlertBoxSelector) { this.HtmlAlertElement = jQuery(AlertBoxSelector); }
	else { this.HtmlAlertElement = jQuery(".AppMessage.alert"); }
	if (this.HtmlAlertElement.length === 0)
	{
		// Better place following HTML piece in the document:
		// <section class="AppMessage alert"><button type="reset" class="close">&times;</button><div></div><button type="reset" class="btn btn-light pull-right">OK</button></section>
		jQuery("body").prepend("<section class='AppMessage alert'><div></div></section>");
		this.HtmlAlertElement = jQuery(".AppMessage.alert");
	}
	else
	{
		this.HtmlAlertElement = this.HtmlAlertElement.eq(0);
	}
	console.info(this.HtmlAlertElement.length);
	this.Dismiss = function()
		{
			if (self.HtmlAlertElement.hasClass("sliding")) { self.HtmlAlertElement.slideUp(); }
			else { self.HtmlAlertElement.hide(); }
		};
	this.Content = this.HtmlAlertElement.children("div");
	this.DisplayXCloseButton = true;
	this.DisplayOkButton = true;

	this.Title = "Notification";
	this.Messages = new Array();
	this.AddMessage = function(message)
		{
			this.Messages.push("<p>" + message + "</p>");
		};
	this.BoxTypes =
		{
			Success: "success",
			Warning: "warning",
			Danger: "danger",
			Info: "info"
		};
	this.Type = this.BoxTypes.Info;
	this.Clear = function()
		{
			while (this.Messages.length > 0)
			{
				this.Messages.pop();
			}
		};
	this.ShowError = function(message) { this.Show("Error", message, this.BoxTypes.Danger); };
	this.ShowWarning = function(message) { this.Show("Warning", message, this.BoxTypes.Warning); };
	this.ShowSuccess = function(message) { this.Show("Success", message, this.BoxTypes.Success); };
	this.ShowInfo = function(message) { this.Show("Info", message, this.BoxTypes.Info); };
	this.Show = function(title, message, type)
		{
			if (this.Content.length === 0)
			{
				this.HtmlAlertElement.append("<div></div>");
				this.Content = this.HtmlAlertElement.children("div");
			}
			else
			{
				this.Content = this.Content.eq(0);
			}
			var XCloseButton;
			if (this.DisplayXCloseButton)
			{
				XCloseButton = this.HtmlAlertElement.find(".close");
				if (XCloseButton.length === 0)
				{
					this.Content.append("<button type='reset' class='close'>&times;</button>");
					XCloseButton = this.HtmlAlertElement.find(".close");
				}
			}
			if (title) { this.Title = title; }
			if (message) { this.AddMessage(message); }
			if (type) { this.Type = type.toLowerCase(); }
			this.HtmlAlertElement.removeClass("alert-info");
			this.HtmlAlertElement.removeClass("alert-success");
			this.HtmlAlertElement.removeClass("alert-warning");
			this.HtmlAlertElement.removeClass("alert-danger");
			switch (this.Type.toLowerCase())
			{
				case this.BoxTypes.Success: { this.HtmlAlertElement.addClass("alert-success"); break; }
				case this.BoxTypes.Warning: { this.HtmlAlertElement.addClass("alert-warning"); break; }
				case this.BoxTypes.Danger: { this.HtmlAlertElement.addClass("alert-danger"); break; }
				default: { this.HtmlAlertElement.addClass("alert-info"); break; }
			}
			this.Content.append("<h4>" + this.Title + "</h4>");
			while (this.Messages.length > 0)
			{
				this.Content.append(this.Messages.pop());
			}
			if (this.DisplayOkButton)
			{
				this.Content.append("<button type='reset' class='btn btn-light pull-right'>OK</button>");
			}
			this.HtmlAlertElement.find("button[type='reset']").click(self.Dismiss);
			this.Content.append("<div class='clearfix'></div>");
			if (self.HtmlAlertElement.hasClass("sliding")) { self.HtmlAlertElement.slideDown(); }
			else { self.HtmlAlertElement.show(); }
		};
	return this;
};



Nurbiosis.EnableTristate = function(ElementSelector)
{
	/// <summary>
	///		<para>Creates a sort of tri-state checkbox. It uses by default elements with a .tristate css class.</para>
	///		<para>Place an INPUT having value TRUE, FALSE or empty inside an element with '.tristate' class, and you're done.</para>
	///		<para>If not present, inside the .tristate there will be appended a .fa inline element.</para>
	/// </summary>
	/// <param name="ElementSelector" type="String">The jQuery selector of the element holding the input and glyph. If not specified, a default '.tristate' is assumed.</param>
	/// <returns></returns>
	function InitializeTristate(tristateElement)
	{
		var inputs = tristateElement.find("input");
		var glyphs = tristateElement.find("[class^='bi-']");
		if (inputs.length === 0) tristateElement.prepend("<input type='hidden' />");
		if (glyphs.length === 0) tristateElement.append("<b class='bi-question-circle'></b>");
		var inputElement = tristateElement.find("input").eq(0);
		var glyphElement = tristateElement.find("[class^='bi-']").eq(0);
		glyphElement.removeClass("bi-question-circle");
		glyphElement.removeClass("bi-x-circle");
		glyphElement.removeClass("bi-check-circle");
		switch (inputElement.val().toLowerCase())
		{
			case "true":
				glyphElement.addClass("bi-check-circle");
				tristateElement.attr("title", "Yes, true");
				break;
			case "false":
				glyphElement.addClass("bi-x-circle");
				tristateElement.attr("title", "No, false");
				break;
			default:
				glyphElement.addClass("bi-question-circle");
				tristateElement.attr("title", "Unknown");
				break;
		}
		if (tristateElement.hasClass("disabled")) return;
		if (inputElement.prop("disabled"))
		{
			tristateElement.addClass("disabled");
			return;
		}
		tristateElement.click(
				function()
				{
					switch (inputElement.val().toLowerCase())
					{
						case "true":
							inputElement.val("false");
							glyphElement.removeClass("bi-check-circle");
							glyphElement.addClass("bi-x-circle");
							tristateElement.attr("title", "No, false");
							break;
						case "false":
							inputElement.val("");
							glyphElement.removeClass("bi-x-circle");
							glyphElement.addClass("bi-question-circle");
							tristateElement.attr("title", "Unknown");
							break;
						default:
							inputElement.val("true");
							glyphElement.removeClass("bi-question-circle");
							glyphElement.addClass("bi-check-circle");
							tristateElement.attr("title", "Yes, true");
							break;
					}
				}
			);
	}
	if (!ElementSelector) ElementSelector = ".tristate";
	jQuery(ElementSelector).each(function() { InitializeTristate(jQuery(this)); });
};



Nurbiosis.AjaxifyForm = function(FormSelector)
{
	///	<summary>
	///		<para>Turns the default POST behavior of a form into an AJAX call. Disables the submit buttons and shows a progress element.</para>
	///		<para>Parameter: the jQuery/CSS selector of the form to be ajaxified.</para>
	///		<para>Options are passed through chaining methods: Progress, Target, Before, OnSuccess, OnError, OnComplete, ResponseType, HideModal.</para>
	///	</summary>
	/// <param name="FormSelector" type="String">jQuery selector of the FORM element to send as data with the AJAX call.</param>
	/// <param name="Progress" type="String">jQuery selector of the progress bar to show during AJAX call. Progress element will be hidden initially.</param>
	/// <param name="Target" type="String">jQuery selector HTML element to be filled with the response data. If ResponseType=JSON, children with 'data-bound' attribute will have the 'value' populated.</param>
	/// <param name="Before" type="Function">Operations to do right after sending the AJAX call, but before getting data. Progress element is shown by default.</param>
	/// <param name="OnSuccess" type="Function">Tasks to do, right after filling the Target element with the data resulted from the call.</param>
	/// <param name="OnError" type="Function">Tasks to do just before alerting the user about the communication failure.</param>
	/// <param name="OnComplete" type="Function">Tasks to to before re-enabling submit buttons and hidding Progress element.</param>
	/// <param name="ResponseType" type="String">If HTML, the Target content will be filled with returned data. If JSON, the elements in Target having data-bound attribute will have the value populated.</param>
	/// <param name="HideModal" type="Boolean">On submitting, hide the parent modal dialog (if exists). Default value is true.</param>
	/// <returns type="jQuery">Returns this function object, to allow chaining calls on methods/setters.</returns>
	var ActionForm = jQuery(FormSelector);
	if (ActionForm.length !== 1)
	{
		console.error("Nurbiosis.AjaxifyForm(): Could not resolve FormSelector to a jQuery element.");
		return null;
	}
	if (!ActionForm.AjaxOptions)
	{
		ActionForm.AjaxOptions =
			{
				UpdateTarget: null,
				ProgressBar: null,
				HideModal: true,
				SubmitButtons: ActionForm.find("button[type='submit'], input[type='submit']"),
				Before: null,
				OnError: null,
				OnSuccess: null,
				OnComplete: null,
				AjaxCallSettings: 
					{
						type: ActionForm.attr("method") ? ActionForm.attr("method").toLowerCase() : "post",
						url: ActionForm.attr("action") ? ActionForm.attr("action") : window.location,
						dataType: "html",
						cache: false
					}
			};
	}
	var AjaxifyOptions = ActionForm.AjaxOptions;
	this.ResponseType = function(responseType)
		{
			/// <summary>
			///		<para>Changes the expeted response type and behavior. Accepted values are "html" and "json".</para>
			///		<para>If HTML, content of the target is updated with the response.</para>
			///		<para>If JSON, input elements having 'data-bind' attributes will have their value updated with corresponding members of received data structure.</para>
			/// </summary>
			AjaxifyOptions.AjaxCallSettings.dataType = responseType.toLowerCase();
			return this;
		};
	this.Target = function(targetSelector)
		{
			/// <summary>
			///		<para>Sets the target element that will have the content updated with the AJAX call result. Parameter: the jQuery/CSS selector of the target element.</para>
			///		<para>If HTML, content of the target is updated with the response.</para>
			///		<para>If JSON, input elements having 'data-bind' attributes will have their value updated with corresponding members of received data structure.</para>
			/// </summary>
			var target = jQuery(targetSelector);
			if (target)
			{
				AjaxifyOptions.UpdateTarget = target;
			}
			return this;
		};
	this.Progress = function(progressSelector)
		{
			/// <summary>
			///		<para>Sets the progress displaying element that will show up during the AJAX call.</para>
			///		<para>Parameter: the jQuery/CSS selector of the target element.</para>
			/// </summary>
			var progress = jQuery(progressSelector);
			if (progress)
			{
				AjaxifyOptions.ProgressBar = progress;
			}
			return this;
		};
	this.HideModal = function(boolean)
		{
			/// <summary>
			///		<para>Parameter: a boolean indicating if parent modal(s) of the form will be closed/hidden at form submission, when the AJAX call is performed.</para>
			/// </summary>
			AjaxifyOptions.HideModal = boolean;
			return this;
		};
	this.Before = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called right after sending the AJAX request.</para>
			///		<para>The function will be most probably executed before the AJAX response is received.</para>
			/// </summary>
			AjaxifyOptions.Before = functionName;
			return this;
		};
	this.OnError = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon error of the AJAX call.</para>
			///		<para>The function is executed before an error alert is displayed to the user.</para>
			/// </summary>
			AjaxifyOptions.OnError = functionName;
			return this;
		};
	this.OnSuccess = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon successfully receiving the AJAX call result.</para>
			///		<para>The function is executed after the call result is processed (eg. target element has its HTML updated).</para>
			///		<para>The received data is passed to this function as parameter.</para>
			/// </summary>
			AjaxifyOptions.OnSuccess = functionName;
			return this;
		};
	this.OnComplete = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called after AJAX call completes, successfully or not.</para>
			///		<para>The function is executed after re-enabling submit buttons and hidding the progress element.</para>
			/// </summary>
			AjaxifyOptions.OnComplete = functionName;
			return this;
		};
	AjaxifyOptions.AjaxCallSettings.success = function(data)
		{
			if (AjaxifyOptions.UpdateTarget)
			{
				if (AjaxifyOptions.AjaxCallSettings.dataType === "html")
				{
					AjaxifyOptions.UpdateTarget.html(data);
				}
				if (AjaxifyOptions.AjaxCallSettings.dataType === "json")
				{
					for (var prop in data)
					{
						if (data.hasOwnProperty(prop))
						{
							AjaxifyOptions.UpdateTarget.find("[data-bound='" + prop + "']").val(data[prop]);
						}
					}
				}
			}
			if (AjaxifyOptions.OnSuccess) AjaxifyOptions.OnSuccess(data);
		};
	AjaxifyOptions.AjaxCallSettings.error = function()
		{
			if (AjaxifyOptions.OnError) AjaxifyOptions.OnError();
			toastr.error("Operation failed while communicating with the server.", "Error");
		};
	AjaxifyOptions.AjaxCallSettings.complete = function()
		{
			if (AjaxifyOptions.SubmitButtons) AjaxifyOptions.SubmitButtons.prop("disabled", false);
			if (AjaxifyOptions.ProgressBar) AjaxifyOptions.ProgressBar.hide();
			if (AjaxifyOptions.OnComplete) AjaxifyOptions.OnComplete();
		};
	function DoSubmit(event)
	{
		event.preventDefault();
		if (AjaxifyOptions.SubmitButtons) AjaxifyOptions.SubmitButtons.prop("disabled", true);
		if (AjaxifyOptions.ProgressBar) AjaxifyOptions.ProgressBar.show();
		AjaxifyOptions.AjaxCallSettings.data = ActionForm.serialize();
		jQuery.ajax(AjaxifyOptions.AjaxCallSettings);
		if (AjaxifyOptions.HideModal) ActionForm.parents(".modal").modal("hide");
		if (AjaxifyOptions.Before) AjaxifyOptions.Before();
	}
	ActionForm.submit(DoSubmit);
	return this;
};



Nurbiosis.AjaxifiedForm = function(FormSelector)
{
	///	<summary>
	///		<para>Submits a form and inserts the response into an element, through AJAX, optionally running some code on success, error or completion.</para>
	///		<para>Use when the form does not have any submit buttons (maybe no visible controls at all), and submission must be done programmatically.</para>
	///		<para>Parameter: the jQuery/CSS selector of the form to be ajaxified.</para>
	///		<para>Options are passed through chaining methods: Progress, Target, Before, OnSuccess, OnError, OnComplete, ResponseType, HideModal.</para>
	///	</summary>
	/// <param name="FormSelector" type="String">jQuery selector of the FORM element to send as data with the AJAX call.</param>
	/// <param name="Progress" type="String">jQuery selector of the progress bar to show during AJAX call. Progress element will be hidden initially.</param>
	/// <param name="Target" type="String">jQuery selector HTML element to be filled with the response data. If ResponseType=JSON, children with 'data-bound' attribute will have the 'value' populated.</param>
	/// <param name="Before" type="Function">Operations to do right after sending the AJAX call, but before getting data. Progress element is shown by default.</param>
	/// <param name="OnSuccess" type="Function">Tasks to do, right after filling the Target element with the data resulted from the call.</param>
	/// <param name="OnError" type="Function">Tasks to do just before alerting the user about the communication failure.</param>
	/// <param name="OnComplete" type="Function">Tasks to to before re-enabling submit buttons and hidding Progress element.</param>
	/// <param name="ResponseType" type="String">If HTML, the Target content will be filled with returned data. If JSON, the elements in Target having data-bound attribute will have the value populated.</param>
	/// <param name="HideModal" type="Boolean">On submitting, hide the parent modal dialog (if exists). Default value is true.</param>
	/// <returns type="jQuery">Returns this function object, to allow chaining calls on methods/setters.</returns>
	var that = this;
	this.ActionForm = jQuery(FormSelector);
	if (this.ActionForm.length !== 1)
	{
		console.error("Nurbiosis.AjaxifiedForm() constructor: Could not resolve FormSelector to a jQuery element.");
		return null;
	}
	this.Options =
		{
			UpdateTarget: null,
			ProgressBar: null,
			HideModal: true,
			SubmitButtons: this.ActionForm.find("button[type='submit'], input[type='submit']"),
			Before: null,
			OnError: null,
			OnSuccess: null,
			OnComplete: null,
			AjaxCallSettings: 
				{
					type: this.ActionForm.attr("method") ? this.ActionForm.attr("method").toLowerCase() : "post",
					url: this.ActionForm.attr("action") ? this.ActionForm.attr("action") : window.location,
					dataType: "html",
					cache: false
				}
		};
	this.ResponseType = function(responseType)
		{
			/// <summary>
			/// <para>Changes the expeted response type and behavior. Accepted values are "html" and "json".</para>
			/// <para>If HTML, content of the target is updated with the response.</para>
			/// <para>If JSON, input elements having 'data-bind' attributes will have their value updated with corresponding members of received data structure.</para>
			/// </summary>
			this.Options.AjaxCallSettings.dataType = responseType.toLowerCase();
			return this;
		};
	this.Target = function(targetSelector)
		{
			/// <summary>
			///		<para>Sets the target element that will have the content updated with the AJAX call result. Parameter: the jQuery/CSS selector of the target element.</para>
			///		<para>If HTML, content of the target is updated with the response.</para>
			///		<para>If JSON, input elements having 'data-bind' attributes will have their value updated with corresponding members of received data structure.</para>
			/// </summary>
			var target = jQuery(targetSelector);
			if (target)
			{
				this.Options.UpdateTarget = target;
			}
			return this;
		};
	this.Progress = function(progressSelector)
		{
			/// <summary>
			///		<para>Sets the progress displaying element that will show up during the AJAX call.</para>
			///		<para>Parameter: the jQuery/CSS selector of the target element.</para>
			/// </summary>
			var progress = jQuery(progressSelector);
			if (progress)
			{
				this.Options.ProgressBar = progress;
			}
			return this;
		};
	this.HideModal = function(boolean)
		{
			/// <summary>
			///		<para>Parameter: a boolean indicating if parent modal(s) of the form will be closed/hidden at form submission, when the AJAX call is performed.</para>
			/// </summary>
			this.Options.HideModal = boolean;
			return this;
		};
	this.Before = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called right after sending the AJAX request.</para>
			///		<para>The function will be most probably executed before the AJAX response is received.</para>
			/// </summary>
			this.Options.Before = functionName;
			return this;
		};
	this.OnError = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon error of the AJAX call.</para>
			///		<para>The function is executed before an error alert is displayed to the user.</para>
			/// </summary>
			this.Options.OnError = functionName;
			return this;
		};
	this.OnSuccess = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon successfully receiving the AJAX call result.</para>
			///		<para>The function is executed after the call result is processed (eg. target element has its HTML updated).</para>
			///		<para>The received data is passed to this function as parameter.</para>
			/// </summary>
			this.Options.OnSuccess = functionName;
			return this;
		};
	this.OnComplete = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called after AJAX call completes, successfully or not.</para>
			///		<para>The function is executed after re-enabling submit buttons and hidding the progress element.</para>
			/// </summary>
			this.Options.OnComplete = functionName;
			return this;
		};
	this.submit = function()
		{
			///	<summary>
			///		<para>Makes the AJAX call, after populating data to be sent and building .success, .error, .complete handlers for the call settings.</para>
			///	</summary>
            if (that.Options.Before) that.Options.Before();
            if (that.Options.SubmitButtons) that.Options.SubmitButtons.prop("disabled", true);
            if (that.Options.ProgressBar) that.Options.ProgressBar.show();
            that.Options.AjaxCallSettings.success = function (data) {
                if (that.Options.UpdateTarget) {
                    if (that.Options.AjaxCallSettings.dataType === "html") {
                        that.Options.UpdateTarget.html(data);
                    }
                    if (that.Options.AjaxCallSettings.dataType === "json") {
                        for (var prop in data) {
                            if (data.hasOwnProperty(prop)) {
                                that.Options.UpdateTarget.find("[data-bound='" + prop + "']").val(data[prop]);
                            }
                        }
                    }
                }
                if (that.Options.OnSuccess) that.Options.OnSuccess(data);
            };
            that.Options.AjaxCallSettings.error = function () {
                if (that.Options.OnError) that.Options.OnError();
                toastr.error("Operation failed while communicating with the server.", "Error");
            };
            that.Options.AjaxCallSettings.complete = function () {
                if (that.Options.SubmitButtons) that.Options.SubmitButtons.prop("disabled", false);
                if (that.Options.ProgressBar) that.Options.ProgressBar.hide();
                if (that.Options.OnComplete) that.Options.OnComplete();
            };
            that.Options.AjaxCallSettings.data = that.ActionForm.serialize();
            jQuery.ajax(that.Options.AjaxCallSettings);
            if (that.Options.HideModal) that.ActionForm.parents(".modal").modal("hide");
			return this;
		};
	return this;
};



Nurbiosis.AjaxLoadingElement = function(ElementSelector)
{
	///	<summary>
	///		<para>Builds an object able to load the content of an element through AJAX, optionally running some code on success, error or completion.</para>
	///		<para>Parameter: the jQuery/CSS selector of the target element to be filled with the response HTML or data.</para>
	///		<para>Options are passed through chaining methods: Source, Progress, Before, OnSuccess, OnError, OnComplete.</para>
	///		<para>Uses attribute 'data-source' of the target element as URL for getting the content. Also uses 'data-progress' to display a progress bar.</para>
	///	</summary>
	///	<param name="ElementSelector" type="String">jQuery selector of the element to be filled with the response HTML data.</param>
	///	<param name="Source" type="String">URL from where to retrieve data. If missing, attribute 'data-source' of the 'Target' element will be used.</param>
	///	<param name="Progress" type="String">jQuery selector of the progress bar to show during AJAX call. Progress element will be hidden initially.</param>
	///	<param name="Before" type="Function">Operations to do right before sending the AJAX call. Progress element is shown by default.</param>
	///	<param name="OnSuccess" type="Function">Tasks to do, additionally to filling the 'Parameters.Target' element with the HTML result of the call.</param>
	///	<param name="OnError" type="Function">Tasks to do just before alerting the user about the communication failure.</param>
	///	<param name="OnComplete" type="Function">Tasks to to after completion and progress bar was hidden.</param>
	///	<returns type="jQuery">Returns this function object, to allow chaining calls on methods/setters.</returns>
	var that = this;
	this.Options = 
		{
			UpdateTarget: null,
			ProgressBar: null,
			Before: null,
			OnError: null,
			OnSuccess: null,
			OnComplete: null,
			AjaxCallSettings:
				{
					type: "get",
					dataType: "html",
					cache: false
				}
		};
	this.Options.UpdateTarget = jQuery(ElementSelector);
	if (this.Options.UpdateTarget.length !== 1)
	{
		console.error("Nurbiosis.AjaxLoadingElement(): Could not resolve ElementSelector to a DOM element.");
		return null;
	}
	if (this.Options.UpdateTarget.attr("data-source"))
	{
		this.Options.AjaxCallSettings.url = this.Options.UpdateTarget.attr("data-source");
	}
	if (this.Options.UpdateTarget.attr("data-progress"))
	{
		this.Options.ProgressBar = jQuery(this.Options.UpdateTarget.attr("data-progress"));
	}
	this.ResponseType = function(responseType)
		{
			/// <summary>
			///		<para>Changes the expeted response type and behavior. Accepted values are "html" and "json". Html is default.</para>
			///		<para>If HTML, content of the target is updated with the response.</para>
			///		<para>If JSON, input elements having 'data-bind' attributes will have their value updated with corresponding members of received data structure.</para>
			/// </summary>
			this.Options.AjaxCallSettings.dataType = responseType.toLowerCase();
			return this;
		};
	this.Source = function(sourceURL)
		{
			/// <summary>
			///		<para>Sets the source URL where the AJAX call will be sent.</para>
			///		<para>Alternatively, this URL can be specified in the 'data-source' attribute of the target element.</para>
			///		<para>The target element is specified by ElementSelector parameter passed in the constructor.</para>
			/// </summary>
			if (sourceURL)
			{
				that.Options.AjaxCallSettings.url = sourceURL;
			}
			return this;
		};
	this.Progress = function(progressSelector)
		{
			/// <summary>
			///		<para>Sets the progress displaying element that will show up during the AJAX call.</para>
			///		<para>Parameter: the jQuery/CSS selector of the target element.</para>
			/// </summary>
			var progress = jQuery(progressSelector);
			if (progress)
			{
				this.Options.ProgressBar = progress;
			}
			return this;
	};
	this.SendingData = function(dataToSend)
		{
			/// <summary>
			///		<para>Specifies the data to be sent along with the AJAX call.</para>
			///		<para>Sets AjaxCallSettings.data before making the request.</para>
			/// </summary>
			this.Options.AjaxCallSettings.data = dataToSend;
			return this;
		};
	this.SendingMethod = function(httpMethod)
		{
			/// <summary>
			///		<para>Specifies the method for making the AJAX request.</para>
			///		<para>Accepts "get", "post" or other HTTP verbs. Default is "get".</para>
			/// </summary>
			this.Options.AjaxCallSettings.type = httpMethod;
			return this;
		};
	this.Before = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called right after sending the AJAX request.</para>
			///		<para>The function will be most probably executed before the AJAX response is received.</para>
			/// </summary>
			this.Options.Before = functionName;
			return this;
		};
	this.OnError = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon error of the AJAX call.</para>
			///		<para>The function is executed before an error alert is displayed to the user.</para>
			/// </summary>
			this.Options.OnError = functionName;
			return this;
		};
	this.OnSuccess = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon successfully receiving the AJAX call result.</para>
			///		<para>The function is executed after the call result is processed (eg. target element has its HTML updated).</para>
			///		<para>The received data is passed to this function as parameter.</para>
			/// </summary>
			this.Options.OnSuccess = functionName;
			return this;
		};
	this.OnComplete = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called after AJAX call completes, successfully or not.</para>
			///		<para>The function is executed after hidding the progress element.</para>
			/// </summary>
			this.Options.OnComplete = functionName;
			return this;
		};
	this.load = function()
		{
			///	<summary>
			///		<para>Makes the AJAX call, after building .success, .error, .complete handlers for the call settings.</para>
			///		<para>Displays the progress element and populates data to be sent with the AJAX request (specified in this.SendingData()).</para>
			///	</summary>
			if (!this.Options.AjaxCallSettings.url)
			{
				console.error("Nurbiosis.AjaxLoadingElement(): no Source URL was provided and 'data-source' attribute on the target element is missing.");
				return this;
			}
			this.Options.AjaxCallSettings.success = function(data)
				{
					if (that.Options.AjaxCallSettings.dataType === "html")
					{
						that.Options.UpdateTarget.html(data);
					}
					if (that.Options.AjaxCallSettings.dataType === "json")
					{
						for (var prop in data)
						{
							if (data.hasOwnProperty(prop))
							{
								that.Options.UpdateTarget.find("[data-bound='" + prop + "']").val(data[prop]);
							}
						}
					}
					if (that.Options.OnSuccess) that.Options.OnSuccess(data);
				};
			this.Options.AjaxCallSettings.error = function()
				{
					if (that.Options.OnError) that.Options.OnError();
					toastr.error("Operation failed while communicating with the server.<br />Failed to retrieve content from server.", "Error");
				};
			this.Options.AjaxCallSettings.complete = function()
				{
					if (that.Options.ProgressBar) that.Options.ProgressBar.hide();
					if (that.Options.OnComplete) that.Options.OnComplete();
				};
			if (this.Options.ProgressBar) this.Options.ProgressBar.show();
			if (this.Options.Before) this.Options.Before();
			jQuery.ajax(this.Options.AjaxCallSettings);
			return this;
		};
	return this;
};



Nurbiosis.LoadElementByAjax = function(ElementSelector)
{
	///	<summary>
	///		<para>Loads the content of an element through AJAX, optionally running some code on success, error or completion.</para>
	///		<para>Parameter: the jQuery/CSS selector of the target element to be filled with the response HTML data.</para>
	///		<para>Options are passed through chained methods: Source, Progress, Before, OnSuccess, OnError, OnComplete.</para>
	///		<para>Uses attribute 'data-source' of the target element as URL for getting the content. Also uses 'data-progress' to display a progress bar.</para>
	///	</summary>
	/// <param name="ElementSelector" type="String">jQuery selector of the element to be filled with the response HTML data.</param>
	/// <param name="Source" type="String">URL from where to retrieve data. If missing, attribute 'data-source' of the 'Target' element will be used.</param>
	/// <param name="Progress" type="String">jQuery selector of the progress bar to show during AJAX call. Progress element will be hidden initially.</param>
	/// <param name="Before" type="Function">Operations to do right before sending the AJAX call. Progress element is shown by default.</param>
	/// <param name="OnSuccess" type="Function">Tasks to do, additionally to filling the 'Parameters.Target' element with the HTML result of the call.</param>
	/// <param name="OnError" type="Function">Tasks to do just before alerting the user about the communication failure.</param>
	/// <param name="OnComplete" type="Function">Tasks to to after completion and progress bar was hidden.</param>
	/// <returns type="jQuery">Returns this function object, to allow chaining calls on methods/setters.</returns>
	var that = this;
	this.Options =
		{
			UpdateTarget: null,
			ProgressBar: null,
			Before: null,
			OnError: null,
			OnSuccess: null,
			OnComplete: null,
			AjaxCallSettings: 
				{
					type: "get",
					dataType: "html",
					cache: false
				}
		};
	this.Options.UpdateTarget = jQuery(ElementSelector);
	if (this.Options.UpdateTarget.length !== 1)
	{
		console.error("Nurbiosis.LoadElementByAjax(): Could not resolve ElementSelector to a DOM element.");
		return null;
	}
	if (this.Options.UpdateTarget.attr("data-source"))
	{
		this.Options.AjaxCallSettings.url = this.Options.UpdateTarget.attr("data-source");
	}
	if (this.Options.UpdateTarget.attr("data-progress"))
	{
		this.Options.ProgressBar = jQuery(this.Options.UpdateTarget.attr("data-progress"));
	}
	this.ResponseType = function(responseType)
		{
			/// <summary>
			/// <para>Changes the expeted response type and behavior. Accepted values are "html" and "json". Html is default.</para>
			/// <para>If HTML, content of the target is updated with the response.</para>
			/// <para>If JSON, input elements having 'data-bind' attributes will have their value updated with corresponding members of received data structure.</para>
			/// </summary>
			this.Options.AjaxCallSettings.dataType = responseType.toLowerCase();
			return this;
		};
	this.Source = function(sourceURL)
		{
			/// <summary>
			///		<para>Sets the source URL where the AJAX call will be sent.</para>
			///		<para>Alternatively, this URL can be specified in the 'data-source' attribute of the target element.</para>
			///		<para>The target element is specified by ElementSelector parameter passed initially.</para>
			/// </summary>
			if (sourceURL) this.Options.AjaxCallSettings.url = sourceURL;
			return this;
		};
	this.Progress = function(progressSelector)
		{
			/// <summary>
			///		<para>Sets the progress displaying element that will show up during the AJAX call.</para>
			///		<para>Parameter: the jQuery/CSS selector of the target element.</para>
			/// </summary>
			var progress = jQuery(progressSelector);
			if (progress)
			{
				this.Options.ProgressBar = progress;
			}
			return this;
		};
	this.SendingData = function(dataToSend)
		{
			/// <summary>
			///		<para>Specifies the data to be sent along with the AJAX call.</para>
			///		<para>Sets AjaxCallSettings.data before making the request.</para>
			/// </summary>
			this.Options.AjaxCallSettings.data = dataToSend;
			return this;
		};
	this.SendingMethod = function(httpMethod)
		{
			/// <summary>
			///		<para>Specifies the method for making the AJAX request.</para>
			///		<para>Accepts "get", "post" or other HTTP verbs. Default is "get".</para>
			/// </summary>
			this.Options.AjaxCallSettings.type = httpMethod;
			return this;
		};
	this.Before = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called right after sending the AJAX request.</para>
			///		<para>The function will be most probably executed before the AJAX response is received.</para>
			/// </summary>
			this.Options.Before = functionName;
			return this;
		};
	this.OnError = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon error of the AJAX call.</para>
			///		<para>The function is executed before an error alert is displayed to the user.</para>
			/// </summary>
			this.Options.OnError = functionName;
			return this;
		};
	this.OnSuccess = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called upon successfully receiving the AJAX call result.</para>
			///		<para>The function is executed after the call result is processed (eg. target element has its HTML updated).</para>
			///		<para>The received data is passed to this function as parameter.</para>
			/// </summary>
			this.Options.OnSuccess = functionName;
			return this;
		};
	this.OnComplete = function(functionName)
		{
			/// <summary>
			///		<para>Parameter: a function pointer. The function is called after AJAX call completes, successfully or not.</para>
			///		<para>The function is executed after hidding the progress element.</para>
			/// </summary>
			this.Options.OnComplete = functionName;
			return this;
		};
	this.go = function()
		{
			///	<summary>
			///		<para>Makes the AJAX call, after building .success, .error, .complete handlers for the call settings.</para>
			///		<para>Displays the progress element and populates data to be sent with the AJAX request (specified in this.SendingData()).</para>
			///	</summary>
			if (!this.Options.AjaxCallSettings.url)
			{
				console.error("Nurbiosis.LoadElementByAjax(): no Source URL was provided and 'data-source' attribute on the target element is missing.");
				return this;
			}
			this.Options.AjaxCallSettings.success = function(data)
				{
					if (that.Options.AjaxCallSettings.dataType === "html")
					{
						that.Options.UpdateTarget.html(data);
					}
					if (that.Options.AjaxCallSettings.dataType === "json")
					{
						for (var prop in data)
						{
							if (data.hasOwnProperty(prop))
							{
								that.Options.UpdateTarget.find("[data-bound='" + prop + "']").val(data[prop]);
							}
						}
					}
					if (that.Options.OnSuccess) that.Options.OnSuccess(data);
				};
			this.Options.AjaxCallSettings.error = function()
				{
					if (that.Options.OnError) that.Options.OnError();
					toastr.error("Operation failed while communicating with the server.<br />Failed to retrieve content from server.", "Error");
				};
			this.Options.AjaxCallSettings.complete = function()
				{
					if (that.Options.ProgressBar) that.Options.ProgressBar.hide();
					if (that.Options.OnComplete) that.Options.OnComplete();
				};
			this.Options.AjaxCallSettings.data = this.Options.SendingData;
			if (this.Options.ProgressBar) this.Options.ProgressBar.show();
			if (this.Options.Before) this.Options.Before();
			jQuery.ajax(this.Options.AjaxCallSettings);
			return this;
		};
	return this;
};



Nurbiosis.CopyTextToClipboard = function(text) {
	/*
	https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript#30810322
	https://codepen.io/DeanMarkTaylor/pen/RMRaJX?editors=1011
	*/
	function fallbackCopyTextToClipboard(text) {
		let textArea = document.createElement("textarea");
		textArea.value = text;
		document.body.appendChild(textArea);
		textArea.focus();
		textArea.select();
		try {
			let successful = document.execCommand("copy");
			//let msg = successful ? "successful" : "unsuccessful";
			//console.log("Nurbiosis.CopyTextToClipboard fallback - Copying text to clipboard was " + msg);
		}
		catch (err) {
			console.log("Nurbiosis.CopyTextToClipboard fallback - Oops, unable to copy to clipboard: ", err);
			console.log(text);
		}
		document.body.removeChild(textArea);
	}
	function asyncCopyToClipboardSuccess() {
		//console.log("Nurbiosis.CopyTextToClipboard async - Copying to clipboard was successful");
		//console.log(text);
	}
	function asyncCopyToClipboardError(err) {
		//console.log("Nurbiosis.CopyTextToClipboard async - Could not copy text to clipboard: ", err);
		//console.log(text);
	}
	if (navigator.clipboard) {
		navigator.clipboard.writeText(text).then(asyncCopyToClipboardSuccess, asyncCopyToClipboardError);
	}
	else {
		fallbackCopyTextToClipboard(text);
		return;
	}
};


/*
toastr.options = {
	"closeButton": true,
	"debug": false,
	"newestOnTop": true,
	"progressBar": true,
	"positionClass": "toast-top-right",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "1000",
	"hideDuration": "2000",
	"timeOut": "10000",
	"extendedTimeOut": "5000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};
*/


function Layout_Start() {
	Nurbiosis.ColorTheme.Startup();
	Nurbiosis.SideBar.Startup();
}
window.addEventListener("DOMContentLoaded", Layout_Start);