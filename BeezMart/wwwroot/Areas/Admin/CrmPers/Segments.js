const MyDataTable = {
    RecordSet: [
        { name: "Segment 1", comments: "Description for first segment", persCount: 256 },
        { name: "Segment 2", comments: "Description for second segment", persCount: 128 },
        { name: "Segment 3", comments: "Description for third segment", persCount: 512 }
    ],
    CurrentRecord: {
        id: 65535,
        name: "Segment",
        comments: "Comments about this segment"
    },
    AxiosOptions: {
        headers: {
            "Content-Type": "application/json",
            "RequestVerificationToken": "Populate at runtime"
        }
    },
    InProgress: false,
    Messaging: {
        title: null,
        message: null,
        display() { 
            const toastElement = document.getElementById("MessagingToast");
            const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastElement);
            toastBootstrap.show();
        }
    },

    init() {
        this.InProgress = true;
        axios.interceptors.response.use(
            response => response,
            error => {
                if (error.response && error.response.data) {
                    this.Messaging.title = error.response.data.title;
                    this.Messaging.message = error.response.data.detail;
                    this.Messaging.display();
                }
                return Promise.reject(error);
            }
        );
        axios.get("/Admin/CrmPers/_Segments")
            .then((response) => {
                this.RecordSet = response.data;
                this.InProgress = false;
            });
        this.AxiosOptions.headers.RequestVerificationToken = document.querySelector("input[name='__RequestVerificationToken']").value;
    },
    editRecord(rowIndex) {
        record = this.RecordSet[rowIndex];
        this.CurrentRecord.id = record.id;
        this.CurrentRecord.name = record.name;
        this.CurrentRecord.comments = record.comments;
        jQuery("#ModalEditor").modal("show");
    },
    addNewRecord() {
        this.CurrentRecord.id = null;
        this.CurrentRecord.name = null;
        this.CurrentRecord.comments = null;
        jQuery("#ModalEditor").modal("show");
    },
    saveRecord() {
        this.InProgress = true;
        axios.post("/Admin/CrmPers/_Segments", this.CurrentRecord, this.AxiosOptions)
            .then((response) => {
                this.RecordSet = response.data;
            });
        jQuery("#ModalEditor").modal("hide");
        this.InProgress = false;
    },
    dropRecord(rowIndex) {
        if (!window.confirm("Confirm record deletion?")) return;
        this.InProgress = true;
        record = this.RecordSet[rowIndex];
        axios.delete(`/Admin/CrmPers/_Segments/${record.id}`, this.AxiosOptions)
            .then((response) => {
                this.RecordSet = response.data;
            });
        jQuery("#ModalEditor").modal("hide");
        this.InProgress = false;
    }
};



function Page_Start() {
    const myModal = document.getElementById("ModalEditor");
    const myInput = document.querySelector("#ModalEditor input[type='text']");
    myModal.addEventListener("shown.bs.modal", () => { myInput.focus(); });
}

jQuery(Page_Start);