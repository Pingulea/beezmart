const MyDataTable = {
    RecordSet: [
        { name: "Flag 1", category: "The category", comments: "Description for first flag", isUnique: true, isRetired: false },
        { name: "Flag 2", category: "The category", comments: "Description for second flag", isUnique: true, isRetired: false },
        { name: "Flag 3", category: "The category", comments: "Description for third flag", isUnique: true, isRetired: false }
    ],
    CurrentRecord: {
        id: 65535,
        name: "Flag",
        category: "Category",
        comments: "Comments about this flag",
        isUnique: true,
        isRetired: false,
        fieldType: undefined
    },
    AxiosOptions: {
        headers: {
            "Content-Type": "application/json",
            "RequestVerificationToken": "Populate at runtime"
        }
    },
    InProgress: false,
    Messaging: {
        title: null,
        message: null,
        display() { 
            const toastElement = document.getElementById("MessagingToast");
            const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastElement);
            toastBootstrap.show();
        }
    },

    init() {
        this.InProgress = true;
        this.CurrentRecord.fieldType = Number(document.querySelector("input[name='FieldType']").value);
        axios.interceptors.response.use(
            response => response,
            error => {
                if (error.response && error.response.data) {
                    this.Messaging.title = error.response.data.title;
                    this.Messaging.message = error.response.data.detail;
                    this.Messaging.display();
                }
                return Promise.reject(error);
            }
        );
        axios.get("/Admin/CrmOrgs/_Flags")
            .then((response) => {
                this.RecordSet = response.data;
                this.InProgress = false;
            });
        this.AxiosOptions.headers.RequestVerificationToken = document.querySelector("input[name='__RequestVerificationToken']").value;
    },
    editRecord(rowIndex) {
        record = this.RecordSet[rowIndex];
        this.CurrentRecord.id = record.id;
        this.CurrentRecord.name = record.name;
        this.CurrentRecord.category = record.category;
        this.CurrentRecord.comments = record.comments;
        this.CurrentRecord.isUnique = record.isUnique;
        this.CurrentRecord.isRetired = record.isRetired;
        jQuery("#ModalEditor").modal("show");
    },
    addNewRecord() {
        this.CurrentRecord.id = null;
        this.CurrentRecord.name = null;
        this.CurrentRecord.category = null;
        this.CurrentRecord.comments = null;
        this.CurrentRecord.isUnique = true;
        this.CurrentRecord.isRetired = false;
        jQuery("#ModalEditor").modal("show");
    },
    saveRecord() {
        this.InProgress = true;
        axios.post("/Admin/CrmOrgs/_Flags", this.CurrentRecord, this.AxiosOptions)
            .then((response) => {
                this.RecordSet = response.data;
            });
        jQuery("#ModalEditor").modal("hide");
        this.InProgress = false;
    }
};



function Page_Start() {
    const myModal = document.getElementById("ModalEditor");
    const myInput = document.querySelector("#ModalEditor input[type='text']");
    myModal.addEventListener("shown.bs.modal", () => { myInput.focus(); });
}

jQuery(Page_Start);