﻿


var MyPage = {
	Explanations: null,
	Explanations_Title: null,
	Explanations_Body: null,
	InitializeFormGroup: function(formGroup){
		let settingName = formGroup.find("label").text();
		let settingField = formGroup.find("input:not(:disabled), select, textarea");
		let btnExplain = formGroup.find("button[data-explain]");
		let btnDefault = formGroup.find("button[data-default]");
		let settingExplanations = btnExplain.attr("data-explain");
		let settingDefaultValue = btnDefault.attr("data-default");
		if (!settingField.val()) { settingField.val(settingDefaultValue); }
		btnExplain.click( function() {
				MyPage.Explanations_Title.text(settingName);
				MyPage.Explanations_Body.html(settingExplanations);
				MyPage.Explanations.modal("show");
			}
		);
		btnDefault.click( function() {
				settingField.val(settingDefaultValue);
			}
		);
	}
};



function Page_Start()
{
	MyPage.Explanations = jQuery("#ModalExplanations");
	MyPage.Explanations_Title = MyPage.Explanations.find(".modal-title");
	MyPage.Explanations_Body = MyPage.Explanations.find(".modal-body");
	jQuery(".form-group").each(function() { MyPage.InitializeFormGroup(jQuery(this)); } );
}



jQuery(Page_Start);