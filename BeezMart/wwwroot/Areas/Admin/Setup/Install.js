﻿


var MyPage = {
    FilesList: null,
};



function btnStart_Click()
{
	function ShowProcess() {
			jQuery("#TsqlFiles").slideDown(600, ProcessNextScript);
		};
	jQuery("#Intro").slideUp(600, ShowProcess);
}



function ProcessNextScript()
{
	MyPage.FilesList.eq(MyPage.CurrentFileIndex).show();
	MyPage.ScriptFileNameCtl.val(MyPage.FilesList.eq(MyPage.CurrentFileIndex).text());
	MyPage.AjaxCallSettings.data = MyPage.ScriptForm.serialize();
	jQuery.ajax(MyPage.AjaxCallSettings);
}
function ScriptFileProcessed()
{
	MyPage.FilesList.eq(MyPage.CurrentFileIndex).hide();
	MyPage.CurrentFileIndex++;
	if (MyPage.CurrentFileIndex < MyPage.FilesList.length)
	{
		let progressPercentage = MyPage.CurrentFileIndex / MyPage.FilesList.length * 100;
		MyPage.progressBar.css("width", progressPercentage + "%");
		MyPage.progressLabel.text(progressPercentage.toFixed(1).toString() + "%");
		ProcessNextScript();
	}
	else
	{
		MyPage.progressBar.css("width", "100%");
		MyPage.progressLabel.text("100%");
		MyPage.progressBar.removeClass("active");
		MyPage.progressBar.removeClass("progress-bar-striped");
        MyPage.progressBar.removeClass("progress-bar-animated");
        MyPage.progressBar.removeClass("bg-primary");
        MyPage.progressBar.addClass("bg-success");
        jQuery("#btnStart").addClass("d-none");
        jQuery("#btnNext").removeClass("d-none").addClass("d-inline-block");
		jQuery("#TsqlFiles h4").text("Database initialization completed");
	}
}



function Page_Start()
{
	MyPage.FilesList = jQuery("#TsqlFiles p");
	MyPage.progressBar = jQuery("#ProgressIndicator .progress-bar");
	MyPage.progressLabel = jQuery("#ProgressIndicator .progress-bar span");
	MyPage.CurrentFileIndex = 0;
	MyPage.ScriptForm = jQuery("#frmScript");
	MyPage.ScriptFileNameCtl = jQuery("#frmScript input[name='ScriptFileName']");
	MyPage.AjaxCallSettings = {
			url: MyPage.ScriptForm.attr("action"),
			type: "post",
			cache: false,
			success: ScriptFileProcessed,
			error: function() {
				let errorMsg = "Some error occured while processing the following file:<br />";
				errorMsg = errorMsg + "<i>" + MyPage.FilesList.eq(MyPage.CurrentFileIndex).text() + "</i>";
				toastr.error(errorMsg, "Error");
				}
		};
}



jQuery(Page_Start);