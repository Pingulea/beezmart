﻿


var MyPage = {
    ItemViewURL_User: null,         // document.getElementById("hdnViewLink_User").value
    ItemViewURL_Role: null,         // document.getElementById("hdnViewLink_Role").value
};



function PutHrefToLinks_Users(item)
{
	let itemLink = item.find("td:nth-child(1) a");
	let recordID = item.find("td:last-child").text();
	itemLink.attr("href", MyPage.ItemViewURL_User + recordID);
}
function PutHrefToLinks_Roles(item)
{
	let itemLink = item;
	let recordID = itemLink.attr("href");
	itemLink.attr("href", MyPage.ItemViewURL_Role + recordID);
}



function Page_Start()
{
	if (document.getElementById("hdnViewLink_User"))
	{
		MyPage.ItemViewURL_User = document.getElementById("hdnViewLink_User").value;
		MyPage.ItemViewURL_User = MyPage.ItemViewURL_User.substr(0, MyPage.ItemViewURL_User.indexOf("=") + 1);
		jQuery("table.table tbody tr").each(function() { PutHrefToLinks_Users(jQuery(this)); });
	}
	if (document.getElementById("hdnViewLink_Role"))
	{
		MyPage.ItemViewURL_Role = document.getElementById("hdnViewLink_Role").value;
		MyPage.ItemViewURL_Role = MyPage.ItemViewURL_Role.substr(0, MyPage.ItemViewURL_Role.indexOf("=") + 1);
        jQuery("#User-Roles a").each(function() { PutHrefToLinks_Roles(jQuery(this)); });
	}
}



jQuery(Page_Start);