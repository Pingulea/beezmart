﻿


var MyPage = {
    btnSuggestedPassword: null,
    txtNewPassword: null,
    txtPasswordConfirm: null,
    btnSuggestedPassword_Click: function () {
        let newPassword = MyPage.btnSuggestedPassword.text();
        MyPage.txtNewPassword.val(newPassword);
        MyPage.txtPasswordConfirm.val(newPassword);
    },
    frmRoleChange: null,
    frmRoleChange_RoleNameToRemove: null,
    frmRoleChange_RoleNameToAdd: null,
    frmResetPassword: null,
};



function AccountRoles_Loaded(data)
{
    let roleSwitches = jQuery("#Account-Roles input[name='Roles']");
    roleSwitches.prop("checked", false);
    if (data) {
        for (let i = 0; i < data.length; i++) {
            jQuery("#Account-Roles input[value='" + data[i] + "']").prop("checked", true);
        }
    }
    roleSwitches.change(function () { AccountRole_Switch(jQuery(this)); });
}
function AccountRole_Switch(checkbox) {
    let checkboxState = checkbox.prop("checked");
    let roleName = checkbox.val();
    MyPage.frmRoleChange_RoleNameToRemove.val("");
    MyPage.frmRoleChange_RoleNameToAdd.val("");
    if (checkboxState == true) MyPage.frmRoleChange_RoleNameToAdd.val(roleName);
    else MyPage.frmRoleChange_RoleNameToRemove.val(roleName);
    MyPage.frmRoleChange.submit();
}



function Page_Start()
{
    MyPage.btnSuggestedPassword = jQuery("#btnSuggestedPassword");
    MyPage.txtNewPassword = jQuery("#ModalResetPassword input[name='NewPassword']");
    MyPage.txtPasswordConfirm = jQuery("#ModalResetPassword input[name='PasswordConfirm']");
    MyPage.btnSuggestedPassword.click(MyPage.btnSuggestedPassword_Click);
    Nurbiosis.LoadElementByAjax("#Account-Roles").ResponseType("JSON").Progress("#Account-Roles progress").OnSuccess(AccountRoles_Loaded).go();
    MyPage.frmRoleChange = jQuery("#frmRoleChange");
    MyPage.frmRoleChange_RoleNameToRemove = jQuery("#frmRoleChange input[name='RoleNameToRemove']");
    MyPage.frmRoleChange_RoleNameToAdd = jQuery("#frmRoleChange input[name='RoleNameToAdd']");
    MyPage.frmResetPassword = jQuery("#frmResetPassword");
    Nurbiosis.AjaxifyForm(MyPage.frmRoleChange).Progress("#Account-Roles progress");
    Nurbiosis.AjaxifyForm(MyPage.frmResetPassword).Target("#ResetPasswordResultMessage").Progress("#User-Account progress");
}



jQuery(Page_Start);