﻿


var MyPage = {
    OptionalFields: null,       // jQuery("#Optional-Fields")
};



function ShowOptionalFields(inputCtl)
{
    if (inputCtl.val()) {
        MyPage.OptionalFields.removeClass("collapse");
    }
}



function Page_Start()
{
    MyPage.OptionalFields = jQuery("#Optional-Fields");
    MyPage.OptionalFields.find("input").each(function () { ShowOptionalFields(jQuery(this)); });
}



jQuery(Page_Start);