﻿var MyPageDOM = {};



function PutHrefToLinksUsers(tr)
{
	
	var itemLink = tr.find("td:nth-child(1) a");
	var recordID = tr.find("td:last-child").text();
	itemLink.attr("href", MyPageDOM.ItemViewURL + recordID);
}



function Page_Start()
{
	if (document.getElementById("hdnViewLinkUser"))
	{
		MyPageDOM.ItemViewURL = document.getElementById("hdnViewLinkUser").value;
		MyPageDOM.ItemViewURL = MyPageDOM.ItemViewURL.substr(0, MyPageDOM.ItemViewURL.indexOf("=") + 1);
		jQuery("table.datagrid tbody tr").each(function() { PutHrefToLinksUsers(jQuery(this)); });
	}
}



jQuery(Page_Start);