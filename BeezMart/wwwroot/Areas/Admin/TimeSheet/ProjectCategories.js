﻿const MyDataTable = {
    RecordSet: [
        { name: "Infrastructure", subCategory: "Networking", isDiscontinued: false },
        { name: "Infrastructure", subCategory: "Data Center", isDiscontinued: false },
        { name: "Infrastructure", subCategory: "Software", isDiscontinued: true }
    ],
    CurrentRecord: {
        id: 65535,
        name: "Development",
        subCategory: "Web Sites",
        isDiscontinued: false
    },
    AxiosOptions: {
        headers: {
            "Content-Type": "application/json",
            "RequestVerificationToken": "Populate at runtime"
        }
    },
    InProgress: false,
    Messaging: {
        title: null,
        message: null,
        display() { 
            const toastElement = document.getElementById("MessagingToast");
            const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastElement);
            toastBootstrap.show();
        }
    },

    init() {
        this.InProgress = true;
        axios.interceptors.response.use(
            response => response,
            error => {
                if (error.response && error.response.data) {
                    this.Messaging.title = error.response.data.title;
                    this.Messaging.message = error.response.data.detail;
                    this.Messaging.display();
                }
                return Promise.reject(error);
            }
        );
        axios.get("/Admin/TimeSheet/_ProjectCategories")
            .then((response) => {
                this.RecordSet = response.data;
                this.InProgress = false;
            });
        this.AxiosOptions.headers.RequestVerificationToken = document.querySelector("input[name='__RequestVerificationToken']").value;
    },
    editRecord(rowIndex) {
        record = this.RecordSet[rowIndex];
        this.CurrentRecord.id = record.id;
        this.CurrentRecord.name = record.name;
        this.CurrentRecord.subCategory = record.subCategory;
        this.CurrentRecord.isDiscontinued = record.isDiscontinued;
        jQuery("#ModalEditor").modal("show");
    },
    addNewRecord() {
        this.CurrentRecord.id = null;
        this.CurrentRecord.name = null;
        this.CurrentRecord.subCategory = null;
        this.CurrentRecord.isDiscontinued = false;
        jQuery("#ModalEditor").modal("show");
    },
    saveRecord() {
        this.InProgress = true;
        axios.post("/Admin/TimeSheet/_ProjectCategories", this.CurrentRecord, this.AxiosOptions)
            .then((response) => {
                this.RecordSet = response.data;
            });
        jQuery("#ModalEditor").modal("hide");
        this.InProgress = false;
    },
    dropRecord(rowIndex) {
        if (!window.confirm("Confirm record deletion?")) return;
        this.InProgress = true;
        record = this.RecordSet[rowIndex];
        axios.delete(`/Admin/TimeSheet/_ProjectCategories/${record.id}`, this.AxiosOptions)
            .then((response) => {
                this.RecordSet = response.data;
            });
        jQuery("#ModalEditor").modal("hide");
        this.InProgress = false;
    }
};



function Page_Start() {
    const myModal = document.getElementById("ModalEditor");
    const myInput = document.querySelector("#ModalEditor input[type='text']");
    myModal.addEventListener("shown.bs.modal", () => { myInput.focus(); });
}

jQuery(Page_Start);