


var MyPage = {};



function DocumentsList_Updated()
{
    function PutHrefToLinks(tr)
    {
        let DocumentLink = tr.find("td:nth-child(1) a");
	    DocumentLink.attr("href", MyPage.DocumentViewUrl + tr.attr("data-id"));
    }
    let tableRows = jQuery("#Documents tbody tr")
    tableRows.each( function() { PutHrefToLinks(jQuery(this)); } );
}



function Page_Start()
{
	MyPage.DocumentViewUrl = jQuery("#hdnDocumentViewUrl").val() + "?ID=";
	DocumentsList_Updated();
}



jQuery(Page_Start);