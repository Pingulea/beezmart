﻿


var MyPage = {
	CrmOrgsViewUrl: null,
	CrmPersViewUrl: null,
	BillingInvoicesViewUrl: null,
	DOM: {
		AdvancedSearch: null,		// jQuery("#AdvancedSearch")
	}
};



function ToggleSelectAllCheckboxes()
{
	jQuery("table thead tr th input[type='checkbox']").change(
			function()
			{
				var triggeringControl = jQuery(this);
				var depedantCheckboxes = triggeringControl.parents("table").eq(0).find("tbody input[type='checkbox']");
				depedantCheckboxes.prop("checked", triggeringControl.prop("checked"));
			}
		);
}

function btnChangeFilter_Click()
{
	jQuery("#SearchResults .alert").alert("close");
	jQuery("#ChangeFilter").slideUp(300, function() { jQuery("#SearchForm").slideDown(600); });
}
function SearchResults_Loaded()
{
	function PutHrefToLinks(tr)
	{
		let checkboxCtl = tr.find("td:nth-child(1) input");
		checkboxCtl.val(tr.attr("data-id"));
		let InvoiceLink = tr.find("td:nth-child(2) a");
		InvoiceLink.attr("href", MyPage.BillingInvoicesViewUrl + tr.attr("data-id"));
		InvoiceLink.attr("target", "_blank");
		let statusCell = tr.find("td:nth-child(3)");
		let statusName = statusCell.attr("class");
		statusCell.attr("title", statusName);
		switch (statusName)
		{
			case "Draft":
				statusCell.html("<i class='bi-list-task text-primary'></i>");
				break;
			case "Validated":
				statusCell.html("<i class='bi-list-task text-info'></i>");
				break;
			case "SentToCustomer":
				statusCell.html("<i class='bi-list-task text-warning'></i>");
				break;
			case "Cancelled":
				statusCell.html("<i class='bi-list-task text-success'></i>");
				break;
			case "Closed":
				statusCell.html("<i class='bi-list-task text-cancelled'></i>");
				break;
			default:
				statusCell.html("<i class='bi-list-task text-danger'></i>");
				break;
		}
		let CrmLink = tr.find("td:nth-child(4) a");
		if (CrmLink.length > 0)
		{
			let OrgsID = CrmLink.attr("data-orgs");
			if (OrgsID) CrmLink.attr("href", MyPage.CrmOrgsViewUrl + OrgsID);
			let PersID = CrmLink.attr("data-pers");
			if (PersID) CrmLink.attr("href", MyPage.CrmPersViewUrl + PersID);
		}
		CrmLink.attr("target", "_blank");
	}
	jQuery("#SearchResults tbody tr").each(function() { PutHrefToLinks(jQuery(this)); });
	jQuery("#SearchForm").slideUp(600, function() { jQuery("#ChangeFilter").slideDown(300); });
}



function Page_Start()
{
	MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	MyPage.BillingInvoicesViewUrl = jQuery("#hdnBillingInvoicesViewUrl").val() + "?InvoiceID=";
	MyPage.DOM.AdvancedSearch = jQuery("#AdvancedSearch");
	jQuery("#btnAdvancedSearch a").click( function() { MyPage.DOM.AdvancedSearch.slideToggle(); } );
	Nurbiosis.AjaxifyForm("#SearchForm form").Target("#SearchResults").Progress("progress").OnSuccess(SearchResults_Loaded);
	ToggleSelectAllCheckboxes();
}



jQuery(Page_Start);