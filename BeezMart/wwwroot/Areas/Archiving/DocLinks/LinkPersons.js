﻿


var MyPage = {
	CrmPersViewUrl: null,
};



function ToggleSelectAllCheckboxes()
{
	jQuery("table thead tr th input[type='checkbox']").change(
			function()
			{
				let triggeringControl = jQuery(this);
				let depedantCheckboxes = triggeringControl.parents("table").eq(0).find("tbody input[type='checkbox']");
				depedantCheckboxes.prop("checked", triggeringControl.prop("checked"));
			}
		);
}

function btnChangeFilter_Click()
{
	jQuery("#SearchResults .alert").alert("close");
	jQuery("#ChangeFilter").slideUp(300, function() { jQuery("#SearchForm").slideDown(600); });
}
function SearchResults_Loaded()
{
	function PutHrefToLinks(tr)
	{
		let checkboxCtl = tr.find("td:nth-child(1) input");
		checkboxCtl.val(tr.attr("data-id"));
		let CrmLink = tr.find("td:nth-child(2) a");
		CrmLink.attr("href", MyPage.CrmPersViewUrl + tr.attr("data-id"));
		CrmLink.attr("target", "_blank");
	}
	jQuery("#SearchResults tbody tr").each(function() { PutHrefToLinks(jQuery(this)); });
	jQuery("#SearchForm").slideUp(600, function() { jQuery("#ChangeFilter").slideDown(300); });
}



function Page_Start()
{
	MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	Nurbiosis.AjaxifyForm("#SearchForm form").Target("#SearchResults").Progress("progress").OnSuccess(SearchResults_Loaded);
	ToggleSelectAllCheckboxes();
}



jQuery(Page_Start);