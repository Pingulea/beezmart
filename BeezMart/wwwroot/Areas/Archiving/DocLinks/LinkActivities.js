﻿


let MyPage = {
	CrmOrgsViewUrl: null,
	CrmPersViewUrl: null,
    TimeSheetActivitiesViewUrl: null,
    DOM: {
        AdvancedSearch: null,		// jQuery("#AdvancedSearch")
    }
};



function ToggleSelectAllCheckboxes()
{
	jQuery("table thead tr th input[type='checkbox']").change(
			function()
			{
				var triggeringControl = jQuery(this);
				var depedantCheckboxes = triggeringControl.parents("table").eq(0).find("tbody input[type='checkbox']");
				depedantCheckboxes.prop("checked", triggeringControl.prop("checked"));
			}
		);
}

function btnChangeFilter_Click()
{
	jQuery("#SearchResults .alert").alert("close");
	jQuery("#ChangeFilter").slideUp(300, function() { jQuery("#SearchForm").slideDown(600); });
}
function SearchResults_Loaded()
{
	function PutHrefToLinks(tr)
	{
		let checkboxCtl = tr.find("td:nth-child(1) input");
		checkboxCtl.val(tr.attr("data-id"));
        let ActivityLink = tr.find("td:nth-child(2) a");
        ActivityLink.attr("href", MyPage.TimeSheetActivitiesViewUrl + tr.attr("data-id"));
        ActivityLink.attr("target", "_blank");
        let CrmLink = tr.find("td:nth-child(4) a");
        if (CrmLink.length > 0) {
            let OrgsID = CrmLink.attr("data-orgs");
            if (OrgsID) CrmLink.attr("href", MyPage.CrmOrgsViewUrl + OrgsID);
            let PersID = CrmLink.attr("data-pers");
            if (PersID) CrmLink.attr("href", MyPage.CrmPersViewUrl + PersID);
        }
        CrmLink.attr("target", "_blank");
	}
	jQuery("#SearchResults tbody tr").each(function() { PutHrefToLinks(jQuery(this)); });
	jQuery("#SearchForm").slideUp(600, function() { jQuery("#ChangeFilter").slideDown(300); });
}



function Page_Start()
{
    MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
    MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
    MyPage.TimeSheetActivitiesViewUrl = jQuery("#hdnTimeSheetActivitiesViewUrl").val() + "?ActivityID=";
    MyPage.DOM.AdvancedSearch = jQuery("#AdvancedSearch");
    jQuery("#btnAdvancedSearch a").click(function () { MyPage.DOM.AdvancedSearch.slideToggle(); });
	Nurbiosis.AjaxifyForm("#SearchForm form").Target("#SearchResults").Progress("progress").OnSuccess(SearchResults_Loaded);
	ToggleSelectAllCheckboxes();
}



jQuery(Page_Start);