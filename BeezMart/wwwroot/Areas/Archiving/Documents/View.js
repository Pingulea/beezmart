


var MyPage = {
    DownloadFileUrl: null,
    ViewOrganizationUrl: null,
    ViewPersonUrl: null,
    ViewInvoiceUrl: null,
    ViewProjectUrl: null,
    ViewActivityUrl: null,
    FilesList: null,
    ListOfOrganizations: null,
    ListOfPersons: null,
    ListOfInvoices: null,
    ListOfProjects: null,
    ListOfActivities: null,
    Forms: {
        FileDelete: null,
        FileRename: null,
        FileUploadBrowse: null,
        FileUploadDropzone: null,
        DeleteOrganization: null,
        DeletePerson: null,
        DeleteInvoice: null,
        DeleteProject: null,
        DeleteActivity: null
    },
    DropZone: null,
    DOM: {
        OldFileName: null,
        NewFileName: null,
        DeleteFileName: null,
        ModalRenameFile: null,
        FilesBrowse: null
    },
    Events: {
        FilesList_Loaded: function () {
            function PutHrefToLinks(tr) {
                let fileID = tr.attr("data-id");
                let fileName = tr.find("td:nth-child(2) a").text();
                let downloadFileLink = tr.find("td:nth-child(2) a");
                let renameFileLink = tr.find("td:nth-child(3) a:nth-child(1)");
                let deleteFileLink = tr.find("td:nth-child(3) a:nth-child(2)");
                downloadFileLink.attr("href", MyPage.DownloadFileUrl + fileID);
                renameFileLink.click(
                    function () {
                        MyPage.DOM.OldFileName.val(fileName);
                        MyPage.DOM.NewFileName.val(fileName);
                        MyPage.DOM.ModalRenameFile.modal("show");
                    }
                );
                deleteFileLink.click(
                    function () {
                        if (!window.confirm("Are you sure you want to delete the file?\n\nThere is no undo for this operation...")) return;
                        MyPage.DOM.DeleteFileName.val(fileName);
                        MyPage.Forms.FileDelete.trigger("submit");
                    }
                );
            }
            let tableRows = jQuery("#FilesList tbody tr");
            tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
        },
        ListOfOrganizations_Loaded: function() {
            function DeleteRecord(recId) {
                MyPage.Forms.DeleteOrganization.find("input[name='OrganizationID']").val(recId);
                MyPage.Forms.DeleteOrganization.trigger("submit");
            }
            function PutHrefToLinks(tr) {
                let recordID = tr.attr("data-id");
                let entityLink = tr.find("td:nth-child(1) a");
                entityLink.attr("href", MyPage.ViewOrganizationUrl + recordID);
                entityLink.attr("target", "_blank");
                entityLink = tr.find("td:last-child a");
                entityLink.click(function () { DeleteRecord(recordID); });
            }
            let tableRows = jQuery("#ListOfOrganizations tbody tr");
            tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
        },
        ListOfPersons_Loaded: function() {
            function DeleteRecord(recId) {
                MyPage.Forms.DeletePerson.find("input[name='PersonID']").val(recId);
                MyPage.Forms.DeletePerson.trigger("submit");
            }
            function PutHrefToLinks(tr) {
                let recordID = tr.attr("data-id");
                let entityLink = tr.find("td:nth-child(1) a");
                entityLink.attr("href", MyPage.ViewPersonUrl + recordID);
                entityLink.attr("target", "_blank");
                entityLink = tr.find("td:last-child a");
                entityLink.click(function () { DeleteRecord(recordID); });
            }
            let tableRows = jQuery("#ListOfPersons tbody tr");
            tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
        },
        ListOfInvoices_Loaded: function() {
            function DeleteRecord(recId) {
                MyPage.Forms.DeleteInvoice.find("input[name='InvoiceID']").val(recId);
                MyPage.Forms.DeleteInvoice.trigger("submit");
            }
            function PutHrefToLinks(tr) {
                let recordID = tr.attr("data-id");
                let entityLink = tr.find("td:nth-child(1) a");
                entityLink.attr("href", MyPage.ViewInvoiceUrl + recordID);
                entityLink.attr("target", "_blank");
                entityLink = tr.find("td:last-child a");
                entityLink.click(function () { DeleteRecord(recordID); });
            }
            let tableRows = jQuery("#ListOfInvoices tbody tr");
            tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
        },
        ListOfProjects_Loaded: function() {
            function DeleteRecord(recId) {
                MyPage.Forms.DeleteProject.find("input[name='ProjectID']").val(recId);
                MyPage.Forms.DeleteProject.trigger("submit");
            }
            function PutHrefToLinks(tr) {
                let recordID = tr.attr("data-id");
                let entityLink = tr.find("td:nth-child(1) a");
                entityLink.attr("href", MyPage.ViewProjectUrl + recordID);
                entityLink.attr("target", "_blank");
                entityLink = tr.find("td:last-child a");
                entityLink.click(function () { DeleteRecord(recordID); });
            }
            let tableRows = jQuery("#ListOfProjects tbody tr");
            tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
        },
        ListOfActivities_Loaded: function() {
            function DeleteRecord(recId) {
                MyPage.Forms.DeleteActivity.find("input[name='ActivityID']").val(recId);
                MyPage.Forms.DeleteActivity.trigger("submit");
            }
            function PutHrefToLinks(tr) {
                let recordID = tr.attr("data-id");
                let entityLink = tr.find("td:nth-child(1) a");
                entityLink.attr("href", MyPage.ViewActivityUrl + recordID);
                entityLink.attr("target", "_blank");
                entityLink = tr.find("td:last-child a");
                entityLink.click(function () { DeleteRecord(recordID); });
            }
            let tableRows = jQuery("#ListOfActivities tbody tr");
            tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
        },
        btnRename_Click: function () {
            let newFileName = MyPage.DOM.NewFileName.val();
            if (newFileName.length === 0) {
                toastr.info("Please type a new name for the file to continue.", "Input needed");
                return;
            }
            MyPage.Forms.FileRename.trigger("submit");
        },
        inputFilesBrowse_Changed: function () {
            let fileNames = "";
            for (let i = 0; i < MyPage.DOM.FilesBrowse.get(0).files.length; i++)
            {
                fileNames += MyPage.DOM.FilesBrowse.get(0).files[i].name;
                fileNames += "<br />";
            }
            MyPage.Forms.FileUploadBrowse.find("p#SelectedFiles").html(fileNames);
        }
    },
    Do: {
        Initialize_DropZone: function () {
            let DropZoneOptions = {
                maxFilesize: 50,            // MB
                chunking: true,
                chunkSize: 1024 * 64,       // in bytes
                createImageThumbnails: false,
                filesizeBase: 1024,
                clickable: true,
                autoProcessQueue: true,
                autoQueue: true
            };
            MyPage.DropZone = new Dropzone("#frmFileUploadDropzone", DropZoneOptions);
            //jQuery("#frmFileUploadDropzone").show();
        }
    }
};



function Page_Start() {
    MyPage.DownloadFileUrl = jQuery("#hdnDownloadFileUrl").val() + "&File=";
    MyPage.ViewOrganizationUrl = jQuery("#hdnViewOrganizationUrl").val();
    MyPage.ViewPersonUrl = jQuery("#hdnViewPersonUrl").val();
    MyPage.ViewInvoiceUrl = jQuery("#hdnViewInvoiceUrl").val();
    MyPage.ViewProjectUrl = jQuery("#hdnViewProjectUrl").val();
    MyPage.ViewActivityUrl = jQuery("#hdnViewActivityUrl").val();
    let mockID = "populate-at-runtime";
    MyPage.ViewOrganizationUrl = MyPage.ViewOrganizationUrl.substr(0, MyPage.ViewOrganizationUrl.indexOf(mockID));
    MyPage.ViewPersonUrl = MyPage.ViewPersonUrl.substr(0, MyPage.ViewPersonUrl.indexOf(mockID));
    MyPage.ViewInvoiceUrl = MyPage.ViewInvoiceUrl.substr(0, MyPage.ViewInvoiceUrl.indexOf(mockID));
    MyPage.ViewProjectUrl = MyPage.ViewProjectUrl.substr(0, MyPage.ViewProjectUrl.indexOf(mockID));
    MyPage.ViewActivityUrl = MyPage.ViewActivityUrl.substr(0, MyPage.ViewActivityUrl.indexOf(mockID));
    MyPage.FilesList = new Nurbiosis.AjaxLoadingElement("#FilesList");
    MyPage.FilesList.Progress("#Files progress").OnSuccess(MyPage.Events.FilesList_Loaded).load();
    MyPage.ListOfOrganizations = new Nurbiosis.AjaxLoadingElement("#ListOfOrganizations");
    MyPage.ListOfPersons = new Nurbiosis.AjaxLoadingElement("#ListOfPersons");
    MyPage.ListOfInvoices = new Nurbiosis.AjaxLoadingElement("#ListOfInvoices");
    MyPage.ListOfProjects = new Nurbiosis.AjaxLoadingElement("#ListOfProjects");
    MyPage.ListOfActivities = new Nurbiosis.AjaxLoadingElement("#ListOfActivities");
    MyPage.ListOfOrganizations.Progress("#Organizations progress").OnSuccess(MyPage.Events.ListOfOrganizations_Loaded).load();
    MyPage.ListOfPersons.Progress("#Persons progress").OnSuccess(MyPage.Events.ListOfPersons_Loaded).load();
    MyPage.ListOfInvoices.Progress("#Invoices progress").OnSuccess(MyPage.Events.ListOfInvoices_Loaded).load();
    MyPage.ListOfProjects.Progress("#Projects progress").OnSuccess(MyPage.Events.ListOfProjects_Loaded).load();
    MyPage.ListOfActivities.Progress("#Activities progress").OnSuccess(MyPage.Events.ListOfActivities_Loaded).load();
    MyPage.Forms.FileDelete = jQuery("#frmFileDelete");
    MyPage.Forms.FileRename = jQuery("#frmFileRename");
    MyPage.Forms.FileUploadBrowse = jQuery("#frmFileUploadBrowse");
    MyPage.Forms.FileUploadDropzone = jQuery("#frmFileUploadDropzone");
    MyPage.Forms.DeleteOrganization = jQuery("#frmOrganizationsDelete");
    MyPage.Forms.DeletePerson = jQuery("#frmPersonsDelete");
    MyPage.Forms.DeleteInvoice = jQuery("#frmInvoicesDelete");
    MyPage.Forms.DeleteProject = jQuery("#frmProjectsDelete");
    MyPage.Forms.DeleteActivity = jQuery("#frmActivitiesDelete");
    MyPage.DOM.OldFileName = jQuery("#frmFileRename input[name='OldFileName']");
    MyPage.DOM.NewFileName = jQuery("#frmFileRename input[name='NewFileName']");
    MyPage.DOM.DeleteFileName = jQuery("#frmFileDelete input[name='FileName']");
    MyPage.DOM.ModalRenameFile = jQuery("#ModalRenameFile");
    MyPage.DOM.FilesBrowse = jQuery("#frmFileUploadBrowse input[name='Files']");
    Nurbiosis.AjaxifyForm(MyPage.Forms.FileDelete).Target("#FilesList").Progress("#Files progress").OnSuccess(MyPage.Events.FilesList_Loaded);
    Nurbiosis.AjaxifyForm(MyPage.Forms.FileRename).Target("#FilesList").Progress("#Files progress").OnSuccess(MyPage.Events.FilesList_Loaded);
    Nurbiosis.AjaxifyForm(MyPage.Forms.DeleteOrganization).Target("#ListOfOrganizations").Progress("#Organizations progress").OnSuccess(MyPage.Events.ListOfOrganizations_Loaded);
    Nurbiosis.AjaxifyForm(MyPage.Forms.DeletePerson).Target("#ListOfPersons").Progress("#Persons progress").OnSuccess(MyPage.Events.ListOfPersons_Loaded);
    Nurbiosis.AjaxifyForm(MyPage.Forms.DeleteInvoice).Target("#ListOfInvoices").Progress("#Invoices progress").OnSuccess(MyPage.Events.ListOfInvoices_Loaded);
    Nurbiosis.AjaxifyForm(MyPage.Forms.DeleteProject).Target("#ListOfProjects").Progress("#Projects progress").OnSuccess(MyPage.Events.ListOfProjects_Loaded);
    Nurbiosis.AjaxifyForm(MyPage.Forms.DeleteActivity).Target("#ListOfActivities").Progress("#Activities progress").OnSuccess(MyPage.Events.ListOfActivities_Loaded);
    //Nurbiosis.AjaxifyForm(MyPage.Forms.FileUploadBrowse).Target("#FilesList").Progress("#Files progress").OnSuccess(MyPage.Events.FilesList_Loaded);
    MyPage.Do.Initialize_DropZone();
}



jQuery(Page_Start);