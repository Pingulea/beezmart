﻿


var MyPage = {
    ViewUrlInvoices: null,
    ViewUrlOrgs: null,
    ViewUrlPers: null,
    PutHrefToLinks: null
};



MyPage.PutHrefToLinks = function (tr) {
    let crmLink = tr.find("td:nth-child(1) a");
    let invoiceLink = tr.find("td:nth-child(2) a");
    let dataValue = crmLink.attr("data-value");
    let customerType = dataValue.substring(0, 6);
    let customerID = dataValue.substr(7);
    if (customerType === "OrgsID") {
        crmLink.attr("href", MyPage.ViewUrlOrgs + customerID);
        invoiceLink.attr("href", MyPage.ViewUrlInvoices + "&OrganizationID=" + customerID);
    }
    else {
        crmLink.attr("href", MyPage.ViewUrlPers + customerID);
        invoiceLink.attr("href", MyPage.ViewUrlInvoices + "&PersonID=" + customerID);
    }
};



function Page_Start()
{
	MyPage.ViewUrlInvoices = jQuery("#hdnInvoicesViewUrl").val();
	MyPage.ViewUrlOrgs = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.ViewUrlPers = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
    jQuery("#Records table.table tbody tr").each(function () { MyPage.PutHrefToLinks(jQuery(this)); });
}



jQuery(Page_Start);