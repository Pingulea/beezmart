﻿


var MyPage = {};



function GetPayment(paymentID)
{
	MyPage.PaymentEditDialog.modal("show");
	MyPage.PaymentEditFields.Source(MyPage.PaymentFieldsDataSourceURL + paymentID).load();
}
function Payment_Loaded()
{
    let dateControl = jQuery("#ModalEditPayment input[name='PaymentDate']");
    let dateString = dateControl.val();
	dateString = dateString.substr(0, dateString.indexOf("T"));
	MyPage.PaymentEditorCalendar.SetDate(dateString);
}
function DeletePayment(paymentID, invoiceID)
{
	if (!confirm("Are you sure you want to delete the payment?\n\nYou won't be able to undo this operation.")) return;
	jQuery("#frmDeletePayment input[name='PaymentID']").val(paymentID);
	jQuery("#frmDeletePayment input[name='InvoiceID']").val(invoiceID);
	jQuery("#PaymentDeleteMessage").html("");
	jQuery("#ModalPaymentDelete").modal("show");
	jQuery("#ModalPaymentDelete .modal-footer a.info").attr("href", MyPage.InvoiceViewUrl + invoiceID);
	jQuery("#frmDeletePayment").submit();
}



function PaymentsList_Updated()
{
	function PutHrefToLinks(tr)
	{
        let InvoiceLink = tr.find("td:nth-child(4) a");
        let InvoiceID = InvoiceLink.attr("data-id")
		InvoiceLink.attr("href", MyPage.InvoiceViewUrl + InvoiceID);
        let CrmLink = tr.find("td:nth-child(5) a");
		if (CrmLink.length > 0)
		{
            let OrgsID = CrmLink.attr("data-orgs")
			if (OrgsID) CrmLink.attr("href", MyPage.CrmOrgsViewUrl + OrgsID);
            let PersID = CrmLink.attr("data-pers")
			if (PersID) CrmLink.attr("href", MyPage.CrmPersViewUrl + PersID);
		}
        let PaymentID = tr.attr("data-record");
		tr.find("td:first-child a").click(function() { GetPayment(PaymentID); });
		tr.find("td:last-child a").click(function() { DeletePayment(PaymentID, InvoiceID); });
	}
	jQuery("#Payments tbody tr").each(function() { PutHrefToLinks(jQuery(this)); });
}
function PaymentEdit_Processed()
{
    jQuery("#ModalPaymentMessage").modal("show");
    let invoiceID = jQuery("#EditPaymentMessage input#hdnInvoiceID").val();
	jQuery("#ModalPaymentMessage .modal-footer a.info").attr("href", MyPage.InvoiceViewUrl + invoiceID);
	MyPage.SearchResult.load();
}
function Payment_Deleted()
{
	MyPage.SearchResult.load();
}


function Page_Start()
{
	MyPage.InvoiceViewUrl = jQuery("#hdnInvoiceViewUrl").val() + "?InvoiceID=";
	MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	Nurbiosis.AttachDatepicker();
	MyPage.PaymentEditorCalendar = new Nurbiosis.InitializeCalendar("#PaymentDateCalendar").Target("#ModalEditPayment input[name='PaymentDate']").go();
	PaymentsList_Updated();
	MyPage.SearchResult = Nurbiosis.AjaxLoadingElement("#Payments").Progress("#prgrsPayments").OnSuccess(PaymentsList_Updated);
	MyPage.PaymentEditDialog = jQuery("#ModalEditPayment");
	MyPage.PaymentEditFields = new Nurbiosis.AjaxLoadingElement(MyPage.PaymentEditDialog).Progress("#ModalEditPayment progress").ResponseType("json").OnSuccess(Payment_Loaded);
	MyPage.PaymentFieldsDataSourceURL = MyPage.PaymentEditDialog.attr("data-source");
	MyPage.PaymentFieldsDataSourceURL = MyPage.PaymentFieldsDataSourceURL.substr(0, MyPage.PaymentFieldsDataSourceURL.indexOf("=") + 1);
	Nurbiosis.AjaxifyForm("#ModalEditPayment form").Target("#EditPaymentMessage").Progress("#ModalEditPayment progress").OnSuccess(PaymentEdit_Processed);
	Nurbiosis.AjaxifyForm("#frmDeletePayment").Target("#PaymentDeleteMessage").Progress("#ModalPaymentDelete progress").OnSuccess(Payment_Deleted);
}



jQuery(Page_Start);