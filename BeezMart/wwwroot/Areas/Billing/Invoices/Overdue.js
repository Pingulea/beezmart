﻿


var MyPage = {
    ViewUrl: {
        Invoice: null,
        CrmOrgs: null,
        CrmPers: null,
    },
    InvoicesSourceURL: {
        Regular: null,
        Storno: null,
        Advance: null,
        Risturn: null,
        Proforma: null,
    }
};



function LoadInitialCurrency()
{
    let defaultCurrency = jQuery("#Currencies").attr("data-value");
    let defaultCurrencyButton = jQuery("#Currencies button[data-value='" + defaultCurrency + "']");
    if (defaultCurrencyButton.length > 0)
    {
        defaultCurrencyButton.removeClass("btn-light").addClass("btn-success");
        LoadCurrencyInvoices(defaultCurrency);
        return;
    }
    let firstCurrencyButton = jQuery("#Currencies button[data-value]");
    if (firstCurrencyButton.length > 0)
    {
        firstCurrencyButton.eq(0).removeClass("btn-light").addClass("btn-success");
        LoadCurrencyInvoices(firstCurrencyButton.eq(0).attr("data-value"));
    }
}
function PutHrefToLinks(tr)
{
    let invoiceLink = tr.find("td:nth-child(1) a");
    invoiceLink.attr("href", MyPage.ViewUrl.Invoice + tr.attr("data-record"));
    let crmLink = tr.find("td:nth-child(2) a");
    if (crmLink.length > 0)
    {
        let dataValue = crmLink.attr("data-value");
        if (dataValue.substring(0, 6) === "OrgsID")
        {
            crmLink.attr("href", MyPage.ViewUrl.CrmOrgs + dataValue.substr(7));
        }
        else
        {
            crmLink.attr("href", MyPage.ViewUrl.CrmPers + dataValue.substr(7));
        }
    }
}
function ChangeCurrency(currency)
{
    let currentlySelectedCurrencyButton = jQuery("#Currencies button.btn-success");
    if (currentlySelectedCurrencyButton.attr("data-value") === currency) return;
    currentlySelectedCurrencyButton.removeClass("btn-success").addClass("btn-light");
    let newlySelectedCurrencyButton = jQuery("#Currencies button[data-value='" + currency + "']");
    newlySelectedCurrencyButton.removeClass("btn-light").addClass("btn-success");
    LoadCurrencyInvoices(currency);
}
function LoadCurrencyInvoices(currency)
{
    MyPage.InvoicesRegular.Source(MyPage.InvoicesSourceURL.Regular + currency).load();
    MyPage.InvoicesStorno.Source(MyPage.InvoicesSourceURL.Storno + currency).load();
    MyPage.InvoicesAdvance.Source(MyPage.InvoicesSourceURL.Advance + currency).load();
    MyPage.InvoicesRisturn.Source(MyPage.InvoicesSourceURL.Risturn + currency).load();
    MyPage.InvoicesProforma.Source(MyPage.InvoicesSourceURL.Proforma + currency).load();
}
function InvoiceListRegular_Updated()
{
	let badge = jQuery("ul.nav-tabs a[href='#Regular'] span.badge");
	let rows = jQuery("#Regular tbody tr");
	badge.text(rows.length);
	rows.each( function() { PutHrefToLinks(jQuery(this)); } );
}
function InvoiceListStorno_Updated()
{
	let badge = jQuery("ul.nav-tabs a[href='#Storno'] span.badge");
	let rows = jQuery("#Storno tbody tr");
	badge.text(rows.length);
	rows.each( function() { PutHrefToLinks(jQuery(this)); } );
}
function InvoiceListAdvance_Updated()
{
	let badge = jQuery("ul.nav-tabs a[href='#Advance'] span.badge");
	let rows = jQuery("#Advance tbody tr");
	badge.text(rows.length);
	rows.each( function() { PutHrefToLinks(jQuery(this)); } );
}
function InvoiceListRisturn_Updated()
{
	let badge = jQuery("ul.nav-tabs a[href='#Risturn'] span.badge");
	let rows = jQuery("#Risturn tbody tr");
	badge.text(rows.length);
	rows.each( function() { PutHrefToLinks(jQuery(this)); } );
}
function InvoiceListProforma_Updated()
{
	let badge = jQuery("ul.nav-tabs a[href='#Proforma'] span.badge");
	let rows = jQuery("#Proforma tbody tr");
	badge.text(rows.length);
	rows.each( function() { PutHrefToLinks(jQuery(this)); } );
}



function GetSourceURL(selector)
{
    let url = jQuery(selector).attr("data-source");
    let urlWithNoCurrency = url.substring(0, url.length - 3);
    return urlWithNoCurrency;
}



function Page_Start()
{
    MyPage.ViewUrl.Invoice = jQuery("#hdnViewUrlInvoice").val() + "?InvoiceID=";
    MyPage.ViewUrl.CrmOrgs = jQuery("#hdnViewUrlCrmOrgs").val() + "?OrganizationID=";
    MyPage.ViewUrl.CrmPers = jQuery("#hdnViewUrlCrmPers").val() + "?PersonID=";
    MyPage.InvoicesSourceURL.Regular = GetSourceURL("#Regular > div");
    MyPage.InvoicesSourceURL.Storno = GetSourceURL("#Storno > div");
    MyPage.InvoicesSourceURL.Advance = GetSourceURL("#Advance > div");
    MyPage.InvoicesSourceURL.Risturn = GetSourceURL("#Risturn > div");
    MyPage.InvoicesSourceURL.Proforma = GetSourceURL("#Proforma > div");
    jQuery("#Currencies button").click(function() { ChangeCurrency(jQuery(this).attr("data-value")); });
    MyPage.InvoicesRegular = new Nurbiosis.AjaxLoadingElement("#Regular > div").Progress("#Regular progress").OnSuccess(InvoiceListRegular_Updated);
    MyPage.InvoicesStorno = new Nurbiosis.AjaxLoadingElement("#Storno > div").Progress("#Storno progress").OnSuccess(InvoiceListStorno_Updated);
    MyPage.InvoicesAdvance = new Nurbiosis.AjaxLoadingElement("#Advance > div").Progress("#Advance progress").OnSuccess(InvoiceListAdvance_Updated);
    MyPage.InvoicesRisturn = new Nurbiosis.AjaxLoadingElement("#Risturn > div").Progress("#Risturn progress").OnSuccess(InvoiceListRisturn_Updated);
    MyPage.InvoicesProforma = new Nurbiosis.AjaxLoadingElement("#Proforma > div").Progress("#Proforma progress").OnSuccess(InvoiceListProforma_Updated);
    LoadInitialCurrency();
}



jQuery(Page_Start);