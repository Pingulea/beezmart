﻿


var MyPage = {};



function ResetModalEditInvoiceItem()
{
	jQuery("#ModalEditInvoiceItem input[name='ID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='ProductCatalogItemID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='ProductCatalogCategoryID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='ProductOrService']").val("P");
	jQuery("#ModalEditInvoiceItem input[name='SalesOrderItemID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='DeliveryDocumentID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='DeliveryDocumentItemID']").val("");
	jQuery("#ModalEditInvoiceItem textarea[name='Name']").val("");
	jQuery("#ModalEditInvoiceItem input[name='Quantity']").val("1");
	jQuery("#ModalEditInvoiceItem input[name='UnitMeasure']").val(jQuery("#ModalEditInvoiceItem input[name='UnitMeasure']").attr("data-default"));
	jQuery("#ModalEditInvoiceItem input[name='UnitPrice']").val("");

}
function DeleteInvoiceItem(InvoiceItemID)
{
	if (!confirm("Are you sure you want to delete the item?\n\nYou cannot undo this operation.")) return;
	jQuery("#frmDelInvoiceItem input[name='ItemID']").val(InvoiceItemID);
	jQuery("#frmDelInvoiceItem").submit();
}
function EditInvoiceItem(InvoiceItemID)
{
	jQuery("#ModalEditInvoiceItem").modal("show");
	MyPage.InvoiceItemFields.Source(MyPage.InvoiceItemFieldsSourceURL + InvoiceItemID).load();
}
function ResetDelegateEditor()
{
	jQuery("#DelegateEditor input[name='ID']").val("");
	jQuery("#DelegateEditor input[name='Name']").val("");
	jQuery("#DelegateEditor input[name='IdField1']").val("");
	jQuery("#DelegateEditor input[name='IdField2']").val("");
	jQuery("#DelegateEditor input[name='IdField3']").val("");
	jQuery("#DelegateEditor input[name='RecordID']").val("");
}
function ResetTransportEditor()
{
	jQuery("#TransportEditor input[name='ID']").val("");
	jQuery("#TransportEditor input[name='Means']").val("");
	jQuery("#TransportEditor input[name='Registration']").val("");
	jQuery("#TransportEditor input[name='RecordID']").val("");
}



function LookupCustomer_Updating()
{
	jQuery("#SearchResultCustomers").html("");
	jQuery("#ModalLookupCustomer").modal("hide");
	jQuery("#ModalCustomers").modal("show");
}
function LookupCustomers_Updated()
{
	jQuery("#ModalCustomers table tbody td:first-child a").click(
			function()
			{
				let ctl = document.getElementById("hdnCustomerSettingsURL");
				let currency = document.getElementById("Invoice_Currency").value;
				let url = ctl.value + "&" + ctl.getAttribute("data-param") + "=" + jQuery(this).attr("data-value") + "&Currency=" + currency;
				jQuery("#ModalCustomers").modal("hide");
				MyPage.CustomerDataFields.Source(url).load();
			}
		);
}
function InvoiceItems_Updated()
{
	jQuery("#InvoiceItemsList tbody tr").each(
			function()
			{
				let invoiceItemID = jQuery(this).attr("data-record");
				jQuery(this).find("td:nth-child(2) a").click(function() { EditInvoiceItem(invoiceItemID); });
				jQuery(this).find("td:nth-child(8) a").click(function() { DeleteInvoiceItem(invoiceItemID); });
			}
		);
    let invoiceTotalDue = jQuery("#InvoiceItemsList tfoot tr").attr("data-value");
    let invoiceTotalDueFormatted = jQuery("#InvoiceItemsList tfoot tr").attr("data-value-formatted");
    jQuery("#TotalDue strong").text(invoiceTotalDueFormatted);
}
function Delegates_Updated()
{
	jQuery("#Delegates tbody tr td:first-child a").click(
			function ()
			{
				let row = jQuery(this).parent().parent();
				jQuery("#Invoice_DelegateName").val(jQuery(this).text());
				jQuery("#Invoice_DelegateID1").val(row.children().eq(1).text());
				jQuery("#Invoice_DelegateID2").val(row.children().eq(2).text());
				jQuery("#Invoice_DelegateID3").val(row.children().eq(3).text());
				jQuery("#ModalPickDelegate").modal("hide");
			}
		);
	jQuery("#Delegates tbody tr td:last-child a").click(
			function ()
			{
				let row = jQuery(this).parent().parent();
				jQuery("#DelegateEditor input[name='ID']").val(row.attr("data-value"));
				jQuery("#DelegateEditor input[name='Name']").val(row.children().eq(0).text());
				jQuery("#DelegateEditor input[name='IdField1']").val(row.children().eq(1).text());
				jQuery("#DelegateEditor input[name='IdField2']").val(row.children().eq(2).text());
				jQuery("#DelegateEditor input[name='IdField3']").val(row.children().eq(3).text());
				jQuery("#DelegateEditor input[name='RecordID']").val(row.attr("data-value"));
				jQuery("#ModalPickDelegate").modal("hide");
				jQuery("#ModalEditDelegate").modal("show");
			}
		);
	jQuery("#Delegates tbody tr td:last-child span").click(
			function()
			{
				if (!confirm("Are you sure you want to delete the item?\n\nYou cannot undo this operation.")) return;
				let row = jQuery(this).parent().parent();
				jQuery("#DelegateEditor input[name='RecordID']").val(row.attr("data-value"));
				jQuery("#DelegateEditor > form").submit();
			}
		);
}
function Transports_Updated()
{
	jQuery("#Transports tbody tr td:first-child a").click(
			function()
			{
				let row = jQuery(this).parent().parent();
				jQuery("#Invoice_TransportDetail1").val(jQuery(this).text());
				jQuery("#Invoice_TransportDetail2").val(row.children().eq(1).text());
				jQuery("#ModalPickTransport").modal("hide");
			}
		);
	jQuery("#Transports tbody tr td:last-child a").click(
			function()
			{
				let row = jQuery(this).parent().parent();
				jQuery("#TransportEditor input[name='ID']").val(row.attr("data-value"));
				jQuery("#TransportEditor input[name='Means']").val(row.children().eq(0).text());
				jQuery("#TransportEditor input[name='Registration']").val(row.children().eq(1).text());
				jQuery("#TransportEditor input[name='RecordID']").val(row.attr("data-value"));
				jQuery("#ModalPickTransport").modal("hide");
				jQuery("#ModalEditTransport").modal("show");
			}
		);
	jQuery("#Transports tbody tr td:last-child span").click(
			function()
			{
				if (!confirm("Are you sure you want to delete the item?\n\nYou cannot undo this operation.")) return;
				let row = jQuery(this).parent().parent();
				jQuery("#TransportEditor input[name='RecordID']").val(row.attr("data-value"));
				jQuery("#TransportEditor > form").submit();
			}
		);
}


function btnCustomersSearchAgain_Click()
{
	jQuery("#ModalLookupCustomer").modal("show");
	jQuery("#ModalCustomers").modal("hide");
}
function btnSaveCustomerDetails_Click()
{
	let customerDetails = 
		{
			OrganizationID: document.getElementById("Invoice_OrganizationID").value,
			PersonID: document.getElementById("Invoice_PersonID").value,
			BillingAddressID: document.getElementById("hdnCustomerBillingAddressID").value,
			Name: document.getElementById("Invoice_CustomerName").value,
			City: document.getElementById("Invoice_CustomerCity").value,
			Province: document.getElementById("Invoice_CustomerProvince").value,
			StreetAddress1: document.getElementById("Invoice_CustomerAddress").value,
			RegistrationNumber: document.getElementById("Invoice_CustomerRegistrationNumber").value,
			TaxID: document.getElementById("Invoice_CustomerTaxID").value,
			BankAccountID: document.getElementById("hdnCustomerBankAccountID").value,
			BankName: document.getElementById("Invoice_CustomerBankName").value,
			BankAccountIBAN: document.getElementById("Invoice_CustomerBankAccount").value,
			Currency: document.getElementById("Invoice_Currency").value,
			__RequestVerificationToken: jQuery("input[name='__RequestVerificationToken']").val()
		};
	let AjaxCallSettings =
		{
			type: "POST",
			url: document.getElementById("hdnSaveCustomerDetailsURL").value,
			data: customerDetails,
			dataType: "HTML",
			cache: false,
			success: function(data)
			{
				jQuery("#SaveCustomerDetailsResult").html(data);
				jQuery("fieldset#CustomerData #SaveCustomerDetails").slideUp();
			},
			error: function()
			{
				toastr.error("Operation failed while communicating with server.", "Error");
			},
			complete: function()
			{
				jQuery("#CustomerData progress").hide();
			}
		};
	jQuery("#CustomerData progress").show();
	jQuery.ajax(AjaxCallSettings);
}
function CustomerData_Keypress()
{
    jQuery("#CustomerData input[name='Invoice.CustomerCity']").on("input", onKeypress);
    jQuery("#CustomerData input[name='Invoice.CustomerProvince']").on("input", onKeypress);
    jQuery("#CustomerData input[name='Invoice.CustomerAddress']").on("input", onKeypress);
    jQuery("#CustomerData input[name='Invoice.CustomerTaxID']").on("input", onKeypress);
    jQuery("#CustomerData input[name='Invoice.CustomerRegistrationNumber']").on("input", onKeypress);
    jQuery("#CustomerData input[name='Invoice.CustomerBankName']").on("input", onKeypress);
    jQuery("#CustomerData input[name='Invoice.CustomerBankAccount']").on("input", onKeypress);
    function onKeypress() {
	    let OrganizationID = document.getElementById("Invoice_OrganizationID").value;
	    let PersonID = document.getElementById("Invoice_PersonID").value;
	    if (OrganizationID.length > 0 || PersonID.length > 0)
	    {
		    jQuery("#SaveCustomerDetails").slideDown();
	    }
    }
}
function ModalEditInvoiceItem_Shown()
{
	jQuery("#ModalEditInvoiceItem textarea[name='Name']").focus();
	if (document.getElementById("Invoice_CustomerName").value == "")
	{
		toastr.warning("Please fill in the customer name first!<br />An invoice item must be linked to an invoice reference; the reference cannot be created without a customer name.", "Validation");
		jQuery("#ModalEditInvoiceItem").modal("hide");
		jQuery("#Invoice_CustomerName").focus();
	}
}
function btnAddDelegate_Click()
{
	ResetDelegateEditor();
	jQuery("#ModalPickDelegate").modal("hide");
	jQuery("#ModalEditDelegate").modal("show");
}
function btnAddTransport_Click()
{
	ResetTransportEditor();
	jQuery("#ModalPickTransport").modal("hide");
	jQuery("#ModalEditTransport").modal("show");
}
function DelegateEditor_Loaded()
{
	function callback()
	{
		jQuery("#ModalEditDelegate").modal("hide");
		jQuery("#ModalPickDelegate").modal("show");
	}
	Nurbiosis.AjaxifyForm("#ModalEditDelegate form").Before(callback).Target("#Delegates").Progress("#ModalPickDelegate progress").OnSuccess(Delegates_Updated);
	Nurbiosis.AjaxifyForm("#DelegateEditor > form").Before(callback).Target("#Delegates").Progress("#ModalPickDelegate progress").OnSuccess(Delegates_Updated);
}
function TransportEditor_Loaded()
{
	function callback()
	{
		jQuery("#ModalEditTransport").modal("hide");
		jQuery("#ModalPickTransport").modal("show");
	}
	Nurbiosis.AjaxifyForm("#ModalEditTransport form").Before(callback).Target("#Transports").Progress("#ModalPickTransport progress").OnSuccess(Transports_Updated);
	Nurbiosis.AjaxifyForm("#TransportEditor > form").Before(callback).Target("#Transports").Progress("#ModalPickTransport progress").OnSuccess(Transports_Updated);
}
function btnTodayNow_Click()
{
	let today = new Date();
	let year = today.getFullYear().toString();
	let month = (today.getMonth() + 1).toString();
	let day = today.getDate().toString();
	let hour = today.getHours().toString();
	let minute = today.getMinutes().toString();
	if (month.length == 1) month = "0" + month;
	if (day.length == 1) day = "0" + day;
	if (hour.length == 1) hour = "0" + hour;
	if (minute.length == 1) minute = "0" + minute;
	jQuery("#Invoice_TransportDetail3").val(year + "-" + month + "-" + day);
	jQuery("#Invoice_TransportDetail4").val(hour + ":" + minute);
}
function btnSaveInvoiceItem_Click()
{
	let ValidationErrors = "";
	if (jQuery("#ModalEditInvoiceItem textarea[name='Name']").val() == "") ValidationErrors += "- Product or service name.<br />";
	if (jQuery("#ModalEditInvoiceItem input[name='Quantity']").val() == "") ValidationErrors += "- Quantity; must be an integer number.<br />";
	if (jQuery("#ModalEditInvoiceItem input[name='UnitMeasure']").val() == "") ValidationErrors += "- Unit measure.<br />";
	if (jQuery("#ModalEditInvoiceItem input[name='UnitPrice']").val() == "") ValidationErrors += "- Unit price; must be a decimal number.<br />";
	if (ValidationErrors.length > 0)
	{
		ValidationErrors = "Error while validating invoice item fields:<br />" + ValidationErrors;
		toastr.warning(ValidationErrors, "Validation");
		return;
	}
	let InvoiceID = document.getElementById("Invoice_ID").value;
	if (InvoiceID == "")
	{
		if (document.getElementById("Invoice_CustomerName").value == "")
		{
			toastr.warning("PLEASE FILL IN THE CUSTOMER NAME FIRST!<br />An invoice item must be linked to an invoice reference; the reference cannot be created without a customer name.", "Validation");
			jQuery("#ModalEditInvoiceItem").modal("hide");
			return;
		}
		let InvoiceForm = jQuery("#frmInvoiceRecord");
		let AjaxCallSettings =
		{
			type: "POST",
			url: InvoiceForm.attr("data-ajax-save"),
			data: InvoiceForm.serialize(),
			dataType: "JSON",
			cache: false,
			success: function(data)
			{
				if (data.InvoiceID)
				{
					document.getElementById("Invoice_ID").value = data.InvoiceID;
                    jQuery("#ModalEditInvoiceItem input[name='InvoiceID']").val(data.InvoiceID);
                    jQuery("#frmDelInvoiceItem input[name='InvoiceID']").val(data.InvoiceID);
					jQuery("#ModalEditInvoiceItem form").submit();
				}
				else
				{
					toastr.error("The invoice record could not be saved; the invoice item cannot be linked to an invoice reference.", "Error");
				}
			},
			error: function()
			{
				toastr.error("Failed to save data to server.", "Error");
			}
		};
		jQuery.ajax(AjaxCallSettings);
	}
	else
	{
		jQuery("#ModalEditInvoiceItem input[name='InvoiceID']").val(InvoiceID);
		jQuery("#ModalEditInvoiceItem form").submit();
	}
}
function btnSave_Click()
{
	if (document.getElementById("Invoice_CustomerName").value == "")
	{
		toastr.warning("The CUSTOMER NAME is a required field for saving an invoice.", "Validation");
		return;
	}
	document.forms["frmInvoiceRecord"].submit();
}
function btnPrint_Click()
{
	let PrintingURL = document.getElementById("hdnPrintingUrl").value;
	PrintingURL = PrintingURL.substring(0, PrintingURL.toLowerCase().indexOf("invoiceid=") + 10);
	if (document.getElementById("Invoice_CustomerName").value == "")
	{
		toastr.warning("PLEASE FILL IN THE CUSTOMER NAME FIRST!<br />An invoice cannot be saved without a customer name.", "Validation");
		return;
	}
	if (jQuery("#InvoiceItemsList tbody tr").length == 0)
	{
		toastr.warning("THE INVOICE HAS NO ITEMS!<br />An invoice is not valid for print if it does not hold any item(s).", "Validation");
		return;
	}
	let InvoiceForm = jQuery("#frmInvoiceRecord");
	let AjaxCallSettings =
	{
		type: "POST",
		url: InvoiceForm.attr("data-ajax-save"),
		data: InvoiceForm.serialize(),
		dataType: "JSON",
		cache: false,
		success: function(data)
		{
			if (data.InvoiceID)
			{
				document.getElementById("Invoice_ID").value = data.InvoiceID;
				window.location = PrintingURL + data.InvoiceID.toString();
			}
			else
			{
				toastr.error("The invoice record could not be saved; the application cannot continue to printing.", "Error");
			}
		},
		error: function()
		{
			toastr.error("Failed to save data to server.", "Error");
		}
	};
	jQuery.ajax(AjaxCallSettings);
}



function Page_Start()
{
	Nurbiosis.AttachDatepicker();
	if (jQuery("#hdnAutoAccountingID").val() == "true")
	{
		jQuery("#Invoice_AccountingID").prop("disabled", true);
		jQuery("label[for='Invoice_AccountingID']").html("Accounting no... <span style='color: red; cursor: help;'>AUTO</span>");
		jQuery("#Invoice_AccountingID").parent().attr("title", "The application is configured to auto-assign Accounting IDs to invoices when they are printed.\n\nYou can only see the format of the IDs.");
	}
	Nurbiosis.AjaxifyForm("#frmLookupOrgs").Before(LookupCustomer_Updating).Target("#SearchResultCustomers").Progress("#ModalCustomers progress").OnSuccess(LookupCustomers_Updated);
	Nurbiosis.AjaxifyForm("#frmLookupPers").Before(LookupCustomer_Updating).Target("#SearchResultCustomers").Progress("#ModalCustomers progress").OnSuccess(LookupCustomers_Updated);
	Nurbiosis.AjaxifyForm("#ModalEditInvoiceItem form").Target("#InvoiceItemsList").Progress("#InvoiceItems progress").OnSuccess(InvoiceItems_Updated);
	Nurbiosis.AjaxifyForm("#frmDelInvoiceItem").Target("#InvoiceItemsList").Progress("#InvoiceItems progress").OnSuccess(InvoiceItems_Updated);
	jQuery("#Invoice_CustomerName").focus();
	jQuery("#ModalLookupCustomer").on("shown.bs.modal", function() { jQuery("#ModalLookupCustomer input[name='OrgsNamePattern']").focus(); });
	jQuery("#ModalEditInvoiceItem").on("shown.bs.modal", ModalEditInvoiceItem_Shown);
	jQuery("#ModalEditInvoiceItem").on("hidden.bs.modal", ResetModalEditInvoiceItem);
	CustomerData_Keypress();
	Delegates_Updated();
	Transports_Updated();
	MyPage.DelegateEditor = new Nurbiosis.AjaxLoadingElement("#DelegateEditor").OnSuccess(DelegateEditor_Loaded).load();
	MyPage.TransportEditor = new Nurbiosis.AjaxLoadingElement("#TransportEditor").OnSuccess(TransportEditor_Loaded).load();
	MyPage.CustomerDataFields = new Nurbiosis.AjaxLoadingElement("#CustomerData").Progress("#CustomerData progress").ResponseType("json");
	MyPage.InvoiceItemFields = new Nurbiosis.AjaxLoadingElement("#ModalEditInvoiceItem form").Progress("#ModalEditInvoiceItem progress").ResponseType("json");
	MyPage.InvoiceItemFieldsSourceURL = jQuery("#ModalEditInvoiceItem form").attr("data-source");
	MyPage.InvoiceItemFieldsSourceURL = MyPage.InvoiceItemFieldsSourceURL.substring(0, MyPage.InvoiceItemFieldsSourceURL.indexOf("ItemID=") + 7);
}



jQuery(Page_Start);