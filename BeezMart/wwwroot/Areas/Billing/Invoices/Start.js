﻿


var MyPageDOM = {
    ModalInvoicingDetails: null,
    ResetInvoicingDetails: null,
    SearchResultsLoaded: null,
    SubmitForm: null
};



MyPageDOM.SearchResultsLoaded = function()
{
    function PopulateSubmitForm(tr)
    {
        let cell1 = tr.find("td:nth-child(1)");
        MyPageDOM.SubmitForm.find("input[name='OrganizationID']").val(cell1.attr("data-OrgsID"));
        MyPageDOM.SubmitForm.find("input[name='PersonID']").val(cell1.attr("data-PersID"));
        MyPageDOM.SubmitForm.find("input[name='CustomerName']").val(cell1.text());
        let cell2 = tr.find("td:nth-child(2)");
        MyPageDOM.SubmitForm.find("input[name='City']").val(cell2.text());
        MyPageDOM.SubmitForm.find("input[name='StreetAddress1']").val(cell2.attr("title"));
        MyPageDOM.SubmitForm.find("input[name='BillingAddressID']").val(cell2.attr("data-addressID"));
        let cell3 = tr.find("td:nth-child(3)");
        MyPageDOM.SubmitForm.find("input[name='StateProvince']").val(cell3.text());
        let cell4 = tr.find("td:nth-child(4)");
        MyPageDOM.SubmitForm.find("input[name='TaxID']").val(cell4.text());
        MyPageDOM.SubmitForm.find("input[name='RegistrationNumber']").val(cell4.attr("title"));
        let cell5 = tr.find("td:nth-child(5)");
        MyPageDOM.SubmitForm.find("input[name='BankName']").val(cell5.attr("title"));
        MyPageDOM.SubmitForm.find("input[name='BankAccountIBAN']").val(cell5.text());
        MyPageDOM.SubmitForm.find("input[name='BankAccountID']").val(cell5.attr("data-accountID"));
    }
    function ActivateLinks(tr)
    {
        tr.find("td:nth-child(1) a").click(function () { PopulateSubmitForm(tr); MyPageDOM.SubmitForm.submit(); });
        tr.find("td:nth-child(6) a").click(function () { PopulateSubmitForm(tr); MyPageDOM.ModalInvoicingDetails.modal("show"); });
    }
    jQuery("table.table tbody tr").each(function () { ActivateLinks(jQuery(this)); });
}



MyPageDOM.ResetInvoicingDetails = function()
{
    MyPageDOM.SubmitForm.find("input[name='OrganizationID']").val("");
    MyPageDOM.SubmitForm.find("input[name='PersonID']").val("");
    MyPageDOM.SubmitForm.find("input[name='CustomerName']").val("");
    MyPageDOM.SubmitForm.find("input[name='City']").val("");
    MyPageDOM.SubmitForm.find("input[name='StateProvince']").val("");
    MyPageDOM.SubmitForm.find("input[name='StreetAddress1']").val("");
    MyPageDOM.SubmitForm.find("input[name='TaxID']").val("");
    MyPageDOM.SubmitForm.find("input[name='RegistrationNumber']").val("");
    MyPageDOM.SubmitForm.find("input[name='BankName']").val("");
    MyPageDOM.SubmitForm.find("input[name='BankAccountIBAN']").val("");
    MyPageDOM.SubmitForm.find("input[name='BillingAddressID']").val("");
    MyPageDOM.SubmitForm.find("input[name='BankAccountID']").val("");
}



function Page_Start()
{
    MyPageDOM.ModalInvoicingDetails = jQuery("#ModalInvoicingDetails");
    MyPageDOM.ModalInvoicingDetails.on("hidden.bs.modal", MyPageDOM.ResetInvoicingDetails);
    MyPageDOM.SubmitForm = MyPageDOM.ModalInvoicingDetails.find("form");
    Nurbiosis.AjaxifyForm("#Search form").Target("#Results").Progress("#Search progress").OnSuccess(MyPageDOM.SearchResultsLoaded);
}



jQuery(Page_Start);