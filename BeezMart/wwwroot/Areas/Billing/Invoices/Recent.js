﻿


var MyPage = {
    DOM: {
        ViewUrlInvoice: null,           // jQuery("#hdnViewUrlInvoice").val() + "?InvoiceID=";
        ViewUrlCrmOrgs: null,           // jQuery("#hdnViewUrlCrmOrgs").val() + "?OrganizationID=";
        CrmPersViewUrl: null,           // jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
    }
};



function PutHrefToLinks(tr)
{
    let InvoiceLink = tr.find("td:nth-child(1) a");
    InvoiceLink.attr("href", MyPage.DOM.ViewUrlInvoice + tr.attr("data-id"));
    let statusCell = tr.find("td:nth-child(2)");
	let statusName = statusCell.attr("class");
	statusCell.attr("title", statusName);
	switch (statusName)
    {
        case "Draft":
            statusCell.html("<i class='bi-list-task text-primary'></i>");
            break;
        case "Validated":
            statusCell.html("<i class='bi-list-task text-info'></i>");
            break;
        case "SentToCustomer":
            statusCell.html("<i class='bi-list-task text-warning'></i>");
            break;
        case "Cancelled":
            statusCell.html("<i class='bi-list-task text-success'></i>");
            break;
        case "Closed":
            statusCell.html("<i class='bi-list-task text-cancelled'></i>");
            break;
        default:
            statusCell.html("<i class='bi-list-task text-danger'></i>");
            break;
    }
    let CrmLink = tr.find("td:nth-child(3) a");
	if (CrmLink.length > 0)
	{
        let OrgsID = CrmLink.attr("data-orgs");
		if (OrgsID) CrmLink.attr("href", MyPage.DOM.ViewUrlCrmOrgs + OrgsID);
        let PersID = CrmLink.attr("data-pers");
		if (PersID) CrmLink.attr("href", MyPage.DOM.CrmPersViewUrl + PersID);
	}
}



function Page_Start()
{
    MyPage.DOM.ViewUrlInvoice = jQuery("#hdnViewUrlInvoice").val() + "?InvoiceID=";
    MyPage.DOM.ViewUrlCrmOrgs = jQuery("#hdnViewUrlCrmOrgs").val() + "?OrganizationID=";
    MyPage.DOM.CrmPersViewUrl = jQuery("#hdnViewUrlCrmPers").val() + "?PersonID=";
    jQuery("#Invoices tbody tr").each(function () { PutHrefToLinks(jQuery(this)); });
}



jQuery(Page_Start);