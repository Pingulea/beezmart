﻿


var MyPage = {
    DOM: {
        InvoiceViewUrl: null,           // jQuery("#hdnInvoiceViewUrl").val() + "?InvoiceID=";
        CrmOrgsViewUrl: null,           // jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
        CrmPersViewUrl: null,           // jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
        AdvancedSearch: null,           // jQuery("#AdvancedSearch");
        InvoiceType: null,              // jQuery("#SearchForm input[name='InvoiceType']");
    },
    Events: {
        btnSubmit_Click: function () {
            jQuery("#SearchForm").slideUp(600, function () { jQuery("#ChangeFilter").slideDown(300); jQuery("#Invoices").slideDown(300); });
            jQuery("ul.nav-tabs a[href] span.badge").text("...");
            MyPage.SearchForms.Regular.submit();
            MyPage.SearchForms.Storno.submit();
            MyPage.SearchForms.Advance.submit();
            MyPage.SearchForms.Risturn.submit();
            MyPage.SearchForms.Proforma.submit();
        },
        btnChangeFilter_Click: function () {
            jQuery("#SearchResults .alert").alert("close");
            jQuery("#ChangeFilter").slideUp(300, function () { jQuery("#SearchForm").slideDown(600); });
        },
    },
    SearchForm: null,
	Export: function(invoiceType) {
        MyPage.DOM.InvoiceType.val(invoiceType);
		let exportUrl = "_Search_Export?" + MyPage.SearchForm.serialize();
		window.open(exportUrl);
	},
	DirectLink: function(invoiceType) {
        MyPage.DOM.InvoiceType.val(invoiceType);
		let pageUrl = window.location.href;
		let questionMarkIndex = pageUrl.indexOf("?");
		if (questionMarkIndex > 0) pageUrl = pageUrl.substring(0, questionMarkIndex);
		let directSearchLink = pageUrl + "?" + MyPage.SearchForm.serialize();
		Nurbiosis.CopyTextToClipboard(directSearchLink);
		let btnDirectLink = jQuery("#btnDirectLink");
		btnDirectLink.attr("title", "Copied to clipboard");
		btnDirectLink.removeClass("btn-outline-secondary").addClass("btn-success");
		function hideTooltip () {
			btnDirectLink.tooltip("hide");
			btnDirectLink.removeClass("btn-success").addClass("btn-outline-secondary");
		}
		let tooltipOptions = {
			//container: "body",
			trigger: "manual"
		};
		btnDirectLink.tooltip(tooltipOptions).tooltip("show");
		setTimeout(hideTooltip, 3000);
	},
    SearchForms: {
        Regular: null,
        Storno: null,
        Advance: null,
        Risturn: null,
        Proforma: null,
    },
    SetInvoiceType: {
        Regular: function () { MyPage.DOM.InvoiceType.val("Regular"); },
        Storno: function () { MyPage.DOM.InvoiceType.val("Storno"); },
        Advance: function () { MyPage.DOM.InvoiceType.val("Advance"); },
        Risturn: function () { MyPage.DOM.InvoiceType.val("Risturn"); },
        Proforma: function () { MyPage.DOM.InvoiceType.val("Proforma"); },
    },
    InvoiceListUpdated: {
        Regular: function () {
            let InvoiceRows = jQuery("#Regular tbody tr")
            InvoiceRows.each(function () { PutHrefToLinks(jQuery(this)); });
            jQuery("ul.nav-tabs a[href='#Regular'] span.badge").text(InvoiceRows.length.toString());
            },
        Storno: function () {
            let InvoiceRows = jQuery("#Storno tbody tr")
            InvoiceRows.each(function () { PutHrefToLinks(jQuery(this)); });
            jQuery("ul.nav-tabs a[href='#Storno'] span.badge").text(InvoiceRows.length.toString());
            },
        Advance: function () {
            let InvoiceRows = jQuery("#Advance tbody tr")
            InvoiceRows.each(function () { PutHrefToLinks(jQuery(this)); });
            jQuery("ul.nav-tabs a[href='#Advance'] span.badge").text(InvoiceRows.length.toString());
            },
        Risturn: function () {
            let InvoiceRows = jQuery("#Risturn tbody tr")
            InvoiceRows.each(function () { PutHrefToLinks(jQuery(this)); });
            jQuery("ul.nav-tabs a[href='#Risturn'] span.badge").text(InvoiceRows.length.toString());
            },
        Proforma: function () {
            let InvoiceRows = jQuery("#Proforma tbody tr")
            InvoiceRows.each(function () { PutHrefToLinks(jQuery(this)); });
            jQuery("ul.nav-tabs a[href='#Proforma'] span.badge").text(InvoiceRows.length.toString());
            },
    },
};



function PutHrefToLinks(tr)
{
    let InvoiceLink = tr.find("td:nth-child(1) a");
    InvoiceLink.attr("href", MyPage.DOM.InvoiceViewUrl + tr.attr("data-id"));
    let statusCell = tr.find("td:nth-child(2)");
	let statusName = statusCell.attr("class");
	statusCell.attr("title", statusName);
    switch (statusName)
    {
        case "Draft":
	        statusCell.html("<i class='bi-list-task text-primary'></i>");
            break;
        case "Validated":
	        statusCell.html("<i class='bi-list-task text-info'></i>");
            break;
        case "SentToCustomer":
	        statusCell.html("<i class='bi-list-task text-warning'></i>");
            break;
        case "Cancelled":
	        statusCell.html("<i class='bi-list-task text-success'></i>");
            break;
        case "Closed":
	        statusCell.html("<i class='bi-list-task text-cancelled'></i>");
            break;
        default:
	        statusCell.html("<i class='bi-list-task text-danger'></i>");
            break;
    }
    let CrmLink = tr.find("td:nth-child(3) a");
	if (CrmLink.length > 0)
	{
        let OrgsID = CrmLink.attr("data-orgs");
		if (OrgsID) CrmLink.attr("href", MyPage.DOM.CrmOrgsViewUrl + OrgsID);
        let PersID = CrmLink.attr("data-pers");
		if (PersID) CrmLink.attr("href", MyPage.DOM.CrmPersViewUrl + PersID);
	}
}



function ShowAdvancedFilter()
{
    let IsAdvancedFilter = false;
	if (jQuery("input[name='IDs']").val()) IsAdvancedFilter = true;
	if (jQuery("input[name='AccIDs']").val()) IsAdvancedFilter = true;
	if (jQuery("input[name='Curr']").val()) IsAdvancedFilter = true;
	if (jQuery("select[name='Status']").attr("data-selected")) IsAdvancedFilter = true;
	if (IsAdvancedFilter) MyPage.DOM.AdvancedSearch.slideDown();
}



function Page_Start()
{
	Nurbiosis.AttachDatepicker();
    Nurbiosis.SyncronizeDropdown("select[name='Status']");
    MyPage.DOM.AdvancedSearch = jQuery("#AdvancedSearch");
    MyPage.DOM.InvoiceType = jQuery("#SearchForm input[name='Type']");
	jQuery("#btnAdvancedSearch a").click( function() { MyPage.DOM.AdvancedSearch.slideToggle(); } );
	MyPage.DOM.InvoiceViewUrl = jQuery("#hdnInvoiceViewUrl").val() + "?InvoiceID=";
	MyPage.DOM.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.DOM.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
    MyPage.SearchForm = jQuery("#SearchForm form");
    MyPage.SearchForms.Regular = new Nurbiosis.AjaxifiedForm(MyPage.SearchForm);
    MyPage.SearchForms.Storno = new Nurbiosis.AjaxifiedForm(MyPage.SearchForm);
    MyPage.SearchForms.Advance = new Nurbiosis.AjaxifiedForm(MyPage.SearchForm);
    MyPage.SearchForms.Risturn = new Nurbiosis.AjaxifiedForm(MyPage.SearchForm);
    MyPage.SearchForms.Proforma = new Nurbiosis.AjaxifiedForm(MyPage.SearchForm);
    MyPage.SearchForms.Regular.Target("#Regular .SearchResults").Progress("#Regular progress").OnSuccess(MyPage.InvoiceListUpdated.Regular).Before(MyPage.SetInvoiceType.Regular);
    MyPage.SearchForms.Storno.Target("#Storno .SearchResults").Progress("#Storno progress").OnSuccess(MyPage.InvoiceListUpdated.Storno).Before(MyPage.SetInvoiceType.Storno);
    MyPage.SearchForms.Advance.Target("#Advance .SearchResults").Progress("#Advance progress").OnSuccess(MyPage.InvoiceListUpdated.Advance).Before(MyPage.SetInvoiceType.Advance);
    MyPage.SearchForms.Risturn.Target("#Risturn .SearchResults").Progress("#Risturn progress").OnSuccess(MyPage.InvoiceListUpdated.Risturn).Before(MyPage.SetInvoiceType.Risturn);
    MyPage.SearchForms.Proforma.Target("#Proforma .SearchResults").Progress("#Proforma progress").OnSuccess(MyPage.InvoiceListUpdated.Proforma).Before(MyPage.SetInvoiceType.Proforma);
    ShowAdvancedFilter();
    if (jQuery("#hdnFilterIsSet").val() == "true") { MyPage.Events.btnSubmit_Click(); }
}



jQuery(Page_Start);