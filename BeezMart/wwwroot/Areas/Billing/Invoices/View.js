﻿var MyPage = {};



var InvoiceViewIsUpdatedAccordingToStatus = false;



function ResetModalEditInvoiceItem()
{
	jQuery("#ModalEditInvoiceItem input[name='ID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='ProductCatalogItemID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='ProductCatalogCategoryID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='ProductOrService']").val("P");
	jQuery("#ModalEditInvoiceItem input[name='SalesOrderItemID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='DeliveryDocumentID']").val("");
	jQuery("#ModalEditInvoiceItem input[name='DeliveryDocumentItemID']").val("");
	jQuery("#ModalEditInvoiceItem textarea[name='Name']").val("");
	jQuery("#ModalEditInvoiceItem input[name='Quantity']").val("1");
	jQuery("#ModalEditInvoiceItem input[name='UnitMeasure']").val(jQuery("#ModalEditInvoiceItem input[name='UnitMeasure']").attr("data-default"));
	jQuery("#ModalEditInvoiceItem input[name='UnitPrice']").val("");
}
function ComputePaidPercentage()
{
    let TotalDue = Number(jQuery("#InvoiceItemsList tfoot tr").attr("data-value"));
	let PaidAmount = Number(jQuery("#Payments tfoot tr td:first-child").attr("data-value"));
	let PaidPercentage = PaidAmount / TotalDue * 100;
	jQuery("#Payments div.progress-bar").width(PaidPercentage + "%");
	let PaidPercentageText = PaidPercentage.toFixed(2).toString();
	jQuery("#Payments div.progress-bar").text(PaidPercentageText + "% paid");
}
function ResetPaymentEditor()
{
	let PaymentDateCtl = jQuery("#ModalEditPayment input[name='PaymentDate']"); PaymentDateCtl.val(PaymentDateCtl.attr("data-default"));
	jQuery("#ModalEditPayment input[name='ID']").val("");
	jQuery("#ModalEditPayment input[name='PaymentCheckID']").val("");
    let CurrencyCtl = jQuery("#ModalEditPayment input[name='Currency']"); CurrencyCtl.val(jQuery("#Currency").text());
    jQuery("#ModalEditPayment input[name='Amount']").val(jQuery("#Payments tfoot tr td:first-child").attr("data-remaining"));
	jQuery("#ModalEditPayment textarea[name='Comments']").val("");
}
function DoTasksByInvoiceStatus()
{
	if (InvoiceViewIsUpdatedAccordingToStatus) return;
    let InvoiceStatus = jQuery("#Status").attr("data-status");
	switch(InvoiceStatus)
	{
		case "Draft":
			jQuery("#Status button:first-child").html("<b>Draft</b>, still working on it");
			jQuery("#Status li[data-value='Draft']").addClass("disabled");
			jQuery("#Status li[data-value='Validated']").removeClass("disabled");
			jQuery("#Status li[data-value='SentToCustomer']").addClass("disabled");
			jQuery("#Status li[data-value='Cancelled']").addClass("disabled");
			jQuery("#Status li[data-value='Closed']").addClass("disabled");
            jQuery("#btnEdit1").show();
            jQuery("#btnEdit2").show();
			jQuery("#btnPrint").hide();
			break;
		case "Validated":
			jQuery("#Status button:first-child").html("<b>Validated</b>, ready to print and send");
			jQuery("#Status li[data-value='Draft']").removeClass("disabled");
			jQuery("#Status li[data-value='Validated']").addClass("disabled");
			jQuery("#Status li[data-value='SentToCustomer']").removeClass("disabled");
			jQuery("#Status li[data-value='Cancelled']").addClass("disabled");
			jQuery("#Status li[data-value='Closed']").addClass("disabled");
			jQuery("#btnEdit1").show();
			jQuery("#btnEdit2").show();
			jQuery("#btnPrint").removeClass("success").addClass("warning").show();
			break;
		case "SentToCustomer":
			jQuery("#Status button:first-child").html("<b>Pending</b>, sent to customer");
			jQuery("#Status li[data-value='Draft']").addClass("disabled");
			jQuery("#Status li[data-value='Validated']").addClass("disabled");
			jQuery("#Status li[data-value='SentToCustomer']").addClass("disabled");
			jQuery("#Status li[data-value='Cancelled']").removeClass("disabled");
			jQuery("#Status li[data-value='Closed']").removeClass("disabled");
			jQuery("#btnEdit1").hide();
			jQuery("#btnEdit2").hide();
			jQuery("#btnPrint").removeClass("warning").addClass("success").show();
			jQuery("#btnCloseInvoice1").show();
			jQuery("#btnCloseInvoice2").show();
			break;
		case "Cancelled":
			jQuery("#Status button:first-child").html("<b>Cancelled</b>, null and void");
			jQuery("#Status li[data-value='Draft']").addClass("disabled");
			jQuery("#Status li[data-value='Validated']").addClass("disabled");
			jQuery("#Status li[data-value='SentToCustomer']").removeClass("disabled");
			jQuery("#Status li[data-value='Cancelled']").addClass("disabled");
			jQuery("#Status li[data-value='Closed']").removeClass("disabled");
			jQuery("#btnEdit1").hide();
			jQuery("#btnEdit2").hide();
			jQuery("#btnPrint").removeClass("warning").addClass("success").show();
			break;
		case "Closed":
			jQuery("#Status button:first-child").html("<b>Closed</b>, paid and archived");
			jQuery("#Status li[data-value='Draft']").addClass("disabled");
			jQuery("#Status li[data-value='Validated']").addClass("disabled");
			jQuery("#Status li[data-value='SentToCustomer']").removeClass("disabled");
			jQuery("#Status li[data-value='Cancelled']").removeClass("disabled");
			jQuery("#Status li[data-value='Closed']").addClass("disabled");
			jQuery("#btnEdit1").hide();
			jQuery("#btnEdit2").hide();
			jQuery("#btnPrint").removeClass("warning").addClass("success").show();
			jQuery("#btnCloseInvoice1").hide();
			jQuery("#btnCloseInvoice2").hide();
			break;
	}
	InvoiceViewIsUpdatedAccordingToStatus = true;
}



function Payment_Processed()
{
	jQuery("#ModalPaymentMessage").modal("show");
    jQuery("#Status").attr("data-status", jQuery("#hdnPaymentSave_InvoiceStatus").val());
    InvoiceViewIsUpdatedAccordingToStatus = false;
    DoTasksByInvoiceStatus();
	MyPage.Payments.load();
}
function Payment_Deleted()
{
    jQuery("#Status").attr("data-status", jQuery("#hdnPaymentDelete_InvoiceStatus").val());
    InvoiceViewIsUpdatedAccordingToStatus = false;
    DoTasksByInvoiceStatus();
	MyPage.Payments.load();
}
function Payment_Loaded()
{
	let dateControl = jQuery("#ModalEditPayment input[name='PaymentDate']");
	let dateString = dateControl.val();
	dateString = dateString.substr(0, dateString.indexOf("T"));
	MyPage.PaymentEditorCalendar.SetDate(dateString);
}



function ChangeStatus_Processed()
{
	let InvoiceStatus = jQuery("#hdnChangeStatus_InvoiceStatus").val();
	if (jQuery("#Status").attr("data-status") !== InvoiceStatus)
	{
		jQuery("#Status").attr("data-status", InvoiceStatus);
		InvoiceViewIsUpdatedAccordingToStatus = false;
	}
	DoTasksByInvoiceStatus();
}



function InvoiceItems_Updated()
{
    let invoiceTotalDue = jQuery("#InvoiceItemsList tfoot tr").attr("data-value");
    let invoiceTotalDueFormatted = jQuery("#InvoiceItemsList tfoot tr").attr("data-value-formatted");
	jQuery("#TotalDue strong").text(invoiceTotalDueFormatted);
	jQuery("#TotalDueEssentials").text(invoiceTotalDueFormatted);
	let InvoiceStatus = jQuery("#InvoiceItemsList table.table").attr("data-invoice-status");
	if (jQuery("#Status").attr("data-status") !== InvoiceStatus)
	{
		jQuery("#Status").attr("data-status", InvoiceStatus);
		InvoiceViewIsUpdatedAccordingToStatus = false;
	}
	DoTasksByInvoiceStatus();
	ComputePaidPercentage();
	if (jQuery("#hdnInvoiceIsEditable").val() === "true")
	{
		jQuery("#InvoiceItemsList tbody tr").each(
				function()
				{
					let invoiceItemID = jQuery(this).attr("data-record");
					jQuery(this).find("td:nth-child(2) a").click(function() { btnEditInvoiceItem_Click(invoiceItemID); });
					jQuery(this).find("td:nth-child(8) a").click(function() { btnDeleteInvoiceItem_Click(invoiceItemID); });
				}
			);
	}
	else
	{
		jQuery("#InvoiceItemsList tbody tr td:nth-child(2)").each(function() { jQuery(this).html(jQuery(this).text()); });
		jQuery("#InvoiceItemsList tbody tr td:nth-child(8) a").hide();
        jQuery("#InvoiceItems .card-footer button").hide();
	}
}
function InvoicePayments_Updated()
{
	DoTasksByInvoiceStatus();
	ComputePaidPercentage();
	ResetPaymentEditor();
	jQuery("#Payments tbody tr").each(
			function ()
			{
				let paymentID = jQuery(this).attr("data-record");
				jQuery(this).find("td:nth-child(1) a").click(function() { btnEditPayment_Click(paymentID); });
				jQuery(this).find("td:nth-child(4) a").click(function() { btnDeletePayment_Click(paymentID); });
			}
		);
}
function ModalEditInvoiceItem_Shown()
{
	jQuery("#ModalEditInvoiceItem textarea[name='Name']").focus();
}
function btnSaveInvoiceItem_Click()
{
	let ValidationErrors = "";
	if (jQuery("#ModalEditInvoiceItem textarea[name='Name']").val() === "") ValidationErrors += "- Product or service name.<br />";
	if (jQuery("#ModalEditInvoiceItem input[name='Quantity']").val() === "") ValidationErrors += "- Quantity; must be an integer number.<br />";
	if (jQuery("#ModalEditInvoiceItem input[name='UnitMeasure']").val() === "") ValidationErrors += "- Unit measure.<br />";
	if (jQuery("#ModalEditInvoiceItem input[name='UnitPrice']").val() === "") ValidationErrors += "- Unit price; must be a decimal number.<br />";
	if (ValidationErrors.length > 0)
	{
		ValidationErrors = "Error while validating invoice item fields:<br />" + ValidationErrors;
		toastr.warning(ValidationErrors, "Validation");
		return;
	}
	let InvoiceID = document.getElementById("hdnInvoiceID").value;
	jQuery("#ModalEditInvoiceItem input[name='InvoiceID']").val(InvoiceID);
	jQuery("#ModalEditInvoiceItem form").submit();
}
function btnEditInvoiceItem_Click(InvoiceItemID)
{
	if (jQuery("#hdnInvoiceIsEditable").val() !== "true")
	{
		toastr.warning("This invoice can no longer be editted. Its current status no longer allows editting.", "Validation");
		return;
	}
	jQuery("#ModalEditInvoiceItem").modal("show");
	MyPage.InvoiceItemFields.Source(MyPage.InvoiceItemFieldsSourceURL + InvoiceItemID).load();
}
function btnDeleteInvoiceItem_Click(InvoiceItemID)
{
	if (jQuery("#hdnInvoiceIsEditable").val() !== "true")
	{
		toastr.warning("This invoice can no longer be editted. Its current status no longer allows editting.", "Validation");
		return;
	}
	if (!confirm("Are you sure you want to delete the item?\n\nYou cannot undo this operation.")) return;
	jQuery("#frmDelInvoiceItem input[name='ItemID']").val(InvoiceItemID);
	jQuery("#frmDelInvoiceItem").submit();
}
function btnDeletePayment_Click(PaymentID)
{
	if (!confirm("Are you sure you want to delete the payment?\n\nYou won't be able to undo this operation.")) return;
	jQuery("#frmDeletePayment input[name='PaymentID']").val(PaymentID);
	jQuery("#PaymentDeleteMessage").html("");
	jQuery("#ModalPaymentDelete").modal("show");
	jQuery("#frmDeletePayment").submit();
}
function btnEditPayment_Click(PaymentID)
{
	MyPage.PaymentEditDialog.modal("show");
	MyPage.PaymentEditFields.Source(MyPage.PaymentEditFieldsDataSourceURL + PaymentID).load();
}
function btnCloseInvoice_Click()
{
	jQuery("#ModalEditPayment").modal("hide");
	jQuery("#ModalPaymentMessage").modal("hide");
	let statusLink_Closed = jQuery("#Status li[data-value='Closed'] a");
	btnChangeStatus_Click(statusLink_Closed);
}
function btnChangeStatus_Click(statusLink)
{
	if (statusLink.parent().hasClass("disabled")) return;
	let newStatus = statusLink.parent().attr("data-value");
	jQuery("#frmChangeStatus input[name='NewStatus']").val(newStatus);
	jQuery("#ModalChangeStatusMessage").modal("show");
	jQuery("#frmChangeStatus").submit();
}
function btnPrint_Click()
{
	let InvoiceStatus = jQuery("#Status").attr("data-status");
	if (InvoiceStatus === "Validated")
	{
		jQuery("#ModalPrintAndLock").modal("show");
	}
	else
	{
		window.location = jQuery("#ModalPrintAndLock .modal-footer a").attr("href");
	}
}



function Page_Start()
{
	MyPage.InvoiceItems = new Nurbiosis.AjaxLoadingElement("#InvoiceItemsList").Progress("#InvoiceItems progress").OnSuccess(InvoiceItems_Updated).load();
	if (jQuery("#Payments").length > 0)
    {
        MyPage.Payments = new Nurbiosis.AjaxLoadingElement("#PaymentsList").Progress("#Payments progress").OnSuccess(InvoicePayments_Updated).load();
	}
	Nurbiosis.AjaxifyForm("#ModalEditInvoiceItem form").Target("#InvoiceItemsList").Progress("#InvoiceItems progress").OnSuccess(InvoiceItems_Updated);
	Nurbiosis.AjaxifyForm("#frmDelInvoiceItem").Target("#InvoiceItemsList").Progress("#InvoiceItems progress").OnSuccess(InvoiceItems_Updated);
	Nurbiosis.AjaxifyForm("#ModalEditPayment form").Target("#AddPaymentMessage").Progress("#ModalEditPayment progress").OnSuccess(Payment_Processed);
	Nurbiosis.AjaxifyForm("#frmDeletePayment").Target("#PaymentDeleteMessage").Progress("#ModalPaymentDelete progress").OnSuccess(Payment_Deleted);
	Nurbiosis.AjaxifyForm("#frmChangeStatus").Target("#ChangeStatusMessage").Progress("#ModalChangeStatusMessage progress").OnSuccess(ChangeStatus_Processed);
	jQuery("#ModalEditInvoiceItem").on("shown.bs.modal", ModalEditInvoiceItem_Shown);
	jQuery("#ModalEditInvoiceItem").on("hidden.bs.modal", ResetModalEditInvoiceItem);
	jQuery("#Status li a").click( function() { btnChangeStatus_Click(jQuery(this)); } );
	MyPage.InvoiceItemFields = new Nurbiosis.AjaxLoadingElement("#ModalEditInvoiceItem form").Progress("#ModalEditInvoiceItem progress").ResponseType("json");
	MyPage.InvoiceItemFieldsSourceURL = jQuery("#ModalEditInvoiceItem form").attr("data-source");
	MyPage.InvoiceItemFieldsSourceURL = MyPage.InvoiceItemFieldsSourceURL.substring(0, MyPage.InvoiceItemFieldsSourceURL.indexOf("ItemID=") + 7);
	MyPage.PaymentEditDialog = jQuery("#ModalEditPayment");
	MyPage.PaymentEditFields = new Nurbiosis.AjaxLoadingElement("#ModalEditPayment form").Progress("#ModalEditPayment progress").OnSuccess(Payment_Loaded).ResponseType("json");
	MyPage.PaymentEditFieldsDataSourceURL = jQuery("#ModalEditPayment form").attr("data-source");
	MyPage.PaymentEditFieldsDataSourceURL = MyPage.PaymentEditFieldsDataSourceURL.substring(0, MyPage.PaymentEditFieldsDataSourceURL.indexOf("PaymentID=") + 10);
	MyPage.PaymentEditorCalendar = new Nurbiosis.InitializeCalendar("#PaymentDateCalendar").Target("#ModalEditPayment input[name='PaymentDate']").go();
}



jQuery(Page_Start);