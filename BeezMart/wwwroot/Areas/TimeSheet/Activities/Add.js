


var MyPage = {};



function LookupProject_Updating()
{
	jQuery("#SearchResultProjects").html("");
	jQuery("#ModalLookupProject").modal("hide");
	jQuery("#ModalProjects").modal("show");
}
function LookupProjects_Updated()
{
	jQuery("#ModalProjects table tbody tr").click(
			function()
			{
                let projectID = jQuery(this).attr("data-value");
                let projectTitle = jQuery(this).find("td:first-child a").text();
                let customerCell = jQuery(this).find("td:nth-child(2)");
                let customerName = customerCell.text();
                let endCustomer = customerCell.attr("title");
                let organizationID = customerCell.attr("data-OrganizationID");
                let personID = customerCell.attr("data-PersonID");
                jQuery("input[name='Activity.ProjectID']").val(projectID);
                jQuery("input[name='Activity.ProjectTitle']").val(projectTitle);
                jQuery("input[name='Activity.CustomerName']").val(customerName);
                jQuery("input[name='Activity.EndCustomer']").val(endCustomer);
                jQuery("input[name='Activity.OrganizationID']").val(organizationID);
                jQuery("input[name='Activity.PersonID']").val(personID);
				jQuery("#ModalProjects").modal("hide");
			}
		);
}



function LookupCustomer_Updating()
{
	jQuery("#SearchResultCustomers").html("");
	jQuery("#ModalLookupCustomer").modal("hide");
	jQuery("#ModalCustomers").modal("show");
}
function LookupCustomers_Updated()
{
	jQuery("#ModalCustomers table tbody td:first-child a").click(
			function()
			{
                jQuery("input[name='Activity.OrganizationID']").val("");
                jQuery("input[name='Activity.PersonID']").val("");
                let customerID = jQuery(this).attr("data-value");
                let customerName = jQuery(this).text();
                let customerType = document.getElementById("hdnCustomerType").value;
                jQuery("input[name='Activity." + customerType + "ID']").val(customerID);
                jQuery("input[name='Activity.CustomerName']").val(customerName);
                jQuery("input[name='Activity.EndCustomer']").val(customerName);
				jQuery("#ModalCustomers").modal("hide");
			}
		);
}



function btnCustomersSearchAgain_Click()
{
    jQuery("#ModalLookupCustomer").modal("show");
    jQuery("#ModalCustomers").modal("hide");
}
function btnProjectsSearchAgain_Click() {
    jQuery("#ModalLookupProject").modal("show");
    jQuery("#ModalProjects").modal("hide");
}



function Page_Start()
{
    Nurbiosis.AttachDatepicker();
    Nurbiosis.AjaxifyForm("#frmLookupProjects").Before(LookupProject_Updating).Target("#SearchResultProjects").Progress("#ModalProjects progress").OnSuccess(LookupProjects_Updated);
    Nurbiosis.AjaxifyForm("#frmLookupOrgs").Before(LookupCustomer_Updating).Target("#SearchResultCustomers").Progress("#ModalCustomers progress").OnSuccess(LookupCustomers_Updated);
    Nurbiosis.AjaxifyForm("#frmLookupPers").Before(LookupCustomer_Updating).Target("#SearchResultCustomers").Progress("#ModalCustomers progress").OnSuccess(LookupCustomers_Updated);
    jQuery("#Activity_Title").focus();
    jQuery("#ModalLookupCustomer").on("shown.bs.modal", function() { jQuery("#ModalLookupCustomer input[name='OrgsNamePattern']").focus(); });
    jQuery("#ModalLookupProject").on("shown.bs.modal", function () { jQuery("#ModalLookupProject input[name='NamePattern']").focus(); });
    Nurbiosis.EnableTinyMCE("#Details textarea");
}



jQuery(Page_Start);