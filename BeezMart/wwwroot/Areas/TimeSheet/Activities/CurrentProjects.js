


var MyPage = {};



function ProjectsList_Updated()
{
    function PutHrefToLinks(tr)
    {
        let ProjectLink = tr.find("td:nth-child(1) a");
	    ProjectLink.attr("href", MyPage.ProjectViewUrl + tr.attr("data-id"));
        let CrmLink = tr.find("td:nth-child(2) a");
	    if (CrmLink.length > 0)
	    {
            let OrgsID = CrmLink.attr("data-orgs")
		    if (OrgsID) CrmLink.attr("href", MyPage.CrmOrgsViewUrl + OrgsID);
            let PersID = CrmLink.attr("data-pers")
		    if (PersID) CrmLink.attr("href", MyPage.CrmPersViewUrl + PersID);
	    }
    }
    let tableRows = jQuery("#Projects tbody tr")
    tableRows.each( function() { PutHrefToLinks(jQuery(this)); } );
}



function Page_Start()
{
	MyPage.ProjectViewUrl = jQuery("#hdnProjectViewUrl").val() + "?ID=";
	MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	ProjectsList_Updated();
}



jQuery(Page_Start);