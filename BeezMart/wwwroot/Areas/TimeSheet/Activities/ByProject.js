


var MyPage = {};



function ActivitiesList_Updated() {
    function PutHrefToLinks(tr) {
        let ActivityLink = tr.find("td:nth-child(1) a");
        ActivityLink.attr("href", MyPage.ActivityViewUrl + tr.attr("data-id"));
    }
    let tableRows = jQuery("#Activities tbody tr")
    tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
}



function Page_Start() {
    MyPage.ActivityViewUrl = jQuery("#hdnActivityViewUrl").val() + "?ID=";
    ActivitiesList_Updated();
}



jQuery(Page_Start);