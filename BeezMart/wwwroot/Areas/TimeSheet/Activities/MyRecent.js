


var MyPage = {};



function ActivitiesList_Updated() {
    function PutHrefToLinks(tr) {
        let ActivityLink = tr.find("td:nth-child(1) a");
        ActivityLink.attr("href", MyPage.ActivityViewUrl + tr.attr("data-id"));
        let ProjectLink = tr.find("td:nth-child(2) a");
        ProjectLink.attr("href", MyPage.ProjectViewUrl + ProjectLink.attr("data-id"));
        let CrmLink = tr.find("td:nth-child(3) a");
        if (CrmLink.length > 0) {
            let OrgsID = CrmLink.attr("data-orgs")
            if (OrgsID) CrmLink.attr("href", MyPage.CrmOrgsViewUrl + OrgsID);
            let PersID = CrmLink.attr("data-pers")
            if (PersID) CrmLink.attr("href", MyPage.CrmPersViewUrl + PersID);
        }
    }
    let tableRows = jQuery("#Activities tbody tr")
    tableRows.each(function () { PutHrefToLinks(jQuery(this)); });
}



function Page_Start() {
    MyPage.ActivityViewUrl = jQuery("#hdnActivityViewUrl").val() + "?ID=";
    MyPage.ProjectViewUrl = jQuery("#hdnProjectViewUrl").val() + "?ID=";
    MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
    MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
    ActivitiesList_Updated();
}



jQuery(Page_Start);