


var MyPage = {};



function LookupCustomer_Updating()
{
    jQuery("#SearchResultCustomers").html("");
    jQuery("#ModalLookupCustomer").modal("hide");
    jQuery("#ModalCustomers").modal("show");
}
function LookupCustomers_Updated()
{
    jQuery("#ModalCustomers table tbody td:first-child a").click(
        function ()
        {
            jQuery("input[name='Project.OrganizationID']").val("");
            jQuery("input[name='Project.PersonID']").val("");
            let customerID = jQuery(this).attr("data-value");
            let customerName = jQuery(this).text();
            let customerType = document.getElementById("hdnCustomerType").value;
            jQuery("input[name='Project." + customerType + "ID']").val(customerID);
            jQuery("input[name='Project.CustomerName']").val(customerName);
            jQuery("#ModalCustomers").modal("hide");
        }
    );
}
function btnCustomersSearchAgain_Click()
{
    jQuery("#ModalLookupCustomer").modal("show");
    jQuery("#ModalCustomers").modal("hide");
}



function Page_Start()
{
    Nurbiosis.AttachDatepicker();
    Nurbiosis.SyncronizeDropdown();
    Nurbiosis.AjaxifyForm("#frmLookupOrgs").Before(LookupCustomer_Updating).Target("#SearchResultCustomers").Progress("#ModalCustomers progress").OnSuccess(LookupCustomers_Updated);
    Nurbiosis.AjaxifyForm("#frmLookupPers").Before(LookupCustomer_Updating).Target("#SearchResultCustomers").Progress("#ModalCustomers progress").OnSuccess(LookupCustomers_Updated);
    jQuery("#ModalLookupCustomer").on("shown.bs.modal", function () { jQuery("#ModalLookupCustomer input[name='OrgsNamePattern']").focus(); });
    Nurbiosis.EnableTinyMCE();
}



jQuery(Page_Start);