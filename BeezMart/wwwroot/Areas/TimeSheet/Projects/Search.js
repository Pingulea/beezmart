﻿


var MyPage = {
	ProjectViewUrl: null,
	CrmOrgsViewUrl: null,
	CrmPersViewUrl: null,
	SearchForm: null,
	Export: function() {
		let exportUrl = "_Search_Export?" + MyPage.SearchForm.serialize();
		window.open(exportUrl);
	},
	DirectLink: function() {
		let pageUrl = window.location.href;
		let questionMarkIndex = pageUrl.indexOf("?");
		if (questionMarkIndex > 0) pageUrl = pageUrl.substring(0, questionMarkIndex);
		let directSearchLink = pageUrl + "?" + MyPage.SearchForm.serialize();
		Nurbiosis.CopyTextToClipboard(directSearchLink);
		let btnDirectLink = jQuery("#btnDirectLink");
		btnDirectLink.attr("title", "Copied to clipboard");
		btnDirectLink.removeClass("btn-outline-secondary").addClass("btn-success");
		function hideTooltip () {
			btnDirectLink.tooltip("hide");
			btnDirectLink.removeClass("btn-success").addClass("btn-outline-secondary");
		}
		let tooltipOptions = {
			container: "body",
			trigger: "manual"
		};
		btnDirectLink.tooltip(tooltipOptions).tooltip("show");
		setTimeout(hideTooltip, 3000);
	}
};



function btnChangeFilter_Click()
{
	jQuery("#SearchResults .alert").alert("close");
	jQuery("#ChangeFilter").slideUp(300, function() { jQuery("#SearchForm").slideDown(600); });
}



function SearchResults_Loaded()
{
    function PutHrefToLinks(tr)
    {
        let ProjectLink = tr.find("td:nth-child(1) a");
	    ProjectLink.attr("href", MyPage.ProjectViewUrl + tr.attr("data-id"));
        let CrmLink = tr.find("td:nth-child(2) a");
	    if (CrmLink.length > 0)
	    {
            let OrgsID = CrmLink.attr("data-orgs");
		    if (OrgsID) CrmLink.attr("href", MyPage.CrmOrgsViewUrl + OrgsID);
            let PersID = CrmLink.attr("data-pers");
		    if (PersID) CrmLink.attr("href", MyPage.CrmPersViewUrl + PersID);
	    }
    }
    let tableRows = jQuery("#SearchResults tbody tr");
	tableRows.each( function() { PutHrefToLinks(jQuery(this)); } );
	jQuery("#SearchForm").slideUp(600, function() { jQuery("#ChangeFilter").slideDown(300); });
}



function ShowAdvancedFilter()
{
    let IsAdvancedFilter = false;
	if (jQuery("input[name='IDs']").val()) IsAdvancedFilter = true;
    if (jQuery("input[name='ProjectManager']").val()) IsAdvancedFilter = true;
    if (jQuery("input[name='ContactDetails']").val()) IsAdvancedFilter = true;
    if (jQuery("select[name='Category']").attr("data-selected")) IsAdvancedFilter = true;
    if (jQuery("select[name='Status']").attr("data-selected")) IsAdvancedFilter = true;
    if (IsAdvancedFilter) MyPage.AdvancedSearch.removeClass("collapse");
}



function Page_Start()
{
	Nurbiosis.AttachDatepicker();
	MyPage.AdvancedSearch = jQuery("#AdvancedSearch");
	jQuery("#btnAdvancedSearch a").click( function() { MyPage.AdvancedSearch.slideToggle(); } );
	MyPage.ProjectViewUrl = jQuery("#hdnProjectViewUrl").val() + "?ID=";
	MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	MyPage.SearchForm = jQuery("#SearchForm form");
	Nurbiosis.AjaxifyForm(MyPage.SearchForm).Target("#SearchResults").Progress("progress").OnSuccess(SearchResults_Loaded);
	ShowAdvancedFilter();
	if (jQuery("#hdnFilterIsSet").val() == "true") MyPage.SearchForm.submit();
}



jQuery(Page_Start);