﻿


var StatesAndProvinces = new Array();



function btnSubmit_Click()
{
	if (document.getElementById("OrgSite_City").value === "")
	{
		toastr.warning("The address is missing the required field CITY.<br />An address needs a City to be saved.", "Validation");
		return;
	}
	jQuery("div.Submit progress").show();
	document.forms["frmEditOrgSite"].submit();
}



function ddlCountry_Change(ddlCountry)
{
	GetStatesProvinces(ddlCountry.options[ddlCountry.selectedIndex].value, StatesAndProvinces);
}



function EnableStateProvinceAutocomplete()
{
	let ddlCountry = document.getElementById("OrgSite_Country");
	GetStatesProvinces(ddlCountry.options[ddlCountry.selectedIndex].value, StatesAndProvinces, "#OrgSite_StateProvince");
}
function GetStatesProvinces(CountryID, StatesAndProvincesArray, AutocompleteControlSelector)
{
	let PopulateArray = function (data) {
		while (StatesAndProvincesArray.length > 0) {
			StatesAndProvincesArray.pop();
		}
		for (i = 0; i < data.length; i++) {
			StatesAndProvincesArray.push(data[i].name);
		}
		if (AutocompleteControlSelector) {
			let AutoCompleteSettings =
				{
					source: StatesAndProvincesArray,
					minLength: 1,
					autoFocus: true
				};
			jQuery(AutocompleteControlSelector).autocomplete(AutoCompleteSettings);
		}
	};
	let ActionUrl = document.getElementById("hdnStatesProvincesUrl").value;
	jQuery.getJSON(ActionUrl + "?CountryID=" + CountryID, PopulateArray);
}




function Page_Start()
{
	Nurbiosis.SyncronizeDropdown();
	EnableStateProvinceAutocomplete();
	jQuery("input[name='OrgSite.City']").focus();
}



jQuery(Page_Start);