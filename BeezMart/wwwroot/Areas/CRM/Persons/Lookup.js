﻿


var CrmPersViewUrl = "";



function PutHrefToLinks(tr)
{
	var CrmLink = tr.find("td:nth-child(2) a");
	CrmLink.attr("href", CrmPersViewUrl + tr.attr("data-id"));
}



function Page_Start()
{
	CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	jQuery("#SearchResults tbody tr").each( function() { PutHrefToLinks(jQuery(this)); } );
}



jQuery(Page_Start);