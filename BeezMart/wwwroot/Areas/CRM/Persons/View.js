﻿var MyPage = {};



function btnPickAddress_Click(SiteAddressType)
{
	jQuery("#ModalPickAddress input[name='SiteAddressType']").val(SiteAddressType);
	jQuery("#ModalPickAddress").modal("show");
	function AllAddresses_Loaded()
	{
		jQuery("#ModalPickAddress .modal-body tbody a").click(function() { PickAddress(jQuery(this).parents("tr").attr("data-value")); });
	}
	if (jQuery("#ModalPickAddress .modal-body").html().length < 10)
	{
		MyPage.AddressPicker = new Nurbiosis.AjaxLoadingElement("#ModalPickAddress .modal-body").Progress("#ModalPickAddress progress").OnSuccess(AllAddresses_Loaded).load();
	}
}
function PickAddress(SiteAddressID)
{
	jQuery("#ModalPickAddress input[name='SiteAddressID']").val(SiteAddressID);
	MyPage.PickAddressForm.submit();
}
function Address_Updated(AddressSummaryHtml)
{
	var AddressContainerSelector = "#Address_" + jQuery("#ModalPickAddress input[name='SiteAddressType']").val();
	jQuery(AddressContainerSelector).html(AddressSummaryHtml);
	jQuery("#ModalPickAddress").modal("hide");
}



function ddlCategory_Change(ddlCategory, FieldListID)
{
	var radioButtonsSelector = "#" + FieldListID + " input[type='radio']";
	jQuery(radioButtonsSelector).prop("checked", false);
	var categoryName = (ddlCategory.selectedIndex === 0) ? "" : ddlCategory.options[ddlCategory.selectedIndex].text;
	var fieldsSelector = "#" + FieldListID + " li";
	jQuery(fieldsSelector).each(
			function()
			{
				if (jQuery(this).attr("data-val-category") === categoryName)
				{
					jQuery(this).show();
				}
				else
				{
					jQuery(this).hide();
				}
			}
		);
}



function ModalEditContact_Reset()
{
	jQuery("#hdnContactDetailID").val("");
	jQuery("#txtContactDetail").val("");
	jQuery("#txtContactComments").val("");
	jQuery("#ddlContactType").val("Phone");
	jQuery("#ddlContactStatus").val("Unconfirmed");
}
function ModalEditFlag_Reset()
{
	jQuery("#frmFlagDetail input[name='ID']").val("");
	jQuery("#txtFlagComments").val("");
	var ddlFlagCategory = document.getElementById("ddlFlagCategory");
	ddlFlagCategory.selectedIndex = 0;
	ddlCategory_Change(ddlFlagCategory, "FlagList");
}
function ModalEditInteger_Reset()
{
	jQuery("#frmIntegerDetail input[name='ID']").val("");
	jQuery("#txtIntegerValue").val("");
	var ddlIntegerCategory = document.getElementById("ddlIntegerCategory");
	ddlIntegerCategory.selectedIndex = 0;
	ddlCategory_Change(ddlIntegerCategory, "IntegerList");
}
function ModalEditString_Reset()
{
	jQuery("#frmStringDetail input[name='ID']").val("");
	jQuery("#txtStringValue").val("");
	var ddlStringCategory = document.getElementById("ddlStringCategory");
	ddlStringCategory.selectedIndex = 0;
	ddlCategory_Change(ddlStringCategory, "StringList");
}
function ModalEditRelationOrgsPers_Reset()
{
	jQuery("#txtJobTitle").val(jQuery("#txtJobTitle").attr("data-default"));
	MyPage.ddlJobCategory.selectedIndex = 0;
	jQuery("#JobTypesList input[type='radio']").prop("checked", false);
	ddlCategory_Change(MyPage.ddlJobCategory, "JobTypesList");
}



function Contacts_Updated()
{
	jQuery("#ContactsListContainer tbody tr").each(
			function()
			{
				var tr = jQuery(this);
				var contactStatus = tr.children().eq(1).attr("title");
				var contactType = tr.attr("data-type");
				var contactComment = tr.children().eq(0).attr("data-comment");
				tr.children().eq(0).attr("title", contactType.toUpperCase() + ", " + contactStatus.toLocaleUpperCase() + ": " + contactComment).tooltip({container: "body"});
				tr.find("td:nth-child(1) a").click(
						function()
						{
							jQuery("#hdnContactDetailID").val(tr.attr("data-record"));
							jQuery("#txtContactDetail").val(tr.children().eq(1).text());
							jQuery("#ddlContactType").val(contactType);
							jQuery("#ddlContactStatus").val(contactStatus);
							jQuery("#txtContactComments").val(contactComment);
							jQuery("#ModalEditContact").modal("show");
						}
					);
				tr.find("td:nth-child(3) a").click(
						function()
						{
							if (!window.confirm("Are you sure you want to delete this contact detail?\n\nYou will not be able to undo this operation."))
							{
								return;
							}
							var contactID = tr.attr("data-record");
							jQuery("#frmContactDelete input[name='ContactID']").val(contactID);
							jQuery("#frmContactDelete").trigger("submit");
						}
					);
			}
		);
}
function Flags_Updated()
{
	var ddlFlagCategory = document.getElementById("ddlFlagCategory");
	jQuery("#FlagsListContainer tbody tr").each(
			function()
			{
				var tr = jQuery(this);
				var flagComments = tr.children().eq(1).attr("title");
				tr.find("td:nth-child(2) a").click(
						function()
						{
							var fieldCategory = tr.children().eq(0).text();
							var categoryIndex = 0;
							for (var i = 0; i < ddlFlagCategory.options.length; i++)
							{
								if (ddlFlagCategory.options[i].text === fieldCategory)
								{
									categoryIndex = i;
								}
							}
							ddlFlagCategory.selectedIndex = categoryIndex;
							ddlCategory_Change(ddlFlagCategory, "FlagList");
							var flagType = tr.attr("data-type");
							jQuery("#FlagList input[value='" + flagType + "']").prop("checked", true);
							jQuery("#frmFlagDetail input[name='ID']").val(tr.attr("data-record"));
							jQuery("#txtFlagComments").val(flagComments);
							jQuery("#ModalEditFlag").modal("show");
						}
					);
				tr.find("td:nth-child(3) a").click(
						function()
						{
							if (!window.confirm("Are you sure you want to delete this flag?\n\nYou will not be able to undo this operation."))
							{
								return;
							}
							var recordID = tr.attr("data-record");
							jQuery("#frmFlagDelete input[name='RecordID']").val(recordID);
							jQuery("#frmFlagDelete").trigger("submit");
						}
					);
			}
		);
}
function Integers_Updated()
{
	var ddlIntegerCategory = document.getElementById("ddlIntegerCategory");
	jQuery("#IntegersListContainer tbody tr").each(
			function()
			{
				var tr = jQuery(this);
				var fieldValue = tr.children().eq(2).text().replace(",","");
				tr.find("td:nth-child(2) a").click(
						function()
						{
							var fieldCategory = tr.children().eq(0).text();
							var categoryIndex = 0;
							for (var i = 0; i < ddlIntegerCategory.options.length; i++)
							{
								if (ddlIntegerCategory.options[i].text === fieldCategory)
								{
									categoryIndex = i;
								}
							}
							ddlIntegerCategory.selectedIndex = categoryIndex;
							ddlCategory_Change(ddlIntegerCategory, "IntegerList");
							var fieldType = tr.attr("data-type");
							jQuery("#IntegerList input[value='" + fieldType + "']").prop("checked", true);
							jQuery("#frmIntegerDetail input[name='ID']").val(tr.attr("data-record"));
							jQuery("#txtIntegerValue").val(fieldValue);
							jQuery("#ModalEditInteger").modal("show");
						}
					);
				tr.find("td:nth-child(4) a").click(
						function()
						{
							if (!window.confirm("Are you sure you want to delete this number attribute?\n\nYou will not be able to undo this operation."))
							{
								return;
							}
							var recordID = tr.attr("data-record");
							jQuery("#frmIntegerDelete input[name='RecordID']").val(recordID);
							jQuery("#frmIntegerDelete").trigger("submit");
						}
					);
			}
		);
}
function Strings_Updated()
{
	var ddlStringCategory = document.getElementById("ddlStringCategory");
	jQuery("#StringsListContainer tbody tr").each(
			function()
			{
				var tr = jQuery(this);
				var fieldValue = tr.children().eq(2).text();
				tr.find("td:nth-child(2) a").click(
						function()
						{
							var fieldCategory = tr.children().eq(0).text();
							var categoryIndex = 0;
							for (var i = 0; i < ddlStringCategory.options.length; i++)
							{
								if (ddlStringCategory.options[i].text === fieldCategory)
								{
									categoryIndex = i;
								}
							}
							ddlStringCategory.selectedIndex = categoryIndex;
							ddlCategory_Change(ddlStringCategory, "StringList");
							var fieldType = tr.attr("data-type");
							jQuery("#StringList input[value='" + fieldType + "']").prop("checked", true);
							jQuery("#frmStringDetail input[name='ID']").val(tr.attr("data-record"));
							jQuery("#txtStringValue").val(fieldValue);
							jQuery("#ModalEditString").modal("show");
						}
					);
				tr.find("td:nth-child(4) a").click(
						function()
						{
							if (!window.confirm("Are you sure you want to delete this text attribute?\n\nYou will not be able to undo this operation."))
							{
								return;
							}
							var recordID = tr.attr("data-record");
							jQuery("#frmStringDelete input[name='RecordID']").val(recordID);
							jQuery("#frmStringDelete").trigger("submit");
						}
					);
			}
		);
}
function Hugetexts_Updated()
{
	var editHugetextActionUrl = document.getElementById("HugetextsListContainer").getAttribute("data-edit") + "&RecordID=";
	jQuery("#HugetextsListContainer > table.table > tbody > tr").each(
			function ()
			{
				var tr = jQuery(this);
				var recordID = tr.attr("data-record");
				tr.find("> td:nth-child(2) a").click(
						function()
						{
							var expandedRow = ExpandDetailRow(tr);
							if (expandedRow.find("div progress"))
							{
								var params = { RecordID: recordID };
								jQuery.get(
										document.getElementById("HugetextsListContainer").getAttribute("data-detail"),
										params,
										function(data) { expandedRow.find("div").html(data); }
									);
							}
						}
					);
				tr.find("> td:nth-child(3) a").click(
						function()
						{
							window.location = editHugetextActionUrl + recordID;
						}
					);
				tr.find("> td:nth-child(4) a").click(
						function()
						{
							if (!window.confirm("Are you sure you want to delete this text detail?\n\nYou will not be able to undo this operation."))
							{
								return;
							}
							jQuery("#frmHugetextDelete input[name='RecordID']").val(recordID);
							jQuery("#frmHugetextDelete").trigger("submit");
						}
					);
			}
		);
}
function RelationsListContainer_Updated()
{
	function EditRelation(row)
	{
		var recordID = row.attr("data-record");
		jQuery("#ModalEditRelationOrgsPers input[name='ID']").val(recordID);
		jQuery("#ModalEditRelationOrgsPers input[name='OrganizationID']").val(row.find("td:first-child a").attr("data-value"));
		jQuery("#ModalEditRelationOrgsPers input[name='PositionTitle']").val(row.find("td:nth-child(2) a").text());
		jQuery("#ModalEditRelationOrgsPers").modal("show");
		var jobTypeID = row.find("td:nth-child(2)").attr("data-value");
		if (jobTypeID)
		{
			var radio = jQuery("#JobTypesList input[value='" + jobTypeID + "']");
			var category = radio.parents("li").attr("data-val-category");
			if (category == "")
			{
				MyPage.ddlJobCategory.selectedIndex = 0;
			}
			else
			{
				for (i = 0; i < MyPage.ddlJobCategory.options.length; i++)
				{
					if (MyPage.ddlJobCategory.options[i].text == category)
					{
						MyPage.ddlJobCategory.selectedIndex = i;
					}
				}
			}
			ddlCategory_Change(MyPage.ddlJobCategory, "JobTypesList");
			radio.prop("checked", true);
		}
	}
	function DeleteRelation(row)
	{
		if (!confirm("Are you sure you want to delete this relation?\nYou\'ll not be able to undo this operation.\n\nNote that both the organization and person records will remain in the database.")) return;
		var recordID = row.attr("data-record");
		MyPage.FormRelationDelete.find("input[name='RelationID']").val(recordID);
		MyPage.FormRelationDelete.submit();
	}
	function PutHrefToLinks(row)
	{
		var CrmLink = row.find("td:nth-child(1) a");
		CrmLink.attr("href", MyPage.CrmOrgsViewUrl + CrmLink.attr("data-value"));
	}
	jQuery("#RelationsListContainer tbody tr").each(
			function()
			{
				var tr = jQuery(this);
				tr.find("td:nth-child(2) a").click( function() { EditRelation(tr); } );
				tr.find("td:nth-child(3) a").click( function() { DeleteRelation(tr); } );
				PutHrefToLinks(tr);
			}
		);
}
function LookupOrganizationResults_Loaded()
{
	jQuery("#LookupOrganizationResult tbody a").click(
			function()
			{
				jQuery("#ModalEditRelationOrgsPers h4 span").text(jQuery(this).text());
				jQuery("#ModalEditRelationOrgsPers input[name='OrganizationID']").val(jQuery(this).attr("data-value"));
				jQuery("#ModalEditRelationOrgsPers input[name='RelationID']").val("");
				jQuery("#ModalAddRelationOrgsPers").modal("hide");
				jQuery("#ModalEditRelationOrgsPers").modal("show");
				jQuery("#ModalEditRelationOrgsPers input[name='PositionTitle']").focus();
			}
		);
}
function RelationEditor_Loaded()
{
	MyPage.RelationsListContainer = new Nurbiosis.AjaxLoadingElement("#RelationsListContainer");
	MyPage.RelationsListContainer.Progress("#Relations progress").OnSuccess(RelationsListContainer_Updated).load();
	Nurbiosis.AjaxifyForm("#ModalAddRelationOrgsPers form").Target("#LookupOrganizationResult").Progress("#ModalAddRelationOrgsPers progress").OnSuccess(LookupOrganizationResults_Loaded).HideModal(false);
	Nurbiosis.AjaxifyForm("#ModalEditRelationOrgsPers form").Target("#RelationsListContainer").Progress("#Relations progress").OnSuccess(RelationsListContainer_Updated);
	jQuery("#txtJobTitle").attr("data-default", jQuery("#txtJobTitle").val());
	jQuery("#ModalAddRelationOrgsPers").on("shown.bs.modal", function() { jQuery("#ModalAddRelationOrgsPers input[name='NamePattern']").focus(); });
	jQuery("#ModalEditRelationOrgsPers").on("hidden.bs.modal", ModalEditRelationOrgsPers_Reset);
	jQuery("#ModalEditRelationOrgsPers").on("shown.bs.modal", function() { jQuery("#ModalEditRelationOrgsPers input[name='PositionTitle']").focus(); });
	MyPage.ddlJobCategory = document.getElementById("ddlJobCategory");
	ddlCategory_Change(MyPage.ddlJobCategory, "JobTypesList");
}
function Activities_Loaded()
{
	function PutHrefToLinks(row)
	{
		var RowLink = row.find("td:nth-child(1) a");
		RowLink.attr("href", MyPage.ActivityEditUrl + row.attr("data-record"));
	}
	jQuery("#ActivitiesListContainer tbody tr").each(
			function()
			{
				var tr = jQuery(this);
				PutHrefToLinks(tr);
			}
		);
}



function ExpandDetailRow(triggeringRow)
{
	if (!triggeringRow) return;
	var targetRow = triggeringRow.siblings().eq(triggeringRow.index());
	if (triggeringRow.hasClass("expanded"))
	{
		triggeringRow.removeClass("expanded");
		targetRow.find("div").eq(0).slideUp(function () { targetRow.css("display", "none"); });
	}
	else
	{
		triggeringRow.addClass("expanded");
		targetRow.css("display", "table-row");
		targetRow.find("div").eq(0).slideDown();
	}
	return targetRow;
}



function Page_Start()
{
	Nurbiosis.EnableTabs("#AddressesSelector");

	Nurbiosis.AjaxifyForm("#frmContactDetail").Target("#ContactsListContainer").Progress("#Contacts progress").OnSuccess(Contacts_Updated);
	Nurbiosis.AjaxifyForm("#frmContactDelete").Target("#ContactsListContainer").Progress("#Contacts progress").OnSuccess(Contacts_Updated);
	Contacts_Updated();
	jQuery("#ModalEditContact").on("shown.bs.modal", function() { jQuery("#ModalEditContact input[name='Contact']").focus(); });
	jQuery("#ModalEditContact").on("hidden.bs.modal", ModalEditContact_Reset);

	Nurbiosis.AjaxifyForm("#frmFlagDetail").Target("#FlagsListContainer").Progress("#Flags progress").OnSuccess(Flags_Updated);
	Nurbiosis.AjaxifyForm("#frmFlagDelete").Target("#FlagsListContainer").Progress("#Flags progress").OnSuccess(Flags_Updated);
	Flags_Updated();
	jQuery("#ModalEditFlag").on("hidden.bs.modal", ModalEditFlag_Reset);
	ModalEditFlag_Reset();

	Nurbiosis.AjaxifyForm("#frmIntegerDetail").Target("#IntegersListContainer").Progress("#Integers progress").OnSuccess(Integers_Updated);
	Nurbiosis.AjaxifyForm("#frmIntegerDelete").Target("#IntegersListContainer").Progress("#Integers progress").OnSuccess(Integers_Updated);
	Integers_Updated();
	jQuery("#ModalEditInteger").on("hidden.bs.modal", ModalEditInteger_Reset);
	ModalEditInteger_Reset();

	Nurbiosis.AjaxifyForm("#frmStringDetail").Target("#StringsListContainer").Progress("#Strings progress").OnSuccess(Strings_Updated);
	Nurbiosis.AjaxifyForm("#frmStringDelete").Target("#StringsListContainer").Progress("#Strings progress").OnSuccess(Strings_Updated);
	Strings_Updated();
	jQuery("#ModalEditString").on("hidden.bs.modal", ModalEditString_Reset);
	ModalEditString_Reset();

	Nurbiosis.AjaxifyForm("#frmHugetextDelete").Target("#HugetextsListContainer").Progress("#HugeTexts progress").OnSuccess(Hugetexts_Updated);
	Hugetexts_Updated();

	var selectedTabIndex = document.getElementById("DetailsTabSelector").getAttribute("data-selected-index");
	jQuery("#DetailsTabSelector li:eq(" + selectedTabIndex + ") a").tab("show");

	MyPage.OtherAddresses = new Nurbiosis.AjaxLoadingElement("#OtherAddresses");
	MyPage.OtherAddresses.load();
	MyPage.PickAddressForm = new Nurbiosis.AjaxifiedForm("#ModalPickAddress form").Progress("#ModalPickAddress progress").OnSuccess(Address_Updated);

	MyPage.CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	MyPage.RelationEditor = new Nurbiosis.AjaxLoadingElement("#RelationEditor");
	MyPage.RelationEditor.OnSuccess(RelationEditor_Loaded).load();
	MyPage.FormRelationDelete = jQuery("#Relations form[method='delete']");
	Nurbiosis.AjaxifyForm(MyPage.FormRelationDelete).Target("#RelationsListContainer").Progress("#Relations progress").OnSuccess(RelationsListContainer_Updated);

	MyPage.ActivityEditUrl = jQuery("#hdnActivityEditUrl").val() + "?ActivityID=";
	MyPage.Activities = new Nurbiosis.AjaxLoadingElement("#ActivitiesListContainer");
	MyPage.Activities.OnSuccess(Activities_Loaded).load();
}



jQuery(Page_Start);