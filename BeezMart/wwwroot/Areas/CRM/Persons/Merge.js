﻿

var MergeButton;
var CrmPersViewUrl = "";



function PutHrefToLinks(tr)
{
	var CrmLink = tr.find("td:nth-child(3) a");
	var recordID = tr.find("td:nth-child(1) input").val();
	CrmLink.attr("href", CrmPersViewUrl + recordID);
}



function btnMerge_Click()
{
	var FromPersCount = 0;
	jQuery("#ResultsFrom input").each(function() { if (jQuery(this).prop("checked")) FromPersCount++; });
	if (FromPersCount == 0)
	{
		toastr.warning("Please select at least a person to merge FROM!", "Validation");
		return;
	}
	var ToPersCount = 0;
	jQuery("#ResultsTo input").each(function() { if (jQuery(this).prop("checked")) ToPersCount++; });
	if (ToPersCount == 0)
	{
		toastr.warning("Please select at least a person to merge TO!", "Validation");
		return;
	}
	jQuery("#frmMerge").submit();
	jQuery("#ModalMergingMessage").modal("show");
}
function btnSearchFrom_Click()
{
	if (MergeButton.prop("disabled")) MergeButton.prop("disabled", false);
	jQuery("#frmFrom").submit();
	if (jQuery("#ResultsTo").html().length < 10)
	{
		jQuery("#txtToName").val(jQuery("#txtFromName").val());
		jQuery("#frmTo").submit();
	}
}
function btnSearchTo_Click()
{
	if (MergeButton.prop("disabled")) MergeButton.prop("disabled", false);
	jQuery("#frmTo").submit();
}
function ResultsFrom_Updated()
{
	jQuery("#ResultsFrom tbody tr").each( function() { PutHrefToLinks(jQuery(this)); } );
}
function ResultsTo_Updated()
{
    jQuery("#ResultsTo tbody tr").each( function() { PutHrefToLinks(jQuery(this)); } );
}
function Mergings_Processed()
{
    jQuery("#frmFrom").submit();
    jQuery("#frmTo").submit();
}



function Page_Start()
{
	MergeButton = jQuery("#frmMerge button");
	CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
	jQuery("#txtFromName").focus();
	Nurbiosis.AjaxifyForm("#frmFrom").Target("#ResultsFrom").Progress("#prgrsFrom").OnSuccess(ResultsFrom_Updated);
	Nurbiosis.AjaxifyForm("#frmTo").Target("#ResultsTo").Progress("#prgrsTo").OnSuccess(ResultsTo_Updated);
	Nurbiosis.AjaxifyForm("#frmMerge").Target("#MergingMessage").Progress("#ModalMergingMessage progress").OnSuccess(Mergings_Processed);
}



jQuery(Page_Start);