﻿


var MyPage = {
    CrmPersViewUrl: null,
    SearchForm: null,
    AdvancedSearch: null,
	Export: function() {
		let exportUrl = "_Search_Export?" + MyPage.SearchForm.serialize();
		window.open(exportUrl);
	},
	DirectLink: function() {
		let pageUrl = window.location.href;
		let questionMarkIndex = pageUrl.indexOf("?");
		if (questionMarkIndex > 0) pageUrl = pageUrl.substring(0, questionMarkIndex);
		let directSearchLink = pageUrl + "?" + MyPage.SearchForm.serialize();
		Nurbiosis.CopyTextToClipboard(directSearchLink);
		let btnDirectLink = jQuery("#btnDirectLink");
		btnDirectLink.attr("title", "Copied to clipboard");
		btnDirectLink.removeClass("btn-outline-secondary").addClass("btn-success");
		function hideTooltip () {
			btnDirectLink.tooltip("hide");
			btnDirectLink.removeClass("btn-success").addClass("btn-outline-secondary");
		}
		let tooltipOptions = {
			container: "body",
			trigger: "manual"
		};
		btnDirectLink.tooltip(tooltipOptions).tooltip("show");
		setTimeout(hideTooltip, 3000);
	}
};



function btnChangeFilter_Click() {
    jQuery("#SearchResults .alert").alert("close");
    jQuery("#ChangeFilter").slideUp(300, function () { jQuery("#SearchForm").slideDown(600); });
}



function SearchResults_Loaded()
{
    function PutHrefToLinks(tr) {
        let CrmLink = tr.find("td:nth-child(2) a");
        CrmLink.attr("href", MyPage.CrmPersViewUrl + tr.attr("data-id"));
    }
	jQuery("#SearchResults tbody tr").each(function() { PutHrefToLinks(jQuery(this)); });
	jQuery("#SearchForm").slideUp(600, function() { jQuery("#ChangeFilter").slideDown(300); });
}



function ShowAdvancedFilter() {
    let IsAdvancedFilter = false;
    if (jQuery("input[name='Name']").val()) IsAdvancedFilter = true;
    if (jQuery("input[name='Contact']").val()) IsAdvancedFilter = true;
    if (jQuery("input[name='City']").val()) IsAdvancedFilter = true;
    if (jQuery("select[name='Segments']").attr("data-selected")) {
        let selectedValues = jQuery("select[name='Segments']").attr("data-selected").split(",");
        jQuery("select[name='Segments']").val(selectedValues);
        IsAdvancedFilter = true;
    }
    if (jQuery("select[name='Flags']").attr("data-selected")) {
        let selectedValues = jQuery("select[name='Flags']").attr("data-selected").split(",");
        jQuery("select[name='Flags']").val(selectedValues);
        IsAdvancedFilter = true;
    }
    if (IsAdvancedFilter) MyPage.AdvancedSearch.removeClass("collapse");
}



function Page_Start()
{
    MyPage.AdvancedSearch = jQuery("#AdvancedSearch");
    jQuery("#btnAdvancedSearch a").click(function () { MyPage.AdvancedSearch.slideToggle(); });
    MyPage.CrmPersViewUrl = jQuery("#hdnCrmPersViewUrl").val() + "?PersonID=";
    MyPage.SearchForm = jQuery("#SearchForm form");
    Nurbiosis.AjaxifyForm(MyPage.SearchForm).Target("#SearchResults").Progress("progress").OnSuccess(SearchResults_Loaded);
    ShowAdvancedFilter();
    if (jQuery("#hdnFilterIsSet").val() == "true") MyPage.SearchForm.submit();
}



jQuery(Page_Start);