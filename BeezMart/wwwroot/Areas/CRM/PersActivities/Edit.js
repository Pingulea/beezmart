﻿


function Page_Start()
{
	var countOfActivityTypesWithNoCategory = 0;
	var ddlActivityCategory = document.getElementById("ddlActivityCategory");
	var ddlActivityType = document.getElementById("ddlActivityType");
	var ActivityTypeID = ddlActivityType.getAttribute("data-val");
	var ddlActivityTypeSelectedIndex = 0;
	var SelectedCategory = "[none]";
	for (i = 0; i < ddlActivityType.options.length; i++)
	{
		if (ddlActivityType.options[i].getAttribute("data-val-category") == "")
		{
			countOfActivityTypesWithNoCategory++;
		}
		if (ddlActivityType.options[i].getAttribute("value") == ActivityTypeID)
		{
			ddlActivityTypeSelectedIndex = i;
			SelectedCategory = ddlActivityType.options[i].getAttribute("data-val-category");
		}
	}
	if (countOfActivityTypesWithNoCategory == 0)
	{
		ddlActivityCategory.removeChild(ddlActivityCategory.options[0]);
	}
	ddlActivityType.selectedIndex = ddlActivityTypeSelectedIndex;
	for (i = 0; i < ddlActivityCategory.options.length; i++)
	{
		if (ddlActivityCategory.options[i].text == SelectedCategory)
		{
			ddlActivityCategory.selectedIndex = i;
		}
	}
	Nurbiosis.CascadeDropdowns("ddlActivityCategory", "ddlActivityType");
	Nurbiosis.AttachDatepicker();
	Nurbiosis.EnableTinyMCE("textarea[name='Activity.Details']");
}



jQuery(Page_Start);