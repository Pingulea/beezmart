﻿


var MyPage = {};



MyPage.DoSubmit = function(toWhere)
{
	jQuery("#frmRelationship input[name='AfterSaveGoTo']").val(toWhere);
	jQuery("#frmRelationship").submit();
}
MyPage.ddlCategory_Change = function()
{
	var ddlCategory = document.getElementById("ddlJobCategory");
	var radioButtonsSelector = "#JobTypesList input[type='radio']";
	jQuery(radioButtonsSelector).prop("checked", false);
	var categoryName = (ddlCategory.selectedIndex === 0) ? "" : ddlCategory.options[ddlCategory.selectedIndex].text;
	var fieldsSelector = "#JobTypesList li";
	jQuery(fieldsSelector).each(
			function()
			{
				if (jQuery(this).attr("data-val-category") === categoryName)
				{
					jQuery(this).show();
				}
				else
				{
					jQuery(this).hide();
				}
			}
		);
}



function Page_Start()
{
	MyPage.ddlCategory_Change();
}



jQuery(Page_Start);