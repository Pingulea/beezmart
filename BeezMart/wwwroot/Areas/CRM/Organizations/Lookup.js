﻿


var CrmOrgsViewUrl = "";



function PutHrefToLinks(tr)
{
	var CrmLink = tr.find("td:nth-child(2) a");
	CrmLink.attr("href", CrmOrgsViewUrl + tr.attr("data-id"));
}



function Page_Start()
{
	CrmOrgsViewUrl = jQuery("#hdnCrmOrgsViewUrl").val() + "?OrganizationID=";
	jQuery("#SearchResults tbody tr").each( function() { PutHrefToLinks(jQuery(this)); } );
}



jQuery(Page_Start);