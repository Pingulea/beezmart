﻿


function Page_Start()
{
	let countOfFieldsWithNoCategory = 0;
	let ddlFieldCategory = document.getElementById("ddlFieldCategory");
	let ddlFieldName = document.getElementById("ddlFieldName");
	let FieldID = ddlFieldName.getAttribute("data-val");
	let ddlFieldNameSelectedIndex = 0;
	let SelectedCategory = "[none]";
	for (i = 0; i < ddlFieldName.options.length; i++)
	{
		if (ddlFieldName.options[i].getAttribute("data-val-category") == "")
		{
			countOfFieldsWithNoCategory++;
		}
		if (ddlFieldName.options[i].getAttribute("value") == FieldID)
		{
			ddlFieldNameSelectedIndex = i;
			SelectedCategory = ddlFieldName.options[i].getAttribute("data-val-category");
		}
	}
	if (countOfFieldsWithNoCategory == 0)
	{
		ddlFieldCategory.removeChild(ddlFieldCategory.options[0]);
	}
	ddlFieldName.selectedIndex = ddlFieldNameSelectedIndex;
	for (i = 0; i < ddlFieldCategory.options.length; i++)
	{
		if (ddlFieldCategory.options[i].text == SelectedCategory)
		{
			ddlFieldCategory.selectedIndex = i;
		}
	}
	Nurbiosis.CascadeDropdowns("ddlFieldCategory", "ddlFieldName");
	Nurbiosis.EnableTinyMCE();
}



jQuery(Page_Start);