﻿using BeezMart.Entities.Billing;
using BeezMart;

namespace BeezMart.Areas.Billing.ViewModels.InvoiceItems;

public class Index_ViewModel
{
	public InvoiceModel? Invoice { get; set; }
	public List<InvoiceItemModel>? InvoiceItems { get; set; }
}
public class Edit_ViewModel
{
	public int? InvoiceID { get; set; }
	public string DefaultUnitMeasure { get; set; } = MyApp.ConfigSettings["Sales.Default-Unit-Measure"];
}