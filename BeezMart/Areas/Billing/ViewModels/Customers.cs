﻿using BeezMart.DAL;
using BeezMart.Entities.CRM;
using System.Data.SqlClient;

namespace BeezMart.Areas.Billing.ViewModels.Customers;

public class Overdue_ViewModel
{
    public Dictionary<OrganizationModel, int>? Organizations { get; set; }
    public Dictionary<PersonModel, int>? Persons { get; set; }

    public async Task LoadOrganizations()
    {
        this.Organizations = new Dictionary<OrganizationModel, int> { };
        SqlDataReader rdrReader, row;
        OrganizationModel item;
        int invoicesCount;
        using (Database db = new Database())
        {
            row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Dashboard_Statistics_2a");
            while (await rdrReader.ReadAsync())
            {
                item = new OrganizationModel { };
                item.ID = (int)row["Organization_ID"];					                                                // Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
		        item.SegmentID = (byte)row["Segment_ID"];                                                               // TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Orgs_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
                item.Segment = (string)row["Segment_Name"];                                                             // NVarChar(256) NOT NULL
                if (row["Parent_Org_ID"] != DBNull.Value) item.ParentOrgID = (int)row["Parent_Org_ID"];                 // Int NULL FOREIGN KEY REFERENCES CRM.Organizations(Organization_ID)
                item.Name = (string)row["Org_Name"];                                                                    // NVarChar(256) NOT NULL
                if (row["Name_Prefix"] != DBNull.Value) item.NamePrefix = (string)row["Name_Prefix"];                   // NVarChar(16) NULL
                if (row["Name_Suffix"] != DBNull.Value) item.NameSuffix = (string)row["Name_Suffix"];                   // NVarChar(16) NULL
                if (row["Alternate_Names"] != DBNull.Value) item.AlternateNames = (string)row["Alternate_Names"];       // NVarChar(1024) NULL
                item.CountryOfOrigin = (string)row["Country_Of_Origin"];                                                // Char(3) NOT NULL DEFAULT 'ROM'
                if (row["Site_ID_Headquarter"] != DBNull.Value) item.SiteIdHome = (int)row["Site_ID_Headquarter"];      // Int NULL
                if (row["Site_ID_Billing"] != DBNull.Value) item.SiteIdBill = (int)row["Site_ID_Billing"];              // Int NULL
                if (row["Site_ID_Shipping"] != DBNull.Value) item.SiteIdShip = (int)row["Site_ID_Shipping"];            // Int NULL
                item.UpdatedOn = (DateTime)row["Updated_On"];                                                           // SmallDateTime NOT NULL DEFAULT GETDATE()
                if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];					    // NVarChar(128) NULL
                invoicesCount = (int)row["Invoices_Count"];					                                            // = (SELECT COUNT(*) FROM Overdue_Invoices I WHERE O.Organization_ID = I.Organization_ID)
                this.Organizations.Add(item, invoicesCount);
            }
            rdrReader.Dispose();
        }
    }
    public async Task LoadPersons()
    {
        this.Persons = new Dictionary<PersonModel, int> { };
        SqlDataReader rdrReader, row;
        PersonModel item;
        int invoicesCount;
        using (Database db = new Database())
        {
            row = rdrReader = await db.GetSqlDataReaderAsync("Billing.Dashboard_Statistics_2b");
            while (await rdrReader.ReadAsync())
            {
                item = new PersonModel { };
                item.ID = (int)row["Person_ID"];                                                                    // Int NOT NULL PRIMARY KEY IDENTITY(1, 1)
                item.SegmentID = (byte)row["Segment_ID"];                                                           // TinyInt NOT NULL DEFAULT 0 FOREIGN KEY REFERENCES CRM.Pers_Segment_Names(Segment_ID) ON DELETE NO ACTION ON UPDATE CASCADE
                item.Segment = (string)row["Segment_Name"];                                                         // NVarChar(256) NOT NULL
                item.FirstName = (string)row["First_Name"];                                                         // NVarChar(64) NOT NULL
                item.LastName = (string)row["Last_Name"];                                                           // NVarChar(64) NOT NULL
                if (row["Name_Prefix"] != DBNull.Value) item.NamePrefix = (string)row["Name_Prefix"];               // NVarChar(16) NULL
                if (row["Name_Suffix"] != DBNull.Value) item.NameSuffix = (string)row["Name_Suffix"];               // NVarChar(16) NULL
                if (row["Nicknames"] != DBNull.Value) item.Nicknames = (string)row["Nicknames"];                    // NVarChar(256) NULL
                item.CountryOfOrigin = (string)row["Country_Of_Origin"];                                            // Char(3) NOT NULL DEFAULT 'ROM'
                if (row["Site_ID_Home"] != DBNull.Value) item.SiteIdHome = (int)row["Site_ID_Home"];                // Int NULL
                if (row["Site_ID_Billing"] != DBNull.Value) item.SiteIdBill = (int)row["Site_ID_Billing"];          // Int NULL
                if (row["Site_ID_Shipping"] != DBNull.Value) item.SiteIdShip = (int)row["Site_ID_Shipping"];        // Int NULL
                item.UpdatedOn = (DateTime)row["Updated_On"];                                                       // SmallDateTime NOT NULL DEFAULT GETDATE()
                if (row["Updated_By"] != DBNull.Value) item.UpdatedBy = (string)row["Updated_By"];					// NVarChar(128) NULL
                invoicesCount = (int)row["Invoices_Count"];					                                        // = (SELECT COUNT(*) FROM Overdue_Invoices I WHERE O.Organization_ID = I.Organization_ID)
                this.Persons.Add(item, invoicesCount);
            }
            rdrReader.Dispose();
        }
    }
}