﻿using BeezMart.Entities.Billing;
using BeezMart;

namespace BeezMart.Areas.Billing.ViewModels.Payments;

public class Index_ViewModel
{
	public DateTime? FromDate { get; set; }
	public DateTime? ToDate { get; set; }
}

public class _OnIndexSave_ViewModel
{
	public string? Message { get; set; }
	public bool ReadyToClose { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
	public int InvoiceID { get; set; }
}

public class _OnIndexDelete_ViewModel
{
	public string? Message { get; set; }
	public bool ReviewInvoice { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
}

public class _OnPendingInvoicesSave_ViewModel
{
	public string? Message { get; set; }
	public bool ReadyToClose { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
	public int InvoiceID { get; set; }
}

public class _OnInvoiceSave_ViewModel
{
	public string? Message { get; set; }
	public bool ReadyToClose { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
}

public class _OnInvoiceEdit_ViewModel
{
	public int? InvoiceID { get; set; }
	public string DefaultCurrency { get; set; } = MyApp.ConfigSettings["Billing.Default-Currency"];
}

public class _OnInvoiceDelete_ViewModel
{
	public string? Message { get; set; }
	public bool ReviewInvoice { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
}