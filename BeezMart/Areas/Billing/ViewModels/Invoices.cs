﻿using BeezMart.Entities.Billing;

namespace BeezMart.Areas.Billing.ViewModels.Invoices;

public class Index_ViewModel
{
		
}

public class Start_ViewModel
{
    public List<CustomerBillingDetailsModel>? BillingDetailsFromInvoices { get; set; }
    public List<CustomerBillingDetailsModel>? BillingDetailsFromSavedSettings { get; set; }
    public List<CustomerBillingDetailsModel>? BillingDetailsFromCRM { get; set; }

    public async Task LookupOrganizations(string searchString, short maxRecords = 10)
    {
        if (this.BillingDetailsFromCRM == null) this.BillingDetailsFromCRM = new List<CustomerBillingDetailsModel>();
        List<CustomerBillingDetailsModel> searchResult = await BeezMart.DAL.Billing.CustomerBillingDetails.LookupOrganizationsAsync(searchString, exactMatch: false, currency: null, maxRows: maxRecords);
        foreach (CustomerBillingDetailsModel billingDetailsRow in searchResult) this.BillingDetailsFromCRM.Add(billingDetailsRow);
    }
    public async Task LookupPersons(string searchString, short maxRecords = 20)
    {
        if (this.BillingDetailsFromCRM == null) this.BillingDetailsFromCRM = new List<CustomerBillingDetailsModel>();
        List<CustomerBillingDetailsModel> searchResult = await BeezMart.DAL.Billing.CustomerBillingDetails.LookupPersonsAsync(searchString, exactMatch: false, currency: null, maxRows: maxRecords);
        foreach (CustomerBillingDetailsModel billingDetailsRow in searchResult) this.BillingDetailsFromCRM.Add(billingDetailsRow);
    }
    public async Task LookupCustomerSavedSettings(string searchString, short maxRecords = 10)
    {
        if (this.BillingDetailsFromSavedSettings == null) this.BillingDetailsFromSavedSettings = new List<CustomerBillingDetailsModel>();
        List<CustomerBillingDetailsModel> searchResult = await BeezMart.DAL.Billing.CustomerBillingDetails.LookupSettingsAsync(searchString, exactMatch: false, currency: null, maxRows: maxRecords);
        foreach (CustomerBillingDetailsModel billingDetailsRow in searchResult) this.BillingDetailsFromSavedSettings.Add(billingDetailsRow);
    }
    public async Task LookupInvoices(string searchString, short maxRecords = 10)
    {
        if (this.BillingDetailsFromInvoices == null) this.BillingDetailsFromInvoices = new List<CustomerBillingDetailsModel>();
        List<CustomerBillingDetailsModel> searchResult = await BeezMart.DAL.Billing.CustomerBillingDetails.LookupInvoicesAsync(searchString, exactMatch: false, currency: null, maxRows: maxRecords);
        foreach (CustomerBillingDetailsModel billingDetailsRow in searchResult) this.BillingDetailsFromInvoices.Add(billingDetailsRow);
    }
}

public class Add_ViewModel
{
	//public int OrganizationID { get; set; }
	//public int PersonID { get; set; }
	public DateTime? ExchangeRateDate { get; set; }
	public InvoiceModel? Invoice { get; set; }
	public List<HandoverDelegateModel>? HandoverDelegates { get; set; }
	public List<HandoverTransportModel>? HandoverTransports { get; set; }
    public int? BillingAddressID { get; set; }
    public int? BankAccountID { get; set; }
}

public class Drafts_ViewModel
{
	public Dictionary<string, int>? InvoiceCountsByCurrency { get; set; }
	public List<InvoiceModel>? InvoicesRegular { get; set; }
	public List<InvoiceModel>? InvoicesStorno { get; set; }
	public List<InvoiceModel>? InvoicesAdvance { get; set; }
	public List<InvoiceModel>? InvoicesRisturn { get; set; }
	public List<InvoiceModel>? InvoicesProForma { get; set; }
}

public class Validated_ViewModel
{
	public Dictionary<string, int>? InvoiceCountsByCurrency { get; set; }
	public List<InvoiceModel>? InvoicesRegular { get; set; }
	public List<InvoiceModel>? InvoicesStorno { get; set; }
	public List<InvoiceModel>? InvoicesAdvance { get; set; }
	public List<InvoiceModel>? InvoicesRisturn { get; set; }
	public List<InvoiceModel>? InvoicesProForma { get; set; }
}

public class View_ViewModel
{
	public bool IsEditable { get; set; }
	public bool IsPayable { get; set; }
	public InvoiceModel? Invoice { get; set; }
}

public class Edit_ViewModel
{
	public bool IsEditable { get; set; }
	public InvoiceModel? Invoice { get; set; }
	public List<HandoverDelegateModel>? HandoverDelegates { get; set; }
	public List<HandoverTransportModel>? HandoverTransports { get; set; }
}

public class Duplicate_ViewModel
{
	public int ExistingInvoiceID { get; set; }
}

public class Stornate_ViewModel
{
	public int ExistingInvoiceID { get; set; }
}

public class Preview_ViewModel
{
	public string? InvoiceCssStyles { get; set; }
	public string? InvoiceHtml { get; set; }
	public InvoiceModel? Invoice { get; set; }
}

public class Print_ViewModel
{
	public bool IsPrintable { get; set; }
	public string? InvoiceCssStyles { get; set; }
	public string? InvoiceHtml { get; set; }
	public InvoiceModel? Invoice { get; set; }
	public Print_ViewModel()
	{
		this.IsPrintable = false;
	}
}

public class Pending_ViewModel
{
	public Dictionary<string, int>? InvoiceCountsByCurrency { get; set; }
	public List<InvoiceModel>? InvoicesRegular { get; set; }
	public List<InvoiceModel>? InvoicesStorno { get; set; }
	public List<InvoiceModel>? InvoicesAdvance { get; set; }
	public List<InvoiceModel>? InvoicesRisturn { get; set; }
	public List<InvoiceModel>? InvoicesProForma { get; set; }
}

public class Overdue_ViewModel
{
	public Dictionary<string, int>? InvoiceCountsByCurrency { get; set; }
	public List<InvoiceModel>? InvoicesRegular { get; set; }
	public List<InvoiceModel>? InvoicesStorno { get; set; }
	public List<InvoiceModel>? InvoicesAdvance { get; set; }
	public List<InvoiceModel>? InvoicesRisturn { get; set; }
	public List<InvoiceModel>? InvoicesProForma { get; set; }
}

public class _Close_ViewModel
{
	public int InvoiceID { get; set; }
	public string? Message { get; set; }
	public bool Success { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
}

public class _ChangeStatus_ViewModel
{
	public int InvoiceID { get; set; }
	public string? Message { get; set; }
	public bool Success { get; set; }
	public InvoiceStatus InvoiceStatus { get; set; }
	public decimal RemainingToBePayed { get; set; }
}

public class Search_ViewModel
{
	public string? Customer { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public bool Exact { get; set; }
	public DateTime? After { get; set; } = null;
	public DateTime? Before { get; set; } = null;
	public string? IDs { get; set; }
	public string? AccIDs { get; set; }					// Accounting IDs
	public string? Curr { get; set; }					// Currency
	public InvoiceStatus? Status { get; set; }			// Status
    public bool Overdue { get; set; }
	public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Customer)) return true;
            if (this.OrganizationID.HasValue) return true;
            if (this.PersonID.HasValue) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			if (!string.IsNullOrEmpty(IDs)) return true;
			if (!string.IsNullOrEmpty(AccIDs)) return true;
			if (this.Status.HasValue) return true;
            //if (!string.IsNullOrEmpty(this.Curr)) return true;
            if (this.Status.HasValue) return true;
            if (this.Overdue) return true;
			return false;
		}
	}
}
public class Search_Params
{
    public string? Customer { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public bool Exact { get; set; }
    public DateTime? After { get; set; } = null;
    public DateTime? Before { get; set; } = null;
    public string? IDs { get; set; }
    public string? AccIDs { get; set; }                  // Accounting IDs
    public string? Curr { get; set; }                    // Currency
    public InvoiceStatus? Status { get; set; }			// Status
    public bool Overdue { get; set; }
    public InvoiceType Type { get; set; } = InvoiceType.Regular;
    public bool FilterIsSet
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Customer)) return true;
            if (this.OrganizationID.HasValue) return true;
            if (this.PersonID.HasValue) return true;
            if (this.After.HasValue) return true;
            if (this.Before.HasValue) return true;
            if (!string.IsNullOrEmpty(IDs)) return true;
            if (!string.IsNullOrEmpty(AccIDs)) return true;
            if (this.Status.HasValue) return true;
            //if (!string.IsNullOrEmpty(this.Curr)) return true;
            if (this.Status.HasValue) return true;
            if (this.Overdue) return true;
            return false;
        }
    }
}