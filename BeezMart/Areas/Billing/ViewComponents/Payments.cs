using BeezMart.Entities.Billing;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.ViewComponents.Payments;

public class OnIndexList : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(DateTime? FromDate, DateTime? ToDate)
    {
        if (!FromDate.HasValue) FromDate = DateTime.Now.AddMonths(-3);
        if (!ToDate.HasValue) ToDate = DateTime.Now.AddMonths(1);
        List<PaymentModel> viewModel = await BeezMart.DAL.Billing.Payments.ListPaymentsDuringAsync(FromDate.Value, ToDate.Value);
        return View("~/Areas/Billing/Views/Payments/_OnIndexList.cshtml", viewModel);
    }
}
