﻿using BeezMart.Entities.Billing;
using BeezMart.Entities.CRM;
using BeezMart.Areas.Billing.ViewModels.Customers;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class CustomersController : Controller
{
	public ViewResult Index()
	{
		return View();
	}

    public async Task<ViewResult> Overdue()
    {
        Overdue_ViewModel viewModel = new Overdue_ViewModel { };
        await viewModel.LoadOrganizations();
        await viewModel.LoadPersons();
        return View(viewModel);
    }

	public PartialViewResult _PickOnInvoice()
	{
		return PartialView();
	}

	public async Task<PartialViewResult> _PickOnInvoiceOrgs(string OrgsNamePattern, bool ExactMatch = false)
	{
		OrganizationModel.LookupFilter filter = new OrganizationModel.LookupFilter()
		{
			NamePattern = OrgsNamePattern,
			NameExactMatch = ExactMatch
		};
		List<OrganizationModel> SearchResult = await BeezMart.DAL.CRM.Organizations.LookupAsync(filter);
		return PartialView(SearchResult);
	}
	public async Task<PartialViewResult> _PickOnInvoicePers(string PersNamePattern, bool ExactMatch = false)
	{
		PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
		{
			NamePattern = PersNamePattern,
			NameExactMatch = ExactMatch
		};
		List<PersonModel> SearchResult = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
		return PartialView(SearchResult);
	}

	public async Task<ActionResult> _BillingSettingsForOrganization(int OrganizationID, string? Currency = null)
	{
        CustomerBillingDetailsModel? billingDetails = await BeezMart.DAL.Billing.CustomerBillingDetails.RetrieveForOrganizationAsync(OrganizationID, Currency);
		if (billingDetails == null) return NotFound();
        return Json(billingDetails);
	}
	public async Task<ActionResult> _BillingSettingsForPerson(int PersonID, string? Currency = null)
	{
        CustomerBillingDetailsModel? billingDetails = await BeezMart.DAL.Billing.CustomerBillingDetails.RetrieveForPersonAsync(PersonID, Currency);
		if (billingDetails == null) return NotFound();
		return Json(billingDetails);
	}

	[ValidateAntiForgeryToken]
	public async Task<PartialViewResult> _SaveFromInvoice(CustomerBillingDetailsModel customerDetails)
	{
		AppMessage msg = new AppMessage();
		msg.Type = AppMessageType.Success;
		if (customerDetails.OrganizationID.HasValue)
		{
			#region save details for organization
			if (!string.IsNullOrEmpty(customerDetails.City))
			{
				OrgSiteModel? address = null;
				if (customerDetails.BillingAddressID.HasValue)
				{
					address = await BeezMart.DAL.CRM.OrgSites.RetrieveAsync(customerDetails.BillingAddressID.Value);
				}
				if (address == null)
				{
					address = new OrgSiteModel();
					address.OrganizationID = customerDetails.OrganizationID.Value;
				}
                if (address.City != customerDetails.City || address.StateProvince != customerDetails.StateProvince || address.StreetAddress1 != customerDetails.StreetAddress1)
                {
                    address.City = customerDetails.City;
					if (!string.IsNullOrEmpty(customerDetails.StateProvince)) address.StateProvince = customerDetails.StateProvince;
					if (!string.IsNullOrEmpty(customerDetails.StreetAddress1)) address.StreetAddress1 = customerDetails.StreetAddress1;
					bool success1 = await BeezMart.DAL.CRM.OrgSites.SaveAsync(address);
					if (success1 && address.ID != null)
					{
						bool success2 = await BeezMart.DAL.CRM.OrgSites.MarkForBillAsync(address.ID.Value, customerDetails.OrganizationID.Value, updatedBy: this.User.Identity?.Name);
						if (success2)
						{
							msg.AppendMessage("Customer billing address has been successfully saved.");
						}
						else
						{
							msg.AppendMessage("Although the customer address has been saved, it could not be set as billing address.");
							msg.Type = AppMessageType.Warning;
						}
					}
					else
					{
						msg.AppendMessage("A problem has occured while attempting to save the billing address for the customer");
						msg.Type = AppMessageType.Warning;
					}
                }
			}
			CustomerSettingsModel? customerSettings = await BeezMart.DAL.Billing.CustomerSettings.GetForOrganizationAsync(customerDetails.OrganizationID.Value);
			if (customerSettings == null)
			{
				customerSettings = new CustomerSettingsModel();
				customerSettings.OrganizationID = customerDetails.OrganizationID.Value;
			}
			if (!string.IsNullOrEmpty(customerDetails.RegistrationNumber)) customerSettings.RegistrationNumber = customerDetails.RegistrationNumber;
			if (!string.IsNullOrEmpty(customerDetails.TaxID)) customerSettings.TaxID = customerDetails.TaxID;
			if (!string.IsNullOrEmpty(customerDetails.Currency)) customerSettings.Currency = customerDetails.Currency;
			if (CustomerAccountModel.IsValidIBAN(customerDetails.BankAccountIBAN) && !string.IsNullOrEmpty(customerDetails.BankName))
			{
				CustomerAccountModel? account = null;
				if (customerDetails.BankAccountID.HasValue)
				{
					account = await BeezMart.DAL.Billing.CustomerAccounts.RetrieveAsync(customerDetails.BankAccountID.Value);
				}
				if (account == null)
				{
					account = new CustomerAccountModel();
					account.OrganizationID = customerDetails.OrganizationID.Value;
				}
                if (account.IBAN != customerDetails.BankAccountIBAN || account.Bank != customerDetails.BankName)
                {
                    account.IBAN = customerDetails.BankAccountIBAN;
					account.Bank = customerDetails.BankName;
					account.Currency = string.IsNullOrEmpty(customerDetails.Currency) ? MyApp.ConfigSettings["Billing.Default-Currency"] : customerDetails.Currency;
					bool success3 = await BeezMart.DAL.Billing.CustomerAccounts.SaveAsync(account);
					if (success3)
					{
						msg.AppendMessage("The customer bank acount has been saved for future use.");
					}
					else
					{
						msg.AppendMessage("The bank account could not be saved for the customer.");
						msg.Type = AppMessageType.Warning;
					}
                }
				if (!customerSettings.PreferredAccountID.HasValue)
				{
					customerSettings.PreferredAccountID = account.ID;
				}
			}
			else
			{
				if (!string.IsNullOrEmpty(customerDetails.BankAccountIBAN))
				{
					msg.AppendMessage("The bank account format is not valid; no bank account was saved for the customer.");
					msg.Type = AppMessageType.Warning;
				}
			}
			bool success4 = await BeezMart.DAL.Billing.CustomerSettings.SaveAsync(customerSettings);
			if (success4)
			{
				msg.AppendMessage("Billing details were successfully saved for the customer.");
			}
			else
			{
				msg.AppendMessage("The customer billing settings could not be saved for future usage.");
				msg.Type = AppMessageType.Warning;
			}
			return PartialView("_Message", msg);
			#endregion
		}
		if (customerDetails.PersonID.HasValue)
		{
			#region save details for person
			if (!string.IsNullOrEmpty(customerDetails.City))
			{
				PersSiteModel? address = null;
				if (customerDetails.BillingAddressID.HasValue)
				{
					address = await BeezMart.DAL.CRM.PersSites.RetrieveAsync(customerDetails.BillingAddressID.Value);
				}
				if (address == null)
				{
					address = new PersSiteModel();
					address.PersonID = customerDetails.PersonID.Value;
				}
                if (address.City != customerDetails.City || address.StateProvince != customerDetails.StateProvince || address.StreetAddress1 != customerDetails.StreetAddress1)
                {
                    address.City = customerDetails.City;
					if (!string.IsNullOrEmpty(customerDetails.StateProvince)) address.StateProvince = customerDetails.StateProvince;
					if (!string.IsNullOrEmpty(customerDetails.StreetAddress1)) address.StreetAddress1 = customerDetails.StreetAddress1;
					bool success1 = await BeezMart.DAL.CRM.PersSites.SaveAsync(address);
					if (success1 && address.ID != null)
					{
						bool success2 = await BeezMart.DAL.CRM.PersSites.MarkForBillAsync(address.ID.Value, customerDetails.PersonID.Value, updatedBy: this.User.Identity?.Name);
						if (success2)
						{
							msg.AppendMessage("Customer billing address has been successfully saved.");
						}
						else
						{
							msg.AppendMessage("Although the customer address has been saved, it could not be set as billing address.");
							msg.Type = AppMessageType.Warning;
						}
					}
					else
					{
						msg.AppendMessage("A problem has occured while attempting to save the billing address for the customer.");
						msg.Type = AppMessageType.Warning;
					}
                }
			}
			CustomerSettingsModel? customerSettings = await BeezMart.DAL.Billing.CustomerSettings.GetForPersonAsync(customerDetails.PersonID.Value);
			if (customerSettings == null)
			{
				customerSettings = new CustomerSettingsModel();
				customerSettings.PersonID = customerDetails.PersonID.Value;
			}
			if (!string.IsNullOrEmpty(customerDetails.RegistrationNumber)) customerSettings.RegistrationNumber = customerDetails.RegistrationNumber;
			if (!string.IsNullOrEmpty(customerDetails.TaxID)) customerSettings.TaxID = customerDetails.TaxID;
			if (!string.IsNullOrEmpty(customerDetails.Currency)) customerSettings.Currency = customerDetails.Currency;
			if (CustomerAccountModel.IsValidIBAN(customerDetails.BankAccountIBAN) && !string.IsNullOrEmpty(customerDetails.BankName))
			{
				CustomerAccountModel? account = null;
				if (customerDetails.BankAccountID.HasValue)
				{
					account = await BeezMart.DAL.Billing.CustomerAccounts.RetrieveAsync(customerDetails.BankAccountID.Value);
				}
				if (account == null)
				{
					account = new CustomerAccountModel();
					account.PersonID = customerDetails.PersonID.Value;
				}
                if (account.IBAN != customerDetails.BankAccountIBAN || account.Bank != customerDetails.BankName)
                {
                    account.IBAN = customerDetails.BankAccountIBAN;
					account.Bank = customerDetails.BankName;
					account.Currency = string.IsNullOrEmpty(customerDetails.Currency) ? MyApp.ConfigSettings["Billing.Default-Currency"] : customerDetails.Currency;
					bool success3 = await BeezMart.DAL.Billing.CustomerAccounts.SaveAsync(account);
					if (success3)
					{
						msg.AppendMessage("The customer bank acount has been saved for future use.");
					}
					else
					{
						msg.AppendMessage("The bank account could not be saved for the customer.");
						msg.Type = AppMessageType.Warning;
					}
                }
				if (!customerSettings.PreferredAccountID.HasValue)
				{
					customerSettings.PreferredAccountID = account.ID;
				}
			}
			else
			{
				if (!string.IsNullOrEmpty(customerDetails.BankAccountIBAN))
				{
					msg.AppendMessage("The bank account format is not valid; no bank account was saved for the customer.");
					msg.Type = AppMessageType.Warning;
				}
			}
			bool success4 = await BeezMart.DAL.Billing.CustomerSettings.SaveAsync(customerSettings);
			if (success4)
			{
				msg.AppendMessage("Billing details were successfully saved for the customer.");
			}
			else
			{
				msg.AppendMessage("The customer billing settings could not be saved for future usage.");
				msg.Type = AppMessageType.Warning;
			}
			return PartialView("_Message", msg);
			#endregion
		}
		msg.AppendMessage("Could not save details: both organization and person references are missing.");
		msg.Type = AppMessageType.Error;
		return PartialView("_Message", msg);
	}
}