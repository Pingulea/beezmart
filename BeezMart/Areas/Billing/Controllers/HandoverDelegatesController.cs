﻿using BeezMart.Entities.Billing;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class HandoverDelegatesController : Controller
{
    public ViewResult Index()
    {
        return View();
    }

    public async Task<PartialViewResult> _PickOnInvoice()
    {
        List<HandoverDelegateModel> viewModel = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
        return PartialView(viewModel);
    }

    public async Task<PartialViewResult> _List()
    {
        List<HandoverDelegateModel> viewModel = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
        return PartialView(viewModel);
    }

    [HttpGet]
    public PartialViewResult _Edit()
    {
        return PartialView();
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(HandoverDelegateModel handoverDelegate)
    {
        if (TryValidateModel(handoverDelegate))
        {
            await BeezMart.DAL.Billing.HandoverDelegates.SaveAsync(handoverDelegate);
        }
        else
        {
            AppMessage msg = new AppMessage();
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        List<HandoverDelegateModel> viewModel = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
        return PartialView(nameof(this._List), viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(short RecordID)
    {
        await BeezMart.DAL.Billing.HandoverDelegates.DeleteAsync(RecordID);
        List<HandoverDelegateModel> viewModel = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
        return PartialView(nameof(this._List), viewModel);
    }
}

