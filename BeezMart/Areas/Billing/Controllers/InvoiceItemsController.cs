﻿using BeezMart.Areas.Billing.ViewModels.InvoiceItems;
using BeezMart.Entities.Billing;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class InvoiceItemsController : Controller
{
    public async Task<PartialViewResult> _OnInvoiceList(int InvoiceID)
    {
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
        if (viewModel.Invoice == null)
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.AppendMessage("The invoice record does not exist in the system: InvoiceID=" + InvoiceID.ToString());
            return PartialView("_Message", msg);
        }
        viewModel.InvoiceItems = await BeezMart.DAL.Billing.InvoiceItems.ListAsync(InvoiceID);
        return PartialView(viewModel);
    }

    [HttpGet]
    public PartialViewResult _OnInvoiceEdit(int? InvoiceID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.InvoiceID = InvoiceID;
        return PartialView(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _OnInvoiceSave(InvoiceItemModel invoiceItem)
    {
        AppMessage msg = new AppMessage();
        TryValidateModel(invoiceItem);
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(invoiceItem.InvoiceID);
        if (viewModel.Invoice == null)
        {
            msg.Type = AppMessageType.Error;
            msg.AppendMessage("The invoice record does not exist in the system: InvoiceID=" + invoiceItem.InvoiceID.ToString());
            return PartialView("_Message", msg);
        }
        if (ModelState.IsValid)
        {
            if (viewModel.Invoice.Status == InvoiceStatus.Draft || viewModel.Invoice.Status == InvoiceStatus.Validated)
            {
                await BeezMart.DAL.Billing.InvoiceItems.SaveAsync(invoiceItem);
                if (viewModel.Invoice.Status == InvoiceStatus.Validated && viewModel.Invoice.ID != null)
                    await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(viewModel.Invoice.ID.Value, InvoiceStatus.Draft);
            }
            else
            {
                msg.Type = AppMessageType.Warning;
                msg.AppendMessage("The invoice cannot be modified since it is not in DRAFT or VALIDATED status!");
                ViewBag.AppMessage = msg.ToHtml();
            }
        }
        else
        {
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
        }
        viewModel.InvoiceItems = await BeezMart.DAL.Billing.InvoiceItems.ListAsync(invoiceItem.InvoiceID);
        return PartialView("_OnInvoiceList", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _OnInvoiceDelete(long ItemID, int InvoiceID)
    {
        AppMessage msg = new AppMessage();
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
        if (viewModel.Invoice == null)
        {
            msg.Type = AppMessageType.Error;
            msg.AppendMessage("The invoice record does not exist in the system: InvoiceID=" + InvoiceID.ToString());
            return PartialView("_Message", msg);
        }
        if (viewModel.Invoice.Status == InvoiceStatus.Draft || viewModel.Invoice.Status == InvoiceStatus.Validated)
        {
            await BeezMart.DAL.Billing.InvoiceItems.DeleteAsync(ItemID);
            if (viewModel.Invoice.Status == InvoiceStatus.Validated && viewModel.Invoice.ID != null)
                await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(viewModel.Invoice.ID.Value, InvoiceStatus.Draft);
        }
        else
        {
            msg.Type = AppMessageType.Warning;
            msg.AppendMessage("The invoice cannot be modified since it is not in DRAFT or VALIDATED status!");
            ViewBag.AppMessage = msg.ToHtml();
        }
        viewModel.InvoiceItems = await BeezMart.DAL.Billing.InvoiceItems.ListAsync(InvoiceID);
        return PartialView("_OnInvoiceList", viewModel);
    }

    public async Task<JsonResult> _GetItem(long ItemID)
    {
        InvoiceItemModel? viewModel = await BeezMart.DAL.Billing.InvoiceItems.RetrieveAsync(ItemID);
        return Json(viewModel);
    }
}

