﻿using BeezMart.Entities.Billing;
using BeezMart.Areas.Billing.ViewModels.Invoices;
using BeezMart.Utils;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class InvoicesController : Controller
{
	protected static string _limitedDefaultLookupResult = "No search criteria is specified. Only the latest {0} invoice records will show up at most.";

	public ActionResult Index()
	{
		return View();
	}

	public async Task<PartialViewResult> _Start(string SearchString)
	{
		if (string.IsNullOrWhiteSpace(SearchString))
		{
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please specify a search string to attempt finding existing invoicing details in the system");
            ViewBag.AppMessage = msg.ToHtmlWarning();
			return PartialView("_Message");
		}
        Start_ViewModel viewModel = new Start_ViewModel();
        await viewModel.LookupOrganizations(SearchString, maxRecords: 10);
        await viewModel.LookupPersons(SearchString, maxRecords: 10);
        await viewModel.LookupCustomerSavedSettings(SearchString, maxRecords: 10);
        await viewModel.LookupInvoices(SearchString, maxRecords: 10);
        return PartialView(viewModel);
	}
	[HttpGet]
	public ViewResult Start()
	{
		// use for adding an invoice starting from a customer, sales order or delivery document
		return View();
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Start(CustomerBillingDetailsModel billingDetails)
	{
		Add_ViewModel viewModel = new Add_ViewModel { };
        viewModel.BillingAddressID = billingDetails.BillingAddressID;
        viewModel.BankAccountID = billingDetails.BankAccountID;
		viewModel.Invoice = new InvoiceModel {
                OrganizationID = billingDetails.OrganizationID,
                PersonID = billingDetails.PersonID,
                CustomerName = billingDetails.CustomerName,
                CustomerCity = billingDetails.City,
                CustomerProvince = billingDetails.StateProvince,
                CustomerAddress = billingDetails.StreetAddress1,
                CustomerRegistrationNumber = billingDetails.RegistrationNumber,
                CustomerTaxID = billingDetails.TaxID,
                CustomerBankName = billingDetails.BankName,
                CustomerBankAccount = billingDetails.BankAccountIBAN,
            };
		viewModel.Invoice.Currency = MyApp.ConfigSettings["Billing.Default-Currency"];
		viewModel.Invoice.VATpercentage = decimal.Parse(MyApp.ConfigSettings["Billing.Default-VAT-Percentage"]);
		viewModel.Invoice.IssueDate = DateTime.Now;
		viewModel.Invoice.DueDate = DateTime.Now.AddDays(int.Parse(MyApp.ConfigSettings["Billing.Default-Payment-Allowance-In-Days"]));
		viewModel.Invoice.Type = InvoiceType.Regular;
		viewModel.HandoverDelegates = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
		viewModel.HandoverTransports = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
		#region auto-populate fields with previously set cookies
		if (Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate"))
		{
			viewModel.Invoice.Detail1 = Request.Cookies["Billing.Invoice.Exchange-Rate"];
			DateTime lastUpdated = DateTime.Now;
			if (Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate-Date"))
			{
				string? tmp = Request.Cookies["Billing.Invoice.Exchange-Rate-Date"];
				if (DateTime.TryParseExact(tmp, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdated))
					viewModel.ExchangeRateDate = lastUpdated;
			}
		}
		if (Request.Cookies.ContainsKey("Billing.Invoice.Operator-Details"))
		{
			viewModel.Invoice.Detail2 = Request.Cookies["Billing.Invoice.Operator-Details"];
		}
		if (Request.Cookies.ContainsKey("Billing.Invoice.Comments"))
		{
			viewModel.Invoice.Detail3 = Request.Cookies["Billing.Invoice.Comments"];
		}
		#endregion
		return View("Add", viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Add()
	{
		Add_ViewModel viewModel = new Add_ViewModel();
		viewModel.Invoice = new InvoiceModel();
		viewModel.Invoice.Currency = MyApp.ConfigSettings["Billing.Default-Currency"];
		viewModel.Invoice.VATpercentage = decimal.Parse(MyApp.ConfigSettings["Billing.Default-VAT-Percentage"], CultureInfo.CurrentUICulture);
		viewModel.Invoice.IssueDate = DateTime.Now;
		viewModel.Invoice.DueDate = DateTime.Now.AddDays(int.Parse(MyApp.ConfigSettings["Billing.Default-Payment-Allowance-In-Days"]));
		viewModel.Invoice.Type = InvoiceType.Regular;
		viewModel.HandoverDelegates = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
		viewModel.HandoverTransports = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
		#region auto-populate fields with previously set cookies
		if (Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate"))
		{
			viewModel.Invoice.Detail1 = Request.Cookies["Billing.Invoice.Exchange-Rate"];
			DateTime lastUpdated = DateTime.Now;
			if (Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate-Date"))
			{
				string? tmp = Request.Cookies["Billing.Invoice.Exchange-Rate-Date"];
				if (DateTime.TryParseExact(tmp, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdated))
					viewModel.ExchangeRateDate = lastUpdated;
			}
		}
		if (Request.Cookies.ContainsKey("Billing.Invoice.Operator-Details"))
		{
			viewModel.Invoice.Detail2 = Request.Cookies["Billing.Invoice.Operator-Details"];
		}
		if (Request.Cookies.ContainsKey("Billing.Invoice.Comments"))
		{
			viewModel.Invoice.Detail3 = Request.Cookies["Billing.Invoice.Comments"];
		}
		#endregion
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Add(Add_ViewModel viewModel)
	{
		TryValidateModel(viewModel);
		if (ModelState.IsValid)
		{
			#region save some fields into cookies for future use
			CookieOptions cookieOptions = new CookieOptions() { Expires = DateTime.Now.AddYears(1) };
			if (Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate"))
			{
				if (viewModel.Invoice?.Detail1 != Request.Cookies["Billing.Invoice.Exchange-Rate"])
				{
                    if (string.IsNullOrWhiteSpace(viewModel.Invoice?.Detail1)) Response.Cookies.Delete("Billing.Invoice.Exchange-Rate");
					else Response.Cookies.Append("Billing.Invoice.Exchange-Rate", viewModel.Invoice.Detail1, cookieOptions);
					if (!Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate-Date"))
					{
						Response.Cookies.Append("Billing.Invoice.Exchange-Rate-Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), cookieOptions);
					}
				}
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(viewModel.Invoice?.Detail1))
				{
					Response.Cookies.Append("Billing.Invoice.Exchange-Rate", viewModel.Invoice.Detail1, cookieOptions);
					Response.Cookies.Append("Billing.Invoice.Exchange-Rate-Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), cookieOptions);
				}
			}
			if (Request.Cookies.ContainsKey("Billing.Invoice.Operator-Details"))
			{
				if (viewModel.Invoice?.Detail2 != Request.Cookies["Billing.Invoice.Operator-Details"])
				{
                    if (string.IsNullOrWhiteSpace(viewModel.Invoice?.Detail2)) Response.Cookies.Delete("Billing.Invoice.Operator-Details");
                    else Response.Cookies.Append("Billing.Invoice.Operator-Details", viewModel.Invoice.Detail2, cookieOptions);
				}
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(viewModel.Invoice?.Detail2))
				{
					Response.Cookies.Append("Billing.Invoice.Operator-Details", viewModel.Invoice.Detail2, cookieOptions);
				}
			}
			if (Request.Cookies.ContainsKey("Billing.Invoice.Comments"))
			{
				if (viewModel.Invoice?.Detail3 != Request.Cookies["Billing.Invoice.Comments"])
				{
                    if (string.IsNullOrWhiteSpace(viewModel.Invoice?.Detail3)) Response.Cookies.Delete("Billing.Invoice.Comments");
                    else Response.Cookies.Append("Billing.Invoice.Comments", viewModel.Invoice.Detail3, cookieOptions);
				}
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(viewModel.Invoice?.Detail3))
				{
					Response.Cookies.Append("Billing.Invoice.Comments", viewModel.Invoice.Detail3, cookieOptions);
				}
			}
			#endregion
			bool success = false;
			if (viewModel.Invoice != null)
				success = await BeezMart.DAL.Billing.Invoices.SaveAsync(viewModel.Invoice);
			if (success)
			{
				return RedirectToAction("Drafts");
			}
		}
		viewModel.HandoverDelegates = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
		viewModel.HandoverTransports = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<JsonResult> _SaveFields(InvoiceModel invoice)
	{
		Dictionary<string, object> result = new Dictionary<string, object>();
		TryValidateModel(invoice);
		if (ModelState.IsValid)
		{
			bool success = await BeezMart.DAL.Billing.Invoices.SaveAsync(invoice);
			if (success && invoice.ID != null)
			{
				result.Add("Message", "Invoice successfully saved.");
				result.Add("InvoiceID", invoice.ID);
			}
			else
			{
				result.Add("Message", "ERROR: Invoice could not be properly saved.");
			}
		}
		else
		{
			result.Add("Message", "ERROR: Invoice fields are incomplete or invalid.");
		}
		return Json(result);
	}

	public async Task<ViewResult> Drafts()
	{
		Drafts_ViewModel viewModel = new Drafts_ViewModel();
		string defaultCurrency = MyApp.ConfigSettings["Billing.Default-Currency"];
		viewModel.InvoicesRegular = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Regular, InvoiceStatus.Draft, defaultCurrency);
		viewModel.InvoicesStorno = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Storno, InvoiceStatus.Draft, defaultCurrency);
		viewModel.InvoicesAdvance = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Advance, InvoiceStatus.Draft, defaultCurrency);
		viewModel.InvoicesRisturn = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Risturn, InvoiceStatus.Draft, defaultCurrency);
		viewModel.InvoicesProForma = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.ProForma, InvoiceStatus.Draft, defaultCurrency);
		viewModel.InvoiceCountsByCurrency = await BeezMart.DAL.Billing.Invoices.GetCountsOfInvoicesByCurrencyAsync(InvoiceStatus.Draft);
		return View(viewModel);
	}
	public async Task<PartialViewResult> _Drafts(InvoiceType Type, string? Currency = null)
	{
		if (string.IsNullOrEmpty(Currency)) Currency = MyApp.ConfigSettings["Billing.Default-Currency"];
		List<InvoiceModel> viewModel = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(Type, InvoiceStatus.Draft, Currency);
		ViewBag.Currency = Currency;
		return PartialView(viewModel);
	}

	public async Task<ViewResult> View(int InvoiceID)
	{
		View_ViewModel viewModel = new View_ViewModel();
		viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
		if (viewModel.Invoice == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The invoice you are looking for could not be found in the database. The record may have been deleted. InvoiceID = " + InvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		viewModel.IsEditable = true;
		if (viewModel.Invoice.Status == InvoiceStatus.Cancelled) viewModel.IsEditable = false;
		if (viewModel.Invoice.Status == InvoiceStatus.Closed) viewModel.IsEditable = false;
		if (viewModel.Invoice.Status == InvoiceStatus.SentToCustomer) viewModel.IsEditable = false;
		viewModel.IsPayable = false;
		if (viewModel.Invoice.Status == InvoiceStatus.SentToCustomer || viewModel.Invoice.Status == InvoiceStatus.Closed)
		{
			if (viewModel.Invoice.Type == InvoiceType.Regular || viewModel.Invoice.Type == InvoiceType.Advance || viewModel.Invoice.Type == InvoiceType.ProForma)
			{
				viewModel.IsPayable = true;
			}
		}
		return View(viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Edit(int InvoiceID)
	{
		Edit_ViewModel viewModel = new Edit_ViewModel();
		viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
		if (viewModel.Invoice == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The invoice you are looking for could not be found in the database. The record may have been deleted. InvoiceID = " + InvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		viewModel.IsEditable = true;
		if (viewModel.Invoice.Status == InvoiceStatus.Cancelled) viewModel.IsEditable = false;
		if (viewModel.Invoice.Status == InvoiceStatus.Closed) viewModel.IsEditable = false;
		if (viewModel.Invoice.Status == InvoiceStatus.SentToCustomer) viewModel.IsEditable = false;
		if (!viewModel.IsEditable)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The invoice you are trying to edit can no longer be modified.");
			msg.AppendMessage(string.Format("It's current status - <b>{0}</b> - does not allow updates. InvoiceID = {1}", viewModel.Invoice.Status, InvoiceID));
			msg.AppendLinkButton("Back to invoice view", Url.Action("View", "Invoices", new { Area = "Billing", InvoiceID = viewModel.Invoice.ID }));
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		viewModel.HandoverDelegates = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
		viewModel.HandoverTransports = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
	{
		TryValidateModel(viewModel);
		if (viewModel.Invoice == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("There was a problem with the submitted fields: it seems no invoice object could be created from the fields submitted.");
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		if (ModelState.IsValid && (viewModel.Invoice.Status == InvoiceStatus.Draft || viewModel.Invoice.Status == InvoiceStatus.Validated))
		{
			#region save some fields into cookies for future use
			CookieOptions cookieOptions = new CookieOptions() { Expires = DateTime.Now.AddYears(1) };
			if (Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate"))
			{
				if (viewModel.Invoice.Detail1 != Request.Cookies["Billing.Invoice.Exchange-Rate"])
				{
                    if (string.IsNullOrWhiteSpace(viewModel.Invoice.Detail1)) Response.Cookies.Delete("Billing.Invoice.Exchange-Rate");
					else Response.Cookies.Append("Billing.Invoice.Exchange-Rate", viewModel.Invoice.Detail1, cookieOptions);
					if (!Request.Cookies.ContainsKey("Billing.Invoice.Exchange-Rate-Date"))
					{
						Response.Cookies.Append("Billing.Invoice.Exchange-Rate-Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), cookieOptions);
					}
				}
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(viewModel.Invoice.Detail1))
				{
					Response.Cookies.Append("Billing.Invoice.Exchange-Rate", viewModel.Invoice.Detail1, cookieOptions);
					Response.Cookies.Append("Billing.Invoice.Exchange-Rate-Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), cookieOptions);
				}
			}
			if (Request.Cookies.ContainsKey("Billing.Invoice.Operator-Details"))
			{
				if (viewModel.Invoice.Detail2 != Request.Cookies["Billing.Invoice.Operator-Details"])
				{
                    if (string.IsNullOrWhiteSpace(viewModel.Invoice.Detail2)) Response.Cookies.Delete("Billing.Invoice.Operator-Details");
					else Response.Cookies.Append("Billing.Invoice.Operator-Details", viewModel.Invoice.Detail2, cookieOptions);
				}
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(viewModel.Invoice.Detail2))
				{
					Response.Cookies.Append("Billing.Invoice.Operator-Details", viewModel.Invoice.Detail2, cookieOptions);
				}
			}
			if (Request.Cookies.ContainsKey("Billing.Invoice.Comments"))
			{
				if (viewModel.Invoice.Detail3 != Request.Cookies["Billing.Invoice.Comments"])
				{
                    if (string.IsNullOrWhiteSpace(viewModel.Invoice.Detail3)) Response.Cookies.Delete("Billing.Invoice.Comments");
					else Response.Cookies.Append("Billing.Invoice.Comments", viewModel.Invoice.Detail3, cookieOptions);
				}
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(viewModel.Invoice.Detail3))
				{
					Response.Cookies.Append("Billing.Invoice.Comments", viewModel.Invoice.Detail3, cookieOptions);
				}
			}
			#endregion
			viewModel.Invoice.Status = InvoiceStatus.Draft;
			bool success = await BeezMart.DAL.Billing.Invoices.SaveAsync(viewModel.Invoice);
			if (success)
			{
				return RedirectToAction("View", new { InvoiceID = viewModel.Invoice.ID });
			}
		}
		viewModel.HandoverDelegates = await BeezMart.DAL.Billing.HandoverDelegates.ListAsync();
		viewModel.HandoverTransports = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
		return View(viewModel);
	}

	[HttpGet]
	public ViewResult Duplicate(int InvoiceID)
	{
		Duplicate_ViewModel viewModel = new Duplicate_ViewModel();
		viewModel.ExistingInvoiceID = InvoiceID;
		return View(viewModel);
	}
	[HttpPost]
	public async Task<ActionResult> Duplicate(Duplicate_ViewModel viewModel)
	{
		InvoiceModel? newInvoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(viewModel.ExistingInvoiceID);
		if (newInvoice == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The invoice you are trying to duplicate for could not be found in the database. The record may have been deleted. InvoiceID = " + viewModel.ExistingInvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		string? InitialInvoice_AccountingID = newInvoice.AccountingID;
		int? InitialInvoice_ID = newInvoice.ID;
		newInvoice.ID = null;
		newInvoice.IssueDate = DateTime.Now;
		newInvoice.DueDate = DateTime.Now.AddDays(int.Parse(MyApp.ConfigSettings["Billing.Default-Payment-Allowance-In-Days"]));
		newInvoice.ClosingDate = null;
		newInvoice.Status = InvoiceStatus.Draft;
		newInvoice.AccountingID = null;
		if (!string.IsNullOrEmpty(newInvoice.TransportDetail3)) newInvoice.TransportDetail3 = DateTime.Now.ToString("yyyy-MM-dd");
		if (!string.IsNullOrEmpty(newInvoice.TransportDetail4)) newInvoice.TransportDetail4 = DateTime.Now.ToString("hh:mm");
		newInvoice.Detail4 = $"This invoice was started as a duplicate of invoice '{InitialInvoice_AccountingID}' (ID {InitialInvoice_ID}).\n{newInvoice.Detail4}";
		await BeezMart.DAL.Billing.Invoices.SaveAsync(newInvoice);
		List<InvoiceItemModel> invoiceItems = await BeezMart.DAL.Billing.InvoiceItems.ListAsync(viewModel.ExistingInvoiceID);
		if (newInvoice.ID != null)
		{
			foreach (InvoiceItemModel invoiceItem in invoiceItems)
			{
				invoiceItem.ID = null;
				invoiceItem.InvoiceID = newInvoice.ID.Value;
				await BeezMart.DAL.Billing.InvoiceItems.SaveAsync(invoiceItem);
			}
		}
		return RedirectToAction("Edit", new { InvoiceID = newInvoice.ID });
	}

	[HttpGet]
	public ViewResult Stornate(int InvoiceID)
	{
		Stornate_ViewModel viewModel = new Stornate_ViewModel();
		viewModel.ExistingInvoiceID = InvoiceID;
		return View(viewModel);
	}
	[HttpPost]
	public async Task<ActionResult> Stornate(Duplicate_ViewModel viewModel)
	{
		InvoiceModel? newInvoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(viewModel.ExistingInvoiceID);
		if (newInvoice == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The invoice you are trying to stornate for could not be found in the database. The record may have been deleted. InvoiceID = " + viewModel.ExistingInvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		string? InitialInvoice_AccountingID = newInvoice.AccountingID;
		int? InitialInvoice_ID = newInvoice.ID;
		newInvoice.ID = null;
		newInvoice.IssueDate = DateTime.Now;
		newInvoice.DueDate = DateTime.Now.AddDays(int.Parse(MyApp.ConfigSettings["Billing.Default-Payment-Allowance-In-Days"]));
		newInvoice.ClosingDate = null;
		newInvoice.Status = InvoiceStatus.Draft;
		newInvoice.Type = InvoiceType.Storno;
		newInvoice.AccountingID = null;
		if (!string.IsNullOrEmpty(newInvoice.TransportDetail3)) newInvoice.TransportDetail3 = DateTime.Now.ToString("yyyy-MM-dd");
		if (!string.IsNullOrEmpty(newInvoice.TransportDetail4)) newInvoice.TransportDetail4 = DateTime.Now.ToString("hh:mm");
		newInvoice.Detail4 = $"This invoice was started as a storno of invoice '{InitialInvoice_AccountingID}' (ID {InitialInvoice_ID}).\n{newInvoice.Detail4}";
		await BeezMart.DAL.Billing.Invoices.SaveAsync(newInvoice);
		List<InvoiceItemModel> invoiceItems = await BeezMart.DAL.Billing.InvoiceItems.ListAsync(viewModel.ExistingInvoiceID);
		if (newInvoice.ID != null)
		{
			foreach (InvoiceItemModel invoiceItem in invoiceItems)
			{
				invoiceItem.ID = null;
				invoiceItem.InvoiceID = newInvoice.ID.Value;
				invoiceItem.Quantity = -invoiceItem.Quantity;
				await BeezMart.DAL.Billing.InvoiceItems.SaveAsync(invoiceItem);
			}
        }
		return RedirectToAction("Edit", new { InvoiceID = newInvoice.ID });
	}

	public async Task<ViewResult> Preview(int InvoiceID)
	{
		AppMessage? msg = null;
		Preview_ViewModel viewModel = new Preview_ViewModel();
		viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
		if (viewModel.Invoice == null)
		{
			msg = new AppMessage();
			msg.IsDismisable = false;
			msg.AppendMessage("The invoice you are looking for could not be found in the database. The record may have been deleted. InvoiceID = " + InvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		HtmlDocument invoiceTemplateHtmlDocument = new HtmlDocument();
		if (string.IsNullOrWhiteSpace(MyApp.PhysicalDirSourceRoot))
			throw new NullReferenceException("MyApp.PhysicalDirSourceRoot is NULL; can't determine location of invoice template file, ???/Content/Invoice_Template.html.");
        StreamReader streamReader = System.IO.File.OpenText(Path.Combine(MyApp.PhysicalDirSourceRoot, "Content/Invoice_Template.html"));
		invoiceTemplateHtmlDocument.Load(streamReader);
        streamReader.Dispose();
		viewModel.InvoiceCssStyles = PrinterVersionHtmlStyles(invoiceTemplateHtmlDocument);
		viewModel.InvoiceHtml = await PrinterVersionHtmlElement(invoiceTemplateHtmlDocument, viewModel.Invoice);
		if (viewModel.Invoice.Status == InvoiceStatus.Draft || viewModel.Invoice.Status == InvoiceStatus.Validated || viewModel.Invoice.Status == InvoiceStatus.NotSet)
		{
			msg = await this.ValidateForPrint(viewModel.Invoice);
			msg.IsDismisable = false;
			if (msg.Count > 0)
			{
				msg.Type = AppMessageType.Warning;
				msg.Title = "Not ready for print: fields validation failed";
				msg.AppendLinkButton("<b class='bi-chevron-left'></b> Back to invoice", Url.Action("View", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-warning");
                msg.AppendLinkButton("<b class='bi-chevron-left'></b> Edit invoice fields", Url.Action("Edit", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-primary");
                ViewBag.AppMessage = msg.ToHtml();
			}
			else
			{
				if (viewModel.Invoice.Status == InvoiceStatus.Draft || viewModel.Invoice.Status == InvoiceStatus.NotSet)
				{
					if (viewModel.Invoice.ID != null)
						await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(viewModel.Invoice.ID.Value, InvoiceStatus.Validated, updatedBy: this.User.Identity?.Name);
				}
				msg.Title = "Ready for print";
				msg.AppendMessage("This invoice is now validated for print - all required fields are present and filled in. You may download the printer version now.");
				//msg.AppendMessage("<b>PLEASE NOTE</b>: downloading the printer version automatically marks the invoice as Pending, or sent to customer. No further edits on the invoice will be allowed from Pending stage on!");
				msg.AppendLinkButton("<b class='bi-chevron-left'></b> Back to invoice", Url.Action("View", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-warning");
				msg.AppendLinkButton("<b class='bi-download'></b> Get printer version", Url.Action("Print", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-primary");
				msg.Type = AppMessageType.Success;
				ViewBag.AppMessage = msg.ToHtml();
			}
		}
		else
		{
			msg = new AppMessage(AppMessageType.Success);
			msg.IsDismisable = false;
			msg.Title = "Ready for print";
			msg.AppendMessage("You may re-print a copy of the invoice.");
			msg.AppendLinkButton("<b class='bi-chevron-left'></b> Back to invoice", Url.Action("View", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-warning");
			msg.AppendLinkButton("<b class='bi-download'></b> Get printer version", Url.Action("PrinterVersionHtml", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-primary");
			ViewBag.AppMessage = msg.ToHtml();
		}
		return View(viewModel);
	}

	public async Task<ActionResult> Print(int InvoiceID)
	{
		AppMessage? msg = null;
		Print_ViewModel viewModel = new Print_ViewModel();
		viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
		if (viewModel.Invoice == null)
		{
			msg = new AppMessage();
			msg.AppendMessage("The invoice you are looking for could not be found in the database. The record may have been deleted. InvoiceID = " + InvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			return View("Message", msg);
		}
		InvoiceStatus initialStatus = viewModel.Invoice.Status;
		if (initialStatus == InvoiceStatus.Draft || initialStatus == InvoiceStatus.Validated || initialStatus == InvoiceStatus.NotSet)
		{
			msg = await this.ValidateForPrint(viewModel.Invoice);
			if (msg.Count > 0)
			{
				msg.Type = AppMessageType.Warning;
				msg.Title = "Not ready for print: fields validation failed";
				if (viewModel.Invoice.TotalDue == 0M) msg.AppendLinkButton("<b class='bi-chevron-left'></b> Back to invoice", Url.Action("View", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-warning");
				else msg.AppendLinkButton("<b class='bi-chevron-left'></b> Back to invoice", Url.Action("Edit", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-warning");
				ViewBag.AppMessage = msg.ToHtml();
			}
			else
			{
				await BeezMart.DAL.Billing.Invoices.AllocateAccountingIDAsync(viewModel.Invoice);
				if (viewModel.Invoice.ID != null)
					await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(viewModel.Invoice.ID.Value, InvoiceStatus.SentToCustomer, updatedBy: this.User.Identity?.Name);
				viewModel.Invoice.Status = InvoiceStatus.SentToCustomer;
			}
		}
		HtmlDocument invoiceTemplateHtmlDocument = new HtmlDocument();
		if (string.IsNullOrWhiteSpace(MyApp.PhysicalDirSourceRoot))
			throw new NullReferenceException("MyApp.PhysicalDirSourceRoot is NULL; can't determine location of invoice template file, ???/Content/Invoice_Template.html.");
		StreamReader streamReader = System.IO.File.OpenText(Path.Combine(MyApp.PhysicalDirSourceRoot, "Content/Invoice_Template.html"));
		invoiceTemplateHtmlDocument.Load(streamReader);
        streamReader.Dispose();
        viewModel.InvoiceCssStyles = PrinterVersionHtmlStyles(invoiceTemplateHtmlDocument);
		viewModel.InvoiceHtml = await PrinterVersionHtmlElement(invoiceTemplateHtmlDocument, viewModel.Invoice);
		if (msg == null) msg = new AppMessage(AppMessageType.Success);
		if (msg.Count == 0)
		{
			viewModel.IsPrintable = true;
			msg.Title = "Ready for print";
			if (initialStatus == InvoiceStatus.Draft || initialStatus == InvoiceStatus.Validated || initialStatus == InvoiceStatus.NotSet)
			{
				msg.AppendMessage("This invoice was checked and is valid for print - all required fields are present and filled in.");
				msg.AppendMessage("Please note that the invoice is now considered printed and it was marked in the system as pending, or sent to customer. Therefore the invoice cannot be modified anymore. You can only change the status cancelled or closed.");
			}
			else
			{
				msg.AppendMessage("The current status of the invoice allows re-printing. Remember that once an invoice is sent to the customer, it cannot be modified anymore.");
			}
			msg.AppendLinkButton("<b class='bi-chevron-left'></b> Back to invoice", Url.Action("View", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-warning");
			msg.AppendLinkButton("<b class='bi-download'></b> Download printer version", Url.Action("PrinterVersionHtml", new { InvoiceID = viewModel.Invoice.ID }), "btn btn-primary");
			ViewBag.AppMessage = msg.ToHtml();
		}
		return View(viewModel);
	}

	public async Task PrinterVersionHtml(int InvoiceID)
	{
		AppMessage msg;
		InvoiceModel? invoiceModel = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
		if (invoiceModel == null || invoiceModel.ID == null)
		{
			msg = new AppMessage();
			msg.AppendMessage("The invoice you are looking for could not be found in the database. The record may have been deleted. InvoiceID = " + InvoiceID.ToString());
			msg.Type = AppMessageType.Error;
			RedirectToAction("Message", "Home", new { message = msg, Area = string.Empty });
		}
		else
		{
			if (invoiceModel.Status == InvoiceStatus.Draft || invoiceModel.Status == InvoiceStatus.NotSet)
			{
				RedirectToAction("Preview", new { InvoiceID = invoiceModel.ID });
			}
			#region fill in invoice fields
			HtmlDocument invoiceTemplateHtmlDocument = new HtmlDocument();
			if (string.IsNullOrWhiteSpace(MyApp.PhysicalDirSourceRoot))
				throw new NullReferenceException("MyApp.PhysicalDirSourceRoot is NULL; can't determine location of invoice template file, ???/Content/Invoice_Template.html.");
			FileStream fileStream_invoiceTemplate = new FileStream(Path.Combine(MyApp.PhysicalDirSourceRoot, "Content/Invoice_Template.html"), FileMode.Open, FileAccess.Read);
			invoiceTemplateHtmlDocument.Load(fileStream_invoiceTemplate);
			HtmlNode bodyNode = invoiceTemplateHtmlDocument.CreateElement("body");
			bodyNode.InnerHtml = await PrinterVersionHtmlElement(invoiceTemplateHtmlDocument, invoiceModel);
			HtmlNode htmlNode = invoiceTemplateHtmlDocument.DocumentNode.SelectSingleNode("/html");
			htmlNode.ChildNodes["body"].Remove();
			htmlNode.AppendChild(bodyNode);
			#endregion
			Response.Clear();
			string invoiceName = string.IsNullOrWhiteSpace(invoiceModel.AccountingID) ? invoiceModel.ID.Value.ToString() : invoiceModel.AccountingID.Replace(' ', '-');
			string invoiceDate = invoiceModel.IssueDate.ToString("yyyy-MM-dd");
			string fileName = $"BeezMart-invoice_{invoiceName}_issued_{invoiceDate}";
			Response.ContentType = "text/html";
			Response.Headers.Append("Content-Disposition", $"attachment; filename={fileName}.html");
			using (MemoryStream memoryStream = new MemoryStream())
			{
				invoiceTemplateHtmlDocument.Save(memoryStream, encoding: System.Text.Encoding.Unicode);
				await Response.BodyWriter.WriteAsync(memoryStream.ToArray());
			}
		}
	}

    public async Task<ViewResult> Recent(int Count)
    {
        List<InvoiceModel> viewModel = await BeezMart.DAL.Billing.Invoices.ListLatestAsync(100);
        return View(viewModel);
    }

	public async Task<ViewResult> Validated()
	{
		Validated_ViewModel viewModel = new Validated_ViewModel();
		string defaultCurrency = MyApp.ConfigSettings["Billing.Default-Currency"];
		viewModel.InvoicesRegular = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Regular, InvoiceStatus.Validated, defaultCurrency);
		viewModel.InvoicesStorno = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Storno, InvoiceStatus.Validated, defaultCurrency);
		viewModel.InvoicesAdvance = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Advance, InvoiceStatus.Validated, defaultCurrency);
		viewModel.InvoicesRisturn = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Risturn, InvoiceStatus.Validated, defaultCurrency);
		viewModel.InvoicesProForma = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.ProForma, InvoiceStatus.Validated, defaultCurrency);
		viewModel.InvoiceCountsByCurrency = await BeezMart.DAL.Billing.Invoices.GetCountsOfInvoicesByCurrencyAsync(InvoiceStatus.Validated);
		return View(viewModel);
	}
	public async Task<PartialViewResult> _Validated(InvoiceType Type, string? Currency = null)
	{
		if (string.IsNullOrEmpty(Currency)) Currency = MyApp.ConfigSettings["Billing.Default-Currency"];
		List<InvoiceModel> viewModel = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(Type, InvoiceStatus.Validated, Currency);
		ViewBag.Currency = Currency;
		return PartialView(viewModel);
	}

	public async Task<ViewResult> Pending()
	{
		Pending_ViewModel viewModel = new Pending_ViewModel();
		string defaultCurrency = MyApp.ConfigSettings["Billing.Default-Currency"];
		viewModel.InvoicesRegular = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Regular, InvoiceStatus.SentToCustomer, defaultCurrency);
		viewModel.InvoicesStorno = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Storno, InvoiceStatus.SentToCustomer, defaultCurrency);
		viewModel.InvoicesAdvance = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Advance, InvoiceStatus.SentToCustomer, defaultCurrency);
		viewModel.InvoicesRisturn = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.Risturn, InvoiceStatus.SentToCustomer, defaultCurrency);
		viewModel.InvoicesProForma = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(InvoiceType.ProForma, InvoiceStatus.SentToCustomer, defaultCurrency);
		viewModel.InvoiceCountsByCurrency = await BeezMart.DAL.Billing.Invoices.GetCountsOfInvoicesByCurrencyAsync(InvoiceStatus.SentToCustomer);
		return View(viewModel);
	}
	public async Task<PartialViewResult> _Pending(InvoiceType Type, string? Currency = null)
	{
		if (string.IsNullOrEmpty(Currency)) Currency = MyApp.ConfigSettings["Billing.Default-Currency"];
		List<InvoiceModel> viewModel = await BeezMart.DAL.Billing.Invoices.ListFilteredByTypeAndStatusAsync(Type, InvoiceStatus.SentToCustomer, Currency);
		ViewBag.Currency = Currency;
		return PartialView(viewModel);
	}
	public async Task<PartialViewResult> _PendingCountByCurrency()
	{
		Dictionary<string, int> viewModel = await BeezMart.DAL.Billing.Invoices.GetCountsOfInvoicesByCurrencyAsync(InvoiceStatus.SentToCustomer);
		return PartialView(viewModel);
	}

	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<PartialViewResult> _ChangeStatus(int InvoiceID, InvoiceStatus NewStatus)
	{
		AppMessage msg = new AppMessage();
		msg.IsDismisable = false;
		InvoiceModel? invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
		_ChangeStatus_ViewModel viewModel = new _ChangeStatus_ViewModel();
		viewModel.Success = false;
		viewModel.RemainingToBePayed = 0M;
		if (invoice == null || invoice.ID == null)
		{
			msg.AppendMessage("The invoice you are looking for could not be found in the database. The record may have been deleted. InvoiceID = " + InvoiceID.ToString());
			viewModel.Message = msg.ToHtmlError();
			viewModel.InvoiceStatus = InvoiceStatus.Draft;
		}
		else
		{
			viewModel.InvoiceID = invoice.ID.Value;
			viewModel.RemainingToBePayed = invoice.RemainingToBePayed;
			AppMessage validationMessages;
			switch (invoice.Status)
			{
				case InvoiceStatus.Draft:
					#region goes to Validated
					if (NewStatus != InvoiceStatus.Validated)
					{
						msg.AppendMessage("A draft invoice can only be Validated for print or deleted from system. It cannot change to any other status. InvoiceID = " + InvoiceID.ToString());
						viewModel.Message = msg.ToHtmlError();
						break;
					}
					validationMessages = await ValidateForPrint(invoice);
					validationMessages.IsDismisable = false;
					if (validationMessages.Count == 0)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.Validated, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now Validated against business rules and it is ready for print.");
							msg.AppendMessage("<b>PLEASE NOTE</b>: downloading the printer version through <span>Print for sending</span> page automatically marks the invoice as <span>Pending</span>, or <i>sent to customer</i>. No further edits on the invoice will be allowed from Pending stage on!");
							msg.AppendLinkButton("<b class='bi-search'></b> Print preview", Url.Action("Preview", new { InvoiceID = invoice.ID.Value }));
							msg.AppendLinkButton("<b class='bi-download'></b> Print for sending", Url.Action("Print", new { InvoiceID = invoice.ID.Value }));
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.Validated;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Validated. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
					}
					else
					{
						if (validationMessages.Count > 1) validationMessages.AppendLink("Edit the invoice fields", Url.Action("Edit", new { InvoiceID = invoice.ID.Value }));
						viewModel.Message = validationMessages.ToHtmlWarning();
					}
					#endregion
					break;
				case InvoiceStatus.Validated:
					#region goes to SentToCustomer or back to Draft
					if (NewStatus != InvoiceStatus.SentToCustomer && NewStatus != InvoiceStatus.Draft)
					{
						msg.AppendMessage("A validated invoice can only be either printed (and marked as Pending, sent to customer), returned in Draft status, or deleted completely from system. It cannot change to any other status. InvoiceID = " + InvoiceID.ToString());
						viewModel.Message = msg.ToHtmlError();
						break;
					}
                    if (NewStatus == InvoiceStatus.Draft)
                    {
                        bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.Draft, updatedBy: this.User.Identity?.Name);
                        msg.AppendMessage("The invoice has now returned to <span>Draft</span> status. You can edit its fields and items.");
                        viewModel.Message = msg.ToHtmlSuccess();
                        invoice.Status = InvoiceStatus.Draft;
                        break;
                    }
					validationMessages = await ValidateForPrint(invoice);
					validationMessages.IsDismisable = false;
					if (validationMessages.Count == 0)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.SentToCustomer, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now marked as <span>Pending</span>, or <i>sent to customer</i>. You can always print a copy of it.");
							msg.AppendMessage("However, from this stage on no further edits are allowed. Only payments for the invoice may be recorded.");
							msg.AppendLinkButton("<b class='bi-download'></b> Print for sending", Url.Action("Print", new { InvoiceID = invoice.ID.Value }));
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.SentToCustomer;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Pending, sent to customer. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
					}
					else
					{
						if (validationMessages.Count > 1) validationMessages.AppendLink("Edit the invoice fields", Url.Action("Edit", new { InvoiceID = invoice.ID.Value }));
						viewModel.Message = validationMessages.ToHtmlWarning();
					}
					#endregion
					break;
				case InvoiceStatus.SentToCustomer:
					#region goes either in Closed or Cancelled
					if (NewStatus != InvoiceStatus.Cancelled && NewStatus != InvoiceStatus.Closed)
					{
						msg.AppendMessage("A Pending, sent to customer invoice can only be either Cancelled or Closed (paid and archived). It cannot change to any other status. InvoiceID = " + InvoiceID.ToString());
						viewModel.Message = msg.ToHtmlError();
						break;
					}
					if (NewStatus == InvoiceStatus.Cancelled)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.Cancelled, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now Cancelled. Its status has been changed accordingly and it has been moved to archived invoices.");
							msg.Type = AppMessageType.Success;
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.Cancelled;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Cancelled. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
						break;
					}
					if (NewStatus == InvoiceStatus.Closed)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.Closed, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now Closed. Its status has been changed accordingly and it has been moved to archived invoices.");
							if (invoice.RemainingToBePayed > 1M)
							{
								msg.AppendMessage(string.Format("<b>Please note</b>, however, that the payments recorded for the invoice do not cover all the invoice due amount. There are still <b>{0} {1}</b> to be payed.", invoice.RemainingToBePayed.ToString("#,##0.00"), invoice.Currency?.ToUpper()));
								msg.AppendLinkButton("Review the invoice", Url.Action("View", new { InvoiceID = invoice.ID.Value }));
							}
							msg.Type = AppMessageType.Success;
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.Closed;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Closed. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
						break;
					}
					#endregion
					break;
				case InvoiceStatus.Cancelled:
					#region goes either in SentToCustomer or Closed
					if (NewStatus != InvoiceStatus.SentToCustomer && NewStatus != InvoiceStatus.Closed)
					{
						msg.AppendMessage("A cancelled invoice can only be marked as Closed or brought back from archive as Pending, sent to customer. It cannot change to any other status. InvoiceID = " + InvoiceID.ToString());
						viewModel.Message = msg.ToHtmlError();
						break;
					}
					if (NewStatus == InvoiceStatus.SentToCustomer)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.SentToCustomer, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now back marked as Pending, sent to customer. However, no edits are allowed in this stage.");
							msg.AppendLinkButton("Get print version", Url.Action("Print", new { InvoiceID = invoice.ID.Value }));
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.SentToCustomer;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Pending, sent to customer. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
						break;
					}
					if (NewStatus == InvoiceStatus.Closed)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.Closed, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now Closed. Its status has been changed accordingly, the invoice is still among archived ones.");
							if (invoice.RemainingToBePayed > 1M)
							{
								msg.AppendMessage(string.Format("<b>Please note</b>, however, that the payments recorded for the invoice do not cover all the invoice due amount. There are still <b>{0} {1}</b> to be payed.", invoice.RemainingToBePayed.ToString("#,##0.00"), invoice.Currency?.ToUpper()));
								msg.AppendLinkButton("Review the invoice", Url.Action("View", new { InvoiceID = invoice.ID.Value }));
							}
							msg.Type = AppMessageType.Success;
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.Closed;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Closed. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
						break;
					}
					#endregion
					break;
				case InvoiceStatus.Closed:
					#region goes either in SentToCustomer or Cancelled
					if (NewStatus != InvoiceStatus.SentToCustomer && NewStatus != InvoiceStatus.Cancelled)
					{
						msg.AppendMessage("A closed invoice can only be brought back from archive as Pending, sent to customer. It cannot change to any other status. InvoiceID = " + InvoiceID.ToString());
						viewModel.Message = msg.ToHtmlError();
						break;
					}
					if (NewStatus == InvoiceStatus.SentToCustomer)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.SentToCustomer, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now back marked as Pending, sent to customer. However, no edits are allowed in this stage.");
							msg.AppendLinkButton("Get print version", Url.Action("Print", new { InvoiceID = invoice.ID.Value }));
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.SentToCustomer;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Pending, sent to customer. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
						break;
					}
					if (NewStatus == InvoiceStatus.Cancelled)
					{
						bool success = await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.Cancelled, updatedBy: this.User.Identity?.Name);
						if (success)
						{
							viewModel.Success = true;
							msg.AppendMessage("The invoice is now marked as Cancelled. Its status has been changed accordingly, the invoice is still among archived ones.");
							msg.Type = AppMessageType.Success;
							viewModel.Message = msg.ToHtmlSuccess();
                            invoice.Status = InvoiceStatus.Cancelled;
						}
						else
						{
							msg.AppendMessage("The invoice status could not be changed to Cancelled. Please contact support. InvoiceID = " + InvoiceID.ToString());
							viewModel.Message = msg.ToHtmlError();
						}
						break;
					}
					#endregion
					break;
			}
			viewModel.InvoiceStatus = invoice.Status;
		}
		return PartialView(viewModel);
	}

	public async Task<ViewResult> Overdue()
	{
		Overdue_ViewModel viewModel = new Overdue_ViewModel();
		string defaultCurrency = MyApp.ConfigSettings["Billing.Default-Currency"];
		viewModel.InvoicesRegular = await BeezMart.DAL.Billing.Invoices.ListOverdueFilteredByTypeAsync(InvoiceType.Regular, defaultCurrency);
		viewModel.InvoicesStorno = await BeezMart.DAL.Billing.Invoices.ListOverdueFilteredByTypeAsync(InvoiceType.Storno, defaultCurrency);
		viewModel.InvoicesAdvance = await BeezMart.DAL.Billing.Invoices.ListOverdueFilteredByTypeAsync(InvoiceType.Advance, defaultCurrency);
		viewModel.InvoicesRisturn = await BeezMart.DAL.Billing.Invoices.ListOverdueFilteredByTypeAsync(InvoiceType.Risturn, defaultCurrency);
		viewModel.InvoicesProForma = await BeezMart.DAL.Billing.Invoices.ListOverdueFilteredByTypeAsync(InvoiceType.ProForma, defaultCurrency);
		viewModel.InvoiceCountsByCurrency = await BeezMart.DAL.Billing.Invoices.GetCountsOfOverdueInvoicesByCurrencyAsync();
		return View(viewModel);
	}
	public async Task<PartialViewResult> _Overdue(InvoiceType Type, string? Currency = null)
	{
		if (string.IsNullOrEmpty(Currency)) Currency = MyApp.ConfigSettings["Billing.Default-Currency"];
		List<InvoiceModel> viewModel = await BeezMart.DAL.Billing.Invoices.ListOverdueFilteredByTypeAsync(Type, Currency);
		ViewBag.Currency = Currency;
		return PartialView(viewModel);
	}

	public async Task<ViewResult> Search()
	{
		Search_ViewModel viewModel = new Search_ViewModel();
		AppMessage msg = new AppMessage();
		if (Request.QueryString.HasValue)
		{
			await TryUpdateModelAsync<Search_ViewModel>(viewModel);
		}
		return View(viewModel);
	}
    public async Task<PartialViewResult> _Search(Search_Params parameters)
    {
        InvoiceModel.LookupFilter filter;
        List<InvoiceModel> viewModel;
		ViewBag.InvoiceType = parameters.Type;
        if (!parameters.FilterIsSet)
        {
            AppMessage msg = new AppMessage();
			string maxRowCount = MyApp.ConfigSettings["Billing.Search.Result.MaxCount.Unfiltered"];
			msg.AppendMessage(string.Format(_limitedDefaultLookupResult, maxRowCount));
            ViewBag.AppMessage = msg.ToHtmlWarning();
            short maxRows = short.Parse(MyApp.ConfigSettings["Billing.Search.Result.MaxCount.Unfiltered"]);
            filter = new InvoiceModel.LookupFilter
            {
                InvoiceType = parameters.Type
            };
            viewModel = await BeezMart.DAL.Billing.Invoices.LookupAsync(filter, maxRows);
            return PartialView(viewModel);
        }
        List<int>? InvoiceIDs = null;
        List<string>? AccountingIDs = null;
        #region split IDs into lists
        if (!string.IsNullOrEmpty(parameters.IDs))
        {
            char[] separators = { ';', ',', '|', ' ' };
            int invoiceID;
            string[] separateIDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            foreach (string invoiceIDstring in separateIDs)
            {
                if (int.TryParse(invoiceIDstring, out invoiceID))
                {
                    if (InvoiceIDs == null) InvoiceIDs = new List<int>();
                    InvoiceIDs.Add(invoiceID);
                }
            }
        }
        if (!string.IsNullOrEmpty(parameters.AccIDs))
        {
            char[] separators = { ';', ',', '|' };
            string accountingID;
            string[] separateIDs = parameters.AccIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            foreach (string accountingIDstring in separateIDs)
            {
                accountingID = accountingIDstring.Trim();
                if (accountingID.Length > 0)
                {
                    if (AccountingIDs == null) AccountingIDs = new List<string>();
                    AccountingIDs.Add(accountingID);
                }
            }
        }
        #endregion
        filter = new InvoiceModel.LookupFilter()
        {
            CustomerNamePattern = parameters.Customer,
            CustomerNameExactMatch = parameters.Exact,
            OrganizationID = parameters.OrganizationID,
            PersonID = parameters.PersonID,
            IssuedAfter = parameters.After,
            IssuedBefore = parameters.Before,
            Currency = parameters.Curr,
            InvoiceIDs = InvoiceIDs,
            AccountingIDs = AccountingIDs,
            InvoiceStatus = parameters.Status,
            Overdue = parameters.Overdue,
            InvoiceType = parameters.Type
        };
        viewModel = await BeezMart.DAL.Billing.Invoices.LookupAsync(filter);
        return PartialView(viewModel);
    }
	public async Task<IActionResult> _Search_Export(Search_Params parameters)
	{
		InvoiceModel.LookupFilter filter;
        List<InvoiceModel> records;
		if (parameters.FilterIsSet)
		{
			List<int>? InvoiceIDs = null;
			List<string>? AccountingIDs = null;
			#region split IDs into lists
			if (!string.IsNullOrEmpty(parameters.IDs))
			{
				char[] separators = { ';', ',', '|', ' ' };
				int invoiceID;
				string[] separateIDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				foreach (string invoiceIDstring in separateIDs)
				{
					if (int.TryParse(invoiceIDstring, out invoiceID))
					{
						if (InvoiceIDs == null) InvoiceIDs = new List<int>();
						InvoiceIDs.Add(invoiceID);
					}
				}
			}
			if (!string.IsNullOrEmpty(parameters.AccIDs))
			{
				char[] separators = { ';', ',', '|' };
				string accountingID;
				string[] separateIDs = parameters.AccIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				foreach (string accountingIDstring in separateIDs)
				{
					accountingID = accountingIDstring.Trim();
					if (accountingID.Length > 0)
					{
						if (AccountingIDs == null) AccountingIDs = new List<string>();
						AccountingIDs.Add(accountingID);
					}
				}
			}
			#endregion
			filter = new InvoiceModel.LookupFilter()
				{
					CustomerNamePattern = parameters.Customer,
					CustomerNameExactMatch = parameters.Exact,
					OrganizationID = parameters.OrganizationID,
					PersonID = parameters.PersonID,
					IssuedAfter = parameters.After,
					IssuedBefore = parameters.Before,
					Currency = parameters.Curr,
					InvoiceIDs = InvoiceIDs,
					AccountingIDs = AccountingIDs,
					InvoiceStatus = parameters.Status,
					Overdue = parameters.Overdue,
					InvoiceType = parameters.Type
				};
			records = await BeezMart.DAL.Billing.Invoices.LookupAsync(filter);
		}
		else
        {
            short maxRows = short.Parse(MyApp.ConfigSettings["Billing.Search.Result.MaxCount.Unfiltered"]);
            filter = new InvoiceModel.LookupFilter
				{
					InvoiceType = parameters.Type
				};
			records = await BeezMart.DAL.Billing.Invoices.LookupAsync(filter, maxRows);
        }
        string xlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        string fileName = "BeezMart-Billing-Invoices.xlsx";
        using (ExcelPackage excelPackage = new ExcelPackage())
        {
            ExcelWorksheet excelWorksheet, wks;
            ExcelRange topRow;
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Invoices");
            #region populate worksheet with records
            wks = excelWorksheet;
            int col = 1;
            wks.SetValue(1, col, "Invoice_ID"); col++;			        // Column A
            wks.SetValue(1, col, "Accounting_ID"); col++;			    // Column B
            wks.SetValue(1, col, "Issue_Date"); col++;			        // Column C
            wks.SetValue(1, col, "Customer_Name"); col++;			    // Column D
            wks.SetValue(1, col, "Customer_City"); col++;			    // Column E
            wks.SetValue(1, col, "Customer_Province"); col++;			// Column F
            wks.SetValue(1, col, "Total_Value"); col++;			        // Column G
            wks.SetValue(1, col, "Currency"); col++;			        // Column H
            wks.SetValue(1, col, "VAT_Percentage"); col++;			    // Column I
			wks.SetValue(1, col, "Total_VAT"); col++;			  		// Column J
            wks.SetValue(1, col, "Due_Date"); col++;			        // Column K
            wks.SetValue(1, col, "Total_Payed"); col++;			        // Column L
            wks.SetValue(1, col, "Invoice_Type_ID"); col++;			    // Column M
			wks.SetValue(1, col, "Invoice_Type"); col++;			    // Column N
            wks.SetValue(1, col, "Invoice_Status_ID"); col++;			// Column O
			wks.SetValue(1, col, "Invoice_Status"); col++;				// Column P
            wks.SetValue(1, col, "Closing_Date"); col++;			    // Column Q
            wks.SetValue(1, col, "Organization_ID"); col++;			    // Column R
            wks.SetValue(1, col, "Person_ID"); col++;			        // Column S
            topRow = wks.Cells["A1:S1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            int row = 1;
            foreach (InvoiceModel item in records)
            {
                row++;
                col = 1;
                wks.SetValue(row, col, item.ID); col++;
                wks.SetValue(row, col, item.AccountingID); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.IssueDate)); col++;
                wks.SetValue(row, col, item.CustomerName); col++;
                wks.SetValue(row, col, item.CustomerCity); col++;
                wks.SetValue(row, col, item.CustomerProvince); col++;
                wks.SetValue(row, col, item.TotalValue); col++;
                wks.SetValue(row, col, item.Currency); col++;
                wks.SetValue(row, col, item.VATpercentage); col++;
				wks.SetValue(row, col, item.TotalVAT); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.DueDate)); col++;
                wks.SetValue(row, col, item.TotalPayed); col++;
                wks.SetValue(row, col, (byte)item.Type); col++;
                wks.SetValue(row, col, item.Type.ToString()); col++;
                wks.SetValue(row, col, (byte)item.Status); col++;
                wks.SetValue(row, col, item.Status.ToString()); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ClosingDate)); col++;
                wks.SetValue(row, col, item.OrganizationID); col++;
                wks.SetValue(row, col, item.PersonID); col++;
            }
            wks.DefaultColWidth = 20;
            for (col = 1; col < 19; col++) wks.Column(col).AutoFit();
            #endregion
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Filter");
            #region populate filtering criteria
            wks = excelWorksheet;
            row = 0;
            row++; wks.SetValue(row, 1, "Selection filter");
            wks.SetValue(row, 2, parameters.FilterIsSet ? "SEE HOW RECORDS WERE SELECTED" : "NO FILTERING CRITERIA WAS SPECIFIED");
            row++; wks.SetValue(row, 1, "Customer name pattern");
            wks.SetValue(row, 2, filter.CustomerNamePattern);
            row++; wks.SetValue(row, 1, "Name exact match");
            wks.SetValue(row, 2, filter.CustomerNameExactMatch.ToString());
            row++; wks.SetValue(row, 1, "Organization ID");
            wks.SetValue(row, 2, filter.OrganizationID);
            row++; wks.SetValue(row, 1, "Person ID");
            wks.SetValue(row, 2, filter.PersonID);
			row++; wks.SetValue(row, 1, "Issued after");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.IssuedAfter));
            row++; wks.SetValue(row, 1, "Issued before");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.IssuedBefore));
            row++; wks.SetValue(row, 1, "Currency");
            wks.SetValue(row, 2, filter.Currency);
            row++; wks.SetValue(row, 1, "Invoice IDs");
            wks.SetValue(row, 2, filter.InvoiceIDs);
            row++; wks.SetValue(row, 1, "Accounting IDs");
            wks.SetValue(row, 2, filter.AccountingIDs);
            row++; wks.SetValue(row, 1, "Invoice status");
            wks.SetValue(row, 2, filter.InvoiceStatus);
            row++; wks.SetValue(row, 1, "Overdue?");
            wks.SetValue(row, 2, filter.Overdue);
            row++; wks.SetValue(row, 1, "Invoice type");
            wks.SetValue(row, 2, filter.InvoiceType);
            topRow = wks.Cells["A1:B1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            wks.Column(1).AutoFit();
            wks.Column(2).AutoFit();
            #endregion
			return File(excelPackage.GetAsByteArray(), xlsxContentType, fileName);
        }
	}

    /// <summary>
    ///		<para>Returns an empty AppMessage with AppMessage.Type = AppMessageType.Success if all fields validate.</para>
    ///		<para>On validation failure, AppMessage will contain the validation messages and will have AppMessage.Type = AppMessageType.Warning.</para>
    /// </summary>
    /// <param name="invoice"></param>
    /// <returns></returns>
    protected async Task<AppMessage> ValidateForPrint(InvoiceModel invoice)
	{
		string FieldsToValidate = MyApp.ConfigSettings["Billing.Invoice.Required-Fields"];
		AppMessage msg = new AppMessage(AppMessageType.Success);
		#region check if the required fields are filled in
		if (string.IsNullOrEmpty(invoice.Currency) && invoice.Currency?.Length != 3)
		{
			msg.AppendMessage("Invoice currency seems to be missing or having an invalid value.");
		}
		if ((string.IsNullOrEmpty(invoice.AccountingID) || invoice.AccountingID.Trim().Length < 1) && MyApp.ConfigSettings["Billing.Auto-Serials"].ToLower() != "true")
		{
			msg.AppendMessage("Invoice number or Accounting ID seems to be missing, and the application is not configured to auto-assign one.");
		}
		if (invoice.Type == InvoiceType.Regular && !invoice.DueDate.HasValue && FieldsToValidate.Contains("Due_Date"))
		{
			msg.AppendMessage("Due date must be specified for the regular invoices as per business rules.");
		}
		if (string.IsNullOrEmpty(invoice.CustomerName) || invoice.CustomerName.Trim().Length < 3)
		{
			msg.AppendMessage("The customer name is missing or too short. It must be specified.");
		}
		if ((string.IsNullOrEmpty(invoice.CustomerCity) || invoice.CustomerCity.Trim().Length < 3) && FieldsToValidate.Contains("Customer_City"))
		{
			msg.AppendMessage("Customer city is not specified, but required as per business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.CustomerProvince) || invoice.CustomerProvince.Trim().Length < 3) && FieldsToValidate.Contains("Customer_Province"))
		{
			msg.AppendMessage("Customer province is not specified, but required as per business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.CustomerAddress) || invoice.CustomerAddress.Trim().Length < 3) && FieldsToValidate.Contains("Customer_Address"))
		{
			msg.AppendMessage("Customer billing address is not specified, but required as per business rules.");
		}
		if (string.IsNullOrEmpty(invoice.CustomerRegistrationNumber) || invoice.CustomerRegistrationNumber.Trim().Length < 5)
		{
			msg.AppendMessage("Customer registration number seem to be missing or is too short, less than 5 chars.");
		}
		if (string.IsNullOrEmpty(invoice.CustomerTaxID) || invoice.CustomerTaxID.Trim().Length < 3)
		{
			msg.AppendMessage("Customer tax or VAT ID seem to be missing or is too short, less than 3 chars.");
		}
		if ((string.IsNullOrEmpty(invoice.CustomerBankName) || invoice.CustomerBankName.Trim().Length < 3) && FieldsToValidate.Contains("Customer_Bank_Name"))
		{
			msg.AppendMessage("Customer bank name is required as per business rule. Please fill.");
		}
		if ((string.IsNullOrEmpty(invoice.CustomerBankAccount) || invoice.CustomerBankAccount.Trim().Length < 1) && FieldsToValidate.Contains("Customer_Bank_Account"))
		{
			msg.AppendMessage("Customer bank account IBAN code is required as per business rule. Please fill.");
		}
		if (invoice.ID == null || (await BeezMart.DAL.Billing.InvoiceItems.ListAsync(invoice.ID.Value)).Count() == 0)
		{
			msg.AppendMessage("This invoice does not exist in database or it has no items: no products or services are charged. An invoice should have at least one line to be printed.");
		}
		if ((string.IsNullOrEmpty(invoice.DelegateName) || invoice.DelegateName.Trim().Length < 3) && FieldsToValidate.Contains("Delegate_Name"))
		{
			msg.AppendMessage("Business rules require handover delegate name to be specified. Please fill.");
		}
		if ((string.IsNullOrEmpty(invoice.DelegateID1) || invoice.DelegateID1.Trim().Length < 1) && FieldsToValidate.Contains("Delegate_ID_Serial"))
		{
			msg.AppendMessage("Business rules require handover delegate ID serial to be specified. Please fill.");
		}
		if ((string.IsNullOrEmpty(invoice.DelegateID2) || invoice.DelegateID2.Trim().Length < 1) && FieldsToValidate.Contains("Delegate_ID_Number"))
		{
			msg.AppendMessage("Business rules require handover delegate ID number to be specified. Please fill.");
		}
		if ((string.IsNullOrEmpty(invoice.DelegateID3) || invoice.DelegateID3.Trim().Length < 1) && FieldsToValidate.Contains("Delegate_ID_Issuing_Authority"))
		{
			msg.AppendMessage("Business rules require handover delegate ID issuing authority to be specified. Please fill.");
		}
		if ((string.IsNullOrEmpty(invoice.TransportName) || invoice.TransportName.Trim().Length < 1) && FieldsToValidate.Contains("Transport_Document_Number"))
		{
			if (invoice.Type == InvoiceType.Regular)
			{
				msg.AppendMessage("The delivery transport document number is required by the business rule on every regular invoice.");
			}
		}
		if ((string.IsNullOrEmpty(invoice.TransportDetail1) || invoice.TransportDetail1.Trim().Length < 1) && FieldsToValidate.Contains("Handover_Means"))
		{
			msg.AppendMessage("Handover means - by auto, postal, or electronic - needs to be specified as per business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.TransportDetail2) || invoice.TransportDetail2.Trim().Length < 1) && FieldsToValidate.Contains("Handover_Vehicle"))
		{
			msg.AppendMessage("Vehicle registration used at handover is required as per business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.TransportDetail3) || invoice.TransportDetail3.Trim().Length < 1) && FieldsToValidate.Contains("Handover_Hour"))
		{
			msg.AppendMessage("Date of the handover is required as per business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.TransportDetail4) || invoice.TransportDetail4.Trim().Length < 1) && FieldsToValidate.Contains("Handover_Minute"))
		{
			msg.AppendMessage("Time of the handover is required as per business rules. Aha, right.");
		}
		if ((string.IsNullOrEmpty(invoice.Detail1) || invoice.Detail1.Trim().Length < 5) && FieldsToValidate.Contains("Exchange_Rate"))
		{
			msg.AppendMessage("Please specify exchange rate; it is required by business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.Detail2) || invoice.Detail2.Trim().Length < 10) && FieldsToValidate.Contains("Operator_Details"))
		{
			msg.AppendMessage("Please specify operator details: full name and registration; it is required by business rules.");
		}
		if ((string.IsNullOrEmpty(invoice.Detail3) || invoice.Detail3.Trim().Length < 1) && FieldsToValidate.Contains("Comments"))
		{
			msg.AppendMessage("Business rules require that a certain comment should be printed on every invoice. Please fill in.");
		}
		#endregion
		if (MyApp.ConfigSettings["Billing.Validate-Issue-Date"].ToLower() == "true" && MyApp.ConfigSettings["Billing.Auto-Serials"].ToLower() == "true")
		{
			#region check date against latest invoice having an accounting ID (Accounting ID must be incremented with issue date)
			InvoiceModel? latestHavingAccountingID = await BeezMart.DAL.Billing.Invoices.RetrieveLatestHavingAccountingIDAsync();
			if (latestHavingAccountingID != null)
			{
				if (invoice.IssueDate.Date < latestHavingAccountingID.IssueDate.Date)
				{
					string _m = @"Please change issue date to at least <b>{2}</b>!<br />
								Invoice no <a href='View?InvoiceID={0}'>{1}</a>, dated {2}, was already printed, and 
								auto-numbering is enabled: an incremental Accounting ID is automatically assigned to each printed invoice.
								An older invoice cannot have a newer/higher Accounting ID, so you'd have to change the issue date to a more recent one.";
					msg.AppendMessage(string.Format(_m, latestHavingAccountingID.ID, latestHavingAccountingID.AccountingID, latestHavingAccountingID.IssueDate.ToString("yyyy-MM-dd")));
				}
			}
			#endregion
		}

		if (msg.Count > 0) msg.Type = AppMessageType.Warning;
		return msg;
	}

	protected string PrinterVersionHtmlStyles(HtmlDocument invoiceTemplate)
	{
		//HtmlNode stylesNode = invoiceTemplate.DocumentNode.SelectSingleNode("/html/head/style");
		HtmlNode stylesNode = invoiceTemplate.DocumentNode.SelectSingleNode("/html/head/style");
		return stylesNode.WriteTo();
	}
	protected async Task<string> PrinterVersionHtmlElement(HtmlDocument invoiceTemplate, InvoiceModel invoiceModel)
	{
		if (invoiceModel.ID == null) return "Error: Invoice.ID is NULL!";
		HtmlNode languageNode = invoiceTemplate.DocumentNode.SelectSingleNode("/html/head/meta[@name='language']");
		CultureInfo invoiceTemplateCulture = new CultureInfo(languageNode.Attributes["content"].Value);
		HtmlNode invoiceHtmlNode = invoiceTemplate.DocumentNode.SelectSingleNode("/html/body/div[@class='InvoiceRenderingBody']");
		HtmlNode invoiceItemsNode = invoiceHtmlNode.SelectSingleNode("//tr[@class='InvoiceItems']//tbody");
		string invoiceItemTemplate = invoiceItemsNode.WriteContentTo().Replace("<tr>", string.Empty).Replace("</tr>", string.Empty);
		List<InvoiceItemModel> invoiceItems = await BeezMart.DAL.Billing.InvoiceItems.ListAsync(invoiceModel.ID.Value);
		int itemCount = 1;
		string unitPriceFormat = MyApp.ConfigSettings["Sales.Decimal-Format-Units"];
		string totalPriceFormat = MyApp.ConfigSettings["Sales.Decimal-Format-Totals"];
		decimal itemVAT = 0;
		StringBuilder stb;
		HtmlNode itemNode;
		invoiceItemsNode.RemoveAll();
		foreach (InvoiceItemModel invoiceItem in invoiceItems)
		{
			itemVAT = invoiceModel.VATpercentage / 100 * invoiceItem.TotalPrice;
			stb = new StringBuilder(invoiceItemTemplate);
			#region populate invoice item fields
			stb.Replace("1<!--[#_Item_No_#]-->", itemCount.ToString());
			stb.Replace("2<!--[#_Item_Name_#]-->", invoiceItem.Name);
			stb.Replace("3<!--[#_Unit_Measure_#]-->", invoiceItem.UnitMeasure);
			stb.Replace("4<!--[#_Quantity_#]-->", invoiceItem.Quantity.ToString("#,##0", invoiceTemplateCulture));
			stb.Replace("5<!--[#_Unit_Price_#]-->", invoiceItem.UnitPrice.ToString(unitPriceFormat, invoiceTemplateCulture));
			stb.Replace("6<!--[#_Total_Price_#]-->", invoiceItem.TotalPrice.ToString(totalPriceFormat, invoiceTemplateCulture));
			stb.Replace("7<!--[#_VAT_Amount_#]-->", itemVAT.ToString(totalPriceFormat, invoiceTemplateCulture));
			stb.Replace("<!--[#_Item_No_#]-->", itemCount.ToString());
			stb.Replace("<!--[#_Item_Name_#]-->", invoiceItem.Name);
			stb.Replace("<!--[#_Unit_Measure_#]-->", invoiceItem.UnitMeasure);
			stb.Replace("<!--[#_Quantity_#]-->", invoiceItem.Quantity.ToString("#,##0", invoiceTemplateCulture));
			stb.Replace("<!--[#_Unit_Price_#]-->", invoiceItem.UnitPrice.ToString(unitPriceFormat, invoiceTemplateCulture));
			stb.Replace("<!--[#_Total_Price_#]-->", invoiceItem.TotalPrice.ToString(totalPriceFormat, invoiceTemplateCulture));
			stb.Replace("<!--[#_VAT_Amount_#]-->", itemVAT.ToString(totalPriceFormat, invoiceTemplateCulture));
			#endregion
			itemCount++;
			itemNode = invoiceTemplate.CreateElement("tr");
			itemNode.InnerHtml = stb.ToString();
			invoiceItemsNode.AppendChild(itemNode);
		}
		stb = new StringBuilder(invoiceHtmlNode.WriteTo());
		StringBuilder customerAddress = new StringBuilder(invoiceModel.CustomerAddress);
		#region format customer address
		if (!string.IsNullOrEmpty(invoiceModel.CustomerCity))
		{
			customerAddress.Append(customerAddress.Length == 0 ? invoiceModel.CustomerCity : string.Format("<br />{0}", invoiceModel.CustomerCity));
		}
		if (!string.IsNullOrEmpty(invoiceModel.CustomerProvince))
		{
			if (string.IsNullOrEmpty(invoiceModel.CustomerCity))
			{
				customerAddress.Append(customerAddress.Length == 0 ? invoiceModel.CustomerProvince : string.Format("<br />{0}", invoiceModel.CustomerProvince));
			}
			else
			{
				customerAddress.Append(string.Format(", {0}", invoiceModel.CustomerProvince));
			}
		}
		#endregion
		#region populate invoice fields
		#region localize invoice type field
		string? InvoiceType = null;
		switch (invoiceModel.Type)
		{
			case BeezMart.Entities.Billing.InvoiceType.NotSet: InvoiceType = BeezMart.Resources.Billing.Common.ResourceManager.GetString("InvoiceType_NotSet", invoiceTemplateCulture); break;
			case BeezMart.Entities.Billing.InvoiceType.Regular: InvoiceType = BeezMart.Resources.Billing.Common.ResourceManager.GetString("InvoiceType_Regular", invoiceTemplateCulture); break;
			case BeezMart.Entities.Billing.InvoiceType.Storno: InvoiceType = BeezMart.Resources.Billing.Common.ResourceManager.GetString("InvoiceType_Storno", invoiceTemplateCulture); break;
			case BeezMart.Entities.Billing.InvoiceType.Advance: InvoiceType = BeezMart.Resources.Billing.Common.ResourceManager.GetString("InvoiceType_Advance", invoiceTemplateCulture); break;
			case BeezMart.Entities.Billing.InvoiceType.Risturn: InvoiceType = BeezMart.Resources.Billing.Common.ResourceManager.GetString("InvoiceType_Risturn", invoiceTemplateCulture); break;
			case BeezMart.Entities.Billing.InvoiceType.ProForma: InvoiceType = BeezMart.Resources.Billing.Common.ResourceManager.GetString("InvoiceType_ProForma", invoiceTemplateCulture); break;
		}
		#endregion
		stb.Replace("[#_Provider_Organization_Name_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Name"]);
		stb.Replace("[#_Provider_Registration_Number_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Registration-Number"]);
		stb.Replace("[#_Provider_Tax_ID_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Tax-ID"]);
		stb.Replace("[#_Provider_Address_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Address"]);
		stb.Replace("[#_Provider_City_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-City"]);
		stb.Replace("[#_Provider_Province_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Province"]);
		stb.Replace("[#_Provider_IBAN_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Bank-Account-IBAN"]);
		stb.Replace("[#_Provider_Bank_#]", MyApp.ConfigSettings["Billing.Invoice.Provider-Bank-Name"]);
		stb.Replace("[#_Invoice_Number_#]", invoiceModel.AccountingID);
		stb.Replace("[#_Issue_Date_#]", invoiceModel.IssueDate.ToString(MyApp.ConfigSettings["Sales.Date-Format-Short"], invoiceTemplateCulture));
		stb.Replace("[#_Transport_Document_#]", invoiceModel.TransportName);
		stb.Replace("[#_VAT_Percentage_#]", invoiceModel.VATpercentage.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("[#_Currency_#]", invoiceModel.Currency);
		stb.Replace("[#_Total_Due_1_#]", invoiceModel.TotalDue.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("[#_Payment_Deadline_#]", invoiceModel.DueDate.HasValue ? invoiceModel.DueDate.Value.ToString(MyApp.ConfigSettings["Sales.Date-Format-Long"], invoiceTemplateCulture) : string.Empty);
		stb.Replace("[#_Invoice_Type_#]", InvoiceType);
		stb.Replace("[#_Customer_Name_#]", invoiceModel.CustomerName);
		stb.Replace("[#_Customer_Registration_Number_#]", invoiceModel.CustomerRegistrationNumber);
		stb.Replace("[#_Customer_Tax_ID_#]", invoiceModel.CustomerTaxID);
		stb.Replace("[#_Customer_Address_#]", customerAddress.ToString());
		stb.Replace("[#_Customer_Bank_#]", invoiceModel.CustomerBankName);
		stb.Replace("[#_Customer_IBAN_#]", invoiceModel.CustomerBankAccount);
		stb.Replace("8<!--[#_Total_Value_#]-->", invoiceModel.TotalValue.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("<!--[#_Total_Value_#]-->", invoiceModel.TotalValue.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("9<!--[#_Total_VAT_#]-->", invoiceModel.TotalVAT.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("<!--[#_Total_VAT_#]-->", invoiceModel.TotalVAT.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("10<!--[#_Total_Due_2_#]-->", invoiceModel.TotalDue.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("<!--[#_Total_Due_2_#]-->", invoiceModel.TotalDue.ToString("#,##0.00", invoiceTemplateCulture));
		stb.Replace("<!--[#_Invoice_Detail_1_#]-->", invoiceModel.Detail1);
		stb.Replace("<!--[#_Invoice_Detail_2_#]-->", invoiceModel.Detail2);
		stb.Replace("<!--[#_Invoice_Detail_3_#]-->", string.IsNullOrEmpty(invoiceModel.Detail3) ? string.Empty : invoiceModel.Detail3.Replace("\n", "<br />"));
		stb.Replace("[#_Transport_Document_#]", invoiceModel.TransportName);
		stb.Replace("[#_Handover_Delegate_#]", invoiceModel.DelegateName);
		stb.Replace("N/A<!--[#_Delegate_ID_1_#]-->", string.IsNullOrEmpty(invoiceModel.DelegateID1) ? "N/A" : invoiceModel.DelegateID1);
		stb.Replace("N/A<!--[#_Delegate_ID_2_#]-->", string.IsNullOrEmpty(invoiceModel.DelegateID2) ? "N/A" : invoiceModel.DelegateID2);
		stb.Replace("N/A<!--[#_Delegate_ID_3_#]-->", string.IsNullOrEmpty(invoiceModel.DelegateID3) ? "N/A" : invoiceModel.DelegateID3);
		stb.Replace("Electronic<!--[#_Handover_Means_#]-->", string.IsNullOrEmpty(invoiceModel.TransportDetail1) ? "Electronic" : invoiceModel.TransportDetail1);
		stb.Replace("<!--[#_Handover_Registration_#]-->", invoiceModel.TransportDetail2);
		stb.Replace("<!--[#_Handover_Date_#]-->", invoiceModel.TransportDetail3);
		stb.Replace("<!--[#_Handover_Hour_#]-->", invoiceModel.TransportDetail4);
		#endregion
		return stb.ToString();
	}
}