﻿using BeezMart.Areas.Billing.ViewModels.Payments;
using BeezMart.Entities.Billing;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class PaymentsController : Controller
{
    public ViewResult Index(Index_ViewModel viewModel)
    {
        if (!viewModel.FromDate.HasValue) viewModel.FromDate = DateTime.Now.AddMonths(-3);
        if (!viewModel.ToDate.HasValue) viewModel.ToDate = DateTime.Now.AddDays(7);
        return View(viewModel);
    }

    #region display and edit payments from the payments index

    public ViewComponentResult _OnIndexList(DateTime? FromDate, DateTime? ToDate)
    {
        return ViewComponent(typeof(BeezMart.Areas.Billing.ViewComponents.Payments.OnIndexList), new { FromDate = FromDate, ToDate = FromDate });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _OnIndexSave(PaymentModel payment)
    {
        AppMessage msg = new AppMessage();
        msg.IsDismisable = false;
        _OnIndexSave_ViewModel viewModel = new _OnIndexSave_ViewModel();
        TryValidateModel(payment);
        viewModel.InvoiceID = payment.InvoiceID;
        if (ModelState.IsValid)
        {
            if (payment.Amount <= 0)
            {
                msg.AppendMessage("The payment amount must be a positive, non-null decimal number!");
                viewModel.Message = msg.ToHtmlError();
            }
            else
            {
                bool success = await BeezMart.DAL.Billing.Payments.SaveAsync(payment);
                if (success)
                {
                    msg.AppendMessage("The invoice payment has been successfully recorded in the system.");
                    InvoiceModel? invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(payment.InvoiceID);
                    if (invoice == null) throw new NullReferenceException($"Invoice record does not exist in database: InvoiceID={payment.InvoiceID}");
                    if (invoice.ID == null) throw new NullReferenceException($"Retrieved invoice record does not have an Invoice.ID: InvoiceID={payment.InvoiceID}"); viewModel.RemainingToBePayed = invoice.RemainingToBePayed;
                    msg.AppendMessage(string.Format("Remaining to be paid for this invoice: <b>{0:#,##0.00}</b> {1}.", invoice.RemainingToBePayed, invoice.Currency));
                    if (invoice.Status == InvoiceStatus.SentToCustomer && invoice.RemainingToBePayed < 1M)
                    {
                        msg.AppendMessage("This invoice should now be closed and archived.");
                        viewModel.ReadyToClose = true;
                    }
                    if (invoice.Status == InvoiceStatus.Closed && invoice.RemainingToBePayed > 1M)
                    {
                        msg.AppendMessage("This invoice was previously archived as closed. Since now its amount remaining to be payed is not 0, the invoice status changed to pending, or sent to customer.");
                        msg.AppendMessage("You may want to review the invoice and take further action on it.");
                        await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.SentToCustomer);
                    }
                    viewModel.Message = msg.ToHtmlSuccess();
                }
                else
                {
                    msg.AppendMessage("Invoice payment could not be saved. Please try again or contact support!");
                    viewModel.Message = msg.ToHtmlError();
                }
            }
        }
        else
        {
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        return PartialView(viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _OnIndexDelete(int PaymentID, int InvoiceID)
    {
        AppMessage msg = new AppMessage();
        msg.IsDismisable = false;
        _OnIndexDelete_ViewModel viewModel = new _OnIndexDelete_ViewModel();
        viewModel.ReviewInvoice = false;
        bool success = await BeezMart.DAL.Billing.Payments.DeleteAsync(PaymentID, updatedBy: this.User.Identity?.Name);
        if (success)
        {
            msg.Type = AppMessageType.Success;
            msg.Title = "Payment deleted";
            msg.AppendMessage("The payment record has been successfully deleted.");
            InvoiceModel? invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
            if (invoice == null) throw new NullReferenceException($"Invoice record does not exist in database: InvoiceID={InvoiceID}");
            if (invoice.ID == null) throw new NullReferenceException($"Retrieved invoice record does not have an Invoice.ID: InvoiceID={InvoiceID}");
            viewModel.RemainingToBePayed = invoice.RemainingToBePayed;
            viewModel.InvoiceStatus = invoice.Status;
            msg.AppendMessage(string.Format("Remaining to be paid for this invoice: <b>{0:#,##0.00}</b> {1}.", invoice.RemainingToBePayed, invoice.Currency));
            if (invoice.Status == InvoiceStatus.Closed && invoice.RemainingToBePayed > 1M)
            {
                await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(InvoiceID, InvoiceStatus.SentToCustomer);
                msg.AppendMessage("The invoice status was automatically changed from closed and archived to pending. You should review the invoice after this change.");
                viewModel.InvoiceStatus = InvoiceStatus.SentToCustomer;
                viewModel.ReviewInvoice = true;
            }
        }
        else
        {
            msg.Type = AppMessageType.Error;
            msg.Title = "Deletion failed";
            msg.AppendMessage("The payment record could not be deleted; someone else could have deleted it in the meantime.");
        }
        viewModel.Message = msg.ToHtml();
        return PartialView(viewModel);
    }

    #endregion

    #region display and edit payments from an invoice view

    public async Task<PartialViewResult> _OnInvoiceList(int InvoiceID, decimal TotalDue)
    {
        ViewBag.TotalDue = TotalDue;
        List<PaymentModel> viewModel = await BeezMart.DAL.Billing.Payments.ListInvoicePaymentsAsync(InvoiceID);
        return PartialView(viewModel);
    }

    [HttpGet]
    public PartialViewResult _OnInvoiceEdit(int InvoiceID)
    {
        ViewBag.InvoiceID = InvoiceID;
        return PartialView();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _OnInvoiceSave(PaymentModel payment)
    {
        AppMessage msg = new AppMessage();
        msg.IsDismisable = false;
        TryValidateModel(payment);
        _OnInvoiceSave_ViewModel viewModel = new _OnInvoiceSave_ViewModel();
        if (ModelState.IsValid)
        {
            bool success = false;
            if (payment.Amount <= 0) msg.AppendMessage("The payment amount must be a positive, non-null decimal number!");
            if (!payment.PaymentDate.HasValue) msg.AppendMessage("The payment date is not specified!");
            if (msg.Length == 0)
            {
                success = await BeezMart.DAL.Billing.Payments.SaveAsync(payment);
                if (success) msg.AppendMessage("The invoice payment has been successfully recorded in the system.");
                else msg.AppendMessage("Invoice payment could not be saved. Please try again or contact support!");
            }
            InvoiceModel? invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(payment.InvoiceID);
            if (invoice == null) throw new NullReferenceException($"Invoice record does not exist in database: InvoiceID={payment.InvoiceID}");
            if (invoice.ID == null) throw new NullReferenceException($"Retrieved invoice record does not have an Invoice.ID: InvoiceID={payment.InvoiceID}");
            bool isPayable = (invoice.Type == InvoiceType.Regular || invoice.Type == InvoiceType.Advance || invoice.Type == InvoiceType.ProForma);
            viewModel.RemainingToBePayed = invoice.RemainingToBePayed;
            msg.AppendMessage(string.Format("Remaining to be paid for this invoice: <b>{0:#,##0.00}</b> {1}.", invoice.RemainingToBePayed, invoice.Currency));
            if (invoice.Status == InvoiceStatus.SentToCustomer && isPayable && invoice.RemainingToBePayed < 1M)
            {
                msg.AppendMessage("This invoice should now be closed and archived.");
                viewModel.ReadyToClose = true;
            }
            if (invoice.Status == InvoiceStatus.Closed && isPayable && invoice.RemainingToBePayed >= 1M)
            {
                msg.AppendMessage("The invoice status has been changed to Pending (or Sent-to-customer) because it is not fully payed now.");
                await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.SentToCustomer);
                invoice.Status = InvoiceStatus.SentToCustomer;
            }
            viewModel.InvoiceStatus = invoice.Status;
            if (success)
            {
                viewModel.Message = msg.ToHtmlSuccess();
            }
            else
            {
                viewModel.Message = msg.ToHtmlError();
            }
        }
        else
        {
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
            viewModel.Message = msg.ToHtmlError();
        }
        return PartialView(viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _OnInvoiceDelete(int PaymentID, int InvoiceID)
    {
        AppMessage msg = new AppMessage();
        msg.IsDismisable = false;
        _OnInvoiceDelete_ViewModel viewModel = new _OnInvoiceDelete_ViewModel();
        viewModel.ReviewInvoice = false;
        bool success = await BeezMart.DAL.Billing.Payments.DeleteAsync(PaymentID);
        if (success)
        {
            msg.Type = AppMessageType.Success;
            msg.Title = "Payment deleted";
            msg.AppendMessage("The payment record has been successfully deleted.");
            InvoiceModel? invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
            if (invoice == null) throw new NullReferenceException($"Invoice record does not exist in database: InvoiceID={InvoiceID}");
            if (invoice.ID == null) throw new NullReferenceException($"Retrieved invoice record does not have an Invoice.ID: InvoiceID={InvoiceID}");
            viewModel.RemainingToBePayed = invoice.RemainingToBePayed;
            viewModel.InvoiceStatus = invoice.Status;
            msg.AppendMessage(string.Format("Remaining to be paid for this invoice: <b>{0:#,##0.00}</b> {1}.", invoice.RemainingToBePayed, invoice.Currency));
            if (invoice.Status == InvoiceStatus.Closed && invoice.RemainingToBePayed > 1M)
            {
                await BeezMart.DAL.Billing.Invoices.ChangeStatusAsync(invoice.ID.Value, InvoiceStatus.SentToCustomer);
                msg.AppendMessage("The invoice status was automatically changed from closed and archived to pending. You should review the invoice after this change.");
                viewModel.ReviewInvoice = true;
            }
            viewModel.InvoiceStatus = invoice.Status;
        }
        else
        {
            msg.Type = AppMessageType.Error;
            msg.Title = "Deletion failed";
            msg.AppendMessage("The payment record could not be deleted; someone else could have deleted it in the meantime.");
        }
        viewModel.Message = msg.ToHtml();
        return PartialView(viewModel);
    }

    #endregion

    public async Task<JsonResult> _GetPayment(int PaymentID)
    {
        PaymentModel? viewModel = await BeezMart.DAL.Billing.Payments.RetrieveAsync(PaymentID);
        return Json(viewModel);
    }
}
