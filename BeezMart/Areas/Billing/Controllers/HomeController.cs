﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class HomeController : Controller
{
    public ActionResult Index()
    {
        return View();
    }
}

