﻿using BeezMart.Entities.Billing;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Billing.Controllers;

[Area("Billing")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForFinance)]
public class HandoverTransportsController : Controller
{
    public ViewResult Index()
    {
        return View();
    }

    public async Task<PartialViewResult> _PickOnInvoice()
    {
        List<HandoverTransportModel> viewModel = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
        return PartialView(viewModel);
    }

    public async Task<PartialViewResult> _List()
    {
        List<HandoverTransportModel> viewModel = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
        return PartialView(viewModel);
    }

    [HttpGet]
    public PartialViewResult _Edit()
    {
        return PartialView();
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(HandoverTransportModel handoverTransport)
    {
        if (TryValidateModel(handoverTransport))
        {
            await BeezMart.DAL.Billing.HandoverTransports.SaveAsync(handoverTransport);
        }
        else
        {
            AppMessage msg = new AppMessage();
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        List<HandoverTransportModel> viewModel = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
        return PartialView(nameof(this._List), viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(short RecordID)
    {
        await BeezMart.DAL.Billing.HandoverTransports.DeleteAsync(RecordID);
        List<HandoverTransportModel> viewModel = await BeezMart.DAL.Billing.HandoverTransports.ListAsync();
        return PartialView(nameof(this._List), viewModel);
    }
}
