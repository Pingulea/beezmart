﻿using BeezMart.Areas.Archiving.ViewModels.Documents;
using BeezMart.Caching.Archiving;
using BeezMart.Entities.Archiving;
using BeezMart;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Drawing;

namespace BeezMart.Areas.Archiving.Controllers;

[Area("Archiving")]
[Authorize]
public class DocumentsController : Controller
{
    protected static string _limitedDefaultLookupResult = "No search criteria is specified. Only the latest {0} document records will show up at most.";

    public async Task<ViewResult> Index()
    {
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.Documents = await BeezMart.DAL.Archiving.Documents.ListLatestAsync(100);
        List<DocumentModel.CategoryModel> categories = await DocumentCategories.GetSubCategoriesAsync();
        foreach (DocumentModel.CategoryModel category in categories)
            if (category.ID != null)
                viewModel.CategoryNames.Add(category.ID.Value, $"{category.Name} :: {category.SubCategory}");
        return View(viewModel);
    }
    public async Task<ViewResult> View(long ID)
    {
        View_ViewModel viewModel = new View_ViewModel { };
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(ID);
        if (viewModel.Document != null && viewModel.Document.CategoryID.HasValue)
        {
            DocumentModel.CategoryModel? documentCategory = await BeezMart.DAL.Archiving.Documents.Categories.RetrieveAsync(viewModel.Document.CategoryID.Value);
            viewModel.DocumentCategoryName = $"{documentCategory?.Name} :: {documentCategory?.SubCategory}";
        }
        if (viewModel.Document == null)
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.Append("The document record you are looking for could not be found");
            return View("Message", msg);
        }
        return View(viewModel);
    }
    [HttpGet]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForDocumentManagement)]
    public ViewResult Add()
    {
        Add_ViewModel viewModel = new Add_ViewModel();
        viewModel.Document = new DocumentModel();
        return View(viewModel);
    }
    [HttpPost]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForDocumentManagement)]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Add(Add_ViewModel viewModel)
    {
        if (viewModel.Document == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Document.");
        if (await BeezMart.DAL.Archiving.Documents.SaveAsync(viewModel.Document)) return RedirectToAction(nameof(this.Index));
        return View(viewModel);
    }
    [HttpGet]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForDocumentManagement)]
    public async Task<ViewResult> Edit(long ID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(ID);
        if (viewModel.Document == null)
        {
            AppMessage msg = new AppMessage();
            msg.Add("The document record you're trying to edit could not be found in the database.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForDocumentManagement)]
    public async Task<IActionResult> Edit(Edit_ViewModel viewModel)
    {
        if (viewModel.Document == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Document.");
        await BeezMart.DAL.Archiving.Documents.SaveAsync(viewModel.Document);
        return RedirectToAction(nameof(this.Index));
    }

    /// <summary>
    ///		<para>Gets an advanced search page with an AJAX-updatable search result panel given by _Search() action</para>
    /// </summary>
    /// <returns></returns>
    public async Task<ViewResult> Search()
    {
        Search_ViewModel viewModel = new Search_ViewModel();
        AppMessage msg = new AppMessage();
        if (Request.QueryString.HasValue)
        {
            await TryUpdateModelAsync<Search_ViewModel>(viewModel);
        }
        return View(viewModel);
    }
    public async Task<PartialViewResult> _Search(Search_Params parameters)
    {
        DocumentModel.LookupFilter filter;
        List<DocumentModel> viewModel;
        if (!parameters.FilterIsSet)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["Sales.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult, maxRowCount));
            ViewBag.AppMessage = msg.ToHtmlWarning();
            int maxCount = int.Parse(MyApp.ConfigSettings["Archiving.Search.Result.MaxCount.Unfiltered"]);
            viewModel = await BeezMart.DAL.Archiving.Documents.ListLatestAsync(maxCount);
            return PartialView(viewModel);
        }
        List<long>? docIDs = null;
        #region split IDs into lists
        if (!string.IsNullOrEmpty(parameters.IDs))
        {
            char[] separators = { ';', ',', '|', ' ' };
            int id;
            string[] IDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in IDs)
            {
                if (int.TryParse(s, out id))
                {
                    if (docIDs == null) docIDs = new List<long> { };
                    docIDs.Add(id);
                }
            }
        }
        #endregion
        filter = new DocumentModel.LookupFilter()
        {
            TitlePattern = parameters.Title,
            TitleExactMatch = parameters.Exact,
            CategoryID = parameters.Category,
            Status = parameters.Status,
            DocumentIDs = docIDs,
            ExternalUrl = parameters.ExtURL,
            ExternalID = parameters.ExtID,
            TimeframeStart = parameters.After,
            TimeframeEnd = parameters.Before
        };
        viewModel = await BeezMart.DAL.Archiving.Documents.LookupAsync(filter);
        return PartialView(viewModel);
    }
    public async Task<IActionResult> _Search_Export(Search_Params parameters)
    {
        DocumentModel.LookupFilter filter;
        List<DocumentModel> records;
        if (parameters.FilterIsSet)
        {
            List<long>? docIDs = null;
            #region split IDs into lists
            if (!string.IsNullOrEmpty(parameters.IDs))
            {
                char[] separators = { ';', ',', '|', ' ' };
                int id;
                string[] IDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in IDs)
                {
                    if (int.TryParse(s, out id))
                    {
                        if (docIDs == null) docIDs = new List<long> { };
                        docIDs.Add(id);
                    }
                }
            }
            #endregion
            filter = new DocumentModel.LookupFilter()
            {
                TitlePattern = parameters.Title,
                TitleExactMatch = parameters.Exact,
                CategoryID = parameters.Category,
                Status = parameters.Status,
                DocumentIDs = docIDs,
                ExternalUrl = parameters.ExtURL,
                ExternalID = parameters.ExtID,
                TimeframeStart = parameters.After,
                TimeframeEnd = parameters.Before
            };
            records = await BeezMart.DAL.Archiving.Documents.LookupAsync(filter);
        }
        else
        {
            int maxCount = int.Parse(MyApp.ConfigSettings["Archiving.Search.Result.MaxCount.Unfiltered"]);
            filter = new DocumentModel.LookupFilter() { };
            records = await BeezMart.DAL.Archiving.Documents.ListLatestAsync(maxCount);
        }
        string xlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        string fileName = "BeezMart-Archiving-Documents.xlsx";
        using (ExcelPackage excelPackage = new ExcelPackage())
        {
            ExcelWorksheet excelWorksheet, wks;
            ExcelRange topRow;
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Documents");
            #region populate worksheet with records
            wks = excelWorksheet;
            int col = 1;
            wks.SetValue(1, col, "Document_ID"); col++;			    // Column 
            wks.SetValue(1, col, "Title"); col++;			        // Column 
            wks.SetValue(1, col, "Document_Date"); col++;			// Column 
            wks.SetValue(1, col, "Expiring_Date"); col++;			// Column 
            wks.SetValue(1, col, "Closing_Date"); col++;			// Column 
            wks.SetValue(1, col, "External_ID"); col++;			    // Column 
            wks.SetValue(1, col, "External_URL"); col++;			// Column 
            wks.SetValue(1, col, "Category_ID"); col++;			    // Column 
            wks.SetValue(1, col, "Category_Name"); col++;			// Column 
            wks.SetValue(1, col, "Sub_Category"); col++;			// Column 
            wks.SetValue(1, col, "Document_Status_ID"); col++;		// Column 
            wks.SetValue(1, col, "Document_Status_Name"); col++;	// Column 
            wks.SetValue(1, col, "Added_On"); col++;			    // Column 
            wks.SetValue(1, col, "Updated_On"); col++;			    // Column 
            wks.SetValue(1, col, "Updated_By"); col++;			    // Column 
            topRow = wks.Cells["A1:O1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            int row = 1;
            foreach (DocumentModel item in records)
            {
                row++;
                col = 1;
                wks.SetValue(row, col, item.ID); col++;
                wks.SetValue(row, col, item.Title); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.DocumentDate)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ExpiringDate)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ClosingDate)); col++;
                wks.SetValue(row, col, item.ExternalID); col++;
                wks.SetValue(row, col, item.ExternalUrl); col++;
                wks.SetValue(row, col, item.CategoryID); col++;
                wks.SetValue(row, col, item.CategoryName); col++;
                wks.SetValue(row, col, item.SubCategory); col++;
                wks.SetValue(row, col, (byte)item.Status); col++;
                wks.SetValue(row, col, item.Status.ToString()); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.AddedOn)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.UpdatedOn)); col++;
                wks.SetValue(row, col, item.UpdatedBy); col++;
            }
            wks.DefaultColWidth = 20;
            for (col = 1; col < 19; col++) wks.Column(col).AutoFit();
            #endregion
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Filter");
            #region populate filtering criteria
            wks = excelWorksheet;
            row = 0;
            row++; wks.SetValue(row, 1, "Selection filter");
            wks.SetValue(row, 2, parameters.FilterIsSet ? "SEE HOW RECORDS WERE SELECTED" : "NO FILTERING CRITERIA WAS SPECIFIED");
            row++; wks.SetValue(row, 1, "Document title pattern");
            wks.SetValue(row, 2, filter.TitlePattern);
            row++; wks.SetValue(row, 1, "Title exact match");
            wks.SetValue(row, 2, filter.TitleExactMatch.ToString());
            row++; wks.SetValue(row, 1, "Category");
            wks.SetValue(row, 2, filter.CategoryID);
            row++; wks.SetValue(row, 1, "Status");
            wks.SetValue(row, 2, filter.Status);
            row++; wks.SetValue(row, 1, "Document IDs");
            wks.SetValue(row, 2, filter.DocumentIDs);
            row++; wks.SetValue(row, 1, "External URL");
            wks.SetValue(row, 2, filter.ExternalUrl);
            row++; wks.SetValue(row, 1, "External ID");
            wks.SetValue(row, 2, filter.ExternalID);
            row++; wks.SetValue(row, 1, "Added or having date after");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.TimeframeStart));
            row++; wks.SetValue(row, 1, "Added or having date before");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.TimeframeEnd));
            topRow = wks.Cells["A1:B1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            wks.Column(1).AutoFit();
            wks.Column(2).AutoFit();
            #endregion
            return File(excelPackage.GetAsByteArray(), xlsxContentType, fileName);
        }
    }
}
