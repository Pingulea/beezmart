﻿using BeezMart.Areas.Archiving.ViewModels.DocLinks;
using BeezMart.DAL.Archiving;
using BeezMart.Entities;
using BeezMart.Entities.Archiving;
using BeezMart.Entities.Billing;
using BeezMart.Entities.CRM;
using BeezMart.Entities.TimeSheet;
using BeezMart.Utils;
using BeezMart;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Archiving.Controllers;

[Area("Archiving")]
[Authorize]
public class DocLinksController : Controller
{
    protected static string _limitedDefaultLookupResult1 = "Since you haven't specified any filtering criteria, the lookup will only show maximum {0} {1} records from database.";
    protected static string _limitedDefaultLookupResult2 = "No search criteria is specified. Only the latest {0} {1} records will show up at most.";

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Link(Link_Params parameters)
    {
        switch (parameters.EntityType)
        {
            case EntityType.CrmOrganization:
                int OrganizationID;
                if (parameters.EntityReferences != null)
                    foreach (string entityRef in parameters.EntityReferences)
                    {
                        OrganizationID = int.Parse(entityRef);
                        await DocLinks.AddOrganizationAsync(parameters.DocumentID, OrganizationID, DateTime.Now, this.User.Identity?.Name);
                    }
                break;
            case EntityType.CrmPerson:
                int PersonID;
                if (parameters.EntityReferences != null)
                    foreach (string entityRef in parameters.EntityReferences)
                    {
                        PersonID = int.Parse(entityRef);
                        await DocLinks.AddPersonAsync(parameters.DocumentID, PersonID, DateTime.Now, this.User.Identity?.Name);
                    }
                break;
            case EntityType.BillingInvoice:
                int InvoiceID;
                if (parameters.EntityReferences != null)
                    foreach (string entityRef in parameters.EntityReferences)
                    {
                        InvoiceID = int.Parse(entityRef);
                        await DocLinks.AddInvoiceAsync(parameters.DocumentID, InvoiceID, DateTime.Now, this.User.Identity?.Name);
                    }
                break;
            case EntityType.SalesProject:
                int ProjectID;
                if (parameters.EntityReferences != null)
                    foreach (string entityRef in parameters.EntityReferences)
                    {
                        ProjectID = int.Parse(entityRef);
                        await DocLinks.AddProjectAsync(parameters.DocumentID, ProjectID, DateTime.Now, this.User.Identity?.Name);
                    }
                break;
            case EntityType.SalesActivity:
                long ActivityID;
                if (parameters.EntityReferences != null)
                    foreach (string entityRef in parameters.EntityReferences)
                    {
                        ActivityID = long.Parse(entityRef);
                        await DocLinks.AddActivityAsync(parameters.DocumentID, ActivityID, DateTime.Now, this.User.Identity?.Name);
                    }
                break;
            default:
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"Entities of unknown or unsupported entity type cannot be linked to a document: {parameters.EntityType}.");
                if (parameters.EntityReferences != null)
                    foreach (string entityRef in parameters.EntityReferences)
                    {
                        ActivityID = long.Parse(entityRef);
                        msg.AppendMessage($"+ Entity reference: {entityRef}.");
                    }
                ViewBag.AppMessage = msg.ToHtmlError();
                return View("Message");
        }
        if (parameters.ReturnUrl == null) return RedirectToAction(nameof(BeezMart.Areas.Archiving.Controllers.HomeController.Index), controllerName: nameof(BeezMart.Areas.Archiving.Controllers.HomeController));
        else return Redirect(parameters.ReturnUrl);
    }

    public async Task<ViewResult> LinkOrganizations(long DocumentID)
    {
        LinkOrganizations_ViewModel viewModel = new LinkOrganizations_ViewModel { };
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(DocumentID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _LinkOrganizations(_LinkOrganizations_Params parameters)
    {
        OrganizationModel.LookupFilter filter = new OrganizationModel.LookupFilter()
        {
            NamePattern = parameters.Name,
            NameExactMatch = parameters.NameExactMatching,
            Contact = parameters.Contact,
            City = parameters.City,
            InSegments = parameters.Segments,
            HasFlags = parameters.Flags
        };
        if (!filter.HasAnyCriteria)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["CRM.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult1, maxRowCount, "organization"));
            ViewBag.AppMessage = msg.ToHtmlWarning();
        }
        List<OrganizationModel> viewModel = await BeezMart.DAL.CRM.Organizations.LookupAsync(filter);
        return PartialView(viewModel);
    }

    public async Task<ViewResult> LinkPersons(long DocumentID)
    {
        LinkPersons_ViewModel viewModel = new LinkPersons_ViewModel { };
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(DocumentID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _LinkPersons(_LinkPersons_Params parameters)
    {
        PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
        {
            NamePattern = parameters.Name,
            NameExactMatch = parameters.NameExactMatching,
            Contact = parameters.Contact,
            City = parameters.City,
            InSegments = parameters.Segments,
            HasFlags = parameters.Flags
        };
        if (!filter.HasAnyCriteria)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["CRM.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult1, maxRowCount, "person"));
            ViewBag.AppMessage = msg.ToHtmlWarning();
        }
        List<PersonModel> viewModel = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
        return PartialView(viewModel);
    }

    public async Task<ViewResult> LinkInvoices(long DocumentID)
    {
        LinkInvoices_ViewModel viewModel = new LinkInvoices_ViewModel { };
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(DocumentID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _LinkInvoices(_LinkInvoices_Params parameters)
    {
        InvoiceModel.LookupFilter filter = new InvoiceModel.LookupFilter()
        {
            CustomerNamePattern = parameters.Customer,
            CustomerNameExactMatch = parameters.Exact,
            OrganizationID = parameters.OrganizationID,
            PersonID = parameters.PersonID,
            Currency = parameters.Curr,
            InvoiceStatus = parameters.Status,
            IssuedAfter = parameters.After,
            IssuedBefore = parameters.Before,
            //AccountingIDs = parameters.AccIDs,
            //InvoiceIDs = parameters.IDs,
            SearchOperator = SearchOperator.And
        };
        char[] separators = { ' ', ',', ';', '|', '\t', '\r' };
        if (!string.IsNullOrWhiteSpace(parameters.AccIDs))
        {
            string[] invoiceAccountingIDs = parameters.AccIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            if (invoiceAccountingIDs.Length > 0) filter.AccountingIDs = invoiceAccountingIDs.ToList();
        }
        if (!string.IsNullOrWhiteSpace(parameters.IDs))
        {
            string[] invoiceIDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            if (invoiceIDs.Length > 0)
            {
                filter.InvoiceIDs = new List<int> { };
                int id;
                foreach (string s in invoiceIDs)
                    if (int.TryParse(s, out id))
                        filter.InvoiceIDs.Add(id);
            }
        }
        if (!filter.HasAnyCriteria)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["Billing.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult2, maxRowCount, "invoice"));
            ViewBag.AppMessage = msg.ToHtmlWarning();
            filter = new InvoiceModel.LookupFilter()
            {
                IssuedAfter = DateTime.Now.AddMonths(-3),
                IssuedBefore = DateTime.Now.AddMonths(1)
            };
        }
        List<InvoiceModel> viewModel = await BeezMart.DAL.Billing.Invoices.LookupAsync(filter);
        return PartialView(viewModel);
    }

    public async Task<ViewResult> LinkProjects(long DocumentID)
    {
        LinkProjects_ViewModel viewModel = new LinkProjects_ViewModel { };
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(DocumentID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _LinkProjects(_LinkProjects_Params parameters)
    {
        ProjectModel.LookupFilter filter = new ProjectModel.LookupFilter
        {
            TitlePattern = parameters.Title,
            TitleExactMatch = parameters.Exact,
            CustomerNamePattern = parameters.Customer,
            OrganizationID = parameters.OrganizationID,
            PersonID = parameters.PersonID,
            TimeframeStart = parameters.After,
            TimeframeEnd = parameters.Before,
            //ProjectIDs = parameters.IDs,
            ProjectManager = parameters.PM,
            CategoryID = parameters.Categ,
            SearchOperator = SearchOperator.And
        };
        char[] separators = { ' ', ',', ';', '|', '\t', '\r' };
        if (!string.IsNullOrWhiteSpace(parameters.IDs))
        {
            string[] projectIDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            if (projectIDs.Length > 0)
            {
                filter.ProjectIDs = new List<int> { };
                int id;
                foreach (string s in projectIDs)
                    if (int.TryParse(s, out id))
                        filter.ProjectIDs.Add(id);
            }
        }
        if (!filter.HasAnyCriteria)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["Sales.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult2, maxRowCount, "project"));
            ViewBag.AppMessage = msg.ToHtmlWarning();
        }
        List<ProjectModel> viewModel = await BeezMart.DAL.TimeSheet.Projects.LookupAsync(filter);
        return PartialView(viewModel);
    }

    public async Task<ViewResult> LinkActivities(long DocumentID)
    {
        LinkActivities_ViewModel viewModel = new LinkActivities_ViewModel { };
        viewModel.Document = await BeezMart.DAL.Archiving.Documents.RetrieveAsync(DocumentID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _LinkActivities(_LinkActivities_Params parameters)
    {
        ActivityModel.LookupFilter filter = new ActivityModel.LookupFilter
        {
            TitlePattern = parameters.Title,
            TitleExactMatch = false,
            CustomerNamePattern = parameters.Customer,
            ProjectTitlePattern = parameters.Prj,
            OrganizationID = parameters.OrganizationID,
            PersonID = parameters.PersonID,
            TimeframeStart = parameters.After,
            TimeframeEnd = parameters.Before,
            SearchOperator = SearchOperator.And
        };
        if (!filter.HasAnyCriteria)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["Sales.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult2, maxRowCount, "project"));
            ViewBag.AppMessage = msg.ToHtmlWarning();
            filter = new ActivityModel.LookupFilter()
            {
                TimeframeStart = DateTime.Now.AddMonths(-6),
                TimeframeEnd = DateTime.Now.AddMonths(1)
            };
        }
        List<ActivityModel> viewModel = await BeezMart.DAL.TimeSheet.Activities.LookupAsync(filter);
        return PartialView(viewModel);
    }

    /// Lookup documents with a search form, to be linked to an entity (Invoice, Organization, Person, Project...)
    public async Task<PartialViewResult> _LinkDocuments(LinkDocuments_ViewModel viewModel)
    {
        DocumentModel.LookupFilter filter = new DocumentModel.LookupFilter
        {
            TitlePattern = viewModel.Title,
            TitleExactMatch = viewModel.Exact,
            CategoryID = viewModel.Categ,
            Status = viewModel.Status,
            ExternalUrl = viewModel.ExtUrl,
            ExternalID = viewModel.ExtID,
            TimeframeStart = viewModel.Start,
            TimeframeEnd = viewModel.End,
            //DocumentIDs = viewModel.IDs,
            SearchOperator = SearchOperator.And
        };
        char[] separators = { ' ', ',', ';', '|', '\t', '\r' };
        if (!string.IsNullOrWhiteSpace(viewModel.IDs))
        {
            string[] projectIDs = viewModel.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            if (projectIDs.Length > 0)
            {
                filter.DocumentIDs = new List<long> { };
                long id;
                foreach (string s in projectIDs)
                    if (long.TryParse(s, out id))
                        filter.DocumentIDs.Add(id);
            }
        }
        if (!filter.HasAnyCriteria)
        {
            AppMessage msg = new AppMessage();
            string maxRowCount = MyApp.ConfigSettings["Archiving.Search.Result.MaxCount.Unfiltered"];
            msg.AppendMessage(string.Format(_limitedDefaultLookupResult2, maxRowCount, "document"));
            ViewBag.AppMessage = msg.ToHtmlWarning();
            filter = new DocumentModel.LookupFilter()
            {
                TimeframeStart = DateTime.Now.AddMonths(-6),
                TimeframeEnd = DateTime.Now.AddMonths(1)
            };
        }
        if (viewModel.EntityReference != null)
            switch (viewModel.EntityType)
            {
                case EntityType.CrmOrganization:
                    int OrganizationID = int.Parse(viewModel.EntityReference);
                    viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
                    break;
                case EntityType.CrmPerson:
                    int PersonID = int.Parse(viewModel.EntityReference);
                    viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
                    break;
                case EntityType.BillingInvoice:
                    int InvoiceID = int.Parse(viewModel.EntityReference);
                    viewModel.Invoice = await BeezMart.DAL.Billing.Invoices.RetrieveAsync(InvoiceID);
                    break;
                case EntityType.SalesProject:
                    int ProjectID = int.Parse(viewModel.EntityReference);
                    viewModel.Project = await BeezMart.DAL.TimeSheet.Projects.RetrieveAsync(ProjectID);
                    break;
                case EntityType.SalesActivity:
                    long ActivityID = long.Parse(viewModel.EntityReference);
                    viewModel.Activity = await BeezMart.DAL.TimeSheet.Activities.RetrieveAsync(ActivityID);
                    break;
                default:
                    AppMessage msg = new AppMessage();
                    msg.AppendMessage($"Unknown or unsupported entity type; cannot retrieve the entity: {viewModel.EntityType}[{viewModel.EntityReference}].");
                    ViewBag.AppMessage = msg.ToHtmlError();
                    return PartialView(viewModel);
            }
        viewModel.Documents = await BeezMart.DAL.Archiving.Documents.LookupAsync(filter);
        return PartialView(viewModel);
    }

    public async Task<PartialViewResult> Organizations(long DocumentID)
    {
        List<OrganizationModel> viewModel = await DocLinks.ListOrganizationsAsync(DocumentID);
        return PartialView(viewModel);
    }
    public async Task<PartialViewResult> Persons(long DocumentID)
    {
        List<PersonModel> viewModel = await DocLinks.ListPersonsAsync(DocumentID);
        return PartialView(viewModel);
    }
    public async Task<PartialViewResult> Invoices(long DocumentID)
    {
        List<InvoiceModel> viewModel = await DocLinks.ListInvoicesAsync(DocumentID);
        return PartialView(viewModel);
    }
    public async Task<PartialViewResult> Projects(long DocumentID)
    {
        List<ProjectModel> viewModel = await DocLinks.ListProjectsAsync(DocumentID);
        return PartialView(viewModel);
    }
    public async Task<PartialViewResult> Activities(long DocumentID)
    {
        List<ActivityModel> viewModel = await DocLinks.ListActivitiesAsync(DocumentID);
        return PartialView(viewModel);
    }
    /// List documents associated to a certain entity (Invoice, Organization, Person, Project...)
    public async Task<PartialViewResult> Documents(EntityType EntityType, string EntityReference)
    {
        List<DocumentModel> viewModel;
        switch (EntityType)
        {
            case EntityType.CrmOrganization:
                int OrganizationID = int.Parse(EntityReference);
                viewModel = await DocLinks.ListForOrganizationAsync(OrganizationID);
                break;
            case EntityType.CrmPerson:
                int PersonID = int.Parse(EntityReference);
                viewModel = await DocLinks.ListForPersonAsync(PersonID);
                break;
            case EntityType.BillingInvoice:
                int InvoiceID = int.Parse(EntityReference);
                viewModel = await DocLinks.ListForInvoiceAsync(InvoiceID);
                break;
            case EntityType.SalesProject:
                int ProjectID = int.Parse(EntityReference);
                viewModel = await DocLinks.ListForProjectAsync(ProjectID);
                break;
            case EntityType.SalesActivity:
                long ActivityID = long.Parse(EntityReference);
                viewModel = await DocLinks.ListForActivityAsync(ActivityID);
                break;
            default:
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"Unknown or unsupported entity type; cannot retrieve associated documents list: {EntityType}[{EntityReference}].");
                ViewBag.AppMessage = msg.ToHtmlError();
                return PartialView("_Message");
        }
        return PartialView(viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> UnlinkOrganization(int OrganizationID, long DocumentID)
    {
        await DocLinks.DeleteLinkToOrganizationAsync(OrganizationID, DocumentID);
        List<OrganizationModel> viewModel = await DocLinks.ListOrganizationsAsync(DocumentID);
        return PartialView("Organizations", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> UnlinkPerson(int PersonID, long DocumentID)
    {
        await DocLinks.DeleteLinkToPersonAsync(PersonID, DocumentID);
        List<PersonModel> viewModel = await DocLinks.ListPersonsAsync(DocumentID);
        return PartialView("Persons", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> UnlinkInvoice(int InvoiceID, long DocumentID)
    {
        await DocLinks.DeleteLinkToInvoiceAsync(InvoiceID, DocumentID);
        List<InvoiceModel> viewModel = await DocLinks.ListInvoicesAsync(DocumentID);
        return PartialView("Invoices", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> UnlinkProject(int ProjectID, long DocumentID)
    {
        await DocLinks.DeleteLinkToProjectAsync(ProjectID, DocumentID);
        List<ProjectModel> viewModel = await DocLinks.ListProjectsAsync(DocumentID);
        return PartialView("Projects", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> UnlinkActivity(long ActivityID, long DocumentID)
    {
        await DocLinks.DeleteLinkToActivityAsync(ActivityID, DocumentID);
        List<ActivityModel> viewModel = await DocLinks.ListActivitiesAsync(DocumentID);
        return PartialView("Activities", viewModel);
    }
}
