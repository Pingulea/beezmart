﻿using BeezMart.Areas.Archiving.ViewModels.Documents;
using BeezMart.Entities.Archiving;
using Microsoft.AspNetCore.Mvc;
using Normalization = BeezMart.Caching.Archiving;
using Repo = BeezMart.DAL.Archiving;
using Microsoft.AspNetCore.Authorization;

namespace BeezMart.Areas.Archiving.Controllers;

[Area("Archiving")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForDocumentManagement)]
public class HomeController : Controller
{
    public async Task<ViewResult> Index()
    {
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.Documents = await Repo.Documents.ListLatestAsync(100);
        List<DocumentModel.CategoryModel> categories = await Normalization.DocumentCategories.GetSubCategoriesAsync();
        foreach (DocumentModel.CategoryModel category in categories)
            if (category.ID != null) viewModel.CategoryNames.Add(category.ID.Value, $"{category.Name} :: {category.SubCategory}");
        return View(viewModel);
    }
}