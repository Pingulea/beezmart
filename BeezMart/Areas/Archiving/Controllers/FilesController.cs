﻿using BeezMart.Areas.Archiving.ViewModels.Files;
using BeezMart.Utils;
using BeezMart.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Archiving.Controllers;

[Area("Archiving")]
[Authorize]
public class FilesController : Controller
{
    public async Task<PartialViewResult> Index(long DocumentID, string AddedOn)
    {
        DateTime addedOn;
        if (!DateTime.TryParse(AddedOn, out addedOn))
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.AppendMessage("Invalid document archival date.");
            msg.AppendMessage("Please use the ISO format YYYY-MM-DD.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        Index_ViewModel viewModel = new Index_ViewModel { };
        AzureBlobService azureBlobService = new AzureBlobService();
        viewModel.FileNames = await azureBlobService.GetFileNamesAsync(DocumentID, addedOn);
        viewModel.DocumentID = DocumentID;
        viewModel.AddedOn = addedOn;
        return PartialView(viewModel);
    }

    public async Task Download(long DocumentID, string AddedOn, string File)
    {
        DateTime addedOn;
        if (!DateTime.TryParse(AddedOn, out addedOn))
        {
            string error = "Invalid document archival date on calling the controller to download file. Please use the ISO format YYYY-MM-DD.";
            byte[] errorArray = System.Text.Encoding.UTF8.GetBytes(error);
            Response.ContentType = "text/plain";
            await Response.Body.WriteAsync(errorArray, 0, errorArray.Length);
            await Response.Body.FlushAsync();
            return;
        }
        Response.ContentType = "application/octet-stream";
        string fileName = System.Net.WebUtility.UrlDecode(File);
        int dotIndex = fileName.LastIndexOf('.');
        if (dotIndex > 0)
        {
            string fileExtension = fileName.Substring(dotIndex);
            Response.ContentType = MimeTypes.GetMimeType(fileExtension);
        }
        Response.Headers.Append("content-disposition", $"attachment; filename=\"{fileName}\"");
        AzureBlobService azureBlobService = new AzureBlobService();
        await azureBlobService.ReadToStreamAsync(fileName, Response.Body, DocumentID, addedOn);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Upload(long DocumentID, string AddedOn, List<IFormFile> Files)
    {
        // if multiple files not working, remove parameters from action and use
        // foreach (var fileName in Request.Files.AllKeys)
        // https://stackoverflow.com/questions/31079667/dropzone-js-uploading-multiples-files-using-asp-net-mvc#33127590
        // https://dotnetcoretutorials.com/2017/03/12/uploading-files-asp-net-core/
        // long DocumentID = long.Parse(Request.Form["DocumentID"]);
        // string AddedOn = Request.Form["AddedOn"];
        DateTime addedOn;
        if (!DateTime.TryParse(AddedOn, out addedOn))
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.AppendMessage("Invalid document archival date.");
            msg.AppendMessage("Please use the ISO format YYYY-MM-DD.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        //AppMessage info = new AppMessage(AppMessageType.Info);
        //info.AppendMessage($"DocumentID = {DocumentID}");
        //info.AppendMessage($"Files count = {Files.Count}");
        //ViewBag.AppMessage = info.ToHtmlWarning();
        AzureBlobService azureBlobService = new AzureBlobService();
        List<string> errors = new List<string> { };
        foreach (IFormFile formFile in Files)
        {
            if (formFile.Length > 0)
            {
                try
                {
                    using (Stream uploadedStream = formFile.OpenReadStream())
                    {
                        if (!(await azureBlobService.SaveFileAsync(uploadedStream, formFile.FileName, DocumentID, addedOn)))
                        {
                            errors.Add($"Could not upload file '{formFile.Name}'. Maybe file name already exists?");
                        }
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(ex.Message);
                }
            }
        }
        if (errors.Count > 0)
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.Title = "Problems encountered";
            msg.AppendMessage("Some problem(s) occured during file(s)upload and save:");
            foreach (string err in errors) msg.AppendMessage(err);
            msg.AppendLinkButton("Back to document record", Url.Action("View", "Documents", new { area = "Archiving", ID = DocumentID }));
            return View("Message", msg);
        }
        return RedirectToAction("View", "Documents", new { area = "Archiving", ID = DocumentID });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> Rename(long DocumentID, string AddedOn, string OldFileName, string NewFileName)
    {
        DateTime addedOn;
        if (!DateTime.TryParse(AddedOn, out addedOn))
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.AppendMessage("Invalid document archival date.");
            msg.AppendMessage("Please use the ISO format YYYY-MM-DD.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        AzureBlobService azureBlobService = new AzureBlobService();
        await azureBlobService.RenameFileAsync(OldFileName, NewFileName, DocumentID, addedOn);
        Index_ViewModel viewModel = new Index_ViewModel { };
        viewModel.FileNames = await azureBlobService.GetFileNamesAsync(DocumentID, addedOn);
        viewModel.DocumentID = DocumentID;
        viewModel.AddedOn = addedOn;
        return PartialView(nameof(this.Index), viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> Delete(long DocumentID, string AddedOn, string FileName)
    {
        DateTime addedOn;
        if (!DateTime.TryParse(AddedOn, out addedOn))
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.AppendMessage("Invalid document archival date.");
            msg.AppendMessage("Please use the ISO format YYYY-MM-DD.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        AzureBlobService azureBlobService = new AzureBlobService();
        await azureBlobService.DeleteFileAsync(FileName, DocumentID, addedOn);
        Index_ViewModel viewModel = new Index_ViewModel { };
        viewModel.FileNames = await azureBlobService.GetFileNamesAsync(DocumentID, addedOn);
        viewModel.DocumentID = DocumentID;
        viewModel.AddedOn = addedOn;
        return PartialView(nameof(this.Index), viewModel);
    }
}