using BeezMart.Entities.Archiving;

namespace BeezMart.Areas.Archiving.ViewModels.Home;

public class Index_ViewModel
{
    public List<DocumentModel>? Documents { get; set; }
    public Dictionary<short, string> CategoryNames { get; set; } = new Dictionary<short, string> { };
}
