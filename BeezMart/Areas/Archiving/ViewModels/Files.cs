namespace BeezMart.Areas.Archiving.ViewModels.Files;

public class Index_ViewModel
{
    public IEnumerable<string>? FileNames { get; set; }
    public long DocumentID { get; set; }
    public DateTime AddedOn { get; set; }
}

public static class FileIcons
{
    /// <summary>
    ///     <para>If extension is unknown or unmapped, return Font Awesome "fa fa-file-o".</para>
    /// </summary>
    public static readonly string DefaultClassName = "fa fa-file-o";

    /// <summary>
    ///     <para>Maps file extensions to icon names for file types in Font Awsome</para>
    ///     <ref>https://www.w3schools.com/icons/fontawesome_icons_filetype.asp</ref>
    /// </summary>
    public static Dictionary<string, string> Mappings = new Dictionary<string, string>
        {
            {"7z",          "fa fa-file-archive-o"},
            {"avi",         "fa fa-file-movie-o"},
            {"bak",         "fa fa-file"},
            {"bmp",         "fa fa-file-image-o"},
            {"config",      "fa fa-code-text"},
            {"cs",          "fa fa-file-text"},
            {"dat",         "fa fa-file"},
            {"docx",        "fa fa-file-word-o"},
            {"doc",         "fa fa-file-word-o"},
            {"gif",         "fa fa-file-picture-o"},
            {"htm",         "fa fa-file-code-o"},
            {"html",        "fa fa-file-code-o"},
            {"jpeg",        "fa fa-file-photo-o"},
            {"jpg",         "fa fa-file-photo-o"},
            {"ldf",         "fa fa-file"},
            {"mdf",         "fa fa-file"},
            {"mp3",         "fa fa-audio-o"},
            {"pdf",         "fa fa-file-pdf-o"},
            {"png",         "fa fa-file-image-o"},
            {"pptx",        "fa fa-file-powerpoint-o"},
            {"ppt",         "fa fa-file-powerpoint-o"},
            {"ps",          "fa fa-file-text"},
            {"rar",         "fa fa-file-zip-o"},
            {"settings",    "fa fa-file"},
            {"txt",         "fa fa-file-text-o"},
            {"vb",          "fa fa-file-text"},
            {"wav",         "fa fa-file-audio-o"},
            {"wma",         "fa fa-file-audio-o"},
            {"wmv",         "fa fa-file-video-o"},
            {"xlsx",        "fa fa-file-excel-o"},
            {"xls",         "fa fa-file-excel-o"},
            {"xml",         "fa fa-file-code-o"},
            {"zip",         "fa fa-file-archive-o"}
        };
    public static string GetClassNameForFileName(string fileName)
    {
        int indexOfDot = fileName.LastIndexOf('.');
        if (indexOfDot < 0) return FileIcons.DefaultClassName;
        if (indexOfDot == 0) return FileIcons.DefaultClassName;
        if (indexOfDot == fileName.Length - 1) return FileIcons.DefaultClassName;
        string fileExtension = fileName.ToLower().Substring(indexOfDot + 1);
        return Mappings.ContainsKey(fileExtension) ? Mappings[fileExtension] : FileIcons.DefaultClassName;
    }
    public static string GetClassNameForFileExtension(string fileExtension)
    {
        string extension = fileExtension.ToLower();
        return Mappings.ContainsKey(extension) ? Mappings[extension] : FileIcons.DefaultClassName;
    }
}