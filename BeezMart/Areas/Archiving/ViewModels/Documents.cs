﻿using BeezMart.Entities.Archiving;

namespace BeezMart.Areas.Archiving.ViewModels.Documents;
public class View_ViewModel
{
    public DocumentModel? Document { get; set; }
    public string? DocumentCategoryName { get; set; }
}
public class Index_ViewModel
{
    public List<DocumentModel>? Documents { get; set; }
    public Dictionary<short, string> CategoryNames { get; set; } = new Dictionary<short, string> { };
}
public class Add_ViewModel
{
    public DocumentModel? Document { get; set; }
    public string? DocumentTitle { get; set; }
}
public class Edit_ViewModel
{
    public DocumentModel? Document { get; set; }
}
public class Search_ViewModel
{
    public string? Title { get; set; }
    public bool Exact { get; set; }
    public short? Category { get; set; }
	public DocumentStatus? Status { get; set; }			// Status
	public string? IDs { get; set; }
    public string? ExtURL { get; set; }
    public string? ExtID { get; set; }
	public DateTime? After { get; set; }
	public DateTime? Before { get; set; }
	public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Title)) return true;
			if (!string.IsNullOrEmpty(this.ExtURL)) return true;
			if (!string.IsNullOrEmpty(this.ExtID)) return true;
            if (this.Category.HasValue) return true;
            if (this.Status.HasValue) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			if (!string.IsNullOrEmpty(IDs)) return true;
			return false;
		}
	}
}
public class Search_Params
{
    public string? Title { get; set; }
    public bool Exact { get; set; }
    public short? Category { get; set; }
	public DocumentStatus? Status { get; set; }			// Status
	public string? IDs { get; set; }
    public string? ExtURL { get; set; }
    public string? ExtID { get; set; }
	public DateTime? After { get; set; }
	public DateTime? Before { get; set; }
	public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Title)) return true;
			if (!string.IsNullOrEmpty(this.ExtURL)) return true;
			if (!string.IsNullOrEmpty(this.ExtID)) return true;
            if (this.Category.HasValue) return true;
            if (this.Status.HasValue) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			if (!string.IsNullOrEmpty(IDs)) return true;
			return false;
		}
	}
}