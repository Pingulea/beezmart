﻿using BeezMart.Entities;
using BeezMart.Entities.Archiving;
using BeezMart.Entities.Billing;
using BeezMart.Entities.CRM;
using BeezMart.Entities.TimeSheet;

namespace BeezMart.Areas.Archiving.ViewModels.DocLinks;

public class Link_Params
{
	public long DocumentID { get; set; }
	public EntityType EntityType { get; set; }
	public string[]? EntityReferences { get; set; }
	public string? ReturnUrl { get; set; }
}
public class LinkOrganizations_ViewModel
{
    public string? Name { get; set; }
	public bool Exact { get; set; }			// NameExactMatching
	public List<byte>? Segments { get; set; }
	public List<short>? Flags { get; set; }
	public string? Contact { get; set; }
	public string? City { get; set; }
    public EntityType EntityType { get; } = EntityType.CrmOrganization;
    public DocumentModel? Document { get; set; }
}
public class _LinkOrganizations_Params
{
    public string? Name { get; set; }
    public bool NameExactMatching { get; set; }
    public string? Contact { get; set; }
    public string? City { get; set; }
    public List<short>? Flags { get; set; }
    public List<byte>? Segments { get; set; }
}
public class LinkPersons_ViewModel
{
    public string? Name { get; set; }
	public bool Exact { get; set; }			// NameExactMatching
	public List<byte>? Segments { get; set; }
	public List<short>? Flags { get; set; }
	public string? Contact { get; set; }
	public string? City { get; set; }
    public EntityType EntityType { get; } = EntityType.CrmPerson;
    public DocumentModel? Document { get; set; }
}
public class _LinkPersons_Params
{
    public string? Name { get; set; }
    public bool NameExactMatching { get; set; }
    public string? Contact { get; set; }
    public string? City { get; set; }
    public List<short>? Flags { get; set; }
    public List<byte>? Segments { get; set; }
}
public class LinkInvoices_ViewModel
{
    public string? Customer { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public bool Exact { get; set; }
	public DateTime? After { get; set; }
	public DateTime? Before { get; set; }
	public string? IDs { get; set; }
	public string? AccIDs { get; set; }					// Accounting IDs
	public string? Curr { get; set; }					// Currency
	public InvoiceStatus? Status { get; set; }			// Status
    public bool Overdue { get; set; }
    public EntityType EntityType { get; } = EntityType.BillingInvoice;
    public DocumentModel? Document { get; set; }
}
public class _LinkInvoices_Params
{
    public string? Customer { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public bool Exact { get; set; }
    public DateTime? After { get; set; }
    public DateTime? Before { get; set; }
    public string? IDs { get; set; }
    public string? AccIDs { get; set; }                  // Accounting IDs
    public string? Curr { get; set; }                    // Currency
    public InvoiceStatus? Status { get; set; }			// Status
    public bool Overdue { get; set; }
}
public class LinkProjects_ViewModel
{
    public string? Title { get; set; }
    public string? Customer { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public bool Exact { get; set; }
	public DateTime? After { get; set; }
	public DateTime? Before { get; set; }
    public string? IDs { get; set; }
    public string? PM { get; set; }
    public short? Categ { get; set; }
    public ProjectStatus? Status { get; set; }
    public EntityType EntityType { get; } = EntityType.SalesProject;
    public DocumentModel? Document { get; set; }
}
public class _LinkProjects_Params
{
    public string? Title { get; set; }
    public string? Customer { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public bool Exact { get; set; }
    public DateTime? After { get; set; }
    public DateTime? Before { get; set; }
    public string? IDs { get; set; }
    public string? PM { get; set; }
    public short? Categ { get; set; }
    public ProjectStatus? Status { get; set; }
}
public class LinkActivities_ViewModel
{
    public string? Title { get; set; }
    public bool Exact { get; set; }
    public string? Customer { get; set; }
    public string? Prj { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
	public DateTime? After { get; set; }
	public DateTime? Before { get; set; }
    public string? IDs { get; set; }
    public short? Categ { get; set; }
    public ProjectStatus? Status { get; set; }
    public EntityType EntityType { get; } = EntityType.SalesActivity;
    public DocumentModel? Document { get; set; }
}
public class _LinkActivities_Params
{
    public string? Title { get; set; }
    public bool Exact { get; set; }
    public string? Customer { get; set; }
    public string? Prj { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
    public DateTime? After { get; set; }
    public DateTime? Before { get; set; }
    public string? IDs { get; set; }
    public short? Categ { get; set; }
    public ProjectStatus? Status { get; set; }
}
public class LinkDocuments_ViewModel
{
    public EntityType EntityType { get; set; }
    public string? EntityReference { get; set; }
    public OrganizationModel? Organization { get; set; }
    public PersonModel? Person { get; set; }
    public InvoiceModel? Invoice { get; set; }
    public ProjectModel? Project { get; set; }
    public ActivityModel? Activity { get; set; }
    public string? IDs { get; set; }
    public string? Title { get; set; }
    public bool Exact { get; set; } = false;
    public short? Categ { get; set; } = null;
    public DocumentStatus? Status { get; set; }
    public string? ExtUrl { get; set; }
    public string? ExtID { get; set; }
    public DateTime? Start { get; set; } = null;
    public DateTime? End { get; set; } = null;
    public List<DocumentModel>? Documents { get; set; }
}