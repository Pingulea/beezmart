﻿using BeezMart.Entities.CRM;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.TimeSheet.Controllers;

[Area("TimeSheet")]
public class CustomersController : Controller
{
    public async Task<PartialViewResult> _PickOrgs(string OrgsNamePattern, bool ExactMatch = false)
    {
        OrganizationModel.LookupFilter filter = new OrganizationModel.LookupFilter()
        {
            NamePattern = OrgsNamePattern,
            NameExactMatch = ExactMatch
        };
        List<OrganizationModel> SearchResult = await BeezMart.DAL.CRM.Organizations.LookupAsync(filter);
        return PartialView(SearchResult);
    }
    public async Task<PartialViewResult> _PickPers(string PersNamePattern, bool ExactMatch = false)
    {
        PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
        {
            NamePattern = PersNamePattern,
            NameExactMatch = ExactMatch
        };
        List<PersonModel> SearchResult = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
        return PartialView(SearchResult);
    }
}

