﻿using BeezMart.Areas.TimeSheet.ViewModels.Activities;
using BeezMart.Entities.TimeSheet;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Drawing;
using Repo = BeezMart.DAL.TimeSheet;

namespace BeezMart.Areas.TimeSheet.Controllers;

[Area("TimeSheet")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForProjectContribution)]
public class ActivitiesController : Controller
{
	public async Task<ViewResult> Index()
	{
		Index_ViewModel viewModel = new Index_ViewModel();
		viewModel.Activities = await Repo.Activities.ListAsync(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1));
        foreach (ActivityModel activity in viewModel.Activities) if (activity.Duration.HasValue) viewModel.TotalDuration += activity.Duration.Value;
		return View(viewModel);
	}
    public async Task<ViewResult> Recent()
    {
        Recent_ViewModel viewModel = new Recent_ViewModel();
        viewModel.Activities = await Repo.Activities.ListAsync(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1), completedBy: null);
        foreach (ActivityModel activity in viewModel.Activities) if (activity.Duration.HasValue) viewModel.TotalDuration += activity.Duration.Value;
        return View(viewModel);
    }
    public async Task<ViewResult> MyRecent()
    {
        MyRecent_ViewModel viewModel = new MyRecent_ViewModel();
        viewModel.Activities = await Repo.Activities.ListAsync(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(1), completedBy: User.Identity?.Name);
        foreach (ActivityModel activity in viewModel.Activities) if (activity.Duration.HasValue) viewModel.TotalDuration += activity.Duration.Value;
        return View(viewModel);
    }

    public async Task<ViewResult> CurrentProjects()
    {
        CurrentProjects_ViewModel viewModel = new CurrentProjects_ViewModel();
        viewModel.Projects = await Repo.Projects.ListAsync(DateTime.Now.AddMonths(-3), DateTime.Now.AddMonths(3));
        return View(viewModel);
    }
    public async Task<ViewResult> ByProject(int ID)
    {
        ByProject_ViewModel viewModel = new ByProject_ViewModel();
        viewModel.Project = await Repo.Projects.RetrieveAsync(ID);
        if (viewModel.Project == null)
        {
            AppMessage msg = new AppMessage();
            msg.Add("The activity record you're trying to edit could not be found in the database.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        if (viewModel.Project.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Project.ID is null.");
        viewModel.Activities = await Repo.Activities.ListAsync(viewModel.Project.ID.Value);
        foreach (ActivityModel activity in viewModel.Activities) if (activity.Duration.HasValue) viewModel.TotalDuration += activity.Duration.Value;
        return View(viewModel);
    }

    public async Task<ViewResult> View(long ID)
	{
        View_ViewModel viewModel = new View_ViewModel();
        viewModel.Activity = await Repo.Activities.RetrieveAsync(ID);
		if (viewModel.Activity == null)
		{
			AppMessage msg = new AppMessage();
			msg.Add("The activity record you're trying to edit could not be found in the database.");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View("Message");
		}
        if (viewModel.Activity.ProjectID.HasValue)
        {
            ProjectModel? project = await Repo.Projects.RetrieveAsync(viewModel.Activity.ProjectID.Value);
            if (project != null) viewModel.Activity.ProjectTitle = project.Title;
        }
        if (viewModel.Activity.Duration.HasValue)
            viewModel.Duration = viewModel.Activity.Duration.Value.TotalMinutes.ToString("#,##0") + " minutes, " + viewModel.Activity.Duration.Value.ToString("hh':'mm");
        return View(viewModel);
	}

    [HttpGet]
    public async Task<ViewResult> Add(int? ProjectID)
    {
        Add_ViewModel viewModel = new Add_ViewModel { };
		viewModel.Activity = new ActivityModel { };
        if (ProjectID.HasValue)
        {
            ProjectModel? project = await Repo.Projects.RetrieveAsync(ProjectID.Value);
            if (project != null)
            {
                if (project.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Project.ID is null.");
                viewModel.Activity.ProjectTitle = project.Title;
                viewModel.Activity.ProjectID = project.ID.Value;
                viewModel.Activity.OrganizationID = project.OrganizationID;
                viewModel.Activity.PersonID = project.PersonID;
                viewModel.Activity.CustomerName = project.CustomerName;
                viewModel.Activity.EndCustomer = project.EndCustomer;
            }
        }
        viewModel.Activity.Status = ActivityStatus.Completed;
        viewModel.Activity.EndedOn = viewModel.Activity.StartedOn = DateTime.Now;
        viewModel.Activity.CompletedBy = viewModel.Activity.LoggedBy = User.Identity?.Name;
        viewModel.Activity.Billable = true;
        return View(viewModel);
    }
    [HttpPost]
	[ValidateAntiForgeryToken]
    public async Task<IActionResult> Add(Add_ViewModel viewModel)
    {
        if (viewModel.Activity == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Activity.");
        viewModel.Activity.Duration = ParseDuration(viewModel.Duration);
        AppMessage msg = new AppMessage();
        if (viewModel.Activity.Duration.HasValue)
        {
            if (viewModel.Activity.Duration?.TotalMinutes < 1)
            {
                msg.AppendMessage("The activity duration is too short (0 minutes?). Please check and retry!");
            }
            if (viewModel.Activity.Duration?.TotalMinutes > 1440)
            {
                msg.AppendMessage("The activity duration exceeds 1440 minutes (24 hours), a full day, which is not allowed. Please split this activity in multiple entries if needed!");
            }
        }
        else
        {
            msg.AppendMessage("The activity duration is missing or has an invalid format. Please check the activity format!");
        }
        if (msg.Count > 0)
        {
            ViewBag.AppMessage = msg.ToHtmlError();
            if (viewModel.Activity.ProjectID.HasValue)
            {
                ProjectModel? project = await Repo.Projects.RetrieveAsync(viewModel.Activity.ProjectID.Value);
                if (project != null)
                {
                    if (project.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Project.ID is null.");
                    viewModel.Activity.ProjectTitle = project.Title;
                    viewModel.Activity.ProjectID = project.ID.Value;
                    viewModel.Activity.OrganizationID = project.OrganizationID;
                    viewModel.Activity.PersonID = project.PersonID;
                    viewModel.Activity.CustomerName = project.CustomerName;
                    viewModel.Activity.EndCustomer = project.EndCustomer;
                }
            }
            return View(viewModel);
        }
        await Repo.Activities.SaveAsync(viewModel.Activity);
        if (viewModel.Activity.ProjectID.HasValue) return RedirectToAction(nameof(this.ByProject), new { ID = viewModel.Activity.ProjectID });
        return RedirectToAction(nameof(this.Recent));
    }

    [HttpGet]
    public async Task<ViewResult> Edit(long ID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.Activity = await Repo.Activities.RetrieveAsync(ID);
		if (viewModel.Activity == null)
		{
			AppMessage msg = new AppMessage();
			msg.Add("The activity record you're trying to edit could not be found in the database.");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View("Message");
		}
        if (viewModel.Activity.Duration.HasValue)
            viewModel.Duration = viewModel.Activity.Duration.Value.ToString("hh':'mm");
        if (viewModel.Activity.ProjectID.HasValue)
        {
            ProjectModel? project = await Repo.Projects.RetrieveAsync(viewModel.Activity.ProjectID.Value);
            if (project != null) viewModel.Activity.ProjectTitle = project.Title;
        }
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(Edit_ViewModel viewModel)
    {
        if (viewModel.Activity == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Activity.");
        viewModel.Activity.Duration = ParseDuration(viewModel.Duration);
        AppMessage msg = new AppMessage();
        if (viewModel.Activity.Duration.HasValue)
        {
            if (viewModel.Activity.Duration?.TotalMinutes < 1)
            {
                msg.AppendMessage("The activity duration is too short (0 minutes?). Please check and retry!");
            }
            if (viewModel.Activity.Duration?.TotalMinutes > 1440)
            {
                msg.AppendMessage("The activity duration exceeds 1440 minutes (24 hours), a full day, which is not allowed. Please split this activity in multiple entries if needed!");
            }
        }
        else
        {
            msg.AppendMessage("The activity duration is missing or has an invalid format. Please check the activity format!");
        }
        if (msg.Count > 0)
        {
            ViewBag.AppMessage = msg.ToHtmlError();
            if (viewModel.Activity.ProjectID.HasValue)
            {
                ProjectModel? project = await Repo.Projects.RetrieveAsync(viewModel.Activity.ProjectID.Value);
                if (project != null)
                {
                    if (project.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Project.ID is null.");
                    viewModel.Activity.ProjectTitle = project.Title;
                    viewModel.Activity.ProjectID = project.ID.Value;
                    viewModel.Activity.OrganizationID = project.OrganizationID;
                    viewModel.Activity.PersonID = project.PersonID;
                    viewModel.Activity.CustomerName = project.CustomerName;
                    viewModel.Activity.EndCustomer = project.EndCustomer;
                }
            }
            return View(viewModel);
        }
        await Repo.Activities.SaveAsync(viewModel.Activity);
        if (viewModel.Activity.ProjectID.HasValue) return RedirectToAction(nameof(this.ByProject), new { ID = viewModel.Activity.ProjectID });
        return RedirectToAction(nameof(this.Recent));
    }

    public async Task<PartialViewResult> _PickProjects(string NamePattern, bool ExactMatch = false)
    {
        List<ProjectModel> viewModel = await Repo.Projects.ListAsync(NamePattern, ExactMatch);
        return PartialView(viewModel);
    }

	/// <summary>
	///		<para>Gets an advanced search page with an AJAX-updatable search result panel given by _Search() action</para>
	/// </summary>
	/// <returns></returns>
    public async Task<ViewResult> Search()
	{
		Search_ViewModel viewModel = new Search_ViewModel();
        AppMessage msg = new AppMessage();
		if (Request.QueryString.HasValue)
		{
			await TryUpdateModelAsync<Search_ViewModel>(viewModel);
		}
		return View(viewModel);
	}
	public async Task<PartialViewResult> _Search(Search_Params parameters)
	{
        ActivityModel.LookupFilter filter;
        List<ActivityModel> viewModel;
		if (!parameters.FilterIsSet)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage(string.Format("Since no search criteria is specified, only the project activities started in the last 30 day are displayed."));
			ViewBag.AppMessage = msg.ToHtmlWarning();
            filter = new ActivityModel.LookupFilter()
                {
                    TimeframeStart = DateTime.Now.AddDays(-30),
                    TimeframeEnd = DateTime.Now.AddMonths(1)
                };
            viewModel = await BeezMart.DAL.TimeSheet.Activities.LookupAsync(filter);
            return PartialView(viewModel);
		}
        filter = new ActivityModel.LookupFilter()
			{
                TitlePattern = parameters.Title,
                TitleExactMatch = parameters.Exact,
                CustomerNamePattern = parameters.Customer,
                ActivityTypeID = parameters.Type,
				Status = parameters.Status,
				ProjectID = parameters.PrjID,
                ProjectTitlePattern = parameters.Prj,
				RequestorNamePattern = parameters.Requestor,
                RequestingDepartment = parameters.Department,
                CompletedBy = parameters.By,
				TimeframeStart = parameters.After,
				TimeframeEnd = parameters.Before
			};
		viewModel = await BeezMart.DAL.TimeSheet.Activities.LookupAsync(filter);
		return PartialView(viewModel);
	}
    public async Task<IActionResult> _Search_Export(Search_Params parameters)
    {
        ActivityModel.LookupFilter filter;
        List<ActivityModel> records;
        if (parameters.FilterIsSet)
        {
            filter = new ActivityModel.LookupFilter()
                {
                    TitlePattern = parameters.Title,
                    TitleExactMatch = parameters.Exact,
                    CustomerNamePattern = parameters.Customer,
                    ActivityTypeID = parameters.Type,
                    Status = parameters.Status,
                    ProjectID = parameters.PrjID,
                    ProjectTitlePattern = parameters.Prj,
                    RequestorNamePattern = parameters.Requestor,
                    RequestingDepartment = parameters.Department,
                    CompletedBy = parameters.By,
                    TimeframeStart = parameters.After,
                    TimeframeEnd = parameters.Before
                };
        }
		else
		{
            filter = new ActivityModel.LookupFilter()
                {
                    TimeframeStart = DateTime.Now.AddDays(-30),
                    TimeframeEnd = DateTime.Now.AddMonths(1)
                };
                
		}
        records = await BeezMart.DAL.TimeSheet.Activities.LookupAsync(filter);
        string xlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        string fileName = "BeezMart-TimeSheet-Project-Activities.xlsx";
        using (ExcelPackage excelPackage = new ExcelPackage())
        {
            ExcelWorksheet excelWorksheet, wks;
            ExcelRange topRow;
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Activities");
            #region populate worksheet with records
            wks = excelWorksheet;
            int col = 1;
            wks.SetValue(1, col, "Activity_ID"); col++;			            // Column A
            wks.SetValue(1, col, "Project_ID"); col++;			            // Column B
            wks.SetValue(1, col, "Project_Title"); col++;                   // Column C
            wks.SetValue(1, col, "Organization_ID"); col++;			        // Column D
            wks.SetValue(1, col, "Person_ID"); col++;				        // Column E
            wks.SetValue(1, col, "Customer_Name"); col++;                   // Column F
            wks.SetValue(1, col, "End_Customer"); col++;                    // Column G
            wks.SetValue(1, col, "Activity_Status_ID"); col++;              // Column H
            wks.SetValue(1, col, "Activity_Status_Name"); col++;            // Column I
            wks.SetValue(1, col, "Activity_Type_ID"); col++;                // Column J
            wks.SetValue(1, col, "Activity_Type_Name"); col++;              // Column K
            wks.SetValue(1, col, "Activity_Type_Category"); col++;          // Column L
            wks.SetValue(1, col, "Title"); col++;                           // Column M
            wks.SetValue(1, col, "Scheduled_Start"); col++;                 // Column N
            wks.SetValue(1, col, "Scheduled_End"); col++;                   // Column O
            wks.SetValue(1, col, "Assigned_On"); col++;                     // Column P
            wks.SetValue(1, col, "Assigned_To"); col++;                     // Column Q
            wks.SetValue(1, col, "Deadline_On"); col++;                     // Column R
            wks.SetValue(1, col, "Started_On"); col++;                      // Column S
            wks.SetValue(1, col, "Ended_On"); col++;                        // Column T
            wks.SetValue(1, col, "Reminder_On"); col++;                     // Column U
            wks.SetValue(1, col, "Requested_By"); col++;                    // Column V
            wks.SetValue(1, col, "Requesting_Department"); col++;           // Column W
            wks.SetValue(1, col, "Completed_By"); col++;                    // Column X
            wks.SetValue(1, col, "Logged_By"); col++;                       // Column Y
            wks.SetValue(1, col, "Billable"); col++;                        // Column Z
            wks.SetValue(1, col, "Invoice_IDs"); col++;                     // Column AA
            wks.SetValue(1, col, "Duration_In_Minutes"); col++;             // Column AB
            wks.SetValue(1, col, "Updated_On"); col++;                      // Column AC
            wks.SetValue(1, col, "Updated_By"); col++;                      // Column AD
            topRow = wks.Cells["A1:AD1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            int row = 1;
            foreach (ActivityModel item in records)
            {
                row++;
                col = 1;
                wks.SetValue(row, col, item.ID); col++;
                wks.SetValue(row, col, item.ProjectID); col++;
                wks.SetValue(row, col, item.ProjectTitle); col++;
                wks.SetValue(row, col, item.OrganizationID); col++;
                wks.SetValue(row, col, item.PersonID); col++;
                wks.SetValue(row, col, item.CustomerName); col++;
                wks.SetValue(row, col, item.EndCustomer); col++;
                wks.SetValue(row, col, (byte)item.Status); col++;
                wks.SetValue(row, col, item.Status.ToString()); col++;
                wks.SetValue(row, col, item.ActivityTypeID); col++;
                wks.SetValue(row, col, item.ActivityTypeName); col++;
                wks.SetValue(row, col, item.ActivityCategory); col++;
                wks.SetValue(row, col, item.Title); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ScheduledStart)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ScheduledEnd)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.AssignedOn)); col++;
                wks.SetValue(row, col, item.AssignedTo); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.DeadlineOn)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.StartedOn)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.EndedOn)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ReminderOn)); col++;
                wks.SetValue(row, col, item.RequestedBy); col++;
                wks.SetValue(row, col, item.RequestingDepartment); col++;
                wks.SetValue(row, col, item.CompletedBy); col++;
                wks.SetValue(row, col, item.LoggedBy); col++;
                wks.SetValue(row, col, item.Billable); col++;
                wks.SetValue(row, col, item.InvoiceIDs); col++;
                wks.SetValue(row, col, item.Duration?.TotalMinutes); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.UpdatedOn)); col++;
                wks.SetValue(row, col, item.UpdatedBy); col++;
            }
            wks.DefaultColWidth = 20;
            for (col = 1; col < 29; col++) wks.Column(col).AutoFit();
            #endregion
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Filter");
            #region populate filtering criteria
            wks = excelWorksheet;
            row = 0;
            row++; wks.SetValue(row, 1, "Selection filter");
            wks.SetValue(row, 2, parameters.FilterIsSet ? "SEE HOW RECORDS WERE SELECTED" : "NO FILTERING CRITERIA WAS SPECIFIED");
            row++; wks.SetValue(row, 1, "Activity title pattern");
            wks.SetValue(row, 2, filter.TitlePattern);
            row++; wks.SetValue(row, 1, "Title exact match");
            wks.SetValue(row, 2, filter.TitleExactMatch.ToString());
            row++; wks.SetValue(row, 1, "Customer name like");
            wks.SetValue(row, 2, filter.CustomerNamePattern);
            row++; wks.SetValue(row, 1, "Activity type");
            wks.SetValue(row, 2, filter.ActivityTypeID);
            row++; wks.SetValue(row, 1, "Activity status");
            wks.SetValue(row, 2, filter.Status);
            row++; wks.SetValue(row, 1, "Project ID");
            wks.SetValue(row, 2, filter.ProjectID);
            row++; wks.SetValue(row, 1, "Project title like");
            wks.SetValue(row, 2, filter.ProjectTitlePattern);
            row++; wks.SetValue(row, 1, "Requestor name like");
            wks.SetValue(row, 2, filter.RequestorNamePattern);
            row++; wks.SetValue(row, 1, "Requesting department like");
            wks.SetValue(row, 2, filter.RequestingDepartment);
            row++; wks.SetValue(row, 1, "Completed by");
            wks.SetValue(row, 2, filter.CompletedBy);
            row++; wks.SetValue(row, 1, "Scheduled or started after");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.TimeframeStart));
            row++; wks.SetValue(row, 1, "Scheduled or ended before");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.TimeframeEnd));
            topRow = wks.Cells["A1:B1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            wks.Column(1).AutoFit();
            wks.Column(2).AutoFit();
            #endregion
            return File(excelPackage.GetAsByteArray(), xlsxContentType, fileName);
        }
    }

    protected TimeSpan? ParseDuration(string? durationString)
    {
        if (string.IsNullOrWhiteSpace(durationString)) return null;
        if ((durationString.Contains('.') || durationString.Contains(',')) && (durationString.Contains('h') || durationString.Contains('H')))
        {
            durationString = durationString.Replace(',', '.');
            string tmp = durationString.Replace("h", string.Empty, StringComparison.InvariantCultureIgnoreCase);
            decimal numberOfHours;
            if (decimal.TryParse(tmp, out numberOfHours))
            {
                return TimeSpan.FromHours((double)numberOfHours);
            }
        }
        int numberOfMinutes;
        if (int.TryParse(durationString, out numberOfMinutes))
        {
            return TimeSpan.FromMinutes(numberOfMinutes);
        }
        TimeSpan timeSpan;
        string[] inputFormats = { @"%h\:%m", @"%h\h%m", @"%h\H%m", @"%h\ %m", @"%h\h", @"%h\H" };
        if (TimeSpan.TryParseExact(durationString, inputFormats, System.Globalization.CultureInfo.CurrentCulture, out timeSpan))
        {
            return timeSpan;
        }
        return null;
    }
}