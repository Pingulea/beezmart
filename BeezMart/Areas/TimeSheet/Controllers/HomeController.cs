﻿using BeezMart.Areas.TimeSheet.ViewModels.Home;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repo = BeezMart.DAL.TimeSheet;

namespace BeezMart.Areas.TimeSheet.Controllers;

[Area("TimeSheet")]
[Authorize]
public class HomeController : Controller
{
    public async Task<ViewResult> Index()
    {
        Index_ViewModel viewModel = new Index_ViewModel { };
        viewModel.Projects = await Repo.Projects.ListLatestAsync(100);
        return View(viewModel);
    }
}

