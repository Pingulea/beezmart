﻿using BeezMart.Areas.TimeSheet.ViewModels.Projects;
using BeezMart.Caching.TimeSheet;
using BeezMart.Entities.TimeSheet;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Drawing;
using Repo = BeezMart.DAL.TimeSheet;

namespace BeezMart.Areas.TimeSheet.Controllers;

[Area("TimeSheet")]
[Authorize]
public class ProjectsController : Controller
{
    public async Task<IActionResult> Index()
    {
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.Projects = await Repo.Projects.ListAsync(DateTime.Now.AddMonths(-3), DateTime.Now.AddMonths(3));
        List<ProjectModel.CategoryModel> categories = await ProjectCategories.GetSubCategoriesAsync();
        foreach (ProjectModel.CategoryModel category in categories)
            if (category.ID.HasValue && !category.IsDiscontinued)
                viewModel.CategoryNames.Add(category.ID.Value, $"{category.Name} :: {category.SubCategory}");
        return View(viewModel);
    }
    public async Task<ViewResult> View(int ID)
    {
        View_ViewModel viewModel = new View_ViewModel { };
        viewModel.Project = await Repo.Projects.RetrieveAsync(ID);
        if (viewModel.Project == null)
        {
            AppMessage msg = new AppMessage(AppMessageType.Error);
            msg.Append("The project record you are looking for could not be found");
            return View("Message", msg);
        }
        if (viewModel.Project.CategoryID.HasValue)
        {
            ProjectModel.CategoryModel? projectCategory = await Repo.Projects.Categories.RetrieveAsync(viewModel.Project.CategoryID.Value);
            if (projectCategory != null)
                viewModel.ProjectCategoryName = $"{projectCategory.Name} :: {projectCategory.SubCategory}";
        }
        return View(viewModel);
    }
    [HttpGet]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForProjectManagement)]
    public ViewResult Add()
    {
        Add_ViewModel viewModel = new Add_ViewModel();
		viewModel.Project = new ProjectModel();
        return View(viewModel);
    }
    [HttpPost]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForProjectManagement)]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Add(Add_ViewModel viewModel)
    {
        if (viewModel.Project == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Project.");
        if (await Repo.Projects.SaveAsync(viewModel.Project)) return RedirectToAction(nameof(this.Index));
        return View(viewModel);
    }
    [HttpGet]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForProjectManagement)]
    public async Task<ViewResult> Edit(int ID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.Project = await Repo.Projects.RetrieveAsync(ID);
		if (viewModel.Project == null)
		{
			AppMessage msg = new AppMessage();
			msg.Add("The project record you're trying to edit could not be found in the database.");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View("Message");
		}
        return View(viewModel);
    }
    [HttpPost]
	[ValidateAntiForgeryToken]
    [Authorize(Roles = BeezMart.Security.UserRoles.UserForProjectManagement)]
    public async Task<IActionResult> Edit(Edit_ViewModel viewModel)
    {
        if (viewModel.Project == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Project.");
        await Repo.Projects.SaveAsync(viewModel.Project);
        return RedirectToAction(nameof(this.Index));
    }

	/// <summary>
	///		<para>Gets an advanced search page with an AJAX-updatable search result panel given by _Search() action</para>
	/// </summary>
	/// <returns></returns>
    public async Task<ViewResult> Search()
	{
		Search_ViewModel viewModel = new Search_ViewModel();
		if (Request.QueryString.HasValue)
		{
			await TryUpdateModelAsync<Search_ViewModel>(viewModel);
		}
		return View(viewModel);
	}
	public async Task<PartialViewResult> _Search(Search_Params parameters)
	{
        ProjectModel.LookupFilter filter;
        List<ProjectModel> viewModel;
		if (!parameters.FilterIsSet)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage(string.Format("Since no search criteria is specified, only the projects started in the last 12 month and ending in maximum 10 years will be displayed."));
			ViewBag.AppMessage = msg.ToHtmlWarning();
            filter = new ProjectModel.LookupFilter()
                {
                    TimeframeStart = DateTime.Now.AddMonths(-12),
                    TimeframeEnd = DateTime.Now.AddYears(10)
                };
            viewModel = await BeezMart.DAL.TimeSheet.Projects.LookupAsync(filter);
            return PartialView(viewModel);
		}
        List<int>? ProjectIDs = null;
		#region split IDs into lists
		if (!string.IsNullOrEmpty(parameters.IDs))
		{
			char[] separators = { ';', ',', '|', ' ' };
			int projectID;
			string[] separateIDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
			foreach (string projectIDstring in separateIDs)
			{
				if (int.TryParse(projectIDstring, out projectID))
				{
					if (ProjectIDs == null) ProjectIDs = new List<int>();
					ProjectIDs.Add(projectID);
				}
			}
		}
		#endregion
		filter = new ProjectModel.LookupFilter()
			{
                TitlePattern = parameters.Title,
                TitleExactMatch = parameters.Exact,
                CategoryID = parameters.Category,
				Status = parameters.Status,
				ProjectIDs = ProjectIDs,
				CustomerNamePattern = parameters.Customer,
                OrganizationID = parameters.OrganizationID,
                PersonID = parameters.PersonID,
				TimeframeStart = parameters.After,
				TimeframeEnd = parameters.Before
			};
		viewModel = await BeezMart.DAL.TimeSheet.Projects.LookupAsync(filter);
		return PartialView(viewModel);
	}
    public async Task<IActionResult> _Search_Export(Search_Params parameters)
    {
        ProjectModel.LookupFilter filter;
        List<ProjectModel> records;
        if (parameters.FilterIsSet)
        {
            List<int>? ProjectIDs = null;
            #region split IDs into lists
            if (!string.IsNullOrEmpty(parameters.IDs))
            {
                char[] separators = { ';', ',', '|', ' ' };
                int projectID;
                string[] separateIDs = parameters.IDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                foreach (string projectIDstring in separateIDs)
                {
                    if (int.TryParse(projectIDstring, out projectID))
                    {
                        if (ProjectIDs == null) ProjectIDs = new List<int>();
                        ProjectIDs.Add(projectID);
                    }
                }
            }
            #endregion
            filter = new ProjectModel.LookupFilter()
                {
                    TitlePattern = parameters.Title,
                    TitleExactMatch = parameters.Exact,
                    CategoryID = parameters.Category,
                    Status = parameters.Status,
                    ProjectIDs = ProjectIDs,
                    CustomerNamePattern = parameters.Customer,
                    OrganizationID = parameters.OrganizationID,
                    PersonID = parameters.PersonID,
                    TimeframeStart = parameters.After,
                    TimeframeEnd = parameters.Before
                };
        }
        else
		{
            filter = new ProjectModel.LookupFilter()
                {
                    TimeframeStart = DateTime.Now.AddMonths(-12),
                    TimeframeEnd = DateTime.Now.AddYears(10)
                };
		}
		records = await BeezMart.DAL.TimeSheet.Projects.LookupAsync(filter);
        string xlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        string fileName = "BeezMart-TimeSheet-Projects.xlsx";
        using (ExcelPackage excelPackage = new ExcelPackage())
        {
            ExcelWorksheet excelWorksheet, wks;
            ExcelRange topRow;
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Projects");
            #region populate worksheet with records
            wks = excelWorksheet;
            int col = 1;
            wks.SetValue(1, col, "Project_ID"); col++;			        // Column A
            wks.SetValue(1, col, "Title"); col++;			            // Column B
            wks.SetValue(1, col, "Project_Manager"); col++;			    // Column C
            wks.SetValue(1, col, "Contact_Details"); col++;			    // Column D
            wks.SetValue(1, col, "Organization_ID"); col++;			    // Column E
            wks.SetValue(1, col, "Person_ID"); col++;			        // Column F
            wks.SetValue(1, col, "Customer_Name"); col++;			    // Column G
            wks.SetValue(1, col, "End_Customer"); col++;			    // Column H
            wks.SetValue(1, col, "Category_ID"); col++;			        // Column I
            wks.SetValue(1, col, "Category_Name"); col++;			    // Column J
            wks.SetValue(1, col, "Sub_Category"); col++;			    // Column K
            wks.SetValue(1, col, "Project_Status_ID"); col++;			// Column L
            wks.SetValue(1, col, "Project_Status_Name"); col++;			// Column M
            wks.SetValue(1, col, "Scheduled_Start"); col++;			    // Column N
            wks.SetValue(1, col, "Scheduled_End"); col++;			    // Column O
            wks.SetValue(1, col, "Started_On"); col++;			        // Column P
            wks.SetValue(1, col, "Ended_On"); col++;			        // Column Q
            wks.SetValue(1, col, "Updated_On"); col++;			        // Column R
            wks.SetValue(1, col, "Updated_By"); col++;			        // Column S
            topRow = wks.Cells["A1:S1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            int row = 1;
            foreach (ProjectModel item in records)
            {
                row++;
                col = 1;
                wks.SetValue(row, col, item.ID); col++;
                wks.SetValue(row, col, item.Title); col++;
                wks.SetValue(row, col, item.ProjectManager); col++;
                wks.SetValue(row, col, item.ContactDetails); col++;
                wks.SetValue(row, col, item.OrganizationID); col++;
                wks.SetValue(row, col, item.PersonID); col++;
                wks.SetValue(row, col, item.CustomerName); col++;
                wks.SetValue(row, col, item.EndCustomer); col++;
                wks.SetValue(row, col, item.CategoryID); col++;
                wks.SetValue(row, col, item.CategoryName); col++;
                wks.SetValue(row, col, item.SubCategory); col++;
                wks.SetValue(row, col, (byte)item.Status); col++;
                wks.SetValue(row, col, item.Status.ToString()); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ScheduledStart)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.ScheduledEnd)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.StartedOn)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.EndedOn)); col++;
                wks.SetValue(row, col, string.Format("{0:yyyy-MM-dd}", item.UpdatedOn)); col++;
                wks.SetValue(row, col, item.UpdatedBy); col++;
            }
            wks.DefaultColWidth = 20;
            for (col = 1; col < 19; col++) wks.Column(col).AutoFit();
            #endregion
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Filter");
            #region populate filtering criteria
            wks = excelWorksheet;
            row = 0;
            row++; wks.SetValue(row, 1, "Selection filter");
            wks.SetValue(row, 2, parameters.FilterIsSet ? "SEE HOW RECORDS WERE SELECTED" : "NO FILTERING CRITERIA WAS SPECIFIED");
            row++; wks.SetValue(row, 1, "Project title pattern");
            wks.SetValue(row, 2, filter.TitlePattern);
            row++; wks.SetValue(row, 1, "Title exact match");
            wks.SetValue(row, 2, filter.TitleExactMatch.ToString());
            row++; wks.SetValue(row, 1, "Category");
            wks.SetValue(row, 2, filter.CategoryID);
            row++; wks.SetValue(row, 1, "Status");
            wks.SetValue(row, 2, filter.Status);
            row++; wks.SetValue(row, 1, "Project IDs");
            wks.SetValue(row, 2, filter.ProjectIDs);
            row++; wks.SetValue(row, 1, "Customer");
            wks.SetValue(row, 2, filter.CustomerNamePattern);
            row++; wks.SetValue(row, 1, "Organization ID");
            wks.SetValue(row, 2, filter.OrganizationID);
            row++; wks.SetValue(row, 1, "Person ID");
            wks.SetValue(row, 2, filter.PersonID);
            row++; wks.SetValue(row, 1, "Scheduled or started after");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.TimeframeStart));
            row++; wks.SetValue(row, 1, "Scheduled or ended before");
            wks.SetValue(row, 2, string.Format("{0:yyyy-MM-dd}", filter.TimeframeEnd));
            topRow = wks.Cells["A1:B1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            wks.Column(1).AutoFit();
            wks.Column(2).AutoFit();
            #endregion
            return File(excelPackage.GetAsByteArray(), xlsxContentType, fileName);
        }
    }
}