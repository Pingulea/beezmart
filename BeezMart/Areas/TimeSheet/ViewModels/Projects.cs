﻿using BeezMart.Entities.TimeSheet;

namespace BeezMart.Areas.TimeSheet.ViewModels.Projects;

public class View_ViewModel
{
    public ProjectModel? Project { get; set; }
    public string? ProjectCategoryName { get; set; }
}
public class Index_ViewModel
{
    public List<ProjectModel>? Projects { get; set; }
    public Dictionary<short, string> CategoryNames { get; set; } = new Dictionary<short, string> { };
}
public class Add_ViewModel
{
    public ProjectModel? Project { get; set; }
    public string? ProjectTitle { get; set; }
}
public class Edit_ViewModel
{
    public ProjectModel? Project { get; set; }
}
public class Search_ViewModel
{
    public string? Title { get; set; }
    public bool Exact { get; set; }
    public short? Category { get; set; }
	public ProjectStatus? Status { get; set; }			// Status
	public string? IDs { get; set; }
    public string? Customer { get; set; }
    public string? ProjectManager { get; set; }
    public string? ContactDetails { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
	public DateTime? After { get; set; } = null;
	public DateTime? Before { get; set; } = null;
	public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Title)) return true;
			if (this.Category.HasValue) return true;
			if (this.Status.HasValue) return true;
			if (!string.IsNullOrEmpty(IDs)) return true;
			if (!string.IsNullOrEmpty(this.Customer)) return true;
			if (!string.IsNullOrEmpty(this.ProjectManager)) return true;
			if (!string.IsNullOrEmpty(this.ContactDetails)) return true;
            if (this.OrganizationID.HasValue) return true;
            if (this.PersonID.HasValue) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			return false;
		}
	}
}
public class Search_Params
{
	public string? Title { get; set; }
    public bool Exact { get; set; }
    public short? Category { get; set; }
	public ProjectStatus? Status { get; set; }			// Status
	public string? IDs { get; set; }
    public string? Customer { get; set; }
    public string? ProjectManager { get; set; }
    public string? ContactDetails { get; set; }
    public int? OrganizationID { get; set; }
    public int? PersonID { get; set; }
	public DateTime? After { get; set; } = null;
	public DateTime? Before { get; set; } = null;
	public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Title)) return true;
			if (this.Category.HasValue) return true;
			if (this.Status.HasValue) return true;
			if (!string.IsNullOrEmpty(IDs)) return true;
			if (!string.IsNullOrEmpty(this.Customer)) return true;
			if (!string.IsNullOrEmpty(this.ProjectManager)) return true;
			if (!string.IsNullOrEmpty(this.ContactDetails)) return true;
            if (this.OrganizationID.HasValue) return true;
            if (this.PersonID.HasValue) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			return false;
		}
	}
}
