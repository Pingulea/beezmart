﻿using BeezMart.Entities.TimeSheet;

namespace BeezMart.Areas.TimeSheet.ViewModels.Home;

public class Index_ViewModel
{
    public List<ProjectModel>? Projects { get; set; }
}
