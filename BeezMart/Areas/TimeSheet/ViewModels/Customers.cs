﻿using BeezMart.Entities.CRM;
using BeezMart.Entities.TimeSheet;

namespace BeezMart.Areas.TimeSheet.ViewModels.Customers;

public class Index_ViewModel
{
    public List<OrganizationModel>? Organizations { get; set; }
}
public class Add_ViewModel
{
    public List<string>? Categories { get; set; }
    public List<ProjectModel.CategoryModel>? SubCategories { get; set; }
    public ProjectModel? Project { get; set; }
}
public class Edit_ViewModel
{
    public List<string>? Categories { get; set; }
    public List<ProjectModel.CategoryModel>? SubCategories { get; set; }
    public ProjectModel? Project { get; set; }
}
