﻿using BeezMart.Entities.TimeSheet;

namespace BeezMart.Areas.TimeSheet.ViewModels.Activities;

public class Index_ViewModel
{
    public List<ActivityModel>? Activities { get; set; }
    public TimeSpan TotalDuration { get; set; }
}
public class Recent_ViewModel
{
    public List<ActivityModel>? Activities { get; set; }
    public TimeSpan TotalDuration { get; set; }
}
public class MyRecent_ViewModel
{
    public List<ActivityModel>? Activities { get; set; }
    public TimeSpan TotalDuration { get; set; }
}
public class CurrentProjects_ViewModel
{
    public List<ProjectModel>? Projects { get; set; }
}
public class ByProject_ViewModel
{
    public ProjectModel? Project { get; set; }
    public List<ActivityModel>? Activities { get; set; }
    public TimeSpan TotalDuration { get; set; }
}
public class View_ViewModel
{
    public ActivityModel? Activity { get; set; }
    public string? Duration { get; set; }
}
public class Add_ViewModel
{
    public ActivityModel? Activity { get; set; }
    public string? Duration { get; set; }
}
public class Edit_ViewModel
{
    public ActivityModel? Activity { get; set; }
    public string? Duration { get; set; }
}
public class Search_ViewModel
{
    public string? Title { get; set; }
    public string? Customer { get; set; }
	public bool Exact { get; set; } = false;
	public short? Type { get; set; } = null;
	public ActivityStatus? Status { get; set; } = null;
	public int? PrjID { get; set; }         // Project ID
	public string? PrjIDs { get; set; }      // List of Project IDs
	public string? Prj { get; set; }         // Project title
	public string? Requestor { get; set; }
	public string? Department { get; set; }
	public string? By { get; set; }
	public DateTime? After = null;
	public DateTime? Before = null;

    public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Title)) return true;
			if (!string.IsNullOrEmpty(this.Customer)) return true;
			if (this.Type.HasValue) return true;
			if (this.Status.HasValue) return true;
            if (this.PrjID.HasValue) return true;
			if (!string.IsNullOrEmpty(this.PrjIDs)) return true;
			if (!string.IsNullOrEmpty(this.Prj)) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			if (!string.IsNullOrEmpty(this.Requestor)) return true;
			if (!string.IsNullOrEmpty(this.Department)) return true;
			if (!string.IsNullOrEmpty(this.By)) return true;
			return false;
		}
	}
}
public class Search_Params
{
    public string? Title { get; set; }
    public string? Customer { get; set; }
	public bool Exact { get; set; } = false;
	public short? Type { get; set; } = null;
	public ActivityStatus? Status { get; set; } = null;
	public int? PrjID { get; set; }         // Project ID
	public string? PrjIDs { get; set; }      // List of Project IDs
	public string? Prj { get; set; }         // Project title
	public string? Requestor { get; set; }
	public string? Department { get; set; }
	public string? By { get; set; }
	public DateTime? After { get; set; } = null;
	public DateTime? Before { get; set; } = null;

    public bool FilterIsSet
	{
		get
		{
			if (!string.IsNullOrEmpty(this.Title)) return true;
			if (!string.IsNullOrEmpty(this.Customer)) return true;
			if (this.Type.HasValue) return true;
			if (this.Status.HasValue) return true;
            if (this.PrjID.HasValue) return true;
			if (!string.IsNullOrEmpty(this.PrjIDs)) return true;
			if (!string.IsNullOrEmpty(this.Prj)) return true;
			if (this.After.HasValue) return true;
			if (this.Before.HasValue) return true;
			if (!string.IsNullOrEmpty(this.Requestor)) return true;
			if (!string.IsNullOrEmpty(this.Department)) return true;
			if (!string.IsNullOrEmpty(this.By)) return true;
			return false;
		}
	}
}
