﻿using BeezMart.Security;

namespace BeezMart.Areas.Admin.ViewModels.Users;

public class Index_ViewModel
{
	public List<ApplicationUser>? UserAccounts { get; set; }
}
public class Add_ViewModel
{
	public ApplicationUser? UserAccount { get; set; }
	public string? Password { get; set; }
	public string? PasswordConfirm { get; set; }
	public string? SuggestedPassword { get; set; }
	public bool SendConfirmation { get; set; }
	public bool SendNotification { get; set; }
    public List<string>? Roles { get; set; }
}
public class Edit_ViewModel
{
	public ApplicationUser? UserAccount { get; set; }
	public bool SendConfirmation { get; set; }
	public bool SendNotification { get; set; }
}
public class Account_ViewModel
{
	public ApplicationUser? UserAccount { get; set; }
	public string? SuggestedPassword { get; set; }
	public bool SendConfirmation { get; set; }
	public bool SendNotification { get; set; }
}
public class Role_ViewModel
{
	public string? RoleName { get; set; }
	public List<ApplicationUser>? UserAccountsInRole { get; set; }
}
