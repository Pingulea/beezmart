﻿using BeezMart.Security;

namespace BeezMart.Areas.Admin.ViewModels.Setup;

public class AdminAccount_ViewModel
{
	public IList<ApplicationUser>? Administrators { get; set;  }
	public string? Password { get; set; }
	public string? PasswordConfirm { get; set; }
	public string? EmailAddress { get; set; }
	public string? SuggestedPassword { get; set; }
	public bool SendReminder { get; set; }
}
public class Install_ViewModel
{
	public List<DirectoryInfo>? TSQL_Script_Folders { get; set; }
	public List<FileInfo>? TSQL_Script_Files { get; set; }
}
