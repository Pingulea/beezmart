﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.Admin.ViewModels.CrmJobTypes;

public class Index_ViewModel
{
	public List<RelationOrgsPersModel.RelationTypeModel>? JobTypes { get; set; }
}
