﻿using BeezMart.Entities;
using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class CrmPersController : Controller
{
    public ViewResult Index()
    {
        return View();
    }

    #region Activity types
    public ViewResult ActivityTypes()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _ActivityTypes()
    {
        List<PersActivityModel.ActivityTypeModel> viewModel = await BeezMart.DAL.CRM.PersActivities.ActivityTypes.ListAllAsync();
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _ActivityTypes([FromBody] PersActivityModel.ActivityTypeModel activityType)
    {
        if (TryValidateModel(activityType))
        {
            bool success = await BeezMart.DAL.CRM.PersActivities.ActivityTypes.SaveAsync(activityType);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<PersActivityModel.ActivityTypeModel> viewModel = await BeezMart.DAL.CRM.PersActivities.ActivityTypes.ListAllAsync();
        return Json(viewModel);
    }
    #endregion

    #region Segments
    public ViewResult Segments()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Segments()
    {
        List<PersSegmentModel> viewModel = await BeezMart.DAL.CRM.PersSegments.ListWithCountsAsync();
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Segments([FromBody] PersSegmentModel segment)
    {
        if (TryValidateModel(segment))
        {
            bool success = await BeezMart.DAL.CRM.PersSegments.SaveAsync(segment);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<PersSegmentModel> viewModel = await BeezMart.DAL.CRM.PersSegments.ListWithCountsAsync();
        return Json(viewModel);
    }
    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Segments([FromRoute] byte Id)
    {
        bool success = await BeezMart.DAL.CRM.PersSegments.DeleteAsync(Id, updatedBy: this.User.Identity?.Name);
        if (!success)
        {
            StringBuilder stb = new StringBuilder();
            stb.AppendLine("<p>The segment could not be deleted. This happened most probably because the are still some persons in this segment.</p>");
            stb.Append("<a class='btn btn-sm btn-info' target='_blank' href='");
            stb.Append(Url.Action("Lookup", "Persons", new { Area = "CRM", Segments = Id }));
            stb.AppendLine("'>Which ones <b class='bi-chevron-right ms-2'></b></a>");
            return Problem(statusCode: 400, title: "Referential integrity", detail: stb.ToString());
        }
        List<PersSegmentModel> viewModel = await BeezMart.DAL.CRM.PersSegments.ListWithCountsAsync();
        return Json(viewModel);
    }
    #endregion

    #region Flags
    public ViewResult Flags()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Flags()
    {
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.Flag);
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Flags([FromBody] PersFieldModel field)
    {
        AppMessage msg = new AppMessage();
        if (TryValidateModel(field))
        {
            bool success = await BeezMart.DAL.CRM.PersFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.Flag);
        return Json(viewModel);
    }
    #endregion

    #region Integer attributes
    public ViewResult Integers()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Integers()
    {
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.Integer);
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Integers([FromBody] PersFieldModel field)
    {
        if (TryValidateModel(field))
        {
            bool success = await BeezMart.DAL.CRM.PersFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.Integer);
        return Json(viewModel);
    }
    #endregion

    #region String attributes
    public ViewResult Strings()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Strings()
    {
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.String);
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Strings([FromBody] PersFieldModel field)
    {
        if (TryValidateModel(field))
        {
            bool success = await BeezMart.DAL.CRM.PersFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.String);
        return Json(viewModel);
    }
    #endregion

    #region Hugetext attributes
    public ViewResult Hugetexts()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Hugetexts()
    {
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.HugeText);
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Hugetexts([FromBody] PersFieldModel field)
    {
        if (TryValidateModel(field))
        {
            bool success = await BeezMart.DAL.CRM.PersFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<PersFieldModel> viewModel = await BeezMart.DAL.CRM.PersFields.ListAllAsync(FieldType.HugeText);
        return Json(viewModel);
    }
    #endregion
}
