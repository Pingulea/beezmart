﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class HomeController : Controller
{
	public ViewResult Index()
	{
		return View();
	}
}