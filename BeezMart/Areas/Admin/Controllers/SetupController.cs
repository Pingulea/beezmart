﻿using BeezMart.Areas.Admin.ViewModels.Setup;
using BeezMart.DAL;
using BeezMart.Services;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using BeezMart.Security;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
public class SetupController : Controller
{

#if LOCAL_ACCOUNTS
	public async Task<ActionResult> Index()
	{
        if (MyApp.MissingHostingConfigurationSettings != null && MyApp.MissingHostingConfigurationSettings.Count > 0)
        {
            return RedirectToAction(nameof(MissingConfigSettings));
        }
        else
        {
            if (MyApp.DatabaseIsSetUp)
            {
                Claim claimForAdminRole = new Claim(type: ClaimTypes.Role, value: BeezMart.Security.UserRoles.ApplicationAdministrator, valueType: ClaimValueTypes.String, issuer: MyApp.IdentityIssuer);
                IList<ApplicationUser> administratorAccounts = await _usersManager.GetUsersForClaimAsync(claimForAdminRole);
                if (administratorAccounts.Count == 0) return RedirectToAction(actionName: nameof(SetupController.AdminAccount));
                else return RedirectToAction(actionName: nameof(SetupController.Install));
            }
            else
            {
                return View();
            }
        }
	}
#else
    public ActionResult Index()
    {
        if (MyApp.MissingHostingConfigurationSettings != null && MyApp.MissingHostingConfigurationSettings.Count > 0)
        {
            return RedirectToAction(nameof(MissingConfigSettings));
        }
        else
        {
            if (MyApp.DatabaseIsSetUp)
            {
                return RedirectToAction(actionName: nameof(SetupController.Settings));
            }
            else
            {
                return View();
            }
        }
    }
#endif

    [HttpGet]
    [AllowAnonymous]
    public ViewResult MissingConfigSettings()
    {
        return View();
    }
    [HttpGet]
    [AllowAnonymous]
    public ViewResult Install()
    {
        Install_ViewModel viewModel = new Install_ViewModel();
        if (MyApp.PhysicalDirSourceRoot == null)
            throw new NullReferenceException("MyApp.PhysicalDirSourceRoot is null; please check the application startup code.");
        DirectoryInfo directoryInfo_AppData = new DirectoryInfo(Path.Combine(MyApp.PhysicalDirSourceRoot, "App_Data"));
        viewModel.TSQL_Script_Folders = new List<DirectoryInfo>();
        viewModel.TSQL_Script_Files = new List<FileInfo>();
        this._logger.LogInformation($"Database Transact-SQL scripts for setting up the application database:");
        foreach (DirectoryInfo directoryInfo in directoryInfo_AppData.GetDirectories("Setup_*"))
        {
            this._logger.LogInformation(directoryInfo.FullName);
            viewModel.TSQL_Script_Folders.Add(directoryInfo);
            foreach (FileInfo fileInfo in directoryInfo.GetFiles("*.sql"))
            {
                this._logger.LogInformation($"   > {fileInfo.Name}");
                viewModel.TSQL_Script_Files.Add(fileInfo);
            }
        }
        return View(viewModel);
    }
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<string> Install_RunScript(string ScriptFileName)
    {
        string TransactSqlScript;
        if (MyApp.PhysicalDirSourceRoot == null)
            throw new NullReferenceException("MyApp.PhysicalDirSourceRoot is null; please check the application startup code.");
        string scriptFile = Path.Combine(MyApp.PhysicalDirSourceRoot, "App_Data", ScriptFileName);
        this._logger.LogInformation($"Executing T-SQL {scriptFile}");
        using (StreamReader streamReader = new StreamReader(new FileStream(scriptFile, FileMode.Open, FileAccess.Read)))
        {
            TransactSqlScript = streamReader.ReadToEnd();
        }
        string returnMessage = "OK";
        using (Database db = new Database())
        {
            Regex regex = new System.Text.RegularExpressions.Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string[] sqlStatements = regex.Split(TransactSqlScript);
            foreach (string sqlStatement in sqlStatements)
            {
                string sqlStatementTrimmed = sqlStatement.Trim();
                if (!string.IsNullOrWhiteSpace(sqlStatementTrimmed))
                {
                    try
                    {
                        await db.ExecuteTextCommandAsync(sqlStatementTrimmed);
                    }
                    catch (Exception ex)
                    {
                        returnMessage = ex.Message;
                    }
                }
            }
        }
        return returnMessage;
    }

    [HttpGet]
    [AllowAnonymous]
    public ViewResult AccessAccounts()
    {
        return View();
    }

#if LOCAL_ACCOUNTS
	[HttpGet]
	[AllowAnonymous]
	public async Task<ViewResult> AdminAccount()
	{
		AdminAccount_ViewModel viewModel = new AdminAccount_ViewModel();
		viewModel.SendReminder = false;
		Claim claimForAdminRole = new Claim(type: ClaimTypes.Role, value: BeezMart.Security.UserRoles.ApplicationAdministrator, valueType: ClaimValueTypes.String, issuer: MyApp.IdentityIssuer);
		viewModel.Administrators = await _usersManager.GetUsersForClaimAsync(claimForAdminRole);
		viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
		return View(viewModel);
	}
	[HttpPost]
	[AllowAnonymous]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> AdminAccount(AdminAccount_ViewModel viewModel)
	{
		AppMessage msg = new AppMessage();
        #region validate the input
		if (string.IsNullOrEmpty(viewModel.Password))
		{
			msg.AppendMessage("You must provide an administrator password.");
		}
		else
		{
			if (viewModel.Password != viewModel.PasswordConfirm)
			{
				msg.AppendMessage("Please check your password input. The two passwords don't match.");
			}
		}
		if (string.IsNullOrEmpty(viewModel.EmailAddress) || !EmailAddressValidator.IsValidEmailAddress(viewModel.EmailAddress))
		{
			msg.AppendMessage("The e-mail address is missing or invalid. You need to provide an e-mail address for password reset purposes.");
		}
		if (msg.Count > 0)
		{
			ViewBag.AppMessage = msg.ToHtmlError();
			viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
			return View(viewModel);
		}
        #endregion
		Claim claimForAdminRole = new Claim(type: ClaimTypes.Role, value: BeezMart.Security.UserRoles.ApplicationAdministrator, valueType: ClaimValueTypes.String, issuer: MyApp.IdentityIssuer);
		viewModel.Administrators = await _usersManager.GetUsersForClaimAsync(claimForAdminRole);
        if (string.IsNullOrWhiteSpace(viewModel.Password)) throw new NullReferenceException("ViewModel.Password is NULL or white-space!");
        if (viewModel.Administrators.Count == 0)
		{
			ApplicationUser applicationAddministrator = new ApplicationUser { UserName = "Administrator", Email = viewModel.EmailAddress };
            applicationAddministrator.UserGUID ??= Guid.NewGuid();
			applicationAddministrator.FirstName = "Master-Admin";
			applicationAddministrator.LastName = "Builtin-User";
			applicationAddministrator.EmailConfirmed = true;
			IdentityResult identityResult = await this._usersManager.CreateAsync(applicationAddministrator, viewModel.Password);
			if (identityResult.Succeeded)
			{
				await _usersManager.AddClaimAsync(applicationAddministrator, new Claim(ClaimTypes.NameIdentifier, "Administrator", ClaimValueTypes.String, MyApp.IdentityIssuer));
				await _usersManager.AddClaimAsync(applicationAddministrator, new Claim(ClaimTypes.GivenName, "Master-Admin", ClaimValueTypes.String, MyApp.IdentityIssuer));
				await _usersManager.AddClaimAsync(applicationAddministrator, new Claim(ClaimTypes.Name, "Builtin-User", ClaimValueTypes.String, MyApp.IdentityIssuer));
				await _usersManager.AddClaimAsync(applicationAddministrator, new Claim(ClaimTypes.Role, BeezMart.Security.UserRoles.ApplicationAdministrator, ClaimValueTypes.String, MyApp.IdentityIssuer));
				await this._signInManager.SignInAsync(applicationAddministrator, isPersistent: false);
				if (viewModel.SendReminder)
				{
					EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Setup/AdminAccount/Reminder.html");
					if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Setup/AdminAccount/Reminder.html'.");
					if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Setup/AdminAccount/Reminder.html'.");
					string AppRootURL = EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext);
					Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?> { };
					personalizationStrings.Add("{# Application Full URL #}", EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext));
					personalizationStrings.Add("{# User Name #}", "Administrator");
#pragma warning disable CS8604 // Possible null reference argument viewModel.Password.
                    personalizationStrings.Add("{# Password #}", HtmlEncoder.Default.Encode(viewModel.Password));
#pragma warning restore CS8604 // Possible null reference argument.
                    personalizationStrings.Add("{# eMail Address #}", viewModel.EmailAddress);
					personalizationStrings.Add("{# Account Profile URL #}", AppRootURL + Url.Action("Index", "Account"));
					personalizationStrings.Add("{# Forgotten Password URL #}", AppRootURL + Url.Action("ForgotPassword", "Account"));
					personalizationStrings.Add("{# Login URL #}", AppRootURL + Url.Action("Login", "Account"));
					emailLayout.Personalize(personalizationStrings);
#pragma warning disable CS8604 // Possible null reference argument viewModel.EmailAddress.
                    List<string>? potentialErrors = await this._emailSender.SendEmailAsync(viewModel.EmailAddress, emailLayout.Subject, emailLayout.Body);
#pragma warning restore CS8604 // Possible null reference argument.
                    if (potentialErrors != null && potentialErrors.Count > 0)
					{
						msg.AppendMessage("Errors occured while sending you the confirmation e-mail with the newly created account details:");
						msg.AppendRaw("<ul>");
						foreach (string error in potentialErrors)
						{
							msg.AppendRaw($"<li>{error}</li>");
						}
						msg.AppendRaw("</ul>");
					}
				}
				return RedirectToAction(actionName: nameof(SetupController.Settings));
			}
			else
			{
				msg.AppendMessage("The application failed to create the built-in ADMINISTRATOR account. Please consult error messages below and try to fix the input.");
				foreach (IdentityError error in identityResult.Errors)
				{
					msg.AppendMessage(error.Description);
				}
				ViewBag.AppMessage = msg.ToHtmlError();
				viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
				return View(viewModel);
			}
		}
		else
		{
			return RedirectToAction(actionName: nameof(SetupController.Settings));
		}
	}
#endif

    [HttpGet]
    [Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
    public ViewResult Settings()
    {
        return View();
    }
    [HttpPost]
    [Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Settings(IFormCollection fields)
    {
        Dictionary<string, string> settings = new Dictionary<string, string>();
        IFormCollection requestForm = await Request.ReadFormAsync();
        foreach (string key in requestForm.Keys)
        {
            if (key == "__RequestVerificationToken") continue;
            string? fieldValue = requestForm[key];
            if (!string.IsNullOrWhiteSpace(fieldValue)) { settings.Add(key, fieldValue); }
        }
        await MyApp.SaveConfigSettingsAsync(settings);
        await MyApp.LoadConfigSettingsAsync();
        MyApp.DatabaseIsSetUp = true;
        return RedirectToAction("Index", "Home", new { Area = "Admin" });
    }

    [HttpGet]
    [Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
    public ViewResult Confirm_Settings()
    {
        return View();
    }

    #region constructor, fields and helpers

#if LOCAL_ACCOUNTS
    private readonly UserManager<ApplicationUser> _usersManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IEmailSender _emailSender;
    private readonly ILogger _logger;

    public SetupController(UserManager<ApplicationUser> usersManager, SignInManager<ApplicationUser> signInManager, IEmailSender emailSender, ILoggerFactory loggerFactory)
    {
        this._usersManager = usersManager;
        this._signInManager = signInManager;
        this._emailSender = emailSender;
        this._logger = loggerFactory.CreateLogger("Setup");
    }
#else
	private readonly ILogger _logger;

    public SetupController(ILoggerFactory loggerFactory)
    {
        this._logger = loggerFactory.CreateLogger("Setup");
    }
#endif

    #endregion
}