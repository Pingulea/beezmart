using BeezMart.Entities.TimeSheet;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class TimeSheetController : Controller
{
    public ViewResult Index()
	{
		return View();
	}

    #region Activity types
    public ViewResult ActivityTypes()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _ActivityTypes()
    {
        List<ActivityModel.ActivityTypeModel> viewModel = await BeezMart.DAL.TimeSheet.Activities.ActivityTypes.ListAllAsync();
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _ActivityTypes([FromBody] ActivityModel.ActivityTypeModel activityType)
    {
        if (TryValidateModel(activityType))
        {
            bool success = await BeezMart.DAL.TimeSheet.Activities.ActivityTypes.SaveAsync(activityType);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<ActivityModel.ActivityTypeModel> viewModel = await BeezMart.DAL.TimeSheet.Activities.ActivityTypes.ListAllAsync();
        return Json(viewModel);
    }
    #endregion

    #region Project categories
    public ViewResult ProjectCategories()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _ProjectCategories()
	{
		List<ProjectModel.CategoryModel> viewModel = await BeezMart.DAL.TimeSheet.Projects.Categories.ListAllAsync();
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _ProjectCategories([FromBody] ProjectModel.CategoryModel projectCategory)
	{
		if (TryValidateModel(projectCategory))
		{
			bool success = await BeezMart.DAL.TimeSheet.Projects.Categories.SaveAsync(projectCategory);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
		else
		{
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
		List<ProjectModel.CategoryModel> viewModel = await BeezMart.DAL.TimeSheet.Projects.Categories.ListAllAsync();
		return Json(viewModel);
	}
    #endregion
}