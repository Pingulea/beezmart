﻿using BeezMart.Areas.Admin.ViewModels.CrmJobTypes;
using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class CrmJobTypesController : Controller
{
    public ViewResult Index()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Index()
    {
        List<RelationOrgsPersModel.RelationTypeModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.RelationTypes.ListAllAsync();
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Index([FromBody] RelationOrgsPersModel.RelationTypeModel jobType)
    {
        if (TryValidateModel(jobType))
        {
            bool success = await BeezMart.DAL.CRM.RelationsOrgsPers.RelationTypes.SaveAsync(jobType);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<RelationOrgsPersModel.RelationTypeModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.RelationTypes.ListAllAsync();
        return Json(viewModel);
    }
    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Index([FromRoute] short Id)
    {
        bool success = await BeezMart.DAL.CRM.RelationsOrgsPers.RelationTypes.DeleteAsync(Id, updatedBy: this.User.Identity?.Name);
        if (success)
        {
            List<RelationOrgsPersModel.RelationTypeModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.RelationTypes.ListAllAsync();
            return Json(viewModel);
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            stb.AppendLine("<p>The job type could not be deleted. This happened most probably because the are still some positions of this type.</p>");
            stb.AppendLine($"<p>Record ID = {Id}</p>");
            //stb.Append("<a class='btn btn-sm btn-info' target='_blank' href='");
            //stb.Append(Url.Action("Lookup", "Organizations", new { Area = "CRM", Segments = RecordID }));
            //stb.AppendLine("'>Which ones <b class='bi-chevron-right ms-2'></b></a>");
            return Problem(statusCode: 400, title: "Referential integrity", detail: stb.ToString());
        }
    }
}
