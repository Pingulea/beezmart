﻿using BeezMart.DAL.CRM;
using BeezMart.Entities;
using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Pqc.Crypto.Frodo;
using System.Linq;
using System.Text;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class CrmOrgsController : Controller
{
	public ViewResult Index()
	{
		return View();
	}

	#region Activity types
	public ViewResult ActivityTypes()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _ActivityTypes()
	{
		List<OrgActivityModel.ActivityTypeModel> viewModel = await BeezMart.DAL.CRM.OrgActivities.ActivityTypes.ListAllAsync();
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _ActivityTypes([FromBody] OrgActivityModel.ActivityTypeModel activityType)
	{
		if (TryValidateModel(activityType))
		{
			bool success = await BeezMart.DAL.CRM.OrgActivities.ActivityTypes.SaveAsync(activityType);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
		else
		{
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
		List<OrgActivityModel.ActivityTypeModel> viewModel = await BeezMart.DAL.CRM.OrgActivities.ActivityTypes.ListAllAsync();
		return Json(viewModel);
	}
	#endregion

	#region Segments
	public ViewResult Segments()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _Segments()
	{
		List<OrgSegmentModel> viewModel = await BeezMart.DAL.CRM.OrgSegments.ListWithCountsAsync();
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _Segments([FromBody] OrgSegmentModel segment)
	{
		if (TryValidateModel(segment))
		{
			bool success = await BeezMart.DAL.CRM.OrgSegments.SaveAsync(segment);
			if (!success)
			{
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
		}
		else
		{
			StringBuilder stb = new StringBuilder();
			foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
		List<OrgSegmentModel> viewModel = await BeezMart.DAL.CRM.OrgSegments.ListWithCountsAsync();
		return Json(viewModel);
    }
	[HttpDelete]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _Segments([FromRoute] byte Id)
	{
		bool success = await BeezMart.DAL.CRM.OrgSegments.DeleteAsync(Id, updatedBy: this.User.Identity?.Name);
		if (success)
		{
			List<OrgSegmentModel> viewModel = await BeezMart.DAL.CRM.OrgSegments.ListWithCountsAsync();
			return Json(viewModel);

		}
		else
        {
            StringBuilder stb = new StringBuilder();
            stb.AppendLine("<p>The segment could not be deleted. This happened most probably because the are still some organizations in this segment.</p>");
            stb.Append("<a class='btn btn-sm btn-info' target='_blank' href='");
            stb.Append(Url.Action("Lookup", "Organizations", new { Area = "CRM", Segments = Id }));
            stb.AppendLine("'>Which ones <b class='bi-chevron-right ms-2'></b></a>");
            return Problem(statusCode: 400, title: "Referential integrity", detail: stb.ToString());
        }
    }
	#endregion

	#region Flags
	public ViewResult Flags()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _Flags()
	{
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.Flag);
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _Flags([FromBody] OrgFieldModel field)
	{
		if (TryValidateModel(field))
		{
			bool success = await BeezMart.DAL.CRM.OrgFields.SaveAsync(field);
			if (!success)
			{
                return Problem(
					statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
					title: Statics.Problems.CouldNotSaveDbRecord.Title,
					detail: Statics.Problems.CouldNotSaveDbRecord.Detail
					);
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.Flag);
		return Json(viewModel);
	}
	#endregion

	#region Integer attributes
	public ViewResult Integers()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _Integers()
	{
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.Integer);
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _Integers([FromBody] OrgFieldModel field)
	{
		if (TryValidateModel(field))
		{
			bool success = await BeezMart.DAL.CRM.OrgFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
		else
		{
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.Integer);
		return Json(viewModel);
	}
	#endregion

	#region String attributes
	public ViewResult Strings()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _Strings()
	{
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.String);
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _Strings([FromBody] OrgFieldModel field)
	{
		if (TryValidateModel(field))
		{
			bool success = await BeezMart.DAL.CRM.OrgFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
		else
		{
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.String);
		return Json(viewModel);
	}
	#endregion

	#region Hugetext attributes
	public ViewResult Hugetexts()
	{
		return View();
	}
	[HttpGet]
	public async Task<IActionResult> _Hugetexts()
	{
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.HugeText);
		return Json(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<IActionResult> _Hugetexts([FromBody] OrgFieldModel field)
	{
		if (TryValidateModel(field))
		{
			bool success = await BeezMart.DAL.CRM.OrgFields.SaveAsync(field);
            if (!success)
            {
                return Problem(
                    statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                    title: Statics.Problems.CouldNotSaveDbRecord.Title,
                    detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                    );
            }
        }
		else
		{
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
		List<OrgFieldModel> viewModel = await BeezMart.DAL.CRM.OrgFields.ListAllAsync(FieldType.HugeText);
		return Json(viewModel);
	}
	#endregion
}