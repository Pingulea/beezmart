﻿using BeezMart.Areas.Admin.ViewModels.Users;
using BeezMart.Security;
using BeezMart.Services;
using BeezMart.Utils;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class UsersController : Controller
{
	public ViewResult Index()
	{
		Index_ViewModel viewModel = new Index_ViewModel { };
		viewModel.UserAccounts = this._usersManager.Users.ToList();
		return View(viewModel);
	}

	public async Task<ViewResult> Role(string Name)
	{
		Role_ViewModel viewModel = new Role_ViewModel { };
		viewModel.RoleName = Name;
		Claim claimForRole = new Claim(type: ClaimTypes.Role, value: viewModel.RoleName, valueType: ClaimValueTypes.String, issuer: MyApp.IdentityIssuer);
		viewModel.UserAccountsInRole = (await _usersManager.GetUsersForClaimAsync(claimForRole)).ToList();
		return View(viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Account(string UserID)
	{
		Account_ViewModel viewModel = new Account_ViewModel { };
		viewModel.UserAccount = await this._usersManager.FindByIdAsync(UserID);
		if (viewModel.UserAccount == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The user account does not exist.");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View("Message");
		}
		viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
        viewModel.SendNotification = true;
		return View(viewModel);
	}
	public async Task<JsonResult> Account_Roles(string UserID)
	{
		ApplicationUser? userAccount;
		string userID = UserID;
		if (this.Request.Method == "POST")
		{
			await this._antiforgery.ValidateRequestAsync(this.HttpContext);
			string? formUserID = Request.Form["UserID"];
			userID = string.IsNullOrWhiteSpace(formUserID) ? UserID : formUserID;
			userAccount = await this._usersManager.FindByIdAsync(userID);
			if (userAccount != null)
			{
				string? roleNameToAdd = Request.Form["RoleNameToAdd"];
				string? roleNameToRemove = Request.Form["RoleNameToRemove"];
				if (!string.IsNullOrEmpty(roleNameToAdd))
				{
					await _usersManager.AddClaimAsync(userAccount, new Claim(ClaimTypes.Role, roleNameToAdd, ClaimValueTypes.String, MyApp.IdentityIssuer));
				}
				if (!string.IsNullOrEmpty(roleNameToRemove))
				{
					await _usersManager.RemoveClaimAsync(userAccount, new Claim(ClaimTypes.Role, roleNameToRemove, ClaimValueTypes.String, MyApp.IdentityIssuer));
				}
			}
		}
		else
		{
			userAccount = await this._usersManager.FindByIdAsync(UserID);
		}
		List<string>? accountRoles = null;
		if (userAccount == null) return Json(accountRoles);
        accountRoles = new List<string>();
		foreach (Claim claim in (await this._usersManager.GetClaimsAsync(userAccount)))
		{
			if (claim.Type == ClaimTypes.Role)
			{
                accountRoles.Add(claim.Value);
			}
		}
        accountRoles.Sort();
		return Json(accountRoles);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<PartialViewResult> Account_ResetPassword(string UserID, string NewPassword, string PasswordConfirm, bool SendConfirmation, bool SendNotification)
	{
		AppMessage msg = new AppMessage { };
		#region validate the input
		if (string.IsNullOrEmpty(NewPassword))
		{
			msg.AppendMessage("You must provide a password for the account.");
		}
		else
		{
			if (NewPassword != PasswordConfirm)
			{
				msg.AppendMessage("Please check your password input. The two passwords don't match.");
			}
		}
		if (msg.Count > 0)
		{
			ViewBag.AppMessage = msg.ToHtmlError();
			return PartialView("_Message");
		}
		#endregion
		ApplicationUser? userProfile = await this._usersManager.FindByIdAsync(UserID);
		if (userProfile == null)
		{
			msg.AppendMessage($"Could not find user accoun by this User ID '{UserID}'!");
			ViewBag.AppMessage = msg.ToHtmlError();
			return PartialView("_Message");
		}
		string passwordResetToken = await this._usersManager.GeneratePasswordResetTokenAsync(userProfile);
		IdentityResult identityResult = await this._usersManager.ResetPasswordAsync(userProfile, passwordResetToken, NewPassword);
		if (identityResult.Succeeded)
		{
			msg.AppendMessage("Account password was successfully changed.");
			msg.AppendMessage("<b>" + NewPassword + "</b>");
			#region send e-mail notification and confirmation
			Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?>();
			personalizationStrings.Add("{# Application Full URL #}", EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext));
			personalizationStrings.Add("{# User Name #}", userProfile.UserName);
			personalizationStrings.Add("{# Password #}", HtmlEncoder.Default.Encode(NewPassword));
			personalizationStrings.Add("{# First Name #}", userProfile.FirstName);
			personalizationStrings.Add("{# Last Name #}", userProfile.LastName);
			personalizationStrings.Add("{# eMail Address #}", userProfile.Email);
			personalizationStrings.Add("{# Parameterized Message To Receivers #}", "WARNING: Use the new password for login from now on; the previous password is no longer accepted.");
			string? currentUserName = this.User.Identity?.Name;
			ApplicationUser? currentUser = null;
			if (!string.IsNullOrWhiteSpace(currentUserName)) currentUser = await this._usersManager.FindByNameAsync(currentUserName);
			AppMessage emailErrors = new AppMessage { };
			if (currentUser != null)
			{
				if (string.IsNullOrWhiteSpace(currentUser.Email)) throw new NullReferenceException("User profile for current user has no e-mail address attached.");
                if (SendConfirmation && EmailAddressValidator.IsValidEmailAddress(currentUser.Email))
				{
					EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Users/ResetPassword/Confirmation.html").Personalize(personalizationStrings);
					if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Users/ResetPassword/Confirmation.html'.");
					if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Users/ResetPassword/Confirmation.html'.");
					List<string>? potentialErrors = await this._emailSender.SendEmailAsync(currentUser.Email, emailLayout.Subject, emailLayout.Body);
					if (potentialErrors != null && potentialErrors.Count > 0)
					{
						emailErrors.AppendMessage("Errors occured while sending you the confirmation e-mail with the new account password:");
						emailErrors.AppendRaw("<ul>");
						foreach (string error in potentialErrors)
						{
							emailErrors.AppendRaw($"<li>{error}</li>");
						}
						emailErrors.AppendRaw("</ul>");
					}
				}
			}
			if (SendNotification && EmailAddressValidator.IsValidEmailAddress(userProfile.Email))
            {
                if (string.IsNullOrWhiteSpace(userProfile.Email)) throw new NullReferenceException("User profile has no e-mail address attached.");
                EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Users/ResetPassword/Notification.html").Personalize(personalizationStrings);
				if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Users/ResetPassword/Notification.html'.");
				if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Users/ResetPassword/Notification.html'.");
				List<string>? potentialErrors = await this._emailSender.SendEmailAsync(userProfile.Email, emailLayout.Subject, emailLayout.Body);
				if (potentialErrors != null && potentialErrors.Count > 0)
				{
					emailErrors.AppendMessage("Errors occured while sending the user a notification e-mail with the new account password:");
					emailErrors.AppendRaw("<ul>");
					foreach (string error in potentialErrors)
					{
						emailErrors.AppendRaw($"<li>{error}</li>");
					}
					emailErrors.AppendRaw("</ul>");
				}
			}
			if (emailErrors.Length > 0)
			{
				foreach (string err in emailErrors)
				{
					msg.AppendRaw(err);
				}
			}
			#endregion
			ViewBag.AppMessage = emailErrors.Length > 0 ? msg.ToHtmlWarning() : msg.ToHtmlSuccess();
			return PartialView("_Message");
		}
		else
		{
			msg.AppendMessage("The application failed to change the password for the user account. Please consult the error messages below:");
			foreach (IdentityError error in identityResult.Errors)
			{
				msg.AppendMessage(error.Description);
			}
			ViewBag.AppMessage = msg.ToHtmlError();
			return PartialView("_Message");
		}
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<PartialViewResult> Account_LockState(string UserID, bool LockState = true)
	{
		AppMessage msg = new AppMessage();
		ApplicationUser? userProfile = await this._usersManager.FindByIdAsync(UserID);
        if (userProfile == null)
        {
            msg.AppendMessage($"Could not find user accoun by this User ID '{UserID}'!");
            ViewBag.AppMessage = msg.ToHtmlError();
            return PartialView("_Message");
        }
        userProfile.IsBlocked = LockState;
		IdentityResult identityResult = await this._usersManager.UpdateAsync(userProfile);
		if (identityResult.Succeeded)
		{
			msg.AppendMessage(string.Format("The user account has been successfully {0}.", LockState ? "blocked away" : "unblocked and re-enabled"));
			ViewBag.AppMessage = msg.ToHtmlSuccess();
			return PartialView("_Message");
		}
		else
		{
			msg.AppendMessage(string.Format("The user account could not be {0}.", LockState ? "blocked away" : "unblocked and re-enabled"));
			foreach (IdentityError error in identityResult.Errors)
			{
				msg.AppendMessage(error.Description);
			}
			ViewBag.AppMessage = msg.ToHtmlError();
			return PartialView("_Message");
		}
	}

	[HttpGet]
	public ViewResult Add()
	{
		Add_ViewModel viewModel = new Add_ViewModel
		{
			UserAccount = new ApplicationUser { },
			SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword(),
			SendConfirmation = false,
			SendNotification = false,
		};
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Add(Add_ViewModel viewModel)
	{
		if (viewModel.UserAccount == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.UserAccount.");
		AppMessage msg = new AppMessage { };
		#region validate the input
		if (string.IsNullOrEmpty(viewModel.UserAccount.UserName))
		{
			msg.AppendMessage("You must provide a Username for the account.");
		}
		if (string.IsNullOrEmpty(viewModel.Password))
		{
			msg.AppendMessage("You must provide a Password for the account.");
		}
		else
		{
			if (viewModel.Password != viewModel.PasswordConfirm)
			{
				msg.AppendMessage("Please check your password input. The two passwords don't match.");
			}
		}
		if (string.IsNullOrEmpty(viewModel.UserAccount.Email) || !EmailAddressValidator.IsValidEmailAddress(viewModel.UserAccount.Email))
		{
			msg.AppendMessage("The e-mail address is missing or invalid. You need to provide an e-mail address for password reset purposes.");
		}
		if (msg.Count > 0)
		{
			ViewBag.AppMessage = msg.ToHtmlError();
			viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
			return View(viewModel);
		}
		#endregion
		viewModel.UserAccount.EmailConfirmed = true;
        viewModel.UserAccount.UserGUID = Guid.NewGuid();
		if (string.IsNullOrWhiteSpace(viewModel.Password)) throw new NullReferenceException("ViewModel.Password is NULL!");
		IdentityResult identityResult = await this._usersManager.CreateAsync(viewModel.UserAccount, viewModel.Password);
		if (identityResult.Succeeded)
		{
			#region send e-mail notification and confirmation
			string AppRootURL = EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext);
			Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?>();
			personalizationStrings.Add("{# Application Full URL #}", EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext));
			personalizationStrings.Add("{# User Name #}", viewModel.UserAccount.UserName);
            if (string.IsNullOrWhiteSpace(viewModel.Password)) throw new NullReferenceException("ViewModel.Password is NULL!");
            personalizationStrings.Add("{# Password #}", HtmlEncoder.Default.Encode(viewModel.Password));
            personalizationStrings.Add("{# First Name #}", viewModel.UserAccount.FirstName);
			personalizationStrings.Add("{# Last Name #}", viewModel.UserAccount.LastName);
			personalizationStrings.Add("{# eMail Address #}", viewModel.UserAccount.Email);
			personalizationStrings.Add("{# Account Profile URL #}", AppRootURL + Url.Action("Index", "Account"));
			personalizationStrings.Add("{# Forgotten Password URL #}", AppRootURL + Url.Action("ForgotPassword", "Account"));
			personalizationStrings.Add("{# Login URL #}", AppRootURL + Url.Action("Login", "Account"));
			personalizationStrings.Add("{# Initial Message To Receivers #}", "You may start using the application, according to the user roles you have been assigned when your account was created.");
            string? currentUserName = this.User.Identity?.Name;
            ApplicationUser? currentUser = null;
            if (!string.IsNullOrWhiteSpace(currentUserName)) currentUser = await this._usersManager.FindByNameAsync(currentUserName);
            if (currentUser != null)
            {
                if (viewModel.SendConfirmation && EmailAddressValidator.IsValidEmailAddress(currentUser.Email))
                {
                    if (string.IsNullOrWhiteSpace(currentUser.Email)) throw new NullReferenceException("User profile for current user has no e-mail address attached.");
                    EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Users/Add/Confirmation.html").Personalize(personalizationStrings);
                    if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Users/Add/Confirmation.html'.");
                    if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Users/Add/Confirmation.html'.");
                    List<string>? potentialErrors = await this._emailSender.SendEmailAsync(currentUser.Email, emailLayout.Subject, emailLayout.Body);
                    if (potentialErrors != null && potentialErrors.Count > 0)
                    {
                        msg.AppendMessage("Errors occured while sending you the confirmation e-mail with the newly created account details:");
                        msg.AppendRaw("<ul>");
                        foreach (string error in potentialErrors)
                        {
                            msg.AppendRaw($"<li>{error}</li>");
                        }
                        msg.AppendRaw("</ul>");
                    }
                }
            }
            if (viewModel.SendNotification && EmailAddressValidator.IsValidEmailAddress(viewModel.UserAccount.Email))
            {
                if (string.IsNullOrWhiteSpace(viewModel.UserAccount.Email)) throw new NullReferenceException("User profile has no e-mail address attached.");
                EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Users/Add/Notification.html").Personalize(personalizationStrings);
				if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Users/Add/Notification.html'.");
				if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Users/Add/Notification.html'.");
				List<string>? potentialErrors = await this._emailSender.SendEmailAsync(viewModel.UserAccount.Email, emailLayout.Subject, emailLayout.Body);
				if (potentialErrors != null && potentialErrors.Count > 0)
				{
					msg.AppendMessage("Errors occured while sending the user a notification e-mail with the newly created account details:");
					msg.AppendRaw("<ul>");
					foreach (string error in potentialErrors)
					{
						msg.AppendRaw($"<li>{error}</li>");
					}
					msg.AppendRaw("</ul>");
				}
			}
            #endregion
			if (viewModel.Roles != null)
				foreach (string roleNameToAdd in viewModel.Roles)
				{
					identityResult = await _usersManager.AddClaimAsync(viewModel.UserAccount, new Claim(ClaimTypes.Role, roleNameToAdd, ClaimValueTypes.String, MyApp.IdentityIssuer));
					if (!identityResult.Succeeded)
					{
						msg.AppendMessage($"User role '{roleNameToAdd}' could not be added.");
					}
				}
			if (msg.Count > 0)
			{
				msg.AppendLinkButton("Continue to user account", Url.Action("Account", new { UserID = viewModel.UserAccount.Id }));
				msg.Type = AppMessageType.Warning;
				ViewBag.AppMessage = msg.ToHtmlWarning();
				return View("Message");
			}
			return RedirectToAction("Account", new { UserID = viewModel.UserAccount.Id });
		}
		else
		{
			msg.AppendMessage("The application failed to create the user account. Please consult error messages below and try to fix the input.");
			foreach (IdentityError error in identityResult.Errors)
			{
				msg.AppendMessage(error.Description);
			}
			ViewBag.AppMessage = msg.ToHtmlError();
			viewModel.SuggestedPassword = BeezMart.Utils.Security.Helper.GeneratePassword();
			return View(viewModel);
		}
	}

	[HttpGet]
	public async Task<ViewResult> Edit(string UserID)
	{
		Edit_ViewModel viewModel = new Edit_ViewModel
		{
			SendConfirmation = true,
			SendNotification = true
		};
		viewModel.UserAccount = await this._usersManager.FindByIdAsync(UserID);
		if (viewModel.UserAccount == null)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage("The user account does not exist.");
			msg.Type = AppMessageType.Warning;
			return View("Message", msg);
		}
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
	{
		if (viewModel.UserAccount == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.UserAccount.");
		ApplicationUser? existingAccount = await this._usersManager.FindByIdAsync(viewModel.UserAccount.Id.ToString());
		AppMessage msg = new AppMessage { };
		if (existingAccount == null)
		{
			msg.AppendMessage("The user account does not exist.");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View("Message");
		}
		if (string.IsNullOrWhiteSpace(viewModel.UserAccount.UserName) || string.IsNullOrWhiteSpace(viewModel.UserAccount.Email))
		{
			msg.AppendMessage("The <b>Account username</b> and <b>E-mail address</b> fields cannot be empty - these are required. Check and try again!");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View(viewModel);
		}
		existingAccount.UserName = viewModel.UserAccount.UserName;
		existingAccount.Email = viewModel.UserAccount.Email;
		existingAccount.FirstName = viewModel.UserAccount.FirstName;
		existingAccount.LastName = viewModel.UserAccount.LastName;
		existingAccount.Organization = viewModel.UserAccount.Organization;
		existingAccount.OrganizationId = viewModel.UserAccount.OrganizationId;
		existingAccount.JobTitle = viewModel.UserAccount.JobTitle;
		existingAccount.Department = viewModel.UserAccount.Department;
		existingAccount.Phone = viewModel.UserAccount.Phone;
		existingAccount.Mobile = viewModel.UserAccount.Mobile;
		existingAccount.City = viewModel.UserAccount.City;
		existingAccount.StateProvince = viewModel.UserAccount.StateProvince;
		existingAccount.StreetAddress1 = viewModel.UserAccount.StreetAddress1;
		existingAccount.StreetAddress2 = viewModel.UserAccount.StreetAddress2;
		existingAccount.StreetAddress3 = viewModel.UserAccount.StreetAddress3;
		existingAccount.PostalCode = viewModel.UserAccount.PostalCode;
		IdentityResult identityResult = await this._usersManager.UpdateAsync(existingAccount);
		if (identityResult.Succeeded)
		{
			#region send e-mail notification and confirmation
			Dictionary<string, string?> personalizationStrings = new Dictionary<string, string?>();
			personalizationStrings.Add("{# Application Full URL #}", EmailTemplateLocalizer.GetApplicationRootURL(this.HttpContext));
			personalizationStrings.Add("{# User Name #}", existingAccount.UserName);
			personalizationStrings.Add("{# First Name #}", existingAccount.FirstName);
			personalizationStrings.Add("{# Last Name #}", existingAccount.LastName);
			personalizationStrings.Add("{# eMail Address #}", existingAccount.Email);
			personalizationStrings.Add("{# Parameterized Message To Receivers #}", "WARNING: The account password has not changed; use the existing password for login.");
            string? currentUserName = this.User.Identity?.Name;
            ApplicationUser? currentUser = null;
            if (!string.IsNullOrWhiteSpace(currentUserName)) currentUser = await this._usersManager.FindByNameAsync(currentUserName);
            if (currentUser != null)
            {
                if (viewModel.SendConfirmation && EmailAddressValidator.IsValidEmailAddress(currentUser.Email))
                {
                    if (string.IsNullOrWhiteSpace(currentUser.Email)) throw new NullReferenceException("User profile for current user has no e-mail address attached.");
                    EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Users/Edit/Confirmation.html").Personalize(personalizationStrings);
                    if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Users/Edit/Confirmation.html'.");
                    if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Users/Edit/Confirmation.html'.");
                    List<string>? potentialErrors = await this._emailSender.SendEmailAsync(currentUser.Email, emailLayout.Subject, emailLayout.Body);
                    if (potentialErrors != null && potentialErrors.Count > 0)
                    {
                        msg.AppendMessage("Errors occured while sending you the confirmation e-mail with the new account profile details:");
                        msg.AppendRaw("<ul>");
                        foreach (string error in potentialErrors)
                        {
                            msg.AppendRaw($"<li>{error}</li>");
                        }
                        msg.AppendRaw("</ul>");
                    }
                }
            }
            if (viewModel.SendNotification && EmailAddressValidator.IsValidEmailAddress(existingAccount.Email))
			{
				EmailLayout emailLayout = new EmailLayout(EmailLayout.MasterTemplateFile, "Users/Edit/Notification.html").Personalize(personalizationStrings);
				if (string.IsNullOrWhiteSpace(emailLayout.Subject)) throw new NullReferenceException("E-mail subject line could not be determined from message template 'Users/Edit/Notification.html'.");
				if (string.IsNullOrWhiteSpace(emailLayout.Body)) throw new NullReferenceException("E-mail body/message could not be determined from message template 'Users/Edit/Notification.html'.");
				List<string>? potentialErrors = await this._emailSender.SendEmailAsync(existingAccount.Email, emailLayout.Subject, emailLayout.Body);
				if (potentialErrors != null && potentialErrors.Count > 0)
				{
					msg.AppendMessage("Errors occured while sending the user a notification e-mail with the new account profile details:");
					msg.AppendRaw("<ul>");
					foreach (string error in potentialErrors)
					{
						msg.AppendRaw($"<li>{error}</li>");
					}
					msg.AppendRaw("</ul>");
				}
			}
			#endregion
			if (msg.Count > 0)
			{
				msg.AppendLinkButton("Continue to user account", Url.Action("Account", new { UserID = existingAccount.Id }));
				ViewBag.AppMessage = msg.ToHtmlWarning();
				return View("Message");
			}
			return RedirectToAction("Account", new { UserID = viewModel.UserAccount.Id });
		}
		else
		{
			msg.AppendMessage("The user account fields could not be saved. Please see below the encountered errors:");
			foreach (IdentityError error in identityResult.Errors)
			{
				msg.AppendMessage(error.Description);
			}
			ViewBag.AppMessage = msg.ToHtmlError();
			return View(viewModel);
		}
	}

	[HttpPost]
	public async Task<PartialViewResult> Delete(string UserID)
	{
		AppMessage msg = new AppMessage();
		ApplicationUser? userProfile = await this._usersManager.FindByIdAsync(UserID);
        if (userProfile == null)
        {
            msg.AppendMessage("The user account does not exist.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return PartialView("_Message");
        }
        IdentityResult identityResult = await this._usersManager.DeleteAsync(userProfile);
		if (identityResult.Succeeded)
		{
			msg.AppendMessage("The user account has been successfully deleted, completely removed.");
			ViewBag.AppMessage = msg.ToHtmlSuccess();
			return PartialView("_Message");
		}
		else
		{
			msg.AppendMessage("The user account could not be deleted or removed from database.");
			foreach (IdentityError error in identityResult.Errors)
			{
				msg.AppendMessage(error.Description);
			}
			ViewBag.AppMessage = msg.ToHtmlError();
			return PartialView("_Message");
		}
	}

	#region constructors, properties or fields, and helpers
	public UsersController(UserManager<ApplicationUser> userManager, IAntiforgery antiforgery, IEmailSender emailSender)
	{
		this._usersManager = userManager;
		this._antiforgery = antiforgery;
		this._emailSender = emailSender;
	}
	private readonly IAntiforgery _antiforgery;
	private readonly UserManager<ApplicationUser> _usersManager;
	private readonly IEmailSender _emailSender;
	#endregion
}
