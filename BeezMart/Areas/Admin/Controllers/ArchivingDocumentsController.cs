using BeezMart.Entities.Archiving;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace BeezMart.Areas.Admin.Controllers;

[Area("Admin")]
[Authorize(Roles = BeezMart.Security.UserRoles.ApplicationAdministrator)]
public class ArchivingDocumentsController : Controller
{
    public ViewResult Index()
    {
        return View();
    }

    #region Categories
    public ViewResult Categories()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> _Categories()
    {
        List<DocumentModel.CategoryModel> viewModel = await BeezMart.DAL.Archiving.Documents.Categories.ListAllAsync();
        return Json(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> _Categories([FromBody] DocumentModel.CategoryModel documentCategory)
    {
        if (TryValidateModel(documentCategory))
        {
            bool success = await BeezMart.DAL.Archiving.Documents.Categories.SaveAsync(documentCategory);
            if (!success)
            {
                if (!success)
                {
                    return Problem(
                        statusCode: Statics.Problems.CouldNotSaveDbRecord.Status,
                        title: Statics.Problems.CouldNotSaveDbRecord.Title,
                        detail: Statics.Problems.CouldNotSaveDbRecord.Detail
                        );
                }
            }
        }
        else
        {
            StringBuilder stb = new StringBuilder();
            foreach (var error in ModelState) stb.Append($"Issue with '{error.Key}': {error.Value}");
            return Problem(statusCode: 400, title: "Validation", detail: stb.ToString());
        }
        List<DocumentModel.CategoryModel> viewModel = await BeezMart.DAL.Archiving.Documents.Categories.ListAllAsync();
        return Json(viewModel);
    }
    #endregion
}
