﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.OrgIntegers;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID)
    {
        List<OrgIntegerModel> viewModel = await BeezMart.DAL.CRM.OrgIntegers.ListAsync(OrganizationID);
        return View("~/Areas/CRM/Views/OrgIntegers/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID, long? RecordID)
    {
        OrgIntegerModel? viewModel = null;
        if (RecordID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.OrgIntegers.RetrieveAsync(RecordID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The figure record you're trying to edit no longer exist in the database. RecordID = {RecordID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new OrgIntegerModel();
            viewModel.OrganizationID = OrganizationID;
        }
        return View("~/Areas/CRM/Views/OrgIntegers/_Edit.cshtml", viewModel);
    }
}

