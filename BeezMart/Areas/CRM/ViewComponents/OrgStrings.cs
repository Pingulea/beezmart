﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.OrgStrings;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID)
    {
        List<OrgStringModel> viewModel = await BeezMart.DAL.CRM.OrgStrings.ListAsync(OrganizationID);
        return View("~/Areas/CRM/Views/OrgStrings/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID, long? RecordID)
    {
        OrgStringModel? viewModel = null;
        if (RecordID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.OrgStrings.RetrieveAsync(RecordID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The attribute you're trying to edit no longer exist in the database. RecordID = {RecordID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new OrgStringModel();
            viewModel.OrganizationID = OrganizationID;
        }
        return View("~/Areas/CRM/Views/OrgStrings/_Edit.cshtml", viewModel);
    }
}

