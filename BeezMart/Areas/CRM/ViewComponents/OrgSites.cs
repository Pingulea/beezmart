﻿using BeezMart.Areas.CRM.ViewModels.OrgSites;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.OrgSites;

public class Summary1 : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID, int? SiteAddressID, string SiteAddressType = "Home")
    {
        Summary1_ViewModel viewModel = new Summary1_ViewModel();
        viewModel.OrganizationID = OrganizationID;
        viewModel.SiteAddressType = SiteAddressType;
        viewModel.SiteAddress = null;
        if (SiteAddressID.HasValue)
        {
            viewModel.SiteAddress = await BeezMart.DAL.CRM.OrgSites.RetrieveAsync(SiteAddressID.Value);
            if (viewModel.SiteAddress == null)
            {
                AppMessage msg = new AppMessage();
                msg.Type = AppMessageType.Error;
                msg.AppendMessage("The organization address record you are looking for does not exist.");
                msg.AppendMessage(string.Format("The reference SiteAddressID={0} could not be found in the database.", SiteAddressID));
                msg.AppendLinkButton("Back to organization", Url.Action("View", "Organizations", new { OrganizationID = OrganizationID }));
                return View("~/Views/Shared/_Message.cshtml", msg);
            }
        }
        return View("~/Areas/CRM/Views/OrgSites/_Summary1.cshtml", viewModel);
    }
}

