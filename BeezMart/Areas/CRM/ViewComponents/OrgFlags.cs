﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.OrgFlags;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID)
    {
        List<OrgFlagModel> viewModel = await BeezMart.DAL.CRM.OrgFlags.ListAsync(OrganizationID);
        return View("~/Areas/CRM/Views/OrgFlags/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID, long? FlagID)
    {
        OrgFlagModel? viewModel = null;
        if (FlagID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.OrgFlags.RetrieveAsync(FlagID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The flag you're trying to edit no longer exist in the database. FlagID = {FlagID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new OrgFlagModel();
            viewModel.OrganizationID = OrganizationID;
        }
        return View("~/Areas/CRM/Views/OrgFlags/_Edit.cshtml", viewModel);
    }
}

