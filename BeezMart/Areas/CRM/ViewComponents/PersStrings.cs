﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.PersStrings;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID)
    {
        List<PersStringModel> viewModel = await BeezMart.DAL.CRM.PersStrings.ListAsync(PersonID);
        return View("~/Areas/CRM/Views/PersStrings/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID, long? RecordID)
    {
        PersStringModel? viewModel = null;
        if (RecordID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.PersStrings.RetrieveAsync(RecordID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The attribute you're trying to edit no longer exist in the database. RecordID = {RecordID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new PersStringModel();
            viewModel.PersonID = PersonID;
        }
        return View("~/Areas/CRM/Views/PersStrings/_Edit.cshtml", viewModel);
    }
}

