﻿using BeezMart.Entities.CRM;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.OrgHugetexts;

public class Index : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID)
    {
        List<OrgHugetextModel> viewModel = await BeezMart.DAL.CRM.OrgHugetexts.ListAsync(OrganizationID);
        ViewBag.OrganizationID = OrganizationID;
        return View("~/Areas/CRM/Views/OrgHugetexts/_Index.cshtml", viewModel);
    }
}

