﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.PersContacts;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID)
    {
        List<PersContactModel> viewModel = await BeezMart.DAL.CRM.PersContacts.ListAsync(PersonID);
        return View("~/Areas/CRM/Views/PersContacts/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID, long? ContactID)
    {
        PersContactModel? viewModel = null;
        if (ContactID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.PersContacts.RetrieveAsync(ContactID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The contact detail you're trying to edit no longer exist in the database. ContactID = {ContactID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new PersContactModel();
            viewModel.PersonID = PersonID;
        }
        return View("~/Areas/CRM/Views/PersContacts/_Edit.cshtml", viewModel);
    }
}

