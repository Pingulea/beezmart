﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.OrgContacts;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID)
    {
        List<OrgContactModel> viewModel = await BeezMart.DAL.CRM.OrgContacts.ListAsync(OrganizationID);
        return View("~/Areas/CRM/Views/OrgContacts/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int OrganizationID, long? ContactID)
    {
        OrgContactModel? viewModel = null;
        if (ContactID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.OrgContacts.RetrieveAsync(ContactID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The contact detail you're trying to edit no longer exist in the database. ContactID = {ContactID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new OrgContactModel();
            viewModel.OrganizationID = OrganizationID;
        }
        return View("~/Areas/CRM/Views/OrgContacts/_Edit.cshtml", viewModel);
    }
}

