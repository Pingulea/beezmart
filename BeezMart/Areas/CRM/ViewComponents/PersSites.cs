﻿using BeezMart.Areas.CRM.ViewModels.PersSites;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.PersSites;

public class Summary1 : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID, int? SiteAddressID, string SiteAddressType = "Home")
    {
        Summary1_ViewModel viewModel = new Summary1_ViewModel();
        viewModel.PersonID = PersonID;
        viewModel.SiteAddressType = SiteAddressType;
        viewModel.SiteAddress = null;
        if (SiteAddressID.HasValue)
        {
            viewModel.SiteAddress = await BeezMart.DAL.CRM.PersSites.RetrieveAsync(SiteAddressID.Value);
            if (viewModel.SiteAddress == null)
            {
                AppMessage msg = new AppMessage();
                msg.Type = AppMessageType.Error;
                msg.AppendMessage("The person address record you are looking for does not exist.");
                msg.AppendMessage(string.Format("The reference SiteAddressID={0} could not be found in the database.", SiteAddressID));
                msg.AppendLinkButton("Back to person", Url.Action("View", "Persons", new { PersonID = PersonID }));
                return View("~/Views/Shared/_Message.cshtml", msg);
            }
        }
        return View("~/Areas/CRM/Views/PersSites/_Summary1.cshtml", viewModel);
    }
}

