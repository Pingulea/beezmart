﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.PersFlags;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID)
    {
        List<PersFlagModel> viewModel = await BeezMart.DAL.CRM.PersFlags.ListAsync(PersonID);
        return View("~/Areas/CRM/Views/PersFlags/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID, long? FlagID)
    {
        PersFlagModel? viewModel = null;
        if (FlagID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.PersFlags.RetrieveAsync(FlagID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The flag you're trying to edit no longer exist in the database. FlagID = {FlagID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new PersFlagModel();
            viewModel.PersonID = PersonID;
        }
        return View("~/Areas/CRM/Views/PersFlags/_Edit.cshtml", viewModel);
    }
}

