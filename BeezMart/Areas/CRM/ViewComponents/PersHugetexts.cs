﻿using BeezMart.Entities.CRM;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.PersHugetexts;

public class Index : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID)
    {
        List<PersHugetextModel> viewModel = await BeezMart.DAL.CRM.PersHugetexts.ListAsync(PersonID);
        ViewBag.PersonID = PersonID;
        return View("~/Areas/CRM/Views/PersHugetexts/_Index.cshtml", viewModel);
    }
}

