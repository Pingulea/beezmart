﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.ViewComponents.PersIntegers;

public class List : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID)
    {
        List<PersIntegerModel> viewModel = await BeezMart.DAL.CRM.PersIntegers.ListAsync(PersonID);
        return View("~/Areas/CRM/Views/PersIntegers/_List.cshtml", viewModel);
    }
}
public class Edit : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int PersonID, long? RecordID)
    {
        PersIntegerModel? viewModel = null;
        if (RecordID.HasValue)
        {
            viewModel = await BeezMart.DAL.CRM.PersIntegers.RetrieveAsync(RecordID.Value);
            if (viewModel == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The figure record you're trying to edit no longer exist in the database. RecordID = {RecordID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (viewModel == null)
        {
            viewModel = new PersIntegerModel();
            viewModel.PersonID = PersonID;
        }
        return View("~/Areas/CRM/Views/PersIntegers/_Edit.cshtml", viewModel);
    }
}

