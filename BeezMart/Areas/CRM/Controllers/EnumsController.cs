﻿using BeezMart.Entities.Geography;
using BeezMart;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize]
public class EnumsController : Controller
{

	public ActionResult Index()
	{
		return View();
	}

	/// <summary>
	///		<para>Gets all the Countries that have been registered in the database</para>
	/// </summary>
	/// <returns></returns>
	public async Task<JsonResult> CountriesAll()
	{
		List<Country> viewModel = await BeezMart.DAL.Geography.Countries.ListAsync();
		return Json(viewModel);
	}

	public async Task<JsonResult> StatesProvinces(string CountryID)
	{
		if (string.IsNullOrEmpty(CountryID)) CountryID = MyApp.ConfigSettings["CRM.Default-CountryID"];
		List<StateProvince> viewModel = await BeezMart.DAL.Geography.StatesProvinces.ListAsync(CountryID);
		return Json(viewModel);
	}
}