﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class OrgStringsController : Controller
{

    public ViewComponentResult _List(int OrganizationID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgStrings.List), new { OrganizationID = OrganizationID });
    }

    [HttpGet]
    public ViewComponentResult _Edit(int OrganizationID, long? RecordID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgStrings.Edit), new { OrganizationID = OrganizationID, RecordID = RecordID });
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(OrgStringModel stringField)
    {
        List<OrgStringModel> viewModel;
        if (!Request.Form.ContainsKey("FieldID") || string.IsNullOrEmpty(Request.Form["FieldID"][0]))
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please select the type of organization attribute you want added!");
            ViewBag.AppMessage = msg.ToHtmlError();
            viewModel = await BeezMart.DAL.CRM.OrgStrings.ListAsync(stringField.OrganizationID);
            return PartialView("_List", viewModel);
        }
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.OrgStrings.SaveAsync(stringField);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete attribute record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        viewModel = await BeezMart.DAL.CRM.OrgStrings.ListAsync(stringField.OrganizationID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int OrganizationID, long RecordID)
    {
        await BeezMart.DAL.CRM.OrgStrings.DeleteAsync(RecordID);
        List<OrgStringModel> viewModel = await BeezMart.DAL.CRM.OrgStrings.ListAsync(OrganizationID);
        return PartialView("_List", viewModel);
    }
}
