﻿using BeezMart.Areas.CRM.ViewModels.RelationOrgsPers;
using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class RelationOrgsPersController : Controller
{

    [HttpGet]
    public async Task<ViewResult> Add(int OrganizationID, int PersonID)
    {
        OrganizationModel? organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
        PersonModel? person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
        if (organization == null || person == null)
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("The referenced organization record or person record could not be found in the database.");
            msg.AppendMessage("The Organization ID was " + OrganizationID.ToString());
            msg.AppendMessage("The Person ID was " + PersonID.ToString());
            msg.Type = AppMessageType.Error;
            return View("Message", msg);
        }
        Add_ViewModel viewModel = new Add_ViewModel();
        viewModel.RelationOrgsPers = new RelationOrgsPersModel();
        viewModel.RelationOrgsPers.OrganizationID = OrganizationID;
        viewModel.RelationOrgsPers.PersonID = PersonID;
        viewModel.RelationOrgsPers.PersFirstName = person.FirstName;
        viewModel.RelationOrgsPers.PersLastName = person.LastName;
        viewModel.RelationOrgsPers.PersNicknames = person.Nicknames;
        viewModel.RelationOrgsPers.OrgName = organization.Name;
        viewModel.RelationOrgsPers.OrgAltnames = organization.AlternateNames;
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Add(Add_ViewModel viewModel)
    {
        if (viewModel.RelationOrgsPers == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.RelationOrgsPers.");
        await BeezMart.DAL.CRM.RelationsOrgsPers.SaveAsync(viewModel.RelationOrgsPers);
        bool redirectToPersonView = !string.IsNullOrWhiteSpace(viewModel.AfterSaveGoTo) && viewModel.AfterSaveGoTo.StartsWith("person", StringComparison.OrdinalIgnoreCase);
        if (redirectToPersonView) return RedirectToAction("View", "Persons", new { PersonID = viewModel.RelationOrgsPers.PersonID, tab = 2 });
        else return RedirectToAction("View", "Organizations", new { OrganizationID = viewModel.RelationOrgsPers.OrganizationID, tab = 2 });
    }

    public async Task<PartialViewResult> _ListPersons(int OrganizationID)
    {
        ViewBag.OrganizationID = OrganizationID;
        List<RelationOrgsPersModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.ListPersonsAsync(OrganizationID);
        return PartialView(viewModel);
    }
    public async Task<PartialViewResult> _ListOrganizations(int PersonID)
    {
        ViewBag.PersonID = PersonID;
        List<RelationOrgsPersModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.ListOrganizationsAsync(PersonID);
        return PartialView(viewModel);
    }

    public async Task<PartialViewResult> _LookupPersons(string NamePattern, bool NameExactMatching)
    {
        PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
        {
            NamePattern = NamePattern,
            NameExactMatch = NameExactMatching
        };
        List<PersonModel> viewModel = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
        return PartialView(viewModel);
    }
    public async Task<PartialViewResult> _LookupOrganizations(string NamePattern, bool NameExactMatching)
    {
        OrganizationModel.LookupFilter filter = new OrganizationModel.LookupFilter()
        {
            NamePattern = NamePattern,
            NameExactMatch = NameExactMatching
        };
        List<OrganizationModel> viewModel = await BeezMart.DAL.CRM.Organizations.LookupAsync(filter);
        return PartialView(viewModel);
    }

    public PartialViewResult _ModalAddPerson(int OrganizationID)
    {
        _ModalAddPerson_ViewModel viewModel = new _ModalAddPerson_ViewModel();
        viewModel.OrganizationID = OrganizationID;
        return PartialView(viewModel);
    }
    public PartialViewResult _ModalAddOrganization(int PersonID)
    {
        _ModalAddOrganization_ViewModel viewModel = new _ModalAddOrganization_ViewModel();
        viewModel.PersonID = PersonID;
        return PartialView(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _ModalEditRelationPerson(RelationOrgsPersModel relation)
    {
        if (TryValidateModel(relation))
        {
            await BeezMart.DAL.CRM.RelationsOrgsPers.SaveAsync(relation);
        }
        ViewBag.OrganizationID = relation.OrganizationID;
        List<RelationOrgsPersModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.ListPersonsAsync(relation.OrganizationID);
        return PartialView("_ListPersons", viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _ModalEditRelationOrganization(RelationOrgsPersModel relation)
    {
        if (TryValidateModel(relation))
        {
            await BeezMart.DAL.CRM.RelationsOrgsPers.SaveAsync(relation);
        }
        ViewBag.PersonID = relation.PersonID;
        List<RelationOrgsPersModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.ListOrganizationsAsync(relation.PersonID);
        return PartialView("_ListOrganizations", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _DeleteFromOrg(long RelationID, int OrganizationID)
    {
        await BeezMart.DAL.CRM.RelationsOrgsPers.DeleteAsync(RelationID);
        List<RelationOrgsPersModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.ListPersonsAsync(OrganizationID);
        return PartialView("_ListPersons", viewModel);
    }
    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _DeleteFromPers(long RelationID, int PersonID)
    {
        await BeezMart.DAL.CRM.RelationsOrgsPers.DeleteAsync(RelationID);
        List<RelationOrgsPersModel> viewModel = await BeezMart.DAL.CRM.RelationsOrgsPers.ListOrganizationsAsync(PersonID);
        return PartialView("_ListOrganizations", viewModel);
    }
}
