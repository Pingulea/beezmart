﻿using BeezMart.Areas.CRM.ViewModels.OrgActivities;
using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class OrgActivitiesController : Controller
{
    public async Task<ViewResult> Index(int OrganizationID)
    {
        Index_ViewModel viewModel = new Index_ViewModel { };
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
        viewModel.Activities = await BeezMart.DAL.CRM.OrgActivities.ListAsync(OrganizationID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _Index(int OrganizationID)
    {
        List<OrgActivityModel> viewModel = await BeezMart.DAL.CRM.OrgActivities.ListAsync(OrganizationID);
        ViewBag.OrganizationID = OrganizationID;
        return PartialView(viewModel);
    }

    [HttpGet]
    public async Task<ViewResult> Edit(long ActivityID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.Activity = await BeezMart.DAL.CRM.OrgActivities.RetrieveAsync(ActivityID);
        if (viewModel.Activity == null)
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage($"The activity record you're trying to edit no longer exist in the database. ActivityID = {ActivityID}.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(viewModel.Activity.OrganizationID);
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
    {
        if (viewModel.Activity == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Activity.");
        bool success = await BeezMart.DAL.CRM.OrgActivities.SaveAsync(viewModel.Activity);
        if (success)
        {
            return RedirectToAction("View", "Organizations", new { OrganizationID = viewModel.Activity.OrganizationID, tab = 4 });
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete activity record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View(viewModel);
        }
    }

    [HttpGet]
    public async Task<ViewResult> Add(int OrganizationID)
    {
        Add_ViewModel viewModel = new Add_ViewModel();
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
        if (viewModel.Organization == null)
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage($"The organization record no longer exist in the database. OrganizationID = {OrganizationID}.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        if (viewModel.Organization.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Organization.ID is null.");
        viewModel.Activity = new OrgActivityModel();
        viewModel.Activity.OrganizationID = viewModel.Organization.ID.Value;
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Add(Add_ViewModel viewModel)
    {
        if (viewModel.Activity == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Activity.");
        bool success = await BeezMart.DAL.CRM.OrgActivities.SaveAsync(viewModel.Activity);
        if (success)
        {
            return RedirectToAction("View", "Organizations", new { OrganizationID = viewModel.Activity.OrganizationID, tab = 4 });
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete activity record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View(viewModel);
        }
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Delete(int OrganizationID, long ActivityID)
    {
        await BeezMart.DAL.CRM.OrgActivities.DeleteAsync(ActivityID);
        return RedirectToAction("View", "Organizations", new { OrganizationID = OrganizationID, tab = 4 });
    }
}
