﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class OrgIntegersController : Controller
{

    public ViewComponentResult _List(int OrganizationID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgIntegers.List), new { OrganizationID = OrganizationID });
    }

    [HttpGet]
    public ViewComponentResult _Edit(int OrganizationID, long? RecordID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgIntegers.Edit), new { OrganizationID = OrganizationID, RecordID = RecordID });
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(OrgIntegerModel integerField)
    {
        List<OrgIntegerModel> viewModel;
        if (!Request.Form.ContainsKey("FieldID") || string.IsNullOrEmpty(Request.Form["FieldID"][0]))
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please select the type of organization figure you want added!");
            ViewBag.AppMessage = msg.ToHtmlError();
            viewModel = await BeezMart.DAL.CRM.OrgIntegers.ListAsync(integerField.OrganizationID);
            return PartialView("_List", viewModel);
        }
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.OrgIntegers.SaveAsync(integerField);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete organization figure record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        viewModel = await BeezMart.DAL.CRM.OrgIntegers.ListAsync(integerField.OrganizationID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int OrganizationID, long RecordID)
    {
        await BeezMart.DAL.CRM.OrgIntegers.DeleteAsync(RecordID);
        List<OrgIntegerModel> viewModel = await BeezMart.DAL.CRM.OrgIntegers.ListAsync(OrganizationID);
        return PartialView("_List", viewModel);
    }
}
