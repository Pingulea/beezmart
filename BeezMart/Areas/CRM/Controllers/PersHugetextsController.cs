﻿using BeezMart.Entities.CRM;
using BeezMart.Areas.CRM.ViewModels.PersHugetexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BeezMart.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeezMart.Areas.CRM.Controllers
{
	[Area("CRM")]
	[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
	public class PersHugetextsController : Controller
	{

		public async Task<PartialViewResult> _Index(int PersonID)
		{
			List<PersHugetextModel> viewModel = await BeezMart.DAL.CRM.PersHugetexts.ListAsync(PersonID);
			ViewBag.PersonID = PersonID;
			return PartialView(viewModel);
		}

		[HttpGet]
		public async Task<ViewResult> Edit(int PersonID, long? RecordID)
		{
			AppMessage msg = new AppMessage();
			Edit_ViewModel viewModel = new Edit_ViewModel();
			viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
			if (viewModel.Person == null)
			{
				msg.AppendMessage($"The person you're trying to edit details for no longer exist in the database. PersonID = {PersonID}");
				ViewBag.AppMessage = msg.ToHtmlError();
				return View("Message");
			}
			if (RecordID.HasValue)
			{
				viewModel.Hugetext = await BeezMart.DAL.CRM.PersHugetexts.RetrieveAsync(RecordID.Value);
				if (viewModel.Hugetext == null)
				{
					msg.AppendMessage($"The detailed info you're trying to edit no longer exist in the database. RecordID = {RecordID}");
					ViewBag.AppMessage = msg.ToHtmlError();
				}
			}
			if (viewModel.Hugetext == null)
			{
				viewModel.Hugetext = new PersHugetextModel();
				viewModel.Hugetext.PersonID = PersonID;
			}
			return View(viewModel);
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
		{
			AppMessage msg = new AppMessage();
			if (ModelState.IsValid)
			{
				if (viewModel.Hugetext == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Hugetext.");
                if (viewModel.Person == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Person.");
                if (viewModel.Person.ID == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Person.ID.");
                viewModel.Hugetext.PersonID = viewModel.Person.ID.Value;
                bool success = await BeezMart.DAL.CRM.PersHugetexts.SaveAsync(viewModel.Hugetext);
				if (success)
				{
					return RedirectToAction("View", "Persons", new { PersonID = viewModel.Hugetext.PersonID, tab = 3 });
				}
			}
			else
			{
				msg.AppendMessage("You have submitted an invalid or incomplete detailed info record. Please check and try again.");
				ViewBag.AppMessage = msg.ToHtmlError();
			}
			return View(viewModel);
		}

		[HttpGet]
		public async Task<PartialViewResult> _Detail(long RecordID)
		{
			PersHugetextModel? viewModel = await BeezMart.DAL.CRM.PersHugetexts.RetrieveAsync(RecordID);
			return PartialView(viewModel);
		}

		[HttpDelete]
		[ValidateAntiForgeryToken]
		public async Task<PartialViewResult> _Delete(int PersonID, long RecordID)
		{
			await BeezMart.DAL.CRM.PersHugetexts.DeleteAsync(RecordID);
			List<PersHugetextModel> viewModel = await BeezMart.DAL.CRM.PersHugetexts.ListAsync(PersonID);
			ViewBag.PersonID = PersonID;
			return PartialView("_Index", viewModel);
		}
	}
}