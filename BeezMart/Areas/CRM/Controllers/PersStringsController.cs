﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersStringsController : Controller
{

    public async Task<PartialViewResult> _List(int PersonID)
    {
        List<PersStringModel> viewModel = await BeezMart.DAL.CRM.PersStrings.ListAsync(PersonID);
        return PartialView(viewModel);
    }

    [HttpGet]
    public async Task<PartialViewResult> _Edit(int PersonID, long? RecordID)
    {
        PersStringModel? stringField = null;
        if (RecordID.HasValue)
        {
            stringField = await BeezMart.DAL.CRM.PersStrings.RetrieveAsync(RecordID.Value);
            if (stringField == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The attribute you're trying to edit no longer exist in the database. RecordID = {RecordID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (stringField == null)
        {
            stringField = new PersStringModel();
            stringField.PersonID = PersonID;
        }
        return PartialView(stringField);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(PersStringModel stringField)
    {
        List<PersStringModel> viewModel;
        if (!Request.Form.ContainsKey("FieldID") || string.IsNullOrEmpty(Request.Form["FieldID"][0]))
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please select the type of person attribute you want added!");
            ViewBag.AppMessage = msg.ToHtmlError();
            viewModel = await BeezMart.DAL.CRM.PersStrings.ListAsync(stringField.PersonID);
            return PartialView("_List", viewModel);
        }
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.PersStrings.SaveAsync(stringField);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete attribute record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        viewModel = await BeezMart.DAL.CRM.PersStrings.ListAsync(stringField.PersonID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int PersonID, long RecordID)
    {
        await BeezMart.DAL.CRM.PersStrings.DeleteAsync(RecordID);
        List<PersStringModel> viewModel = await BeezMart.DAL.CRM.PersStrings.ListAsync(PersonID);
        return PartialView("_List", viewModel);
    }
}
