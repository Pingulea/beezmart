﻿using BeezMart.Areas.CRM.ViewModels.Persons;
using BeezMart.Caching.CRM;
using BeezMart.Entities.CRM;
using BeezMart;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Drawing;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersonsController : Controller
{
	protected static string _limitedDefaultLookupResult = "Since you haven't specified any filtering criteria, the lookup will only show maximum {0} person records from database.";

	public ActionResult Index()
	{
		return View();
	}

	/// <summary>
	///		<para>Gets an advanced search page with an AJAX-updatable search result panel given by _Search() action</para>
	/// </summary>
	/// <returns></returns>
	public async Task<ViewResult> Search()
	{
		Search_ViewModel viewModel = new Search_ViewModel();
        if (Request.QueryString.HasValue)
        {
            await TryUpdateModelAsync<Search_ViewModel>(viewModel);
        }
        return View(viewModel);
	}
	public async Task<PartialViewResult> _Search(Search_Params parameters)
    {
        PersonModel.LookupFilter filter;
        List<PersonModel> viewModel;
        if (!parameters.FilterIsSet)
        {
            AppMessage msg = new AppMessage();
			string maxRowCount = MyApp.ConfigSettings["CRM.Search.Result.MaxCount.Unfiltered"];
			msg.AppendMessage(string.Format(_limitedDefaultLookupResult, maxRowCount));
			ViewBag.AppMessage = msg.ToHtmlWarning();
            filter = new PersonModel.LookupFilter() { };
            viewModel = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
            return PartialView(viewModel);
        }
        filter = new PersonModel.LookupFilter()
        {
            NamePattern = parameters.Name,
            NameExactMatch = parameters.NameExactMatching,
            Contact = parameters.Contact,
            City = parameters.City,
            InSegments = parameters.Segments,
            HasFlags = parameters.Flags
        };
		viewModel = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
		return PartialView(viewModel);
	}
	public async Task<IActionResult> _Search_Export(Search_Params parameters)
	{
		PersonModel.LookupFilter filter;
        List<PersonModel> records;
		if (parameters.FilterIsSet)
		{
			filter = new PersonModel.LookupFilter()
				{
					NamePattern = parameters.Name,
					NameExactMatch = parameters.NameExactMatching,
					Contact = parameters.Contact,
					City = parameters.City,
					InSegments = parameters.Segments,
					HasFlags = parameters.Flags
				};
		}
		else
        {
            filter = new PersonModel.LookupFilter() { };
        }
        records = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
        string xlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        string fileName = "BeezMart-CRM-Persons.xlsx";
        using (ExcelPackage excelPackage = new ExcelPackage())
        {
            ExcelWorksheet excelWorksheet, wks;
            ExcelRange topRow;
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Persons");
            #region populate worksheet with records
            wks = excelWorksheet;
            int col = 1;
            wks.SetValue(1, col, "Person_ID"); col++;			        // Column A
            wks.SetValue(1, col, "First_Name"); col++;			        // Column B
            wks.SetValue(1, col, "Last_Name"); col++;			        // Column C
            wks.SetValue(1, col, "Segment_ID"); col++;			        // Column D
            wks.SetValue(1, col, "Segment_Name"); col++;			    // Column E
            wks.SetValue(1, col, "Name_Prefix"); col++;			        // Column F
            wks.SetValue(1, col, "Name_Suffix"); col++;			        // Column G
            wks.SetValue(1, col, "Nicknames"); col++;			        // Column H
            wks.SetValue(1, col, "Country_Of_Origin"); col++;			// Column I
            wks.SetValue(1, col, "Site_ID_Home"); col++;			    // Column J
            wks.SetValue(1, col, "Site_ID_Billing"); col++;			    // Column K
            wks.SetValue(1, col, "Site_ID_Shipping"); col++;			// Column L
            wks.SetValue(1, col, "Country"); col++;			        	// Column M
            wks.SetValue(1, col, "Region"); col++;			        	// Column N
            wks.SetValue(1, col, "State_Province"); col++;			    // Column O
            wks.SetValue(1, col, "City"); col++;			        	// Column P
            wks.SetValue(1, col, "Street_Address_1"); col++;			// Column Q
            topRow = wks.Cells["A1:Q1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            int row = 1;
            foreach (PersonModel item in records)
            {
                row++;
                col = 1;
                wks.SetValue(row, col, item.ID); col++;
                wks.SetValue(row, col, item.FirstName); col++;
                wks.SetValue(row, col, item.LastName); col++;
                wks.SetValue(row, col, item.SegmentID); col++;
                wks.SetValue(row, col, item.Segment); col++;
                wks.SetValue(row, col, item.NamePrefix); col++;
                wks.SetValue(row, col, item.NameSuffix); col++;
                wks.SetValue(row, col, item.Nicknames); col++;
                wks.SetValue(row, col, item.CountryOfOrigin); col++;
                wks.SetValue(row, col, item.SiteIdHome); col++;
                wks.SetValue(row, col, item.SiteIdBill); col++;
                wks.SetValue(row, col, item.SiteIdShip); col++;
                wks.SetValue(row, col, item.Home_Country); col++;
                wks.SetValue(row, col, item.Home_Region); col++;
                wks.SetValue(row, col, item.Home_StateProvince); col++;
                wks.SetValue(row, col, item.Home_City); col++;
                wks.SetValue(row, col, item.Home_Address); col++;
            }
            wks.DefaultColWidth = 20;
            for (col = 1; col < 19; col++) wks.Column(col).AutoFit();
            #endregion
            excelWorksheet = excelPackage.Workbook.Worksheets.Add("Filter");
            #region populate filtering criteria
            wks = excelWorksheet;
            row = 0;
            row++; wks.SetValue(row, 1, "Selection filter");
            wks.SetValue(row, 2, parameters.FilterIsSet ? "SEE HOW RECORDS WERE SELECTED" : "NO FILTERING CRITERIA WAS SPECIFIED");
            row++; wks.SetValue(row, 1, "Name pattern");
            wks.SetValue(row, 2, filter.NamePattern);
            row++; wks.SetValue(row, 1, "Name exact match");
            wks.SetValue(row, 2, filter.NameExactMatch.ToString());
            row++; wks.SetValue(row, 1, "Contact");
            wks.SetValue(row, 2, filter.Contact);
            row++; wks.SetValue(row, 1, "City");
            wks.SetValue(row, 2, filter.City);
            row++; wks.SetValue(row, 1, "In segments (IDs)");
            wks.SetValue(row, 2, filter.InSegments);
            row++; wks.SetValue(row, 1, "Has flags (IDs)");
            wks.SetValue(row, 2, filter.HasFlags);
            topRow = wks.Cells["A1:B1"];
            topRow.Style.Font.Bold = true;
            topRow.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            topRow.Style.Font.Color.SetColor(Color.Yellow);
            topRow.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            topRow.Style.Fill.BackgroundColor.SetColor(Color.DarkGreen);
            wks.Column(1).AutoFit();
            wks.Column(2).AutoFit();
            #endregion
			return File(excelPackage.GetAsByteArray(), xlsxContentType, fileName);
        }
	}

	/// <summary>
	///		<para>Displays a list of persons filtered by a set of criteria</para>
	/// </summary>
	/// <param name="viewModel"></param>
	/// <returns></returns>
	public async Task<ViewResult> Lookup(Lookup_ViewModel viewModel)
	{
		PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
			{
				NamePattern = viewModel.Name,
				NameExactMatch = viewModel.Exact,
				Contact = viewModel.Contact,
				City = viewModel.City,
				InSegments = viewModel.Segments,
				HasFlags = viewModel.Flags
			};
		if (!filter.HasAnyCriteria)
		{
			AppMessage msg = new AppMessage();
			msg.AppendMessage(string.Format("Since no filtering criteria is specified, lookup only shows maximum top {0} person records from database, ordered by person name.", MyApp.ConfigSettings["CRM.Search.Result.MaxCount.Unfiltered"]));
			ViewBag.AppMessage = msg.ToHtmlWarning();
		}
		List<PersonModel> results = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
		return View(results);
	}

	public async Task<ViewResult> View(int PersonID)
	{
		View_ViewModel viewModel = new View_ViewModel();
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
		if (viewModel.Person == null)
		{
			AppMessage msg = new AppMessage();
			msg.Type = AppMessageType.Error;
			msg.AppendMessage("The person record you are looking for does not exist.");
			msg.AppendMessage(string.Format("The reference PersonID={0} could not be found in the database.", PersonID));
			msg.AppendLinkButton("Lookup persons", Url.Action("Search"));
			return View("Message", msg);
		}
		return View(viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Add(int? OrganizationID)
	{
		Add_ViewModel viewModel = new Add_ViewModel();
		viewModel.ListOfSegments = await PersSegments.GetListAsync();
		viewModel.OrganizationID = OrganizationID;
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Add(Add_ViewModel viewModel)
	{
		AppMessage msg = new AppMessage();
		if (!ModelState.IsValid)
		{
			foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View(viewModel);
		}
		if (viewModel.Person == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Person.");
		bool success = await BeezMart.DAL.CRM.Persons.SaveAsync(viewModel.Person);
		if (viewModel.Person.ID == null) throw new NullReferenceException("Saving the ViewModel.Person record resulted in iewModel.Person.ID being null");
		if (success)
		{
			if (viewModel.OrganizationID.HasValue)
			{
				return RedirectToAction("Add", "RelationOrgsPers", routeValues: new { OrganizationID = viewModel.OrganizationID.Value, PersonID = viewModel.Person.ID.Value });
			}
			if (viewModel.AddAddressToo)
			{
				string? urlToViewPerson = Url.Action("View", new { PersonID = viewModel.Person.ID.Value });
				return RedirectToAction("Add", "PersSites", routeValues: new { PersonID = viewModel.Person.ID.Value, Type = "home,billing,shipping", NextUrl = urlToViewPerson });
			}
			return RedirectToAction("View", routeValues: new { PersonID = viewModel.Person.ID.Value });
		}
		return View(viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Edit(int PersonID)
	{
		Edit_ViewModel viewModel = new Edit_ViewModel();
		viewModel.ListOfSegments = await PersSegments.GetListAsync();
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
		if (viewModel.Person == null)
		{
			AppMessage msg = new AppMessage();
			msg.Type = AppMessageType.Error;
			msg.AppendMessage("The person record you are trying to edit does not exist.");
			msg.AppendMessage($"The reference PersonID={PersonID} could not be found in the database.");
			msg.AppendLinkButton("Lookup persons", Url.Action("Search"));
			return View("Message", msg);
		}
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
	{
		if (viewModel.Person == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Person.");
		if (ModelState.IsValid)
		{
			bool success = await BeezMart.DAL.CRM.Persons.SaveAsync(viewModel.Person);
			if (viewModel.Person.ID == null) throw new NullReferenceException("After having saved the record in database, its ID is missing: ViewModel.Person.ID is null.");
			if (success)
			{
				return RedirectToAction("View", routeValues: new { PersonID = viewModel.Person.ID.Value });
			}
		}
		else
		{
			AppMessage msg = new AppMessage();
			foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
			ViewBag.AppMessage = msg.ToHtmlError();
		}
		return View(viewModel);
	}

	[HttpGet]
	public ViewResult Merge()
	{
		Merge_ViewModel viewModel = new Merge_ViewModel();
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<string?> Merge(int[] FromPersIDs, int? ToPersID)
	{
		AppMessage msg = new AppMessage();
		msg.IsDismisable = false;
		if (FromPersIDs == null || FromPersIDs.Length == 0)
		{
			msg.AppendMessage("Please specify at least one person record that should be merged.");
		}
		if (!ToPersID.HasValue)
		{
			msg.AppendMessage("Please specify the person record to which all info is merged.");
		}
		if (msg.Count > 0)
		{
			return msg.ToHtmlError();
		}
		int countOfPerformedMergings = 0;
#pragma warning disable CS8602 // Dereference of a possibly null reference: FromOrgIDs.
		foreach (int FromPersID in FromPersIDs)
        {
#pragma warning disable CS8629 // Nullable value type may be null: ToPersID.
            if (FromPersID == ToPersID.Value) continue;
            bool success = await BeezMart.DAL.CRM.Persons.MergeAsync(FromPersID, ToPersID.Value);
			if (success) countOfPerformedMergings++;
#pragma warning restore CS8629 // Nullable value type may be null.
		}
#pragma warning restore CS8602 // Dereference of a possibly null reference.
		if (countOfPerformedMergings == 0)
		{
			msg.AppendMessage("No mergings were performed, as per the selection you made.");
			msg.AppendMessage("The FROM and TO person records were, apparently, the same.");
			msg.AppendMessage("Please check and try again.");
			return msg.ToHtmlWarning();
		}
		else
		{
			msg.AppendMessage(string.Format("As per the selection you made, {0} person {1} merged.", countOfPerformedMergings, countOfPerformedMergings > 1 ? "records were" : "record was"));
			msg.AppendMessage("Please note that after mergings, the remaining person record could end up having duplicate detail info, such as duplicate phone numbers, related organizations, addresses or other fields.");
			msg.AppendMessage("It is advisable that you review the remaining person record and cleanup potential duplicate info related to it.");
			msg.AppendLinkButton("Review remaining person", Url.Action("View", new { PersonID = ToPersID }));
			return msg.ToHtmlSuccess();
		}
	}
	public async Task<PartialViewResult> _MergeFrom(string Name, bool NameExactMatching, string Contact, string City, List<short> Flags, List<byte> Segments)
	{
		PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
			{
				NamePattern = Name,
				NameExactMatch = NameExactMatching,
				Contact = Contact,
				City = City,
				InSegments = Segments,
				HasFlags = Flags
			};
		if (!filter.HasAnyCriteria)
		{
			AppMessage msg = new AppMessage();
			string maxRowCount = MyApp.ConfigSettings["CRM.Search.Result.MaxCount.Unfiltered"];
			msg.AppendMessage(string.Format(_limitedDefaultLookupResult, maxRowCount));
			ViewBag.AppMessage = msg.ToHtmlWarning();
		}
		List<PersonModel> viewModel = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
		return PartialView(viewModel);
	}
	public async Task<PartialViewResult> _MergeTo(string Name, bool NameExactMatching, string Contact, string City, List<short> Flags, List<byte> Segments)
	{
		PersonModel.LookupFilter filter = new PersonModel.LookupFilter()
			{
				NamePattern = Name,
				NameExactMatch = NameExactMatching,
				Contact = Contact,
				City = City,
				InSegments = Segments,
				HasFlags = Flags
			};
		if (!filter.HasAnyCriteria)
		{
			AppMessage msg = new AppMessage();
			string maxRowCount = MyApp.ConfigSettings["CRM.Search.Result.MaxCount.Unfiltered"];
			msg.AppendMessage(string.Format(_limitedDefaultLookupResult, maxRowCount));
			ViewBag.AppMessage = msg.ToHtmlWarning();
		}
		List<PersonModel> viewModel = await BeezMart.DAL.CRM.Persons.LookupAsync(filter);
		return PartialView(viewModel);
	}
}