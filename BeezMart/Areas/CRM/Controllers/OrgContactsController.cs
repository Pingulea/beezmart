﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class OrgContactsController : Controller
{

    public ViewComponentResult _List(int OrganizationID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgContacts.List), new { OrganizationID = OrganizationID });
    }

    [HttpGet]
    public ViewComponentResult _Edit(int OrganizationID, long? ContactID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgContacts.Edit), new { OrganizationID = OrganizationID, ContactID = ContactID });
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(OrgContactModel orgContact)
    {
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.OrgContacts.SaveAsync(orgContact);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete contact detail. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        List<OrgContactModel> viewModel = await BeezMart.DAL.CRM.OrgContacts.ListAsync(orgContact.OrganizationID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int OrganizationID, long ContactID)
    {
        await BeezMart.DAL.CRM.OrgContacts.DeleteAsync(ContactID);
        List<OrgContactModel> viewModel = await BeezMart.DAL.CRM.OrgContacts.ListAsync(OrganizationID);
        return PartialView("_List", viewModel);
    }
}
