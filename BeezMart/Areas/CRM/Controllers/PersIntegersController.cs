﻿using BeezMart.Entities.CRM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BeezMart.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersIntegersController : Controller
{

    public async Task<PartialViewResult> _List(int PersonID)
    {
        List<PersIntegerModel> viewModel = await BeezMart.DAL.CRM.PersIntegers.ListAsync(PersonID);
        return PartialView(viewModel);
    }

    [HttpGet]
    public async Task<PartialViewResult> _Edit(int PersonID, long? RecordID)
    {
        PersIntegerModel? integerField = null;
        if (RecordID.HasValue)
        {
            integerField = await BeezMart.DAL.CRM.PersIntegers.RetrieveAsync(RecordID.Value);
            if (integerField == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The figure record you're trying to edit no longer exist in the database. RecordID = {RecordID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (integerField == null)
        {
            integerField = new PersIntegerModel();
            integerField.PersonID = PersonID;
        }
        return PartialView(integerField);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(PersIntegerModel integerField)
    {
        List<PersIntegerModel> viewModel;
        if (!Request.Form.ContainsKey("FieldID") || string.IsNullOrEmpty(Request.Form["FieldID"][0]))
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please select the type of number you want added!");
            ViewBag.AppMessage = msg.ToHtmlError();
            viewModel = await BeezMart.DAL.CRM.PersIntegers.ListAsync(integerField.PersonID);
            return PartialView("_List", viewModel);
        }
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.PersIntegers.SaveAsync(integerField);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete person figure record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        viewModel = await BeezMart.DAL.CRM.PersIntegers.ListAsync(integerField.PersonID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int PersonID, long RecordID)
    {
        await BeezMart.DAL.CRM.PersIntegers.DeleteAsync(RecordID);
        List<PersIntegerModel> viewModel = await BeezMart.DAL.CRM.PersIntegers.ListAsync(PersonID);
        return PartialView("_List", viewModel);
    }
}
