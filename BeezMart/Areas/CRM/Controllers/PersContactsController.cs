﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersContactsController : Controller
{

    public ViewComponentResult _List(int PersonID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.PersContacts.List), new { PersonID = PersonID });
    }

    [HttpGet]
    public ViewComponentResult _Edit(int PersonID, long? ContactID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.PersContacts.Edit), new { PersonID = PersonID, ContactID = ContactID });
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(PersContactModel persContact)
    {
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.PersContacts.SaveAsync(persContact);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete contact detail. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        List<PersContactModel> viewModel = await BeezMart.DAL.CRM.PersContacts.ListAsync(persContact.PersonID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int PersonID, long ContactID)
    {
        await BeezMart.DAL.CRM.PersContacts.DeleteAsync(ContactID);
        List<PersContactModel> viewModel = await BeezMart.DAL.CRM.PersContacts.ListAsync(PersonID);
        return PartialView("_List", viewModel);
    }
}
