﻿using BeezMart.Entities.CRM;
using BeezMart.Areas.CRM.ViewModels.OrgHugetexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BeezMart.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeezMart.Areas.CRM.Controllers
{
	[Area("CRM")]
	[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
	public class OrgHugetextsController : Controller
	{

		public ViewComponentResult _Index(int OrganizationID)
		{
			return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgHugetexts.Index), new { OrganizationID = OrganizationID });
		}

		[HttpGet]
		public async Task<ViewResult> Edit(int OrganizationID, long? RecordID)
		{
			AppMessage msg = new AppMessage();
			Edit_ViewModel viewModel = new Edit_ViewModel();
			viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
			if (viewModel.Organization == null)
			{
				msg.AppendMessage($"The organization you're trying to edit details for no longer exist in the database. OrganizationID = {OrganizationID}");
				ViewBag.AppMessage = msg.ToHtmlError();
				return View("Message");
			}
			if (RecordID.HasValue)
			{
				viewModel.Hugetext = await BeezMart.DAL.CRM.OrgHugetexts.RetrieveAsync(RecordID.Value);
				if (viewModel.Hugetext == null)
				{
					msg.AppendMessage($"The detailed info you're trying to edit no longer exist in the database. RecordID = {RecordID}");
					ViewBag.AppMessage = msg.ToHtmlError();
				}
			}
			if (viewModel.Hugetext == null)
			{
				viewModel.Hugetext = new OrgHugetextModel();
				viewModel.Hugetext.OrganizationID = OrganizationID;
			}
			return View(viewModel);
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
		{
			AppMessage msg = new AppMessage();
			if (ModelState.IsValid)
			{
				if (viewModel.Hugetext == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Hugetext.");
				if (viewModel.Organization == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Organization.");
				if (viewModel.Organization.ID == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Organization.ID.");
                viewModel.Hugetext.OrganizationID = viewModel.Organization.ID.Value;
				bool success = await BeezMart.DAL.CRM.OrgHugetexts.SaveAsync(viewModel.Hugetext);
				if (success)
				{
					return RedirectToAction("View", "Organizations", new { OrganizationID = viewModel.Hugetext.OrganizationID, tab = 3 });
				}
			}
			else
			{
				msg.AppendMessage("You have submitted an invalid or incomplete detailed info record. Please check and try again.");
				ViewBag.AppMessage = msg.ToHtmlError();
			}
			return View(viewModel);
		}

		[HttpGet]
		public async Task<PartialViewResult> _Detail(long RecordID)
		{
			OrgHugetextModel? viewModel = await BeezMart.DAL.CRM.OrgHugetexts.RetrieveAsync(RecordID);
			return PartialView(viewModel);
		}

		[HttpDelete]
		[ValidateAntiForgeryToken]
		public async Task<PartialViewResult> _Delete(int OrganizationID, long RecordID)
		{
			await BeezMart.DAL.CRM.OrgHugetexts.DeleteAsync(RecordID);
			List<OrgHugetextModel> viewModel = await BeezMart.DAL.CRM.OrgHugetexts.ListAsync(OrganizationID);
			ViewBag.OrganizationID = OrganizationID;
			return PartialView("_Index", viewModel);
		}
	}
}