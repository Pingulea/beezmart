﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class OrgFlagsController : Controller
{

    public ViewComponentResult _List(int OrganizationID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgContacts.List), new { OrganizationID = OrganizationID });
    }

    [HttpGet]
    public ViewComponentResult _Edit(int OrganizationID, long? FlagID)
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgContacts.Edit), new { OrganizationID = OrganizationID, FlagID = FlagID });
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(OrgFlagModel orgFlag)
    {
        List<OrgFlagModel> viewModel;
        if (!Request.Form.ContainsKey("FieldID") || string.IsNullOrEmpty(Request.Form["FieldID"][0]))
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please select the type of organization flag you want added!");
            ViewBag.AppMessage = msg.ToHtmlError();
            viewModel = await BeezMart.DAL.CRM.OrgFlags.ListAsync(orgFlag.OrganizationID);
            return PartialView("_List", viewModel);
        }
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.OrgFlags.SaveAsync(orgFlag);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete organization flag. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        viewModel = await BeezMart.DAL.CRM.OrgFlags.ListAsync(orgFlag.OrganizationID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int OrganizationID, long RecordID)
    {
        await BeezMart.DAL.CRM.OrgFlags.DeleteAsync(RecordID);
        List<OrgFlagModel> viewModel = await BeezMart.DAL.CRM.OrgFlags.ListAsync(OrganizationID);
        return PartialView("_List", viewModel);
    }
}