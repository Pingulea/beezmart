﻿using BeezMart.Areas.CRM.ViewModels.Reports;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class ReportsController : Controller
{

    public ActionResult Index()
    {
        return View();
    }

    public ViewResult PersonsByJobType(short? JobTypeID)
    {

        return View();
    }

    public ViewResult EmailAddresses()
    {
        EmailAddresses_ViewModel viewModel = new EmailAddresses_ViewModel();
        return View(viewModel);
    }
}
