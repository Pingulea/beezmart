﻿using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersFlagsController : Controller
{

    public async Task<PartialViewResult> _List(int PersonID)
    {
        List<PersFlagModel> viewModel = await BeezMart.DAL.CRM.PersFlags.ListAsync(PersonID);
        return PartialView(viewModel);
    }

    [HttpGet]
    public async Task<PartialViewResult> _Edit(int PersonID, long? FlagID)
    {
        PersFlagModel? persFlag = null;
        if (FlagID.HasValue)
        {
            persFlag = await BeezMart.DAL.CRM.PersFlags.RetrieveAsync(FlagID.Value);
            if (persFlag == null)
            {
                AppMessage msg = new AppMessage();
                msg.AppendMessage($"The flag you're trying to edit no longer exist in the database. FlagID = {FlagID}.");
                ViewBag.AppMessage = msg.ToHtmlError();
            }
        }
        if (persFlag == null)
        {
            persFlag = new PersFlagModel();
            persFlag.PersonID = PersonID;
        }
        return PartialView(persFlag);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Edit(PersFlagModel persFlag)
    {
        List<PersFlagModel> viewModel;
        if (!Request.Form.ContainsKey("FieldID") || string.IsNullOrEmpty(Request.Form["FieldID"][0]))
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("Please select the type of person flag you want added!");
            ViewBag.AppMessage = msg.ToHtmlError();
            viewModel = await BeezMart.DAL.CRM.PersFlags.ListAsync(persFlag.PersonID);
            return PartialView("_List", viewModel);
        }
        if (ModelState.IsValid)
        {
            await BeezMart.DAL.CRM.PersFlags.SaveAsync(persFlag);
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete person flag. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
        }
        viewModel = await BeezMart.DAL.CRM.PersFlags.ListAsync(persFlag.PersonID);
        return PartialView("_List", viewModel);
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Delete(int PersonID, long RecordID)
    {
        await BeezMart.DAL.CRM.PersFlags.DeleteAsync(RecordID);
        List<PersFlagModel> viewModel = await BeezMart.DAL.CRM.PersFlags.ListAsync(PersonID);
        return PartialView("_List", viewModel);
    }
}
