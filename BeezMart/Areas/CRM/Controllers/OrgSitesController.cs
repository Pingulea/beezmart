﻿using BeezMart.Areas.CRM.ViewModels.OrgSites;
using BeezMart.Entities.CRM;
using BeezMart;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class OrgSitesController : Controller
{

    public async Task<ViewResult> Index(int OrganizationID)
    {
        Index_ViewModel viewModel = new Index_ViewModel();
        viewModel.OrgSites = await BeezMart.DAL.CRM.OrgSites.ListAllAsync(OrganizationID);
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
        return View(viewModel);
    }

    public async Task<PartialViewResult> _IndexOther(int OrganizationID)
    {
        IndexOther_ViewModel viewModel = new IndexOther_ViewModel();
        viewModel.OrgSites = await BeezMart.DAL.CRM.OrgSites.ListOtherAsync(OrganizationID);
        viewModel.OrganizationID = OrganizationID;
        return PartialView(viewModel);
    }

    public async Task<PartialViewResult> _IndexAll(int OrganizationID)
    {
        IndexAll_ViewModel viewModel = new IndexAll_ViewModel();
        viewModel.OrgSites = await BeezMart.DAL.CRM.OrgSites.ListAllAsync(OrganizationID);
        viewModel.OrganizationID = OrganizationID;
        return PartialView(viewModel);
    }

    [HttpGet]
    public async Task<ViewResult> Edit(int SiteAddressID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.OrgSite = await BeezMart.DAL.CRM.OrgSites.RetrieveAsync(SiteAddressID);
        if (viewModel.OrgSite == null)
        {
            AppMessage msg = new AppMessage();
            msg.Type = AppMessageType.Error;
            msg.AppendMessage("The organization address record you are looking for does not exist.");
            msg.AppendMessage(string.Format("The reference SiteAddressID={0} could not be found in the database.", SiteAddressID));
            msg.AppendLinkButton("Lookup organizations", Url.Action("Search"));
            return View("Message", msg);
        }
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(viewModel.OrgSite.OrganizationID);
        if (viewModel.Organization == null) throw new NullReferenceException($"Could not find the Organization record in the database. ViewModel.Organization is null. Organization.ID was {viewModel.OrgSite.OrganizationID}.");
        if (viewModel.OrgSite.ID == viewModel.Organization.SiteIdHome) viewModel.IsHome = true;
        if (viewModel.OrgSite.ID == viewModel.Organization.SiteIdBill) viewModel.IsBilling = true;
        if (viewModel.OrgSite.ID == viewModel.Organization.SiteIdShip) viewModel.IsShipping = true;
        if (Request.Headers.ContainsKey("Referer")) viewModel.NextURL = Request.Headers["Referer"][0];
        if (string.IsNullOrEmpty(viewModel.NextURL) && Request.Headers.ContainsKey("Referrer")) viewModel.NextURL = Request.Headers["Referrer"][0];
        if (string.IsNullOrEmpty(viewModel.NextURL)) viewModel.NextURL = MyApp.EnvSettings.Hosting.RootURL;
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
    {
        AppMessage msg = new AppMessage();
        if (viewModel.OrgSite == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.OrgSite.");
        if (viewModel.OrgSite.ID == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.OrgSite.ID.");
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(viewModel.OrgSite.OrganizationID);
        if (viewModel.Organization == null) throw new NullReferenceException($"Could not find the record in the database: ViewModel.Organization is null. ID was {viewModel.OrgSite.OrganizationID}.");
        if (viewModel.Organization.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Organization.ID is null.");
        if (!ModelState.IsValid)
        {
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View(viewModel);
        }
        viewModel.OrgSite.UpdatedBy = User.Identity?.Name;
        bool success = await BeezMart.DAL.CRM.OrgSites.SaveAsync(viewModel.OrgSite);
        if (success)
        {
            if (viewModel.IsHome)
            {
                viewModel.Organization.SiteIdHome = viewModel.OrgSite.ID.Value;
                await BeezMart.DAL.CRM.OrgSites.MarkForHomeAsync(viewModel.OrgSite.ID.Value, viewModel.Organization.ID.Value, this.User.Identity?.Name);
            }
            else
            {
                if (viewModel.Organization.SiteIdHome == viewModel.OrgSite.ID.Value) await BeezMart.DAL.CRM.Organizations.SetAddressForHomeAsync(viewModel.Organization.ID.Value, null);
            }
            if (viewModel.IsBilling)
            {
                viewModel.Organization.SiteIdBill = viewModel.OrgSite.ID.Value;
                await BeezMart.DAL.CRM.OrgSites.MarkForBillAsync(viewModel.OrgSite.ID.Value, viewModel.Organization.ID.Value, this.User.Identity?.Name);
            }
            else
            {
                if (viewModel.Organization.SiteIdBill == viewModel.OrgSite.ID.Value) await BeezMart.DAL.CRM.Organizations.SetAddressForBillAsync(viewModel.Organization.ID.Value, null);
            }
            if (viewModel.IsShipping)
            {
                viewModel.Organization.SiteIdShip = viewModel.OrgSite.ID.Value;
                await BeezMart.DAL.CRM.OrgSites.MarkForShipAsync(viewModel.OrgSite.ID.Value, viewModel.Organization.ID.Value, this.User.Identity?.Name);
            }
            else
            {
                if (viewModel.Organization.SiteIdShip == viewModel.OrgSite.ID.Value) await BeezMart.DAL.CRM.Organizations.SetAddressForShipAsync(viewModel.Organization.ID.Value, null);
            }
            if (string.IsNullOrEmpty(viewModel.NextURL))
            {
                return RedirectToAction("View", "Organizations", new { OrganizationID = viewModel.OrgSite.OrganizationID });
            }
            else
            {
                return Redirect(viewModel.NextURL);
            }
        }
        return View(viewModel);
    }

    [HttpGet]
    public async Task<ViewResult> Add(int OrganizationID, string Type, string NextURL)
    {
        Add_ViewModel viewModel = new Add_ViewModel();
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(OrganizationID);
        if (viewModel.Organization == null)
        {
            AppMessage msg = new AppMessage();
            msg.Type = AppMessageType.Error;
            msg.AppendMessage("The organization record you are looking for does not exist.");
            msg.AppendMessage($"The reference PersonID={OrganizationID} could not be found in the database. The application cannot add an address to a non-existing organization");
            msg.AppendLinkButton("Lookup organizations", Url.Action("Search"));
            return View("Message", msg);
        }
        viewModel.OrgSite = new OrgSiteModel();
        viewModel.OrgSite.Country = viewModel.Organization.CountryOfOrigin;
        viewModel.OrgSite.OrganizationID = OrganizationID;
        if (!string.IsNullOrWhiteSpace(Type))
        {
            string addressType = Type.ToLower();
            if (addressType.Contains("headquarter") || addressType.Contains("hq") || addressType.Contains("home")) viewModel.IsHome = true;
            if (addressType.Contains("billing") || addressType.Contains("invoicing") || addressType.Contains("bill")) viewModel.IsBilling = true;
            if (addressType.Contains("shipping") || addressType.Contains("delivery") || addressType.Contains("ship")) viewModel.IsShipping = true;
        }
        viewModel.NextURL = NextURL;
        if (string.IsNullOrEmpty(viewModel.NextURL) && Request.Headers.ContainsKey("Referer")) viewModel.NextURL = Request.Headers["Referer"][0];
        if (string.IsNullOrEmpty(viewModel.NextURL) && Request.Headers.ContainsKey("Referrer")) viewModel.NextURL = Request.Headers["Referrer"][0];
        if (string.IsNullOrEmpty(viewModel.NextURL)) viewModel.NextURL = MyApp.EnvSettings.Hosting.RootURL;
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Add(Add_ViewModel viewModel)
    {
        AppMessage msg = new AppMessage();
        if (viewModel.OrgSite == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.OrgSite.");
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(viewModel.OrgSite.OrganizationID);
        if (viewModel.Organization == null) throw new NullReferenceException($"Could not find the record in the database: ViewModel.Organization is null. ID was {viewModel.OrgSite.OrganizationID}.");
        if (viewModel.Organization.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Organization.ID is null.");
        if (!ModelState.IsValid)
        {
            foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View(viewModel);
        }
        viewModel.OrgSite.UpdatedBy = User.Identity?.Name;
        bool success = await BeezMart.DAL.CRM.OrgSites.SaveAsync(viewModel.OrgSite);
        if (viewModel.OrgSite.ID == null) throw new NullReferenceException("After having saved the record in database, its ID is missing: ViewModel.OrgSite.ID is null.");
        if (success)
        {
            if (viewModel.IsHome)
            {
                viewModel.Organization.SiteIdHome = viewModel.OrgSite.ID.Value;
                await BeezMart.DAL.CRM.OrgSites.MarkForHomeAsync(viewModel.OrgSite.ID.Value, viewModel.Organization.ID.Value);
            }
            if (viewModel.IsBilling)
            {
                viewModel.Organization.SiteIdBill = viewModel.OrgSite.ID.Value;
                await BeezMart.DAL.CRM.OrgSites.MarkForBillAsync(viewModel.OrgSite.ID.Value, viewModel.Organization.ID.Value);
            }
            if (viewModel.IsShipping)
            {
                viewModel.Organization.SiteIdShip = viewModel.OrgSite.ID.Value;
                await BeezMart.DAL.CRM.OrgSites.MarkForShipAsync(viewModel.OrgSite.ID.Value, viewModel.Organization.ID.Value);
            }
            if (string.IsNullOrEmpty(viewModel.NextURL))
            {
                return RedirectToAction("View", "Organizations", new { OrganizationID = viewModel.OrgSite.OrganizationID });
            }
            else
            {
                return Redirect(viewModel.NextURL);
            }
        }
        return View(viewModel);
    }

    public async Task<ViewResult> View(int SiteAddressID)
    {
        View_ViewModel viewModel = new View_ViewModel();
        viewModel.OrgSite = await BeezMart.DAL.CRM.OrgSites.RetrieveAsync(SiteAddressID);
        if (viewModel.OrgSite == null)
        {
            AppMessage msg = new AppMessage();
            msg.Type = AppMessageType.Error;
            msg.AppendMessage("The organization address record you are looking for does not exist.");
            msg.AppendMessage(string.Format("The reference SiteAddressID={0} could not be found in the database.", SiteAddressID));
            msg.AppendLinkButton("Lookup organizations", Url.Action("Search"));
            return View("Message", msg);
        }
        viewModel.Organization = await BeezMart.DAL.CRM.Organizations.RetrieveAsync(viewModel.OrgSite.OrganizationID);
        if (viewModel.Organization == null) throw new NullReferenceException($"Could not find the record in the database: ViewModel.Organization is null. ID was {viewModel.OrgSite.OrganizationID}.");
        if (viewModel.OrgSite.ID == viewModel.Organization.SiteIdHome) viewModel.IsHome = true;
        if (viewModel.OrgSite.ID == viewModel.Organization.SiteIdBill) viewModel.IsBilling = true;
        if (viewModel.OrgSite.ID == viewModel.Organization.SiteIdShip) viewModel.IsShipping = true;
        return View(viewModel);
    }

    public ViewComponentResult _Summary1(int OrganizationID, int? SiteAddressID, string SiteAddressType = "Home")
    {
        return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.OrgSites.Summary1), new { OrganizationID = OrganizationID, SiteAddressID = SiteAddressID, SiteAddressType = SiteAddressType });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<PartialViewResult> _Pick(int OrganizationID, int SiteAddressID, string SiteAddressType = "Home")
    {
        Summary1_ViewModel viewModel = new Summary1_ViewModel();
        viewModel.OrganizationID = OrganizationID;
        viewModel.SiteAddressType = SiteAddressType;
        viewModel.SiteAddress = await BeezMart.DAL.CRM.OrgSites.RetrieveAsync(SiteAddressID);
        if (viewModel.SiteAddress == null)
        {
            AppMessage msg = new AppMessage();
            msg.Type = AppMessageType.Error;
            msg.AppendMessage("The organization address record you are looking for does not exist.");
            msg.AppendMessage(string.Format("The reference SiteAddressID={0} could not be found in the database.", SiteAddressID));
            msg.AppendLinkButton("Back to organization", Url.Action("View", "Organizations", new { OrganizationID = OrganizationID }));
            return PartialView("_Message", msg);
        }
        else
        {
            if (SiteAddressType.Contains("Home"))
            {
                await BeezMart.DAL.CRM.OrgSites.MarkForHomeAsync(SiteAddressID, OrganizationID);
            }
            if (SiteAddressType.Contains("Bill"))
            {
                await BeezMart.DAL.CRM.OrgSites.MarkForBillAsync(SiteAddressID, OrganizationID);
            }
            if (SiteAddressType.Contains("Ship"))
            {
                await BeezMart.DAL.CRM.OrgSites.MarkForShipAsync(SiteAddressID, OrganizationID);
            }
        }
        return PartialView("_Summary1", viewModel);
    }
}
