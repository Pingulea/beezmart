﻿using BeezMart.Areas.CRM.ViewModels.PersActivities;
using BeezMart.Entities.CRM;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersActivitiesController : Controller
{

    public async Task<ViewResult> Index(int PersonID)
    {
        Index_ViewModel viewModel = new Index_ViewModel { };
        viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
        viewModel.Activities = await BeezMart.DAL.CRM.PersActivities.ListAsync(PersonID);
        return View(viewModel);
    }
    public async Task<PartialViewResult> _Index(int PersonID)
    {
        List<PersActivityModel> viewModel = await BeezMart.DAL.CRM.PersActivities.ListAsync(PersonID);
        ViewBag.PersonID = PersonID;
        return PartialView(viewModel);
    }

    [HttpGet]
    public async Task<ViewResult> Edit(long ActivityID)
    {
        Edit_ViewModel viewModel = new Edit_ViewModel();
        viewModel.Activity = await BeezMart.DAL.CRM.PersActivities.RetrieveAsync(ActivityID);
        if (viewModel.Activity == null)
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage($"The activity record you're trying to edit no longer exist in the database. ActivityID = {ActivityID}.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(viewModel.Activity.PersonID);
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
    {
        if (viewModel.Activity == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Activity.");
        bool success = await BeezMart.DAL.CRM.PersActivities.SaveAsync(viewModel.Activity);
        if (success)
        {
            return RedirectToAction("View", "Persons", new { PersonID = viewModel.Activity.PersonID, tab = 4 });
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete activity record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View(viewModel);
        }
    }

    [HttpGet]
    public async Task<ViewResult> Add(int PersonID)
    {
        Add_ViewModel viewModel = new Add_ViewModel();
        viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
        if (viewModel.Person == null)
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage($"The person record no longer exist in the database. PersonID = {PersonID}.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View("Message");
        }
        if (viewModel.Person.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Person.ID is null.");
        viewModel.Activity = new PersActivityModel();
        viewModel.Activity.PersonID = viewModel.Person.ID.Value;
        return View(viewModel);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Add(Add_ViewModel viewModel)
    {
        if (viewModel.Activity == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.Activity.");
        bool success = await BeezMart.DAL.CRM.PersActivities.SaveAsync(viewModel.Activity);
        if (success)
        {
            return RedirectToAction("View", "Persons", new { PersonID = viewModel.Activity.PersonID, tab = 4 });
        }
        else
        {
            AppMessage msg = new AppMessage();
            msg.AppendMessage("You have submitted an invalid or incomplete activity record. Please check and try again.");
            ViewBag.AppMessage = msg.ToHtmlError();
            return View(viewModel);
        }
    }

    [HttpDelete]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Delete(int PersonID, long ActivityID)
    {
        await BeezMart.DAL.CRM.PersActivities.DeleteAsync(ActivityID);
        return RedirectToAction("View", "Persons", new { PersonID = PersonID, tab = 4 });
    }
}
