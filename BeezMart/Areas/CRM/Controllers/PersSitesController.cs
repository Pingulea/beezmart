﻿using BeezMart.Areas.CRM.ViewModels.PersSites;
using BeezMart.Entities.CRM;
using BeezMart;
using BeezMart.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeezMart.Areas.CRM.Controllers;

[Area("CRM")]
[Authorize(Roles = BeezMart.Security.UserRoles.UserForCRM)]
public class PersSitesController : Controller
{

	public async Task<ViewResult> Index(int PersonID)
	{
		Index_ViewModel viewModel = new Index_ViewModel();
		viewModel.PersSites = await BeezMart.DAL.CRM.PersSites.ListAllAsync(PersonID);
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
		return View(viewModel);
	}

	public async Task<PartialViewResult> _IndexOther(int PersonID)
	{
		IndexOther_ViewModel viewModel = new IndexOther_ViewModel();
		viewModel.PersSites = await BeezMart.DAL.CRM.PersSites.ListOtherAsync(PersonID);
		viewModel.PersonID = PersonID;
		return PartialView(viewModel);
	}

	public async Task<PartialViewResult> _IndexAll(int PersonID)
	{
		IndexAll_ViewModel viewModel = new IndexAll_ViewModel();
		viewModel.PersSites = await BeezMart.DAL.CRM.PersSites.ListAllAsync(PersonID);
		viewModel.PersonID = PersonID;
		return PartialView(viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Edit(int SiteAddressID)
	{
		Edit_ViewModel viewModel = new Edit_ViewModel();
		viewModel.PersSite = await BeezMart.DAL.CRM.PersSites.RetrieveAsync(SiteAddressID);
		if (viewModel.PersSite == null)
		{
			AppMessage msg = new AppMessage();
			msg.Type = AppMessageType.Error;
			msg.AppendMessage("The person address record you are looking for does not exist.");
			msg.AppendMessage(string.Format("The reference SiteAddressID={0} could not be found in the database.", SiteAddressID));
			msg.AppendLinkButton("Lookup persons", Url.Action("Search"));
			return View("Message", msg);
		}
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(viewModel.PersSite.PersonID);
		if (viewModel.Person == null) throw new NullReferenceException($"Could not find the Person record in the database. ViewModel.Person is null. Person.ID was {viewModel.PersSite.PersonID}.");
		if (viewModel.PersSite.ID == viewModel.Person.SiteIdHome) viewModel.IsHome = true;
		if (viewModel.PersSite.ID == viewModel.Person.SiteIdBill) viewModel.IsBilling = true;
		if (viewModel.PersSite.ID == viewModel.Person.SiteIdShip) viewModel.IsShipping = true;
		if (Request.Headers.ContainsKey("Referer")) viewModel.NextURL = Request.Headers["Referer"][0];
		if (string.IsNullOrEmpty(viewModel.NextURL) && Request.Headers.ContainsKey("Referrer")) viewModel.NextURL = Request.Headers["Referrer"][0];
		if (string.IsNullOrEmpty(viewModel.NextURL)) viewModel.NextURL = MyApp.EnvSettings.Hosting.RootURL;
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Edit(Edit_ViewModel viewModel)
	{
		AppMessage msg = new AppMessage();
		if (viewModel.PersSite == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.PersSite.");
		if (viewModel.PersSite.ID == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.PersSite.ID.");
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(viewModel.PersSite.PersonID);
		if (viewModel.Person == null) throw new NullReferenceException($"Could not find the record in the database: ViewModel.Person is null. ID was {viewModel.PersSite.PersonID}.");
		if (viewModel.Person.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Person.ID is null.");
		if (!ModelState.IsValid)
		{
			foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View(viewModel);
		}
		viewModel.PersSite.UpdatedBy = User.Identity?.Name;
		bool success = await BeezMart.DAL.CRM.PersSites.SaveAsync(viewModel.PersSite);
		if (success)
		{
			if (viewModel.IsHome)
			{
				viewModel.Person.SiteIdHome = viewModel.PersSite.ID.Value;
				await BeezMart.DAL.CRM.PersSites.MarkForHomeAsync(viewModel.PersSite.ID.Value, viewModel.Person.ID.Value, this.User.Identity?.Name);
			}
			else
			{
				if (viewModel.Person.SiteIdHome == viewModel.PersSite.ID.Value) await BeezMart.DAL.CRM.Persons.SetAddressForHomeAsync(viewModel.Person.ID.Value, null);
			}
			if (viewModel.IsBilling)
			{
				viewModel.Person.SiteIdBill = viewModel.PersSite.ID.Value;
				await BeezMart.DAL.CRM.PersSites.MarkForBillAsync(viewModel.PersSite.ID.Value, viewModel.Person.ID.Value, this.User.Identity?.Name);
			}
			else
			{
				if (viewModel.Person.SiteIdBill == viewModel.PersSite.ID.Value) await BeezMart.DAL.CRM.Persons.SetAddressForBillAsync(viewModel.Person.ID.Value, null);
			}
			if (viewModel.IsShipping)
			{
				viewModel.Person.SiteIdShip = viewModel.PersSite.ID.Value;
				await BeezMart.DAL.CRM.PersSites.MarkForShipAsync(viewModel.PersSite.ID.Value, viewModel.Person.ID.Value, this.User.Identity?.Name);
			}
			else
			{
				if (viewModel.Person.SiteIdShip == viewModel.PersSite.ID.Value) await BeezMart.DAL.CRM.Persons.SetAddressForShipAsync(viewModel.Person.ID.Value, null);
			}
			if (string.IsNullOrEmpty(viewModel.NextURL))
			{
				return RedirectToAction("View", "Persons", new { PersonID = viewModel.PersSite.PersonID });
			}
			else
			{
				return Redirect(viewModel.NextURL);
			}
		}
		return View(viewModel);
	}

	[HttpGet]
	public async Task<ViewResult> Add(int PersonID, string Type, string NextURL)
	{
		Add_ViewModel viewModel = new Add_ViewModel();
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(PersonID);
		if (viewModel.Person == null)
		{
			AppMessage msg = new AppMessage();
			msg.Type = AppMessageType.Error;
			msg.AppendMessage("The person record you are looking for does not exist.");
			msg.AppendMessage(string.Format("The reference PersonID={0} could not be found in the database. The application cannot add an address to a non-existing person.", PersonID));
			msg.AppendLinkButton("Lookup persons", Url.Action("Search"));
			return View("Message", msg);
		}
		viewModel.PersSite = new PersSiteModel();
		viewModel.PersSite.Country = viewModel.Person.CountryOfOrigin;
		viewModel.PersSite.PersonID = PersonID;
		if (!string.IsNullOrWhiteSpace(Type))
		{
			string addressType = Type.ToLower();
			if (addressType.Contains("home") || addressType.Contains("residence") || addressType.Contains("main")) viewModel.IsHome = true;
			if (addressType.Contains("billing") || addressType.Contains("invoicing") || addressType.Contains("bill")) viewModel.IsBilling = true;
			if (addressType.Contains("shipping") || addressType.Contains("delivery") || addressType.Contains("ship")) viewModel.IsShipping = true;
		}
		viewModel.NextURL = NextURL;
		if (string.IsNullOrEmpty(viewModel.NextURL) && Request.Headers.ContainsKey("Referer")) viewModel.NextURL = Request.Headers["Referer"][0];
		if (string.IsNullOrEmpty(viewModel.NextURL) && Request.Headers.ContainsKey("Referrer")) viewModel.NextURL = Request.Headers["Referrer"][0];
		if (string.IsNullOrEmpty(viewModel.NextURL)) viewModel.NextURL = MyApp.EnvSettings.Hosting.RootURL;
		return View(viewModel);
	}
	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<ActionResult> Add(Add_ViewModel viewModel)
	{
		AppMessage msg = new AppMessage();
		if (viewModel.PersSite == null) throw new NullReferenceException("Model binding has resulted in a null ViewModel.PersSite.");
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(viewModel.PersSite.PersonID);
		if (viewModel.Person == null) throw new NullReferenceException($"Could not find the record in the database: ViewModel.Person is null. ID was {viewModel.PersSite.PersonID}.");
		if (viewModel.Person.ID == null) throw new NullReferenceException("After retrieving the record from database, its identifier is missing: Person.ID is null.");
		if (!ModelState.IsValid)
		{
			foreach (var error in ModelState) msg.AppendMessage($"Validation ({error.Key}): {error.Value}");
			ViewBag.AppMessage = msg.ToHtmlError();
			return View(viewModel);
		}
		viewModel.PersSite.UpdatedBy = User.Identity?.Name;
		bool success = await BeezMart.DAL.CRM.PersSites.SaveAsync(viewModel.PersSite);
		if (viewModel.PersSite.ID == null) throw new NullReferenceException("After having saved the record in database, its ID is missing: ViewModel.PersSite.ID is null.");
		if (success)
		{
			if (viewModel.IsHome)
			{
				viewModel.Person.SiteIdHome = viewModel.PersSite.ID.Value;
				await BeezMart.DAL.CRM.PersSites.MarkForHomeAsync(viewModel.PersSite.ID.Value, viewModel.Person.ID.Value, updatedBy: this.User.Identity?.Name);
			}
			if (viewModel.IsBilling)
			{
				viewModel.Person.SiteIdBill = viewModel.PersSite.ID.Value;
				await BeezMart.DAL.CRM.PersSites.MarkForBillAsync(viewModel.PersSite.ID.Value, viewModel.Person.ID.Value, updatedBy: this.User.Identity?.Name);
			}
			if (viewModel.IsShipping)
			{
				viewModel.Person.SiteIdShip = viewModel.PersSite.ID.Value;
				await BeezMart.DAL.CRM.PersSites.MarkForShipAsync(viewModel.PersSite.ID.Value, viewModel.Person.ID.Value, updatedBy: this.User.Identity?.Name);
			}
			if (string.IsNullOrEmpty(viewModel.NextURL))
			{
				return RedirectToAction("View", "Persons", new { PersonID = viewModel.PersSite.PersonID });
			}
			else
			{
				return Redirect(viewModel.NextURL);
			}
		}
		return View(viewModel);
	}

	public async Task<ViewResult> View(int SiteAddressID)
	{
		View_ViewModel viewModel = new View_ViewModel();
		viewModel.PersSite = await BeezMart.DAL.CRM.PersSites.RetrieveAsync(SiteAddressID);
		if (viewModel.PersSite == null)
		{
			AppMessage msg = new AppMessage();
			msg.Type = AppMessageType.Error;
			msg.AppendMessage("The person address record you are looking for does not exist.");
			msg.AppendMessage($"The reference SiteAddressID={SiteAddressID} could not be found in the database.");
			msg.AppendLinkButton("Lookup persons", Url.Action("Search"));
			return View("Message", msg);
		}
		viewModel.Person = await BeezMart.DAL.CRM.Persons.RetrieveAsync(viewModel.PersSite.PersonID);
		if (viewModel.Person == null) throw new NullReferenceException($"Could not find the record in the database: ViewModel.Person is null. ID was {viewModel.PersSite.PersonID}.");
		if (viewModel.PersSite.ID == viewModel.Person.SiteIdHome) viewModel.IsHome = true;
		if (viewModel.PersSite.ID == viewModel.Person.SiteIdBill) viewModel.IsBilling = true;
		if (viewModel.PersSite.ID == viewModel.Person.SiteIdShip) viewModel.IsShipping = true;
		return View(viewModel);
	}

	public ViewComponentResult _Summary1(int PersonID, int? SiteAddressID, string SiteAddressType = "Home")
	{
		return ViewComponent(typeof(BeezMart.Areas.CRM.ViewComponents.PersSites.Summary1), new { PersonID = PersonID, SiteAddressID = SiteAddressID, SiteAddressType = SiteAddressType });
	}

	[HttpPost]
	[ValidateAntiForgeryToken]
	public async Task<PartialViewResult> _Pick(int PersonID, int SiteAddressID, string SiteAddressType = "Home")
	{
		Summary1_ViewModel viewModel = new Summary1_ViewModel();
		viewModel.PersonID = PersonID;
		viewModel.SiteAddressType = SiteAddressType;
		viewModel.SiteAddress = await BeezMart.DAL.CRM.PersSites.RetrieveAsync(SiteAddressID);
		if (viewModel.SiteAddress == null)
		{
			AppMessage msg = new AppMessage();
			msg.Type = AppMessageType.Error;
			msg.AppendMessage("The person address record you are looking for does not exist.");
			msg.AppendMessage($"The reference SiteAddressID={SiteAddressID} could not be found in the database.");
			msg.AppendLinkButton("Back to person", Url.Action("View", "Persons", new { PersonID = PersonID }));
			return PartialView("_Message", msg);
		}
		else
		{
			if (SiteAddressType.Contains("Home"))
			{
				await BeezMart.DAL.CRM.PersSites.MarkForHomeAsync(SiteAddressID, PersonID, updatedBy: this.User.Identity?.Name);
			}
			if (SiteAddressType.Contains("Bill"))
			{
				await BeezMart.DAL.CRM.PersSites.MarkForBillAsync(SiteAddressID, PersonID, updatedBy: this.User.Identity?.Name);
			}
			if (SiteAddressType.Contains("Ship"))
			{
				await BeezMart.DAL.CRM.PersSites.MarkForShipAsync(SiteAddressID, PersonID, updatedBy: this.User.Identity?.Name);
			}
		}
		return PartialView("_Summary1", viewModel);
	}
}