﻿using BeezMart.Entities.CRM;
using BeezMart;

namespace BeezMart.Areas.CRM.ViewModels.Persons;

public class Search_ViewModel
{
	public string? Name { get; set; }
	public bool NameExactMatching { get; set; }			// NameExactMatching
	public string? Segments { get; set; }
	public string? Flags { get; set; }
	public string? Contact { get; set; }
	public string? City { get; set; }
    public bool FilterIsSet
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Name)) return true;
            if (!string.IsNullOrEmpty(this.Contact)) return true;
            if (!string.IsNullOrEmpty(this.City)) return true;
            if (this.Segments != null && this.Segments.Length > 0) return true;
            if (this.Flags != null && this.Flags.Length > 0) return true;
            return false;
        }
    }
}
public class Search_Params
{
    public string? Name { get; set; }
    public bool NameExactMatching { get; set; }         // NameExactMatching
    public string? Contact { get; set; }
    public string? City { get; set; }
    public List<byte>? Segments { get; set; }
    public List<short>? Flags { get; set; }
    public bool FilterIsSet
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Name)) return true;
            if (!string.IsNullOrEmpty(this.Contact)) return true;
            if (!string.IsNullOrEmpty(this.City)) return true;
            if (this.Segments != null && this.Segments.Count > 0) return true;
            if (this.Flags != null && this.Flags.Count > 0) return true;
            return false;
        }
    }
}

public class Lookup_ViewModel
{
	public string? Name { get; set; }
	public bool Exact { get; set; }			// NameExactMatching
	public List<byte>? Segments { get; set; }
	public List<short>? Flags { get; set; }
	public string? Contact { get; set; }
	public string? City { get; set; }
}

public class View_ViewModel
{
	public PersonModel? Person { get; set; }
}

public class Add_ViewModel
{

	public PersonModel Person { get; set; } = new PersonModel();
	public int? OrganizationID { get; set; }
	public bool AddAddressToo { get; set; }
	public List<PersSegmentModel>? ListOfSegments { get; set; }
	public Add_ViewModel()
	{
		this.Person.CountryOfOrigin = MyApp.ConfigSettings["CRM.Default-CountryID"];
	}
}

public class Edit_ViewModel
{
	public PersonModel? Person { get; set; }
	public List<PersSegmentModel>? ListOfSegments { get; set; }
}

public class Merge_ViewModel
{
	public string? FromName { get; set; }
	public bool FromNameExactMatching { get; set; }
	public byte[]? FromSegments { get; set; }
	public short[]? FromFlags { get; set; }
	public string? FromContact { get; set; }
	public string? FromCity { get; set; }
	public string? ToName { get; set; }
	public bool ToNameExactMatching { get; set; }
	public byte[]? ToSegments { get; set; }
	public short[]? ToFlags { get; set; }
	public string? ToContact { get; set; }
	public string? ToCity { get; set; }
}