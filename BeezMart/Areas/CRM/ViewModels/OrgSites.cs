﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.OrgSites;

public class Edit_ViewModel
{
	public OrgSiteModel? OrgSite { get; set; }
	public OrganizationModel? Organization { get; set; }
	public bool IsHome { get; set; }
	public bool IsBilling { get; set; }
	public bool IsShipping { get; set; }
	public string? NextURL { get; set; }
}

public class Add_ViewModel
{
	public OrgSiteModel? OrgSite { get; set; }
	public OrganizationModel? Organization { get; set; }
	public bool IsHome { get; set; }
	public bool IsBilling { get; set; }
	public bool IsShipping { get; set; }
	public string? NextURL { get; set; }
}

public class View_ViewModel
{
	public OrgSiteModel? OrgSite { get; set; }
	public OrganizationModel? Organization { get; set; }
	public bool IsHome { get; set; }
	public bool IsBilling { get; set; }
	public bool IsShipping { get; set; }
}

public class Index_ViewModel
{
	public List<OrgSiteModel>? OrgSites { get; set; }
	public OrganizationModel? Organization { get; set; }
}

public class IndexOther_ViewModel
{
	public List<OrgSiteModel>? OrgSites { get; set; }
	public int OrganizationID { get; set; }
}

public class IndexAll_ViewModel
{
	public List<OrgSiteModel>? OrgSites { get; set; }
	public int OrganizationID { get; set; }
}

public class Summary1_ViewModel
{
	public int OrganizationID { get; set; }
	public string? SiteAddressType { get; set; }
	public OrgSiteModel? SiteAddress { get; set; }
}