﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.PersHugetexts;

public class Edit_ViewModel
{
	public PersHugetextModel? Hugetext { get; set; }
	public PersonModel? Person { get; set; }
}
