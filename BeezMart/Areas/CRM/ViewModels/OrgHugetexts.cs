﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.OrgHugetexts;

public class Edit_ViewModel
{
	public OrgHugetextModel? Hugetext { get; set; }
	public OrganizationModel? Organization { get; set; }
}
