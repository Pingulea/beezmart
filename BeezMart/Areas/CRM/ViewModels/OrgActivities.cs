﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.OrgActivities;

public class Edit_ViewModel
{
    public OrganizationModel? Organization { get; set; }
    public OrgActivityModel? Activity { get; set; }
}
public class Add_ViewModel
{
    public OrganizationModel? Organization { get; set; }
    public OrgActivityModel? Activity { get; set; }
}
public class Index_ViewModel
{
    public OrganizationModel? Organization { get; set; }
    public List<OrgActivityModel>? Activities { get; set; }
}
