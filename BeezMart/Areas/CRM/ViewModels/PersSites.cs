﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.PersSites;

public class Edit_ViewModel
{
	public PersSiteModel? PersSite { get; set; }
	public PersonModel? Person { get; set; }
	public bool IsHome { get; set; }
	public bool IsBilling { get; set; }
	public bool IsShipping { get; set; }
	public string? NextURL { get; set; }
}

public class Add_ViewModel
{
	public PersSiteModel? PersSite { get; set; }
	public PersonModel? Person { get; set; }
	public bool IsHome { get; set; }
	public bool IsBilling { get; set; }
	public bool IsShipping { get; set; }
	public string? NextURL { get; set; }
}

public class View_ViewModel
{
	public PersSiteModel? PersSite { get; set; }
	public PersonModel? Person { get; set; }
	public bool IsHome { get; set; }
	public bool IsBilling { get; set; }
	public bool IsShipping { get; set; }
}

public class Index_ViewModel
{
	public List<PersSiteModel>? PersSites { get; set; }
	public PersonModel? Person { get; set; }
}

public class IndexOther_ViewModel
{
	public List<PersSiteModel>? PersSites { get; set; }
	public int PersonID { get; set; }
}

public class IndexAll_ViewModel
{
	public List<PersSiteModel>? PersSites { get; set; }
	public int PersonID { get; set; }
}

public class Summary1_ViewModel
{
	public int PersonID { get; set; }
	public string? SiteAddressType { get; set; }
	public PersSiteModel? SiteAddress { get; set; }
}
