﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.RelationOrgsPers;

public class Add_ViewModel
{
	public RelationOrgsPersModel? RelationOrgsPers { get; set; }
	public string? AfterSaveGoTo { get; set; }
}

public class _ModalAddPerson_ViewModel
{
	public int OrganizationID { get; set; }
}

public class _ModalAddOrganization_ViewModel
{
	public int PersonID { get; set; }
}
