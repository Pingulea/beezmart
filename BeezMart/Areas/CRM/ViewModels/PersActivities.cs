﻿using BeezMart.Entities.CRM;

namespace BeezMart.Areas.CRM.ViewModels.PersActivities;

public class Edit_ViewModel
{
	public PersonModel? Person { get; set; }
	public PersActivityModel? Activity { get; set; }
}
public class Add_ViewModel
{
	public PersonModel? Person { get; set; }
	public PersActivityModel? Activity { get; set; }
}
public class Index_ViewModel
{
	public PersonModel? Person { get; set; }
	public List<PersActivityModel>? Activities { get; set; }
}
